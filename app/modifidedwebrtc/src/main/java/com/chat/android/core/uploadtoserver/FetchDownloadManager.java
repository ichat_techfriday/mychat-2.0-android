package com.chat.android.core.uploadtoserver;

import android.content.Context;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.service.Constants;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2core.DownloadBlock;
import com.tonyodev.fetch2core.Downloader;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;


public class FetchDownloadManager implements FetchListener {
    private static final String TAG = "FetchDownloadManager";
    private static final FetchDownloadManager ourInstance = new FetchDownloadManager();
    private Fetch fetch;
    private Context mContext;
    private FileUploadDownloadManager fileUploadDownloadManager;

    private FileDownloadListener normalChatFileCompleteListener;
    private FileDownloadListener secretChatFileCompleteListener;

    private MessageDbController db;

    public static FetchDownloadManager getInstance() {
        return ourInstance;
    }

    private FetchDownloadManager() {
    }


    public void init(Context context) {
        mContext = context;
        db = CoreController.getDBInstance(mContext);
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        OkHttpClient.Builder builder= new OkHttpClient.Builder();
        builder.readTimeout(20_000L, TimeUnit.MILLISECONDS)
                .connectTimeout(15_000L, TimeUnit.MILLISECONDS)
                .cache(null)
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(false)
               .cookieJar(new JavaNetCookieJar(cookieManager));
  /*      builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {


                return      chain.proceed(   chain.request().newBuilder()
                        .addHeader("authorization", SessionManager.getInstance(mContext).getSecurityToken())
                .addHeader("requesttype", "site")
                .addHeader("userid", SessionManager.getInstance(mContext).getCurrentUserID())
                .addHeader("referer", Constants.BASE_IP)
                        .build());
            }
        });*/
        okhttp3.OkHttpClient okHttpClient =builder.build();

        FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(context)
                .setDownloadConcurrentLimit(30)
                .setHttpDownloader(new OkHttpDownloader(okHttpClient, Downloader.FileDownloaderType.SEQUENTIAL))
                .enableAutoStart(true)
                .enableRetryOnNetworkGain(true)
                .build();

        fetch = Fetch.Impl.getInstance(fetchConfiguration);
        fetch.addListener(this);
    }

    public void startDownloadFile(final String msgId, String serverPath, final String filePath, FileUploadDownloadManager fileUploadDownloadManager) {
        if (mContext == null) {
            mContext = CoreController.mcontext;
        }
        this.fileUploadDownloadManager = fileUploadDownloadManager;
        if (serverPath.startsWith("./")) {
            serverPath = serverPath.replaceFirst("./", Constants.SOCKET_IP);
        }



       // if (!cancelFile(msgId)) {
            MyLog.d(TAG, "startDownloadFile serverpath: " + serverPath);
            final Request request = new Request(serverPath, filePath);
            request.setPriority(Priority.HIGH);
            request.setNetworkType(NetworkType.ALL);
            //request.addHeader("clientKey", "SD78DF93_3947&MVNGHE1WONG");
            request.addHeader("authorization", SessionManager.getInstance(mContext).getSecurityToken());
            request.addHeader("requesttype", "site");
            request.addHeader("userid", SessionManager.getInstance(mContext).getCurrentUserID());
            request.addHeader("referer", serverPath);
            request.addHeader("Accept", "application/vnd.android.package-archive");
            request.setTag(msgId);

        //cancelFile(msgId,filePath);
            //simulate download started - but it will started after few seconds
            if (AppUtils.isNetworkAvailable(mContext)) {
                clearOldFile(msgId);
               // if(!isResumableFile(msgId)) {
                    setProgress(1, msgId);
                    fetch.enqueue(request, new Func<Request>() {
                        @Override
                        public void call(@NotNull Request result) {
                            int downloadedId = result.getId();
                            db.initDownload(msgId, MessageFactory.DOWNLOAD_STATUS_DOWNLOADING, filePath, downloadedId);
                            MyLog.d(TAG, "call: " + downloadedId);
                        }
                    }, new Func<Error>() {
                        @Override
                        public void call(@NotNull Error result) {
                            MyLog.e(TAG, "call: error ", result.getThrowable());
                        }
                    });
               // }
                //}
            }
            else{
                Toast.makeText(mContext,mContext.getString(R.string.no_internet_connection),Toast.LENGTH_SHORT).show();
            }
      //  }
    }

    private void deleteExistingFile(String filePath) {
        File file=new File(filePath);
        file.delete();
        File file1=new File(filePath);
        if(!file1.exists()){
            try {
                file1.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void cancelDownload(String msgId) {
        int downloadId = db.getParticularMessage(msgId).getDownloadId();
        if (downloadId != 0)
            fetch.cancel(downloadId);
    }

    @Override
    public void onAdded(@NotNull Download download) {
        MyLog.d(TAG, "onAdded: ");
    }

    @Override
    public void onCancelled(@NotNull Download download) {
        MyLog.d(TAG, "onCancelled: ");
    }

    @Override
    public void onCompleted(@NotNull Download download) {
        MyLog.d(TAG, "onCompleted: ");
        String msgId = download.getTag();
        setCompletedProgress(msgId);
        String localPath = db.getParticularMessage(msgId).getDownloadingPath();
        if (normalChatFileCompleteListener != null) {
            normalChatFileCompleteListener.downloadCompleted(msgId, localPath);
        }
        if (secretChatFileCompleteListener != null) {
            secretChatFileCompleteListener.downloadCompleted(msgId, localPath);
        }

    }

    @Override
    public void onDeleted(@NotNull Download download) {
        MyLog.d(TAG, "onDeleted: ");
    }

    @Override
    public void onDownloadBlockUpdated(@NotNull Download download, @NotNull DownloadBlock downloadBlock, int i) {
        MyLog.d(TAG, "onDownloadBlockUpdated: ");
    }

    @Override
    public void onError(@NotNull Download download, @NotNull Error error, @Nullable Throwable throwable) {
        MyLog.d(TAG, "onError: ");

        if(throwable!=null) {

            String errorMsg = throwable.getMessage();
            String msgId = download.getTag();
            MyLog.d(TAG, "@@onError: " + errorMsg);
            if(errorMsg!=null && (errorMsg.contains("Failed to connect") ||  errorMsg.contains("timeout"))){
             //   String msgId = download.getTag();
                MessageItemChat messageItemChat = db.getParticularMessage(msgId);
                fileUploadDownloadManager.startFileDownload(EventBus.getDefault(),messageItemChat,messageItemChat.isSecretChat());
            }else  if (errorMsg!=null && errorMsg.contains("request_not_successful")){
                //Check Type of Download
                if (normalChatFileCompleteListener != null) {
                    normalChatFileCompleteListener.DownloadError(0, msgId);
                }
                CoreController.logout(CoreController.mcontext);
                //Toast.makeText(mContext,"Network Connection is unstable",Toast.LENGTH_SHORT).show();
            }
            else{
                //stop the downloading when error
             //   String msgId = download.getTag();
                if(msgId!=null) {
                    MessageItemChat messageItemChat = db.getParticularMessage(msgId);
                    messageItemChat.setUploadDownloadProgress(0);
                    messageItemChat.setUploadStatus(MessageFactory.UPLOAD_STATUS_PAUSED);
                    db.updateChatMessage(messageItemChat, messageItemChat.getType());
                }
                }

        }

    }

    @Override
    public void onPaused(@NotNull Download download) {
        MyLog.d(TAG, "onPaused: ");
    }

    @Override
    public void onProgress(@NotNull Download download, long l, long l1) {
        int progress = download.getProgress();
        String msgId = download.getTag();

        MyLog.d(TAG, "onProgress: " + progress + " msgId: " + msgId);
        setProgress(progress, msgId);


    }

    void setCompletedProgress(String msgId){
        try {
            MyLog.d(TAG, "setCompletedProgress: ");
            MessageItemChat messageItemChat = db.getParticularMessage(msgId);
            messageItemChat.setUploadDownloadProgress(100);
            messageItemChat.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_COMPLETED);
            db.updateChatMessage(messageItemChat, messageItemChat.getType());
        }
        catch (Exception e){
            MyLog.e(TAG, "setCompletedProgress: ",e );
        }
        }

    void setProgress(int progress, String msgId) {
        //for simulate user to feel it uploading
        if (progress == 0){
            progress = 2;
        }

        MyLog.e("progress","progress"+progress);
        MessageItemChat messageItemChat = db.getParticularMessage(msgId);
        messageItemChat.setUploadDownloadProgress(progress);
        MyLog.d(TAG, "onProgress messageId from DB: " + messageItemChat.getMessageId());
        db.updateChatMessage(messageItemChat, messageItemChat.getType());
        if (normalChatFileCompleteListener != null)
            normalChatFileCompleteListener.progress(progress, msgId);
        if (secretChatFileCompleteListener != null)
            secretChatFileCompleteListener.progress(progress, msgId);
    }

    @Override
    public void onQueued(@NotNull Download download, boolean b) {

    }

    @Override
    public void onRemoved(@NotNull Download download) {
        MyLog.d(TAG, "onRemoved: ");
    }

    @Override
    public void onResumed(@NotNull Download download) {
        MyLog.d(TAG, "onResumed: ");
    }

    @Override
    public void onStarted(@NotNull Download download, @NotNull List<? extends DownloadBlock> list, int i) {
        MyLog.d(TAG, "onStarted: ");
    }

    @Override
    public void onWaitingNetwork(@NotNull Download download) {
        MyLog.d(TAG, "onWaitingNetwork: ");
    }

    public void normalChatListener(FileDownloadListener fileDownloadListener) {
        normalChatFileCompleteListener = fileDownloadListener;
    }

    public void secretChatListener(FileDownloadListener fileDownloadListener) {
        secretChatFileCompleteListener = fileDownloadListener;
    }

    public void pauseFile(String msgId) {
        try {
            MyLog.d(TAG, "pauseFile: ");
            MessageItemChat messageItemChat = db.getParticularMessage(msgId);
            int downloadId = messageItemChat.getDownloadId();
            if (downloadId != 0) {
                MyLog.d(TAG, "pauseFile: downloadId" + downloadId);
                fetch.pause(downloadId);
                messageItemChat.setUploadDownloadProgress(0);
                messageItemChat.setUploadStatus(MessageFactory.UPLOAD_STATUS_PAUSED);
                db.updateChatMessage(messageItemChat, messageItemChat.getType());
            }
        } catch (Exception e) {
            MyLog.e(TAG, "pauseFile: ", e);
        }
    }

    public void cancelFile(String msgId,String filePath) {
        try {
            MyLog.d(TAG, "cancelFile: ");
            MessageItemChat messageItemChat = db.getParticularMessage(msgId);
            int downloadId = messageItemChat.getDownloadId();
            if (downloadId != 0) {
                deleteExistingFile(filePath);
                MyLog.d(TAG, "cancelFile: downloadId" + downloadId);
                fetch.cancel(downloadId);
                return;
            }
        } catch (Exception e) {
            MyLog.e(TAG, "pauseFile: ", e);
        }
        MyLog.d(TAG, "cancelFile: no its new file");
    }

    public boolean isResumableFile(String msgId) {
        try {
            MessageItemChat messageItemChat = db.getParticularMessage(msgId);
            int downloadId = messageItemChat.getDownloadId();
            if (downloadId != 0) {
                fetch.resume(downloadId);
                return true;
            }
        } catch (Exception e) {
            MyLog.e(TAG, "pauseFile: ", e);
        }
        return false;
    }

    public void clearOldFile(String msgId) {
        try {
            MessageItemChat messageItemChat = db.getParticularMessage(msgId);
            int downloadId = messageItemChat.getDownloadId();
            if (downloadId != 0) {
                fetch.cancel(downloadId);
                fetch.remove(downloadId);
                fetch.delete(downloadId);

            }
        } catch (Exception e) {
            MyLog.e(TAG, "pauseFile: ", e);
        }


}
}
