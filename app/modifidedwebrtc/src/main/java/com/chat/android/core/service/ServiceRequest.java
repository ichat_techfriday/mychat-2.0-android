package com.chat.android.core.service;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.chat.android.app.utils.MyLog;

import java.util.HashMap;
import java.util.Map;

/**
 * * Created by  CASPERON TECH on 10/5/2016.
 */
public class ServiceRequest {

    private String TAG = ServiceRequest.class.getCanonicalName();
    private Context context;
    private ServiceListener mServiceListener;
    private StringRequest stringRequest;

    public interface ServiceListener {
        int NOCONNECTIONERROR = 0;
        int AUTHFAILUREERROR = 1;
        int SERVERERROR = 2;
        int NETWORKERROR = 3;
        int PARSEERROR = 4;
        void onCompleteListener(String response);
        void onErrorListener(int state);
    }

    public ServiceRequest(Context context) {
        this.context = context;
    }

    public void makeServiceRequest(final String url, int method, final HashMap<String, String> param, ServiceListener listener) {

        this.mServiceListener = listener;
        stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mServiceListener.onCompleteListener(response);
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String json = null;

                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    Log.e("Error"," Error"+new String(response.data));
                    Log.e("Error"," statusCode"+response.statusCode);

                    /*  switch(response.statusCode){
                        case 400:
                            json = new String(response.data);
                            json = trimMessage(json, "message");
                            if(json != null) displayMessage(json);
                            break;
                    }*/
                    //Additional cases
                }
                int state = ServiceListener.NOCONNECTIONERROR;
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    state = ServiceListener.NOCONNECTIONERROR;

                } else if (error instanceof AuthFailureError) {
                    state = ServiceListener.AUTHFAILUREERROR;
                } else if (error instanceof ServerError) {
                    state = ServiceListener.SERVERERROR;
                } else if (error instanceof NetworkError) {
                    state = ServiceListener.NETWORKERROR;
                } else if (error instanceof ParseError) {
                    state = ServiceListener.PARSEERROR;
                }
                mServiceListener.onErrorListener(state);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                MyLog.d(TAG, param.toString());
                return param;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("referer",Constants.BASE_IP);
                return headers;
            }

        };
        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandler.getInstance(context).addToRequestQueue(stringRequest);
    }
}
