package com.chat.android.core.scimbohelperclass;

/**
 * Created by CAS60 on 2/17/2017.
 */
public class ScimboFontUtils {

    public static String getAvnNextLTProDemiName() {
        String font = "fonts/AvenirNextLTProDemi.ttf";
        return font;
    }

    public static String getAvnNextLTProRegularName() {
        String font = "fonts/AvenirNextLTProRegular.ttf";
        return font;
    }

    public static String getAvnNextLTProBoldName() {
        String font = "fonts/Avenir-Next-LT-Pro-bold.ttf";
        return font;
    }

    public static String getRobotoMediumName() {
        String font = "fonts/Roboto-Medium.ttf";
        return font;
    }

    public static String getRobotoRegularName() {
        String font = "fonts/robotoregular.ttf";
        return font;
    }
    public static String getWhatsappFontStyle() {
        return "fonts/robotoregular.ttf";
    }

    public static String getWhatsappBoldFontStyle() {
        //return "fonts/Avenir-Next-LT-Pro-bold.ttf";
        return "fonts/Roboto-Bold.ttf";
    }
}
