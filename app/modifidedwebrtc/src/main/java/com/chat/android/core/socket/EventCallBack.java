package com.chat.android.core.socket;

import com.chat.android.core.model.SendMessageEvent;

public interface EventCallBack{
    void onMessageEvent(SendMessageEvent event);
}
