package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chat.android.R;

/**
 * Created by user145 on 4/6/2018.
 */

public class VHDeleteSelf extends RecyclerView.ViewHolder {

    public TextView tv_time;
    public View selection_layout;


    public VHDeleteSelf(View itemView) {
        super(itemView);
        tv_time = itemView.findViewById(R.id.tv_time);
        selection_layout=itemView.findViewById(R.id.selection_layout);
    }
}
