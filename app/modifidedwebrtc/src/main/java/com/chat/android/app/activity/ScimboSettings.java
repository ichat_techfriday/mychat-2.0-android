package com.chat.android.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.chat.android.R;
import com.chat.android.app.adapter.UltimateSettingAdapter;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.ScimboSettingsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ScimboSettings extends CoreActivity implements View.OnClickListener {

    /* Display the settings page - options to do stuff like help, check profile etc */
    private ListView lv_Settings;
    private AvnNextLTProRegTextView Username, Status;
    private ImageView profileImage, backnavigate;
    private RelativeLayout headerlayout;

    /*private String[] values = { "Help", "Profile", "Account", "Chat setting",
        "Notification", "Contacts" };*/
    private String[] values = {"Account", /*"Chats", */"Notifications", "Email Settings"
            , "Contacts", "About & Help"};

    private int[] imagesIcon = {R.drawable.ic_account,
            /*R.drawable.ic_chat,*/ R.drawable.ic_notify, R.drawable.ic_mail, R.drawable.ic_contact,
            R.drawable.ic_help};

    ArrayList<ScimboSettingsModel> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        setTitle("Settings");

        // Setting the menu String value to get them from the resource/strings
        values[0] = getString(R.string.Account);
        values[1] = getString(R.string.Notifications);
        values[2] = getString(R.string.email_settings);
        values[3] = getString(R.string.Contacts);
        values[4] = getString(R.string.about_and_help);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        backnavigate = findViewById(R.id.backnavigate);

        backnavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        for (int i = 0; i < values.length; i++) {
            boolean canAdd = false;
            if (i != 3) {
                canAdd = true;
            } else if (SessionManager.getInstance(this).getLockChatEnabled().equals("1")) {
                canAdd = true;
            }

            if (canAdd) {
                ScimboSettingsModel model = new ScimboSettingsModel();
                model.setTitle(values[i]);
                model.setResourceId(imagesIcon[i]);
                dataList.add(model);
            }
        }

        lv_Settings = findViewById(R.id.listViewSettings);
        LayoutInflater infalter = getLayoutInflater();
        ViewGroup header = (ViewGroup) infalter.inflate(R.layout.listviewheader, lv_Settings, false);
        lv_Settings.addHeaderView(header);
        lv_Settings.setAdapter(new UltimateSettingAdapter(ScimboSettings.this, dataList));


        Username = header.findViewById(R.id.username);
        Status = header.findViewById(R.id.userstatus);

        if (!SessionManager.getInstance(ScimboSettings.this).getcurrentUserstatus().isEmpty()) {
            Status.setText(SessionManager.getInstance(ScimboSettings.this).getcurrentUserstatus());
        }
        Username.setText(SessionManager.getInstance(ScimboSettings.this).getnameOfCurrentUser());
        String uname = Username.getText().toString();
        /*if (!uname.equals("")) {
            uname = uname.toLowerCase();
            uname = uname.substring(0, 1).toUpperCase() + uname.substring(1).toLowerCase();
        }*/
        Username.setText(uname);
        profileImage = (CircleImageView) header.findViewById(R.id.userprofile);
        headerlayout = header.findViewById(R.id.header);
        headerlayout.setOnClickListener(ScimboSettings.this);
        String pic = SessionManager.getInstance(ScimboSettings.this).getUserProfilePic();
        if (pic != null && !pic.isEmpty()) {

            /*Glide.with(Settings.this).load(pic).asBitmap()

                    .fitCenter().placeholder(R.mipmap.chat_attachment_profile_default_image_frame).
                    into(new BitmapImageViewTarget(profileImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            profileImage.setImageDrawable(circularBitmapDrawable);
                        }
                    });*/
/*            Picasso.with(this).load(AppUtils.getValidProfilePath(pic)).error(
                    R.drawable.ic_account_circle_white_64dp).into(profileImage);*/
            AppUtils.loadImage(ScimboSettings.this,AppUtils.getValidProfilePath(pic),profileImage,150,R.drawable.ic_account_circle_white_64dp);
        }

        lv_Settings.setOnItemClickListener(new OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                if (!SessionManager.getInstance(ScimboSettings.this).getLockChatEnabled().equals("1")
                        && position > 3) {
                    position++;
                }


                switch (position) {
                    case 1:
                        ActivityLauncher.launchAccount(ScimboSettings.this);
                        break;
                    case 2:
                    //    ActivityLauncher.launchChatSetting(ScimboSettings.this);
                        ActivityLauncher.launchNotification(ScimboSettings.this);

                        break;
                    case 3:
                        Intent intent = new Intent(ScimboSettings.this, EmailSettings.class);
                        startActivity(intent);
//                        Intent intent = new Intent(ScimboSettings.this, EmailSettings.class);

                        break;

                    case 4:
  /*                      Intent intent = new Intent(ScimboSettings.this, EmailSettings.class);
                        startActivity(intent);
  */
                        ActivityLauncher.launchContactSettings(ScimboSettings.this);

                        break;

                    case 5:
//                        ActivityLauncher.launchContactSettings(ScimboSettings.this);
                        ActivityLauncher.launchAbouthelp(ScimboSettings.this);

                        break;
                    case 6:
//                        ActivityLauncher.launchAbouthelp(ScimboSettings.this);
                        break;
                }

            }

        });
      //  lv_Settings.getAdapter().getItem(2);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.header:
                ActivityLauncher.launchUserProfile(ScimboSettings.this);

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Status.setText(SessionManager.getInstance(ScimboSettings.this).getcurrentUserstatus());
        Username.setText(SessionManager.getInstance(ScimboSettings.this).getnameOfCurrentUser());
        /*String uname = Username.getText().toString();
        if (!uname.equals("")) {
            uname = uname.toLowerCase();
            uname = uname.substring(0, 1).toUpperCase() + uname.substring(1).toLowerCase();
        }
        Username.setText(uname);*/

    }

}
