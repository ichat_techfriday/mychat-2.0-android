package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chat.android.R;


/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredWebLinkReceived extends RecyclerView.ViewHolder {

    public TextView senderName, message, time, tvDateLbl, tvInfoMsg, tvWebTitle, tvWebLink, tvWebLinkDesc,fromname,toname,datelbl;
    public RelativeLayout mainReceived;
    public ImageView starred, ivWebLink,userprofile;
    public RelativeLayout relative_layout_message, rlWebLink;


    public VHStarredWebLinkReceived(View view) {
        super(view);

        relative_layout_message = view.findViewById(R.id.relative_layout_message);

        senderName = view.findViewById(R.id.lblMsgFrom);

        message = view.findViewById(R.id.txtMsg);

        time = view.findViewById(R.id.ts);

        tvDateLbl = view.findViewById(R.id.tvDateLbl);

        userprofile = view.findViewById(R.id.userprofile);
        starred = view.findViewById(R.id.starredindicator);

        mainReceived = view.findViewById(R.id.mainReceived);
        rlWebLink = view.findViewById(R.id.rlWebLink);

        tvInfoMsg = view.findViewById(R.id.tvInfoMsg);
        message.setText(Html.fromHtml(message.getText().toString() + " &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;")); // 10 spaces

        tvWebTitle = view.findViewById(R.id.tvWebTitle);
        tvWebLink = view.findViewById(R.id.tvWebLink);
        tvWebLinkDesc = view.findViewById(R.id.tvWebLinkDesc);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        ivWebLink = view.findViewById(R.id.ivWebLink);
    }
}
