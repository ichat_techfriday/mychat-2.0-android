package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chat.android.R;


/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredWebLinkSent extends RecyclerView.ViewHolder {


    public TextView message, time, tvDateLbl, tvWebTitle, tvWebLink, tvWebLinkDesc,fromname,toname,datelbl;

    public ImageView singleTick, doubleTickGreen, doubleTickBlue, clock, starred, ivWebLink,userprofile;

    public RelativeLayout mainSent, rlWebLink;

    public VHStarredWebLinkSent(View view) {
        super(view);




        message = view.findViewById(R.id.txtMsg);

        time = view.findViewById(R.id.ts);

        tvDateLbl = view.findViewById(R.id.tvDateLbl);

        singleTick = view.findViewById(R.id.single_tick_green);

        doubleTickGreen = view.findViewById(R.id.double_tick_green);

        doubleTickBlue = view.findViewById(R.id.double_tick_blue);
        userprofile = view.findViewById(R.id.userprofile);
        clock = view.findViewById(R.id.clock);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        mainSent = view.findViewById(R.id.mainSent);
        rlWebLink = view.findViewById(R.id.rlWebLink);

        starred = view.findViewById(R.id.starredindicator);

        tvWebTitle = view.findViewById(R.id.tvWebTitle);
        tvWebLink = view.findViewById(R.id.tvWebLink);
        tvWebLinkDesc = view.findViewById(R.id.tvWebLinkDesc);

        ivWebLink = view.findViewById(R.id.ivWebLink);

    }
}
