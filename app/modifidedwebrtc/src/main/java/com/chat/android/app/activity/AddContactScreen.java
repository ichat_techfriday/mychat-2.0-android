package com.chat.android.app.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.AvnNextLTProDemiButton;
import com.chat.android.app.widget.AvnNextLTProRegEditText;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 10/26/2016.
 */
public class AddContactScreen extends CoreActivity {

    private static final String TAG = "AddContactScreen";
    private AvnNextLTProRegEditText name, phoneNumber;
    private AvnNextLTProDemiButton save, cancel;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        Toast.makeText(this, "Contact EVENT " + event.getEventName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact_screen);
        setTitle("Add Contact");
        name = findViewById(R.id.name);
        phoneNumber = findViewById(R.id.phoneNumber);
        save = findViewById(R.id.save);
        cancel = findViewById(R.id.cancel);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (phoneNumber.getText().length() > 0) {
                    addContactTodataBase();
                } else {
                    Toast.makeText(getApplicationContext(), "Phone Number Cannot be Empty", Toast.LENGTH_SHORT).show();
                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void addContactTodataBase() {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_ADD_CONTACT);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("msisdn", SessionManager.getInstance(this).getPhoneNumberOfCurrentUser());
            jsonObject.put("favourite", phoneNumber.getText().toString());
        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }
        messageEvent.setMessageObject(jsonObject);
        EventBus.getDefault().post(messageEvent);
    }
}
