package com.chat.android.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.core.model.ImageItem;

import java.util.ArrayList;

/**
 * Created by CAS56 on 3/13/2017.
 */
public class GridViewAdapter extends ArrayAdapter<ImageItem> {

    private Context context;
    private int layoutResourceId;
    private ArrayList<ImageItem> data = new ArrayList<ImageItem>();

    public GridViewAdapter(Context context, int layoutResourceId, ArrayList<ImageItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.duration = row.findViewById(R.id.duration);
            holder.image = row.findViewById(R.id.image);
            holder.video = row.findViewById(R.id.video);
            holder.durationbg=row.findViewById(R.id.duration_background);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        ImageItem item = data.get(position);
        if(item.getTitle().contains("video")){
            //holder.image.setImageBitmap(item.getImage());
            AppUtils.loadLocalVideoThumbanail(context,item.getImage(),holder.image);
            holder.video.setVisibility(View.VISIBLE);
            holder.duration.setVisibility(View.VISIBLE);
            holder.duration.setText(item.getDuration());
            holder.durationbg.setVisibility(View.VISIBLE);
        }
        else if(item.getTitle().contains("audio")){
            holder.video.setVisibility(View.GONE);
            holder.image.setVisibility(View.VISIBLE);
            holder.image.setImageResource(R.drawable.ic_placeholder_message);
            holder.duration.setVisibility(View.VISIBLE);
            holder.duration.setText(item.getDuration());
            holder.durationbg.setVisibility(View.VISIBLE);
        }
        else{
            holder.video.setVisibility(View.GONE);
            holder.duration.setVisibility(View.GONE);
            holder.durationbg.setVisibility(View.GONE);
            //holder.image.setImageBitmap(item.getImage());
            AppUtils.loadLocalImage(context,item.getImage(),holder.image);
        }
        return row;
    }

    static class ViewHolder {
        TextView duration;
        ImageView image,video;
        View durationbg;

    }
}
