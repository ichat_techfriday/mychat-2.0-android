package com.chat.android.status.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.SessionManager;
import com.chat.android.status.controller.StatusUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by user134 on 4/5/2018.
 */

public class StatusDB extends SQLiteOpenHelper {
    private final static int DB_Version = 3;

    private final Context mContext;
    private final static String TABLE_STATUS = "table_status";
    private final static String TABLE_MUTED_USERS = "table_muted_users";


    private final static String KEY_ID = "_id";
    private final static String KEY_DOC_ID = "doc_id";
    private final static String KEY_RECORD_ID = "record_id";
    private final static String KEY_CONV_ID = "convId";
    private final static String KEY_USER_ID = "user_id";
    private final static String KEY_IS_VIDEO = "is_video";
    private final static String KEY_IS_IMAGE = "is_image";
    private final static String KEY_IS_UPDATED_IN_SERVER = "is_updated_in_server";
    private final static String KEY_STATUS = "status";
    private final static String KEY_DATA = "data";
    private final static String KEY_IS_UPLOADED = "is_uploaded";
    private final static String KEY_LOCAL_PATH = "local_path";
    private final static String KEY_CAPTION = "caption";
    private final static String KEY_UPLOADED_TIME = "uploaded_time";
    private final static String KEY_LAST_UPDATE_TIME = "last_update_time";

    //Text Status'
    private final static String KEY_TEXT_STATUS_COLOR_CODE = "text_color_code";
    private final static String KEY_IS_TEXT_STATUS = "is_text_status";
    private final static String KEY_TEXT_FONT = "text_font";
    private final static String KEY_TEXT_CAPTION = "text_caption";
    private final static String KEY_STATUS_FEELINGS = "status_feelings";



    public static final int NOT_UPLOADED = 0;
    public static final int UPLOADING = 1;
    public static final int UPLOADED = 2;


    private static final int NOT_UPDATED_IN_SERVER = 0;
    private static final int UPDATED_IN_SERVER = 1;


    public static final String MUTED = "muted";
    public static final String UN_MUTED = "un_muted";

    private static final String TAG = StatusDB.class.getSimpleName();

    private String mCurrentUserId;

    private static final String CREATE_TABLE_STATUS = "CREATE TABLE " + TABLE_STATUS + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_IS_VIDEO + " INTEGER DEFAULT 0,"
            + KEY_IS_IMAGE + " INTEGER DEFAULT 0,"
            + KEY_IS_UPLOADED + " INTEGER DEFAULT 0,"
            + KEY_LOCAL_PATH + " TEXT,"
            + KEY_USER_ID + " TEXT,"
            + KEY_CAPTION + " TEXT,"
            + KEY_STATUS + " TEXT,"
            + KEY_DATA + " TEXT,"
            + KEY_DOC_ID + " TEXT,"
            + KEY_RECORD_ID + " TEXT,"
            + KEY_CONV_ID + " TEXT,"
            + KEY_UPLOADED_TIME + " INTEGER,"
            + KEY_IS_TEXT_STATUS + " INTEGER DEFAULT 0,"
            + KEY_TEXT_STATUS_COLOR_CODE + " TEXT,"
            + KEY_TEXT_FONT + " TEXT,"
            + KEY_TEXT_CAPTION + " TEXT,"
            + KEY_STATUS_FEELINGS + " TEXT)";

    //KEY_IS_UPDATED_IN_SERVER  0 - Not updated  1 - updated
    private static final String CREATE_TABLE_MUTE = "CREATE TABLE " + TABLE_MUTED_USERS + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_IS_UPDATED_IN_SERVER + " INTEGER DEFAULT 0,"
            + KEY_STATUS + " TEXT,"
            + KEY_USER_ID + " TEXT,"
            + KEY_LAST_UPDATE_TIME + " INTEGER)";

    private SQLiteDatabase mDatabaseInstance;

    private final static String DB_Name = "status_db";
    private Gson gson;

    private SQLiteDatabase getDatabaseInstance() {
        if (mDatabaseInstance == null) {
            mDatabaseInstance = getWritableDatabase();
        }

        if (!mDatabaseInstance.isOpen()) {
            mDatabaseInstance = getWritableDatabase();
        }

        return mDatabaseInstance;
    }

    public StatusDB(Context context) {
        super(context, DB_Name, null, DB_Version);
        this.mContext = context;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    public void close() {
        if (mDatabaseInstance != null && mDatabaseInstance.isOpen()) {
            mDatabaseInstance.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_STATUS);
        db.execSQL(CREATE_TABLE_MUTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATUS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MUTED_USERS);
        onCreate(db);
    }


    public void clearDatabase() {
        getDatabaseInstance().delete(TABLE_STATUS, null, null);
        getDatabaseInstance().delete(TABLE_MUTED_USERS, null, null);
    }
//Default Insert Status without Text status
 /*   public void insertNewStatus(List<StatusModel> statusModelList, String currentUserId) {
        if (statusModelList == null) {
            MyLog.e(TAG, "StatusTest--> insertNewStatus: statusModelList null.. check ");
            return;
        }
        try {
            String docId = "";
            getDatabaseInstance().beginTransaction();
            for (StatusModel statusModel : statusModelList) {

                String id = statusModel.getId();
                long uploadedTime = statusModel.getTimeUploaded();

                if (statusModel.getId() == null || statusModel.getId().isEmpty() || statusModel.getId().equals("0")) {
                    id = String.valueOf(System.currentTimeMillis());
                }
                if (uploadedTime <= 0) {
                    uploadedTime = System.currentTimeMillis();
                }
                else {
                    long oneDayBeforeMillis=getOneDayBeforeMillis();
                    //post uploaded time < oneDay before --> no need to show. we only show between (onedaybefore - current)
                    if(uploadedTime<oneDayBeforeMillis){
                        continue;
                    }
                }
                statusModel.setId(id);
                statusModel.setTimeUploaded(uploadedTime);
                String name= StatusUtil.getNameById(mContext,statusModel.getUserId());
                statusModel.setName(name);

                //data from others
                if (statusModel.getDocId() != null && !statusModel.getDocId().isEmpty()) {
                    docId = statusModel.getDocId();
                } else {
                    docId = currentUserId + "-" + uploadedTime;
                }
                statusModel.setDocId(docId);
                ContentValues values = new ContentValues();
                values.put(KEY_ID, AppUtils.parseLong(id));
                values.put(KEY_USER_ID, statusModel.getUserId());
                values.put(KEY_LOCAL_PATH, statusModel.getLocalPath());
                values.put(KEY_RECORD_ID,statusModel.getRecordId());
                values.put(KEY_IS_UPLOADED, NOT_UPLOADED);
                values.put(KEY_IS_IMAGE, getSQLiteBoolean(statusModel.isImage()));
                values.put(KEY_IS_VIDEO, getSQLiteBoolean(statusModel.isVideo()));
                String caption = statusModel.getCaption();
                if (caption == null)
                    caption = "";
                values.put(KEY_CAPTION, caption);
                values.put(KEY_UPLOADED_TIME, uploadedTime);
                values.put(KEY_STATUS, statusModel.getStatus());
                values.put(KEY_DATA, gson.toJson(statusModel));
                values.put(KEY_DOC_ID, docId);
                // Inserting Row
                long result = getDatabaseInstance().insert(TABLE_STATUS, null, values);
                MyLog.d(TAG, "StatusTest--> insertNewStatus result of insert: " + result);
                MyLog.d(TAG, "StatusTest--> DOCID_TEST insertNewStatus: docId: " + docId);
                MyLog.d(TAG, "StatusTest-->  insertNewStatus: localPath: " + statusModel.getLocalPath());
            }
            getDatabaseInstance().setTransactionSuccessful();
        } catch (Exception e) {
            MyLog.e(TAG, "StatusTest--> insertNewStatus: ", e);
        } finally {
            getDatabaseInstance().endTransaction();
        }
    }
*/


    //Default Insert Status with Text status
    public void insertNewStatus(List<StatusModel> statusModelList, String currentUserId) {
        if (statusModelList == null) {
            MyLog.e(TAG, "StatusTest--> insertNewStatus: statusModelList null.. check ");
            return;
        }
        try {
            String docId = "";
            getDatabaseInstance().beginTransaction();
            for (StatusModel statusModel : statusModelList) {

                String id = statusModel.getId();
                long uploadedTime = statusModel.getTimeUploaded();

                if (statusModel.getId() == null || statusModel.getId().isEmpty() || statusModel.getId().equals("0")) {
                    id = String.valueOf(System.currentTimeMillis());
                }
                if (uploadedTime <= 0) {
                    uploadedTime = System.currentTimeMillis();
                } else {
                    long oneDayBeforeMillis = getOneDayBeforeMillis();
                    //post uploaded time < oneDay before --> no need to show. we only show between (onedaybefore - current)
                    if (uploadedTime < oneDayBeforeMillis) {
                        continue;
                    }
                }
                //Set textStatus

                if (statusModel.isTextstatus()) {
                    if (id.contains("-")) {
                        String[] ids = id.split("-");
                        id = ids[1].toString().trim();
                    }
                }
                statusModel.setId(id);
                statusModel.setTimeUploaded(uploadedTime);
                String name = StatusUtil.getNameById(mContext, statusModel.getUserId());
                statusModel.setName(name);

                //data from others
                if (statusModel.getDocId() != null && !statusModel.getDocId().isEmpty()) {
                    docId = statusModel.getDocId();
                } else {
                    docId = currentUserId + "-" + uploadedTime;
                }
                statusModel.setDocId(docId);


                ContentValues values = new ContentValues();
                //Set textStatus
                if (statusModel.isTextstatus()) {

                    if (id != null && !id.isEmpty() && !id.equals("null")) {
                        try {
                            long l = Long.parseLong(id);
                            values.put(KEY_ID, l);
                        } catch (NumberFormatException e) {
                            Log.e("NumberFormatException: ", "NumberFormatException" + e.getMessage());
                            values.put(KEY_ID, id);

                        }
                    }


                    values.put(KEY_IS_TEXT_STATUS, getSQLiteBoolean(statusModel.isTextstatus()));
                    values.put(KEY_TEXT_STATUS_COLOR_CODE, statusModel.getTextstatus_color_code());
                    values.put(KEY_TEXT_FONT, statusModel.getTextstatus_text_font());
                    String textcaption = statusModel.getTextstatus_caption();
                    Log.e(TAG, "KEY_TEXT_CAPTION" + textcaption);

                    values.put(KEY_TEXT_CAPTION, statusModel.getTextstatus_caption());

                    values.put(KEY_STATUS_FEELINGS, statusModel.getStatus_feelings());

                } else {
                    values.put(KEY_ID, AppUtils.parseLong(id));
                }
                //values.put(KEY_ID, AppUtils.parseLong(id));

                values.put(KEY_USER_ID, statusModel.getUserId());
                values.put(KEY_LOCAL_PATH, statusModel.getLocalPath());
                values.put(KEY_RECORD_ID, statusModel.getRecordId());
                if(statusModel.isTextstatus()){
                    values.put(KEY_IS_UPLOADED, UPLOADED);
                }
                else
                values.put(KEY_IS_UPLOADED, NOT_UPLOADED);
                values.put(KEY_IS_IMAGE, getSQLiteBoolean(statusModel.isImage()));
                values.put(KEY_IS_VIDEO, getSQLiteBoolean(statusModel.isVideo()));
                String caption = statusModel.getCaption();
                if (caption == null)
                    caption = "";
                values.put(KEY_CAPTION, caption);
                values.put(KEY_UPLOADED_TIME, uploadedTime);
                values.put(KEY_STATUS, statusModel.getStatus());
                values.put(KEY_DATA, gson.toJson(statusModel));
                values.put(KEY_DOC_ID, docId);
                // Inserting Row
                long result = getDatabaseInstance().insertWithOnConflict(TABLE_STATUS, null, values,SQLiteDatabase.CONFLICT_REPLACE);
                MyLog.d(TAG, "StatusTest--> insertNewStatus result of insert: " + result);
                MyLog.d(TAG, "StatusTest--> DOCID_TEST insertNewStatus: docId: " + docId);
                MyLog.d(TAG, "StatusTest-->  insertNewStatus: localPath: " + statusModel.getLocalPath());
            }
            getDatabaseInstance().setTransactionSuccessful();
        } catch (Exception e) {
            MyLog.e(TAG, "StatusTest--> insertNewStatus: ", e);
        } finally {
            getDatabaseInstance().endTransaction();
        }
    }

    public void insertMutedUser(String mutedUserId) {
        if (mutedUserId == null) {
            MyLog.e(TAG, "insertNewStatus: statusModelList null.. check ");
            return;
        }
        try {
            //delete if already have records
            deleteMutedUser(mutedUserId);

            //insert records
            getDatabaseInstance().beginTransaction();
            ContentValues values = new ContentValues();
            long id = System.currentTimeMillis();
            values.put(KEY_ID, id);
            values.put(KEY_USER_ID, mutedUserId);
            values.put(KEY_STATUS, MUTED);
            values.put(KEY_IS_UPDATED_IN_SERVER, NOT_UPDATED_IN_SERVER);
            values.put(KEY_LAST_UPDATE_TIME, id);
            // Inserting Row
            long result = getDatabaseInstance().insert(TABLE_MUTED_USERS, null, values);
            MyLog.d(TAG, "insertMutedUser result of insert: " + result);
            getDatabaseInstance().setTransactionSuccessful();
        } catch (Exception e) {
            MyLog.e(TAG, "insertMutedUser: ", e);
        } finally {
            getDatabaseInstance().endTransaction();
        }
    }


    private int getSQLiteBoolean(boolean input) {
        int result = 0;
        if (input)
            return 1;
        return result;
    }


    public void updateStatusUploaded(String docId) {
        try {
            MyLog.d(TAG, "updateStatusUploaded docId: " + docId);
            String selectQuery = "SELECT " + KEY_ID + " FROM " + TABLE_STATUS + " WHERE " +
                    KEY_DOC_ID + "='" + docId + "'";
            Cursor cursor = getDatabaseInstance().rawQuery(selectQuery, null);
            setStatusUploaded(cursor, docId);
        } catch (Exception e) {
            MyLog.e(TAG, "updateStatusUploaded: ", e);
        }
    }

    public void updateUserMuted(String userId) {
        try {
            MyLog.d(TAG, "updateUserMuted docId: " + userId);
            String selectQuery = "SELECT " + KEY_ID + " FROM " + TABLE_MUTED_USERS + " WHERE " +
                    KEY_USER_ID + "='" + userId + "'";
            Cursor cursor = getDatabaseInstance().rawQuery(selectQuery, null);
            updateSingleData(cursor, TABLE_MUTED_USERS, KEY_IS_UPDATED_IN_SERVER, UPDATED_IN_SERVER);
        } catch (Exception e) {
            MyLog.e(TAG, "updateStatusUploaded: ", e);
        }
    }

    public void updateConvId(String docId, String convId) {
        try {
            MyLog.d(TAG, "updateStatusUploaded docId: " + docId);
            String selectQuery = "SELECT " + KEY_CONV_ID + "," + KEY_ID + " FROM " + TABLE_STATUS + " WHERE " +
                    KEY_DOC_ID + "='" + docId + "'";
            Cursor cursor = getDatabaseInstance().rawQuery(selectQuery, null);
            updateSingleData(cursor, TABLE_STATUS, KEY_CONV_ID, convId);
        } catch (Exception e) {
            MyLog.e(TAG, "updateStatusUploaded: ", e);
        }
    }

    public void updateMuteStatus(String userId, String status) {
        try {

            String selectQuery = "SELECT * FROM " + TABLE_MUTED_USERS + " WHERE " +
                    KEY_USER_ID + "='" + userId + "'";
            Cursor cursor = getDatabaseInstance().rawQuery(selectQuery, null);
            updateSingleData(cursor, TABLE_MUTED_USERS, KEY_STATUS, status);
        } catch (Exception e) {
            MyLog.e(TAG, "updateMuteStatus: ", e);
        }
    }


    public void updateRecordId(String recordId, String docId) {
        try {
            String selectQuery = "SELECT " + KEY_DATA + "," + KEY_ID + "," + KEY_RECORD_ID + " FROM " + TABLE_STATUS + " WHERE " +
                    KEY_DOC_ID + "='" + docId + "'";
            Cursor cursor = getDatabaseInstance().rawQuery(selectQuery, null);
            updateStatusModel(cursor, recordId, docId);
        } catch (Exception e) {
            MyLog.e(TAG, "updateRecordId: ", e);
        }
    }

    public void updateStatusOfStatus(long id, String status) {
        try {
            String selectQuery = "SELECT " + KEY_STATUS + "," + KEY_ID + " FROM " + TABLE_STATUS + " WHERE " +
                    KEY_ID + "=" + id;
            Cursor cursor = getDatabaseInstance().rawQuery(selectQuery, null);
            setStatusViewed(cursor, status);
        } catch (Exception e) {
            MyLog.e(TAG, "updateStatusUploaded: ", e);
        }
    }



    public void updateStatusRecordId(String recordId, String status) {
        try {
          /*  String selectQuery = "SELECT " + KEY_STATUS + "," + KEY_RECORD_ID + " FROM " + TABLE_STATUS + " WHERE " +
                    KEY_RECORD_ID + "=" + id;*/
            String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " + KEY_RECORD_ID + " = '" + recordId + "'";

            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            setStatusViewedRecord(cursor, status);
        } catch (Exception e) {
            MyLog.e(TAG, "updateStatusUploaded: ", e);
        }
    }
    private void updateSingleData(Cursor cursor, String tableName, String key, String value) {
        try {
            MyLog.d(TAG, "updateSingleData: start");
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    try {
                        long rowId = cursor.getLong(cursor.getColumnIndex(KEY_ID));
                        cursor.close();
                        ContentValues updateValues = new ContentValues();
                        updateValues.put(key, value);
                        getDatabaseInstance().update(tableName, updateValues, KEY_ID + "=" + rowId, null);
                    } catch (Exception e) {
                        MyLog.e(TAG, "updateSingleData: ", e);
                    }
                } else {
                    cursor.close();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "updateSingleData: ", e);
        }
    }


    private void updateSingleData(Cursor cursor, String tableName, String key, int value) {
        try {
            MyLog.d(TAG, "updateSingleData: start");
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    try {
                        long rowId = cursor.getLong(cursor.getColumnIndex(KEY_ID));
                        cursor.close();
                        ContentValues updateValues = new ContentValues();
                        updateValues.put(key, value);
                        getDatabaseInstance().update(tableName, updateValues, KEY_ID + "=" + rowId, null);
                    } catch (Exception e) {
                        MyLog.e(TAG, "updateSingleData: ", e);
                    }
                } else {
                    cursor.close();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "updateSingleData: ", e);
        }
    }


    private void setStatusUploaded(Cursor cursor, String docId) {
        try {
            MyLog.d(TAG, "setStatusUploaded: start");
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    try {
                        long rowId = cursor.getLong(cursor.getColumnIndex(KEY_ID));
                        MyLog.d(TAG, "StatusTest--> setStatusUploaded: docId: " + docId);
                        MyLog.d(TAG, "StatusTest--> setStatusUploaded: rowId: " + rowId);
                        cursor.close();
                        ContentValues updateValues = new ContentValues();
                        updateValues.put(KEY_IS_UPLOADED, UPLOADED); //1 --> true (Sqlite not have boolean. so 1 - true 0 - false)
                        updateValues.put(KEY_DOC_ID, docId);
                        int result = getDatabaseInstance().update(TABLE_STATUS, updateValues, KEY_ID + "=" + rowId, null);
                        MyLog.d(TAG, "StatusTest--> setStatusUploaded: " + result);
                    } catch (Exception e) {
                        MyLog.e(TAG, "setStatusUploaded: ", e);
                    }
                } else {
                    cursor.close();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "setStatusUploaded: ", e);
        }
    }

    private void updateStatusModel(Cursor cursor, String recordId, String docId) {
        try {
            MyLog.d(TAG, "updateStatusModel: start");
            StatusModel statusModel;
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    try {
                        long rowId = cursor.getLong(cursor.getColumnIndex(KEY_ID));
                        String data = cursor.getString(cursor.getColumnIndex(KEY_DATA));
                        statusModel = gson.fromJson(data, StatusModel.class);
                        statusModel.setRecordId(recordId);
                        statusModel.setDocId(docId);
                        cursor.close();
                        ContentValues updateValues = new ContentValues();
                        updateValues.put(KEY_RECORD_ID, recordId);
                        updateValues.put(KEY_DATA, gson.toJson(statusModel)); //1 --> true (Sqlite not have boolean. so 1 - true 0 - false)
                        getDatabaseInstance().update(TABLE_STATUS, updateValues, KEY_ID + "=" + rowId, null);
                    } catch (Exception e) {
                        MyLog.e(TAG, "updateStatusModel: ", e);
                    }
                } else {
                    cursor.close();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "updateStatusModel: ", e);
        }
    }


    // 0 - un-viewed
    // 1- viewed
    // 2-viewed & updated to sender

    private void setStatusViewedRecord(Cursor cursor, String status) {
        try {
            MyLog.d(TAG, "setStatusViewed: start");
            if (cursor != null) {
                if (cursor.moveToFirst()) {

                    try {
                        long rowId = cursor.getLong(cursor.getColumnIndex(KEY_ID));
                        cursor.close();
                        ContentValues updateValues = new ContentValues();
                        updateValues.put(KEY_STATUS, status);
                        int result =    getDatabaseInstance().updateWithOnConflict(TABLE_STATUS, updateValues,KEY_ID + "=" + rowId, null,SQLiteDatabase.CONFLICT_REPLACE);
                        MyLog.e(TAG, "result: "+result);
                        } catch (Exception e) {
                        MyLog.e(TAG, "result:Exception "+ e.getMessage());
                    }
                } else {
                    cursor.close();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "setStatusViewed: ", e);
        }
    }

    private void setStatusViewed(Cursor cursor, String status) {
        try {
            MyLog.d(TAG, "setStatusViewed: start");
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    try {
                        long rowId = cursor.getLong(cursor.getColumnIndex(KEY_ID));
                        MyLog.d(TAG, "setStatusViewed: id: " + rowId);
                        cursor.close();
                        ContentValues updateValues = new ContentValues();
                        updateValues.put(KEY_STATUS, status);
                        int result = getDatabaseInstance().update(TABLE_STATUS, updateValues, KEY_ID + "=" + rowId, null);
                        MyLog.d(TAG, "setStatusViewed: " + result);
                    } catch (Exception e) {
                        MyLog.e(TAG, "setStatusViewed: ", e);
                    }
                } else {
                    cursor.close();
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "setStatusViewed: ", e);
        }
    }

    public synchronized ArrayList<StatusModel> getMyUnUploadedStatus() {
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_USER_ID + " = '" + mCurrentUserId + "' AND " + KEY_IS_UPLOADED + "=" + NOT_UPLOADED + " ORDER BY " + KEY_UPLOADED_TIME + " ASC ";
        return getList(query);
    }

    public synchronized ArrayList<StatusModel> getMyFailToUploadStatus() {
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_USER_ID + " = '" + mCurrentUserId + "' AND " + KEY_IS_UPLOADED + "=" + UPLOADING + " ORDER BY " + KEY_UPLOADED_TIME + " ASC ";
        return getList(query);
    }

    public ArrayList<StatusModel> getMyAllStatus() {
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_USER_ID + " = '" + mCurrentUserId + "' ORDER BY " + KEY_UPLOADED_TIME + " DESC ";
        return getList(query);
    }

    private long getOneDayBeforeMillis() {
        long currentTime = System.currentTimeMillis();
        long oneDayMillis = java.util.concurrent.TimeUnit.MILLISECONDS.convert(1, java.util.concurrent.TimeUnit.DAYS);
        long oneDayBeforeMillis = currentTime - oneDayMillis;
        return oneDayBeforeMillis;
    }

    public int deleteOldStatus() {

        try {
            return getDatabaseInstance().delete(TABLE_STATUS, KEY_UPLOADED_TIME + " < ?",
                    new String[]{String.valueOf(getOneDayBeforeMillis())});
        } catch (Exception e) {
            MyLog.e(TAG, "deleteOldStatus: ", e);
        }
        return -1;
    }


    public int deleteStatus(String docId) {

        MyLog.d(TAG, "deleteStatus docId: " + docId);
        try {
            return getDatabaseInstance().delete(TABLE_STATUS, KEY_DOC_ID + " = ?",
                    new String[]{docId});
        } catch (Exception e) {
            MyLog.e(TAG, "deleteStatus: ", e);
        }
        return -1;
    }


    private void deleteMutedUser(String userId) {
        try {
            getDatabaseInstance().delete(TABLE_MUTED_USERS, KEY_USER_ID + " = ?",
                    new String[]{userId});
        } catch (Exception e) {
            MyLog.e(TAG, "deleteStatus: ", e);
        }
    }


    public LinkedList<String> getStatusUserIds() {
        long oneDayMillis = java.util.concurrent.TimeUnit.MILLISECONDS.convert(1, java.util.concurrent.TimeUnit.DAYS);
        long oneDayBeforeMillis = System.currentTimeMillis() - oneDayMillis;

        String query = "SELECT  DISTINCT " + KEY_USER_ID + " FROM " + TABLE_STATUS +
                " WHERE " + KEY_UPLOADED_TIME + ">" + oneDayBeforeMillis +
                " ORDER BY " + KEY_UPLOADED_TIME + " DESC ";
        Cursor cursor = null;
        LinkedList<String> uniqueUsersList = new LinkedList<>();
        try {
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(KEY_USER_ID));
                    uniqueUsersList.add(data);
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getMyUnUploadedStatus: ", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return uniqueUsersList;
    }

    public StatusModel getRecentStatus(String userId) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_USER_ID + " = '" + userId + "' ORDER BY " + KEY_UPLOADED_TIME + " DESC LIMIT 1";
        return getSingleData(query);
    }

    public String getStatusOfStatus(long id) {
        String query = "SELECT " + KEY_STATUS + " FROM " + TABLE_STATUS + " WHERE " +
                KEY_ID + " = " + id;
        return getSingleData(query, KEY_STATUS);
    }


    public int uploadStatus(String docId) {
        String query = "SELECT " + KEY_IS_UPLOADED + " FROM " + TABLE_STATUS + " WHERE " +
                KEY_DOC_ID + " = '" + docId + "'";
        return getSingleDataInt(query, KEY_IS_UPLOADED);
    }


    public String getConvId(String docId) {
        String query = "SELECT " + KEY_CONV_ID + " FROM " + TABLE_STATUS + " WHERE " +
                KEY_DOC_ID + " = '" + docId + "'";
        return getSingleData(query, KEY_CONV_ID);
    }


    public boolean isMutedUser(String userId) {
        try {
            String query = "SELECT * FROM " + TABLE_MUTED_USERS + " WHERE " +
                    KEY_USER_ID + " ='" + userId + "' AND " + KEY_STATUS + " ='" + MUTED + "'";
            int totalCount = getItemsCount(query);
            if (totalCount > 0)
                return true;
        } catch (Exception e) {
            MyLog.e(TAG, "isMutedUser: ", e);
        }
        return false;
    }


    public int getMutedUsersCount() {
        try {
            String query = "SELECT * FROM " + TABLE_MUTED_USERS + " WHERE " +
                    KEY_STATUS + " ='" + MUTED + "'";
            return getItemsCount(query);
        } catch (Exception e) {
            MyLog.e(TAG, "isMutedUser: ", e);
        }
        return 0;
    }

    public List<String> getMutedUsersList() {
        List<String> result = new ArrayList<>();

        try {
            String query = "SELECT * FROM " + TABLE_MUTED_USERS + " WHERE " +
                    KEY_STATUS + " ='" + MUTED + "'";
            result = getList(query, KEY_USER_ID);
            if (result != null)
                return result;
        } catch (Exception e) {
            MyLog.e(TAG, "isMutedUser: ", e);
        }
        return result;
    }


    private long getIdByDocId(String docId) {
        String query = "SELECT " + KEY_ID + " FROM " + TABLE_STATUS + " WHERE " +
                KEY_DOC_ID + " = '" + docId + "'";
        return getId(query, KEY_ID);
    }


    private long getIdByRecordId(String docId) {
        String query = "SELECT " + KEY_ID + " FROM " + TABLE_STATUS + " WHERE " +
                KEY_RECORD_ID + " = '" + docId + "'";
        return getId(query, KEY_ID);
    }

    public StatusModel getParticularStatus(String id) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " + KEY_ID + " = " + id;
        return getSingleData(query);
    }

    public StatusModel getParticularStatusByRecordId(String recordId) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " + KEY_RECORD_ID + " = '" + recordId + "'";
        return getSingleData(query);
    }

    public StatusModel getParticularStatusByDocId(String docId) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " + KEY_DOC_ID + " = '" + docId + "'";
        return getSingleData(query);
    }


    public void updateFileUploadingStatus(long id, int status) {
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_IS_UPLOADED, status);
            int result = getDatabaseInstance().update(TABLE_STATUS, values, KEY_ID + "=" + id, null);
            MyLog.d(TAG, "updateFileUploadingStatus: " + result);

        } catch (Exception e) {
            MyLog.e(TAG, "updateFileUploadingStatus: ", e);
        }
    }

    public void addStatusViewedHistory(String docId, StatusHistoryModel statusHistoryModel) {
        StatusModel statusModel = getParticularStatusByDocId(docId);
        if (statusModel == null) {
            try {
                String[] splitIds = docId.split("-");
                String id = splitIds[1];
                statusModel = getParticularStatus(id);
            } catch (Exception e) {
                MyLog.e(TAG, "addStatusViewedHistory: ", e);
            }
        }
        if (statusModel != null) {
            boolean alreadyUpdated = false;
            List<StatusHistoryModel> statusHistoryList = new ArrayList<>();
            if (statusModel.getStatusViewHistory() != null && statusModel.getStatusViewHistory().size() > 0) {
                statusHistoryList = statusModel.getStatusViewHistory();
                if (statusHistoryList != null) {
                    for (StatusHistoryModel data : statusHistoryList) {
                        if (data.getUserId().equals(statusHistoryModel.getUserId())) {
                            alreadyUpdated = true;
                        }
                    }
                }
            }
            if (!alreadyUpdated) {

                if (statusHistoryList == null) {
                    statusHistoryList = new ArrayList<>();
                }
                statusHistoryList.add(statusHistoryModel);
                statusModel.setStatusViewHistory(statusHistoryList);
                String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                        KEY_ID + " = " + getIdByDocId(docId);
                Cursor cursor = getDatabaseInstance().rawQuery(query, null);

                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        try {
                            long rowId = cursor.getLong(cursor.getColumnIndex(KEY_ID));
                            MyLog.d(TAG, "addStatusViewedHistory: id: " + rowId);
                            cursor.close();
                            ContentValues values = new ContentValues();
                            values.put(KEY_DATA, gson.toJson(statusModel));
                            int result = getDatabaseInstance().update(TABLE_STATUS, values, KEY_ID + "=" + rowId, null);
                            MyLog.d(TAG, "addStatusViewedHistory: " + result);
                        } catch (Exception e) {
                            MyLog.e(TAG, "addStatusViewedHistory: ", e);
                        }
                    } else {
                        cursor.close();
                    }
                }
            }
        }
    }



    public void addStatusViewedHistoryUser(String mRecordId, StatusHistoryModel statusHistoryModel) {
        StatusModel statusModel = getParticularStatusByRecordId(mRecordId);
        if (statusModel == null) {
            try {
              /*  String[] splitIds = docId.split("-");
                String id = splitIds[1];*/
                //statusModel = getParticularStatus(id);
            } catch (Exception e) {
                MyLog.e(TAG, "addStatusViewedHistory: ", e);
            }
        }
        if (statusModel != null) {
            boolean alreadyUpdated = false;
            List<StatusHistoryModel> statusHistoryList = new ArrayList<>();
            if (statusModel.getStatusViewHistory() != null && statusModel.getStatusViewHistory().size() > 0) {
                statusHistoryList = statusModel.getStatusViewHistory();
                if (statusHistoryList != null) {
                    for (StatusHistoryModel data : statusHistoryList) {
                        if (data.getUserId().equals(statusHistoryModel.getUserId())) {
                            alreadyUpdated = true;
                        }
                    }
                }
            }
            if (!alreadyUpdated) {

                if (statusHistoryList == null) {
                    statusHistoryList = new ArrayList<>();
                }
                statusHistoryList.add(statusHistoryModel);
                statusModel.setStatusViewHistory(statusHistoryList);
                String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                        KEY_ID + " = " + getIdByRecordId(mRecordId);
                Cursor cursor = getDatabaseInstance().rawQuery(query, null);

                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        try {
                            long rowId = cursor.getLong(cursor.getColumnIndex(KEY_ID));
                            MyLog.d(TAG, "addStatusViewedHistory: id: " + rowId);
                            cursor.close();
                            ContentValues values = new ContentValues();
                            values.put(KEY_DATA, gson.toJson(statusModel));
                            int result = getDatabaseInstance().update(TABLE_STATUS, values, KEY_ID + "=" + rowId, null);
                            MyLog.d(TAG, "addStatusViewedHistory: " + result);
                        } catch (Exception e) {
                            MyLog.e(TAG, "addStatusViewedHistory: ", e);
                        }
                    } else {
                        cursor.close();
                    }
                }
            }
        }
    }

    public int getStatusCount(String userId) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_USER_ID + " = '" + userId + "'";
        return getItemsCount(query);
    }


    public int getStatusCountById(long id) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_ID + " = " + id;
        return getItemsCount(query);
    }

    public int getStatusCountByDocId(String docId) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_DOC_ID + " = '" + docId + "'";
        return getItemsCount(query);
    }


    public int getStatusCount(String userId, String status) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_USER_ID + " = '" + userId + "' AND" + KEY_STATUS + "= '" + status + "'";
        return getItemsCount(query);
    }


    public int getMyUploadedStatusCount() {
        mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_USER_ID + " = '" + mCurrentUserId + "' AND " + KEY_STATUS + "=" + "1";
        return getItemsCount(query);
    }


    public int getUnViewedStatusCount(String userId) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " +
                KEY_USER_ID + " = '" + userId + "' AND " + KEY_STATUS + "='" + "0'";
        return getItemsCount(query);
    }

    public ArrayList<StatusModel> getParticularPersonStatusList(String userId) {
        String query = "SELECT * FROM " + TABLE_STATUS + " WHERE " + KEY_USER_ID + " = '" + userId + "' ORDER BY " + KEY_UPLOADED_TIME + " ASC ";
        return getList(query);
    }


    public void clearStatusTable() {
        try {
            String query = "DELETE FROM " + TABLE_STATUS;
            getDatabaseInstance().execSQL(query);
            MyLog.d(TAG, "clearStatusTable: success");
        } catch (Exception e) {
            MyLog.e(TAG, "clearStatusTable: ", e);
        }
    }

    private int getItemsCount(String query) {
        int count=0;
        Cursor cursor = null;
        try {
            cursor = getDatabaseInstance().rawQuery(query, null);
            count= cursor.getCount();
        } catch (Exception e) {
            MyLog.e(TAG, "getItemsCount: ", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return count;
    }

    //SINGLE DATA -  StatusModel
    private StatusModel getSingleData(String query) {
        StatusModel statusModel = null;
        Cursor cursor = null;
        try {
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(KEY_DATA));
                    statusModel = gson.fromJson(data, StatusModel.class);

                }

            }
        } catch (Exception e) {
            MyLog.e(TAG, "getSingleData: ", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return statusModel;
    }


    private String getSingleData(String query, String key) {
        String data = null;
        Cursor cursor = null;
        try {
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    data = cursor.getString(cursor.getColumnIndex(key));
                }

            }
        } catch (Exception e) {
            MyLog.e(TAG, "getSingleData: ", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return data;
    }


    private int getSingleDataInt(String query, String key) {
        int data = 0;
        Cursor cursor = null;
        try {
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    data = cursor.getInt(cursor.getColumnIndex(key));
                }

            }
        } catch (Exception e) {
            MyLog.e(TAG, "getSingleDataInt: ", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return data;
    }

    private long getId(String query, String key) {
        long data = 0L;
        Cursor cursor = null;
        try {
            cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    data = cursor.getLong(cursor.getColumnIndex(key));
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getMyUnUploadedStatus: ", e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return data;
    }


    private ArrayList<StatusModel> getList(String query) {
        ArrayList<StatusModel> result = new ArrayList<>();
        try {
            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(KEY_DATA));
                    long id = cursor.getLong(cursor.getColumnIndex(KEY_ID));
                    StatusModel statusModel = gson.fromJson(data, StatusModel.class);
                    statusModel.setId(String.valueOf(id));
                    result.add(statusModel);
                }
                cursor.close();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getMyUnUploadedStatus: ", e);
        }

        return result;
    }


    private ArrayList<String> getList(String query, String key) {
        ArrayList<String> result = new ArrayList<>();
        try {
            Cursor cursor = getDatabaseInstance().rawQuery(query, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String data = cursor.getString(cursor.getColumnIndex(key));
                    result.add(data);
                }
                cursor.close();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getMyUnUploadedStatus: ", e);
        }

        return result;
    }

}
