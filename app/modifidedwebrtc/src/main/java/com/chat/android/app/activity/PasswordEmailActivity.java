package com.chat.android.app.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.chat.android.R;
import com.chat.android.core.CoreActivity;

/**
 * Created by CAS63 on 4/26/2017.
 */
public class PasswordEmailActivity extends CoreActivity {

    ImageView back_navigator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_mail);
        back_navigator = findViewById(R.id.back_navigator);

        back_navigator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
