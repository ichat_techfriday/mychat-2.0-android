package com.chat.android.app.adapter;

import android.content.Context;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.chat.android.R;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.app.widget.AvnNextLTProDemiTextView;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.model.ScimboContactModel;

import java.util.ArrayList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


/**
 */
public class SecretChatAdapter extends RecyclerView.Adapter<SecretChatAdapter.ViewHolder> implements Filterable {
    Session session;
    private Context context;
    public List<ScimboContactModel> mDisplayedValues;
    private List<ScimboContactModel> mOriginalValues;
    private UserInfoSession userInfoSession;
    private Getcontactname getcontactname;
    private static final String TAG = "SecretChatAdapter";
    public SecretChatAdapter(Context context, List<ScimboContactModel> data) {
        this.context = context;
        this.mDisplayedValues = data;
        this.mOriginalValues = data;
        session = new Session(context);
        userInfoSession = new UserInfoSession(context);
        getcontactname = new Getcontactname(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Typeface face2 = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        public AvnNextLTProDemiTextView tvName;
        public AvnNextLTProRegTextView mobileText;
        protected EmojiconTextView tvStatus;
        protected CircleImageView ivUser;
        public int pos;

        public ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.userName_contacts);
            tvStatus = itemView.findViewById(R.id.status_contacts);
            ivUser = itemView.findViewById(R.id.userPhoto_contacts);
            mobileText = itemView.findViewById(R.id.mobileText);
            tvStatus.setTypeface(face2);
            tvStatus.setTextSize(11);
        }
        //protected Transformation transformation;
    }

    public ScimboContactModel getItem(int position) {
        return mDisplayedValues.get(position);
    }

    public void updateInfo(List<ScimboContactModel> aitem) {
        this.mDisplayedValues = aitem;
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.secret_chat_contact_list_items, parent, false);

        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ScimboContactModel contact = mDisplayedValues.get(position);
        viewHolder.tvStatus.setTextSize(11);
        viewHolder.tvName.setText(contact.getFirstName());
        viewHolder.pos = position;

        try {
            String userId = contact.get_id();
            getcontactname.configProfilepic(viewHolder.ivUser, userId, true, true, R.mipmap.chat_attachment_profile_default_image_frame);

            if (!contact.getStatus().contentEquals("")) {
                getcontactname.setProfileStatusText(viewHolder.tvStatus, userId, contact.getStatus(), true);
            } else {
                viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_not_available));
            }
        } catch (Exception e) {
            viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_not_available));
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<ScimboContactModel>) results.values; // has the filtered values

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<ScimboContactModel> FilteredArrList = new ArrayList<>();
try {
    if (mOriginalValues == null) {
        mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
    }

    if (constraint == null || constraint.length() == 0) {

        // set the Original result to return
        results.count = mOriginalValues.size();
        results.values = mOriginalValues;
    } else {
        constraint = constraint.toString().toLowerCase();
        for (int i = 0; i < mOriginalValues.size(); i++) {


            String contactName = mOriginalValues.get(i).getFirstName();
            String contactNo = mOriginalValues.get(i).getNumberInDevice();

            if (contactName.toLowerCase().contains(constraint) || contactNo.toLowerCase().contains(constraint)) {
                ScimboContactModel mycontact = new ScimboContactModel();
                mycontact.setFirstName(mOriginalValues.get(i).getFirstName());

                mycontact.set_id(mOriginalValues.get(i).get_id());
                mycontact.setStatus(mOriginalValues.get(i).getStatus());
                mycontact.setAvatarImageUrl(mOriginalValues.get(i).getAvatarImageUrl());
                mycontact.setNumberInDevice(mOriginalValues.get(i).getNumberInDevice());

                FilteredArrList.add(mycontact);
            }


        }
        // set the Filtered result to return
        results.count = FilteredArrList.size();
        results.values = FilteredArrList;
    }
}
catch (Exception e){
    MyLog.e(TAG, "performFiltering: ",e );
}
                return results;
            }
        };
        return filter;
    }


}


