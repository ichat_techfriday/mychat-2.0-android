package com.chat.android.app.activity;

import com.luseen.screenshotobserver.ScreenshotObserverService;

/**
 * Created by user134 on 5/18/2018.
 */

public class ScreenShotDetector extends ScreenshotObserverService {
    private static ScreenShotListener screenShotListener;
public interface ScreenShotListener{
    void screenShotTaken();
}
public static void setListener(ScreenShotListener listener){
    screenShotListener=listener;
}

    @Override
    protected void onScreenShotTaken(String path, String fileName) {
if(screenShotListener!=null)
    screenShotListener.screenShotTaken();
    }
}
