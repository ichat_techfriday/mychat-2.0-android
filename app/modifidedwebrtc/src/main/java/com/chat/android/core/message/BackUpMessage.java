package com.chat.android.core.message;

import com.chat.android.app.utils.MyLog;
import com.google.android.gms.drive.DriveId;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CAS60 on 5/11/2017.
 */
public class BackUpMessage {
    private static final String TAG = "BackUpMessage";
    public JSONObject getBackUpMessageObject(String currentUserId, long gdFileSize, String gdFileName, DriveId gdFileId, long backUpTS,
                                       String backUpDuration, String backupGmailId, String backUpOver) {
        JSONObject driveObj = new JSONObject();
        try {
            driveObj.put("FileSize", gdFileSize);
            driveObj.put("FileName", gdFileName);
            driveObj.put("DriveId", gdFileId);
            driveObj.put("CreatedTs", backUpTS);
            driveObj.put("BackUpDuration", backUpDuration);
            driveObj.put("BackUpGmailId", backupGmailId);
            driveObj.put("BackUpOver", backUpOver);

            JSONObject object = new JSONObject();
            object.put("from", currentUserId);
            object.put("drive_settings", driveObj);

        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }

        return driveObj;
    }

}
