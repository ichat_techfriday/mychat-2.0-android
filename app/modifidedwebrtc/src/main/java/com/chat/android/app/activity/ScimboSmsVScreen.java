package com.chat.android.app.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.SharedPreference;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.SCLoginModel;
import com.chat.android.core.scimbohelperclass.ScimboDialogUtils;
import com.chat.android.core.scimbohelperclass.ScimboSMSReceiver;
import com.chat.android.core.service.Constants;
import com.chat.android.core.service.ServiceRequest;
import com.chat.android.fcm.MyFirebaseInstanceIDService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

/*
 * Created by  CASPERON TECH on 10/5/2016.
 */
public class ScimboSmsVScreen extends CoreActivity {

    private TextView tv, mobile_number, instructionlabel;
    private ImageButton editNumber;
    private ProgressBar progressBar;
    private int progressStatus = 0;
    private Handler handler = new Handler();
    ImageView back, iv_okbutton;

    EditText edit_text, edEmailOTP;
    ///private TextView timer;
    private AlertDialog.Builder alertDialog;
    private String tag_string_req = "string_req";
    protected static final String TAG = "ScimboSmsVScreen";
    IntentFilter intentFilter;
    private TextView textresend;

    TextView timer;
    private final AtomicInteger mRefreshRequestCounter = new AtomicInteger(0);
    ScimboSmsVScreen msessionmanager;
    /* Object of the receiver */
    private ScimboSMSReceiver readSms;
    CountDownTimer cTimer = null;

    private String GCM_ID = "";

    private SCLoginModel SCLoginModel = null;


    private String Handy_username = "";
    private String Handy_email = "";
    private String Handy_phone = "";
    private String Handy_password = "";
    private boolean isTwitgo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isTwitgo = getResources().getBoolean(R.bool.is_twitgo);

        if (isTwitgo)
            setContentView(R.layout.activity_sms_verification_screen_twitgo);
        else
            setContentView(R.layout.activity_sms_verification_screen);
        SessionManager.getInstance(ScimboSmsVScreen.this).IsnumberVerified(false);
        SessionManager.getInstance(ScimboSmsVScreen.this).Islogedin(false);


        back = findViewById(R.id.back);
        iv_okbutton = findViewById(R.id.okButton);
        mobile_number = findViewById(R.id.textView1);
        mobile_number.setText("to  " + SessionManager.getInstance(this).getPhoneNumberOfCurrentUser());
        // editNumber = (ImageButton) findViewById(R.id.editNumber);
        edit_text = findViewById(R.id.edit_text);
        if (isTwitgo)
            edEmailOTP = findViewById(R.id.tvEmailOTPResend);
        textresend = findViewById(R.id.textresend);

        timer = findViewById(R.id.timer);
        boolean navigateFrom = getIntent().getBooleanExtra("FromVerifyPage", false);


        Intent s = getIntent();
        GCM_ID = s.getStringExtra("gcmid");

        if (navigateFrom) {
            SessionManager.getInstance(ScimboSmsVScreen.this).setOTPCountDownTime();
        }

        initProgress(getString(R.string.loading_in), true);
        displayTimer();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityLauncher.launchVerifyPhoneScreen(ScimboSmsVScreen.this);
            }
        });
        textresend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resend();
                //VerifyPhoneNumber.makeStringReq();

            }
        });
        iv_okbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTwitgo) {
                    if (edEmailOTP.getText().toString().isEmpty()) {
                        Toast.makeText(ScimboSmsVScreen.this, "Please enter email OTP", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (edit_text.getText().toString().length() == 4) {
                    verifyCode(edit_text.getText().toString());
                } else if (edit_text.getText().toString().length() == 0) {
                    Toast.makeText(ScimboSmsVScreen.this, "Please enter the Verification Code", Toast.LENGTH_SHORT).show();
                }

            }
        });

        Session session = new Session(this);
        session.putgalleryPrefs("def");


        readSms = new ScimboSMSReceiver() {
            @Override
            protected void onSmsReceived(String s) {
                /* Call the verify SMS code API */
                MyLog.e("SMS Code", s);
                //     edit_text.setText(s);
                verifyCode(s);
            }
        };


        mobile_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityLauncher.launchVerifyPhoneScreen(ScimboSmsVScreen.this);
            }
        });

        intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(1000);


        progressBar = findViewById(R.id.progressBar1);
        // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        // progressBar.textView_Timer
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    // Update the progress bar and display the

                    // current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressStatus);
                            //timer.setText(progressStatus + "/"
                            // + progressBar.getMax());
                            // timer.setText(progressStatus+"/"+progressBar.getMax());
                        }
                    });
                    try {
                        // Sleep for 2 seconds.

                        // Just to display the progress slowly
                        Thread.sleep(900);
                    } catch (InterruptedException e) {
                        MyLog.e(TAG, "", e);
                    }
                }
            }
        }).start();


        // Alert Builder to accept verification code from user
        alertDialog = new AlertDialog.Builder(ScimboSmsVScreen.this);
        alertDialog.setMessage("Enter Verification Code");
        alertDialog.setCancelable(false);
        final EditText input = new EditText(ScimboSmsVScreen.this);
        input.setText("");

        //Auto Verify
        //autoVerify();
        //verifyCode(bundle.getCharSequence("code").toString());
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (input.getText().toString().length() == 4) {
                    verifyCode(input.getText().toString());
                }
            }
        });

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setView(input);

        // alertDialog.show();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                try {
                    //alertDialog.show();
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }, 300 * 3 * 1);

        if (SessionManager.getInstance(ScimboSmsVScreen.this).getTwilioMode().equalsIgnoreCase(
                SessionManager.TWILIO_DEV_MODE)) {
            MyLog.d(TAG, "onCreate: otp : " + SessionManager.getInstance(ScimboSmsVScreen.this).getLoginOTP());
            // edit_text.setText(SessionManager.getInstance(ScimboSmsVScreen.this).getLoginOTP());
        }
    }

    private void autoVerify() {
        try {
            String code = getIntent().getStringExtra("otp");
            verifyCode(code);
        } catch (Exception e) {
            MyLog.e(TAG, "onCreate: ", e);
        }

    }

    private void displayTimer() {
        long smsSentAt = SessionManager.getInstance(this).getOTPCountDownTime();
        long timeDiff = Calendar.getInstance().getTimeInMillis() - smsSentAt;

        long countDownStartFrom;
        if (timeDiff >= 0 && timeDiff <= 60000) {
            countDownStartFrom = 60000 - timeDiff;

            cTimer = new CountDownTimer(countDownStartFrom, 1000) {

                public void onTick(long millisUntilFinished) {
                    textresend.setEnabled(false);
                    timer.setText("" + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    timer.setText("0");
                    textresend.setEnabled(true);
                }
            }.start();
        } else {
            timer.setText("0");
            textresend.setEnabled(true);
        }
    }


    private void resend() {

        if (MyFirebaseInstanceIDService.token != null && MyFirebaseInstanceIDService.token.length() > 0)
            GCM_ID = MyFirebaseInstanceIDService.token;
        else
            GCM_ID = MyFirebaseInstanceIDService.getFCMToken(this);

        //System.out.println("GCMID"+""+""+GCM_ID);


        showProgressDialog();
        HashMap<String, String> params = new HashMap<String, String>();
        String cCode = SessionManager.getInstance(ScimboSmsVScreen.this).getUserCountryCode();
        String uPhone = SessionManager.getInstance(ScimboSmsVScreen.this).getPhoneNumberOfCurrentUser();

        String Countrycode = SessionManager.getInstance(ScimboSmsVScreen.this).getUserMobileNoWithoutCountryCode();


        params.put("user_name", Handy_username);
        params.put("email", Handy_email);
        params.put("password", Handy_password);

        params.put("msisdn", uPhone);
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_ID);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("PhNumber", SessionManager.getInstance(ScimboSmsVScreen.this).getUserMobileNoWithoutCountryCode());
        params.put("CountryCode", cCode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        textresend.setEnabled(false);
        ServiceRequest request = new ServiceRequest(this);
        request.makeServiceRequest(Constants.RESEND_SMS_OTP, Request.Method.POST, params, resendListener);
    }

    ServiceRequest.ServiceListener resendListener = new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
            hideProgressDialog();

            if (SCLoginModel == null)
                SCLoginModel = new SCLoginModel();

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            SCLoginModel = gson.fromJson(response, SCLoginModel.class);

            if (SCLoginModel != null) {
                MyLog.d(TAG, "onCompleteListener OTP: " + SCLoginModel.getCode());

            }
            // success
            if (SCLoginModel != null && SCLoginModel.getErrNum().equals("0")) {
                /* Move to the SMS verification screen */

                /* Save the msisdn in shared preferences here */
                SharedPreferences shPref = getSharedPreferences("global_settings",
                        MODE_PRIVATE);
                SharedPreferences.Editor et = shPref.edit();

                try {
                    if (SCLoginModel.getName() != null && !SCLoginModel.getName().contentEquals("")) {
                     /*   byte[] data = Base64.decode(SCLoginModel.getName(), Base64.DEFAULT);
                        String text = new String(data);
                        et.putString("userName", text);*/
                        et.putString("userName", SCLoginModel.getName());
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }

                try {
                    if (SCLoginModel.getStatus() != null && !SCLoginModel.getStatus().contentEquals("")) {
                        byte[] data = Base64.decode(SCLoginModel.getStatus(), Base64.DEFAULT);
                        String text = new String(data);
                        et.putString("Status", text);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }

                et.apply();

                SessionManager.getInstance(ScimboSmsVScreen.this).setOTPCountDownTime();
                displayTimer();

                try {
                    JSONObject object = new JSONObject(response);
                    String code = object.getString("code");
                    SessionManager.getInstance(ScimboSmsVScreen.this).setLoginOTP(code);

                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            } else {
                textresend.setEnabled(true);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                        ScimboSmsVScreen.this);
                alertDialog.setTitle("Login failed");
                alertDialog.setMessage("Please verify your mobile number and try again!");

                alertDialog.setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }

            // skipOTP();

        }

        @Override
        public void onErrorListener(int state) {
            hideProgressDialog();
            textresend.setEnabled(true);
            ScimboDialogUtils.showCheckInternetDialog(ScimboSmsVScreen.this);
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    /* API to verify the code */
    private void verifyCode(final String otp) {

        if (MyFirebaseInstanceIDService.token != null && MyFirebaseInstanceIDService.token.length() > 0)
            GCM_ID = MyFirebaseInstanceIDService.token;
        else
            GCM_ID = MyFirebaseInstanceIDService.getFCMToken(this);


        //System.out.println("GCMID"+""+""+GCM_ID);


        String msisdn = SessionManager.getInstance(this).getPhoneNumberOfCurrentUser();
        String mobileNo = SessionManager.getInstance(this).getUserMobileNoWithoutCountryCode();
        String countryCode = SessionManager.getInstance(this).getUserCountryCode();

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("user_name", Handy_username);
        params.put("email", Handy_email);
        params.put("password", Handy_password);

        params.put("msisdn", msisdn);
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_ID);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("PhNumber", mobileNo);
        params.put("CountryCode", countryCode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        params.put("code", otp);
        if (isTwitgo)
            params.put("email_otp", edEmailOTP.getText().toString());
        params.put("callToken", "Android");
        params.put("Appversion", "");//raad
        params.put("brand", "" + Build.MANUFACTURER);//raad
        params.put("model", "" + Build.MODEL);//raad
        params.put("pushToken", SessionManager.getInstance(this).getCurrentUserID());

        ServiceRequest request = new ServiceRequest(this);
        request.makeServiceRequest(Constants.VERIFY_SMS_CODE, Request.Method.POST, params, verifyCodeListener);

    }

    ServiceRequest.ServiceListener verifyCodeListener = new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
            MyLog.d("Loginrequest", response);

            hideProgressDialog();
            if (SCLoginModel == null)
                SCLoginModel = new SCLoginModel();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            SCLoginModel = gson.fromJson(response, SCLoginModel.class);
           Log.e("response", response);

            MyLog.d(TAG, "onCompleteListener: loginCount: " + SCLoginModel.getLoginCount());
            // success
            if (SCLoginModel.getErrNum().equals("0")) {

                // if (Constants.IS_ENCRYPTION_ENABLED) {
                setToken(SCLoginModel);
                //}
                /* Move to the profile info screen */
//                Toast.makeText(VerifyPhoneScreen.this, "Mobile number verified successfully", Toast.LENGTH_SHORT).show();
                SessionManager.getInstance(ScimboSmsVScreen.this).IsnumberVerified(true);
                SessionManager.getInstance(ScimboSmsVScreen.this).Islogedin(true);
                SessionManager.getInstance(ScimboSmsVScreen.this).setLoginCount(SCLoginModel.getLoginCount());
                //Log.d("CODE", loginModel.getCode());
                SharedPreferences shPref = getSharedPreferences("global_settings",
                        MODE_PRIVATE);
                SharedPreferences.Editor et = shPref.edit();
                //Save my token
                SharedPreference.getInstance().save(ScimboSmsVScreen.this, "securitytoken", SCLoginModel.getToken());
                et.putString("userId", SessionManager.getInstance(ScimboSmsVScreen.this).getPhoneNumberOfCurrentUser());
                et.apply();
                Session session = new Session(ScimboSmsVScreen.this);
                session.putgalleryPrefs("def");
                ActivityLauncher.launchProfileInfoScreen(ScimboSmsVScreen.this, SessionManager.getInstance(ScimboSmsVScreen.this).getPhoneNumberOfCurrentUser());

            } else {

                try {
                    JSONObject ob = new JSONObject(response);
                    Log.e("ob", "ob" + ob);
                    String message = ob.getString("message");
                    Toast.makeText(ScimboSmsVScreen.this, message, Toast.LENGTH_LONG).show();
                    SharedPreferences shPref = getSharedPreferences("global_settings",
                            MODE_PRIVATE);
                    SharedPreferences.Editor et = shPref.edit();
                    et.putString("userId", SessionManager.getInstance(ScimboSmsVScreen.this).getPhoneNumberOfCurrentUser());
                    et.apply();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ScimboSmsVScreen.this, "Response Catch Error", Toast.LENGTH_LONG).show();
                }

            }
        }

        @Override
        public void onErrorListener(int state) {
            hideProgressDialog();
        }
    };

    public void setToken(SCLoginModel SCLoginModel) {
        if (SCLoginModel.getToken() != null) {
            String tokenOriginal = SCLoginModel.getToken();
            String encodedToken = AppUtils.SHA256(tokenOriginal);
            MyLog.d(TAG, "onCompleteListener securityToken: " + tokenOriginal);

            SessionManager.getInstance(this).setUserSecurityToken(tokenOriginal.replace("\n", ""));
            if (encodedToken != null)
                SessionManager.getInstance(this).setUserSecurityTokenHash(encodedToken.replace("\n", ""));
        }
    }
}
