package com.chat.android.core.model;

/**
 * Created by CAS56 on 3/13/2017.
 */

public class ImageItem {
    private String image;
    private String title, duration;

    public ImageItem(String imagePath, String title, String duration) {
        super();
        this.image = imagePath;
        this.title = title;
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}