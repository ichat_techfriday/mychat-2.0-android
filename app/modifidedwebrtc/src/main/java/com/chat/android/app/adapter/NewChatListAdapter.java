package com.chat.android.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chat.android.R;
import com.chat.android.app.activity.ChatListFragment;
import com.chat.android.app.dialog.ProfileImageDialog;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.GroupMessageUtil;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.TimeStampUtils;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.ShortcutBadgeManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.MuteStatusPojo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.fragment.app.FragmentManager;

public class NewChatListAdapter extends BaseAdapter implements Filterable {
    public static boolean tick_show = false;
    private static final String TAG = "NewChatListAdapter";
    private Context mContext;
    ArrayList<MessageItemChat> mListData;
    public List<MessageItemChat> mDisplayedValues;
    private ShortcutBadgeManager shortcutBadgeManager;
    public Getcontactname getcontactname;
    private String currentUserId;
    private Session session;
    private UserInfoSession userInfoSession;
    private long imageTS;
    private ChatListFragment callback;
    private LayoutInflater mInflater;
    private FragmentManager fragmentManager;
    private ChatListItemClickListener listener;
    ContactDB_Sqlite contactDB_sqlite = null;
    boolean mClearChat = false;

    public NewChatListAdapter(Context mContext, ArrayList<MessageItemChat> mListData, FragmentManager fragmentManager) {

        this.mListData = mListData;
        this.mDisplayedValues = mListData;
        this.mContext = mContext;
        this.fragmentManager = fragmentManager;
        session = new Session(mContext);
        userInfoSession = new UserInfoSession(mContext);
        contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
        shortcutBadgeManager = new ShortcutBadgeManager(mContext);
        currentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        getcontactname = new Getcontactname(mContext);
        imageTS = Calendar.getInstance().getTimeInMillis();
        mInflater = LayoutInflater.from(mContext);

    }

    @Override
    public int getCount() {
        return this.mDisplayedValues.size();
    }


    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    public MessageItemChat getItems(int position) {
        return mDisplayedValues.get(position);
    }


    public class ViewHolder {

        public TextView newMessage;
        public TextView storeName, newMessageCount, newMessageDate, tvTyping;
        public de.hdodenhof.circleimageview.CircleImageView storeImage;
        public ImageView ivMsgType, mute_chatlist, tick, ivChatIcon;
        public RelativeLayout rlChat;
        public RelativeLayout chatlist;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.new_chat_list_item, parent, false);
            holder = new ViewHolder();

            holder.ivChatIcon = convertView.findViewById(R.id.iv_chat_icon);
            holder.newMessage = convertView.findViewById(R.id.newMessage);
            holder.newMessageDate = convertView.findViewById(R.id.newMessageDate);
            holder.storeName = convertView.findViewById(R.id.storeName);
            holder.storeImage = convertView.findViewById(R.id.storeImage);
            holder.newMessageCount = convertView.findViewById(R.id.newMessageCount);
            holder.tvTyping = convertView.findViewById(R.id.tvTyping);

            holder.mute_chatlist = convertView.findViewById(R.id.mute_chatlist);
            holder.ivMsgType = convertView.findViewById(R.id.ivMsgType);
            holder.tick = convertView.findViewById(R.id.tick);
            holder.rlChat = convertView.findViewById(R.id.rlChat);
            holder.chatlist = convertView.findViewById(R.id.chatlist);
            setAvenirNextLTProRegularTypeface(holder.newMessage);
            setAvnNextLTProDemiTypeface(holder.storeName);
            convertView.setTag(holder);
            convertView.setTag(R.string.position, position);
            holder.storeName.setTag(mListData.get(position).getMessageId());

            configureViewHolderChat(position, holder.ivMsgType, holder.ivChatIcon, holder.newMessage, holder.tvTyping, holder.newMessageDate, holder.storeName,
                    holder.newMessageCount, holder.storeImage, holder.mute_chatlist, holder.tick, holder.rlChat, holder.chatlist);

        } else {

            holder = (ViewHolder) convertView.getTag();
            String oldMsgId = (String) holder.storeName.getTag();


            if (!mListData.get(position).getMessageId().equals(oldMsgId)) {
/*                configureViewHolderChat(position,holder.ivMsgType,holder.ivChatIcon,holder.newMessage,holder.tvTyping,holder.newMessageDate,holder.storeName,
                        holder.newMessageCount,holder.storeImage,holder.mute_chatlist,holder.tick,holder.rlChat,holder.chatlist );*/
                holder.storeName.setTag(mListData.get(position).getMessageId());
                MyLog.d(TAG, "getView: load again " + position);
            } else {
/*                holder.storeName.post(new Runnable() {
                    @Override
                    public void run() {
                        configureViewHolderChat(position,holder.ivMsgType,holder.ivChatIcon,holder.newMessage,holder.tvTyping,holder.newMessageDate,holder.storeName,
                                holder.newMessageCount,holder.storeImage,holder.mute_chatlist,holder.tick,holder.rlChat,holder.chatlist );

                    }
                });*/
                MyLog.d(TAG, "getView: no need load" + position);
            }
        }

        configureViewHolderChat(position, holder.ivMsgType, holder.ivChatIcon, holder.newMessage, holder.tvTyping, holder.newMessageDate, holder.storeName,
                holder.newMessageCount, holder.storeImage, holder.mute_chatlist, holder.tick, holder.rlChat, holder.chatlist);
        holder.storeImage.setOnClickListener(new ImageClcik(position, holder));

        return convertView;
    }


    @Override
    public int getViewTypeCount() {
        if (getCount() > 0)
            return getCount();

        return super.getViewTypeCount();
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    private void configureViewHolderChat(int position, ImageView ivMsgType, ImageView ivChatIcon, TextView newMessage, TextView tvTyping, TextView newMessageDate, TextView storeName, TextView newMessageCount, de.hdodenhof.circleimageview.CircleImageView storeImage, ImageView mute_chatlist, ImageView tick, RelativeLayout rlChat,
                                         RelativeLayout chatlist) {
        MyLog.d(TAG, "getView: " + position);
        MyLog.d(TAG, "configureViewHolderChat: ");
        MessageItemChat chat;
        try {
            chat = mDisplayedValues.get(position);

        } catch (Exception e) {
            return;
        }
        ivMsgType.setVisibility(View.GONE);

        if (chat.isSecretChat())
            ivChatIcon.setVisibility(View.VISIBLE);
        else
            ivChatIcon.setVisibility(View.GONE);
        Log.e("newMessage", "chat.getMessageType() " + chat.getMessageType());
        Log.e("newMessage", "chat.getTextMessage() nill " + chat.getTextMessage());

        if (chat.getMessageType() != null) {
            newMessage.setVisibility(View.VISIBLE);
            tvTyping.setVisibility(View.GONE);
            if (!chat.getMessageType().equals("" + MessageFactory.text)) {
                ivMsgType.setVisibility(View.VISIBLE);
            }

            if (chat.getTypingAt() != 0) {
                newMessage.setVisibility(View.GONE);
                ivMsgType.setVisibility(View.GONE);
                tvTyping.setVisibility(View.VISIBLE);
                ivMsgType.setImageResource(0);

                if (chat.getMessageId().contains("-g-")) {
                    tvTyping.setText(chat.getTypePerson().concat(" " + mContext.getString(R.string.action_typing)));
                } else {
                    tvTyping.setText(mContext.getString(R.string.action_typing));
                }
            }


            // TODO: :Roman: Add margin to the icon
            switch (chat.getMessageType()) {
                case "" + MessageFactory.text:
                    newMessage.setTextColor(ContextCompat.getColor(mContext, R.color.chatlist_messagecolor));
                    newMessage.setText(chat.getTextMessage());
                    ivMsgType.setImageResource(0);
                    break;
                case "" + MessageFactory.picture:
                    ivMsgType.setVisibility(View.VISIBLE);
                    newMessage.setText(R.string.Image);
                    ivMsgType.setImageResource(R.drawable.ib_camera);
                    break;
                case "" + MessageFactory.contact:
                    ivMsgType.setVisibility(View.VISIBLE);
                    newMessage.setText(R.string.contact);
                    ivMsgType.setImageResource(R.drawable.ic_contacts_storage_usage);
                    break;
                case "" + MessageFactory.video:
                    ivMsgType.setVisibility(View.VISIBLE);
                    newMessage.setText(R.string.video);
                    ivMsgType.setImageResource(R.drawable.ic_video_storage_usage);
                    break;
                case "" + MessageFactory.audio:
                    ivMsgType.setVisibility(View.VISIBLE);
                    newMessage.setText(R.string.audio);
                    ivMsgType.setImageResource(R.drawable.ic_audio_storage_usage);
                    break;
                case "" + MessageFactory.document:
                    ivMsgType.setVisibility(View.VISIBLE);
                    newMessage.setText(R.string.doc);
                    ivMsgType.setImageResource(R.drawable.ic_documents_storage_usage);
                    break;
                case "" + MessageFactory.web_link:
                    ivMsgType.setVisibility(View.VISIBLE);
                    newMessage.setText(R.string.weblink);
                    ivMsgType.setImageResource(R.drawable.ic_business_link);
                    break;
                case "" + MessageFactory.location:
                    ivMsgType.setVisibility(View.VISIBLE);
                    newMessage.setText(R.string.LocationChat);
                    ivMsgType.setImageResource(R.drawable.ic_location_storage_usage);
                    break;
                case "" + MessageFactory.missed_call:
                    ivMsgType.setVisibility(View.VISIBLE);
                    String callType = chat.getCallType();

//                String tsNextLine = TimeStampUtils.get12HrTimeFormat(mContext, chat.getTS());

                    if (callType != null && callType.equals(MessageFactory.video_call + "")) {
                        newMessage.setText(R.string.missed_video_call);
                    } else {
                        newMessage.setText(R.string.missevoicecall);
                    }

                    ivMsgType.setImageResource(R.drawable.ic_missed_call);
                    break;
                case "" + MessageFactory.group_event_info:
                    ivMsgType.setImageResource(0);
                    ivMsgType.setVisibility(View.GONE);


                    String createdBy = chat.getCreatedByUserId();
                    String createdTo = chat.getCreatedToUserId();
                    if (mContext != null) {
                        String msg = GroupMessageUtil.getGroupEventMsg(chat, mContext, currentUserId, getContactNameIfExists(createdBy), getContactNameIfExists(createdTo));

                        if (msg != null) {
                            newMessage.setText(msg);
                        } else {
                            newMessage.setText("");
                        }
                    }
                    break;
                case "" + MessageFactory.DELETE_SELF:
                    ivMsgType.setVisibility(View.VISIBLE);
                    newMessage.setText(mContext.getResources().getString(R.string.you_deleted_text));
                    ivMsgType.setImageResource(R.drawable.icon_deleted);

                    break;
                case "" + MessageFactory.DELETE_OTHER:
                    ivMsgType.setVisibility(View.VISIBLE);
                    newMessage.setText(mContext.getResources().getString(R.string.other_deleted_text));
                    ivMsgType.setImageResource(R.drawable.icon_deleted);
                    break;
                case "" + MessageFactory.SCREEN_SHOT_TAKEN:
                    ivMsgType.setVisibility(View.GONE);
                    newMessage.setTextColor(ContextCompat.getColor(mContext, R.color.chatlist_messagecolor));
                    // Log.e("getTextMessage","getTextMessage"+chat.getTextMessage());
                    String name = getcontactname.getSendername(chat.getSenderMsisdn());
                    Log.e("name", "chat.getTextMessage()" + name + chat.getTextMessage());
                    if (chat.getTextMessage().contains("You")) {
                        newMessage.setText(chat.getTextMessage());
                    } else {
                        newMessage.setText(name + " " + chat.getTextMessage());

                    }
                    ivMsgType.setImageResource(0);
                    break;
                default:
                    newMessage.setText("");
                    ivMsgType.setImageResource(0);
                    break;
            }
        } else {

            Log.e("newMessage", "chat.getMessageType() nill " + chat.getMessageType());
            newMessage.setText("");
        }

        if (chat.getTypingAt() != 0) {
            newMessage.setText("");
            ivMsgType.setImageResource(0);
        }

        String Name = chat.getSenderName();
        MyLog.d(TAG, "configureViewHolderChat Name: " + Name);
        MyLog.d(TAG, "configureViewHolderChat getGroupName: " + chat.getGroupName());

        if (Name != null && !Name.equalsIgnoreCase("")) {

            storeName.setText(chat.getSenderName());

        } else if (chat.getGroupName() != null && chat.getGroupName().length() > 0) {
            storeName.setText(chat.getGroupName());
        } else {

            storeName.setText(chat.getSenderMsisdn());
        }

        if (chat.isSecretChat()) {
            storeName.setTextColor(ContextCompat.getColor(mContext, R.color.secret_chat_list_color));
        } else {
            storeName.setTextColor(ContextCompat.getColor(mContext, R.color.chat_list_header));
        }


        try {

            configureDateLabel(newMessageDate, position);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

        newMessageCount.setVisibility(View.GONE);
        String[] arrDocId = chat.getMessageId().split("-");
        String toUserId = arrDocId[1];
        String docId = currentUserId.concat("-").concat(toUserId);
        MuteStatusPojo muteData = null;
        String convId = null;


        if (chat.getMessageId().contains("-g-")) {
            docId = docId.concat("-g");
            convId = toUserId;
            muteData = contactDB_sqlite.getMuteStatus(currentUserId, null, arrDocId[1], false);

            String path = chat.getAvatarImageUrl();
            if (path != null && !path.equals("")) {

                //RequestOptions options=new RequestOptions().error(R.mipmap.group_chat_attachment_profile_icon);
                /*Glide.with(mContext).load(path)
                        .into(storeImage);*/
                AppUtils.loadImage(mContext, AppUtils.getValidGroupPath(path), storeImage, 100, R.mipmap.group_chat_attachment_profile_icon);
                //  Log.e(TAG,"AppUtils.loadImage");
            } else {
                Glide.with(mContext).load(R.mipmap.group_chat_attachment_profile_icon)
                        .into(storeImage);
                //  Log.e(TAG,"Glide.with(mContext).load(");
            }
        } else {
            convId = userInfoSession.getChatConvId(docId);
            muteData = contactDB_sqlite.getMuteStatus(currentUserId, toUserId, convId, false);
            getcontactname.configProfilepic(storeImage, toUserId, false, true, R.mipmap.chat_attachment_profile_default_image_frame);
            Log.e(TAG, "getcontactname.configProfilepic");

        }

        if (muteData != null && muteData.getMuteStatus().equals("1")) {
            if (!chat.isSecretChat())
                mute_chatlist.setVisibility(View.VISIBLE);
        } else {
            mute_chatlist.setVisibility(View.GONE);
        }


        if (convId != null && !convId.equals("")) {

            int countMsg = shortcutBadgeManager.getSingleBadgeCount(convId);
            if (countMsg > 0 || !session.getmark(toUserId)) {
                if (!chat.isSecretChat())
                    newMessageCount.setVisibility(View.VISIBLE);
                if (countMsg > 0) {
                    newMessageCount.setText("" + countMsg);
                } else {
                    newMessageCount.setText("");
                }
            } else {
                newMessageCount.setVisibility(View.GONE);
            }
        } else {
            newMessageCount.setVisibility(View.GONE);
        }

        if (chat.isSelected()) {
            tick.setVisibility(View.VISIBLE);
            chatlist.setBackgroundColor(Color.parseColor("#4DBDBDBD"));

        } else {
            tick.setVisibility(View.GONE);
            chatlist.setBackgroundColor(Color.TRANSPARENT);

        }
        Log.e("isclearchat", "isclearchat" + chat.isclearchat());
        if (chat.isclearchat()) {
            newMessage.setVisibility(View.GONE);
            ivMsgType.setVisibility(View.GONE);

        }


    }

    private String getContactNameIfExists(String userId) {
        String userName = null;
        MyLog.d(TAG, "getUserOpponenetDetails: fromadapter ");
        //ScimboContactModel contact = contactDB_sqlite.getUserOpponenetDetails(userId);
//        ScimboContactModel contact = CoreController.getContactsDbInstance(mContext).getUserDetails(userId);
        String msisdn = contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.MSISDN);
        if (msisdn != null) {
            userName = getcontactname.getSendername(userId, msisdn);

           /* if (msisdn.equalsIgnoreCase("null")) {
                userName = contact.getFirstName();
            }*/
        } else {
            if (callback != null) {
                callback.getUserDetails(userId);
            }
        }
        return userName;
    }

    private void configureDateLabel(TextView tvDateLbl, int position) {

        MessageItemChat item = mDisplayedValues.get(position);
        if (item.getTS() != null && !item.getTS().equals("")) {
            String currentItemTS = item.getTS();

            if (currentItemTS.equals("0")) {
                tvDateLbl.setText("");
            } else {

                Date currentItemDate = TimeStampUtils.getMessageTStoDate(mContext, currentItemTS);
                if (currentItemDate != null) {
                    String mydate = TimeStampUtils.get12HrTimeFormat(mContext, item.getTS());
                    mydate = mydate.replace(".", "");
                    setDateText(tvDateLbl, currentItemDate, currentItemTS, mydate);
                } else {
                    tvDateLbl.setText("");
                }
            }
        } else {
            tvDateLbl.setText("");
        }
    }

    private void setDateText(TextView tvDateLbl, Date currentItemDate, String ts, String time) {
        Date today = TimeStampUtils.getDateFormat(Calendar.getInstance().getTimeInMillis());
        Date yesterday = TimeStampUtils.getYesterdayDate(today);

        if (currentItemDate.equals(today)) {
            tvDateLbl.setText(time);
        } else if (currentItemDate.equals(yesterday)) {
            tvDateLbl.setText("Yesterday");
        } else {
            DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            String formatDate = df.format(currentItemDate);
            tvDateLbl.setText(formatDate);
        }
    }

    private class ImageClcik implements View.OnClickListener {

        int position = 0;
        ViewHolder holder;

        public ImageClcik(int position, ViewHolder holder) {

            this.position = position;
            this.holder = holder;


        }

        @Override
        public void onClick(View view) {

            MessageItemChat list = mDisplayedValues.get(position);

            String Uid = mDisplayedValues.get(position).getMessageId().split("-")[1];
            Bundle bundle = new Bundle();
            bundle.putSerializable("MessageItem", mDisplayedValues.get(position));
            bundle.putString("userID", Uid);

            bundle.putSerializable("ProfilePic", null);
            //need
            if (list.isGroup()) {
                bundle.putSerializable("GroupChat", true);
            } else {
                bundle.putSerializable("GroupChat", false);
            }
            bundle.putLong("imageTS", imageTS);
            bundle.putBoolean("FromSecretChat", false);
            ProfileImageDialog dialog = new ProfileImageDialog();
            dialog.setArguments(bundle);
            dialog.show(fragmentManager, "profile");

        }
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<MessageItemChat>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
                    // Toast.makeText(mContext, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<MessageItemChat> FilteredArrList = new ArrayList<>();

                if (mListData == null) {
                    mListData = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mListData.size();
                    results.values = mListData;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mListData.size(); i++) {


                        String senderName = mListData.get(i).getSenderName();
                        if (senderName.toLowerCase().contains(constraint)) {
                            FilteredArrList.add(mListData.get(i));
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public interface ChatListItemClickListener {

        void onItemLongClick(MessageItemChat messageItemChat, View view, int position);
    }

    public void setChatListItemClickListener(ChatListItemClickListener listener) {
        this.listener = listener;
    }

    public void setAvenirNextLTProRegularTypeface(TextView textView) {
        Typeface face = Typeface.createFromAsset(mContext.getAssets(),
                "fonts/AvenirNextLTProRegular.ttf");
        textView.setTypeface(face);
    }

    public void setAvnNextLTProDemiTypeface(TextView textView) {
        Typeface face = Typeface.createFromAsset(mContext.getAssets(),
                "fonts/AvenirNextLTProDemi.ttf");
        textView.setTypeface(face);
    }
}

