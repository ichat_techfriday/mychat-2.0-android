package com.chat.android.status.view.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.CoreController;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.ScimboContactModel;
import com.chat.android.core.service.Constants;
import com.chat.android.status.controller.SharedPrefUtil;
import com.chat.android.status.controller.StatusUtil;
import com.chat.android.status.controller.interfaces.OnListClickListener;
import com.chat.android.status.view.activity.StatusPrivacyActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by user134 on 4/20/2018.
 */

public class StatusPrivacyContactsAdapter extends RecyclerView.Adapter<StatusPrivacyContactsAdapter.MyViewHolder> implements Filterable {


    private Context context;
    private ArrayList<ScimboContactModel> mData;
    private ArrayList<ScimboContactModel> mDisplayedValues;
    private Getcontactname getcontactname;
    private String type;
    private boolean selectAllClicked = false;
    private static final String TAG = StatusPrivacyContactsAdapter.class.getSimpleName();
    private int totalSelected=0 ;
    private OnListClickListener onListClickListener;
    private ContactDB_Sqlite contactDB_sqlite;
    private List<String> previousSelectedUsers=new ArrayList<>();

    public StatusPrivacyContactsAdapter(Context context, String type, boolean selectAllClicked, OnListClickListener onListClickListener) {
        this.context = context;
         contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        getcontactname = new Getcontactname(context);
        mData = contactDB_sqlite.getSavedScimboContacts();
        mDisplayedValues=mData;
        this.type = type;
        this.selectAllClicked = selectAllClicked;
        this.onListClickListener=onListClickListener;
        previousSelectedUsers =prevSelectedlist();

        if(previousSelectedUsers!=null && previousSelectedUsers.size()>0){
            for(int i=0;i<mDisplayedValues.size();i++){
                String userId=mDisplayedValues.get(i).get_id();
                if(userId!=null && previousSelectedUsers.contains(userId)){
                    mDisplayedValues.get(i).setSelected(true);
                    totalSelected++;
                }
            }
        }
    }

    public  List<String> prevSelectedlist(){
        String prevSelectedType = SharedPrefUtil.getPrivacyType();
        //only show the previous users if we select same type.
        // ex: Prev -> Hide Status From (We select 4 ) Now also select Hide Status From (We need to show prev 4 members)
        // Else no need to show.
        if(prevSelectedType.equals(type)){
            String commaSeparatedUserIds= SharedPrefUtil.getSelectedContactIds();
            MyLog.d(TAG, "StatusPrivacyContactsAdapter: "+commaSeparatedUserIds);
            MyLog.d(TAG, "StatusPrivacyContactsAdapter: "+previousSelectedUsers.size());
            return Arrays.asList(commaSeparatedUserIds.split("\\s*,\\s*"));
        }
        return new ArrayList<>();
    }




    public ArrayList<ScimboContactModel> getData() {
        return mDisplayedValues;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.status_listrow_privacy_contacts, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ScimboContactModel contact = mDisplayedValues.get(position);
        if (contact.getFirstName() != null)
            holder.tvName.setText(contact.getFirstName());
        else
            holder.tvName.setText(contact.getMsisdn());

        try {
            String userId = contact.get_id();
            String imageTS = contactDB_sqlite.getDpUpdatedTime(userId);
            if(imageTS==null || imageTS.isEmpty())
                imageTS="1";
            String avatar = Constants.USER_PROFILE_URL + userId + ".jpg?id=" + imageTS;
            StatusUtil.setProfileImage(holder.ivProfile,context,avatar,userId);
            //getcontactname.configProfilepic(holder.ivProfile, userId, false, false, R.mipmap.chat_attachment_profile_default_image_frame);
        } catch (Exception e) {
            Picasso.with(context).load(R.drawable.personprofile).into(holder.ivProfile);
        }

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!contact.isSelected()){
                    totalSelected++;
                }
                else {
                    totalSelected--;
                }
                if(onListClickListener!=null){
                    onListClickListener.onItemClick(position,totalSelected,false);
                }
                mDisplayedValues.get(position).setSelected(!contact.isSelected());
                notifyItemChanged(position);
            }
        });


        if (  mDisplayedValues.get(position).isSelected() ) {
            holder.ivSelectIcon.setVisibility(View.VISIBLE);
        } else {
            holder.ivSelectIcon.setVisibility(View.GONE);
        }

    }

    public void setSelectAll(boolean selectAllClicked) {
        totalSelected=0;
        if(mDisplayedValues!=null && mDisplayedValues.size()>0) {
            for (int i = 0; i < mDisplayedValues.size(); i++) {
                mDisplayedValues.get(i).setSelected(selectAllClicked);
                if(selectAllClicked){
                    totalSelected++;
                }
            }
            this.selectAllClicked = selectAllClicked;
            notifyDataSetChanged();
        }
    }

    public boolean getSelectAll() {
        return selectAllClicked;
    }

    @Override
    public int getItemCount() {
        if (mDisplayedValues == null)
            return 0;

        return mDisplayedValues.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<ScimboContactModel>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
//                    Toast.makeText(context, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<ScimboContactModel> FilteredArrList = new ArrayList<>();

                if (mData == null) {
                    mData = new ArrayList<>(mDisplayedValues); // saves the original data in mData
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mData.size();
                    results.values = mData;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mData.size(); i++) {


                        String contactName = mData.get(i).getFirstName();
                        String contactNo = mData.get(i).getNumberInDevice();

                        if (contactName.toLowerCase().contains(constraint) || contactNo.toLowerCase().contains(constraint)) {
                            FilteredArrList.add(mData.get(i));
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivProfile, ivSelectIcon;
        private TextView tvName;
        private View rootView;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            ivSelectIcon = itemView.findViewById(R.id.iv_selection_icon);
            ivProfile = itemView.findViewById(R.id.iv_profile_image);
            rootView = itemView.findViewById(R.id.root_view);

            switch (type) {
                case StatusPrivacyActivity.TYPE_ONLY_SHARE_WITH:
                    ivSelectIcon.setCircleBackgroundColor(ContextCompat.getColor(context, R.color.status_green));
                    break;

                case StatusPrivacyActivity.TYPE_SHARE_ALL_EXCEPT:
                    ivSelectIcon.setCircleBackgroundColor(ContextCompat.getColor(context, R.color.status_red));
                    break;
            }

        }
    }
}
