package com.chat.android.app.activity;

/**
 * Created by CAS63 on 3/21/2017.
 */

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.core.text.TextUtilsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.adapter.RItemAdapter;
import com.chat.android.app.adapter.SecretChatAdapter;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.AvnNextLTProDemiTextView;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.ScimboContactModel;
import com.chat.android.core.service.Constants;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class SelectSecretChatContact extends CoreActivity implements AdapterView.OnItemClickListener,
        LoaderManager.LoaderCallbacks<Object> {
    RecyclerView lvContacts;
    private EditText mSearchEt;
    SecretChatAdapter adapter;
    EditText etSearch;
    AvnNextLTProDemiTextView selectcontact;
    AvnNextLTProRegTextView selectcontactmember;
    ImageView serach, overflow, backarrow, backButton;
    InputMethodManager inputMethodManager;
    ProgressDialog dialog;
    BroadcastReceiver mRegistrationBroadcastReceiver;
    private SessionManager sessionManager;
    private ArrayList<MessageItemChat> mChatData = new ArrayList<>();
    AvnNextLTProRegTextView contact_empty;
    private static final String TAG = "SelectSecretChatContact";
    String username, profileimage;
    private SearchView searchView;
    String receiverDocumentID, uniqueCurrentID;
    public static final int CONTACTS_LOADER_ID = 1;
    Getcontactname getcontactname;

    private ArrayList<ScimboContactModel> scimboEntries;

    private LinearLayoutManager mContactListManager;
    private int mFirstVisibleItemPosition, mLastVisibleItemPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.secret_chat_contact_list);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.nowletschat.android.contact_refresh");
        registerReceiver(contactsRefreshReceiver, intentFilter);

        lvContacts = findViewById(R.id.listContacts);
        LinearLayoutManager mediaManager = new LinearLayoutManager(SelectSecretChatContact.this, LinearLayoutManager.VERTICAL, false);
        getcontactname = new Getcontactname(SelectSecretChatContact.this);
        lvContacts.setLayoutManager(mediaManager);
        backButton = findViewById(R.id.backarrow_contactsetting);
        backarrow = findViewById(R.id.backarrow);
        contact_empty = findViewById(R.id.contact_empty);
        overflow = findViewById(R.id.overflow);
        serach = findViewById(R.id.search);
        etSearch = findViewById(R.id.etSearch);

        selectcontact = findViewById(R.id.selectcontact);
        selectcontactmember = findViewById(R.id.selectcontactmember);

        initProgress(getString(R.string.loading_in), true);

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        sessionManager = SessionManager.getInstance(this);

        mContactListManager = new LinearLayoutManager(SelectSecretChatContact.this, LinearLayoutManager.VERTICAL, false);
        lvContacts.setLayoutManager(mContactListManager);
        lvContacts.addOnScrollListener(contactScrollListener);

        lvContacts.addOnItemTouchListener(new RItemAdapter(this, lvContacts, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(SelectSecretChatContact.this, SecretChatViewActivity.class);
                ScimboContactModel e = adapter.getItem(position);
                if (e != null) {
                    if (e.getStatus() != null && e.getStatus().length() > 0) {
                        String[] array = e.getStatus().split(",");
                        intent.putExtra("receiverUid", e.getNumberInDevice());
                        intent.putExtra("receiverName", e.getFirstName());
                        intent.putExtra("documentId", e.get_id());
                        intent.putExtra("Username", e.getFirstName());
                        intent.putExtra("Image", e.getAvatarImageUrl());
                        intent.putExtra("type", 0);
                        String msisdn = e.getNumberInDevice();
                        intent.putExtra("msisdn", msisdn);
                        startActivity(intent);
                        finish();

                    }

                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
/*        getSupportLoaderManager().initLoader(CONTACTS_LOADER_ID,
                null,  SelectSecretChatContact.this);*/
        overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView invite, Refresh, contact, Aboutandhelp;
                final RelativeLayout invitefriend, contacts, refresh, aboutandhelp;
                final Dialog dialogcontent = new Dialog(SelectSecretChatContact.this);
                dialogcontent.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogcontent.setContentView(R.layout.overflowcontactdialog);
                WindowManager.LayoutParams wmlp = dialogcontent.getWindow().getAttributes();

                if( TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == ViewCompat.LAYOUT_DIRECTION_LTR)
                {
                    wmlp.gravity = Gravity.TOP | Gravity.END;
                }
                else
                wmlp.gravity = Gravity.TOP | Gravity.START;
                wmlp.x = -5;   //x position
                wmlp.y = -5;   //y position
                invite = dialogcontent.findViewById(R.id.invitefriends);
                contact = dialogcontent.findViewById(R.id.contacts);
                Aboutandhelp = dialogcontent.findViewById(R.id.Aboutandhelp);
                Refresh = dialogcontent.findViewById(R.id.Refresh);
                invitefriend = dialogcontent.findViewById(R.id.R1_more);
                contacts = dialogcontent.findViewById(R.id.R2_more);
                refresh = dialogcontent.findViewById(R.id.R3_more);
                aboutandhelp = dialogcontent.findViewById(R.id.R4_more);
                dialogcontent.setCanceledOnTouchOutside(true);
                dialogcontent.show();
                Typeface face = CoreController.getInstance().getRobotoRegularTypeFace();
                invite.setTypeface(face);
                contact.setTypeface(face);
                Aboutandhelp.setTypeface(face);
                Refresh.setTypeface(face);
                invitefriend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        performInvite();
                        dialogcontent.dismiss();

                    }
                });
                contacts.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                        startActivityForResult(intent, 1);
                        dialogcontent.dismiss();

                    }
                });
                refresh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogcontent.dismiss();
                        showProgressDialog();
                        ScimboContactsService.startContactService(SelectSecretChatContact.this, true);
                    }
                });
                aboutandhelp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityLauncher.launchAbouthelp(SelectSecretChatContact.this);
                        dialogcontent.dismiss();
                    }
                });
            }
        });

        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP && etSearch.getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                    if (event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etSearch.setText("");
                        return false;
                    }
                }
                return false;
            }
        });

        serach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchActions();
                //showSoftKeyboard(v);

                etSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                        if (cs.length() > 0) {
                            if (cs.length() == 1) {
                                etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.cancel_normal, 0);
                            }
                            try {
                                adapter.getFilter().filter(cs);
                            }
                            catch (Exception e){
                                MyLog.e(TAG, "onTextChanged: ",e );
                            }
                        } else {
                            etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            adapter.updateInfo(scimboEntries);
                        }

                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                        // TODO Auto-generated method stub
                    }
                });
                backarrow.setVisibility(View.VISIBLE);
                backButton.setVisibility(View.GONE);

                backarrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        etSearch.setVisibility(View.GONE);
                        serach.setVisibility(View.VISIBLE);
                        overflow.setVisibility(View.VISIBLE);
                        selectcontactmember.setVisibility(View.VISIBLE);
                        selectcontact.setVisibility(View.VISIBLE);
                        backarrow.setVisibility(View.GONE);
                        backButton.setVisibility(View.VISIBLE);
                    }
                });

                showKeyboard();
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loadContactsFromDB();
        /* Variables for serch */

    }

    private void loadContactsFromDB() {
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        scimboEntries = contactDB_sqlite.getSavedScimboContacts();
        setAdapter();
    }

    private void showSearchActions() {
        backarrow.setVisibility(View.VISIBLE);
        serach.setVisibility(View.GONE);
        backButton.setVisibility(View.GONE);
        selectcontact.setVisibility(View.GONE);
        selectcontactmember.setVisibility(View.GONE);
        overflow.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();
    }

    private void setAdapter() {
        if (scimboEntries != null) {
            selectcontactmember.setText(scimboEntries.size() + " "+getString(R.string.Contacts));
        }

        if (scimboEntries != null && scimboEntries.size() > 0) {
            Collections.sort(scimboEntries, Getcontactname.nameAscComparator);
            adapter = new SecretChatAdapter(SelectSecretChatContact.this, scimboEntries);
            lvContacts.setVisibility(View.VISIBLE);
            contact_empty.setVisibility(View.GONE);
            lvContacts.setAdapter(adapter);
        } else {
            contact_empty.setVisibility(View.VISIBLE);
            lvContacts.setVisibility(View.GONE);
            contact_empty.setText("No contacts available for chat");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
         /* Move to message activity and start a chat with this user */

        Intent intent = new Intent(SelectSecretChatContact.this, SecretChatViewActivity.class);
        ScimboContactModel e = adapter.getItem(position);
        if (e != null) {
            if (e.getStatus() != null && e.getStatus().length() > 0) {
                String[] array = e.getStatus().split(",");
                intent.putExtra("receiverUid", e.getNumberInDevice());
                intent.putExtra("receiverName", e.getFirstName());
                intent.putExtra("documentId", e.get_id());
                intent.putExtra("Username", e.getFirstName());
                intent.putExtra("Image", e.getAvatarImageUrl());
                intent.putExtra("type", 0);
                String msisdn = e.getNumberInDevice();
                intent.putExtra("msisdn", msisdn);
                startActivity(intent);
                finish();
                this.overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }

        }
    }


    private void performInvite() {

        Intent emailIntent = new Intent();
        emailIntent.setAction(Intent.ACTION_SEND);
        // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
        // emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_native)));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name) + ":Android");
        emailIntent.putExtra(Intent.EXTRA_TEXT, Constants.getAppStoreLink(SelectSecretChatContact.this));
        emailIntent.putExtra(Intent.EXTRA_TEXT, Constants.getAppStoreLink(SelectSecretChatContact.this));
        emailIntent.setType("message/rfc822");

        PackageManager pm = getApplication().getPackageManager();
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");


        Intent openInChooser = Intent.createChooser(emailIntent, "E-mail");

        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
        for (int i = 0; i < resInfo.size(); i++) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if (packageName.contains("android.email")) {
                emailIntent.setPackage(packageName);
            } else if (packageName.contains("twitter") || packageName.contains("facebook") || packageName.contains("mms") || packageName.contains("android.gm")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                if (packageName.contains("twitter")) {
                    //  intent.putExtra(Intent.EXTRA_TEXT, "share_twitter");
                } else if (packageName.contains("facebook")) {
                    // Warning: Facebook IGNORES our text. They say "These fields are intended for users to express themselves. Pre-filling these fields erodes the authenticity of the user voice."
                    // One workaround is to use the Facebook SDK to post, but that doesn't allow the user to choose how they want to share. We can also make a custom landing page, and the link
                    // will show the <meta content ="..."> text from that page with our link in Facebook.
                    // intent.putExtra(Intent.EXTRA_TEXT, rshare_facebook);
                } else if (packageName.contains("mms")) {
                    Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    // smsIntent.putExtra("address", labelText);
                    smsIntent.putExtra("sms_body", Constants.getAppStoreLink(SelectSecretChatContact.this));
                    startActivity(smsIntent);
                } else if (packageName.contains("android.gm")) { // If Gmail shows up twice, try removing this else-if clause and the reference to "android.gm" above
                    // intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_gmail)));
                    intent.putExtra(Intent.EXTRA_TEXT, Constants.getAppStoreLink(SelectSecretChatContact.this));
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                    intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name) + ":Android");
                    intent.setType("message/rfc822");
                }

                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }

        // convert intentList to array
        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        startActivity(openInChooser);
    }


   /* @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.setting_contacts, menu);
        MenuItem searchItem = menu.findItem(R.id.chats_searchIcon);
        MenuItem overflowicon = menu.findItem(R.id.overflowmenu);
        searchItem.setVisible(false);
        if (ScimboContacts.scimboEntries.size() > 0) {
            searchItem.setVisible(true);
            searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    searchView.setIconifiedByDefault(true);
                    searchView.setIconified(true);
                    searchView.setQuery("", false);
                    searchView.clearFocus();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (newText.equals("") && newText.isEmpty()) {
                        searchView.clearFocus();
                    }
                    adapter.getFilter().filter(newText);
                    return false;
                }
            });

            searchView.setIconifiedByDefault(true);
            searchView.setQuery("", false);
            searchView.clearFocus();
            searchView.setIconified(true);

            AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            try {
                Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                mCursorDrawableRes.setAccessible(true);
                mCursorDrawableRes.set(searchTextView, 0); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
            } catch (Exception e) {
            }
            overflowicon.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    final TextView invite, Refresh, contact, Aboutandhelp;
                    final RelativeLayout invitefriend, contacts, refresh, aboutandhelp;
                    final Dialog dialogcontent = new Dialog(SettingContact.this);
                    dialogcontent.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogcontent.setContentView(R.layout.overflowcontactdialog);
                    WindowManager.LayoutParams wmlp = dialogcontent.getWindow().getAttributes();

                    wmlp.gravity = Gravity.TOP | Gravity.RIGHT;
                    wmlp.x = -5;   //x position
                    wmlp.y = -5;   //y position
                    invite = (TextView) dialogcontent.findViewById(R.id.invitefriends);
                    contact = (TextView) dialogcontent.findViewById(R.id.contacts);
                    Aboutandhelp = (TextView) dialogcontent.findViewById(R.id.Aboutandhelp);
                    Refresh = (TextView) dialogcontent.findViewById(R.id.Refresh);
                    invitefriend = (RelativeLayout) dialogcontent.findViewById(R.id.R1_more);
                    contacts = (RelativeLayout) dialogcontent.findViewById(R.id.R2_more);
                    refresh = (RelativeLayout) dialogcontent.findViewById(R.id.R3_more);
                    aboutandhelp = (RelativeLayout) dialogcontent.findViewById(R.id.R4_more);
                    dialogcontent.setCanceledOnTouchOutside(true);
                    dialogcontent.show();
                    invite.setTypeface(face);
                    contact.setTypeface(face);
                    Aboutandhelp.setTypeface(face);
                    Refresh.setTypeface(face);
                    invitefriend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            performInvite();
                            dialogcontent.dismiss();

                        }
                    });
                    contacts.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                            startActivityForResult(intent, 1);
                            dialogcontent.dismiss();

                        }
                    });
                    refresh.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSupportLoaderManager().initLoader(CONTACTS_LOADER_ID,
                                    null, SettingContact.this);
                            dialogcontent.dismiss();
                        }
                    });
                    aboutandhelp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ActivityLauncher.launchAbouthelp(SettingContact.this);
                            dialogcontent.dismiss();
                        }
                    });
                    return false;
                }
            });

        }

        return true;
    }*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_USER_DETAILS)) {
            loadUserDetails(event.getObjectsArray()[0].toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {

    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideKeyboard();
        unregisterReceiver(contactsRefreshReceiver);
    }

    ContactsRefreshReceiver contactsRefreshReceiver = new ContactsRefreshReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            hideProgressDialog();
            loadContactsFromDB();
        }
    };

    @Override
    public void onBackPressed() {
        if (etSearch.getVisibility() == View.GONE) {
            hideKeyboard();
            super.onBackPressed();
        } else {
            backarrow.performClick();
        }
    }

    private RecyclerView.OnScrollListener contactScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            notifyScrollChanged();
        }
    };

    private void notifyScrollChanged() {
        mFirstVisibleItemPosition = mContactListManager.findFirstVisibleItemPosition();
        mLastVisibleItemPosition = mContactListManager.findLastVisibleItemPosition();
        int totalItems = scimboEntries.size();

/*        try {
            if (totalItems > mFirstVisibleItemPosition && totalItems > mLastVisibleItemPosition) {
                for (int i = mFirstVisibleItemPosition; i <= mLastVisibleItemPosition; i++) {
                    String userId = scimboEntries.get(i).get_id();
                //    MessageService.getUpdatedUserDetails(EventBus.getDefault(), userId);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.e(TAG,"",e);
        }*/
    }

    private void loadUserDetails(String data) {
        try {
            JSONObject object = new JSONObject(data);
            String userId = object.getString("id");

            try {
                for (int i = mFirstVisibleItemPosition; i <= mLastVisibleItemPosition; i++) {
                    if (scimboEntries.get(i).get_id().equalsIgnoreCase(userId)) {
                        adapter.notifyItemChanged(i);
                    }
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                MyLog.e(TAG,"",e);
            }

        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }
    }
}
