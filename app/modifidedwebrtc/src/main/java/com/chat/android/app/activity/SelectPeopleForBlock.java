package com.chat.android.app.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.adapter.BlockListAdapter;
import com.chat.android.app.adapter.RItemAdapter;
import com.chat.android.app.utils.BlockUserUtils;
import com.chat.android.app.utils.ConnectivityInfo;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.ScimboContactModel;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by CAS63 on 3/7/2017.
 */
public class SelectPeopleForBlock extends CoreActivity {
    private static final String TAG = "SelectPeopleForBlock";

    private BlockListAdapter adapter;
    private List<ScimboContactModel> scimboEntries = new ArrayList<>();
    private List<String> filteredList = new ArrayList<>();
    private ArrayList<ScimboContactModel> myFinalList = new ArrayList<>();
    private ArrayList<ScimboContactModel> selectedItem = new ArrayList<>();
    String id, from;
    RecyclerView recyclerView;
    Session session;
    SearchView searchView;

    ScimboContactModel item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_people_block);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Select Contact");
        actionBar.setDisplayHomeAsUpEnabled(true);
        session = new Session(SelectPeopleForBlock.this);

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        scimboEntries = contactDB_sqlite.getSavedScimboContacts();
        from = SessionManager.getInstance(this).getCurrentUserID();

        ArrayList<ScimboContactModel> contactList = contactDB_sqlite.getAllScimboContacts();

        for (int i = 0; i < contactList.size(); i++) {
            String toUserId = contactList.get(i).get_id();
            if (contactDB_sqlite.getBlockedStatus(toUserId, false).equalsIgnoreCase("0")) {
                filteredList.add(contactList.get(i).get_id());
            }
        }

        if (filteredList.size() != 0) {
            for (int i = 0; i < filteredList.size(); i++) {
                ScimboContactModel scimboContactModel = new ScimboContactModel();

                for (int j = 0; j < scimboEntries.size(); j++) {
                    if (scimboEntries.get(j).get_id().equalsIgnoreCase(filteredList.get(i))) {

                        scimboContactModel = scimboEntries.get(j);

                        myFinalList.add(scimboContactModel);
                    }

                }

            }
        } else {
            myFinalList.addAll(scimboEntries);
        }
        Collections.sort(myFinalList, Getcontactname.nameAscComparator);
        adapter = new BlockListAdapter(SelectPeopleForBlock.this, myFinalList);
        recyclerView = findViewById(R.id.listToBlock);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SelectPeopleForBlock.this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        initProgress(getString(R.string.loading_in), false);

        recyclerView.addOnItemTouchListener(new RItemAdapter(this, recyclerView, new RItemAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                if (ConnectivityInfo.isInternetConnected(SelectPeopleForBlock.this)) {
                    showProgressDialog();

                    item = adapter.getItem(position);
                    selectedItem.add(item);
                    id = item.get_id();

                    BlockUserUtils.changeUserBlockedStatus(SelectPeopleForBlock.this, EventBus.getDefault(),
                            from, id, false);

                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(SelectPeopleForBlock.this, "Check your network connection", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {


            }


        }));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_BLOCK_USER)) {
            block(event);
        }
    }

    public void block(ReceviceMessageEvent event) {
        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());
            MyLog.e("Response---", object.toString());
            String stat = object.getString("status");
            String toid = object.getString("to");
            String fromUserId = object.getString("from");
            if (from.equalsIgnoreCase(fromUserId)) {
                hideProgressDialog();

                loadBlockContactList(object);
            }

        } catch (Exception e) {
            MyLog.e(TAG,"",e);
        }


    }


    private void loadBlockContactList(JSONObject object) {
        try {
            String err = object.getString("err");
            if (err.equalsIgnoreCase("0")) {
                Intent okIntent = new Intent();
                okIntent.putExtra("BlockAdded", true);
                // okIntent.putExtra("mySelectedData",item);
                setResult(RESULT_OK, okIntent);
                finish();
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.select_people_for_group, menu);
        MenuItem searchItem = menu.findItem(R.id.menuSearch);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        if (myFinalList.size() > 0) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    searchView.setIconifiedByDefault(true);
                    searchView.setIconified(true);
                    searchView.setQuery("", false);
                    searchView.clearFocus();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {

                    if (newText.equals("") && newText.isEmpty()) {
                        searchView.clearFocus();
                        //closeKeypad();
                    }
                    if (newText.length() > 0) {
                        adapter.getFilter().filter(newText);
                    } else {
                        adapter.updateInfo(myFinalList);
                    }


                    return false;
                }
            });

            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    menu.findItem(R.id.menuSearch).setVisible(true);
                    return false;
                }
            });

            searchView.setOnSearchClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menu.findItem(R.id.menuSearch).setVisible(false);
                }
            });


            searchView.setIconifiedByDefault(true);
            searchView.setQuery("", false);
            searchView.clearFocus();
            searchView.setIconified(true);

            AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
            searchTextView.setTextColor(Color.WHITE);
            try {
                Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                mCursorDrawableRes.setAccessible(true);
                mCursorDrawableRes.set(searchTextView, 0);
            } catch (Exception e) {
                MyLog.e(TAG,"",e);
            }
        }
        MenuItemCompat.setActionView(searchItem, searchView);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case android.R.id.home:
                finish();
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

}
