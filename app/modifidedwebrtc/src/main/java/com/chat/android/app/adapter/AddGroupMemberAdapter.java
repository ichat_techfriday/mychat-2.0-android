package com.chat.android.app.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.CoreController;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.ScimboContactModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CAS60 on 11/30/2016.
 */
public class AddGroupMemberAdapter extends RecyclerView.Adapter<AddGroupMemberAdapter.ViewHolderAddGroupMember> implements Filterable {

    private List<ScimboContactModel> contactList;
    Getcontactname getcontactname;
    private int blockedContactColor, unblockedContactColor;
    ContactDB_Sqlite contactDB_sqlite;
    private List<ScimboContactModel> mDisplayedValues;

    public AddGroupMemberAdapter(Context mContext, List<ScimboContactModel> contactList) {
        this.contactList = contactList;
        getcontactname = new Getcontactname(mContext);
        mDisplayedValues=contactList;

        blockedContactColor = ContextCompat.getColor(mContext, R.color.blocked_user_bg);
        unblockedContactColor = ContextCompat.getColor(mContext, R.color.white);

        contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
    }

    @Override
    public ViewHolderAddGroupMember onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_group_info, parent, false);
        ViewHolderAddGroupMember holder = new ViewHolderAddGroupMember(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolderAddGroupMember holder, int position) {

        ScimboContactModel contact = mDisplayedValues.get(position);

        holder.tvName.setText(contact.getFirstName());
//        holder.tvStatus.setText(contact.getStatus());
        getcontactname.setProfileStatusText(holder.tvStatus, contact.get_id(), contact.getStatus(), false);

        String toID = mDisplayedValues.get(position).get_id();
        getcontactname.configProfilepic(holder.ivUserDp, toID, false, false, R.mipmap.chat_attachment_profile_default_image_frame);

        if (contactDB_sqlite.getBlockedStatus(toID, false).equals("1")) {
            holder.itemView.setBackgroundColor(blockedContactColor);
        } else {
            holder.itemView.setBackgroundColor(unblockedContactColor);
        }
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }
    public ScimboContactModel getItem(int position) {
        return mDisplayedValues.get(position);
    }


    public class ViewHolderAddGroupMember extends RecyclerView.ViewHolder {

        CircleImageView ivUserDp;
        TextView tvName, tvAdmin, tvStatus;

        public ViewHolderAddGroupMember(View itemView) {
            super(itemView);

            ivUserDp =  itemView.findViewById(R.id.ivUserDp);
            tvName = itemView.findViewById(R.id.tvName);
            tvAdmin = itemView.findViewById(R.id.tvAdmin);
            tvStatus = itemView.findViewById(R.id.tvStatus);
        }
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<ScimboContactModel>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
//                    Toast.makeText(context, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<ScimboContactModel> FilteredArrList = new ArrayList<>();

                if (contactList == null) {
                    contactList = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mDisplayedValues.size();
                    results.values = mDisplayedValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < contactList.size(); i++) {

                        String name = contactList.get(i).getFirstName();
                        String msisdn = contactList.get(i).getMsisdn();
                        if (name.toLowerCase().contains(constraint) || msisdn.contains(constraint)) {
                            FilteredArrList.add(contactList.get(i));
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }


    public void updateInfo(List<ScimboContactModel> aitem) {
        this.contactList = aitem;
        notifyDataSetChanged();
    }

}
