package com.chat.android.app.widget;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatImageView;

public class TopCropImageView extends AppCompatImageView {
    private static boolean topCropEnabled=false;
    public TopCropImageView(Context context) {
        super(context);
        init();
    }

    public TopCropImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TopCropImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        recomputeImgMatrix();
    }

    @Override
    protected boolean setFrame(int l, int t, int r, int b) {
        recomputeImgMatrix();
        return super.setFrame(l, t, r, b);
    }

    private void init() {
        setScaleType(ScaleType.MATRIX);
    }

    private void recomputeImgMatrix() {
        //currently disabled
        if (topCropEnabled) {
            final Drawable drawable = getDrawable();
            if (drawable == null) {
                return;
            }

            final Matrix matrix = getImageMatrix();

            float scale = 1.0f;
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            float screenWidth = displaymetrics.widthPixels;
            final int drawableWidth = drawable.getIntrinsicWidth();

            scale = screenWidth / drawableWidth;

            matrix.setScale(scale, scale);
            setImageMatrix(matrix);
        }
        else {
            setScaleType(ScaleType.CENTER_CROP);
        }
    }
}