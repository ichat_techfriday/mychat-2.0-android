package com.chat.android.app.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;

import android.content.BroadcastReceiver;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.account.ContactsSyncAdapterService;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.ShortcutBadgeManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.ContactsPojo;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.ScimboContactModel;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.scimbohelperclass.ScimboRegularExp;
import com.chat.android.core.socket.MessageService;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class ScimboContactsService {

    private SessionManager sessionManager;

    public static List<ScimboContactModel> contactEntries = new ArrayList<>();
    private List<ScimboContactModel> contacts;
    private List<ScimboContactModel> scimboEntries;
    private List<ScimboContactModel> othercontacts;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    public static String contact = new String();
    public static long contactLoadedAt = 0;
    private static boolean isNeedToSendServerNow = false;
    private Session session;
    private String uniqueCurrentID;
    private String currentUserno;

    private UserInfoSession userInfoSession;
    private Handler eventHandler;
    private Runnable eventRunnable;

    public static boolean isStarted = false;

    public static String savedNumber = "";
    public static String savedName = "";
    private static BroadCastSavedName broadCastSavedName;
    public static final String KEY_FROM_CONTACT_SYNC_SVC = "FromContactSyncSvc";
    public static final String KEY_REFRESH_COMPLETED = "RefreshCompleted";
    ShortcutBadgeManager contactDBtime;
    private static final String TAG = "ScimContactsService" + ">>>";


    public void init(Context context) {
        isStarted = true;

        contactDBtime = new ShortcutBadgeManager(context);
        contacts = new ArrayList<>();
        scimboEntries = new ArrayList<>();
        othercontacts = new ArrayList<>();

        userInfoSession = new UserInfoSession(context);

        sessionManager = SessionManager.getInstance(context);
        session = new Session(context);

        currentUserno = sessionManager.getPhoneNumberOfCurrentUser();
        uniqueCurrentID = sessionManager.getCurrentUserID();

    }

    public void startContactSync(boolean fromContactSync, final Context context) {


        long timeDiff = Calendar.getInstance().getTimeInMillis() - contactLoadedAt;
        long minRefreshDiff = 3 * 60 * 1000; // 3 minutes

        if (savedNumber != null && savedNumber.length() > 0) {
            new ReadContactsTask(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else if (contact == null || contact.equals("") || (timeDiff > minRefreshDiff) || fromContactSync) {
            new ReadContactsTask(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateDataToTheServer(context);
                }
            }, 100);
        }
    }


    public void onMessageEvent(final ContactRefresh event, Context context) {
        isNeedToSendServerNow = true;
        startContactSync(true, context);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final ReceviceMessageEvent event, final Context context) {
        MyLog.d(TAG, event.getEventName());

        if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    storeContact(event, context);
                }
            }).start();

        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_PRIVACY_SETTINGS)) {
            loadPrivacySetting(event);
        }
    }


    public void storeContact(ReceviceMessageEvent event, Context context) {
        try {
            Object[] args = event.getObjectsArray();
            JSONObject data = new JSONObject(args[0].toString());
            MyLog.e("storeContact", "storeContact" + data);
            //Check Favorites is not empty array

            JSONArray dataa = data.getJSONArray("Favorites");

            if (dataa.length() == 0) {
                Toast.makeText(context,"No Contacts Found!!",Toast.LENGTH_SHORT).show();
            }

            setAdapter(data.toString(), context);

            if (!sessionManager.isContactSyncFinished()) {
                sessionManager.setContactSyncFinished();
            }
        } catch (Exception e) {
            Log.e(TAG, "storeContact: ", e);
        }
    }


    private synchronized void setAdapter(String values, final Context context) {
        MyLog.d(TAG, "setAdapter() ");
        final ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        final long savedRevision = sessionManager.getContactSavedRevision() + 1;
        //for manual refresh --> update the old revision to new revision
       /* if (savedRevision >1) {
            MyLog.d(TAG, "setAdapter: revision now updated");
            CoreController.getContactSqliteDBintstance(this).updateRevision_when_contatct_delete(savedRevision, savedRevision - 1);
        }*/
        try {
            session.setFavContacts(values);
            contacts.clear();
            scimboEntries.clear();
            othercontacts.clear();
            JSONObject data = new JSONObject(values);

            JSONArray array = data.getJSONArray("Favorites");


            // boolean contactUpdated = false;
            for (int i = 0; i < array.length(); i++) {
                final JSONObject obj = new JSONObject(array.get(i).toString());
                new Thread() {
                    @Override
                    public void run() {
                        saveSingleContact(obj, context, contactDB_sqlite, savedRevision);
                    }
                }.start();
            }

            new Thread() {
                @Override
                public void run() {
                    try {
                        //warning --> dont remove Thread.sleep()
                        //reason - above for loop run on separate thread -> so we need to give delay for save that contact in DB. else it will not work
                        Thread.sleep(2000);

                        filterContact(savedRevision, context);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    long accountStarted = sessionManager.getAccountSyncStartTS();
                    long accountCompleted = sessionManager.getAccountSyncCompletedTS();
                    long currentTime = Calendar.getInstance().getTimeInMillis();
                    long startedDiff = currentTime - accountStarted;
                    long completedDiff = currentTime - accountCompleted;
                    if (startedDiff > ContactsSyncAdapterService.MIN_TIME_ACCOUNT_CREATE &&
                            completedDiff > ContactsSyncAdapterService.MIN_TIME_ACCOUNT_CREATE) {
                        createScimboAccount(context);
                    }
                }
            }).start();

            MyLog.d(TAG, "setAdapter: end");

        } catch (Exception e) {
            MyLog.e(TAG, "", e);

        }
    }

    private void filterContact(long savedRevision, Context context) {
        sessionManager.setContactSavedRevision(savedRevision);
        MyLog.d(TAG, String.valueOf(savedRevision));


        for (int i = 0; i < contactEntries.size(); i++) {
            if (contactEntries.get(i).get_id() == null) {
                othercontacts.add(contactEntries.get(i));
            }
        }

        for (int i = 0; i < scimboEntries.size(); i++) {
            String a1 = scimboEntries.get(i).get_id();
            for (int j = i + 1; j < scimboEntries.size(); j++) {
                String a2 = scimboEntries.get(j).get_id();
                if (a1 != null && a2 != null && a1.equalsIgnoreCase(a2)) {
                    scimboEntries.remove(scimboEntries.get(j));
                }
            }

        }
        for (int i = 0; i < othercontacts.size(); i++) {
            String a1 = null;
            try {
                if (othercontacts != null && othercontacts.get(i) != null)
                    a1 = othercontacts.get(i).getNumberInDevice();
            } catch (Exception e) {
                Log.e(TAG, "setAdapter: ", e);
            }
            for (int j = i + 1; j < othercontacts.size(); j++) {
                //  if(othercontacts.get(j)!=null ) {
                String a2 = null;
                try {
                    if (othercontacts != null && othercontacts.get(j) != null)
                      if (othercontacts.get(j).getNumberInDevice()!=null){
                          a2 = othercontacts.get(j).getNumberInDevice();
                      }
                } catch (Exception e) {
                    Log.e(TAG, "setAdapter: ", e);
                }
                if (a1 != null && a2 != null && a1.equalsIgnoreCase(a2)) {
                    othercontacts.remove(othercontacts.get(j));
                }
                //   }
            }
        }

        for (int i = 0; i < othercontacts.size(); i++) {
            String mCurrentUserMsisdn = sessionManager.getPhoneNumberOfCurrentUser();
            try {
                if (othercontacts.get(i).getNumberInDevice().startsWith(sessionManager.getCountryCodeOfCurrentUser())) {
                    if (othercontacts.get(i).getNumberInDevice().equalsIgnoreCase(mCurrentUserMsisdn)) {
                        othercontacts.remove(i);
                    }
                } else {
                    String myStrPhno = mCurrentUserMsisdn.replace(sessionManager.getCountryCodeOfCurrentUser(), "");
                    if (othercontacts.get(i).getNumberInDevice().equalsIgnoreCase(myStrPhno)) {
                        othercontacts.remove(i);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "filterContact: ", e);
            }

        }

        contacts.addAll(scimboEntries);
        contacts.addAll(othercontacts);

        sendContactBroadcast(true, context);
    }

    private void saveSingleContact(JSONObject obj, Context context, ContactDB_Sqlite contactDB_sqlite, long savedRevision) {
        try {
            Log.d(TAG, "saveSingleContact: ");
            String securityCode = null;
            if (AppUtils.isEncryptionEnabled(context)) {
                securityCode = obj.getString("security_code");
            }
            String msisdn = obj.getString("msisdn");
            String id = obj.getString("_id");
            String countryCode = obj.getString("CountryCode");
            String profilePic = obj.getString("ProfilePic");
            String userStatus = obj.getString("Status");
            String locPhNo = obj.getString("PhNumber");
            JSONObject privacy = obj.getJSONObject("privacy");
            String last_seen = privacy.getString("last_seen");
            String profile = privacy.getString("profile_photo");
            String status = privacy.getString("status");
            String profile_photo_status = privacy.getString("profile_photo_status");

            String local_phNo = obj.getString("contactPhno");

            //  String last_seen_status = privacy.getString("last_seen_status");
            //   String profile_status = privacy.getString("profile_status");

            String mineContactStatus = "0";
            if (privacy.has("contactUserList")) {
                String toContactsList = privacy.getString("contactUserList");
                if (toContactsList.contains(uniqueCurrentID)) {
                    mineContactStatus = "1";
                }
            }

            String docId = uniqueCurrentID + "-" + id;

            String normalConvId = userInfoSession.getChatConvId(docId);
            if (normalConvId == null || normalConvId.equals("")) {
                getConvId(id, "no");
            }

            docId = docId + "-secret";
            String secretConvId = userInfoSession.getChatConvId(docId);
            if (secretConvId == null || secretConvId.equals("")) {
                getConvId(id, "yes");
            }

            for (int contactIndex = 0; contactIndex < contactEntries.size(); contactIndex++) {

                if (locPhNo.equalsIgnoreCase(contactEntries.get(contactIndex).getNumberInDevice())
                        || msisdn.equalsIgnoreCase(contactEntries.get(contactIndex).getNumberInDevice())
                        || msisdn.equalsIgnoreCase("+" + contactEntries.get(contactIndex).getNumberInDevice())
                        || contactEntries.get(contactIndex).getNumberInDevice().contains(local_phNo)
                        ) {


                    try {
                        if (ScimboRegularExp.isEncodedBase64String(userStatus)) {
                            userStatus = new String(Base64.decode(userStatus, Base64.DEFAULT), StandardCharsets.UTF_8);
                        }
                    } catch (Exception e) {
                        MyLog.e(TAG, "setAdapter: ", e);
                    }

                    contactEntries.get(contactIndex).set_id(id);
                    contactEntries.get(contactIndex).setStatus(userStatus);
                    contactEntries.get(contactIndex).setCountryCode(countryCode);
                    contactEntries.get(contactIndex).setMsisdn(msisdn);
                    contactEntries.get(contactIndex).setAvatarImageUrl(profilePic);

                    //MyLog.d(TAG, "setAdapter: contact position start>:");
                    //new db sqlite
                    contactDB_sqlite.updateMyContactStatus(id, mineContactStatus);
                    contactDB_sqlite.updateLastSeenVisibility(id, getPrivacyStatus(last_seen));
                    contactDB_sqlite.updateProfilePicVisibility(id, getPrivacyStatus(profile));
                    contactDB_sqlite.updateProfileStatusVisibility(id, getPrivacyStatus(status));
                    if (AppUtils.isEncryptionEnabled(context)) {
                        contactDB_sqlite.updateSecurityToken_(id, securityCode);
                    }
                    //MyLog.d(TAG, "setAdapter: contact position end");

                    ScimboContactModel entry = contactEntries.get(contactIndex);
                    if (!id.equalsIgnoreCase(uniqueCurrentID)) {
                        scimboEntries.add(entry);


                        //new db sqlite
                        contactDB_sqlite.updateUserDetails(id, entry);
                        contactDB_sqlite.updateSavedRevision(id, savedRevision);

                        MyLog.d("Scimbo_contacts", entry.getFirstName() + "    ---" + id);
                    }


                    if (!profile.equalsIgnoreCase("nobody")) {
                        if (!profile_photo_status.equalsIgnoreCase("0")) {
                            contactEntries.get(contactIndex).setAvatarImageUrl(profilePic);
                        }
                    }
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "saveSingleContact: ", e);
        }
    }

    private void sendContactBroadcast(boolean isRefreshed, Context context) {
        Intent broadcastIntent = new Intent("com.nowletschat.android.contact_refresh");
        broadcastIntent.putExtra(KEY_REFRESH_COMPLETED, isRefreshed);
        context.sendBroadcast(broadcastIntent);
    }

    private void createScimboAccount(final Context context) {

        try {
            sessionManager.setAccountSyncStartTS(Calendar.getInstance().getTimeInMillis());

            Account account = new Account(context.getString(R.string.app_name), context.getString(R.string.account_type));
            AccountManager removeManager = AccountManager.get(context);
            removeManager.removeAccount(account, new AccountManagerCallback<Boolean>() {
                @Override
                public void run(AccountManagerFuture<Boolean> accountManagerFuture) {
                    Account account = new Account(context.getString(R.string.app_name), context.getString(R.string.account_type));
                    AccountManager addManager = AccountManager.get(context);
                    try {

                        if (addManager.addAccountExplicitly(account, null, null)) {
                            ContentResolver.setSyncAutomatically(account, ContactsContract.AUTHORITY, true);
                            ContentResolver.setMasterSyncAutomatically(true);
                        }
                    } catch (SecurityException e) {

                    }
                }
            }, null);

        } catch (Exception e) {

            //System.out.println("Account Error"+e);
        }

    }

    private String getPrivacyStatus(String visibleTo) {
        switch (visibleTo.toLowerCase()) {

            case ContactDB_Sqlite.PRIVACY_TO_MY_CONTACTS:
                return ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS;

            case ContactDB_Sqlite.PRIVACY_TO_NOBODY:
                return ContactDB_Sqlite.PRIVACY_STATUS_NOBODY;

            default:
                return ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE;
        }
    }

    private void getConvId(String receiverId, String secretType) {

        try {
            JSONObject obj = new JSONObject();
            obj.put("from", uniqueCurrentID);
            obj.put("to", receiverId);
            obj.put("secret_type", secretType);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_CONV_ID);
            event.setMessageObject(obj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }


    public void loadPrivacySetting(ReceviceMessageEvent event) {
        try {
            Object[] objects = event.getObjectsArray();
            JSONObject object = new JSONObject(objects[0].toString());
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }


    }


    public static void setBroadCastSavedName(BroadCastSavedName broadCastSavedNme) {
        broadCastSavedName = broadCastSavedNme;
    }

    public class ReadContactsTask extends AsyncTask<Void, Integer, Long> {
        private Context context;

        public ReadContactsTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            MyLog.d(TAG, "onPreExecute: ");
        }

        protected Long doInBackground(Void... urls) {

            try {
                readContacts(context);
            } catch (Exception e) {
                MyLog.e(TAG, "doInBackground: ", e);
            }
            return 0L;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {

            Calendar c = Calendar.getInstance();


            updateDataToTheServer(context);
        }

    }

    private void updateDataToTheServer(Context context) {
        MyLog.d(TAG, "updateDataToTheServer: ");
        if (SocketManager.isConnected) {
            SendMessageEvent messageEvent = new SendMessageEvent();
            messageEvent.setEventName(SocketManager.EVENT_GET_FAVORITE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("indexAt", "0");
                jsonObject.put("msisdn", sessionManager.getPhoneNumberOfCurrentUser());
                jsonObject.put("Contacts", contact);
            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }

            messageEvent.setMessageObject(jsonObject);
            EventBus.getDefault().post(messageEvent);
            setEventTimeout(context);
        }
    }

    public int getNumber(String data) {
        try {
            if (data != null) {
                return Integer.parseInt(data);
            }
        } catch (NumberFormatException e) {
            MyLog.e(TAG, "", e);
        }
        return 0;
    }

    private void readContacts(Context context) throws RemoteException {
        MyLog.d(TAG, "^^^ readContacts: start >>>");
        final String SELECTION = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        final String[] PROJECTION = new String[]{
                ContactsContract.CommonDataKinds.Phone._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.TYPE
        };


        ContentResolver cr = context.getContentResolver();
        ContentProviderClient mCProviderClient = cr.acquireContentProviderClient(ContactsContract.Contacts.CONTENT_URI);
        Cursor cur;
        boolean firstTimeSyncCompleted = contactDBtime.getfirstTimecontactSyncCompleted();

        MyLog.d(TAG, "readContacts: first sync completed: " + firstTimeSyncCompleted);

        cur = mCProviderClient.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION,
                SELECTION, null, null);


        JSONArray arrContacts = new JSONArray();
        HashMap<String, ContactsPojo> Contactdata = new HashMap<String, ContactsPojo>();
        String phone = null;
        String phType = "";
        String name = "";
        String phNo = "";
        ContactsPojo contactsPojo = null;
        if (cur != null && cur.getCount() > 0) {
            MyLog.d(TAG, "readContacts: total count: " + cur.getCount());
            final int nameIndex = cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            final int numberIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            final int phoneTypeIndex = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
            while (cur.moveToNext()) {
                name = cur
                        .getString(nameIndex);

                try {
                    phone = cur.getString(numberIndex);
                } catch (Exception e) {
                    if (savedNumber != null && !savedNumber.isEmpty()) {
                        phone = savedNumber;
                    }
                }
                try {
                    phType = cur.getString(phoneTypeIndex);
                } catch (Exception e) {
                    phType = "Mobile";
                    MyLog.e(TAG, "", e);
                }
                String type = "";


                switch (getNumber(phType)) {
                    case 1:
                        type = "Home";
                        break;
                    case 2:
                        type = "Mobile";
                        break;
                    case 3:
                        type = "Work";
                        break;
                    case 4:
                        type = "Work Fax";
                        break;
                    case 5:
                        type = "Home Fax";
                        break;
                    case 6:
                        type = "Pager";
                        break;
                    case 7:
                        type = "Other";
                        break;
                    case 8:
                        type = "Callback";
                        break;
                    default:
                        type = "Custom";
                        break;

                }

                try {
                    if (!phone.equals("") && !phone.isEmpty()) {
                        phNo = phone.replace(" ", "").replace("(", "").replace(")", "").replace("-", "");
                    }


                    if (phNo != null && phNo.startsWith("0") && phNo.length() == 11) {
                        //replace the 0 start characters
                        phNo = phNo.replaceFirst("0", "");
                        MyLog.d(TAG, "readContacts: phno: " + phNo);
                    }
                    contactsPojo = new ContactsPojo();
                    JSONObject contactObj = new JSONObject();
                    contactObj.put("Phno", phNo);
                    contactObj.put("Name", name);
                    contactObj.put("Type", type);
                    if (broadCastSavedName != null) {
                        if (phNo != null && savedNumber != null && phNo.contains(savedNumber)) {
                            isNeedToSendServerNow = true;
                            broadCastSavedName.savedName(name);
                            savedNumber = null;
                        }
                    }
                    try {
                        arrContacts.put(contactObj);
                        contactsPojo.setNumber(phNo);
                        contactsPojo.setName(name);
                        contactsPojo.setType(type);
                        Contactdata.put(phNo, contactsPojo);

                        // data.add(contactsPojo);
//                                    ////System.out.println("---" + arrContacts.length());
//                                    ////System.out.println("---" + Contactdata.size());
                    } catch (Exception e) {
                        MyLog.e(TAG, "", e);
                    }

                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            }
            //         pCur.close();
            //   }
            //   }
            //  }

            cur.close();
        }

        if (contactEntries != null) {
            contactEntries.clear();
        }
        Set<Map.Entry<String, ContactsPojo>> set = Contactdata.entrySet();
        for (Map.Entry<String, ContactsPojo> entry : set) {
            ScimboContactModel d = new ScimboContactModel();
            d.setFirstName(entry.getValue().getName());
            d.setNumberInDevice(entry.getValue().getNumber());
            d.setType(entry.getValue().getType());
            contactEntries.add(d);
            if (currentUserno.equalsIgnoreCase(d.getNumberInDevice())) {
                contactEntries.remove(d);
            }
        }

        contact = arrContacts.toString();
        contactLoadedAt = Calendar.getInstance().getTimeInMillis();
        try {
            Collections.sort(contactEntries, Getcontactname.nameAscComparator);
        } catch (Exception e) {
            Log.e(TAG, "readContacts: ", e);
        }
        contactDBtime.setfirsttimecontactSyncCompleted(true);
        contactDBtime.setContactLastRefreshTime(System.currentTimeMillis());
        MyLog.d(TAG, "^^^ readContacts: end");
        if (isNeedToSendServerNow) {
            isNeedToSendServerNow = false;
            updateDataToTheServer(context);
        }
    }

    private void setEventTimeout(final Context context) {
        if (eventHandler == null) {

            eventHandler = new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message message) {
                    // This is where you do your work in the UI thread.
                    sendContactBroadcast(false, context);

                }
            };
            eventHandler.postDelayed(eventRunnable, SocketManager.CONTACT_REFRESH_TIMEOUT);
        }
    }

    public static void startContactService(Context context, boolean isContactSync) {
        Log.e(TAG, "bindContactService: " + isContactSync);
        if (!isStarted && MessageService.isStarted()) {
            EventBus.getDefault().post(new ContactRefresh());
        }
    }

    public static void bindContactService(Context context, boolean isContactSync) {
        Log.d(TAG, "bindContactService: " + isContactSync);
        isNeedToSendServerNow = true;
        EventBus.getDefault().post(new ContactRefresh());
    }


    public interface BroadCastSavedName {
        void savedName(String name);
    }

}
