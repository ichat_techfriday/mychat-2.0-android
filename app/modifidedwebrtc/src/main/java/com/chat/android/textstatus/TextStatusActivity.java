package com.chat.android.textstatus;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.chat.android.R;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.status.controller.StatusUploadUtil;
import com.chat.android.status.model.StatusModel;

import java.util.ArrayList;
import java.util.Collections;

import androidx.appcompat.app.AppCompatActivity;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static com.chat.android.status.view.activity.StatusAddActivity.STATUS_ADD;

public class TextStatusActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout main_layout;
    private RelativeLayout close_layout;
    private EmojiconEditText status_edit_text;
    private LinearLayout smiley_layout;
    private ImageView smiley_image;
    private LinearLayout font_layout;
    private ImageView font_imageview;
    private LinearLayout paint_layout;
    private ImageButton send_status_button;
    private Context mContext;
    int color = 0;
    int font = 0;


    String Defaultcolor = "#233540";
    String Colorcode = "#7ACCA5";
    String Colorcode1 = "#6D267D";
    String Colorcode2 = "#5696FE";
    String Colorcode3 = "#7D8FA3";
    String Colorcode4 = "#72666B";

    String Colorcode5 = "#56C9FF";
    String Colorcode6 = "#26C4DB";
    String Colorcode7 = "#FE7A6B";
    String Colorcode8 = "#8C688F";
    String Colorcode9 = "#C79ECC";
    String Colorcode10 = "#B5B226";
    String Colorcode11 = "#EFB230";
    String Colorcode12 = "#AD8772";
    String Colorcode13 = "#782138";
    String Colorcode14 = "#C1A040";
    String Colorcode15 = "#A52B70";
    String Colorcode16 = "#8294C9";
    String Colorcode17 = "#54C166";
    String Colorcode18 = "#FF898C";

    String Server_send_color_code = "#8BB1D6";
    String Server_Send_Font_Type = "0";
    String themeCategory = "0";

    View rootView;
    EmojIconActions emojIcon;

    private ArrayList<StatusModel> Textstatuslist = new ArrayList<>();

    private String from, mCurrentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_status);
        mContext = TextStatusActivity.this;
        initialize();

    }

    private void initialize() {

        SessionManager sessionManager = SessionManager.getInstance(this);
        from = sessionManager.getCurrentUserID();
        mCurrentUserId = sessionManager.getCurrentUserID();

        main_layout = (RelativeLayout) findViewById(R.id.main_layout);
        close_layout = (RelativeLayout) findViewById(R.id.close_layout);
        status_edit_text = (EmojiconEditText) findViewById(R.id.status_edit_text);
        smiley_layout = (LinearLayout) findViewById(R.id.smiley_layout);
        smiley_image = (ImageView) findViewById(R.id.smiley_image);
        font_layout = (LinearLayout) findViewById(R.id.font_layout);
        font_imageview = (ImageView) findViewById(R.id.font_imageview);
        paint_layout = (LinearLayout) findViewById(R.id.paint_layout);
        rootView = findViewById(R.id.mainRelativeLayout);
        send_status_button = (ImageButton) findViewById(R.id.send_status_button);


        emojIcon = new EmojIconActions(this, rootView, status_edit_text, smiley_image);
        emojIcon.setIconsIds(R.drawable.textstatus_keyboard, R.drawable.status_text_smiley);

        status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Karla-Regular.ttf"));
        themeCategory = getIntent().getStringExtra("category");
        OnclickMethod();
    }

    private void OnclickMethod() {

        close_layout.setOnClickListener(this);
        smiley_layout.setOnClickListener(this);
        font_layout.setOnClickListener(this);
        paint_layout.setOnClickListener(this);
        smiley_image.setOnClickListener(this);
        send_status_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.close_layout:

                finish();

                break;


            case R.id.smiley_image:

                emojIcon.ShowEmojIcon();

                break;

            case R.id.font_layout:

                FontChange();

                break;

            case R.id.paint_layout:

                BackgroundColorChange();

                break;

            case R.id.send_status_button:

                StatusSendToServer();

                break;
        }

    }

    private void FontChange() {

        if (font == 0) {

            font = 1;
            Server_Send_Font_Type = "0";

            //  status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Karla-Bold.ttf"));
            status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Roboto-Medium.ttf"));

        } else if (font == 1) {

            font = 2;

            Server_Send_Font_Type = "1";

            // status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Karla-Italic.ttf"));
            status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/TimesNewRomanPSMT.ttf"));


        } else if (font == 2) {

            font = 3;

            Server_Send_Font_Type = "2";

            //  status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Karla-Regular.ttf"));
            status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/NoricanRegular.ttf"));

        } else if (font == 3) {

            font = 4;

            Server_Send_Font_Type = "3";

            //  status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Karla-Regular.ttf"));
            status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/BryndanWrite.ttf"));

        } else if (font == 4) {

            font = 0;

            Server_Send_Font_Type = "4";

            //  status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Karla-Regular.ttf"));
            status_edit_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OswaldHeavy.ttf"));

        }
    }


    private void BackgroundColorChange() {

        if (color == 0) {

            color = 1;

            main_layout.setBackgroundColor(Color.parseColor(Colorcode));

            Server_send_color_code = Colorcode;

        } else if (color == 1) {

            color = 2;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode1));

            Server_send_color_code = Colorcode1;

        } else if (color == 2) {

            color = 3;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode2));

            Server_send_color_code = Colorcode2;

        } else if (color == 3) {

            color = 4;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode3));

            Server_send_color_code = Colorcode3;

        } else if (color == 4) {

            color = 5;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode4));

            Server_send_color_code = Colorcode4;

        } else if (color == 5) {

            color = 6;
         /*   main_layout.setBackgroundColor(Color.parseColor(Defaultcolor));

            Server_send_color_code=Defaultcolor;*/
            main_layout.setBackgroundColor(Color.parseColor(Colorcode5));

            Server_send_color_code = Colorcode5;

        } else if (color == 6) {

            color = 7;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode6));

            Server_send_color_code = Colorcode6;

        } else if (color == 7) {

            color = 8;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode7));

            Server_send_color_code = Colorcode7;

        } else if (color == 8) {

            color = 9;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode8));

            Server_send_color_code = Colorcode8;

        } else if (color == 9) {

            color = 10;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode9));

            Server_send_color_code = Colorcode9;

        } else if (color == 10) {

            color = 11;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode10));

            Server_send_color_code = Colorcode10;

        } else if (color == 11) {

            color = 12;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode11));

            Server_send_color_code = Colorcode11;

        } else if (color == 12) {

            color = 13;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode12));

            Server_send_color_code = Colorcode12;

        } else if (color == 13) {

            color = 14;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode13));

            Server_send_color_code = Colorcode13;

        } else if (color == 14) {

            color = 15;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode14));

            Server_send_color_code = Colorcode14;

        } else if (color == 15) {

            color = 16;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode15));

            Server_send_color_code = Colorcode15;

        } else if (color == 16) {

            color = 17;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode16));

            Server_send_color_code = Colorcode16;

        } else if (color == 17) {

            color = 18;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode17));

            Server_send_color_code = Colorcode17;

        } else if (color == 18) {

            color = 19;
            main_layout.setBackgroundColor(Color.parseColor(Colorcode18));

            Server_send_color_code = Colorcode18;

        } else if (color == 19) {

            color = 0;
            main_layout.setBackgroundColor(Color.parseColor(Defaultcolor));

            Server_send_color_code = Defaultcolor;

        }


    }


    private void StatusSendToServer() {

        String Caption_text = status_edit_text.getText().toString();

        StatusModel status = new StatusModel();
        status.setTextstatus(true);
        status.setTextstatus_color_code(Server_send_color_code);
        status.setTextstatus_text_font(Server_Send_Font_Type);
        status.setTextstatus_caption(Caption_text);
        status.setThemeCategory(themeCategory);
        status.setUserId(mCurrentUserId);
      //  Log.e("StatusSendToServer", "StatusSendToServer" + status.toString());
        CoreController.getStatusDB(getApplicationContext()).insertNewStatus(Collections.singletonList(status), mCurrentUserId);
        StatusUploadUtil.getInstance().checkAndUploadStatus(mContext);
        Intent okIntent = new Intent();
        setResult(STATUS_ADD, okIntent);
        finish();

    }
}
