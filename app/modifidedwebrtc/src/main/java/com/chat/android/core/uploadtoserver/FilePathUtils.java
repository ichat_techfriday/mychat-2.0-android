package com.chat.android.core.uploadtoserver;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

public class FilePathUtils {
    public static String getImageRealFilePath(Context context, Uri uri) {
try {
    String wholeID = DocumentsContract.getDocumentId(uri);
    // Split at colon, use second item in the array
    String id = wholeID.split(":")[1];
    String[] column = {MediaStore.Images.Media.DATA};
    // where id is equal to
    String sel = MediaStore.Images.Media._ID + "=?";
    Cursor cursor = context.getContentResolver().
            query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);
    String filePath = null;
    if (cursor != null) {
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
    }
    return filePath;
}catch (Exception e){
    return uri.getPath();
}
}

    public static String getVideoRealFilePath(Context context, Uri uri) {
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];
            String[] column = {MediaStore.Video.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Video.Media._ID + "=?";
            Cursor cursor = context.getContentResolver().
                    query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                            column, sel, new String[]{id}, null);
            String filePath = "";
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        } catch (Exception e) {
            return uri.getPath();
        }
    }

    public static String getAudioRealFilePath(Context context, Uri uri) {
try {
    String wholeID = DocumentsContract.getDocumentId(uri);
    // Split at colon, use second item in the array
    String id = wholeID.split(":")[1];
    String[] column = {MediaStore.Audio.Media.DATA};
    // where id is equal to
    String sel = MediaStore.Audio.Media._ID + "=?";
    Cursor cursor = context.getContentResolver().
            query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);
    String filePath = "";
    int columnIndex = cursor.getColumnIndex(column[0]);
    if (cursor.moveToFirst()) {
        filePath = cursor.getString(columnIndex);
    }
    cursor.close();
    return filePath;
}
catch (Exception e){
    return uri.getPath();
}
}
}
