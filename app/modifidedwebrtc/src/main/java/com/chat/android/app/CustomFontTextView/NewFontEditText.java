package com.chat.android.app.CustomFontTextView;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

public class NewFontEditText extends AppCompatEditText {

    public NewFontEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NewFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NewFontEditText(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Karla-Bold.ttf");
        setTypeface(tf);
    }
}
