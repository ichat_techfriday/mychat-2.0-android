package com.chat.android.app.activity;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.chat.android.R;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.core.CoreActivity;

/**
 */
public class ScimboNewPageAbout extends CoreActivity {

    private static final String TAG = "ScimboNewPageAbout";
    private AvnNextLTProRegTextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();


        tvVersion = findViewById(R.id.tvVersion);
        PackageManager manager = getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            String version = "Version " + info.versionName;
            tvVersion.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            MyLog.e(TAG,"",e);
        }

    }
    @Override
    public void onBackPressed() {

            super.onBackPressed();
        }


}
