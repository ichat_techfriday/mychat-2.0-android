package com.chat.android.app.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chat.android.app.calls.CallsActivity;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chat.android.R;
import com.chat.android.app.calls.CallHistoryFragment;
import com.chat.android.app.dialog.ChatLockPwdDialog;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.CommonData;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.RemoteConfigUtils;
import com.chat.android.app.utils.SharedPreference;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.ShortcutBadgeManager;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.message.PictureMessage;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.scimbohelperclass.ScimboDialogUtils;
import com.chat.android.core.scimbohelperclass.ScimboImageUtils;
import com.chat.android.core.scimbohelperclass.ScimboPermissionValidator;
import com.chat.android.core.service.Constants;
import com.chat.android.core.socket.MessageService;
import com.chat.android.core.socket.NotificationUtil;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.core.uploadtoserver.FileUploadDownloadManager;
import com.chat.android.fcm.MyFirebaseInstanceIDService;
import com.chat.android.status.controller.SharedPrefUtil;
import com.chat.android.status.view.StatusHomeFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.soundcloud.android.crop.Crop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewHomeScreenActivty extends CoreActivity implements SearchView.OnQueryTextListener,
        DrawerLayout.DrawerListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "NewHomeScreenActivty";
    public static final String FROM_MISSED_CALL_NOTIFICATION = "FromMissedNotify";
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TabType tabType;
    private Toolbar toolbar;
    //  private LinearLayout mLnrcallStatus;
    boolean mLoadContacts = false;
    boolean is_telpon_chat ;
    private String[] tabArray;
    //  private static final Integer[] tabIcons = {R.drawable.ic_chat_white_24dp, R.drawable.custom_call_icon, R.drawable.ic_account_box_white_24dp};

    boolean mAutostart;
    private final Intent[] POWERMANAGER_INTENTS = {
            new Intent().setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")),
            new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")),
            //new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity")),
            new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.startupapp.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.startup.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity")),
            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager")),
            //new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")),
            //new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity")),
            new Intent().setComponent(new ComponentName("com.samsung.android.lool", "com.samsung.android.sm.ui.battery.BatteryActivity")),
            new Intent().setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")),
            new Intent().setComponent(new ComponentName("com.samsung.android.lool", "com.samsung.android.sm.ui.battery.BatteryActivity")),
            new Intent().setComponent(new ComponentName("com.miui.securitycenter", "com.miui.powercenter.PowerSettings")),
            new Intent().setComponent(new ComponentName("com.htc.pitroad", "com.htc.pitroad.landingpage.activity.LandingPageActivity")),

//            mContext.getPackageManager().getLaunchIntentForPackage("com.asus.mobilemanager"),
            //     mContext.getPackageManager().getLaunchIntentForPackage("com.huawei.systemmanager"),
            //    mContext.getPackageManager().getLaunchIntentForPackage("com.color.safecenter"),
            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.entry.FunctionActivity")).setData(Uri.parse("mobilemanager://function/entry/AutoStart")),
            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.MainActivity"))
    };

    String status = "", from, myID, username, pwd, stat;
    MessageItemChat messageItemChat;
    private UserInfoSession userInfoSession;
    String mUserId, uniqueCurrentID;
    public SessionManager sessionManager;
    private boolean isDeviceAlertShowing;

    private DrawerLayout mDrawerLayout;
    NavigationView navigationView;
    private Typeface avnRegFont, avnDemiFont;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int CAMERA_REQUEST_CODE = 2;
    Uri cameraImageUri;
    final Context context = this;
    private ImageView ivProfilePic;
    private TextView tvName;
    private ImageView bgimage;
    private ArrayList<ScimboPermissionValidator.Constants> myPermissionConstantsArrayList;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 123;
    Cursor cur;
    private Map<String, String> mycontact = new HashMap();


    private View updateview;
    public Dialog updatedialog;

    boolean navigate_check = false;
    private boolean isDeninedRTPs = false;
    private boolean showRationaleRTPs = false;


    private Boolean isInternetPresent = false;
    private String GCM_Id = "";


    public static String Tab_Number = "";


    class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equalsIgnoreCase("com.setupdate.dialog")) {

                UpdateDialogShow();
            }
        }
    }

    Receiver receiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home_screen_activty);
        MyLog.d(TAG, "performance onCreate: ");
        initialize();


        Bundle bundle = getIntent().getExtras();
        boolean fromMissedNotify = false;
        if (bundle != null) {

            fromMissedNotify = bundle.getBoolean(FROM_MISSED_CALL_NOTIFICATION, false);
            fromMissedNotify = bundle.getBoolean(FROM_MISSED_CALL_NOTIFICATION, false);

            if (!fromMissedNotify) {
                from = bundle.getString("fromNotify");
                myID = bundle.getString("ID");
                username = bundle.getString("uname");
                stat = bundle.getString("status");
                pwd = bundle.getString("pwd");
                messageItemChat = (MessageItemChat) bundle.getSerializable("MessageItem");


                if (from.equalsIgnoreCase("service")) {

                    //Toast.makeText(this, "Sorry! This Chat is Already Locked..Unlock to proceed...",Toast.LENGTH_SHORT).show();
                    performNavigationToChatView(myID, username, stat, pwd, messageItemChat);
                }
            }
        }
        //tabArray= {"CHATS", "CALLS", "STATUS"};//Tab title array

        //   tabArray= {getString(R.string.chats), getString(R.string.calls), getString(R.string.status)};
        tabArray = new String[]{getString(R.string.chats), getString(R.string.calls), getString(R.string.status)};
        SetNavigationConfiguration();

        //Implementing tab selected listener over tablayout
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());//setting current selected item over viewpager
                switch (tab.getPosition()) {
                    case 0:
                        //  Log.e("TAG", "TAB1");
                        Tab_Number = "1";
                        break;
                    case 1:

                        Tab_Number = "2";
                        break;
                    case 2:
                        //  Log.e("TAG", "TAB3");
                        Tab_Number = "3";
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        //Call tab type method
        onTabType();
        mAutostart = SharedPreference.getInstance().getBool(context, "autostart");
        if (!CoreController.isObbip && !mAutostart) {
            //  addAutoStartup();

            for (Intent intent : POWERMANAGER_INTENTS) {
                if (getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY) != null) {
                    // show dialog to ask user action
                    //Check if is not enabled
                    /*String manufacturer = android.os.Build.MANUFACTURER;
                    if ("HUAWEI".equalsIgnoreCase(manufacturer)) {
                        huaweiProtectedApps();
                    }
                    else*/
                   AlertDialogCreate(context, intent);
                    break;
                }
            }
        }
        //For Googleplayprotect in PlayStore
        //----------------------------------Battery Optimization Stop-------------------------------
       /* if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                try {
                    intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                    intent.setData(Uri.parse("package:" + packageName));
                    startActivity(intent);
                }
                catch (Exception e){
                    Log.e(TAG, "onCreate: ",e );
                }

            }
        }
*/
        if (fromMissedNotify) {
            //Change tab to calls
            setTab();
        }
        MyLog.d(TAG, "onCreate: performance 2 ");
    }


    public void AlertDialogCreate(final Context mContext, final Intent Intent) {
        String title = getString(R.string.autostart_msg) + " "+ mContext.getResources().getString(R.string.app_name) + " ";
        String manufacturer = android.os.Build.MANUFACTURER;
        if ("HUAWEI".equalsIgnoreCase(manufacturer)) {
            title = "Disable auto-manage for " + mContext.getResources().getString(R.string.app_name) + "";
        }
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, R.style.AlertDialogTheme);

        alertDialogBuilder
                .setMessage(title + getString(R.string.autostart_msg_continuing))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mContext.getResources().getBoolean(R.bool.is_arabic)) {
                            Toast.makeText(context, getString(R.string.enable_toast_msg) + getString(R.string.in_autostart_toast_msg) + mContext.getResources().getString(R.string.app_name), Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(context, getString(R.string.enable_toast_msg) + mContext.getResources().getString(R.string.app_name) + getString(R.string.in_autostart_toast_msg), Toast.LENGTH_LONG).show();
                        }
                        SharedPreference.getInstance().saveBool(context, "autostart", true);
                        startActivity(Intent);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Toast.makeText(this, "CANCEL button click ", Toast.LENGTH_SHORT).show();
                        dialog.cancel();

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        if (!alertDialog.isShowing()) {
            // show it
            alertDialog.show();
        }
    }

    private void initialize() {

        Tab_Number = "";

        userInfoSession = new UserInfoSession(NewHomeScreenActivty.this);


        tabType = TabType.CUSTOM;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);//setting tab over viewpager
        navigationView = findViewById(R.id.navigation_view);
        //  mLnrcallStatus = findViewById(R.id.lnrcallStatus);

        avnRegFont = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        avnDemiFont = CoreController.getInstance().getAvnNextLTProDemiTypeface();

        receiver = new Receiver();
        IntentFilter intent = new IntentFilter();
        intent.addAction("com.setupdate.dialog");
        registerReceiver(receiver, intent);

        // mLnrcallStatus.setOnClickListener(this);
      /*  mLnrcallStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(context, CallsActivity.class);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                context.startActivity(callIntent);

            }
        });*/
    }


    private void getintent() {

    }

    public void setTab() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(1, true);

                tabLayout.setScrollPosition(1, 0f, true);
            }
        }, 1000);

    }

    private void SetNavigationConfiguration() {

        init_navigator();

        sessionManager = SessionManager.getInstance(NewHomeScreenActivty.this);
        sessionManager.IsapplicationisKilled(false);
        sessionManager.setPushdisplay(true);

        if (SessionManager.getInstance(NewHomeScreenActivty.this).isLoginKeySent()
                && !SessionManager.getInstance(NewHomeScreenActivty.this).isValidDevice()) {
            //     showDeviceChangedAlert();
        } else {
            if (Constants.IS_FROM_SHARING_PAGE) {
                MyLog.d(TAG, "SetNavigationConfiguration: no need to createUser again");
                Constants.IS_FROM_SHARING_PAGE = false;
            } else {
                createUser();
            }

            Date startDate = null;
            Date cDate = new Date();
            final DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor((getResources().getColor(R.color.Statusbar)));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        } else {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }


        if (!MessageService.isStarted()) {

            /*Intent s = new Intent(NewHomeScreenActivty.this, MessageService.class);
            startService(s);*/

            startService();
        }


    }

    public void startService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AppUtils.startService(this, MessageService.class);
        } else {
            Intent s = new Intent(this, MessageService.class);
            startService(s);
        }
    }

    public void performNavigationToChatView(String receiverDocumentID, String username, String stat, String pwd, MessageItemChat messageItemChat) {

        openUnlockChatDialog(receiverDocumentID, stat, pwd, messageItemChat);

    }

    private void openUnlockChatDialog(String receiverDocumentID, String stat, String pwd, MessageItemChat messageItemChat) {
        String convId = userInfoSession.getChatConvId(receiverDocumentID);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1("Enter your Password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setforgotpwdlabel("Forgot Password");
        dialog.setHeader("Unlock Chat");
        dialog.setButtonText("Unlock");
        Bundle bundle = new Bundle();
        bundle.putSerializable("MessageItem", messageItemChat);
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "single");
        bundle.putString("from", uniqueCurrentID);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatunLock");
    }

    /**
     * on the basis of tab type call respective method
     **/
    private void onTabType() {
        switch (tabType) {
            case DEFAULT:
                //don't do anything here
                break;
            case ICON_TEXT:
            case ICONS_ONLY:
                //for both Types call set Icons method
                //    tabWithIcon();
                break;
            case CUSTOM:
                //Call custom tab method
                setUpCustomTabs();
                break;
        }
    }

    //Setting View Pager
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new ChatListFragment(), "");
        adapter.addFrag(new CallHistoryFragment(), "");
        adapter.addFrag(new StatusHomeFragment(), "");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }


    private void setUpCustomTabs() {
        for (int i = 0; i < tabArray.length; i++) {
            TextView customTab = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null);//get custom view
            customTab.setText(tabArray[i]);//set text over view
            //  customTab.setCompoundDrawablesWithIntrinsicBounds(0, tabIcons[i], 0, 0);//set icon above the view
            TabLayout.Tab tab = tabLayout.getTabAt(i);//get tab via position
            if (tab != null)
                tab.setCustomView(customTab);//set custom view
        }
    }

    private final List<Fragment> mFragmentList = new ArrayList<>();//fragment arraylist

    //View Pager fragments setting adapter class
    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<String> mFragmentTitleList = new ArrayList<>();//title arraylist

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }


        //adding fragments and title method
        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (tabType == TabType.ICONS_ONLY)
                return "";
            return mFragmentTitleList.get(position);
        }
    }


    private void showDeviceChangedAlert() {
        Intent msgSvcIntent = new Intent(NewHomeScreenActivty.this, MessageService.class);
        stopService(msgSvcIntent);

        CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage("This " + getResources().getString(R.string.app_name) + " account used in another one mobile, Do you want to continue with this mobile?");
        //dialog.setNegativeButtonText("No");
        dialog.setPositiveButtonText("OK");
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                SessionManager.getInstance(NewHomeScreenActivty.this).logoutUser(false);
                SessionManager.getInstance(NewHomeScreenActivty.this).setIsValidDevice(true);
                finish();
            }

            @Override
            public void onNegativeButtonClick() {
//                sessionManager.logoutUser();
                SessionManager.getInstance(NewHomeScreenActivty.this).logoutUser(false);
                /*Intent mainIntent = new Intent(Intent.ACTION_MAIN);
                mainIntent.addCategory(Intent.CATEGORY_HOME);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainIntent);*/
                finish();
            }
        });

        if (!isDeviceAlertShowing) {
            dialog.show(getSupportFragmentManager(), "Account validate");
            isDeviceAlertShowing = true;
        }
    }

    private void createUser() {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_CREATE_USER);
        JSONObject object = new JSONObject();
        try {
            object.put("_id", SessionManager.getInstance(getApplicationContext()).getCurrentUserID());
            object.put("mode", "phone");
            object.put("chat_type", "single");
            object.put("device", Constants.DEVICE);
            String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();
            MyLog.d("createUser", "securityToken: " + securityToken);
            object.put("token", securityToken);


        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        messageEvent.setMessageObject(object);
        EventBus.getDefault().post(messageEvent);
    }

    private void validateDeviceWithAccount() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String userId = SessionManager.getInstance(NewHomeScreenActivty.this).getCurrentUserID();

                try {
                    JSONObject msgObj = new JSONObject();
                    msgObj.put("from", userId);

                    SendMessageEvent event = new SendMessageEvent();
                    event.setEventName(SocketManager.EVENT_CHECK_MOBILE_LOGIN_KEY);
                    event.setMessageObject(msgObj);
                    EventBus.getDefault().post(event);
                } catch (JSONException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }, 5000);

    }


    private void getServerTime() {
        try {
            String userId = SessionManager.getInstance(NewHomeScreenActivty.this).getCurrentUserID();
            JSONObject timeObj = new JSONObject();
            timeObj.put("from", userId);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_SERVER_TIME);
            event.setMessageObject(timeObj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }


    //-------------------------------------------------Navigation Method Set--------------------------------------

    private void init_navigator() {
        // Navigation Drawer
        ActionBarDrawerToggle mActionBarDrawerToggle;

        mDrawerLayout = findViewById(R.id.drawer_layout);

        // Code here will be triggered once the drawer closes as we don't want anything to happen so we leave this blank
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar, R.string.navigation_drawer_opened, R.string.navigation_drawer_closed);

        //calling sync state is necessary or else your hamburger icon wont show up
        mActionBarDrawerToggle.syncState();

        mDrawerLayout.addDrawerListener(NewHomeScreenActivty.this);

        // mNavigationView.setItemIconTintList(null);
        navigationView = findViewById(R.id.navigation_view);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id) {
                    case R.id.chat:
                        //viewPager.setCurrentItem(0);
                        ActivityLauncher.launchChatSettings(NewHomeScreenActivty.this);
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                     //   menuItem.setChecked(false);
                        return true;
                    case R.id.contact:
                        /*viewPager.setCurrentItem(2);
                        mDrawerLayout.closeDrawer(GravityCompat.START);*/
                        Intent contactIntent = new Intent(NewHomeScreenActivty.this, SettingContact.class);
                        startActivity(contactIntent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    //    menuItem.setChecked(false);
                        return true;
                    case R.id.acc:
                        Intent intent = new Intent(getApplicationContext(), Account_main_list.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                     //   menuItem.setChecked(false);
                        return true;
                    case R.id.abt:
                        Intent intent_abt = new Intent(getApplicationContext(), AboutHelp.class);
                        startActivity(intent_abt);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                     //   menuItem.setChecked(false);
                        return true;
                    case R.id.datause:
                        Intent intent_datausage = new Intent(getApplicationContext(), DataUsage.class);
                        startActivity(intent_datausage);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                   //     menuItem.setChecked(false);
                        return true;
                    case R.id.notify:
                        Intent intent_notify = new Intent(getApplicationContext(), NotificationSettings.class);
                        startActivity(intent_notify);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                     //   menuItem.setChecked(false);
                        return true;


                }
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }

        });

        overrideFonts(this, mDrawerLayout);
    }


    public void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                TextView tv = ((TextView) v);
                if (tv.getText().toString().equalsIgnoreCase(getResources().getString(R.string.app_name))) {
                    tv.setTypeface(avnDemiFont);
                } else {
                    tv.setTypeface(avnRegFont);
                }
            }
        } catch (Exception e) {
        }
    }


    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

        overrideFonts(NewHomeScreenActivty.this, navigationView);
    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {
        ivProfilePic = drawerView.findViewById(R.id.ivProfilePic);
        tvName = drawerView.findViewById(R.id.tvName);
        bgimage = drawerView.findViewById(R.id.bgimage);
        ivProfilePic.setOnClickListener(NewHomeScreenActivty.this);
        tvName.setText(SessionManager.getInstance(NewHomeScreenActivty.this).getnameOfCurrentUser());

        String path = AppUtils.getProfileFilePath(this);
        if (path != null && !path.isEmpty()) {
            GlideUrl glideUrl = null;
            //       Picasso.with(NewHomeScreenActivty.this).load(path).error(R.drawable.nav_menu_background).noPlaceholder().into(ivProfilePic);
            if (AppUtils.isEncryptionEnabled(this)) {
                glideUrl = new GlideUrl(path, new LazyHeaders.Builder()
                        .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                        .addHeader("requesttype", "site")
                        .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                        .addHeader("referer", path)
                        .build());
            } else {
                glideUrl = new GlideUrl(path,
                        new LazyHeaders.Builder()
                                .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                                .addHeader("requesttype", "site")
                                .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                                .addHeader("referer", path)
                                .build());
            }

            Glide
                    .with(context)
                    .load(glideUrl)

                    // .load(AppUtils.getUrlWithHeaders(path, context))
                    .asBitmap()
                    .error(R.drawable.nav_menu_background)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .into(new SimpleTarget<Bitmap>() {

                        @Override
                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                            // TODO Auto-generated method stub
                            ivProfilePic.setImageBitmap(arg0);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                        }
                    });
        } else {
            is_telpon_chat = context.getResources().getBoolean(R.bool.is_telpon_chat);
/*if (is_telpon_chat){
    ivProfilePic.setImageResource(R.drawable.ic_profile_default);

}else {
    ivProfilePic.setImageResource(R.drawable.nav_menu_background);

}*/
            ivProfilePic.setImageResource(R.drawable.ic_profile_nav_header);


         }


    }


    @Override
    public void onDrawerClosed(@NonNull View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ivProfilePic:
                Intent profileIntent = new Intent(NewHomeScreenActivty.this, UserProfile.class);
                startActivity(profileIntent);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                if (data != null) {
                    Uri selectedImageUri = data.getData();
                    beginCrop(selectedImageUri);
                }

            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                beginCrop(cameraImageUri);
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {

            if (data != null) {
                Uri uri = Crop.getOutput(data);
                String filePath = uri.getPath();

                try {
                    Bitmap alignedBitmap = ScimboImageUtils.getAlignedBitmap(ScimboImageUtils.getThumbnailBitmap(filePath, 150), filePath);
                    uploadImage(alignedBitmap);
                } catch (IOException e) {
                    MyLog.e(TAG, "", e);
                }
            }
        }
        if (requestCode == 9) {
            if (resultCode == RESULT_OK) {
                MyLog.e(TAG, "SHOW");
                showProgres();
            }
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void uploadImage(Bitmap circleBmp) {

        if (circleBmp != null) {
            try {
                File imgDir = new File(MessageFactory.PROFILE_IMAGE_PATH);
                if (!imgDir.exists()) {
                    imgDir.mkdirs();
                }

                String profileImgPath = imgDir + "/" + Calendar.getInstance().getTimeInMillis() + "_pro.jpg";

                File file = new File(profileImgPath);
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();

                OutputStream outStream = new FileOutputStream(file);
                circleBmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                outStream.flush();
                outStream.close();

                String serverFileName = SessionManager.getInstance(NewHomeScreenActivty.this).getCurrentUserID().concat(".jpg");

                PictureMessage message = new PictureMessage(NewHomeScreenActivty.this);
                JSONObject object = (JSONObject) message.createUserProfileImageObject(serverFileName, profileImgPath);
                FileUploadDownloadManager fileUploadDownloadMgnr = new FileUploadDownloadManager(NewHomeScreenActivty.this);
                Log.d(TAG, "onClick: startFileUpload13");
                fileUploadDownloadMgnr.startFileUpload(EventBus.getDefault(), object);
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }


        }
    }


    //---------------------------------Event Bus Received Event-------------------------------------------------


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final ReceviceMessageEvent event) {

        hideProgressDialog();
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            Log.e(TAG, "EVENT_IMAGE_UPLOAD");
            Object[] array = event.getObjectsArray();
            try {
                JSONObject objects = new JSONObject(array[0].toString());
                String err = objects.getString("err");
                String message = objects.getString("message");

                if (err.equalsIgnoreCase("0")) {
                    String from = objects.getString("from");
                    String type = objects.getString("type");

                    if (from.equalsIgnoreCase(SessionManager.getInstance(NewHomeScreenActivty.this).getCurrentUserID())
                            && type.equalsIgnoreCase("single")) {
                        String path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();

                        AppUtils.loadImage(NewHomeScreenActivty.this, AppUtils.getValidProfilePath(path),
                                ivProfilePic, 150, R.drawable.ic_profile_default);

                        Toast.makeText(NewHomeScreenActivty.this, message, Toast.LENGTH_SHORT).show();
                    }

                }

            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MESSAGE)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String docId = object.getString("doc_id");
                if (docId.contains(SessionManager.getInstance(NewHomeScreenActivty.this).getCurrentUserID())) {
                    //  changeTabTextCount();
                }
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP)) {

            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String groupAction = object.getString("groupType");

                if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MESSAGE)) {
                    Object[] array = event.getObjectsArray();
                    JSONObject objects = new JSONObject(array[0].toString());
                    if (objects.has("payload")) {
                        //  changeTabTextCount();
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION)) {
            loadDeviceLoginMessage(event.getObjectsArray());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHECK_MOBILE_LOGIN_KEY)) {
            //  loadCheckLoginKey(event.getObjectsArray());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_UPDATE_INFO)) {
        }/* else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_USER_AUTHENTICATED)) {
            Log.d(TAG, "onSuccessListener: EVENT_USER_AUTHENTICATED OfflineMsgTest");
            if (Constants.IS_ENCRYPTION_ENABLED) {
                fetchSecretKeys();
            }
        }else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_SECRET_KEYS)) {
            Object[] obj = event.getObjectsArray();
            changesecuritytoken(obj[0].toString());

        }*/


    }

    private void changesecuritytoken(String data) {
        try {
            MyLog.d(TAG, "EVENT_GET_SECRET_KEYS: ");
            //   String data = response[0].toString();
            JSONObject object = new JSONObject(data);
            String publicKey = "", privateKey = "";
            if (object.has("public_key")) {
                publicKey = object.getString("public_key");
                privateKey = object.getString("private_key");
                SessionManager.getInstance(this).setPublicEncryptionKey(publicKey);
                SessionManager.getInstance(this).setPrivateEncryptionKey(privateKey);
            }


        } catch (Exception e) {
            MyLog.e(TAG, "onSuccessListener: ", e);
        }
    }


    //------------------------------------------Menu Item Click Function-------------------------------

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if (viewPager.getCurrentItem() == 0) {
            menu.findItem(R.id.refresh).setVisible(false);

        } /*else if (viewPager.getCurrentItem() == 1) {
            menu.findItem(R.id.refresh).setVisible(false);

        }*/ else if (viewPager.getCurrentItem() == 2) {
            // configure
            menu.findItem(R.id.refresh).setVisible(false);

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refresh) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onStart() {
        super.onStart();

        MyLog.e(TAG, "onStart");
        SocketManager.clearCallBack();
        EventBus.getDefault().register(NewHomeScreenActivty.this);
    }


    @Override
    public void onStop() {
        super.onStop();
        MyLog.e(TAG, "onStop");
//        new ChangeSetController(HomeScreen.this).setChangeStatus("0");
        EventBus.getDefault().unregister(NewHomeScreenActivty.this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        MyLog.d(TAG, "onResume: performance");

        //checkcallstatus();
        AppUtils.clearNotification(this);
        if (AppUtils.isServiceRunning(this, MessageService.class)) {
            NotificationUtil.clearNotificationData();
        }


        NotificationUtil.clearNotificationData();

        checkAndRequestPermissions();
        // changeTabTextCount();
        checkAndShowForceUpdateDialog();
        if (SessionManager.getInstance(NewHomeScreenActivty.this).isLoginKeySent() && !SessionManager.getInstance(NewHomeScreenActivty.this).isValidDevice()) {
            showDeviceChangedAlert();
        }
        String publicKey = SessionManager.getInstance(NewHomeScreenActivty.this).getPublicEncryptionKey();
        String privateKey = SessionManager.getInstance(NewHomeScreenActivty.this).getPrivateEncryptionKey();

        //MyLog.d(TAG, "onCreate: publicKey==" + publicKey);
        //MyLog.d(TAG, "onCreate: privateKey==" + privateKey);
        MyLog.d(TAG, "onResume: 2");
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppUtils.isNetworkAvailable(NewHomeScreenActivty.this)) {
                    if (!FirebaseApp.getApps(NewHomeScreenActivty.this).isEmpty()) {
                        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                    }
                    RemoteConfigUtils.getInstance().checkUpdate(NewHomeScreenActivty.this);
                    //new AppUpdateDialogAsync(HomeScreen.this).execute();
                }
            }
        }, 100);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        sessionManager = null;

        if (receiver != null) {

            unregisterReceiver(receiver);
        }

        mUserId = null;


    }

    private void checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            myPermissionConstantsArrayList = new ArrayList<>();
//            myPermissionConstantsArrayList.add(ScimboPermissionValidator.Constants.PERMISSION_READ_PHONE_STATE);
            myPermissionConstantsArrayList.add(ScimboPermissionValidator.Constants.PERMISSSION_READ_CONTACTS);
            myPermissionConstantsArrayList.add(ScimboPermissionValidator.Constants.PERMISSION_WRITE_CONTACTS);
            myPermissionConstantsArrayList.add(ScimboPermissionValidator.Constants.WAKE_LOCK);
            if (ScimboPermissionValidator.checkPermission(NewHomeScreenActivty.this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE)) {
                onPermissionGranted();
            }
        } else {
            onPermissionGranted();
        }
    }

    private void onPermissionGranted() {
        if (!mLoadContacts) {
            new getAllContactOfPhone().execute();
            mLoadContacts = true;
        }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            isDeninedRTPs = true;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                showRationaleRTPs = shouldShowRequestPermissionRationale(permission);
                            }
                        }
                        break;
                    }
                    onPermissionResult();
                } else {

                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void onPermissionResult() {
        if (isDeninedRTPs) {
            if (!showRationaleRTPs) {
                //goToSettings();
                ScimboDialogUtils.showPermissionDeniedDialog(NewHomeScreenActivty.this);
            } else {
                isDeninedRTPs = false;
                ScimboPermissionValidator.checkPermission(this,
                        myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE);
            }
        } else {
            onPermissionGranted();
        }
    }


    class getAllContactOfPhone extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                ContentResolver cr = getContentResolver();
                cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                String contacts = "";
                if (cur.getCount() > 0) {

                    while (cur.moveToNext()) {
                        String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                            Cursor pCur = cr.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{id}, null);
                            while (pCur.moveToNext()) {


                                String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                phoneNo = phoneNo.replaceAll("\\s+", "");
                                phoneNo = phoneNo.replaceAll("[^\\d.]", "");
                                if (!phoneNo.trim().equals("null")) {
                                    contacts += phoneNo.trim() + ",";//}

                                    mycontact.put(phoneNo.trim(), name);


                                }
                                //MyLog.d("single contact", phoneNo.trim());
                            }

                            pCur.close();
                        }
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            } finally {
                if (cur != null) {
                    cur.close();
                }
            }

            // MyLog.d("Contacts", mycontact.toString());
            return null;
        }


    }


    public void changeTabTextCount() {
        int chatbadge = 0, groupbadge = 0;
        ShortcutBadgeManager mgnr = new ShortcutBadgeManager(NewHomeScreenActivty.this);
        MessageDbController db = CoreController.getDBInstance(NewHomeScreenActivty.this);
        Session session = new Session(NewHomeScreenActivty.this);

        ArrayList<MessageItemChat> databases = db.selectChatList(MessageFactory.CHAT_TYPE_SINGLE);
        chatbadge = 0;
        for (MessageItemChat msgItem : databases) {
            String toUserId = msgItem.getReceiverID();
            String docID = uniqueCurrentID + "-" + toUserId;
            String convId = userInfoSession.getChatConvId(docID);

            if (convId != null && !convId.equals("") && !session.getarchive(docID)) {
                int count = mgnr.getSingleBadgeCount(convId);
                    /*if (!mark) {
                        count = count + 1;
                    }*/
                if (count > 0) {

                    chatbadge = chatbadge + 1;
                }
            }
        }


        ArrayList<MessageItemChat> groupChats = db.selectChatList(MessageFactory.CHAT_TYPE_GROUP);
        groupbadge = 0;
        for (MessageItemChat msgItem : groupChats) {

            String groupId = msgItem.getReceiverID();
//            Boolean mark = session.getmark(groupId);
            int count = mgnr.getSingleBadgeCount(groupId);
            String docID = uniqueCurrentID + "-" + groupId + "-g";
            /*if (!mark) {
                count = count + 1;
            }*/
            if (count > 0 && !session.getarchive(docID)) {
                groupbadge = groupbadge + 1;
            }
        }

        TextView tvSingleCount = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.tvCount);
        if (chatbadge > 0) {
            chatbadge = chatbadge + groupbadge;
            tvSingleCount.setVisibility(View.VISIBLE);
            tvSingleCount.setText(String.valueOf(chatbadge));
        } else if (groupbadge > 0) {
            chatbadge = chatbadge + groupbadge;
            tvSingleCount.setVisibility(View.VISIBLE);
            tvSingleCount.setText(String.valueOf(chatbadge));
        } else {
            if (tvSingleCount != null) {
                tvSingleCount.setVisibility(View.GONE);
            }
        }

    }


    //---------------------------------------------Check Force Update--------------------------------


    private void checkAndShowForceUpdateDialog() {
        boolean isForceUpdateAvailable = SharedPrefUtil.read(RemoteConfigUtils.KEY_IS_FORCE_UPDATE, false);

        try {
            int currentVersion = getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            int liveVersionNumber = SharedPrefUtil.read(RemoteConfigUtils.KEY_LIVE_VERSION, 0);
            MyLog.d(TAG, "checkAndShowForceUpdateDialog current: " + currentVersion);
            MyLog.d(TAG, "checkAndShowForceUpdateDialog live: " + liveVersionNumber);
            String msg = SharedPrefUtil.read(RemoteConfigUtils.KEY_MESSAGE, "");
            if (isForceUpdateAvailable) {
                if (liveVersionNumber > currentVersion) {
                    RemoteConfigUtils.getInstance().showUpdateDialog(NewHomeScreenActivty.this,
                            true, msg);
                }
            } else {
                if (RemoteConfigUtils.getInstance().shallWeShowPopUp()) {
                    if (liveVersionNumber > currentVersion) {
                        RemoteConfigUtils.getInstance().showUpdateDialog(NewHomeScreenActivty.this,
                                false, msg);
                    }
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "checkAndShowForceUpdateDialog: ", e);
        }


    }


    private void loadDeviceLoginMessage(Object[] objectsArray) {
        try {
            JSONObject object = new JSONObject(objectsArray[0].toString());

            String err = object.getString("err");
            if (err.equalsIgnoreCase("0")) {
                String msg = object.getString("msg");

                JSONObject apiObj = object.getJSONObject("apiMobileKeys");
                String deviceId = apiObj.getString("DeviceId");
                String loginKey = apiObj.getString("login_key");
                String timeStamp = apiObj.getString("timestamp");

                String deviceLoginKey = sessionManager.getLoginKey();

                String settingsDeviceId = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                /*if (!deviceId.equals(settingsDeviceId)) {
                    showDeviceChangedAlert();
                }*/
                if (!deviceId.equalsIgnoreCase(settingsDeviceId)) {
                    showDeviceChangedAlert();
                }
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }


    //--------------------------------------Old User Need to Update This App--------------------------
    private void UpdateDialogShow() {


        try {

            if (updatedialog != null) {

                if (updatedialog.isShowing()) {
                    updatedialog.dismiss();
                }

                updatedialog = null;
            }


            DisplayMetrics metrics = this.getResources().getDisplayMetrics();
            int screenWidth = (int) (metrics.widthPixels * 0.80);//fill only 80% of the screen
            updateview = View.inflate(NewHomeScreenActivty.this, R.layout.updatedialog_layout, null);
            updatedialog = new Dialog(NewHomeScreenActivty.this);
            updatedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            updatedialog.setContentView(updateview);
            updatedialog.setCanceledOnTouchOutside(false);
            updatedialog.setCancelable(false);
            updatedialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
            updatedialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            final EditText email = updateview.findViewById(R.id.email);
            final EditText password = updateview.findViewById(R.id.password);
            Button update_ripple = updateview.findViewById(R.id.update_ripple);

            update_ripple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (email.getText().toString().length() == 0) {
                        errorEdit(email, getResources().getString(R.string.register_label_alert_registeremail));
                    } else if (!isValidEmail(email.getText().toString())) {
                        errorEdit(email, getResources().getString(R.string.register_label_alert_email));
                    } else if (password.getText().toString().length() == 0) {
                        errorEdit(password, getResources().getString(R.string.password_alert));
                    } else if (!isValidPassword(password.getText().toString())) {
                        errorEdit(password, getResources().getString(R.string.register_label_alert_password));
                    } else {


                        if (AppUtils.isNetworkAvailable(NewHomeScreenActivty.this)) {

                            String email_is = email.getText().toString();
                            String Pass = password.getText().toString();

                            Updatetoserver(email_is, Pass);

                        } else {

                            Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_LONG).show();
                        }

                    }

                }
            });

            updatedialog.show();


        } catch (Exception e) {
            MyLog.e(TAG, "UpdateDialogShow: ", e);

        }


    }

    private void Updatetoserver(String email_id, String pass) {

        startLoading();

        try {
            String userId = SessionManager.getInstance(NewHomeScreenActivty.this).getCurrentUserID();
            JSONObject updateObj = new JSONObject();
            updateObj.put("from", userId);
            updateObj.put("email", email_id);
            updateObj.put("password", pass);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_UPDATE_INFO);
            event.setMessageObject(updateObj);
            EventBus.getDefault().post(event);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

    }


    // --------Code to set error for EditText--------
    private void errorEdit(EditText editName, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewHomeScreenActivty.this, R.anim.shake);
        editName.startAnimation(shake);
        editName.setError(msg);
    }

    // --------Code to Check Email Validation--------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    // --------validating password with retype password--------
    private boolean isValidPassword(String pass) {
        if (pass.length() < 6) {
            return false;
        } else if (!pass.matches("(.*[a-z].*)")) {
            return false;
        } else if (!pass.matches("(.*[0-9].*)")) {
            return false;
        } else return pass.matches("(.*[A-Z].*)");

    }


    //--------------------------Back Pressed Button---------------------------


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            if (CommonData.selectedChatItems != null) {

                if (CommonData.selectedChatItems.size() > 0) {

                    Intent s = new Intent();
                    s.setAction("com.selectedchat.remove");
                    sendBroadcast(s);
                    return false;

                } else {
                    finish();

                    return true;
                }

            } else {
                finish();

                return true;
            }

        }
        return false;


    }


    private void startLoading() {

        showProgressDialog();

    }

    private void stopLoading() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                hideProgressDialog();

            }
        }, 500);
    }


}
