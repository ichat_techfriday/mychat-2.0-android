package com.chat.android.app.CustomFontTextView;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

public class NewFontTextViewRegular extends AppCompatTextView {

    public NewFontTextViewRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NewFontTextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NewFontTextViewRegular(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Karla-Regular.ttf");
        setTypeface(tf);
    }
}