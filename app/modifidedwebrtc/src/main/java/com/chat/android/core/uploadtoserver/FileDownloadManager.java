package com.chat.android.core.uploadtoserver;

import android.content.Context;

import com.chat.android.app.activity.ChatPageActivity;
import com.chat.android.app.activity.SecretChatViewActivity;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class FileDownloadManager {

    private Context mContext;
    private FileUploadDownloadManager fileUploadDownloadManager;
    private static final String TAG = "FileDownloadManager";
    
    private FileDownloadManager(){
        
    }
    public static FileDownloadManager fileDownloadManager=new FileDownloadManager();
    
    public static FileDownloadManager getInstance(){
        return fileDownloadManager;
    }
    public void  init(FileUploadDownloadManager fileUploadDownloadManager,Context context){
        this.fileUploadDownloadManager=fileUploadDownloadManager;
        this.mContext=context;
    }
    
    

    public void VideoDownloadProgress(EventBus eventBus, JSONObject object) {

       // MyLog.d(TAG, "VideoDownloadProgress: "+object);
        int downloa_progress = 0;
        String End = "0";
        try {
            String Msgid = object.getString("MsgId");
            String Docid = object.getString("DocId");
            String IMagename = object.getString("ImageName");
            String LocalPath = object.getString("LocalPath");
            String LocalFilename = object.getString("LocalFileName");
            String Start = object.getString("start");
            String FileSize = object.getString("filesize");
            String Bytesread = object.getString("bytesRead");
            if (object.has("end")) {
                End = object.getString("end");
            }
            MyLog.d(TAG, "VideoDownloadProgress: start "+Start+" Filesize "+FileSize +" BytesRead: "+Bytesread);

            boolean Filepause = fileUploadDownloadManager.isDownloadFilePaused(Msgid);

            if (!Filepause) {

                if (object.has("bufferData")) {

                    fileUploadDownloadManager.setDownloadResumePauseFileObject(Msgid, object);

                    if (object.get("bufferData") instanceof byte[]) {

                        byte[] buffer = (byte[]) object.get("bufferData");

                        int len = buffer.length;

                        int download_start = Integer.parseInt(Start);

                        downloa_progress = download_start + len;

                        MyLog.e(TAG, "Download Server Progress" + "" + downloa_progress);
                        MyLog.d(TAG, "VideoDownloadProgress: End "+End);

                        if (End.equalsIgnoreCase("0")) {

                            int filesize = Integer.parseInt(FileSize);
                            MyLog.d(TAG, "VideoDownloadProgress: filesize: "+filesize +" downloa_progress: "+downloa_progress);
                            if (filesize > downloa_progress) {

                                SendMessageEvent event = new SendMessageEvent();
                                JSONObject eventObj = new JSONObject();
                                eventObj.put("MsgId", Msgid);

                                eventObj.put("DocId", Docid);
                                eventObj.put("ImageName", IMagename);
                                eventObj.put("LocalPath", LocalPath);
                                eventObj.put("LocalFileName", LocalFilename);
                                eventObj.put("start", downloa_progress);
                                eventObj.put("filesize", FileSize);
                                eventObj.put("bytesRead", Bytesread);

                                event.setEventName(SocketManager.EVENT_FILE_DOWNLOAD);
                                event.setMessageObject(eventObj);
                                eventBus.post(event);

                                MyLog.e(TAG, "Download Sending Object2 FileDownloadManager>>" + "" + eventObj);
                            }


                        } else if (End.equalsIgnoreCase("1")) {

                            MyLog.e(TAG, "End=1 Download File Completed");
                        }


                    } else {

                        MyLog.d(TAG, ">>>>>>>>>>>> VideoDownloadProgress: else  not byte[] ");
                        String bufferdata = String.valueOf(object.get("bufferData"));

                        String source = "password";
                        byte[] byteArray = new byte[0];
                        byteArray = bufferdata.getBytes(StandardCharsets.UTF_16);
                         /*   //System.out.println(new String(Base64.decode(Base64.encode(byteArray,
                                    Base64.DEFAULT), Base64.DEFAULT)));

                            Base64.encode(byteArray,
                                    Base64.DEFAULT);*/


                        //byte[] buffer = (byte[]) object.get("bufferData");

                        int len = byteArray.length;

                        int download_start = AppUtils.parseInt(Start);

                        int downloa_progres = download_start + len;

                        MyLog.e(TAG, ">>>>>>>>>>>> Download Server Progress" + "" + downloa_progres);

                        if (End.equalsIgnoreCase("0")) {

                            int filesize = Integer.parseInt(FileSize);
                            MyLog.d(TAG, ">>>>>>>>>>>> VideoDownloadProgress: filesize: "+filesize +" downloa_progres: "+downloa_progres);
                            if (filesize > downloa_progres) {
                                MyLog.d(TAG, ">>>>>>>>>>>> VideoDownloadProgress: again requested");
                                SendMessageEvent event = new SendMessageEvent();
                                JSONObject eventObj = new JSONObject();
                                eventObj.put("MsgId", Msgid);

                                eventObj.put("DocId", Docid);
                                eventObj.put("ImageName", IMagename);
                                eventObj.put("LocalPath", LocalPath);
                                eventObj.put("LocalFileName", LocalFilename);
                                eventObj.put("start", downloa_progres);
                                eventObj.put("filesize", FileSize);
                                eventObj.put("bytesRead", Bytesread);

                                event.setEventName(SocketManager.EVENT_FILE_DOWNLOAD);
                                event.setMessageObject(eventObj);
                                eventBus.post(event);

                                MyLog.e(TAG, ">>>>>>>>>>>> Download Sending Object3 FileDownloadManager>>" + "" + eventObj);
                            } else {
                                MyLog.d(TAG, ">>>>>>>>>>>> VideoDownloadProgress: finished");
                                String secretType = object.optString("secret_type");
                                boolean isSecretChat = false;
                                if (secretType != null && secretType.equalsIgnoreCase("yes")) {
                                    isSecretChat = true;
                                }

                                String docId = object.getString("DocId");
                                fileUploadDownloadManager.removeDownloadProgress(Msgid);
                                if (isSecretChat) {
                                    SecretChatViewActivity activity = (SecretChatViewActivity) mContext;
                                    activity.VideoDownloadComplete(docId, Msgid, LocalPath);
                                } else {
                                    ChatPageActivity activity = (ChatPageActivity) mContext;
                                    activity.VideoDownloadComplete(docId, Msgid, LocalPath);
                                }

                            }


                        } else if (End.equalsIgnoreCase("1")) {

                            MyLog.e(TAG, ">>>>>>>>>>>> Download File Completed");
                        }

                    }


                } else {
                    SendMessageEvent event = new SendMessageEvent();
                    JSONObject eventObj = new JSONObject();
                    eventObj.put("MsgId", Msgid);

                    eventObj.put("DocId", Docid);
                    eventObj.put("ImageName", IMagename);
                    eventObj.put("LocalPath", LocalPath);
                    eventObj.put("LocalFileName", LocalFilename);
                    eventObj.put("start", 0);
                    eventObj.put("filesize", FileSize);
                    eventObj.put("bytesRead", Bytesread);

                    event.setEventName(SocketManager.EVENT_FILE_DOWNLOAD);
                    event.setMessageObject(eventObj);
                    eventBus.post(event);


                    MyLog.e(TAG, "Download First Sending Object4 FileDownloadManager>>" + "" + eventObj);

                }

            } else {
                MyLog.e(TAG, "Download First Server Sending Pause");
            }

        } catch (Exception e) {
            MyLog.e(TAG, "VideoDownloadProgress: ",e );
        }

    }

}
