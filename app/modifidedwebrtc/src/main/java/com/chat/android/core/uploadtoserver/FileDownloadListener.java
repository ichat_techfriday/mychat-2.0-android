package com.chat.android.core.uploadtoserver;

public interface FileDownloadListener {

void downloadCompleted(String msgId, String path);
void progress(int progress, String msgId);
    void DownloadError(int progress, String msgId);

}
