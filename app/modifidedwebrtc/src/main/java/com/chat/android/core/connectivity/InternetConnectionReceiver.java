package com.chat.android.core.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.socket.MessageService;
import com.chat.android.core.socket.SocketManager;

/**
 */
public class InternetConnectionReceiver extends BroadcastReceiver {

    public static String TAG = InternetConnectionReceiver.class.getSimpleName();



    @Override
    public void onReceive(Context context, Intent intent) {
        if (AppUtils.isNetworkAvailable(context)) {
            MyLog.e(TAG, "Connected to a network");
            if(!AppUtils.isMyServiceRunning(context,MessageService.class)){
                AppUtils.startService(context,MessageService.class);
            } else if(!SocketManager.getInstance().isConnected()){
                SocketManager.getInstance().connect();
            }
        }
        else{
            MyLog.e(TAG, "disConnected to a network");
        }
    }


}
