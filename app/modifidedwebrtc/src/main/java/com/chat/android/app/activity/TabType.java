package com.chat.android.app.activity;

/**
 * Created by sonu on 12/06/17.
 */

public enum TabType {
    DEFAULT, ICON_TEXT,ICONS_ONLY,CUSTOM
}
