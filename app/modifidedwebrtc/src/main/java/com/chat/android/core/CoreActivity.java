package com.chat.android.core;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;

import androidx.appcompat.app.ActionBar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.chat.android.R;
import com.chat.android.app.activity.GroupInfo;
import com.chat.android.app.activity.LoginAuthenticationActivity;
import com.chat.android.app.activity.SecretChatList;
import com.chat.android.app.activity.SecretChatViewActivity;
import com.chat.android.app.activity.UserInfo;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.DeviceLockUtil;
import com.chat.android.app.utils.LoginAuthUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.message.ChangeSetController;
import com.chat.android.core.service.Constants;
import com.chat.android.core.service.ContactsSync;
import com.chat.android.core.socket.MessageService;
import com.chat.android.core.socket.SocketManager;

import java.util.List;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * * Created by  CASPERON TECH on 10/5/2016.
 */
public class CoreActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;

    private SessionManager sessionManager;
    private String mCurrentUserId;
    private boolean isValidDevice, isLoginKeySent;
    private Handler statusHandler;
    private Runnable statusRunnable;
    private long userLeaveTime;
    private static final String TAG = "CoreActivity";
    private String uniqueCurrentID = "";
    private static Context mContext;
    private static ProgressDialog pDialog;
    @Override
    public void onStart() {
        super.onStart();
        sessionManager = SessionManager.getInstance(CoreActivity.this);
        mCurrentUserId = sessionManager.getCurrentUserID();
        isValidDevice = sessionManager.isValidDevice();
        isLoginKeySent = sessionManager.isLoginKeySent();
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        mContext = CoreActivity.this;



    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        userLeaveTime = System.currentTimeMillis();
        Constants.isUserLeave = true;
        MyLog.d(TAG, "onUserLeaveHint: ");


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        /*Locale newLocale = null;
        // .. create or get your new Locale object here.
        String mLanguage = SharedPreference.getInstance().getValue(newBase, "lan");
        String mLanguageContry = SharedPreference.getInstance().getValue(newBase, "lancontry");

        if (mLanguage == null) {
            mLanguage = "en";
        }
        if (mLanguageContry != null) {
            newLocale = new Locale(mLanguage, mLanguageContry);

        } else {
            newLocale = new Locale(mLanguage);

        }
        //Log.e("CoreActivity", "mLanguage" + mLanguage);
        Context context = ContextWrapper.wrap(newBase, newLocale);*/
        super.attachBaseContext(newBase);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);
        /*String mLanguage = SharedPreference.getInstance().getValue(mContext, "lan");
        if (mLanguage == null) {
            mLanguage = "en";
        }
        Log.e("CoreActivity", "mLanguage" + mLanguage);
        Locale myLocale = new Locale(mLanguage);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);*/
    }

    @Override
    public void onStop() {
        super.onStop();
        hidepDialog();
        statusHandler = new Handler();
        statusRunnable = new Runnable() {
            @Override
            public void run() {
                if (!sessionManager.isScreenActivated()) {
                    ChangeSetController.setChangeStatus("0");
                }
            }
        };
        statusHandler.postDelayed(statusRunnable, 2000);


        if (Constants.isUserLeave) {
            //JobAlarmSingle.scheduleJob();
            long defTime = System.currentTimeMillis() - userLeaveTime;
            MyLog.d(TAG, "onStop: difference time " + defTime);
            if (defTime < 200) {
                MyLog.d(TAG, "onStop: recent apps pressed");
                Constants.isRecentClicked = true;
                Constants.recentAppsClickTime = System.currentTimeMillis();
            } else {
                MyLog.d(TAG, "onStop: home button pressed");
            }
            //DemoSyncJob.scheduleJob();
            //Check background service is running
          /*  if (!AppUtils.isMyServiceRunning(this, MessageService.class)) {
                Log.e("CoreActivity", "isMyServiceRunning started");
                DemoSyncJob.scheduleJob();
                AppUtils.startService(this,MessageService.class);
            } else {
                Log.e("CoreActivity", "isMyServiceRunning running already");
            }*/
        }
    }


    public void initProgress(String message, boolean cancelable) {
        progressDialog = getProgressDialogInstance();
        progressDialog.setMessage(message);
        progressDialog.setCancelable(cancelable);
    }

    public ProgressDialog getProgressDialogInstance() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminateDrawable(ContextCompat.getDrawable(this, R.drawable.color_primary_progress_dialog));
        dialog.setIndeterminate(true);
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return dialog;
    }

    public void showKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(
                    getCurrentFocus().getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public void hideKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void showProgressDialog() {
        try {
            if (progressDialog != null && !progressDialog.isShowing() && !isFinishing())
                progressDialog.show();
        } catch (Exception e) {
            MyLog.e(TAG, "showProgressDialog: ", e);
        }
    }

    public void showProgressDialog(Activity mActivity) {
        try {

            if (progressDialog != null && !progressDialog.isShowing() && !isFinishing())
                if (!mActivity.isFinishing()) {
                    progressDialog.show();
                }
        } catch (Exception e) {
            MyLog.e(TAG, "showProgressDialog: ", e);
        }
    }
    public void hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing() && !isFinishing())
                progressDialog.dismiss();
        } catch (Exception e) {
            MyLog.e(TAG, "hideProgressDialog: ", e);
        }
    }

    public static void showProgres() {
        hidepDialog();
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage(mContext.getString(R.string.please_wait_msg));
        //pDialog.setCancelable(false);

        showpDialog();


    }

    public static void showpDialog() {
        if (!pDialog.isShowing() && pDialog != null) {
            pDialog.show();
        }
    }

    public static void hidepDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Your own code to create the view
        // ...
        MyLog.d(TAG, "onCreate: ");
        Constants.isUserLeave = false;
        Constants.isRecentClicked = false;
        Constants.recentAppsClickTime = 0;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        checkForUpdates();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // ... your own onResume implementation
        checkForCrashes();
        /*if(com.chat.android.HandyScript.FCM.MyFirebaseMessagingService.mSocketManager!=null){
            com.chat.android.HandyScript.FCM.MyFirebaseMessagingService.mSocketManager.disconnect();
        }*/
//Valid device is always false
        if (isAppRunning() && mCurrentUserId != null && !mCurrentUserId.equals("")/* && isValidDevice*/) {
            // Maintain online status
            sessionManager.setIsScreenActivated(true);

            ChangeSetController.setChangeStatus("1");

            if (!ContactsSync.isStarted) {
/*                Intent contactIntent = new Intent(CoreActivity.this, ContactsSync.class);
                startService(contactIntent);*/
                //ScimboContactsService.startContactService(this, true);

                       AppUtils.startService(this, ContactsSync.class);

            }

            //   if (!MessageService.isStarted() ) {
       /*     if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                // only for gingerbread and newer versions
                if (!AppUtils.isServiceRunning(mContext, MessageService.class)) {
                    Intent msgSvcIntent = new Intent(CoreActivity.this, MessageService.class);
                    startService(msgSvcIntent);
                }
                if (!AppUtils.isServiceRunning(mContext, TemporaryForegroundService.class)) {
                    Intent msgSvcIntent = new Intent(CoreActivity.this, TemporaryForegroundService.class);
                    startService(msgSvcIntent);
                }
            }*/
            if (!AppUtils.isServiceRunning(mContext, MessageService.class)) {
                AppUtils.startService(mContext,MessageService.class);
            }

            long currentMillis= System.currentTimeMillis();
            long diffMillis= currentMillis -userLeaveTime;
            boolean isDeviceLocked=SessionManager.getInstance(this).isDeviceLocked();
            boolean isDeviceLockEnaled=SessionManager.getInstance(this).isDeviceLockEnabled();
           if(!Constants.IS_FROM_THIRD_PARTY_APP && !Constants.IS_FROM_SHARING_PAGE  && !Constants.IS_FROM_PASSWORD_PAGE) {
              // if (isDeviceLocked || (userLeaveTime > 0 && diffMillis > 60000)) {
               long deviceLockSettingsDuration= DeviceLockUtil.getDeviceLockDuration(this);
               if(userLeaveTime>0 && diffMillis>deviceLockSettingsDuration) {
                   if (isDeviceLockEnaled  && LoginAuthUtils.isDeviceHasLock(this)) {
                       Intent intent = new Intent(this, LoginAuthenticationActivity.class);
                       startActivity(intent);
                       SessionManager.getInstance(this).setIsDeviceLocked(false);
                       finishAffinity();
                   }
               }
           }
            SessionManager.getInstance(this).setIsDeviceLocked(false);
           Constants.IS_FROM_PASSWORD_PAGE=false;
           Constants.IS_FROM_THIRD_PARTY_APP=false;
        }
        String className= getClass().getName();
        Log.d(TAG, "onResume: "+className);
        if(getSupportActionBar()!=null && !className.contains(UserInfo.class.getName())
                &&!className.contains(GroupInfo.class.getName())
                &&!className.contains(SecretChatList.class.getName())
                &&!className.contains(SecretChatViewActivity.class.getName()))
            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.toolbar_gradient));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterManagers();

        sessionManager.setIsScreenActivated(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
      //  Runtime.getRuntime().gc();
        if (!isAppRunning()) {
            if (statusHandler != null) {
                statusHandler.removeCallbacks(statusRunnable);
            }
            ChangeSetController.setChangeStatus("0");
            /*//Start our background service and check message service not running
            if (!AppUtils.isServiceRunning(CoreActivity.this, MessageService.class)){

            }*/
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Constants.IS_FROM_THIRD_PARTY_APP=true;
    }

    private void checkForCrashes() {
        //CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        //UpdateManager.register(this);
    }

    private void unregisterManagers() {
        //UpdateManager.unregister();
    }

    private boolean isAppRunning() {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;
        }

        return false;
    }

    public boolean checkAudioRecordPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }
}
