package com.chat.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.chat.android.app.utils.MyLog;
import com.chat.android.fcm.Config;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.chat.android.fcm.AppConfig.REG_ID;

/**
 * Created by user145 on 8/4/2017.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    public static String token;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        MyLog.d(TAG, "onTokenRefresh: "+refreshedToken);

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);
        token=refreshedToken;
        storeToken(this,token);
        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }
    private void sendRegistrationToServer(final String token) {

        Log.e(TAG, ": " + token);

    }
    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.apply();
    }

    private void storeToken(Context context, String regId) {
        final SharedPreferences prefs = context.getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(REG_ID, regId);
        editor.apply();
    }

    public static String getFCMToken(Context context){
        final SharedPreferences prefs = context.getSharedPreferences(TAG,Context.MODE_PRIVATE);
        String fcmToken= prefs.getString(REG_ID, "");
        MyLog.d(TAG, "getFCMToken: "+fcmToken);
        return fcmToken;
    }
}
