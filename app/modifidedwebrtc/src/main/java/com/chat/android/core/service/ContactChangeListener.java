package com.chat.android.core.service;

import android.net.Uri;

public interface ContactChangeListener {


    void onContactChange(Uri uri);
}
