package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chat.android.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredLocationREceived extends RecyclerView.ViewHolder implements OnMapReadyCallback {

    public TextView senderName, time, fromname, toname, datelbl;
    public ImageView imageViewindicatior, starredindicator_below,userprofile;
    public ImageView ivMap;
    public GoogleMap mMap;
    public String nameSelected = "";
    public LatLng positionSelected = new LatLng(0, 0);

    public VHStarredLocationREceived(View view) {
        super(view);
        senderName = view.findViewById(R.id.lblMsgFrom);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        time = view.findViewById(R.id.ts);
        ivMap = view.findViewById(R.id.ivMap);
        userprofile= view.findViewById(R.id.userprofile);
        imageViewindicatior = view.findViewById(R.id.imageView);
        starredindicator_below = view.findViewById(R.id.starredindicator_below);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.addMarker(new MarkerOptions().position(positionSelected).title(nameSelected));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionSelected, 16.0f));
    }
}
