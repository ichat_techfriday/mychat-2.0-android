package com.chat.android.core.model;

/**
 * Created by CAS60 on 9/18/2017.
 */
public class ScimboSettingsModel {

    private String title;
    private int resourceId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }
}
