package com.chat.android.app.encryption;

import com.blakequ.rsa.FileEncryptionManager;
import com.chat.android.app.utils.MyLog;
import com.google.android.gms.common.util.Base64Utils;



public class RSAUtil {
    private FileEncryptionManager mFileEncryptionManager = FileEncryptionManager.getInstance();
    private static final String TAG = "RSAUtil";

    public void init(String publicKey, String privateKey) {
        try {
            publicKey = publicKey.replace(
                    "-----BEGIN PUBLIC KEY-----\n", "")
                    .replace("-----END PUBLIC KEY-----\n", "").replace("\n", "")
                .trim();

            privateKey = privateKey.replace(
                    "-----BEGIN RSA PRIVATE KEY-----\n", "")
                    .replace("-----END RSA PRIVATE KEY-----", "").replace("\n", "")
                .trim();


            mFileEncryptionManager.setRSAKey(publicKey, privateKey, true);
        } catch (Exception e) {
            MyLog.e(TAG, "init: ", e);
        }
    }

    public String getEncryptedMsg(String plainText) {
        try {
            byte[] encryptByte = mFileEncryptionManager.encryptByPublicKey(plainText.getBytes());
            return Base64Utils.encode(encryptByte);
        } catch (Exception e) {
            MyLog.e(TAG, "getEncrypteMsg: ", e);
            return plainText;
        }
    }


    public String getDecryptedMsg(String encryptedString) {
        try {

            byte[] decryptByte = mFileEncryptionManager.decryptByPrivateKey(Base64Utils.decode(encryptedString));
            return new String(decryptByte);
        } catch (Exception e) {
            MyLog.e(TAG, "getEncrypteMsg: ", e);
            return encryptedString;
        }
    }


}
