package com.chat.android.app.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chat.android.R;
import com.chat.android.app.model.NewGroupDetails_Model;
import com.chat.android.app.model.NewGroup_DB;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.TimeStampUtils;
import com.chat.android.app.widget.AvnNextLTProDemiButton;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.app.widget.CustomDemiTextView;
import com.chat.android.app.widget.CustomRegTextView;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.scimbohelperclass.ScimboUtilities;
import com.chat.android.core.service.Constants;
import com.chat.android.core.socket.MessageService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//Only for RAAD
public class NewGroupListAdapter extends RecyclerView.Adapter<NewGroupListAdapter.MyVieeHolder> {

    private Activity context;
    private List<NewGroupDetails_Model> modelList;
    private static final String TAG = NewGroupListAdapter.class.getSimpleName()+">>";
    private String mCurrentUserId;
    private SessionManager sessionManager;


    public NewGroupListAdapter(Activity context, List<NewGroupDetails_Model> modelList) {
        this.context = context;
        this.modelList = modelList;
    }

    @Override
    public MyVieeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyLog.d(TAG, "onCreateViewHolder: ");
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_newgroup_list, parent, false);

        return new MyVieeHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyVieeHolder holder, int position) {

        MyLog.d(TAG, "onBindViewHolder: "+position);
        NewGroupDetails_Model model = modelList.get(position);
        String path = model.getProfilePic();
        if (path != null && !path.equals("")) {
            if (!path.startsWith(Constants.SOCKET_IP)) {
                path = Constants.SOCKET_IP + path;
            }
            Glide.with(context).load(path)
                    .into(holder.group_dp);
        } else {
            Glide.with(context).load(R.mipmap.group_chat_attachment_profile_icon)
                    .into(holder.group_dp);
        }
        holder.group_name.setText(model.getGroupName());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        Date date2 = new Date();
        String current_date = sdf.format(date2);
        current_date = current_date.substring(0, 10);

        if(model.getTimestamp() != null) {
            String deliveryTime = TimeStampUtils.getServerTimeStamp(context,
                    Long.parseLong(model.getTimestamp()));

            long l = Long.parseLong(deliveryTime);
            Date d = new Date(l);
            String date = d.toString();
            String d1 = sdf.format(d);
            String mytime = ScimboUtilities.convert24to12hourformat(date.substring(11, 19));
            d1 = d1.substring(0, 10);
            if (current_date.equals(d1)) {
                holder.time.setText(mytime);
            } else {
                String[] separated = d1.substring(0, 10).split("-");
                String d6 = separated[2] + "-" + separated[1] + "-" + separated[0];
                String finaldate = d6 + "" + "," + mytime;
                holder.time.setText(finaldate);
            }
        }
        else {
            holder.time.setText("");
        }

    }

    public void updateInfo(List<NewGroupDetails_Model> aitem) {
        this.modelList = aitem;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class MyVieeHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        CircleImageView group_dp;
        public CustomDemiTextView group_name;
        public CustomRegTextView time;
        AvnNextLTProDemiButton btn_accept, btn_deny;


        public MyVieeHolder(View itemView) {
            super(itemView);
            group_dp = itemView.findViewById(R.id.group_photo);
            group_name  = itemView.findViewById(R.id.group_name);
            time = itemView.findViewById(R.id.time);
            btn_accept = itemView.findViewById(R.id.btnAccept);
            btn_deny = itemView.findViewById(R.id.btnDeny);
            btn_accept.setOnClickListener(this);
            btn_deny.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if(view == btn_accept) {
                if(AppUtils.isNetworkAvailable(context)) {
                    int i = getAdapterPosition();
                    NewGroupDetails_Model modelTest = modelList.get(getAdapterPosition());
                    //NewGroup_DB db = CoreController.getmNewGroup_db(context);

                    MessageService.service.addNewGroupInDb(modelTest);
                    //db.deleteGroupData(modelTest.getId());
                    // context.startActivity(new Intent(context, NewHomeScreenActivty.class));
                    context.finish();
                }
                else{
                    Toast.makeText(context,context.getString(R.string.no_internet_connection),Toast.LENGTH_SHORT).show();
                }
            }
            if(view == btn_deny) {
                if(AppUtils.isNetworkAvailable(context)) {
                    NewGroupDetails_Model modelTest = modelList.get(getAdapterPosition());
                    NewGroup_DB db = CoreController.getmNewGroup_db(context);
                    //if(MessageService.service!=null)
                    // MessageService.service.createGroup(modelTest.getGroupId());
                    sessionManager = SessionManager.getInstance(context);
                    mCurrentUserId = sessionManager.getCurrentUserID();

                    MessageService.service.denyGroup(modelTest.getGroupId());
                    //db.deleteGroupData(modelTest.getId());
                    //context.startActivity(new Intent(context, NewHomeScreenActivty.class));
                    context.finish();
                }
                else{
                    Toast.makeText(context,context.getString(R.string.no_internet_connection),Toast.LENGTH_SHORT).show();
                }
            }
            else {

            }
        }
    }



}
