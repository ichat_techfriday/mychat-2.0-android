package com.chat.android.core.message;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Base64;

import com.chat.android.app.model.CreatePost_Model;
import com.chat.android.app.utils.GroupInfoSession;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.model.GroupInfoPojo;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.core.uploadtoserver.FileUploadDownloadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 */
public class VideoMessage extends BaseMessage implements Message {

    private Context context;
    private static final String TAG = "VideoMessage";

    public VideoMessage(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public Object getMessageObject(String to, String payload, Boolean isSecretChat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.picture);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);

            if (isSecretChat) {
//                setId(getId() + "-secret");
                object.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);
            }
        } catch (Exception e) {
            MyLog.e(TAG, "getMessageObject: ",e );
        }
        return object;
    }

    @Override
    public Object getGroupMessageObject(String to, String payload, String groupName) {
        this.to = to;
        setId(from + "-" + to + "-g");
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.picture);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
            object.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
            object.put("userName", groupName);
        } catch (Exception e) {
            MyLog.e(TAG, "getGroupMessageObject: ",e );
        }
        return object;
    }

    public MessageItemChat createMessageItem(boolean isSelf, String videoPath, String status,
                                             String receiverUid, String senderName, String caption) {
        item = new MessageItemChat();
        item.setMessageId(getId() + "-" + tsForServerEpoch);
        item.setIsSelf(isSelf);
        item.setVideoPath(videoPath);
        item.setChatFileLocalPath(videoPath);
        item.setDeliveryStatus(status);
        item.setReceiverUid(receiverUid);
        item.setMessageType("" + type);
        item.setReceiverID(to);
        item.setTextMessage(caption);
        item.setSenderName(senderName);
        item.setTS(getShortTimeFormat());
        item.setFileBufferAt(0);
        item.setDownloadStatus(MessageFactory.DOWNLOAD_STATUS_COMPLETED);

        if (getId().contains("-g")) {
            GroupInfoSession groupInfoSession = new GroupInfoSession(context);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(getId());

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG,"",e);
                }
            }
        }

        try {
            File file = new File(videoPath);
            byte[] bytesArray = new byte[(int) file.length()];
            item.setFileSize(String.valueOf(bytesArray.length));
        } catch (Exception e) {
            MyLog.e(TAG, "createMessageItem: ",e );
        }

        return item;
    }

    public Object createVideoUploadObject(String msgId, String docId, String videoName, String videoPath,
                                          String receiverName, String caption, String chatType, boolean isSecretChat) {
        JSONObject uploadObj = new JSONObject();

        try {
            uploadObj.put("err", 0);
            uploadObj.put("ImageName", videoName);
            uploadObj.put("id", msgId);
            uploadObj.put("from", docId.split("-")[0]);
            uploadObj.put("to", docId.split("-")[1]);
            uploadObj.put("toDocId", msgId);
            uploadObj.put("docId", docId);
            uploadObj.put("bufferAt", -1); // for get first index is 0.
            uploadObj.put("LocalPath", videoPath);
            uploadObj.put("type", MessageFactory.video);
            uploadObj.put("ReceiverName", receiverName);
            uploadObj.put("mode", "phone");
            uploadObj.put("payload", caption);
            uploadObj.put("chat_type", chatType);
            uploadObj.put("start", 0);
            uploadObj.put("FileEnd",0);

            if (isSecretChat) {
                uploadObj.put("secret_type", "yes");
            } else {
                uploadObj.put("secret_type", "no");
            }

            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
            mdr.setDataSource(videoPath);
            int videoHeight = Integer.parseInt(mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
            int videoWidth = Integer.parseInt(mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

            Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            thumbBmp.compress(Bitmap.CompressFormat.JPEG, 35, out);
            byte[] thumbArray = out.toByteArray();
            try {
                out.close();
            } catch (Exception e) {
                MyLog.e(TAG, "createVideoUploadObject: ",e );
            }
            String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
            if (!thumbData.startsWith("data:image/jpeg;base64,")) {
                thumbData = "data:image/jpeg;base64," + thumbData;
            }

            File videoFile = new File(videoPath);
            uploadObj.put("size", videoFile.length());
            uploadObj.put("original_filename", videoFile.getName());
            uploadObj.put("thumbnail_data", thumbData);
            uploadObj.put("height", videoHeight);
            uploadObj.put("width", videoWidth);
            uploadObj.put("Duration", duration);

            FileUploadDownloadManager uploadDownloadManager = new FileUploadDownloadManager(context);
            uploadDownloadManager.setUploadProgress(item.getMessageId(), 0, uploadObj);
        } catch (Exception e) {
            MyLog.e(TAG, "createVideoUploadObject: ",e );
        }

        return uploadObj;
    }



    public Object createStatusVideoUploadObject(String msgId, String docId,String videoName, String fileExtension, String videoPath,
                                          String receiverName, String caption, String chatType) {
        JSONObject uploadObj = new JSONObject();

        try {

            uploadObj.put("err", 0);
            uploadObj.put("ImageName", videoName+"-"+tsForServerEpoch+fileExtension);
            uploadObj.put("id", docId);
            uploadObj.put("from", from);

            uploadObj.put("toDocId", docId);
            uploadObj.put("docId", docId);
            uploadObj.put("bufferAt", -1); // for get first index is 0.
            uploadObj.put("LocalPath", videoPath);
            uploadObj.put("type", MessageFactory.video);
            uploadObj.put("ReceiverName", receiverName);
            uploadObj.put("mode", "phone");
            uploadObj.put("payload", caption);
            uploadObj.put("chat_type", chatType);



            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
            mdr.setDataSource(videoPath);
            int videoHeight = Integer.parseInt(mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
            int videoWidth = Integer.parseInt(mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

            Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            thumbBmp.compress(Bitmap.CompressFormat.JPEG, 50, out);
            byte[] thumbArray = out.toByteArray();
            try {
                out.close();
            } catch (IOException e) {
                MyLog.e(TAG, "createStatusVideoUploadObject: ",e );
            }
            String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
            if (!thumbData.startsWith("data:image/jpeg;base64,")) {
                thumbData = "data:image/jpeg;base64," + thumbData;
            }

            File videoFile = new File(videoPath);
            uploadObj.put("size", videoFile.length());
            uploadObj.put("original_filename", videoFile.getName());
            uploadObj.put("thumbnail_data", thumbData);
            uploadObj.put("height", videoHeight);
            uploadObj.put("width", videoWidth);
            uploadObj.put("Duration", duration);
            uploadObj.put("uploadType", "status");
            uploadObj.put("FileEnd",0);
            uploadObj.put("start",0);

            FileUploadDownloadManager uploadDownloadManager = new FileUploadDownloadManager(context);
            uploadDownloadManager.setUploadProgress(item.getMessageId(), 0, uploadObj);
        } catch (Exception e) {
            MyLog.e(TAG, "createStatusVideoUploadObject: ",e );
        }

        return uploadObj;
    }

    public Object getGroupVideoMessageObject(String msgId, int fileSize, String thumbData, int videoWidth, int videoHeight,
                                             String duration, String serverFilePath, String groupName, String caption) {

        JSONObject msgObj = new JSONObject();
        try {
            String[] splitIds = msgId.split("-");
            String fromUserId = splitIds[0];
            String toUserId = splitIds[1];
            String picMsgId;
            if (msgId.contains("-g")) {
                picMsgId = splitIds[3];
            } else {
                picMsgId = splitIds[2];
            }
            msgObj.put("from", fromUserId);
            msgObj.put("to", toUserId);
            msgObj.put("type", MessageFactory.video);
            msgObj.put("payload", caption);

            msgObj.put("id", picMsgId);
            msgObj.put("toDocId", msgId);
            msgObj.put("filesize", fileSize);
            msgObj.put("width", videoWidth);
            msgObj.put("height", videoHeight);
            msgObj.put("thumbnail", serverFilePath);
            msgObj.put("thumbnail_data", thumbData);
            msgObj.put("duration", duration);
            msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
            msgObj.put("userName", groupName);
        } catch (Exception ex) {
            MyLog.e(TAG, "getGroupVideoMessageObject: ",ex );
        }
        return msgObj;
    }

    public Object getVideoMessageObject(String msgId, int fileSize, String thumbData, int videoWidth, int videoHeight,
                                        String duration, String serverFilePath, String caption,
                                        boolean isSecretChat,boolean isStatus,String statusDocId) {
        JSONObject msgObj = new JSONObject();
        try {
            String[] splitIds = msgId.split("-");
            String fromUserId = splitIds[0];
            String toUserId = splitIds[1];
            String picMsgId;
            if (msgId.contains("-g")) {
                picMsgId = splitIds[3];
            } else {
                try {
                    picMsgId = splitIds[2];
                }
                catch (Exception e){
                    //for status upload it only have xxxxxxxx-yyyy
                    picMsgId=splitIds[1];
                }
            }

            if(isStatus){
/*                //sample 343434343--34343
                if(msgId.contains("--")){
                    msgId= msgId.replace("--","-");
                }*/
                msgId= statusDocId;
            }
            msgObj.put("from", fromUserId);
            msgObj.put("to", toUserId);
            msgObj.put("type", MessageFactory.video);
            msgObj.put("payload", caption);
            msgObj.put("id", picMsgId);
            msgObj.put("toDocId", msgId);
            msgObj.put("filesize", fileSize);
            msgObj.put("width", videoWidth);
            msgObj.put("height", videoHeight);
            msgObj.put("thumbnail", serverFilePath);
            msgObj.put("thumbnail_data", thumbData);
            msgObj.put("duration", duration);

            if (isSecretChat) {
                msgObj.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);
            }

        } catch (Exception ex) {
            MyLog.e(TAG, "getVideoMessageObject: ",ex);
        }
        return msgObj;
    }






}