package com.chat.android.video;

/**
 * Created by user134 on 7/16/2018.
 */

public class VideoCompressPojo {
    private boolean isCompressed;
    private String originalFIlePath;
    private String compressedFilePath;

    public boolean isCompressed() {
        return isCompressed;
    }

    public void setCompressed(boolean compressed) {
        isCompressed = compressed;
    }

    public String getOriginalFIlePath() {
        return originalFIlePath;
    }

    public void setOriginalFIlePath(String originalFIlePath) {
        this.originalFIlePath = originalFIlePath;
    }

    public String getCompressedFilePath() {
        return compressedFilePath;
    }

    public void setCompressedFilePath(String compressedFilePath) {
        this.compressedFilePath = compressedFilePath;
    }
}
