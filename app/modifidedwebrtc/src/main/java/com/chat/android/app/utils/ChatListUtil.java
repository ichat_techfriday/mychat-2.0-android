package com.chat.android.app.utils;

import android.app.Activity;
import android.util.Log;

import com.chat.android.app.activity.ForwardContact;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.GroupInfoPojo;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.ScimboContactModel;

import java.util.ArrayList;
import java.util.List;

public class ChatListUtil {
    private static final String TAG = "ChatListUtil";
public static ArrayList<ScimboContactModel> getGroupList(Activity activity){
    ArrayList<ScimboContactModel> grouplist = new ArrayList<>();
    MessageDbController db = CoreController.getDBInstance(activity);
    SessionManager sessionManager = SessionManager.getInstance(activity);
    Session session = new Session(activity);
    String mCurrentUserId=sessionManager.getCurrentUserID();
    GroupInfoSession groupInfoSession = new GroupInfoSession(activity);
    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(activity);
    Getcontactname getcontactname = new Getcontactname(activity);
    ArrayList<MessageItemChat> groupChats = db.selectChatList(MessageFactory.CHAT_TYPE_GROUP);
    List<String> lists = session.getBlockedIds();
    for (MessageItemChat msgItem : groupChats) {
        try {
//            getGroupDetails(msgItem.getConvId());
//            Log.d("getDataFromDB", msgItem.getGroupName());
            String groupId = msgItem.getReceiverID();
            String docIdd = mCurrentUserId + "-" + groupId + "-g";
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docIdd);


            if (lists != null && lists.size() != 0) {
                if (!lists.contains(msgItem.getReceiverID())) {
                    if (infoPojo.getGroupName() != null) {
                        StringBuilder sb = null;
                        ScimboContactModel vScimboContactModel = new ScimboContactModel();
                        String groupid = msgItem.getReceiverID().split("-")[0];
                        vScimboContactModel.set_id(groupid);
                        vScimboContactModel.setType(msgItem.getGroupEventType());
                        vScimboContactModel.setAvatarImageUrl(infoPojo.getAvatarPath());
                        vScimboContactModel.setFirstName(infoPojo.getGroupName());
                        String from = msgItem.getMessageId().split("-")[0];
                        String to = msgItem.getReceiverID();
                        String docId = from + "-" + to + "-g";
                        boolean hasGroupInfo = groupInfoSession.hasGroupInfo(docId);
                        if (hasGroupInfo) {
                            infoPojo = groupInfoSession.getGroupInfo(docId);
                            sb = new StringBuilder();
                            String memername = "";
                            if (infoPojo != null && infoPojo.getGroupMembers() != null) {
                                String[] contacts = infoPojo.getGroupMembers().split(",");

                                for (int i = 0; i < contacts.length; i++) {
                                    if (!contacts[i].equalsIgnoreCase(from)) {
                                        String msisdn = contactDB_sqlite.getSingleData(contacts[i], ContactDB_Sqlite.MSISDN);
                                        if (msisdn != null) {
                                            memername = getcontactname.getSendername(contacts[i], msisdn);
                                            sb.append(memername);
                                            if (contacts.length - 1 != i) {
                                                sb.append(", ");
                                            }
                                        }
                                    } else {
                                        memername = "You";
                                        sb.append(memername);
                                        if (contacts.length - 1 != i) {
                                            sb.append(", ");
                                        }
                                    }
                                }
                            }
                        }

                        vScimboContactModel.setStatus(String.valueOf(sb));
                        if(infoPojo!=null && infoPojo.isLiveGroup())
                        grouplist.add(vScimboContactModel);
                    }
                }
            } else {
                if (infoPojo.getGroupName() != null) {
                    StringBuilder sb = null;
                    ScimboContactModel vScimboContactModel = new ScimboContactModel();
                    String groupid = msgItem.getReceiverID().split("-")[0];
                    vScimboContactModel.set_id(groupid);
                    vScimboContactModel.setType("");
                    vScimboContactModel.setAvatarImageUrl(infoPojo.getAvatarPath());
                    vScimboContactModel.setFirstName(infoPojo.getGroupName());
                    vScimboContactModel.setGroup(true);
                    String from = msgItem.getMessageId().split("-")[0];
                    String to = msgItem.getReceiverID();
                    String docId = from + "-" + to + "-g";

                    boolean hasGroupInfo = groupInfoSession.hasGroupInfo(docId);

                    if (hasGroupInfo) {

                        infoPojo = groupInfoSession.getGroupInfo(docId);
                        sb = new StringBuilder();
                        String memername = "";
                        if (infoPojo != null && infoPojo.getGroupMembers() != null) {
                            String[] contacts = infoPojo.getGroupMembers().split(",");

                            for (int i = 0; i < contacts.length; i++) {
                                if (!contacts[i].equalsIgnoreCase(from)) {
                                    String msisdn = contactDB_sqlite.getSingleData(contacts[i], ContactDB_Sqlite.MSISDN);
                                    if (msisdn != null) {
                                        memername = getcontactname.getSendername(contacts[i], msisdn);
                                        sb.append(memername);
                                        if (contacts.length - 1 != i) {
                                            sb.append(", ");
                                        }
                                    }
                                } else {
                                    memername = "You";
                                    sb.append(memername);
                                    if (contacts.length - 1 != i) {
                                        sb.append(", ");
                                    }
                                }
                            }
                        }
                    }

                    vScimboContactModel.setStatus(String.valueOf(sb));
                    if(infoPojo!=null && infoPojo.isLiveGroup())
                    grouplist.add(vScimboContactModel);
                }
            }
        }catch (Exception e){
            Log.e(TAG, "getGroupList: ",e );
        }
    }
    return grouplist;
}
}
