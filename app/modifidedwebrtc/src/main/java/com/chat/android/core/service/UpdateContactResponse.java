package com.chat.android.core.service;

import android.content.Context;
import android.content.Intent;
import android.util.Base64;

import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.ScimboContactModel;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.scimbohelperclass.ScimboRegularExp;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static com.chat.android.app.activity.ScimboContactsService.KEY_REFRESH_COMPLETED;

public class UpdateContactResponse {
    private static final UpdateContactResponse ourInstance = new UpdateContactResponse();
    private static final String TAG = "UpdateContactResponse";
    public static List<ScimboContactModel> contactEntries = new ArrayList<>();
    private List<ScimboContactModel> contacts = new ArrayList<>();
    private List<ScimboContactModel> scimboEntries = new ArrayList<>();
    private List<ScimboContactModel> othercontacts = new ArrayList<>();
    private String uniqueCurrentID;
    private UserInfoSession userInfoSession;

    private UpdateContactResponse() {
    }

    public static UpdateContactResponse getInstance() {
        return ourInstance;
    }

    public void updateContact(ReceviceMessageEvent event, Context context) {

        MyLog.d(TAG, "setAdapter() ");
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        long savedRevision = SessionManager.getInstance(context).getContactSavedRevision();
        //for manual refresh --> update the old revision to new revision
       /* if (savedRevision >1) {
            MyLog.d(TAG, "setAdapter: revision now updated");
            CoreController.getContactSqliteDBintstance(this).updateRevision_when_contatct_delete(savedRevision, savedRevision - 1);
        }*/
        try {
            Object[] args = event.getObjectsArray();
            JSONObject socketData = new JSONObject(args[0].toString());
            String values = socketData.toString();
            uniqueCurrentID = SessionManager.getInstance(context).getCurrentUserID();
            contacts = new ArrayList<>();
            scimboEntries = new ArrayList<>();
            othercontacts = new ArrayList<>();
            userInfoSession = new UserInfoSession(context);
            JSONObject data = new JSONObject(values);

            // String status_= data.getString("status");
            //  Log.d(TAG, "updateContact: "+status_);
            if (data.has("Favorites")) {
                JSONArray array = data.getJSONArray("Favorites");


                // boolean contactUpdated = false;
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = new JSONObject(array.get(i).toString());
                    String securityCode = null;
                    if (AppUtils.isEncryptionEnabled(context)) {
                        if (obj.has("security_code")) {
                            securityCode = obj.getString("security_code");
                        }
                    }
                    String msisdn = obj.getString("msisdn");
                    String id = obj.getString("_id");
                    String countryCode = obj.getString("CountryCode");
                    String profilePic = obj.getString("ProfilePic");
                    String userStatus = obj.getString("Status");
                    String locPhNo = obj.getString("PhNumber");
                    JSONObject privacy = obj.getJSONObject("privacy");
                    String last_seen = privacy.getString("last_seen");
                    String profile = privacy.getString("profile_photo");
                    String status = privacy.getString("status");
                    String profile_photo_status = privacy.getString("profile_photo_status");

                    String local_phNo = obj.getString("contactPhno");
                    String contactName = obj.getString("contactName");

                    //  String last_seen_status = privacy.getString("last_seen_status");
                    //   String profile_status = privacy.getString("profile_status");

                    String mineContactStatus = "0";
                    if (privacy.has("contactUserList")) {
                        String toContactsList = privacy.getString("contactUserList");
                        if (toContactsList.contains(uniqueCurrentID)) {
                            mineContactStatus = "1";
                        }
                    }

                    String docId = uniqueCurrentID + "-" + id;

                    String normalConvId = userInfoSession.getChatConvId(docId);
                    if (normalConvId == null || normalConvId.equals("")) {
                        getConvId(id, "no");
                    }

                    docId = docId + "-secret";
                    String secretConvId = userInfoSession.getChatConvId(docId);
                    if (secretConvId == null || secretConvId.equals("")) {
                        getConvId(id, "yes");
                    }
                    int contactIndex = 0;
                    contactEntries.add(new ScimboContactModel());
//                for (int contactIndex = 0; contactIndex < contactEntries.size(); contactIndex++) {

  /*                  if (locPhNo.equalsIgnoreCase(contactEntries.get(contactIndex).getNumberInDevice())
                            || msisdn.equalsIgnoreCase(contactEntries.get(contactIndex).getNumberInDevice())
                            || msisdn.equalsIgnoreCase("+" + contactEntries.get(contactIndex).getNumberInDevice())
                            || contactEntries.get(contactIndex).getNumberInDevice().contains(local_phNo)
                            )

                    {
*/

                    try {
                        if (ScimboRegularExp.isEncodedBase64String(userStatus)) {
                            userStatus = new String(Base64.decode(userStatus, Base64.DEFAULT), StandardCharsets.UTF_8);
                        }
                    } catch (Exception e) {
                        MyLog.e(TAG, "setAdapter: ", e);
                    }

                    contactEntries.get(contactIndex).set_id(id);
                    contactEntries.get(contactIndex).setStatus(userStatus);
                    contactEntries.get(contactIndex).setCountryCode(countryCode);
                    contactEntries.get(contactIndex).setMsisdn(msisdn);
                    contactEntries.get(contactIndex).setAvatarImageUrl(profilePic);
                    contactEntries.get(contactIndex).setNumberInDevice(msisdn);
                    contactEntries.get(contactIndex).setFirstName(contactName);
                    contactEntries.get(contactIndex).setType("mobile");

                    //MyLog.d(TAG, "setAdapter: contact position start>:");
                    //new db sqlite
                    contactDB_sqlite.updateMyContactStatus(id, mineContactStatus);
                    contactDB_sqlite.updateLastSeenVisibility(id, getPrivacyStatus(last_seen));
                    contactDB_sqlite.updateProfilePicVisibility(id, getPrivacyStatus(profile));
                    contactDB_sqlite.updateProfileStatusVisibility(id, getPrivacyStatus(status));
                    if (AppUtils.isEncryptionEnabled(context)) {
                        if (obj.has("security_code")) {
                            contactDB_sqlite.updateSecurityToken_(id, securityCode);
                        }
                    }
                    //MyLog.d(TAG, "setAdapter: contact position end");

                    ScimboContactModel entry = contactEntries.get(contactIndex);
                    if (!id.equalsIgnoreCase(uniqueCurrentID)) {
                        scimboEntries.add(entry);


                        //new db sqlite
                        contactDB_sqlite.updateUserDetails(id, entry);
                        contactDB_sqlite.updateSavedRevision(id, savedRevision);

                        MyLog.d("Scimbo_contacts", entry.getFirstName() + "    ---" + id);
                    }


                    if (!profile.equalsIgnoreCase("nobody")) {
                        if (!profile_photo_status.equalsIgnoreCase("0")) {
                            contactEntries.get(contactIndex).setAvatarImageUrl(profilePic);
                        }
                    }
                    break;
                }
            }
            // }
            //  }

            SessionManager.getInstance(context).setContactSavedRevision(savedRevision);
            MyLog.d(TAG, String.valueOf(savedRevision));


            for (int i = 0; i < contactEntries.size(); i++) {
                if (contactEntries.get(i).get_id() == null) {
                    othercontacts.add(contactEntries.get(i));
                }
            }

            for (int i = 0; i < scimboEntries.size(); i++) {
                String a1 = scimboEntries.get(i).get_id();
                for (int j = i + 1; j < scimboEntries.size(); j++) {
                    String a2 = scimboEntries.get(j).get_id();
                    if (a1 != null && a2 != null && a1.equalsIgnoreCase(a2)) {
                        scimboEntries.remove(scimboEntries.get(j));
                    }
                }

            }
            for (int i = 0; i < othercontacts.size(); i++) {
                String a1 = othercontacts.get(i).getNumberInDevice();
                for (int j = i + 1; j < othercontacts.size(); j++) {
                    String a2 = othercontacts.get(j).getNumberInDevice();
                    if (a1 != null && a2 != null && a1.equalsIgnoreCase(a2)) {
                        othercontacts.remove(othercontacts.get(j));
                    }
                }
            }

            for (int i = 0; i < othercontacts.size(); i++) {
                String mCurrentUserMsisdn = SessionManager.getInstance(context).getPhoneNumberOfCurrentUser();
                if (othercontacts.get(i).getNumberInDevice().startsWith(SessionManager.getInstance(context).getCountryCodeOfCurrentUser())) {
                    if (othercontacts.get(i).getNumberInDevice().equalsIgnoreCase(mCurrentUserMsisdn)) {
                        othercontacts.remove(i);
                    }
                } else {
                    String myStrPhno = mCurrentUserMsisdn.replace(SessionManager.getInstance(context).getCountryCodeOfCurrentUser(), "");
                    if (othercontacts.get(i).getNumberInDevice().equalsIgnoreCase(myStrPhno)) {
                        othercontacts.remove(i);
                    }
                }

            }

            contacts.addAll(scimboEntries);
            contacts.addAll(othercontacts);

            sendContactBroadcast(true, context);


            MyLog.d(TAG, "setAdapter: end");

        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void sendContactBroadcast(boolean isRefreshed, Context context) {
        Intent broadcastIntent = new Intent("com.nowletschat.android.contact_refresh");
        broadcastIntent.putExtra(KEY_REFRESH_COMPLETED, isRefreshed);
        context.sendBroadcast(broadcastIntent);
    }

    private void getConvId(String receiverId, String secretType) {

        try {
            JSONObject obj = new JSONObject();
            obj.put("from", uniqueCurrentID);
            obj.put("to", receiverId);
            obj.put("secret_type", secretType);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GET_CONV_ID);
            event.setMessageObject(obj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private String getPrivacyStatus(String visibleTo) {
        switch (visibleTo.toLowerCase()) {

            case ContactDB_Sqlite.PRIVACY_TO_MY_CONTACTS:
                return ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS;

            case ContactDB_Sqlite.PRIVACY_TO_NOBODY:
                return ContactDB_Sqlite.PRIVACY_STATUS_NOBODY;

            default:
                return ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE;
        }
    }
}
