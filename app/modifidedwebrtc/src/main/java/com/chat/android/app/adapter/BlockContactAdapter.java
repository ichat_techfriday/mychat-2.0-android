package com.chat.android.app.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.widget.AvnNextLTProDemiTextView;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.model.BlockListPojo;

import java.util.ArrayList;

/**
 * Created by CAS63 on 3/7/2017.
 */
public class BlockContactAdapter extends RecyclerView.Adapter<BlockContactAdapter.MyViewHolder> {

    Activity context;
    private ArrayList<BlockListPojo> blockListPojos = new ArrayList<>();


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public AvnNextLTProDemiTextView uname, unumber;
        CircleImageView imageview;
        public ImageView ivSecretIcon;

        public MyViewHolder(View view) {
            super(view);
            uname = view.findViewById(R.id.BlockUserName);
            unumber = view.findViewById(R.id.blockUserNumber);
            imageview = view.findViewById(R.id.block_user_image);
            ivSecretIcon = view.findViewById(R.id.ivSecretIcon);
        }
    }

    public BlockContactAdapter(Activity context, ArrayList<BlockListPojo> blockListPojos) {
        super();
        this.context = context;
        this.blockListPojos = blockListPojos;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.block_contact_adapter, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BlockListPojo blockListPojo = blockListPojos.get(position);
        holder.uname.setText(blockListPojo.getName());
        holder.unumber.setText(blockListPojo.getNumber());
        String image = blockListPojo.getImagePath();

        if ((image != null && !image.equals(""))) {
            String path = AppUtils.getValidProfilePath(image);
        /*    Picasso.with(context).load(path).fit().error(
                    R.mipmap.chat_attachment_profile_default_image_frame)
                    .into(holder.imageview);
      */      Glide.with(context).load(path).error(
                    R.mipmap.chat_attachment_profile_default_image_frame).fitCenter()
                    .into(holder.imageview);
        } else {
         //   Picasso.with(context).load(R.mipmap.chat_attachment_profile_default_image_frame).into(holder.imageview);
            Glide.with(context).load(R.mipmap.chat_attachment_profile_default_image_frame)
                    .into(holder.imageview);
        }

        if (blockListPojo.isSecretChat()) {
            holder.ivSecretIcon.setVisibility(View.VISIBLE);
        } else {
            holder.ivSecretIcon.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return this.blockListPojos.size();
    }


}
