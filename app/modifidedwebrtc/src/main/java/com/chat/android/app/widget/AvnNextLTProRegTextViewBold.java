package com.chat.android.app.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.chat.android.core.CoreController;

/**
 * Created by user145 on 3/1/2018.
 */
public class AvnNextLTProRegTextViewBold extends CustomEmojiTextView {

    public AvnNextLTProRegTextViewBold(Context context) {
        super(context);
        init();
    }

    public AvnNextLTProRegTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AvnNextLTProRegTextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface face = CoreController.getInstance().getAvnNextLTProBoldTypeface();
        setTypeface(face);
        //setUseSystemDefault(false);
    }
}
