package com.chat.android.app.calls;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.utils.TimeStampUtils;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.CallItemChat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CAS60 on 7/31/2017.
 */
public class CallInfoAdapter extends RecyclerView.Adapter<CallInfoAdapter.VHCallInfo> {

    private Context mContext;
    private ArrayList<CallItemChat> callItemList;

    private Date todayDate, yesterdayDate;
    private String strCallType;
    private Boolean isArabic;

    public CallInfoAdapter(Context context, int callType, ArrayList<CallItemChat> callItemList) {
        this.mContext = context;
        this.callItemList = callItemList;
        isArabic = mContext.getResources().getBoolean(R.bool.is_arabic);
        todayDate = TimeStampUtils.getDateFormat(Calendar.getInstance().getTimeInMillis());
        yesterdayDate = TimeStampUtils.getYesterdayDate(todayDate);

        if(callType == MessageFactory.audio_call) {
            strCallType = context.getString(R.string.voice);
        } else {
            strCallType = context.getString(R.string.video);
        }
    }

    @Override
    public VHCallInfo onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_call_info, parent, false);
        return new VHCallInfo(view);
    }

    @Override
    public void onBindViewHolder(VHCallInfo holder, int position) {
        if (position == 0) {
            holder.tvDate.setVisibility(View.VISIBLE);
            holder.divider.setVisibility(View.VISIBLE);
        } else {
            holder.tvDate.setVisibility(View.GONE);
            holder.divider.setVisibility(View.GONE);
        }

        CallItemChat callItem = callItemList.get(position);

        if (!callItem.isSelf()) {
            String callStatus = callItem.getCallStatus();

            if (callStatus.equals(MessageFactory.CALL_STATUS_REJECTED + "")
                    || callStatus.equals(MessageFactory.CALL_STATUS_MISSED + "")) {
                holder.ivCallType.setBackgroundResource(R.drawable.ic_in_call_missed);
                String text = "";
                if (isArabic){
                    text = "مكالمة " + strCallType + " لم يرد عليها ...";
                } else {
                    text = " Missed " + strCallType + " call...";
                }
                holder.tvCallType.setText(text);
                holder.tvCallStatus.setVisibility(View.INVISIBLE);
            } else {
                holder.ivCallType.setBackgroundResource(R.drawable.ic_in_call_received);
//                String text = "Incoming " + strCallType + " call";
                String text = "";
                if (isArabic){
                    text = "مكالمة " + strCallType + " واردة ...";
                } else {
                    text = " Incoming " + strCallType + " call";
                }
                holder.tvCallType.setText(text);
                holder.tvCallStatus.setVisibility(View.VISIBLE);
                holder.tvCallStatus.setText(callItem.getCallDuration());
            }
        } else {
            holder.ivCallType.setBackgroundResource(R.drawable.ic_out_call);
//            String text = "Outgoing " + strCallType + " call...";
            String text = "";
            if (isArabic){
                text = "مكالمة " + strCallType + " صادرة ...";
            } else {
                text = " Outgoing " + strCallType + " call...";
            }
            holder.tvCallType.setText(text);
            holder.tvCallStatus.setVisibility(View.VISIBLE);
            holder.tvCallStatus.setText(callItem.getCallDuration());
        }

        Date currentItemDate = TimeStampUtils.getDateFormat(Long.parseLong(callItem.getTS()));
        String time = TimeStampUtils.get12HrTimeFormat(mContext, callItem.getTS());
        holder.tvTime.setText(time);

        if (currentItemDate != null) {
            if (currentItemDate.equals(todayDate)) {
                holder.tvDate.setText(mContext.getString(R.string.today));
            } else if (currentItemDate.equals(yesterdayDate)) {
                holder.tvDate.setText(mContext.getString(R.string.yesterday));
            } else {
                DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
                String formatDate = df.format(currentItemDate);
                holder.tvDate.setText(formatDate);
            }
        } else {
            holder.tvDate.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return callItemList.size();
    }

    public class VHCallInfo extends RecyclerView.ViewHolder {

        private TextView tvDate, tvCallType, tvCallStatus, tvTime;
        private ImageView ivCallType;
        private View divider;

        public VHCallInfo(View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.tvDate);
            tvCallType = itemView.findViewById(R.id.tvCallType);
            tvCallStatus = itemView.findViewById(R.id.tvCallStatus);
            tvTime = itemView.findViewById(R.id.tvTime);

            ivCallType = itemView.findViewById(R.id.ivCallType);

            divider = itemView.findViewById(R.id.divider);
        }
    }

}
