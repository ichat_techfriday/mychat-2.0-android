package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chat.android.R;

/**
 * Created by user145 on 4/6/2018.
 */

public class VHInfoMessage extends RecyclerView.ViewHolder {

    public TextView tvInfoMsg, tvDateLbl;

    public VHInfoMessage(View itemView) {
        super(itemView);
        tvInfoMsg = itemView.findViewById(R.id.tvInfoMsg);
        tvDateLbl = itemView.findViewById(R.id.tvDateLbl);

    }
}


