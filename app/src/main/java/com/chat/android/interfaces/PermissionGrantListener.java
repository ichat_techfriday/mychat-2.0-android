package com.chat.android.interfaces;

/**
 * Created by Ali on 1/19/2018.
 */

public interface PermissionGrantListener {

    void onPermissionGranted(boolean granted);
}
