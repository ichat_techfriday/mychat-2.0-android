package com.chat.android.interfaces;

public interface INetworkResponseListener {
    void onNetworkResponse(boolean status, String message, String responseForRequest, Object body);
}
