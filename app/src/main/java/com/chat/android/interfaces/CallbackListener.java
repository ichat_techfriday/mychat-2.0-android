package com.chat.android.interfaces;

public interface CallbackListener {
    void callback(String result);
}
