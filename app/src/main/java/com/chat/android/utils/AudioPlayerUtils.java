package com.chat.android.utils;

import android.content.Context;
import android.media.MediaPlayer;

public class AudioPlayerUtils {
    private static MediaPlayer mediaPlayer;
    private static int audioId = -1;
    public static void playSound(Context context, int sound) {
        if (!StorageUtility.getDataFromPreferences(context, AppConstants.SPKeys.SOUNDS.getValue(), true)) return;
        if (StorageUtility.getDataFromPreferences(context, AppConstants.SPKeys.ONLINE_STATUS.getValue(), "").equals(AppConstants.UserStatus.OFFLINE.getValue())) return;
        if (StorageUtility.getDataFromPreferences(context, AppConstants.SPKeys.ONLINE_STATUS.getValue(), "").equals(AppConstants.UserStatus.BUSY.getValue())) return;

        if (audioId == sound) return;
        audioId = sound;
        mediaPlayer = MediaPlayer.create(context, sound);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                audioId = -1;
                if (mediaPlayer == null) return;
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        });
        mediaPlayer.start();
    }

}

