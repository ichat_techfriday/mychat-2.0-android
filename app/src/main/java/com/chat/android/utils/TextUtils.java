package com.chat.android.utils;

import android.widget.EditText;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


public class TextUtils {
    public static boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();

    }

    public static boolean isValidName(String name) {

        Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789_]*");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches() && name.length() > 0;

    }

    public static boolean isLongEnough(String number) {

        return number.length() > 0;
    }

    public static boolean isLongEnough(String number, int length) {

        return number.length() == length;
    }
    public static boolean isEmpty(String value) {
        if(value == null || value.equals("")) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(EditText value) {
        if(value == null || getValue(value).equals("")) {
            return true;
        }
        return false;
    }


    public static boolean isEmpty(CharSequence value) {
        if(value == null || value.equals("")) {
            return true;
        }
        return false;
    }


    public static String twoDecimals(int value){
        return String.format(Locale.US,"%02d", value);
    }


    public static boolean isValidUrl(String text) {
        boolean foundMatch = false;
        try {
            Pattern regex = Pattern.compile("\\b(?:(https?|ftp|file)://|www\\.)?[-A-Z0-9+&#/%?=~_|$!:,.;]*[A-Z0-9+&@#/%=~_|$]\\.[-A-Z0-9+&@#/%?=~_|$!:,.;]*[A-Z0-9+&@#/%=~_|$]", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
            Matcher regexMatcher = regex.matcher(text);
            foundMatch = regexMatcher.matches();
        } catch (PatternSyntaxException ex) {

        }
        return foundMatch;
    }

    public static String capitalizeFirstCharacter(String toCapitalize) {
        String capital = "";
        if(!TextUtils.isEmpty(toCapitalize)) {
            capital = toCapitalize.substring(0, 1).toUpperCase() + toCapitalize.substring(1).toLowerCase();
            capital = capital.trim();
        }
        return capital;
    }

    public static int getIntValue(String string) {
        try {
            return Integer.parseInt(string);
        }catch (Exception e) {
//            e.printStackTrace();
        }
        return -1;
    }

    public static long getLongValue(String string) {
        try {
            return Long.parseLong(string);
        }catch (Exception e) {
//            e.printStackTrace();
        }
        return -1;
    }

    public static String getValue(EditText editText) {
        return editText.getText().toString();
    }

    public static String getValue(TextView textView) {
        return textView.getText().toString();
    }

    public static String toTitleCase(String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    public static boolean isGoldPin(String pin) {
        final List<Pattern> rxs = new ArrayList<>();
        rxs.add(Pattern.compile("[0-9](.)\\1{6}")); //aaaaaa
        rxs.add(Pattern.compile("[0-9](.)\\1{3}[0-9]{1}")); //aaaaab
        rxs.add(Pattern.compile("[0-9](.)\\1{1}[0-9](.)\\2{1}")); //aaaabb, aaabbb, abbbbb
        rxs.add(Pattern.compile("(.){1}\\1[0-9](.){2}\\2")); //abbbbbbb
        for (Pattern rx : rxs)
            if (rx.matcher(pin).matches())
                return true;
        return checkGoldUsingCode(pin);

    }

    private static boolean checkGoldUsingCode(String pin) {
        if (pin.length() < 6) return false;
        String[] ar = getStringArrayFromString(pin);
        boolean isGolden = false;
        if (isNumbersConsecutive(pin))//abcdefghi
            isGolden = true;
        else if (ar[0].equals(ar[1]) && ar[2].equals(ar[3]) && ar[4].equals(ar[5])  && ar[0].equals(ar[4]))//aabbaa
            isGolden = true;
        else if ((ar[0]+ar[1]).equals(ar[2]+ar[3]) && (ar[4]+ar[5]).equals(ar[0]+ar[1]))//ababab
            isGolden = true;
        else if (ar[0].equals(ar[1]) && (ar[2]+ar[3]).equals(ar[4]+ar[5]) && ar[0].equals(ar[5]) && ar[3].equals(ar[4]) && ar[0].equals(ar[3])) //aababa
            isGolden = true;
        else if (ar[0].equals(ar[5]) && (ar[1]+ar[2]).equals(ar[3]+ar[4])) //abbbba
            isGolden = true;
        else if (ar[0].equals(ar[1]) && ar[2].equals(ar[3]) && ar[4].equals(ar[5]) && ar[0].equals(ar[4])) //aabbaa
            isGolden = true;
        else if (ar[0].equals(ar[2]) && ar[2].equals(ar[3]) && ar[3].equals(ar[4]) && ar[4].equals(ar[5])) //abaaaa
            isGolden = true;
        else if (ar[0].equals(ar[1]) && ar[3].equals(ar[4]) && ar[4].equals(ar[5]) && ar[5].equals(ar[0]) && ar[1].equals(ar[3])) //aabaaa
            isGolden = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[2]) && ar[2].equals(ar[4]) && ar[4].equals(ar[5]) && ar[5].equals(ar[0])) //aaabaa
            isGolden = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[2]) && ar[2].equals(ar[3]) && ar[3].equals(ar[5]) && ar[5].equals(ar[0])) //aaaaba
            isGolden = true;
        else if (ar[0].equals(ar[1]) && ar[2].equals(ar[3]) && ar[4].equals(ar[5])) //aabbcc
            isGolden = true;
        else if (pin.substring(0,2).equals(pin.substring(3,5)) && isNumbersConsecutive(pin.substring(0,2)))
            isGolden = true;
        else if (pin.substring(0,3).equals(pin.substring(3,6)))
            isGolden = true;

        return isGolden;
    }

    private static boolean isNumbersConsecutive(String pin) {
        int[] results = new int[pin.length()];
        String[] ar = getStringArrayFromString(pin);

        for (int i = 0; i < pin.length(); i++) {
            try {
                results[i] = Integer.parseInt(ar[i]);
            } catch (NumberFormatException nfe) {
                return false;
            }
        }

        for (int i = 0; i < ar.length - 1; i++) {
            if (results[i] + 1 != results[i + 1]) {
                return false;
            }
        }
        return true;
    }

    public static boolean isSilverNumber(String pin) {
        if (pin.length() <6) return false;
        String[] ar = getStringArrayFromString(pin);

        boolean isSilver = false;
        if (pin.substring(0,3).equals(pin.substring(4,7))) //abcdabcd (golden if a,b,c,d are consective otherwise silver)
            isSilver = true;
        else if (pin.substring(0,2).equals(pin.substring(3,5))  && pin.substring(0,1).equals(pin.substring(6,7))) //abcabcab
            isSilver = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[2]) && ar[2].equals(ar[3]))//aaaabcde
            isSilver = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[6]) && ar[6].equals(ar[7]))//aabcdeaa
            isSilver = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[2]) && ar[2].equals(ar[3]) && ar[3].equals(ar[4]) && ar[4].equals(ar[5]))//aaaaaabc
            isSilver = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[2]) && ar[2].equals(ar[3]) && ar[5].equals(ar[6]) && ar[6].equals(ar[7]))//aaaabccc
            isSilver = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[2]) && ar[2].equals(ar[3]) && ar[4].equals(ar[5]) && ar[6].equals(ar[7]))//aaaabbcc
            isSilver = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[2]) && ar[2].equals(ar[3]) && ar[3].equals(ar[4]) && ar[4].equals(ar[5]))//aaaaaabc
            isSilver = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[2]) && ar[2].equals(ar[3]) && ar[3].equals(ar[4]) && ar[4].equals(ar[5]))
            isSilver = true;
        else if (pin.substring(0,2).equals(pin.substring(5,7)))//aaabcaaa
            isSilver = true;
        else if (ar[0].equals(ar[1]) && ar[1].equals(ar[2]) && ar[2].equals(ar[3]) && ar[4].equals(ar[5]) && ar[5].equals(ar[6]))//aaaabbbc
            isSilver = true;
        return isSilver;
    }

    private static String[] getStringArrayFromString(String str) {
        if (str == null)
            return null;
        String[] array = new String[str.length()];
        for (int i = 0; i< str.length(); i++) {
            array[i] = String.valueOf(str.charAt(i));
        }
        return array;
    }

    public static boolean isURLValid() {
        final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";

        Pattern p = Pattern.compile(URL_REGEX);
        Matcher m = p.matcher("example.com");//replace with string to compare
        return m.find();
    }

    public static String getFirstName(String name) {
        String firstName = "", lastName = "";
        try {
            if (name.split("\\w+").length > 1) {
                lastName = name.substring(name.lastIndexOf(" ") + 1);
                firstName = name.substring(0, name.indexOf(' '));
            } else {
                firstName = name;
            }
        } catch (Exception e) {
//            RapporrUtils.notifyException(e);
            firstName = name;
        }
        return firstName;
    }

}
