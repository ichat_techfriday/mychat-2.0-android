package com.chat.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class StorageUtility {
    private static final String PREFERENCE_KEY_USER_ID = "userId";
    private static final String PREFERENCE_KEY_NICKNAME = "nickname";
    private static final String PREFERENCE_KEY_CONNECTED = "connected";
    private static final String PREFERENCE_KEY_GROUP_CHANNEL_LAST_READ = "last_read";
    private static final String PREFERENCE_KEY_NOTIFICATIONS_SHOW_PREVIEWS = "notificationsShowPreviews";

    public static void saveDataInPreferences(Context c, String key, String value) {

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(c).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void saveDataInPreferences(Context c, String key, int value) {
        if (c == null)
            return;
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(c).edit();
        editor.putInt(key, value);
        editor.apply();
    }


    public static void saveDataInPreferences(Context c, String key, boolean value) {
        if (c == null)
            return;
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(c).edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void saveDataInPreferences(Context c, String key, long  value) {
        if (c == null)
            return;
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(c).edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static boolean getDataFromPreferences(Context c, String key, boolean defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getBoolean(key, defaultValue);
    }
    public static long  getDataFromPreferences(Context c, String key, long defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getLong(key, defaultValue);
    }


    public static String getDataFromPreferences(Context c, String key, String defaultValue) {
        if (c == null)
            return defaultValue;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getString(key, defaultValue);
    }


    public static int getDataFromPreferences(Context c, String key, int defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        return preferences.getInt(key, defaultValue);
    }

    public static void clearAllPreferences(Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.clear();
        editor.apply();
    }

    //send bird functions
    public static void setUserId(Context context, String userId) {
        saveDataInPreferences(context, PREFERENCE_KEY_USER_ID, userId);
    }

    public static String getUserId(Context context) {
        return getDataFromPreferences(context, PREFERENCE_KEY_USER_ID, "");
    }

    public static void setNickname(Context context, String nickname) {
        saveDataInPreferences(context, PREFERENCE_KEY_NICKNAME, nickname);
    }

    public static String getNickname(Context context) {
        return getDataFromPreferences(context, PREFERENCE_KEY_NICKNAME, "");
    }

    public static void setConnected(Context context, boolean tf) {
        saveDataInPreferences(context, PREFERENCE_KEY_CONNECTED, tf);
    }

    public static boolean getConnected(Context context) {
        return getDataFromPreferences(context, PREFERENCE_KEY_CONNECTED, false);
    }
}
