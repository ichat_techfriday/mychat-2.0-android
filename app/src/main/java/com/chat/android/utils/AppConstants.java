package com.chat.android.utils;

import com.chat.android.app.activity.Item;

public class AppConstants {
    public static final String AGORA_APP_ID             = "a7c43926c0394f54aad2ba623cf26a78";
    private static final Integer SAMPLE_RATE = 44100;
    private static final Integer SAMPLE_NUM_OF_CHANNEL = 2;
    private static final Integer BITS_PER_SAMPLE = 16;
    private static final Integer SAMPLES = 441;
    public static final Integer BUFFER_SIZE = SAMPLES * BITS_PER_SAMPLE / 8 * SAMPLE_NUM_OF_CHANNEL;
    public static final Integer PUSH_INTERVAL = SAMPLES * 1000 / SAMPLE_RATE;

    public static final String JPEG_FILE_PREFIX             = "IMG_";
    public static final String JPEG_FILE_SUFFIX             = ".jpg";
    public static final String FOLDER_NAME_THUMB            = "Thumbnails";
    public static final String CANCEL                       = "cancel";
    public static final  String LANGUAGE_ENGLISH            = "en-US";
    public static final String USER_DEFAULT_LANGUAGE        = LANGUAGE_ENGLISH;
    public static final String KEY_USER_LANGUAGE            = "user_language";
    public static final String CALLING_ACTIVITY             = "callingActivity";
    public static final String INSTANT_TRANSLATION_POPUP_ANY_CHAT =  "instant_translation_popup_any_chat";
    public static final String INSTANT_TRANSLATION_POPUP_VERIFY_PHONE_SCREEN =  "instant_translation_popup_verify_phone_screen";
    public static final String ISFROM                       = "isFrom";
    public static final String INITIATOR                    = "inititator";
    public static final String CALL_TYPE                    = "call_type";
    public static final String NOTIFICATION_ID              = "NOTIFICATION_ID";
//    public static final String ONE_MONTH_SUB_ID             = "mychat_bms1";
    public static final String ONE_MONTH_SUB_ID             = "ms_mychat";

    public enum FriendStatus {
        DISABLED("DISABLED"),
        FRIENDS("FRIENDS"),
        REQUEST_SENT("SENT"),
        REQUEST_RECEIVED("RECEIVED"),
        DELETED("DELETED"),
        ;

        private String value;
        FriendStatus(String i) {
            value = i;
        }

        public String getValue() {
            return value;
        }
    }

    public enum SPKeys {
        IS_TUTORIAL_FOR_STATUS_SEEN("IS_TUTORIAL_FOR_STATUS_SEEN"),
        SOUNDS("SOUNDS"),
        ONLINE_STATUS("online_status"),
        LANGUAGE("language"),
        PIN_CODE("keyUserPinCode"),
        PIN_CODE_TIMESTAMP("PIN_CODE_TIMESTAMP"),
        BRANCH_MSISDN_ID("BRANCH_MSISDN_ID"),
        OUT_GOING_LANGUAGE("OUT_GOING_LANGUAGE"),
        OUT_GOING_LANGUAGE_CODE("OUT_GOING_LANGUAGE_CODE"),
        MEETUP_FULL_NAME("MEETUP_FULL_NAME"),
        MEETUP_BIO("MEETUP_BIO"),
        MEETUP_DOB("MEETUP_DOB"),
        MEETUP_INTRO_COMPLETED("MEETUP_INTRO_COMPLETED"),
        MEETUP_PERMISSION_ALLOWED("MEETUP_PERMISSION_ALLOWED"),;

        private String value;
        SPKeys(String i) {
            value = i;
        }

        public String getValue() {
            return value;
        }
    }

    public enum UserStatus {
        ONLINE("ONLINE"),
        BUSY("BUSY"),
        OFFLINE("OFFLINE"),
        AWAY("AWAY");

        String value;

        UserStatus(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum Tab {
        CONTACTS(0),
        CONVERSATIONS(1),
        CALLS(2),
        SETTING(3);

        int mValue;
        Tab(int value) {
            this.mValue = value;
        }
        public int getValue() {
            return mValue;
        }
    }

    public enum IntentKeys {
        FRIEND_NAME("friend_name"),
        FRIEND_IMAGE("friend_image"),
        FRIEND_ID("friend_id"),
        PIN_CODE("pin_code"),
        ONLINE_STATUS("online_status"),
        IS_FROM("is_from"),
        FILE_PATH("image_path"),
        FILE_TYPE("image_type"),
        THUMBNAIL_PATH("thumbnail_path"),
        FILE_URI("file_uri"),
        LOCATION_ADDRESS("location_address"),
        LOCATION_LATITUDE("location_latitude"),
        LOCATION_LONGITUDE("location_longitude"),
        EXTRA_KEY_LANGUAGE_KEY("key_language_key"),
        EXTRA_KEY_LANGUAGE("key_language"),
        EXTRA_KEY_LANGUAGE_CODE("key_language_code"),
        MEETUP_SCREEN_TYPE("key_meetup_screen_type"),
        MEETUP_DISTANCE_IN_METERS("distanceInMeters");
        String mValue;
        IntentKeys(String value) {
            this.mValue = value;
        }

        public String getValue() {
            return mValue;
        }
    }

    public enum RequestCodes {
        REQUEST_CAMERA(9161),
        REQUEST_GALLERY(9162),
        REQUEST_VIDEO(9163),
        REQUEST_LOCATION(257),
        REQUEST_PERMISSION(256),
        REQUEST_ADD_FRIEND(255),
        REQUEST_CHANGE_LANGUAGE(254);

        int mValue;
        RequestCodes(int value) {
            this.mValue = value;
        }

        public int getCode() {
            return mValue;
        }
    }

    public enum MediaType {
        IMAGE("IMAGE"),
        VIDEO("VIDEO"),
        AUDIO("AUDIO"),
        NUDGE("NUDGE"),
        LOCATION("LOCATION"),
        TEXT("TEXT"),
        INFO("INFO"),
        GIF("GIF");

        String mValue;
        MediaType(String value) {
            this.mValue = value;
        }

        public String getValue() {
            return mValue;
        }
    }

    public enum MimeTypes{
        JPG("jpg"),
        PNG("png");

        String mValue;
        MimeTypes(String value) {
            this.mValue = value;
        }
    }

    public enum IS_FROM{
        SettingsFragment("settings_fragment");

        String mValue;
        IS_FROM(String value) {
            this.mValue = value;
        }
    }

    public enum MEETUP_SCREEN_NAME{
        MeetupInfoFragment("MeetupInfoFragment"),
        MeetupRadiusFragment("MeetupRadiusFragment"),
        MeetupProfileFragment("MeetupProfileFragment")
        ;

        String mValue;
        MEETUP_SCREEN_NAME(String value) {
            this.mValue = value;
        }
        public String getValue() {
            return mValue;
        }
    }


}
