package com.chat.android.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chat.android.core.CoreController;
import com.chat.android.core.database.PinCodesDB_Sqlite;
import com.chat.android.core.model.PinCodeModel;
import com.chat.android.core.service.Constants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


public class PincodesCacheUtility {
    private static PincodesCacheUtility pinCodesCacheUtility;
    private HashMap<String, PinCodeModel> pinCodeModels;
    private HashSet<String> phoneNumbers;

    private PincodesCacheUtility() {
        pinCodeModels = new HashMap<>();
        phoneNumbers = new HashSet<>();
    }

    public static PincodesCacheUtility getInstance(Context context) {
        if (pinCodesCacheUtility == null)
            pinCodesCacheUtility = new PincodesCacheUtility();
        return pinCodesCacheUtility;
    }

    public void init(Context context) {
        ArrayList<PinCodeModel> list = getPinCodes(context);
        //if (list == null || list.size() == 0) {
        getPinCodesFromServer(context);
        //}
    }

    public ArrayList<PinCodeModel> getPinCodes(Context context) {
        ArrayList pinCodeModelsList = CoreController.getPinCodesDB(context).getPinCodes();
        if(pinCodeModelsList.size() > 0)
            setPinMap(pinCodeModelsList);
        return pinCodeModelsList;
    }

    public void savePinCodes(Context context, final ArrayList<PinCodeModel> modelsList) {
        if (modelsList == null)
            return;
        setPinMap(modelsList);
        CoreController.getPinCodesDB(context).savePinCodes(modelsList);
    }

    public boolean doesPinCodeExists(String pinCode) {
        if (pinCodeModels == null || pinCodeModels.size() == 0)
            return false;
        PinCodeModel pinCodeModel = pinCodeModels.get(pinCode);
        return pinCodeModel != null;
    }

    private void setPinMap(ArrayList<PinCodeModel> conversationMap) {
        if (pinCodeModels == null)
            pinCodeModels = new HashMap<>();

        for (PinCodeModel pinCodeModel : conversationMap) {
            pinCodeModels.put(pinCodeModel.getPinCode(), pinCodeModel);
            String phoneNumber = pinCodeModel.getPhoneNumber();
            if (!TextUtils.isEmpty(phoneNumber))
                phoneNumbers.add(phoneNumber);
        }
    }

    public void removePinCode(String pinCode) {
        if(pinCodeModels == null)
            return;
        pinCodeModels.remove(pinCode);
    }

    private ArrayList<PinCodeModel> getListFromMap() {
        ArrayList<PinCodeModel> pinCodes = new ArrayList<>();
        for (Map.Entry<String, PinCodeModel> pair : pinCodeModels.entrySet()) {
            pinCodes.add(pair.getValue());
        }

        return pinCodes;
    }

    public void updatePin(Context context, String pinCode, String phoneNumber) {
        PinCodeModel pinCodeModel = new PinCodeModel(phoneNumber, pinCode);
        CoreController.getPinCodesDB(context).updatePinCode(pinCodeModel);
        getPinCodes(context);
    }

    public HashSet<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public String getPhoneNumberFromPin(String pin) {
        if (pinCodeModels == null)
            return "";
        PinCodeModel pinCodeModel = pinCodeModels.get(pin);
        if (pinCodeModel == null)
            return "";
        return pinCodeModel.getPhoneNumber();
    }

    public String getPinCodeFromPhone(String memberId) {
        if (pinCodeModels == null)
            return "";
        for (Map.Entry<String, PinCodeModel> pair : pinCodeModels.entrySet()) {
            String key = pair.getKey();
            PinCodeModel value = pair.getValue();
            String phoneNumber = value.getPhoneNumber();
            if (phoneNumber != null && phoneNumber.equals(memberId)) {
                return value.getPinCode();
            }
        }
        return "";
    }

    public String getNameFromPhoneNumber(Context context, String msidin) {
        if (pinCodeModels == null || pinCodeModels.size() == 0) {
            init(context);
        }
        for (Map.Entry<String, PinCodeModel> pair : pinCodeModels.entrySet()) {
            String key = pair.getKey();
            PinCodeModel value = pair.getValue();
            String phoneNumber = value.getPhoneNumber();
            if (phoneNumber != null && phoneNumber.equals(msidin)) {
                return value.getName();
            }
        }
        return "";
    }

    private void getPinCodesFromServer(final Context context) {
        // prepare the Request
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = Constants.PIN_CODE;
        String timestamp = StorageUtility.getDataFromPreferences(context, AppConstants.SPKeys.PIN_CODE_TIMESTAMP.getValue(), "");
        if (!android.text.TextUtils.isEmpty(timestamp))
            url = url + "?timestamp=\"" + timestamp + "\"";
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject object) {
                        // display response
                        Log.e("Response", object.toString());
                        try {
                            if (object.has("status")) {
                                if (object.optString("status").equals("1")) {
                                    ArrayList<PinCodeModel> pinCodeModels = null;
                                    if (object.has("pinCodes")) {
                                        Type type = new TypeToken<ArrayList<PinCodeModel>>() {
                                        }.getType();
                                        pinCodeModels = new Gson().fromJson(object.optString("pinCodes"), type);
                                        PincodesCacheUtility pinCodesCacheUtility = PincodesCacheUtility.getInstance(context);
                                        pinCodesCacheUtility.savePinCodes(context, pinCodeModels);
                                    }
                                    if (object.has("timestamp")) {
                                        String timeStamp = object.optString("timestamp");
                                        StorageUtility.saveDataInPreferences(context, AppConstants.SPKeys.PIN_CODE_TIMESTAMP.getValue(), timeStamp);
                                    }
                                }
                            }

                        } catch (Exception e) {
                            Log.e("Exception", "Exception" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //     Log.d("Error.Response", error);
                    }
                }
        );

// add it to the RequestQueue
        queue.add(getRequest);
    }

    public void savePinCodeItem(Context context, String newUserId, String newUserMsisdn, String newUserName) {
        //pinCodeModels.put(newUserMsisdn)
        getPinCodesFromServer(context);
    }
}
