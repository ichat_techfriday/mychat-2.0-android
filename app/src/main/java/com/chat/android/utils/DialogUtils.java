package com.chat.android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.chat.android.R;
import com.chat.android.app.adapter.BottomSheetAdapter;
import com.chat.android.app.callbacks.CallbackListener;
import com.chat.android.app.model.BottomSheetModel;
import com.chat.android.core.model.ErrorMessageModel;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;

public class DialogUtils {

    private static AlertDialog alert;

    public static void showNativeDialog(final Context context, String title, String message, DialogInterface.OnClickListener clickListener, int dialogMessageTextSize) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogTheme);
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), clickListener);
        alert = builder.create();
        alert.show();
        TextView textView = alert.findViewById(android.R.id.message);
        if (textView != null) {
            textView.setTextSize(dialogMessageTextSize);
        }
    }

    public static void showNativeDialog(final Context context, ErrorMessageModel errorMessageModel, String buttonText, DialogInterface.OnClickListener clickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogTheme);
        builder.setMessage(errorMessageModel.getMessage())
                .setTitle(errorMessageModel.getTitle())
                .setCancelable(false)
                .setPositiveButton(buttonText, clickListener);
        AlertDialog alert = builder.create();
        alert.show();
        TextView textView = alert.findViewById(android.R.id.message);
        if (textView != null) {
            textView.setTextSize(16);
        }
    }

    public static void showNativeDialog(final Context context, ErrorMessageModel errorMessageModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogTheme);
        builder.setMessage(errorMessageModel.getMessage())
                .setTitle(errorMessageModel.getTitle())
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        TextView textView = alert.findViewById(android.R.id.message);
        if (textView != null) {
            textView.setTextSize(16);
        }
    }

    public static void showAlertWithButtons(final Context context, String title, String message, String positiveBtn, String negativeBtn,
                                            DialogInterface.OnClickListener posListener, DialogInterface.OnClickListener negListener,
                                            DialogInterface.OnCancelListener cancelListener) {
        try {
            if(alert != null && alert.isShowing()) {
                alert.dismiss();
            }
        } catch (Exception e) {}



        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogTheme);
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton(positiveBtn, posListener)
                .setNegativeButton(negativeBtn, negListener);
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void dismiss() {
        if (alert != null) {
            try {
                alert.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void showBottomSheetDialog(Context context, String title, final ArrayList<BottomSheetModel> data, final CallbackListener callbackListener) {
        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.layout_bottom_sheet, null);
        ListView recyclerView = (ListView) view.findViewById(R.id.list_view);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        BottomSheetAdapter adapter = new BottomSheetAdapter(context, data);
        recyclerView.setAdapter(adapter);

        final BottomSheetDialog dialog = new BottomSheetDialog(context);
        dialog.setContentView(view);
        tvTitle.setText(title);
        dialog.show();
        recyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                BottomSheetModel model = data.get(position);
                callbackListener.callback(model.getData());
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                callbackListener.callback(AppConstants.CANCEL);
            }
        });
    }

}
