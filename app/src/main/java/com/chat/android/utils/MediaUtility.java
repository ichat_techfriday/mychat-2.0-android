package com.chat.android.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.chat.android.BuildConfig;
import com.chat.android.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MediaUtility {
    private static String mCurrentPhotoPath;
    public static void startCameraIntent(AppCompatActivity activity) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 1048 * 1048);
        takePictureIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        File f;
        try {
            f = setUpPhotoFile(activity);
            mCurrentPhotoPath = f.getAbsolutePath();
            Uri uri;
            if(MyChatUtils.getAPIVersion() >= 23) {
                uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", f);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            }
            else {
                uri = Uri.fromFile(f);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            }
        } catch (IOException e) {
            MyChatUtils.notifyException(e);
            mCurrentPhotoPath = null;
        }
        activity.startActivityForResult(takePictureIntent, AppConstants.RequestCodes.REQUEST_CAMERA.getCode());
    }

    public static File setUpPhotoFile(Context context) throws IOException {
        File f = createImageFile(context);
        mCurrentPhotoPath = f.getAbsolutePath();
        return f;
    }

    private static File createImageFile(Context context) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = AppConstants.JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getDirectoryBasedOnType(context, AppConstants.MediaType.IMAGE, true);
        return File.createTempFile(imageFileName, AppConstants.JPEG_FILE_SUFFIX, albumF);
    }

    public static File getAlbumDir(Context context) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/"+context.getString(R.string.app_name));
        if(!myDir.exists())
            myDir.mkdir();
        return myDir;
    }

    public static File getDirectoryBasedOnType(Context context, AppConstants.MediaType mediaType, boolean isSentByLoggedInUser) {
        File albumF = getAlbumDir(context);
        File outputFile = null;
        outputFile = new File(albumF, mediaType.getValue());
        if (!outputFile.exists())
            outputFile.mkdir();
        if (isSentByLoggedInUser) {
            File file = new File(outputFile, "Sent");
            if (!file.exists())
                file.mkdir();
            createNoMediaFile(file);
            return file;
        }

        return outputFile;
    }

    public static File getDirectoryBasedOnTypeToSave(Context context, AppConstants.MediaType mediaType, boolean isSentByLoggedInUser) {
//        File albumF = getAlbumDir(context);
        File outputFile = null;
        outputFile = new File(context.getString(R.string.app_name), mediaType.getValue());
        if (!outputFile.exists())
            outputFile.mkdir();
        if (isSentByLoggedInUser) {
            File file = new File(outputFile, "Sent");
            if (!file.exists())
                file.mkdir();
            return file;
        }

        return outputFile;
    }


    public static String getCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public static void deleteFile() {
        try{
            File file = new File(mCurrentPhotoPath);
            file.delete();
        } catch (Exception e) {
            MyChatUtils.notifyException(e);
        }
    }

    public static void setmCurrentPhotoPath(String mCurrentPhotoPath) {
        MediaUtility.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    public static void startGalleryByIntent(Context context) {
        Intent intent1 = new Intent();
        intent1.setAction(Intent.ACTION_GET_CONTENT);
        intent1.addCategory(Intent.CATEGORY_OPENABLE);
        intent1.setType("image/*");
        ((Activity)context).startActivityForResult(intent1, AppConstants.RequestCodes.REQUEST_GALLERY.getCode());
    }

    public static void startGalleryToSelectVideo(Context context) {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ((Activity)context).startActivityForResult(Intent.createChooser(intent,"Select Video"),AppConstants.RequestCodes.REQUEST_VIDEO.getCode());
    }

    @SuppressLint("NewApi")
    public static String getPath(Context context,Uri uri)
    {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static File getFolderImagePath(Context context) {
        String externalCacheDir = getInternalCacheDir(context).getPath();
        File imgFolder = new File(externalCacheDir, "images");
        if (!imgFolder.exists()) {
            //noinspection ResultOfMethodCallIgnored
            imgFolder.mkdir();
        }
        return imgFolder;
    }

    public static File getInternalCacheDir(Context context) {
        File cacheFolder = new File(context.getCacheDir(), "cache");
        if (!cacheFolder.exists()) {
            //noinspection ResultOfMethodCallIgnored
            cacheFolder.mkdir();
        }
        return cacheFolder;
    }





    public static String getThumbnailBitmapFromVideo(Context context, Uri selectedImageUri) {
        Bitmap bitmap = null;
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(context, selectedImageUri);
            bitmap = ThumbnailUtils.extractThumbnail(retriever.getFrameAtTime(1000 , MediaMetadataRetriever.OPTION_PREVIOUS_SYNC), 300, 300);
        } catch (Exception e) {
            MyChatUtils.notifyException(e);
            return "";
        }
        File parentDirectory = getDirectoryBasedOnType(context, AppConstants.MediaType.VIDEO, false);
        File myDir = new File(parentDirectory, AppConstants.FOLDER_NAME_THUMB);
        if (!myDir.exists())
            myDir.mkdirs();
        return saveImageFromBitmap(myDir, bitmap);
    }

    public static String saveImageFromBitmap(File myDir, Bitmap bitmap) {
        String path = "";

        String fname = getFileName() + ".jpg";
        File file = new File (myDir, fname);

        if (file.exists ())
            file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            path=file.getPath();

        } catch (Exception e) {
            MyChatUtils.notifyException(e);
        }
        createNoMediaFile(myDir);
        return path;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage) throws IOException {
        int MAX_HEIGHT = 720;
        int MAX_WIDTH  = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        float photoRotation = 0;
        boolean hasRotation = false;
        String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
        try {
            Cursor cursor = context.getContentResolver().query(selectedImage, projection, null, null, null);
            if (cursor.moveToFirst()) {
                photoRotation = cursor.getInt(0);
                hasRotation = true;
            }
            cursor.close();
        } catch (Exception e) {
        }

        if (!hasRotation) {
            ExifInterface exif = new ExifInterface(MediaUtility.getPath(context, selectedImage));
            int exifRotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (exifRotation) {
                case ExifInterface.ORIENTATION_ROTATE_90: {
                    photoRotation = 90.0f;
                    break;
                }
                case ExifInterface.ORIENTATION_ROTATE_180: {
                    photoRotation = 180.0f;
                    break;
                }
                case ExifInterface.ORIENTATION_ROTATE_270: {
                    photoRotation = 270.0f;
                    break;
                }
            }
        }

        String path = MediaUtility.getPath(context, selectedImage);
        if(TextUtils.isEmpty(path))
            return img;

        ExifInterface ei = new ExifInterface(path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public static String getPathOfCopiedFile(Context context, String sourcePath, AppConstants.MediaType mediaType, boolean isSentByLoggedInUser) {
        String path = "";
        try {
            File sd = getDirectoryBasedOnType(context, mediaType, isSentByLoggedInUser);
            if (sd.canWrite()) {
                File source = new File(sourcePath);
                String extension = sourcePath.substring(sourcePath.lastIndexOf("."));
                File destination = new File(sd, getFileName() + extension) ;
                if (source.exists()) {
                    FileChannel src = new FileInputStream(source).getChannel();
                    FileChannel dst = new FileOutputStream(destination).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
                path = destination.getAbsolutePath();
                createNoMediaFile(sd);
            }
        } catch (Exception e) {
            MyChatUtils.notifyException(e);
        }
        return path;
    }

    public static String getDuration(Context context, String pathStr) {
        try {
            Uri uri = Uri.fromFile(new File(pathStr));
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(context, uri);
            return mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        } catch (Exception e) {
            MyChatUtils.notifyException(e);
        }
        return "";
    }

    public static Bitmap rotateBitmapOrientation(String photoFilePath) {
        // Create and configure BitmapFactory
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoFilePath, bounds);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(photoFilePath, opts);
        // Read EXIF Data
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(photoFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;
        // Rotate Bitmap
        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        // Return result
        return rotatedBitmap;
    }

    public static Bitmap BITMAP_RESIZER(Bitmap bitmap, int width, int height) {
        Bitmap background = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        float originalWidth = bitmap.getWidth(), originalHeight = bitmap.getHeight();
        Canvas canvas = new Canvas(background);
        float scale, xTranslation = 0.0f, yTranslation = 0.0f;
        if (originalWidth > originalHeight) {
            scale = height/originalHeight;
            xTranslation = (width - originalWidth * scale)/2.0f;
        }
        else {
            scale = width / originalWidth;
            yTranslation = (height - originalHeight * scale)/2.0f;
        }
        Matrix transformation = new Matrix();
        transformation.postTranslate(xTranslation, yTranslation);
        transformation.preScale(scale, scale);
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        canvas.drawBitmap(bitmap, transformation, paint);
        return background;
    }

    public static String saveFileAndGetPath(Context context, Bitmap photo, String imageName, String mimeType) {
        String root = Environment.getExternalStorageDirectory().toString();

        File myDir = getDirectoryBasedOnType(context, AppConstants.MediaType.IMAGE, true);
        myDir.mkdirs();

        File file = new File(myDir + "/" + imageName + mimeType);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            MyChatUtils.notifyException(e);
            return "";
        }
        return file.getPath();
    }

    public static String getFileName() {
        return "media_file-" + System.currentTimeMillis();
    }

    public static String getMimeType(Context context, Uri uri) {
        String mimeType = AppConstants.MimeTypes.JPG.name();
        if (uri == null)
            return mimeType;

        ContentResolver cR = context.getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        mimeType = mime.getExtensionFromMimeType(cR.getType(uri));
        if (mimeType == null)
            mimeType = AppConstants.MimeTypes.JPG.name();
        return mimeType;
    }

    public static void createNoMediaFile(File parentDirectoryPath) {
        File output = new File(parentDirectoryPath, ".nomedia");
        try {
            boolean fileCreated = output.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getEmptyFilePath() {

      return   Environment.getExternalStorageDirectory()+"temp.jpg";
    }
}
