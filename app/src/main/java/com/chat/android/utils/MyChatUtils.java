package com.chat.android.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.Toast;


import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.Toolbar;
import com.chat.android.R;
import com.chat.android.app.utils.MyLog;

import java.util.Locale;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

public class MyChatUtils {

    public static boolean isNetworkAvailable(Context activity) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception ex) {
            MyChatUtils.notifyException(ex);
        }
        return false;
    }

    public static void hideKeyboard(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

//    Locale.getDefault().getLanguage()        ---> en
//Locale.getDefault().getISO3Language()    ---> eng
//Locale.getDefault().getCountry()         ---> US
//Locale.getDefault().getISO3Country()     ---> USA
//Locale.getDefault().toString()           ---> en_US
//Locale.getDefault().getDisplayLanguage() ---> English
//Locale.getDefault().getDisplayCountry()  ---> United States
//Locale.getDefault().getDisplayName()     ---> English (United States)
//
    public static String getKeyboardLanguage(Activity context) {
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
        String localeString = ims.getLocale();
        Locale locale = new Locale(localeString);
        String currentLanguage = locale.getLanguage();
        String getDisplayLanguage = locale.getDisplayLanguage();
        return currentLanguage;
    }
 public static String getKeyboardLanguageNew(Activity context) {
     Locale locale;
     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
         locale = Resources.getSystem().getConfiguration().getLocales().get(0);
     } else {
         //noinspection deprecation
         locale = Resources.getSystem().getConfiguration().locale;
     }
     String currentLanguage = locale.getLanguage();
        return currentLanguage;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                MyChatUtils.notifyException(e);
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static boolean isLocationPermissionAllowed(Context mContext) {

        int fineLocation = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocation = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION);
        boolean isEnabled = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

            int bgLocation = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_BACKGROUND_LOCATION);

            boolean isAppLocationPermissionGranted = (bgLocation == PackageManager.PERMISSION_GRANTED) &&
                    (coarseLocation == PackageManager.PERMISSION_GRANTED);

            boolean preciseLocationAllowed = (fineLocation == PackageManager.PERMISSION_GRANTED)
                    && (coarseLocation == PackageManager.PERMISSION_GRANTED);

            if (preciseLocationAllowed) {
                MyLog.e("PERMISSION", "Precise location is enabled in Android 12");
            } else {
                MyLog.e("PERMISSION", "Precise location is disabled in Android 12");
                isEnabled = false;
            }

            if (isAppLocationPermissionGranted) {
                Log.e("PERMISSION", "Location is allowed all the time");
            } else if (coarseLocation == PackageManager.PERMISSION_GRANTED) {
                MyLog.e("PERMISSION", "Location is allowed while using the app");
            } else {
                isEnabled = false;
                MyLog.e("PERMISSION", "Location is not allowed.");
            }

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

            int bgLocation = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_BACKGROUND_LOCATION);

            boolean isAppLocationPermissionGranted = (bgLocation == PackageManager.PERMISSION_GRANTED) &&
                    (coarseLocation == PackageManager.PERMISSION_GRANTED);

            if (isAppLocationPermissionGranted) {
                MyLog.e("PERMISSION", "Location is allowed all the time");
            } else if (coarseLocation == PackageManager.PERMISSION_GRANTED) {
                MyLog.e("PERMISSION", "Location is allowed while using the app");
            } else {
                isEnabled = false;
                MyLog.e("PERMISSION", "Location is not allowed.");
            }

        } else {

            boolean isAppLocationPermissionGranted = (fineLocation == PackageManager.PERMISSION_GRANTED) &&
                    (coarseLocation == PackageManager.PERMISSION_GRANTED);

            if (isAppLocationPermissionGranted) {
                MyLog.e("PERMISSION", "Location permission is granted");
            } else {
                isEnabled = false;
                MyLog.e("PERMISSION", "Location permission is not granted");
            }
        }
        return isEnabled;
    }


    public static boolean isPhoneNumberValid(Context context, String region, String number) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.createInstance(context);

        try {
            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(number, region);
            return phoneNumberUtil.isValidNumber(phoneNumber);
        } catch (Exception e) {
            notifyException(e);
        }
        return false;

    }

    public static boolean isPhoneNumberValid(Context context, String number) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.createInstance(context);

        try {
            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(number, "");
            return phoneNumberUtil.isValidNumber(phoneNumber);
        } catch (Exception e) {
            notifyException(e);
        }
        return false;

    }

    public static void notifyException(Exception e) {

        if (e != null) {
//            e.printStackTrace();
            MyLog.e("notifyException", e.getMessage());
        }
    }

    public static int getColor(Context context, int id) {
        if (getAPIVersion() >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static int getAPIVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static void setTitleFont(Context context, Toolbar toolbar) {
        toolbar.setTitleTextAppearance(context, R.style.NunitoBoldTextAppearance);
    }

    public static void copyToClipboard(Context mContext, String pinCode) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("pin code", String.valueOf(pinCode));
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
        Toast.makeText(mContext, mContext.getString(R.string.copied_to_clipboard), Toast.LENGTH_SHORT).show();
    }

    public static String parseStatusForServer(Context mContext, String result) {

        if (result.equalsIgnoreCase(mContext.getString(R.string.online)))
            return AppConstants.UserStatus.ONLINE.getValue();
        if (result.equalsIgnoreCase(mContext.getString(R.string.offline)))
            return AppConstants.UserStatus.OFFLINE.getValue();
        if (result.equalsIgnoreCase(mContext.getString(R.string.away)))
            return AppConstants.UserStatus.AWAY.getValue();
        if (result.equalsIgnoreCase(mContext.getString(R.string.busy)))
            return AppConstants.UserStatus.BUSY.getValue();
        return "";
    }

    public static int getOnlineStatusResource(String onlineStatus) {
        int res = R.drawable.online_status_circle;
        if (TextUtils.isEmpty(onlineStatus))
            return res;
        if (onlineStatus.equals(AppConstants.UserStatus.OFFLINE.getValue()))
            res = R.drawable.offline_status_circle;
        else if (onlineStatus.equals(AppConstants.UserStatus.BUSY.getValue()))
            res = R.drawable.busy_status_circle;
        else if (onlineStatus.equals(AppConstants.UserStatus.AWAY.getValue()))
            res = R.drawable.away_status_circle;

        return res;
    }

    public static int getOnlineStatusImageResource(String onlineStatus) {
        int res = R.drawable.ic_online_status;
        if (TextUtils.isEmpty(onlineStatus))
            return res;
        if (onlineStatus.equals(AppConstants.UserStatus.OFFLINE.getValue()))
            res = R.drawable.ic_offline_status;
        else if (onlineStatus.equals(AppConstants.UserStatus.BUSY.getValue()))
            res = R.drawable.ic_busy_status;
        else if (onlineStatus.equals(AppConstants.UserStatus.AWAY.getValue()))
            res = R.drawable.ic_away_status;

        return res;
    }

    public static String parseStatusToLocal(Context context, String result) {

        int res = 0;
        if (result.equalsIgnoreCase(AppConstants.UserStatus.ONLINE.getValue()))
            res = R.string.online;
        if (result.equalsIgnoreCase(AppConstants.UserStatus.OFFLINE.getValue()))
            res = R.string.offline;
        if (result.equalsIgnoreCase(AppConstants.UserStatus.BUSY.getValue())) res = R.string.busy;
        if (result.equalsIgnoreCase(AppConstants.UserStatus.AWAY.getValue())) res = R.string.away;

        if (res > 0) {

            return context.getString(res);
        }

        return result;

    }

    public static boolean isBase64Encoded(String value)
    {
        try {
            byte[] decodedString = Base64.decode(value, Base64.DEFAULT);
            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String getLanguageKey(Context context, String id) {
        return getLanguageKey(context, AppConstants.SPKeys.OUT_GOING_LANGUAGE.getValue(), id);
    }

    public static String getLanguageKey(Context context, String postFix, String id) {
        if (id == null)
            return "";

        return id + "_" + postFix;
    }

    public static void setLocale(Context mContext, String languageCode) {

        Resources res = mContext.getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(languageCode);
        res.updateConfiguration(conf, dm);
    }

}
