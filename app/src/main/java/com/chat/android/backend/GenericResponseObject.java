package com.chat.android.backend;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GenericResponseObject<ResponseType> {

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<ResponseType> data;

    @SerializedName("status")
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResponseType> getData() {
        return data;
    }

    public void setData(List<ResponseType> data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
