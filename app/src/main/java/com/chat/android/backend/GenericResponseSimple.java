package com.chat.android.backend;

import com.google.gson.annotations.SerializedName;

public class GenericResponseSimple<ResponseType> {

    @SerializedName("Message")
    private String message;

    @SerializedName("data")
    private ResponseType data;

    @SerializedName("Result")
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseType getData() {
        return data;
    }

    public void setData(ResponseType data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
