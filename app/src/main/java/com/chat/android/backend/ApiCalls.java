package com.chat.android.backend;

import android.content.Context;
import android.util.Log;

import com.chat.android.app.activity.SendPaymentFragmentStep3;
import com.chat.android.app.model.ApiRequestModel;
import com.chat.android.app.model.CashPLusUpdateUserProfileResponse_Model;
import com.chat.android.app.model.CashPlusLoginResponseModel;
import com.chat.android.app.model.CashplusLoginModel;
import com.chat.android.app.model.CashplusUpdateUserProfile;
import com.chat.android.app.model.GetBankBranches;
import com.chat.android.app.model.GetBankPayoutLocations;
import com.chat.android.app.model.GetCashPayoutLocations;
import com.chat.android.app.model.GetCities;
import com.chat.android.app.model.GetContactsModel;
import com.chat.android.app.model.GetDestinations;
import com.chat.android.app.model.GetIDCardTypes;
import com.chat.android.app.model.GetOccupationTypes;
import com.chat.android.app.model.GetPaymentMethods;
import com.chat.android.app.model.GetPriceOffer;
import com.chat.android.app.model.GetProcessResponse;
import com.chat.android.app.model.GetSendingReasons;
import com.chat.android.app.model.MobileUserVerification;
import com.chat.android.app.model.ProcessTransactionModel;
import com.chat.android.app.model.RegisterMobileUser;
import com.chat.android.app.model.RegisterCashPlusResponse;
import com.chat.android.app.model.SaveContactModel;
import com.chat.android.app.model.SetUserPasswordModel;
import com.chat.android.app.model.PasswordResetModel;
import com.chat.android.app.model.VerificationPinResponseModel;
import com.chat.android.app.model.meetup.GetNearByMeetupsResponse;
import com.chat.android.app.model.meetup.UpdateMeetupPostData;
import com.chat.android.app.model.meetup.UpdateMeetupResponseData;
import com.chat.android.app.model.updateCallStatusModel;
import com.chat.android.app.utils.MyLog;
import com.chat.android.interfaces.INetworkResponseListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;


public class ApiCalls {
    private static String TAG = "ApiCalls";
    private static ApiCalls instance;
    private Context mContextview;

    private ApiCalls(final Context mContextview) {
        if (this.mContextview == null)
            this.mContextview = mContextview;
    }

    public static ApiCalls getInstance(final Context context) {
        if (instance == null) {
            instance = new ApiCalls(context);
        }
        return instance;
    }

    public void BroadcastErrorType(INetworkResponseListener iNetworkResponseListener, int Responsecode, String responsehandler) {

        switch (Responsecode) {
            case 404:
                iNetworkResponseListener.onNetworkResponse(false, "Server Error 404 ", responsehandler, null);
                break;
            case 500:
                iNetworkResponseListener.onNetworkResponse(false, "Server Error 505 ", responsehandler, null);
                break;
            default:
                iNetworkResponseListener.onNetworkResponse(false, "Unknown Server Error ", responsehandler, null);
                break;
        }


    }

    public void GetDestinations(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetDestinations> call = ApiServices.getGenerixMISService(context, false).GetDestinations(apiRequestModel);
        call.enqueue(new Callback<GetDestinations>() {
            @Override
            public void onResponse(Call<GetDestinations> call, Response<GetDestinations> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.Destinations, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.Destinations, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.Destinations, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.Destinations);
                }
            }

            @Override
            public void onFailure(Call<GetDestinations> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.Destinations, null);

            }
        });
    }

    public void GetCities(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetCities> call = ApiServices.getGenerixMISService(context, false).GetCities(apiRequestModel);
        call.enqueue(new Callback<GetCities>() {
            @Override
            public void onResponse(Call<GetCities> call, Response<GetCities> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.Cities, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.Cities, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.Cities, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.Cities);
                }
            }

            @Override
            public void onFailure(Call<GetCities> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.Cities, null);

            }
        });
    }

    public void GetContacts(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetContactsModel> call = ApiServices.getGenerixMISService(context, false).GetContacts(apiRequestModel);
        call.enqueue(new Callback<GetContactsModel>() {
            @Override
            public void onResponse(Call<GetContactsModel> call, Response<GetContactsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.GetContacts, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.GetContacts, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.GetContacts, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.GetContacts);
                }
            }

            @Override
            public void onFailure(Call<GetContactsModel> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.GetContacts, null);

            }
        });
    }

    public void GetContactsPaymentMethod(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetPaymentMethods> call = ApiServices.getGenerixMISService(context, false).GetContactPaymentMethod(apiRequestModel);
        call.enqueue(new Callback<GetPaymentMethods>() {
            @Override
            public void onResponse(Call<GetPaymentMethods> call, Response<GetPaymentMethods> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.GetContactPaymentMethod, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.GetContactPaymentMethod, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.GetContactPaymentMethod, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.GetContactPaymentMethod);
                }
            }

            @Override
            public void onFailure(Call<GetPaymentMethods> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.GetContactPaymentMethod, null);

            }
        });
    }

    public void GetIDCardTypes(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetIDCardTypes> call = ApiServices.getGenerixMISService(context, false).GetIDCardTypes(apiRequestModel);
        call.enqueue(new Callback<GetIDCardTypes>() {
            @Override
            public void onResponse(Call<GetIDCardTypes> call, Response<GetIDCardTypes> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.IDTypes, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.IDTypes, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.IDTypes, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.IDTypes);
                }
            }

            @Override
            public void onFailure(Call<GetIDCardTypes> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.IDTypes, null);

            }
        });
    }

    public void GetOccupationTypes(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetOccupationTypes> call = ApiServices.getGenerixMISService(context, false).GetOccupationTypes(apiRequestModel);
        call.enqueue(new Callback<GetOccupationTypes>() {
            @Override
            public void onResponse(Call<GetOccupationTypes> call, Response<GetOccupationTypes> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.OccupationTypes, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.OccupationTypes, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.OccupationTypes, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.OccupationTypes);
                }
            }

            @Override
            public void onFailure(Call<GetOccupationTypes> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.OccupationTypes, null);

            }
        });
    }

    public void GetCashPayoutLocations(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetCashPayoutLocations> call = ApiServices.getGenerixMISService(context, false).GetCashPayoutLocations(apiRequestModel);
        call.enqueue(new Callback<GetCashPayoutLocations>() {
            @Override
            public void onResponse(Call<GetCashPayoutLocations> call, Response<GetCashPayoutLocations> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.CashPayoutLocations, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.CashPayoutLocations, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.CashPayoutLocations, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.CashPayoutLocations);
                }
            }

            @Override
            public void onFailure(Call<GetCashPayoutLocations> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.CashPayoutLocations, null);

            }
        });
    }

    public void GetPriceOffer(final Context context, @Body SendPaymentFragmentStep3.SetPriceModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetPriceOffer> call = ApiServices.getGenerixMISService(context, false).GetPriceOffer(apiRequestModel);
        call.enqueue(new Callback<GetPriceOffer>() {
            @Override
            public void onResponse(Call<GetPriceOffer> call, Response<GetPriceOffer> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.GetPriceOffer, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.GetPriceOffer, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.GetPriceOffer, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.GetPriceOffer);
                }
            }

            @Override
            public void onFailure(Call<GetPriceOffer> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.GetPriceOffer, null);

            }
        });
    }

    public void ProcessTransaction(final Context context, @Body ProcessTransactionModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetProcessResponse> call = ApiServices.getGenerixMISService(context, false).ProcessTransaction(apiRequestModel);
        call.enqueue(new Callback<GetProcessResponse>() {
            @Override
            public void onResponse(Call<GetProcessResponse> call, Response<GetProcessResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.ProcessTransactions, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.ProcessTransactions, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.ProcessTransactions, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.ProcessTransactions);
                }
            }

            @Override
            public void onFailure(Call<GetProcessResponse> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.ProcessTransactions, null);

            }
        });
    }

    public void SaveContact(final Context context, @Body SaveContactModel saveContactModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<SaveContactModel> call = ApiServices.getGenerixMISService(context, false).SaveContact(saveContactModel);
        call.enqueue(new Callback<SaveContactModel>() {
            @Override
            public void onResponse(Call<SaveContactModel> call, Response<SaveContactModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.SaveContact, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.SaveContact, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.SaveContact, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.SaveContact);
                }
            }

            @Override
            public void onFailure(Call<SaveContactModel> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.SaveContact, null);

            }
        });
    }


    public void GetBanksPayoutLocations(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetBankPayoutLocations> call = ApiServices.getGenerixMISService(context, false).GetBanksPayoutLocations(apiRequestModel);
        call.enqueue(new Callback<GetBankPayoutLocations>() {
            @Override
            public void onResponse(Call<GetBankPayoutLocations> call, Response<GetBankPayoutLocations> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.BankPayoutLocations, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.BankPayoutLocations, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.BankPayoutLocations, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.BankPayoutLocations);
                }
            }

            @Override
            public void onFailure(Call<GetBankPayoutLocations> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.BankPayoutLocations, null);

            }
        });
    }

    public void GetBankBranches(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetBankBranches> call = ApiServices.getGenerixMISService(context, false).GetBankBranches(apiRequestModel);
        call.enqueue(new Callback<GetBankBranches>() {
            @Override
            public void onResponse(Call<GetBankBranches> call, Response<GetBankBranches> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.Branches, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.Branches, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.Branches, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.Branches);
                }
            }

            @Override
            public void onFailure(Call<GetBankBranches> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.Branches, null);

            }
        });
    }

    public void GetSendingReasonsByCountry(final Context context, @Body ApiRequestModel apiRequestModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<GetSendingReasons> call = ApiServices.getGenerixMISService(context, false).GetSendingReasonsByCountry(apiRequestModel);
        call.enqueue(new Callback<GetSendingReasons>() {
            @Override
            public void onResponse(Call<GetSendingReasons> call, Response<GetSendingReasons> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.SendingReasons, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.SendingReasons, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.SendingReasons, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.SendingReasons);
                }
            }

            @Override
            public void onFailure(Call<GetSendingReasons> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.SendingReasons, null);

            }
        });
    }

    public void Login(final Context context, CashplusLoginModel cashplusLoginModel, final INetworkResponseListener iNetworkResponseListener) {

        Call<CashPlusLoginResponseModel> call = ApiServices.getGenerixMISService(context, false).Login(cashplusLoginModel);
        call.enqueue(new Callback<CashPlusLoginResponseModel>() {
            @Override
            public void onResponse(Call<CashPlusLoginResponseModel> call, Response<CashPlusLoginResponseModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.Login, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.Login, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.Login, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.Login);
                }
            }

            @Override
            public void onFailure(Call<CashPlusLoginResponseModel> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.Login, null);

            }
        });
    }

    public void RegisterMobileUser(final Context context, String Number, String IMEI, final INetworkResponseListener iNetworkResponseListener) {
        RegisterMobileUser registerMobileUser = new RegisterMobileUser(Number, IMEI);
        Call<RegisterCashPlusResponse> call = ApiServices.getGenerixMISService(context, false).RegisterMobileUser(registerMobileUser);
        call.enqueue(new Callback<RegisterCashPlusResponse>() {
            @Override
            public void onResponse(Call<RegisterCashPlusResponse> call, Response<RegisterCashPlusResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.RegisterMobileUser, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.RegisterMobileUser, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.RegisterMobileUser, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.RegisterMobileUser);
                }

            }


            @Override
            public void onFailure(Call<RegisterCashPlusResponse> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.RegisterMobileUser, null);

            }

        });
    }

    public void SetUserPassword(final Context context, SetUserPasswordModel setUserPasswordModel, final INetworkResponseListener iNetworkResponseListener) {
        Call<PasswordResetModel> call = ApiServices.getGenerixMISService(context, false).SetUserPassword(setUserPasswordModel);
        call.enqueue(new Callback<PasswordResetModel>() {
            @Override
            public void onResponse(Call<PasswordResetModel> call, Response<PasswordResetModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.NewPassword, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.NewPassword, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.NewPassword, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.NewPassword);
                }

            }


            @Override
            public void onFailure(Call<PasswordResetModel> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.NewPassword, null);

            }

        });
    }

    public void VerifyMobileUser(final Context context, String Number, String IMEI, Integer pincode, String company, final INetworkResponseListener iNetworkResponseListener) {
        MobileUserVerification mobileUserVerification = new MobileUserVerification();
        Call<VerificationPinResponseModel> call = ApiServices.getGenerixMISService(context, false).VerifyMobileUser(Number, IMEI, pincode, company);
        call.enqueue(new Callback<VerificationPinResponseModel>() {
            @Override
            public void onResponse(Call<VerificationPinResponseModel> call, Response<VerificationPinResponseModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.Verification, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.Verification, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.Verification, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.RegisterMobileUser);
                }

            }


            @Override
            public void onFailure(Call<VerificationPinResponseModel> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.RegisterMobileUser, null);

            }

        });
    }

    public void CashPLusUpdateUserProfile(final Context context, CashplusUpdateUserProfile cashplusUpdateUserProfile, final INetworkResponseListener iNetworkResponseListener) {
        Gson gson = new Gson();
        String json = gson.toJson(cashplusUpdateUserProfile);
        Log.e("model", json);
        Call<CashPLusUpdateUserProfileResponse_Model> call = ApiServices.getGenerixMISService(context, false).CashPLusUpdateUserProfile(cashplusUpdateUserProfile);
        call.enqueue(new Callback<CashPLusUpdateUserProfileResponse_Model>() {
            @Override
            public void onResponse(Call<CashPLusUpdateUserProfileResponse_Model> call, Response<CashPLusUpdateUserProfileResponse_Model> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body() != null) {
                            iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.CashPLusUpdateUserProfile, response.body());
                        } else {
                            iNetworkResponseListener.onNetworkResponse(false, "No Data", Constants.CashPLusUpdateUserProfile, null);
                        }
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.CashPLusUpdateUserProfile, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.CashPLusUpdateUserProfile);
                }

            }


            @Override
            public void onFailure(Call<CashPLusUpdateUserProfileResponse_Model> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.CashPLusUpdateUserProfile, null);

            }

        });
    }

    public void updateCallStatus(final Context context, String roomId) {

        Call<ResponseBody> call = ApiServices.getGenerixMISService(context, true).updateCallStatus(roomId);
        MyLog.d("UpdateCallStatus roomId " + roomId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        MyLog.e(TAG, "updateCallStatus onResponse " + response.body().string());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                MyLog.e(TAG, "updateCallStatus onFailure " + t.getMessage() + " " + t.getLocalizedMessage());
            }
        });
    }


    public void updateMeetup(final Context context, String user_id, String name, double lat, double lng, String status, String bio, String dob, final INetworkResponseListener iNetworkResponseListener) {
        UpdateMeetupPostData updateMeetupPostData = new UpdateMeetupPostData(user_id, name, lat, lng, status, bio, dob);
        Call<UpdateMeetupResponseData> call = ApiServices.getGenerixMISService(context, true).updateMeetup(updateMeetupPostData);
        call.enqueue(new Callback<UpdateMeetupResponseData>() {
            @Override
            public void onResponse(Call<UpdateMeetupResponseData> call, Response<UpdateMeetupResponseData> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        iNetworkResponseListener.onNetworkResponse(true, "Has Data", Constants.UpdateMeetupPostData, response.body());
                    } else {
                        iNetworkResponseListener.onNetworkResponse(false, "Response Body is Empty", Constants.UpdateMeetupPostData, null);
                    }
                } else {
                    BroadcastErrorType(iNetworkResponseListener, response.code(), Constants.UpdateMeetupPostData);
                }
            }

            @Override
            public void onFailure(Call<UpdateMeetupResponseData> call, Throwable t) {
                iNetworkResponseListener.onNetworkResponse(false, "Something went wrong", Constants.RegisterMobileUser, null);
            }
        });

    }

    public void getNearByMeetups(
            final Context context,
            String latitude,
            String longitude,
            int maxDistance,
            int limit,
            String user_id,
            Callback<GetNearByMeetupsResponse> callback) {
        Call<GetNearByMeetupsResponse> callNearbyMeetupUsers = ApiServices.getGenerixMISService(context, true).getNearByMeetups(latitude, longitude, maxDistance+"", limit+"", user_id);
        callNearbyMeetupUsers.enqueue(callback);
    }

    public void acceptCall(final Context context, String roomId, int callStatus) {

        Call<updateCallStatusModel> call = ApiServices.getGenerixMISService(context, true).acceptCall(roomId, callStatus);
        call.enqueue(new Callback<updateCallStatusModel>() {
            @Override
            public void onResponse(Call<updateCallStatusModel> call, Response<updateCallStatusModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        MyLog.e(TAG, "acceptCall onResponse " + response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<updateCallStatusModel> call, Throwable t) {
                MyLog.e(TAG, "acceptCall onFailure " + t.getMessage());
            }
        });
    }

    public void cancelCallFromNotification(final Context context, String roomId, String userId) {

        Call<updateCallStatusModel> call = ApiServices.getGenerixMISService(context, true).cancelCallFromNotification(roomId, userId);
        call.enqueue(new Callback<updateCallStatusModel>() {
            @Override
            public void onResponse(Call<updateCallStatusModel> call, Response<updateCallStatusModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        MyLog.e(TAG, "acceptCall onResponse " + response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<updateCallStatusModel> call, Throwable t) {
                MyLog.e(TAG, "acceptCall onFailure " + t.getMessage());
            }
        });
    }

    public void checkCallStatus(final Context context, String recordId) {
        MyLog.e(TAG, "checkCallStatus " + recordId);

        Call<ResponseBody> call = ApiServices.getGenerixMISService(context, true).checkCallStatus(recordId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String responseStr = response.body().string();
                            MyLog.e(TAG, "checkCallStatus onResponse " + responseStr);
                            JSONObject jsonObject = new JSONObject(responseStr);
                            String callStatus = "";
                            if (jsonObject.has("call_status"))
                                callStatus = jsonObject.getString("call_status");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                MyLog.e(TAG, "checkCallStatus onFailure " + t.getMessage());
            }
        });
    }

    public void getAgoraToken(
            final Context context,
            String channelName,
            Callback<ResponseBody> callback) {
        Call<ResponseBody> callAgora = ApiServices.getGenerixMISService(context, true).getAgoraToken(true, channelName);
        callAgora.enqueue(callback);
    }

    public void joinCall(
            final Context context,
            String roomId,
            boolean isReceiver,
            Callback<ResponseBody> callback) {
        Call<ResponseBody> callAgora = ApiServices.getGenerixMISService(context, true).joinCall(roomId, "loopback", isReceiver);
        callAgora.enqueue(callback);
    }

    public void leaveCall(
            final Context context,
            String roomId,
            String clientId,
            Callback<ResponseBody> callback) {
        Call<ResponseBody> callAgora = ApiServices.getGenerixMISService(context, true).leaveCall(roomId, clientId, "loopback");
        callAgora.enqueue(callback);
    }
}
