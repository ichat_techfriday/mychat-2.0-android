package com.chat.android.backend;

public class Constants {
    public static final String Destinations = "Destinations";
    public static final String Cities = "Cities";
    public static final String Branches = "Branches";
    public static final String CashPayoutLocations = "CashPayoutLocations";
    public static final String BankPayoutLocations = "BankPayoutLocations";
    public static final String GetPriceOffer = "GetPriceOffer";
    public static final String Login = "Login";
    public static final String RegisterMobileUser =  "RegisterMobileUser";
    public static final String Verification= "Verification";
    public static final String NewPassword= "NewPassword";
    public static final String CashPLusUpdateUserProfile= "CashPLusUpdateUserProfile";
    public static final String myChat= "myChat";
    public static final String LebononPhone= "+96171000040";
    public static final String TestingIMEI= "5ad34f0b-2634-4190-94f7-1f5551839e32";
    public static final String Salty= "Cp@SaltyP@ssw0rd";
    public static final String IDTypes= "IDTypes";
    public static final String OccupationTypes= "OccupationTypes";
    public static final String SendingReasons= "SendingReasons";
    public static final String ProcessTransactions= "ProcessTransactions";
    public static final String SaveContact= "SaveContact";
    public static final String GetContacts= "GetContacts";
    public static final String GetContactPaymentMethod= "GetContactPaymentMethod";

    public static final String UpdateMeetupPostData =  "UpdateMeetupPostData";
    public static final String GetNearByMeetups =  "GetNearByMeetups";

}
