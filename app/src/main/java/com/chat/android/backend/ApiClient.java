package com.chat.android.backend;

import android.content.Context;


import com.chat.android.BuildConfig;
import com.chat.android.app.utils.MyLog;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static Retrofit retrofitClient;
    public static synchronized Retrofit getApiClient(Context context, boolean callStatusUpdate) {
        String apiEndPoint = "";
        if (callStatusUpdate){
            apiEndPoint = BuildConfig.BASE_URL;
        }else{
            apiEndPoint = "https://" +"rms.cashpluslb.com:1189"+ "/";
        }
        MyLog.d("apiEndPoint ", apiEndPoint);
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(1, TimeUnit.MINUTES);
        builder.connectTimeout(15, TimeUnit.SECONDS);
        builder.writeTimeout(1, TimeUnit.MINUTES);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        retrofitClient = new Retrofit.Builder()
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(apiEndPoint)
                .build();

        return retrofitClient;
    }
}