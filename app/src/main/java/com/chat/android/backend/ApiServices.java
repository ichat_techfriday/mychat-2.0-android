package com.chat.android.backend;

import android.content.Context;

import com.chat.android.app.activity.SendPaymentFragmentStep3;
import com.chat.android.app.model.ApiRequestModel;
import com.chat.android.app.model.CashPLusUpdateUserProfileResponse_Model;
import com.chat.android.app.model.CashPlusLoginResponseModel;
import com.chat.android.app.model.CashplusLoginModel;
import com.chat.android.app.model.CashplusUpdateUserProfile;
import com.chat.android.app.model.GetBankBranches;
import com.chat.android.app.model.GetBankPayoutLocations;
import com.chat.android.app.model.GetCashPayoutLocations;
import com.chat.android.app.model.GetCities;
import com.chat.android.app.model.GetContactsModel;
import com.chat.android.app.model.GetDestinations;
import com.chat.android.app.model.GetIDCardTypes;
import com.chat.android.app.model.GetOccupationTypes;
import com.chat.android.app.model.GetPaymentMethods;
import com.chat.android.app.model.GetPriceOffer;
import com.chat.android.app.model.GetProcessResponse;
import com.chat.android.app.model.GetSendingReasons;
import com.chat.android.app.model.ProcessTransactionModel;
import com.chat.android.app.model.RegisterMobileUser;
import com.chat.android.app.model.RegisterCashPlusResponse;
import com.chat.android.app.model.SaveContactModel;
import com.chat.android.app.model.SetUserPasswordModel;
import com.chat.android.app.model.PasswordResetModel;
import com.chat.android.app.model.VerificationPinResponseModel;
import com.chat.android.app.model.meetup.GetNearByMeetupsResponse;
import com.chat.android.app.model.meetup.UpdateMeetupPostData;
import com.chat.android.app.model.meetup.UpdateMeetupResponseData;
import com.chat.android.app.model.updateCallStatusModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiServices {

    private static GenerixMISService generixMISService;

    static GenerixMISService getGenerixMISService(Context context, boolean callStatusupdate) {

        generixMISService = ApiClient.getApiClient(context, callStatusupdate)
                .create(GenerixMISService.class);
        return generixMISService;

    }


    public interface GenerixMISService {
        @POST("/api3/RemittanceSend/GetDestinations")
        Call<GetDestinations> GetDestinations(@Body ApiRequestModel apiRequestModel);

        @POST("/api3/RemittanceSend/GetCitiesByCountry")
        Call<GetCities> GetCities(@Body ApiRequestModel apiRequestModel);

        @POST("/api3/RemittanceSend/GetContacts")
        Call<GetContactsModel> GetContacts(@Body ApiRequestModel apiRequestModel);

        @POST("/api3/RemittanceSend/GetContactPaymentMethod")
        Call<GetPaymentMethods> GetContactPaymentMethod(@Body ApiRequestModel apiRequestModel);

        @POST("/api3/RemittanceSend/GetIDCardTypes")
        Call<GetIDCardTypes> GetIDCardTypes(@Body ApiRequestModel apiRequestModel);

        @POST("/api3/RemittanceSend/GetOccupationTypes")
        Call<GetOccupationTypes> GetOccupationTypes(@Body ApiRequestModel apiRequestModel);

        @POST("/api3/RemittanceSend/GetCashPayoutLocationsBycountryAndCity")
        Call<GetCashPayoutLocations> GetCashPayoutLocations(@Body ApiRequestModel apiRequestModel);

        @POST("/api3/RemittanceSend/GetPriceOfferByCountryAndPM")
        Call<GetPriceOffer> GetPriceOffer(@Body SendPaymentFragmentStep3.SetPriceModel apiRequestModel);

        @POST("/api3/RemittanceSend/ProcessQuickMobileTransaction")
        Call<GetProcessResponse> ProcessTransaction(@Body ProcessTransactionModel processTransactionModel);

        @POST("/api3/RemittanceSend/SaveContact")
        Call<SaveContactModel> SaveContact(@Body SaveContactModel saveContactModel);

        @POST("/api3/RemittanceSend/GetBankBranchesByBankCode")
        Call<GetBankBranches> GetBankBranches(@Body ApiRequestModel setUserPasswordModel);

        @POST("/api3/RemittanceSend/GetSendingReasonsByCountry")
        Call<GetSendingReasons> GetSendingReasonsByCountry(@Body ApiRequestModel apiRequestModel);

        @POST("/api3/RemittanceSend/GetBanksByCountry")
        Call<GetBankPayoutLocations> GetBanksPayoutLocations(@Body ApiRequestModel apiRequestModel);

        @POST("/api3/Agents/Login")
        Call<CashPlusLoginResponseModel> Login(@Body CashplusLoginModel cashplusLoginModel);

        @POST("/api3/Agents/RegisterMobileUser")
        Call<RegisterCashPlusResponse> RegisterMobileUser(@Body RegisterMobileUser registerMobileUser);

        @POST("/api3/Agents/VerifyMobileUser")
        Call<VerificationPinResponseModel> VerifyMobileUser(@Query("MobileNumber") String mobileNumber, @Query("IMEI") String imei, @Query("PinCode") Integer pincode, @Query("Company") String company);

        @POST("/api3/Agents/SetUserPassword")
        Call<PasswordResetModel> SetUserPassword(@Body SetUserPasswordModel setUserPasswordModel);

        @POST("/api3/Agents/UpdateUserProfile")
        Call<CashPLusUpdateUserProfileResponse_Model> CashPLusUpdateUserProfile(@Body CashplusUpdateUserProfile cashplusUpdateUserProfile);

        @GET("/api/voip/ack")
        Call<ResponseBody> updateCallStatus(@Query("roomId") String roomId);

        @POST("/api/meetup")
        Call<UpdateMeetupResponseData> updateMeetup(@Body UpdateMeetupPostData updateMeetupPostData);

        @GET("/api/meetup/nearby?")
        Call<GetNearByMeetupsResponse> getNearByMeetups(@Query("lat") String latitude,
                                                        @Query("lng") String longitude,
                                                        @Query("maxDistance") String maxDistance,
                                                        @Query("limit") String limit,
                                                        @Query("user_id") String userId);

        @GET("/api/call/accept")
        Call<updateCallStatusModel> acceptCall(@Query("roomid") String roomId, @Query("call_status") int callStatus);

        @GET("/api/cancelCallFromNotification")
        Call<updateCallStatusModel> cancelCallFromNotification(@Query("roomid") String roomId, @Query("user_id") String userId);

        @GET("/api/status")
        Call<ResponseBody> checkCallStatus(@Query("recordId") String recordId);

        @GET("/api/agora/token")
        Call<ResponseBody> getAgoraToken(@Query("isPublisher") boolean isPublisher,
                                         @Query("channel") String channel);

        @POST("/join/{ROOM_ID}")
        Call<ResponseBody> joinCall(@Path("ROOM_ID") String roomId, @Query("debug") String debug, @Query("isReceiver") boolean isReceiver);


        @POST("/leave/{ROOM_ID}/{CLIENT_ID}")
        Call<ResponseBody> leaveCall(@Path("ROOM_ID") String roomId, @Path("CLIENT_ID") String clientId, @Query("debug") String debug);


    }

}
