package com.chat.android.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;

import android.os.Build;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;

import com.chat.android.R;
import com.chat.android.app.activity.NewHomeScreenActivty;
import com.chat.android.app.calls.IncomingCallActivity;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.backend.ApiCalls;
import com.chat.android.core.CoreController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.service.CallNotificationActionReceiver;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.StorageUtility;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

//import android.support.v7.app.NotificationCompat;

/**
 * Created by user145 on 8/4/2017.
 */
public class NotificationUtils {

    private static String TAG = NotificationUtils.class.getSimpleName();

    private Context mContext;
    private static String CHANNEL_ID = CoreController.mcontext.getString(R.string.app_name) + "CallChannel";
    private static String CHANNEL_ID2 = CoreController.mcontext.getString(R.string.app_name) + "CallChannel2";
    private static String CHANNEL_NAME = CoreController.mcontext.getString(R.string.app_name) + "Call Channel";
    private static String CHANNEL_NAME2 = CoreController.mcontext.getString(R.string.app_name) + "Call Channel2";
    private static boolean notifySoundEnabled = true;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    public static NotificationCompat.Builder generateNotificationBuilder(Context mContext, String docId, String callType, String name, String fromUserId, String toUserId, String fromUserMSISDN, String callRoomId, String recordId, boolean isVideoCall, String callTimestamp) {
        try {
            MyLog.d(TAG, "generateNotificationBuilder: recordId "+ recordId);
            ApiCalls.getInstance(mContext).updateCallStatus(mContext, callRoomId);
            int NOTIFICATION_ID = getNotificationID(recordId);
            Intent receiveCallAction = new Intent(mContext, CallNotificationActionReceiver.class);

            receiveCallAction = getIntent(receiveCallAction, NOTIFICATION_ID, docId, fromUserId, toUserId, fromUserMSISDN, callRoomId, recordId, isVideoCall, callTimestamp, IncomingCallActivity.RECEIVE_CALL);
            receiveCallAction.putExtra("ConstantApp.CALL_RESPONSE_ACTION_KEY", "ConstantApp.CALL_RECEIVE_ACTION");

            Intent cancelCallAction = new Intent(mContext, CallNotificationActionReceiver.class);
            cancelCallAction = getIntent(cancelCallAction, NOTIFICATION_ID, docId, fromUserId, toUserId, fromUserMSISDN, callRoomId, recordId, isVideoCall, callTimestamp, IncomingCallActivity.CANCEL_CALL);
            cancelCallAction.putExtra("ConstantApp.CALL_RESPONSE_ACTION_KEY", "ConstantApp.CALL_CANCEL_ACTION");
//          cancelCallAction.putExtra("ConstantApp.CALL_RESPONSE_ACTION_KEY", "ConstantApp.CALL_RECEIVE_ACTION");

            Intent callDialogAction = new Intent(mContext, CallNotificationActionReceiver.class);
            callDialogAction = getIntent(callDialogAction, NOTIFICATION_ID, docId, fromUserId, toUserId, fromUserMSISDN, callRoomId, recordId, isVideoCall, callTimestamp, IncomingCallActivity.DIALOG_CALL);

            PendingIntent receiveCallPendingIntent;
            PendingIntent cancelCallPendingIntent;
            PendingIntent callDialogPendingIntent;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                receiveCallPendingIntent = PendingIntent.getBroadcast(mContext, 1200, receiveCallAction, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
                cancelCallPendingIntent = PendingIntent.getBroadcast(mContext, 1201, cancelCallAction, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
                callDialogPendingIntent = PendingIntent.getBroadcast(mContext, 1202, callDialogAction, PendingIntent.FLAG_IMMUTABLE|PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                receiveCallPendingIntent = PendingIntent.getBroadcast(mContext, 1200, receiveCallAction, PendingIntent.FLAG_UPDATE_CURRENT);
                cancelCallPendingIntent = PendingIntent.getBroadcast(mContext, 1201, cancelCallAction, PendingIntent.FLAG_UPDATE_CURRENT);
                callDialogPendingIntent = PendingIntent.getBroadcast(mContext, 1202, callDialogAction, PendingIntent.FLAG_UPDATE_CURRENT);
            }

            String callTypeToDisplay = mContext.getString(R.string.audio);
            if (callType.equals(MessageFactory.video_call + "")) {
                callTypeToDisplay = mContext.getString(R.string.video);
            }
            if (!StorageUtility.getDataFromPreferences(mContext.getApplicationContext(), AppConstants.SPKeys.SOUNDS.getValue(), true))
                notifySoundEnabled = false;


            AppUtils.createChannel(mContext, CHANNEL_ID, CHANNEL_NAME);
            AppUtils.createChannel2(mContext, CHANNEL_ID2, CHANNEL_NAME2);
            NotificationCompat.Builder notificationBuilder = null;

            Uri ringUri = Settings.System.DEFAULT_RINGTONE_URI;
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                callDialogAction = new Intent(mContext, IncomingCallActivity.class);
                callDialogAction = getIntent(callDialogAction, NOTIFICATION_ID, docId, fromUserId, toUserId, fromUserMSISDN, callRoomId, recordId, isVideoCall, callTimestamp, IncomingCallActivity.DIALOG_CALL);

                receiveCallAction = new Intent(mContext, IncomingCallActivity.class);
                receiveCallAction = getIntent(receiveCallAction, NOTIFICATION_ID, docId, fromUserId, toUserId, fromUserMSISDN, callRoomId, recordId, isVideoCall, callTimestamp, IncomingCallActivity.RECEIVE_CALL);
                receiveCallAction.putExtra("ConstantApp.CALL_RESPONSE_ACTION_KEY", "ConstantApp.CALL_RECEIVE_ACTION");

                cancelCallAction = new Intent(mContext, IncomingCallActivity.class);
                cancelCallAction = getIntent(cancelCallAction, NOTIFICATION_ID, docId, fromUserId, toUserId, fromUserMSISDN, callRoomId, recordId, isVideoCall, callTimestamp, IncomingCallActivity.CANCEL_CALL);
                cancelCallAction.putExtra("ConstantApp.CALL_RESPONSE_ACTION_KEY", "ConstantApp.CALL_CANCEL_ACTION");

                Intent backIntent = new Intent(mContext, NewHomeScreenActivty.class);
                backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                callDialogPendingIntent = PendingIntent.getActivities(
                        mContext, 1201,
                        new Intent[]{backIntent, callDialogAction}, PendingIntent.FLAG_ONE_SHOT|PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE);

                cancelCallPendingIntent = PendingIntent.getActivities(
                        mContext, 1200,
                        new Intent[]{backIntent, cancelCallAction}, PendingIntent.FLAG_ONE_SHOT|PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE);

                receiveCallPendingIntent = PendingIntent.getActivities(
                        mContext, 1202,
                        new Intent[]{backIntent, receiveCallAction}, PendingIntent.FLAG_ONE_SHOT|PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE);

                notificationBuilder = new NotificationCompat.Builder(mContext, notifySoundEnabled ? CHANNEL_ID: CHANNEL_ID2)
                        .setContentTitle(name)
                        .setContentText(mContext.getString(R.string.incoming) + " " + callTypeToDisplay + " " + mContext.getString(R.string.call))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setCategory(NotificationCompat.CATEGORY_CALL)
                        .addAction(R.drawable.ic_reject_call, mContext.getString(R.string.reject_call), cancelCallPendingIntent)
                        .addAction(R.drawable.ic_accept_call, mContext.getString(R.string.answer_call), receiveCallPendingIntent)
                        .setAutoCancel(true)
                        .setFullScreenIntent(callDialogPendingIntent, true)
                        .setDefaults(Notification.DEFAULT_VIBRATE);
            } else {
                notificationBuilder = new NotificationCompat.Builder(mContext, notifySoundEnabled ? CHANNEL_ID: CHANNEL_ID2)
                        .setContentTitle(name)
                        .setContentText(mContext.getString(R.string.incoming) + " " + callTypeToDisplay + " " + mContext.getString(R.string.call))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setCategory(NotificationCompat.CATEGORY_CALL)
                        .addAction(R.drawable.ic_reject_call, mContext.getString(R.string.reject_call), cancelCallPendingIntent)
                        .addAction(R.drawable.ic_accept_call, mContext.getString(R.string.answer_call), receiveCallPendingIntent)
                        .setAutoCancel(true)
                        .setFullScreenIntent(callDialogPendingIntent, true)
                        .setDefaults(Notification.DEFAULT_VIBRATE);
            }

            return notificationBuilder;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Intent getIntent(Intent intent, int NOTIFICATION_ID, String docId, String fromUserId, String toUserId, String fromUserMSISDN, String callRoomId, String recordId, boolean isVideoCall, String callTimestamp, String action) {
        intent.putExtra(AppConstants.NOTIFICATION_ID, NOTIFICATION_ID);
        intent.putExtra(IncomingCallActivity.EXTRA_DOC_ID, docId);
        intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_ID, fromUserId);
        intent.putExtra(IncomingCallActivity.EXTRA_TO_USER_ID, toUserId);
        intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_MSISDN, fromUserMSISDN);
        intent.putExtra(IncomingCallActivity.EXTRA_CALL_ROOM_ID, callRoomId);
        intent.putExtra(IncomingCallActivity.EXTRA_CALL_RECORD_ID, recordId);
        intent.putExtra(IncomingCallActivity.EXTRA_CALL_TYPE, isVideoCall);
        intent.putExtra(IncomingCallActivity.EXTRA_CALL_TIME_STAMP, callTimestamp);
        intent.putExtra(IncomingCallActivity.EXTRA_ACTION_TYPE, action);
        intent.putExtra(IncomingCallActivity.EXTRA_CALL_ACTION, action);
        intent.setAction(action);

        return intent;
    }

    public static int getNotificationID(String recordId) {
        int notificationId = com.chat.android.utils.TextUtils.getIntValue(recordId);
        if (notificationId == -1)
            notificationId = 120; //default notification id
        return notificationId;
    }

    public void showNotificationMessage(String title, String message, String timeStamp, Intent intent) {
        showNotificationMessage(title, message, timeStamp, intent, null);
    }

    public void showNotificationMessage(final String title, final String message, final String timeStamp, Intent intent, String imageUrl) {
        MyLog.d(TAG, "showNotificationMessage " + title);
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;


        // notification icon
        final int icon = R.mipmap.ic_launcher;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        final PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mContext,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT|PendingIntent.FLAG_MUTABLE
                );

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                mContext);

//        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                + "://" + mContext.getPackageName() + "/raw/notification");


        final Uri alarmSound = Uri.parse("android.resource://com.maidac/"+R.raw.notifysnd);

        if (!TextUtils.isEmpty(imageUrl)) {

            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {

                Bitmap bitmap = getBitmapFromURL(imageUrl);

                if (bitmap != null) {
                    showBigNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                } else {
                    showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
                }
            }
        } else {
            showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound);
            playNotificationSound();
        }
    }


    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
//                .setSound(alarmSound)
                .setSound(Uri.parse("android.resource://com.maidac/"+R.raw.notifysnd))
                .setStyle(inboxStyle)
                .setStyle(new androidx.core.app.NotificationCompat.BigTextStyle().bigText(message))
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(Config.NOTIFICATION_ID, notification);
    }

    private void showBigNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, int icon, String title, String message, String timeStamp, PendingIntent resultPendingIntent, Uri alarmSound) {
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
//                .setSound(alarmSound)
                .setSound(Uri.parse("android.resource://com.maidac/"+R.raw.notifysnd))
                .setStyle(bigPictureStyle)
                .setWhen(getTimeMilliSec(timeStamp))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(Config.NOTIFICATION_ID_BIG_IMAGE, notification);
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            MyLog.e(TAG,"",e);
            return null;
        }
    }

    // Playing notification sound
    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + mContext.getPackageName() + R.raw.notifysnd);
            Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
            r.play();
        } catch (Exception e) {
            MyLog.e(TAG,"",e);
        }
    }



    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            MyLog.e(TAG,"",e);
        }
        return 0;
    }
}
