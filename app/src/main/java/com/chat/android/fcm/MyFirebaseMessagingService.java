package com.chat.android.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.chat.android.R;
import com.chat.android.app.activity.ChatPageActivity;
import com.chat.android.app.activity.NewHomeScreenActivty;
import com.chat.android.app.calls.CallAck;
import com.chat.android.app.calls.CallMessage;
import com.chat.android.app.calls.CallsActivity;
import com.chat.android.app.calls.IncomingCallActivity;
import com.chat.android.app.model.GroupInviteBraodCast;
import com.chat.android.app.model.RejectCallBroadCast;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.AutoDownLoadUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.backend.ApiCalls;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.ShortcutBadgeManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.IncomingMessage;

import com.chat.android.core.message.MessageAck;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.message.MesssageObjectReceiver;
import com.chat.android.core.model.CallItemChat;
import com.chat.android.core.model.ChatLockPojo;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.MuteStatusPojo;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.service.CallNotificationService;
import com.chat.android.core.service.Constants;
import com.chat.android.core.service.ContactsSync;
import com.chat.android.core.socket.MessageService;
import com.chat.android.core.socket.MyPeriodicWork;
import com.chat.android.core.socket.NotificationUtil;
import com.chat.android.core.socket.SocketManager;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import io.socket.client.Socket;

import com.chat.android.utils.AppConstants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import kotlinx.coroutines.GlobalScope;
import me.leolin.shortcutbadger.ShortcutBadger;
import androidx.work.Data;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private Handler incomCallBroadcastHandler;
    public LinkedList<String> messageIds = new LinkedList<>();
    public SocketManager mSocketManager = null;
    private String mCurrentUserId;
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    public static int missedCallCount = 0;
    public static String lastMissedCallId = "";
    private MesssageObjectReceiver objectReceiver;
    private NotificationUtils notificationUtils;
    private Context mContext;
    private Session session;
    String uniqueCurrentID = "";
    private IncomingMessage incomingMsg;
    private UserInfoSession userInfoSession;
    Getcontactname getcontactname;
    public static ArrayList<HashMap<String, Boolean>> chat = new ArrayList<>();
    private SessionManager sessionManager;
    boolean secretchat = false;
    boolean singletchat_boolean = false;
    boolean isMuted = false;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        MyLog.d("Taha+++", "onMessageReceived: Firebase" + remoteMessage.toString());
        mContext = MyFirebaseMessagingService.this;
        boolean isRaad = getResources().getBoolean(R.bool.is_raad);
        sessionManager = SessionManager.getInstance(MyFirebaseMessagingService.this);
        incomingMsg = new IncomingMessage(MyFirebaseMessagingService.this);
        session = new Session(MyFirebaseMessagingService.this);
        userInfoSession = new UserInfoSession(MyFirebaseMessagingService.this);
        getcontactname = new Getcontactname(MyFirebaseMessagingService.this);
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        mCurrentUserId = uniqueCurrentID;
        NotificationUtil.getInstance().init(this);

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            handleNotification(remoteMessage.getNotification().getBody());

        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            try {

                startBackgroundServices();
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                JSONObject data_object = json.getJSONObject("data");
                MyLog.d("Taha+++", "onMessageReceived: data_object = " + data_object.toString());

                String isSilent = null;
                if (data_object.has("is_silent")) {
                    isSilent = data_object.getString("is_silent");
                    MyLog.d("Taha+++", "onMessageReceived: isSilent = "+isSilent);

                    if (isSilent.equals("true")) {
                        EventBus.getDefault().post(new RejectCallBroadCast(data_object.toString()));
                        return;
                    }
                }


                String chatType = "";
                if (data_object.has("message_type"))
                    chatType = data_object.getString("message_type");
                MyLog.d("Taha+++", "onMessageReceived: chatType = "+chatType);
                if (chatType.equalsIgnoreCase("audio_call")) {
                    String callRoomId = data_object.getString("roomid");
                    String recordId = data_object.getString("recordId");

                    ApiCalls.getInstance(mContext).updateCallStatus(mContext, callRoomId);
                    //ApiCalls.getInstance(mContext).checkCallStatus(mContext, recordId);
                }
                String Secretchat = "";
                boolean isSecretMsg = false;
                if (data_object.optString("secret_type").equalsIgnoreCase("yes")) {
                    isSecretMsg = true;
                }
                if (data_object.has("secret_type"))
                    Secretchat = data_object.optString("secret_type");
                secretchat = Secretchat.equalsIgnoreCase("1");

                try {
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    String toId = null;
                    if (data_object.has("convId")) {
                        toId = data_object.getString("convId");
                    }else if (data_object.has("groupId")){
                        toId=data_object.getString("groupId");
                    }
                    if(toId!=null){
                        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, null, toId, secretchat);
                        if (muteData != null && muteData.getMuteStatus().equals("1")) {
                            Date cDate = new Date();
                            isMuted = cDate.getTime() <= muteData.getExpireTs();
                        }
                    }

                    String stat="";
                    String pwd;
                    if(data_object.has("doc_id")) {
                        String docId=data_object.getString("doc_id");
                        ChatLockPojo lockPojo = getChatLockdetailfromDB(docId, chatType);
                        if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {
                            stat = lockPojo.getStatus();
                            pwd = lockPojo.getPassword();
                        }
                    }
                    if (!isMuted) {

                        if (sessionManager.getlogin()) {
                            boolean isLockChat=false;
                            if (stat!=null && stat.equals("1")) {
                                isLockChat=true;
                            }
                           // String from=json.optJSONObject("data").optString("from");
//                            if(!AppUtils.isServiceRunning(this,MessageService.class)) {
//                                MyLog.d("Taha+++", "onMessageReceived:  Starting service");
//                                AppUtils.startService(this, MessageService.class);
//                            }
                            //        loadMessage(json.optJSONObject("data"),singletchat_boolean,secretchat);
                        }

                    }

                } catch (Exception exx) {
                    exx.printStackTrace();
                }
                MyLog.d("Taha+++", "onMessageReceived:  Handler aboveee ");
                boolean finalIsSecretMsg = isSecretMsg;

                String finalChatType = chatType;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        MyLog.d("Taha+++", "onMessageReceived:  Handler ");
                        try {
                            if (finalChatType.equalsIgnoreCase("single_message")) {
                                singletchat_boolean = true;
                                String msgId = data_object.getString("msgId");
                                String convId = data_object.optString("convId");
                                changeBadgeCount(convId, msgId);
                                MyLog.d("Taha", "Firebase onMessageReceived: singleMessage");
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, data_object, data_object.getString("from"), data_object.getString("type"), true, finalIsSecretMsg, false);
                            } else if (finalChatType.equalsIgnoreCase("audio_call")) {

                                singletchat_boolean = true;
                                loadIncomingCallData(data_object);
                            } else if (finalChatType.equals("group_invite")) {
                                if (isRaad) {
                                    MesssageObjectReceiver.addToDBForApproval(data_object, MyFirebaseMessagingService.this, mCurrentUserId);

                                    NotificationUtil.getInstance().newGroupNotification(MyFirebaseMessagingService.this, data_object);
                                    EventBus.getDefault().post(new GroupInviteBraodCast());
                                }
                            } else if (finalChatType.equalsIgnoreCase("send_friend_request")) {
                                handleFriendRequestEvent(remoteMessage.getData().toString());
                            } else if (finalChatType.equalsIgnoreCase("accept_friend_request") || finalChatType.equalsIgnoreCase("delete_friend_request")) {
                                handleFriendRequestCount(remoteMessage.getData().toString());
                            } else if (finalChatType.equalsIgnoreCase("group_chat")) {
                                String msgId = data_object.getString("msgId");
                                String convId = data_object.optString("groupId");
                                changeBadgeCount(convId, msgId);
                                MyLog.d("Taha", "Firebase onMessageReceived: groupChat");
                                NotificationUtil.getInstance().CustomshowNotification(CoreController.mcontext, data_object, data_object.getString("from"), data_object.getString("type"), false, finalIsSecretMsg, false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startBackgroundServices() {
        AppUtils.startService(this, MessageService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.startForegroundService(new Intent(this, MessageService.class));
        } else {
            this.startService(new Intent(this, MessageService.class));
        }
    }

    private void handleFriendRequestEvent(String data) {
        try {
            JSONObject object = new JSONObject(data);
            JSONObject friendObject = object.getJSONObject("data");
            String id = "";
            if (friendObject.has("_id")) {
                if (friendObject.getString("RequestStatus").equals("RECEIVED")) {
                    id = friendObject.getString("_id");
                    changeBadgeCount(id, "friend");
                }
            }
            NotificationUtil.getInstance().newFriendCustomNotification(CoreController.mcontext, friendObject);
        } catch (JSONException exc) {

        }
    }

    private void handleFriendRequestCount(String data) {
        try {
            JSONObject object = new JSONObject(data);
            JSONObject friendObject = object.getJSONObject("data");
            String id = "";
            if (friendObject.has("_id")) {
                id = friendObject.getString("_id");
                ShortcutBadgeManager shortcutBadgeManager = new ShortcutBadgeManager(MyFirebaseMessagingService.this);
                shortcutBadgeManager.removeMessageCount(id, "friend");
            }
            NotificationUtil.getInstance().newFriendCustomNotification(CoreController.mcontext, friendObject);
        } catch (JSONException exc) {

        }
    }

    public void WorkerManager() {
        Constraints constraints = new Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresCharging(true)
                .setRequiresStorageNotLow(true)
                .build();
        Data.Builder data = new Data.Builder();
        data.putString("SyncMaster", "SyncMaster");


        PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(
                MyPeriodicWork.class, 15, TimeUnit.MINUTES)
                .addTag("Sync")
                .setInputData(data.build())
                .setConstraints(constraints)
                .build();

        WorkManager.getInstance().enqueue(periodicWorkRequest);

    }
    private void handleNotification(String message) {

    }


    private void loadIncomingCallData(JSONObject callObj) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        try {


            String from = callObj.getString("from");
            String to = callObj.getString("to");
            String callStatus = callObj.getString("call_status");
            String Room_id = callObj.getString("roomid");
            String recordId = callObj.getString("recordId");
            if (to.equalsIgnoreCase(uniqueCurrentID) && callStatus.equals(MessageFactory.CALL_STATUS_CALLING + "")) {

                CallItemChat callItem = incomingMsg.loadIncomingCall(callObj);
                MessageDbController db = CoreController.getDBInstance(this);
                db.updateCallLogs(callItem);

                boolean isVideoCall = false;
                if (callItem.getCallType().equals(MessageFactory.video_call + "")) {
                    isVideoCall = true;
                }
//                if (!AppUtils.isMyServiceRunning(this, MessageService.class)) {
//
//                    AppUtils.startService(this, MessageService.class);
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                        this.startForegroundService(new Intent(this, MessageService.class));
//                    } else {
//                        this.startService(new Intent(this, MessageService.class));
//                    }
//                }
//                else
                    if (!CallsActivity.isStarted && !IncomingCallActivity.isStarted) {

                    String ts = callObj.getString("timestamp");
                        MyLog.d("Initiate notification from Firebase service ");

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                        int NOTIFICATION_ID = NotificationUtils.getNotificationID(recordId);
                        MyLog.d("Initiate notification from Firebase service for Build.VERSION_CODES.S ");
                        String name = getcontactname.getSendername(callItem.getOpponentUserId(), callItem.getOpponentUserMsisdn());
                        NotificationCompat.Builder notificationBuilder = NotificationUtils.generateNotificationBuilder(this, callItem.getCallId(), callItem.getCallType(), name, callItem.getOpponentUserId(), to, callItem.getOpponentUserMsisdn(), Room_id, recordId, isVideoCall, ts);

                        NotificationManager notificationManager = (NotificationManager) mContext.
                                getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());

                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        MyLog.d("Initiate notification from Firebase service for Build.VERSION_CODES.P ");

                        Getcontactname getcontactname = new Getcontactname(this);
                        String name = getcontactname.getSendername(callItem.getOpponentUserId(), callItem.getOpponentUserMsisdn());

                        Intent serviceIntent = new Intent(getApplicationContext(), CallNotificationService.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putString(AppConstants.INITIATOR, name);
                        mBundle.putString(AppConstants.CALL_TYPE, callItem.getCallType());
                        mBundle.putString(IncomingCallActivity.EXTRA_DOC_ID, callItem.getCallId());
                        mBundle.putString(IncomingCallActivity.EXTRA_FROM_USER_ID, callItem.getOpponentUserId());
                        mBundle.putString(IncomingCallActivity.EXTRA_TO_USER_ID, to);
                        mBundle.putString(IncomingCallActivity.EXTRA_FROM_USER_MSISDN, callItem.getOpponentUserMsisdn());
                        mBundle.putString(IncomingCallActivity.EXTRA_CALL_ROOM_ID, Room_id);
                        mBundle.putString(IncomingCallActivity.EXTRA_CALL_RECORD_ID, recordId);
                        mBundle.putBoolean(IncomingCallActivity.EXTRA_CALL_TYPE, isVideoCall);
                        mBundle.putString(IncomingCallActivity.EXTRA_CALL_TIME_STAMP, ts);
                        serviceIntent.putExtras(mBundle);
                        ContextCompat.startForegroundService(getApplicationContext(), serviceIntent);
                    } else {
                        Intent intent = new Intent();
                        intent.setClass(this, IncomingCallActivity.class);

                        intent.putExtra(IncomingCallActivity.EXTRA_DOC_ID, callItem.getCallId());
                        intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_ID, callItem.getOpponentUserId());
                        intent.putExtra(IncomingCallActivity.EXTRA_TO_USER_ID, to);
                        intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_MSISDN, callItem.getOpponentUserMsisdn());
                        intent.putExtra(IncomingCallActivity.EXTRA_CALL_ROOM_ID, Room_id);
                        intent.putExtra(IncomingCallActivity.EXTRA_CALL_RECORD_ID, recordId);
                        intent.putExtra(IncomingCallActivity.EXTRA_CALL_TYPE, isVideoCall);
                        intent.putExtra(IncomingCallActivity.EXTRA_CALL_TIME_STAMP, ts);

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        if (callItem.getOpponentUserId().equalsIgnoreCase(CallsActivity.opponentUserId)) {
                            sendIncomingCallBroadcast(intent);
                        }
                        String type = "" + MessageFactory.audio_call;
                        if (isVideoCall)
                            type = "" + MessageFactory.video_call;
                        JSONObject ackObj = CallMessage.getCallStatusObject(to, callItem.getOpponentUserId(),
                                callItem.getId(), callItem.getCallId(), callItem.getRecordId(), MessageFactory.CALL_STATUS_ARRIVED, type);
                        SendMessageEvent event = new SendMessageEvent();
                        event.setEventName(SocketManager.EVENT_CALL_STATUS);
                        event.setMessageObject(ackObj);
                        EventBus.getDefault().post(event);

                        sendCallAckToServer(callObj, callItem);

                    }
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendCallAckToServer(JSONObject callObj, CallItemChat callItem) {
        try {
            String callId;
            if (callObj.has("docId")) {
                callId = callObj.getString("docId");
            } else {
                callId = callObj.getString("doc_id");
            }
            SendMessageEvent messageEvent = new SendMessageEvent();
            messageEvent.setEventName(SocketManager.EVENT_CALL_ACK);
            CallAck ack = (CallAck) MessageFactory.getMessage(MessageFactory.call_ack, (this));
            messageEvent.setMessageObject((JSONObject) ack.getMessageObject(callItem.getOpponentUserId(),
                    callId, MessageFactory.DELIVERY_STATUS_DELIVERED, callItem.getId()));
            EventBus.getDefault().post(messageEvent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendIncomingCallBroadcast(final Intent dataIntent) {

        Runnable incomCallBroadcastRunnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setAction(getPackageName() + ".incoming_call");
                intent.putExtras(dataIntent.getExtras());
                sendBroadcast(intent);
            }
        };
        incomCallBroadcastHandler.postDelayed(incomCallBroadcastRunnable, 200);
    }


    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }


    public static String getDate(long milliSeconds, String aDateFormat) {
        DateFormat formatter = new SimpleDateFormat(aDateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    public void navigateToChatFromService(Intent intent, String receiverDocumentID, String username) {

        intent.putExtra("receiverUid", "");
        intent.putExtra("receiverName", "");
        intent.putExtra("documentId", receiverDocumentID);
        intent.putExtra("Image", "");
        intent.putExtra("type", 0);
        intent.putExtra("backfrom", true);
        intent.putExtra("Username", username);
    }

    private void showMissedCallNotification(String toUserId, String toUserMsisdn) {
        missedCallCount++;
        String content;
        if (missedCallCount > 1) {
            content = missedCallCount + " "+getResources().getString(R.string.missed_calls);
        } else {
            content =String.format(getResources().getString(R.string.missed_calls_count),missedCallCount);
            lastMissedCallId = toUserId;
        }

        if (lastMissedCallId.equalsIgnoreCase(toUserId)) {
            String name = getcontactname.getSendername(toUserId, toUserMsisdn);
            content = content + " "+getResources().getString(R.string.from)+" " + name;
        }

        Intent intent = new Intent(this, NewHomeScreenActivty.class);
        intent.putExtra(NewHomeScreenActivty.FROM_MISSED_CALL_NOTIFICATION, true);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_MUTABLE);
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder =
                    new NotificationCompat.Builder(getApplicationContext(), "1")
                            .setSmallIcon(R.mipmap.ic_launcher)

                            // Set Ticker Message
                            .setTicker("")
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(content)
                            .setAutoCancel(true)

                            // Set PendingIntent into Notification
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setContentIntent(pIntent)
                            .setContentIntent(pIntent)
                            // Set RemoteViews into Notification
                            .setColor(0xF01a9e5);
        } else {
            builder = new NotificationCompat.Builder(this)
                    // Set Icon
                    .setSmallIcon(R.mipmap.ic_launcher)

                    // Set Ticker Message
                    .setTicker("")
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(content)
                    .setAutoCancel(true)
                    // Set PendingIntent into Notification
                    .setContentIntent(pIntent)
                    // Set RemoteViews into Notification

                    .setColor(ContextCompat.getColor(MyFirebaseMessagingService.this, R.color.transparent));
        }

        NotificationCompat.BigTextStyle style =
                new NotificationCompat.BigTextStyle(builder);

        style.bigText(content)
                .setBigContentTitle(getString(R.string.app_name));

        builder.setStyle(style);


        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("1",
                    getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_LOW);

            notificationmanager.createNotificationChannel(channel);
        }
        notificationmanager.notify(1, builder.build());
    }



    private void setVibrateForOreo(NotificationChannel channel) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!session.getvibratePrefsName().contains("Off")) {
                channel.enableVibration(true);

                if (session.getvibratePrefsName().contains("Long")) {
                    Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(3000);
                } else if (session.getvibratePrefsName().contains("Short")) {
                    Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(500);

                } else {
                    Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(1500);
                }
            }
        }
    }

    private void notificationnotify(NotificationManager notificationmanager, int i, NotificationCompat.Builder builder) {

        if (!ChatPageActivity.Companion.isChatPage()) {
            if (!session.getvibratePrefsName().contains("Off")) {
                if (session.getvibratePrefsName().contains("Long"))
                    builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
                else if (session.getvibratePrefsName().contains("Short"))
                    builder.setVibrate(new long[]{1000, 1000});
                else
                    builder.setVibrate(new long[]{1000, 1000, 1000});

            }
            notificationmanager.notify(i, builder.build());
        }


    }

    public ChatLockPojo getChatLockdetailfromDB(String documentID, String chatType) {
        MessageDbController dbController = CoreController.getDBInstance(this);
        String convId = userInfoSession.getChatConvId(documentID);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo pojo = dbController.getChatLockData(receiverId, chatType);
        return pojo;
    }


    private synchronized void loadMessage(JSONObject objects,boolean singletchat_boolean,boolean secretchat) {
        MyLog.d("Taha", "Firebase loadMessage: Trying to load the new message");
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            MessageItemChat item = null;
            int normal_Offline = 0;
            String type = objects.optString("type");
            if (type!=null && !type.isEmpty() && Integer.parseInt(objects.optString("type")) == 23) {
                return;
            }
            if (objects.has("is_deleted_everyone")) {
                normal_Offline = objects.optInt("is_deleted_everyone");
            }
            if (type!=null && type.equalsIgnoreCase("" + MessageFactory.timer_change)) {
            } else {
                if (singletchat_boolean)
                    item = incomingMsg.loadSingleMessage(objects);
                else {
                    item = incomingMsg.loadGroupMessage(objects);
                    String groupId = "";
                    if (objects.has("groupId")) {
                        groupId = objects.getString("groupId");
                        item.setReceiverID(groupId);
                    }
                }
                String from = objects.optString("from");
                String to = objects.optString("to");
                String secretType = objects.optString("secret_type");
                String msgId = objects.optString("msgId");

                if (messageIds.contains(msgId)) {
                    return;
                }
                messageIds.add(msgId);
                boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                if (is_gossip) {
                    if (Build.VERSION.SDK_INT >= 27) {

                        NotificationUtil.getInstance().CustomshowNotificationWithReply(this, objects, from, objects.optString("type"), singletchat_boolean, secretchat, false);
                    }else {
                        MyLog.d("Taha", "Firebase loadMessage: singleMessage isGossip");
                        NotificationUtil.getInstance().CustomshowNotification(this,objects,from,objects.optString("type"),singletchat_boolean,secretchat,false);
                    }
                }else {
                    MyLog.d("Taha", "Firebase loadMessage: singleMessage");
                    NotificationUtil.getInstance().CustomshowNotification(this,objects,from,objects.optString("type"),singletchat_boolean,secretchat,false);
                }

                //                String id = objects.getString("id");
                String convId = objects.optString("convId");
                String stat = "", pwd = "";
                String doc_id;
                if (objects.has("docId")) {
                    doc_id = objects.optString("docId");
                } else {
                    doc_id = objects.optString("doc_id");
                }
                String uniqueID = to + "-" + from;

                item.setIsSelf(false);
                item.setDeliveryStatus(MessageFactory.DELIVERY_STATUS_DELIVERED);

                String chatType = MessageFactory.CHAT_TYPE_SINGLE;
                if (objects.has("chat_type"))
                    chatType = objects.getString("chat_type");//MessageFactory.CHAT_TYPE_SINGLE;
                if (secretType.equalsIgnoreCase("yes")) {
                    chatType = MessageFactory.CHAT_TYPE_SECRET;
                    uniqueID = uniqueID + "-" + MessageFactory.CHAT_TYPE_SECRET;
                    String timer = objects.getString("incognito_timer");
                    String timerCreatedBy = objects.getString("incognito_user");
                    item.setSecretTimer(timer);
                    item.setSecretTimeCreatedBy(timerCreatedBy);

                    //new dbsqlite
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                    contactDB_sqlite.updateSecretMessageTimer(to, timer, timerCreatedBy, item.getMessageId());
                }

                if (session.getarchivecount() != 0) {
                    if (session.getarchive(uniqueCurrentID + "-" + from))
                        session.removearchive(uniqueCurrentID + "-" + from);
                }

                MessageDbController db = CoreController.getDBInstance(this);

                MyLog.d("Taha", "loadMessage: updateChatMessage");
                db.updateChatMessage(item, chatType);

                AutoDownLoadUtils.getInstance().checkAndAutoDownload(this,item);
                if(chatType.equals(MessageFactory.CHAT_TYPE_GROUP)) {
                    changeBadgeCount(convId, item.getMessageId());
                }
                if (!userInfoSession.hasChatConvId(uniqueID)) {
                    userInfoSession.updateChatConvId(uniqueID, from, convId);
                }
                MyLog.d("Taha", "loadMessage: isChatPage = " + ChatPageActivity.Companion.isChatPage());
                if (ChatPageActivity.Companion.isChatPage()) {
                    MyLog.d("Taha", "loadMessage: attempting to hit the broadcast");
                    Intent intent = new Intent();
                    intent.setAction(SocketManager.EVENT_MESSAGE);
                    sendBroadcast(intent);
                }
            }

            //-------------Delete Chat-----------------

            if (normal_Offline == 1) {
                String new_docId, new_msgId, new_type, new_recId, new_convId;
                try {
                    String fromId = objects.getString("from");

                    if (!fromId.equalsIgnoreCase(uniqueCurrentID)) {
                        String chat_id = (String) objects.get("docId");
                        String[] ids = chat_id.split("-");

                        new_type = objects.getString("chat_type");
                        MessageDbController db = CoreController.getDBInstance(this);

                        if (new_type.equalsIgnoreCase("single")) {
                            new_docId = ids[1] + "-" + ids[0];
                            new_msgId = new_docId + "-" + ids[2];
                            db.deleteSingleMessage(new_docId, new_msgId, "single", "other");
                            db.deleteChatListPage(new_docId, new_msgId, "single", "other");
                        } else {
                            new_docId = ids[1] + "-g-" + ids[0];
                            new_msgId = uniqueCurrentID + "-g-" + ids[1] + "-g-" + ids[3];
                            String groupAndMsgId = ids[1] + "-g-" + ids[3];

                            db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                            db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                        }


                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendAckToServer(String to, String doc_id, String id, boolean isSecretChat,String mConvId) {
        //Check if it is blocked message or not
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_MESSAGE_ACK);
        MessageAck ack = (MessageAck) MessageFactory.getMessage(MessageFactory.message_ack, (this));
        messageEvent.setMessageObject((JSONObject) ack.getMessageObject(to, doc_id,
                MessageFactory.DELIVERY_STATUS_DELIVERED, id, isSecretChat,mConvId));
        //initSocketManagerCallback();
        mSocketManager.send(messageEvent.getMessageObject(), SocketManager.EVENT_MESSAGE_ACK);
    }

    private void changeBadgeCount(String convId,String msgId) {
        ShortcutBadgeManager shortcutBadgeMgnr = new ShortcutBadgeManager(this);
        if (shortcutBadgeMgnr.getSingleBadgeCount(msgId) == 0) {
            shortcutBadgeMgnr.putMessageCount(convId, msgId);
            int totalCount = shortcutBadgeMgnr.getTotalCount();
            try {
                ShortcutBadger.applyCount(this, totalCount);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }


    private void initSocketManagerCallback() {
        if (mSocketManager == null) {
//Check call back is the same screen

            if (SocketManager.callBack == null) {
                SocketManager.callBack = callBack;
            } else if (SocketManager.callBack instanceof ContactsSync) {
                SocketManager.callBack = callBack;
            } else {
                SocketManager.callBack = callBack;
            }
            mSocketManager = SocketManager.getInstance();
            mSocketManager.init(this, callBack);
        }
    }

    SocketManager.SocketCallBack callBack = new SocketManager.SocketCallBack() {
        @Override
        public void onSuccessListener(String eventName, Object... response) {
            ReceviceMessageEvent me = new ReceviceMessageEvent();
            me.setEventName(eventName);
            if (AppUtils.isEncryptionEnabled(mContext)) {
                try {
                    if (response != null && !SocketManager.excludedList.contains(eventName)) {
                        response[0] = SocketManager.getDecryptedMessage(mContext, response[0].toString(), eventName);
                        String decrypted = response[0].toString();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            me.setObjectsArray(response);
            if (eventName != null && !eventName.equals("sc_change_online_status") && !eventName.equals("sc_get_offline_status"))

                switch (eventName) {

                    case SocketManager.EVENT_USER_AUTHENTICATED: {
                        String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();
                        if (AppUtils.isEmpty(securityToken)) {
                            fetchSecretKeys();
                        }
                    }
                    break;
                    case Socket.EVENT_CONNECT: {
                        createUser();
                    }
                    break;

                    case Socket.EVENT_DISCONNECT:
                        //mSocketManager.disconnect();
                        break;
                    case SocketManager.EVENT_GROUP: {
                        handleGroupResponse(response);
                    }
                    case SocketManager.EVENT_MESSAGE_ACK: {
                        loadMessageAckMessage(response[0].toString());
                    }
                    break;

                    case SocketManager.EVENT_MESSAGE: {
                        if (AppUtils.isEncryptionEnabled(mContext)) {
                            //Decrypt my message
                            try {
                                JSONObject object = null;
                                if (response != null) {
                                    object = new JSONObject(response[0].toString());
                                    MyLog.d("Taha", "onSuccessListener: loadMessage triggered");
                                    loadMessage(object,singletchat_boolean, secretchat);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            JSONObject object = (JSONObject) response[0];
                            MyLog.d("Taha", "onSuccessListener: loadMessage triggered from the else block");
                            loadMessage(object,singletchat_boolean, secretchat);
                        }
                    }

                    break;

                    case SocketManager.EVENT_GET_SECRET_KEYS: {
                        try {
                            String data = response[0].toString();
                            JSONObject object = new JSONObject(data);
                            String publicKey = "", privateKey = "";
                            if (object.has("public_key")) {
                                publicKey = object.getString("public_key");
                                privateKey = object.getString("private_key");
                                SessionManager.getInstance(mContext).setPublicEncryptionKey(publicKey);
                                SessionManager.getInstance(mContext).setPrivateEncryptionKey(privateKey);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                    //   dispatcher.addwork(me);
                }
        }

    };

    // Handle Group Event response
    private void handleGroupResponse(Object[] response) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();

        try {
            JSONObject objects = new JSONObject(response[0].toString());
            String groupAction = objects.getString("groupType");

            String error = objects.getString("err");

            if (error.equalsIgnoreCase("0")) {


                if (objects.has("from")) {
                    String from = objects.getString("from");
                    if (!from.equalsIgnoreCase(uniqueCurrentID)) {
                        //getUserDetails(from);
                    }
                }

                if (groupAction.equalsIgnoreCase(SocketManager.ACTION_EVENT_GROUP_MESSAGE)) {
                    storeGroupMsgInDataBase(response);
                }
            } else if (error.equalsIgnoreCase("1")) {

                String message = objects.getString("msg");


                String messageid = objects.getString("doc_id");

                MessageDbController db = CoreController.getDBInstance(mContext);

                JSONObject ob = db.FirstTimeUploadObjectGet(messageid);

            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    // Store Group message to local DB
    private void storeGroupMsgInDataBase(Object[] response) {

        try {
            JSONObject objects = new JSONObject(response[0].toString());
            if (Integer.parseInt(objects.optString("type")) == 23) {
                return;
            }
            String to;
            if (objects.has("to")) {
                to = objects.getString("to");
            } else {
                to = objects.getString("groupId");
            }
            if (objects.has("id")) {
                String msgId = objects.getString("id");
                if (messageIds.contains(msgId)) {
                    return;
                }
                messageIds.add(msgId);
            }
            String docId = uniqueCurrentID.concat("-").concat(to).concat("-g");

            //   objectReceiver.storeGroupMsgInDataBase(response);
            if (objects.has("payload")) {

                String stat = "", pwd = "";
                ChatLockPojo lockPojo = getChatLockdetailfromDB(docId, MessageFactory.CHAT_TYPE_GROUP);
                if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {
                    stat = lockPojo.getStatus();
                    pwd = lockPojo.getPassword();
                }

                if (!stat.equalsIgnoreCase("1")) {
                    if (!ChatPageActivity.Companion.isChatPage()) {

                        boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                        if (is_gossip) {
                            if (Build.VERSION.SDK_INT >= 27) {

                                NotificationUtil.getInstance().CustomshowNotificationWithReply(this, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                            }else {
                                MyLog.d("Taha", "Firebase storeGroupMsgInDataBase: isGossip");
                                NotificationUtil.getInstance().CustomshowNotification(this,objects, objects.getString("from"), objects.getString("type"), false, false,false);
                            }
                        }else {
                            MyLog.d("Taha", "Firebase storeGroupMsgInDataBase: ");
                            NotificationUtil.getInstance().CustomshowNotification(this,objects, objects.getString("from"), objects.getString("type"), false, false,false);
                        }
                    } else {
                        if (!session.gettoid().equalsIgnoreCase(docId)) {
                            boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                            if (is_gossip) {
                                if (Build.VERSION.SDK_INT >= 27) {

                                    NotificationUtil.getInstance().CustomshowNotificationWithReply(this, objects, objects.getString("from"), objects.getString("type"), false, false, false);
                                }else {
                                    MyLog.d("Taha", "storeGroupMsgInDataBase: isChatPage isGossip");
                                    NotificationUtil.getInstance().CustomshowNotification(this,objects, objects.getString("from"), objects.getString("type"), false, false,false);
                                }
                            }else {
                                MyLog.d("Taha", "storeGroupMsgInDataBase: isChatPage ");
                                NotificationUtil.getInstance().CustomshowNotification(this,objects, objects.getString("from"), objects.getString("type"), false, false,false);
                            }

                        }

                    }
                }

                else if (stat.equalsIgnoreCase("1")) {
                    boolean is_gossip = getResources().getBoolean(R.bool.is_gossip);
                    if (is_gossip) {
                        if (Build.VERSION.SDK_INT >= 27) {


                            NotificationUtil.getInstance().CustomshowNotificationWithReply(this, objects, objects.getString("from"), objects.getString("type"), false, false, true);
                        }else {
                            MyLog.d("Taha", "storeGroupMsgInDataBase: isGossip");
                            NotificationUtil.getInstance().CustomshowNotification(this,objects, objects.getString("from"), objects.getString("type"), false, false,true);

                        }
                    }else {
                        MyLog.d("Taha", "storeGroupMsgInDataBase:");
                        NotificationUtil.getInstance().CustomshowNotification(this,objects, objects.getString("from"), objects.getString("type"), false, false,true);
                    }

                }
                //-------------Delete Chat----------------
                int group_type, is_deleted;
                String new_msgId, groupId;
                if (objects.has("groupType")) {
                    group_type = objects.getInt("groupType");
                    if (group_type == 9) {
                        if (objects.has("is_deleted_everyone")) {
                            is_deleted = objects.getInt("is_deleted_everyone");
                            if (is_deleted == 1) {
                                try {
                                    MessageDbController db = CoreController.getDBInstance(this);
                                    String fromId = objects.getString("from");

                                    if (!fromId.equalsIgnoreCase(uniqueCurrentID)) {
                                        String chat_id = (String) objects.get("toDocOId");
                                        String[] ids = chat_id.split("-");

                                        groupId = objects.getString("groupId");
                                        new_msgId = uniqueCurrentID + "-g-" + ids[1] + "-g-" + ids[3];
                                        String groupAndMsgId = ids[1] + "-g-" + ids[3];

                                        if (ChatPageActivity.Companion.getChat_Activity() == null) {
                                            db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                                            db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                                        } else if (!ChatPageActivity.Companion.getActivity_GroupId().equalsIgnoreCase(groupId)) {
                                            db.deleteSingleMessage(groupAndMsgId, new_msgId, "group", "other");
                                            db.deleteChatListPage(groupAndMsgId, new_msgId, "group", "other");
                                        }

                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    }
                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createUser() {
        try {
            JSONObject object = new JSONObject();
            object.put("_id", mCurrentUserId);
            object.put("mode", "phone");
            object.put("chat_type", "single");
            String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();
            object.put("token", securityToken);
            object.put("device", Constants.DEVICE);

            mSocketManager.send(object, SocketManager.EVENT_CREATE_USER);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fetchSecretKeys() {
        try {

            JSONObject fetchKeysObj = new JSONObject();
            fetchKeysObj.put("userId", SessionManager.getInstance(this).getCurrentUserID());
            mSocketManager.send(fetchKeysObj, SocketManager.EVENT_GET_SECRET_KEYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMessageAckMessage(String data) {

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        try {
            JSONObject object = new JSONObject(data);
            String from = object.getString("from");
            if (from.equalsIgnoreCase(uniqueCurrentID)) {
                String to = object.getString("to");
                String chatId = object.getString("doc_id");
                String status = object.getString("status");
                String timeStamp = object.getString("currenttime");
                String secretType = object.getString("secret_type");

                String docId = from + "-" + to;
                try {
                    String[] splitIds = chatId.split("-");
                    String id = splitIds[2];
                    String msgId = docId + "-" + id;

                    if (secretType.equalsIgnoreCase("yes")) {
                        docId = docId + "-secret";
                    }

                    MessageDbController db = CoreController.getDBInstance(this);
                    db.updateChatMessage(docId, msgId, status, timeStamp, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
