package com.chat.android.fcm;

/**
 * Created by user145 on 5/28/2018.
 */

public interface AppConfig {
    String TAG = "GCM TAG";
    String REG_ID = "regId";
    String APP_VERSION = "app_version";
    String PARSE_APPLICATION_ID = "689148702481";
}
