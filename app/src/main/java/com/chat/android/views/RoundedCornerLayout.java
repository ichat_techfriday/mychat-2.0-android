package com.chat.android.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.RelativeLayout;

import com.chat.android.R;


public class RoundedCornerLayout extends RelativeLayout {
    private float cornerRadius;

    private Path path;

    public RoundedCornerLayout(Context context) {
        super(context);
        init(context);
    }

    public RoundedCornerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RoundedCornerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        final DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimensionPixelSize(R.dimen.chat_cardview_radius), metrics);

        setWillNotDraw(false);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        final RectF r = new RectF(0, 0, w, h);
        path = new Path();
        path.addRoundRect(r, cornerRadius, cornerRadius, Path.Direction.CW);
        path.close();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.clipPath(path);
        super.draw(canvas);
        canvas.restore();
    }
}
