package com.chat.android.app.activity;

import static com.chat.android.utils.AppConstants.RequestCodes.REQUEST_LOCATION;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.chat.android.R;
import com.chat.android.app.model.meetup.UpdateMeetupResponseData;
import com.chat.android.app.utils.MyLog;
import com.chat.android.backend.ApiCalls;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.scimbohelperclass.ScimboDialogUtils;
import com.chat.android.core.scimbohelperclass.ScimboPermissionValidator;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.interfaces.PermissionGrantListener;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.GpsTracker;
import com.chat.android.utils.TextUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mukesh.countrypicker.Country;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import android.Manifest;

public class MeetupBaseFragment extends Fragment implements INetworkResponseListener, LocationListener {

    protected String mFullName = "", mBio = "", mDob = "", mLongitude = "", mLatitude = "", userActivationStatus = "activate";
    protected int maxDistance, limit = 20;
    protected GpsTracker gpsTracker;
    protected double lat, lng;
    protected Context mContext;
    protected Activity mActivity;
//    LocationManager locationManager;
//    protected Location location;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private SettingsClient mSettingsClient;
    private LocationCallback mLocationCallback;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Location mLocation;
    private ArrayList<ScimboPermissionValidator.Constants> myPermissionConstantsArrayList;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MyLog.d("onAttach: ");
        mContext = context;
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //getting mBundle info
        Bundle mBundle = this.getArguments();
        if (mBundle != null) {
            mFullName = mBundle.getString("fullName");
            mBio = mBundle.getString("bio");
            mDob = mBundle.getString("dob");
            maxDistance = mBundle.getInt(AppConstants.IntentKeys.MEETUP_DISTANCE_IN_METERS.getValue());
            lat = mBundle.getDouble("latitude");
            lng = mBundle.getDouble("longitude");
            mLatitude = String.valueOf(lat);
            mLongitude = String.valueOf(lng);
        }
        View appBarContainer = view.findViewById(R.id.app_bar_meetup);
        if (appBarContainer != null) {
            //back btn click listener
            appBarContainer.findViewById(R.id.back_btn).setOnClickListener(v -> {
               mActivity.onBackPressed();
            });
            //btn share click listener
            appBarContainer.findViewById(R.id.share).setOnClickListener(v -> {
                Intent intent = new Intent(mContext, NewHomeScreenActivty.class);
                intent.putExtra(NewHomeScreenActivty.FROM_FRIEND_NOTIFICATION, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                startActivity(intent);
                mActivity.finish();
            });
        }
        TextView requestsBadge = view.findViewById(R.id.tv_badge);
        int friendRequestsCount = getFriendRequestCount();
        requestsBadge.setText(String.valueOf(friendRequestsCount));
    }

    private int getFriendRequestCount() {
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
        List<FriendModel> friendModels = contactDB_sqlite.getSavedFriendModels();
        int friendRequestsCount = 0;
        if (friendModels == null)
            return friendRequestsCount;

        for (FriendModel friendModel : friendModels) {
            String status = friendModel.getRequestStatus();
            if (TextUtils.isEmpty(status))
                continue;
            if (status.equals(AppConstants.FriendStatus.DELETED.getValue()))
                continue;
            if (status.equals(AppConstants.FriendStatus.REQUEST_RECEIVED.getValue()))
                friendRequestsCount++;
        }
        return friendRequestsCount;
    }

    protected void requestAccess() {
        if ( ContextCompat.checkSelfPermission( mContext, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission( mContext, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION }, AppConstants.RequestCodes.REQUEST_LOCATION.getCode());
        } else {
            fetchLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION.getCode()) {
           fetchLocation();
        }
    }

    protected void fetchLocation() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);
        mSettingsClient = LocationServices.getSettingsClient(mContext);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                MyLog.d("ENTERED LocationCallback::onLocationResult");
                super.onLocationResult(locationResult);
                mLocation = locationResult.getLastLocation();
                if (mLocation == null)
                    return;
                setLatLng(mLocation);
            }
        };

        // set up the request. note that you can use setNumUpdates(1) and
        // setInterval(0) to get one request.
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
        requestLocationPermission();
    }

    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new  DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getLocation() {
//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, this);
////        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, this);
//
//        Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//        setLatLng(locationGPS);
    }

    private void setLatLng(Location locationGPS) {
        if (locationGPS != null) {
//            this.location = locationGPS;
            lat = locationGPS.getLatitude();
            lng = locationGPS.getLongitude();
            mLatitude = String.valueOf(lat);
            mLongitude = String.valueOf(lng);
            MyLog.d("Latitude " + lat + " longitude " + lng);
        }
    }

    protected void deactivateMeetup() {
        MyLog.d("Meetup", "deactivating meetup module");
        userActivationStatus = "deactivate";
        updateMeetup();
    }

    protected void updateMeetup() {
        //making api call
        ApiCalls.getInstance(getContext()).updateMeetup(getContext(),
                SessionManager.getInstance(getContext()).getCurrentUserID(),
                mFullName,
                lat,
                lng,
                userActivationStatus,
                mBio,
                mDob,
                this
        );
    }

    @Override
    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        MyLog.d("onNetworkResponse " + status + " " + message + " " +  responseForRequest);
        if (status) {
            UpdateMeetupResponseData updateMeetupResponseData = (UpdateMeetupResponseData) body;
        } else {
//            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    protected void OpenFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getActivity().getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_layout, fragment, backStateName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        setLatLng(location);
    }



    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener((Activity) mContext, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mFusedLocationProviderClient.requestLocationUpdates(
                                mLocationRequest,
                                mLocationCallback,
                                Looper.getMainLooper()
                        );
                        if (mLocation != null) {
                            setLatLng(mLocation);
                        }
                    }
                })
                .addOnFailureListener((Activity) mContext, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    int requestCheckSettings = 100;  //?
                                    rae.startResolutionForResult((Activity) mContext, requestCheckSettings);
                                } catch (IntentSender.SendIntentException sie) {
                                    sie.printStackTrace();
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings.";
                                MyLog.d(errorMessage);
                        }
                    }
                });
    }

    protected void requestLocationPermission() {
        CoreActivity baseActivity = (CoreActivity) mContext;
        baseActivity.requestForPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, new PermissionGrantListener() {
            @Override
            public void onPermissionGranted(boolean granted) {
                if (granted) {
                    startLocationUpdates();
                }
            }
        });
    }


    @Override
    public void onStop() {
        super.onStop();
    }

}