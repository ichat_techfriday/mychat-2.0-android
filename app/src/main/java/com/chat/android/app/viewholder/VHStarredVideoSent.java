package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.widget.AvnNextLTProRegTextView;


/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredVideoSent extends RecyclerView.ViewHolder {
    public AvnNextLTProRegTextView captiontext;

    public TextView senderName, time, fromname, toname, datelbl, duration,duration_above,ts_abovecaption;

    public ImageView singleTick, doubleTickGreen, doubleTickBlue, clock, starredindicator_below, userprofile;
    public ImageView single_tick_green_above,double_tick_green_above,double_tick_blue_above,starredindicator_above,clock_above;
    public RelativeLayout caption,videoabove_layout,video_belowlayout;
    public ImageView thumbnail;

    public VHStarredVideoSent(View view) {
        super(view);
        caption = view.findViewById(R.id.caption);
        captiontext = view.findViewById(R.id.captiontext);
        senderName = view.findViewById(R.id.lblMsgFrom);
        time = view.findViewById(R.id.ts);
        singleTick = view.findViewById(R.id.single_tick_green);
        doubleTickGreen = view.findViewById(R.id.double_tick_green);
        doubleTickBlue = view.findViewById(R.id.double_tick_blue);
        clock = view.findViewById(R.id.clock);
        starredindicator_below = view.findViewById(R.id.starredindicator_below);
        thumbnail = view.findViewById(R.id.vidshow);
        userprofile = view.findViewById(R.id.userprofile);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        duration = view.findViewById(R.id.duration);

        duration_above= view.findViewById(R.id.duration_above);
        single_tick_green_above = view.findViewById(R.id.single_tick_green_above);
        double_tick_green_above = view.findViewById(R.id.double_tick_green_above);
        double_tick_blue_above = view.findViewById(R.id.double_tick_blue_above);
        clock_above = view.findViewById(R.id.clock_above);
        starredindicator_above = view.findViewById(R.id.starredindicator_above);
        ts_abovecaption = view.findViewById(R.id.ts_abovecaption);
        videoabove_layout= view.findViewById(R.id.videoabove_layout);
        video_belowlayout= view.findViewById(R.id.video_belowlayout);

    }
}
