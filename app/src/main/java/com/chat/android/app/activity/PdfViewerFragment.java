package com.chat.android.app.activity;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;

import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.chat.android.BuildConfig;
import com.chat.android.R;
import com.chat.android.core.model.FriendModel;
import com.chat.android.utils.StorageUtility;
import com.google.gson.Gson;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class PdfViewerFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static PdfViewerFragment fragment;
    private Button finishBtn;
    private ImageView ic_share;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ImageView iv;

    public PdfViewerFragment() {
        // Required empty public constructor
    }

    public static PdfViewerFragment newInstance(String param1) {
        fragment = new PdfViewerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString("file");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pdf_viewer, container, false);
        iv = (ImageView) view.findViewById(R.id.imageView);
        finishBtn = view.findViewById(R.id.finishBtn);
        ic_share = view.findViewById(R.id.ic_share);
        //File file = new File(mParam1);
        String path = Environment.getExternalStorageDirectory() + "/" + "MyFirstApp/";
        String file = path + "sample.pdf";
        openPdf(new File(file));
        finishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String model = StorageUtility.getDataFromPreferences(getContext(), "send_pay", "");
                Gson gson = new Gson();
                FriendModel friendModel = gson.fromJson(model, FriendModel.class);
                Intent intent = new Intent(getContext(), ChatPageActivity.class);
                intent.putExtra("receiverUid", friendModel.getNumberInDevice());
                intent.putExtra("receiverName", friendModel.getFirstName());
                intent.putExtra("documentId", friendModel.get_id());
                intent.putExtra("Username", friendModel.getFirstName());
                intent.putExtra("Image", friendModel.getAvatarImageUrl());
                intent.putExtra("msisdn", friendModel.getMsisdn());
                startActivity(intent);
                getActivity().finish();
            }
        });


        return view;
    }

    void openPdf(File file) {

        ParcelFileDescriptor fd = openFile(file);
        int pageNum = 0;
        PdfiumCore pdfiumCore = new PdfiumCore(getContext());
        try {
            PdfDocument pdfDocument = pdfiumCore.newDocument(fd);

            pdfiumCore.openPage(pdfDocument, pageNum);

            int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNum);
            int height = pdfiumCore.getPageHeightPoint(pdfDocument, pageNum);

            // ARGB_8888 - best quality, high memory usage, higher possibility of OutOfMemoryError
            // RGB_565 - little worse quality, twice less memory usage
            Bitmap bitmap = Bitmap.createBitmap(width, height,
                    Bitmap.Config.RGB_565);
            pdfiumCore.renderPageBitmap(pdfDocument, bitmap, pageNum, 0, 0,
                    width, height);
            //if you need to render annotations and form fields, you can use
            //the same method above adding 'true' as last param
            ic_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //sharePalette(bitmap);
                    shareViaIntent(getContext(), bitmap);
                    //shareOnOtherSocialMedia(bitmap);
                }
            });
            iv.setImageBitmap(bitmap);

            pdfiumCore.closeDocument(pdfDocument); // important!
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void sharePalette(Bitmap bitmap) {

        String bitmapPath = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), bitmap, "Cashplus Receipt", "share receipt");
        Uri bitmapUri = Uri.parse(bitmapPath);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/png");
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        startActivity(Intent.createChooser(intent, "Share"));
    }
    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    public void shareViaIntent(Context context, Bitmap bitmap) {
        String path = null;
        OutputStream imageOutStream;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DISPLAY_NAME,
                        System.currentTimeMillis());
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                values.put(MediaStore.Images.Media.RELATIVE_PATH,
                        Environment.DIRECTORY_PICTURES);

                Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                values);

                imageOutStream = context.getContentResolver().openOutputStream(uri);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, imageOutStream);
                imageOutStream.close();

                path = getRealPathFromURI(uri);

            } else {

                String bitmapPath = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Cashplus Receipt", "share receipt");
                Uri bitmapUri = Uri.parse(bitmapPath);
                path = getRealPathFromURI(bitmapUri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Intent shareIntent = new Intent(Intent.ACTION_SEND);
//        shareIntent.setType("text/plain");


        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        //shareIntent.setType("image/*");
        shareIntent.setType("text/plain");
        //shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //shareIntent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        shareIntent.putExtra(Intent.EXTRA_TEXT,path);
        Intent chooserIntent = Intent.createChooser(shareIntent,"Share with");
        chooserIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(chooserIntent);
        //shareIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        //shareIntent.putExtra(Intent.EXTRA_STREAM, bitmapUri);

//        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
//        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, path);
//        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        startActivity(Intent.createChooser(shareIntent, "Share to"));

//        final ArrayList<Intent> shareIntents;
//        try {
//            shareIntents = getExternalShareIntents(context, shareIntent);
//
//            Intent chooserIntent = Intent.createChooser(shareIntent, "Share");
//            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, shareIntents.toArray(new Parcelable[]{}));
//            chooserIntent.putExtra(Intent.EXTRA_TEXT, path);
//            chooserIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            context.startActivity(chooserIntent);
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
    }

    private ArrayList<Intent> getExternalShareIntents(Context context, Intent shareIntent) throws Throwable {
        final List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(shareIntent, 0);

        final ArrayList<Intent> shareIntents = new ArrayList<>();
        for (ResolveInfo resolveInfo : resolveInfos) {
            final String packageName = resolveInfo.activityInfo.packageName;

            if (packageName.equals(BuildConfig.APPLICATION_ID)) {
                continue;
            }

            Intent intent = new Intent(shareIntent);
            intent.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));
            shareIntents.add(intent);
        }

        return shareIntents;
    }
    void shareOnOtherSocialMedia(Bitmap bitmap) {
        String bitmapPath = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), bitmap, "Cashplus Receipt", "share receipt");
        Uri bitmapUri = Uri.parse(bitmapPath);
        String path = getRealPathFromURI(bitmapUri);

        List<Intent> shareIntentsLists = new ArrayList<Intent>();
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        List<ResolveInfo> resInfos = getContext().getPackageManager().queryIntentActivities(shareIntent, 0);
        if (!resInfos.isEmpty()) {
            for (ResolveInfo resInfo : resInfos) {
                String packageName = resInfo.activityInfo.packageName;
                if (!packageName.toLowerCase().contains("cartoon")) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                    intent.setAction(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(path, getActivity().getIntent().getStringExtra(Intent.EXTRA_TEXT));
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setPackage(packageName);
                    shareIntentsLists.add(intent);
                }
            }
            if (!shareIntentsLists.isEmpty()) {
                Intent chooserIntent = Intent.createChooser(shareIntentsLists.remove(0), "Choose app to share");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, shareIntentsLists.toArray(new Parcelable[]{}));
                startActivity(chooserIntent);
            } else
                Log.e("Error", "No Apps can perform your task");

        }
    }

    public ParcelFileDescriptor openFile(File f) {
        ParcelFileDescriptor pfd;
        try {
            pfd = ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_ONLY);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return pfd;
    }
   
}