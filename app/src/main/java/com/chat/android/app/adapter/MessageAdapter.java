package com.chat.android.app.adapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Telephony;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.budiyev.android.circularprogressbar.CircularProgressBar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chat.android.R;
import com.chat.android.app.activity.ChatPageActivity;
import com.chat.android.app.activity.ForwardContact;
import com.chat.android.app.activity.FullScreenVideoActivity;
import com.chat.android.app.activity.ImageZoom;
import com.chat.android.app.activity.ItemClickListener;
import com.chat.android.app.activity.Savecontact;
import com.chat.android.app.activity.ScimboContactsService;
import com.chat.android.app.activity.SecretChatViewActivity;
import com.chat.android.app.activity.UserInfo;
import com.chat.android.app.dialog.ChatLockPwdDialog;
import com.chat.android.app.dialog.CustomMultiTextItemsDialog;
import com.chat.android.app.utils.AndroidUtilities;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.CommonData;
import com.chat.android.app.utils.DocOpenUtils;
import com.chat.android.app.utils.Emoji;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.GroupMessageUtil;
import com.chat.android.app.utils.MsgInfoUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.TimeStampUtils;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.app.widget.TopCropImageView;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.ChatLockPojo;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.MessageObjectChat;
import com.chat.android.core.model.MultiTextDialogPojo;
import com.chat.android.core.scimbohelperclass.ScimboImageUtils;
import com.chat.android.core.service.Constants;
import com.chat.android.core.uploadtoserver.FileUploadDownloadManager;
import com.chat.android.status.controller.StatusUtil;
import com.chat.android.status.model.StatusModel;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Taha Adnan on 5/11/2022.
 */
public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int MESSAGERECEIVED = 0;
    public static final int MESSAGESENT = 1;
    public static final int IMAGERECEIVED = 2;
    public static final int IMAGESENT = 3;
    public static final int VIDEORECEIVED = 4;
    public static final int VIDEOSENT = 5;
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("h:mm a", Locale.US);
    private static final String TAG = ">>>@@@@@@";
    public static int lastPlayedAt = -1;
    Uri builtUri = null;

    public static Timer mTimer;
    public static MediaPlayer mPlayer;
    private final int LOCATIONRECEIVED = 6;
    private final int LOCATIONSENT = 7;
    private final int CONTACTRECEIVED = 8;
    private final int CONTACTSENT = 9;
    private final int AUDIORECEIVED = 10;
    private final int AUDIOSENT = 11;
    private final int ServerMessAGE = 12;
    private final int WEB_LINK_RECEIVED = 13;
    private final int WEB_LINK_SENT = 14;
    private final int DOCUMENT_RECEIVED = 15;
    private final int DOCUMENT_SENT = 16;
    private final int GROUP_EVENT_INFO = 17;
    private final int MISSED_CALL_INFO = 18;
    private final int MESSAGE_SELF_DELETED = 19;
    private final int MESSAGE_OTHER_DELETED = 20;
    private final int TIMER_CHANGE = 21;
    private final int SCREEN_SHOT_TAKEN = 22;
    String mydate = "";


    // private final int END_TO_ENCRYPTION_INFO = 21;
    private ItemClickListener itemClickListener;
    private ArrayList<MessageItemChat> mOriginalValues = new ArrayList<>();
    private LayoutInflater mInflater;
    private Activity context;
    private ArrayList<MessageItemChat> mListData;
    private Getcontactname getcontactname;
    private SessionManager sessionmanager;
    private String mCurrentUserId = "";
    private boolean isGroupChat = false;
    private FileUploadDownloadManager fileUploadDownloadManager;
    private FragmentManager fragmentManager;
    private UserInfoSession userInfoSession;
    private int selectedItemColor, unSelectedItemColor;
    private int audioPlayedItemPosition;
    private Boolean FirstItemSelected = false;
    private ItemFilter mFilter = new ItemFilter();
    private boolean ConvertViewset = false;


    private boolean isSecretChat = false;


    public MessageAdapter(boolean isSecretChat, Activity context, ArrayList<MessageItemChat> mListData,
                          FragmentManager fragmentManager, ItemClickListener itemClickListener) {

        this.context = context;
        this.isSecretChat = isSecretChat;
        this.mListData = mListData;
        this.mOriginalValues = mListData;
        this.fragmentManager = fragmentManager;
        mInflater = LayoutInflater.from(context);
        getcontactname = new Getcontactname(context);
        sessionmanager = SessionManager.getInstance(context);
        mCurrentUserId = sessionmanager.getCurrentUserID();
        fileUploadDownloadManager = new FileUploadDownloadManager(context);
        userInfoSession = new UserInfoSession(context);
        selectedItemColor = ContextCompat.getColor(context, R.color.selected_chat);
        unSelectedItemColor = Color.TRANSPARENT;


        this.itemClickListener = itemClickListener;
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public void setIsGroupChat(boolean isGroupChat) {

        this.isGroupChat = isGroupChat;
    }

    public void setfirstItemSelected(boolean isFirstItemSelected) {

        FirstItemSelected = isFirstItemSelected;

    }

    public void stopAudioOnClearChat() {

        if (mTimer != null && mPlayer != null) {
            mTimer.cancel();
            mPlayer.release();
        }
    }
    public void stopAudio() {
        lastPlayedAt = -1;
        if (mTimer != null && mPlayer != null) {
            mTimer.cancel();
            mPlayer.release();
        }
    }

    public void updateInfo(ArrayList<MessageItemChat> aitem) {
        this.mListData = aitem;
        notifyDataSetChanged();
    }

    public void updateReceiptsList(ArrayList<MessageItemChat> mListData) {
        mListData.clear();
        mListData.addAll(mListData);
        this.notifyDataSetChanged();
    }

    public Filter getFilter() {
        return mFilter;
    }

    public void stopAudioOnMessageDelete(int position) {
        if (position > -1 && position == lastPlayedAt) {
            mTimer.cancel();
            mPlayer.release();
        }
    }

    public void setConvertviewBoolean(boolean s) {

        ConvertViewset = s;
    }

    public Object getItem(int position) {
        return mListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (mListData != null && mListData.size() > 0) {
            //      MyLog.d(TAG, "getCount: " + mListData.size());
            return mListData.size();
        } else {
//            MyLog.d(TAG, "getCount: 0");
            return 0;
        }
    }

    //    @Override
    public int getViewTypeCount() {
        return 23;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;

        switch (viewType) {
            case MESSAGESENT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_sender_text, parent, false);
                return new ViewHolder1(itemView);

            case IMAGESENT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_image_send_layout, parent, false);
                return new ViewHolder3(itemView);

            case VIDEOSENT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_video_send_layout, parent, false);
                return new ViewHolder4(itemView);

            case LOCATIONSENT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_location_send_layout, parent, false);
                return new ViewHolder7(itemView);

            case CONTACTSENT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_contact_send_layout, parent, false);
                return new ViewHolder5(itemView);

            case AUDIOSENT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_audio_send_layout, parent, false);
                return new ViewHolder6(itemView);

            case DOCUMENT_SENT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_document_send_layout, parent, false);
                return new ViewHolder8(itemView);

            case WEB_LINK_SENT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_web_link_layout, parent, false);
                return new ViewHolder9(itemView);

            case MESSAGE_SELF_DELETED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_send_delete_layout, parent, false);
                return new ViewHolder10(itemView);

            case SCREEN_SHOT_TAKEN:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vh_screen_shot_taken, parent, false);
                return new ViewHolder23(itemView);

            case MESSAGERECEIVED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_receiver_text, parent, false);
                return new ViewHolder2(itemView);

            case IMAGERECEIVED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_receiver_image_layout, parent, false);
                return new ViewHolder11(itemView);

            case VIDEORECEIVED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_video_receive_layout, parent, false);
                return new ViewHolder12(itemView);

            case LOCATIONRECEIVED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_location_receive_layout, parent, false);
                return new ViewHolder20(itemView);

            case CONTACTRECEIVED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_receiver_contact_layout, parent, false);
                return new ViewHolder19(itemView);

            case AUDIORECEIVED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_audio_receive_layout, parent, false);
                return new ViewHolder13(itemView);

            case DOCUMENT_RECEIVED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_document_receive_layout, parent, false);
                return new ViewHolder14(itemView);

            case GROUP_EVENT_INFO:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_group_info_layout, parent, false);
                return new ViewHolder18(itemView);

            case WEB_LINK_RECEIVED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_weblink_receive_layout, parent, false);
                return new ViewHolder15(itemView);

            case MESSAGE_OTHER_DELETED:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_receiver_side_delete_layout, parent, false);
                return new ViewHolder16(itemView);

            case MISSED_CALL_INFO:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_missed_call_layout, parent, false);
                return new ViewHolder17(itemView);

            case TIMER_CHANGE:
            default:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nlc_empty_list_item, parent, false);
                return new ViewHolder22(itemView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(position > mListData.size())
            return;

        int viewType = getItemViewType(position);
        final MessageItemChat message = mListData.get(position);
        if (message.isSelf()) {
            switch (viewType) {
                case MESSAGESENT:
                    if (holder instanceof MessageAdapter.ViewHolder1)
                        bindViewHolder1((MessageAdapter.ViewHolder1) holder, message, position);
                    break;
                case IMAGESENT:
                    if (holder instanceof MessageAdapter.ViewHolder3)
                        bindViewHolder3((MessageAdapter.ViewHolder3) holder, message, position);
                    break;
                case VIDEOSENT:
                    if (holder instanceof MessageAdapter.ViewHolder4)
                        bindViewHolder4((MessageAdapter.ViewHolder4) holder, message, position);
                    break;
                case LOCATIONSENT:
                    if (holder instanceof MessageAdapter.ViewHolder7)
                        bindViewHolder7((MessageAdapter.ViewHolder7) holder, message, position);
                    break;
                case CONTACTSENT:
                    if (holder instanceof MessageAdapter.ViewHolder5)
                        bindViewHolder5((MessageAdapter.ViewHolder5) holder, message, position);
                    break;
                case AUDIOSENT:
                    if (holder instanceof MessageAdapter.ViewHolder6)
                        bindViewHolder6((MessageAdapter.ViewHolder6) holder, message, position);
                    break;
                case DOCUMENT_SENT:
                    if (holder instanceof MessageAdapter.ViewHolder8)
                        bindViewHolder8((MessageAdapter.ViewHolder8) holder, message, position);
                    break;
                case WEB_LINK_SENT:
                    if (holder instanceof MessageAdapter.ViewHolder9)
                        bindViewHolder9((MessageAdapter.ViewHolder9) holder, message, position);
                    break;
                case MESSAGE_SELF_DELETED:
                    if (holder instanceof MessageAdapter.ViewHolder10)
                        bindViewHolder10((MessageAdapter.ViewHolder10) holder, message, position);
                    break;
                case TIMER_CHANGE:
                    if (holder instanceof MessageAdapter.ViewHolder22)
                        bindViewHolder22((MessageAdapter.ViewHolder22) holder, message, position);
                    break;
                case SCREEN_SHOT_TAKEN:
                    if (holder instanceof MessageAdapter.ViewHolder23)
                        bindViewHolder23((MessageAdapter.ViewHolder23) holder, message.getTextMessage(), position);
                    break;
            }
        } else {
            switch (viewType) {
                case MESSAGERECEIVED:
                    if (holder instanceof MessageAdapter.ViewHolder2)
                        bindViewHolder2((MessageAdapter.ViewHolder2) holder, message, position);
                    break;
                case IMAGERECEIVED:
                    if (holder instanceof MessageAdapter.ViewHolder11)
                        bindViewHolder11((MessageAdapter.ViewHolder11) holder, message, position);
                    break;
                case VIDEORECEIVED:
                    if (holder instanceof MessageAdapter.ViewHolder12)
                        bindViewHolder12((MessageAdapter.ViewHolder12) holder, message, position);
                    break;
                case LOCATIONRECEIVED:
                    if (holder instanceof MessageAdapter.ViewHolder20)
                        bindViewHolder20((MessageAdapter.ViewHolder20) holder, message, position);
                    break;
                case CONTACTRECEIVED:
                    if (holder instanceof MessageAdapter.ViewHolder19)
                        bindViewHolder19((MessageAdapter.ViewHolder19) holder, message, position);
                    break;
                case AUDIORECEIVED:
                    if (holder instanceof MessageAdapter.ViewHolder13)
                        bindViewHolder13((MessageAdapter.ViewHolder13) holder, message, position);
                    break;
                case DOCUMENT_RECEIVED:
                    if (holder instanceof MessageAdapter.ViewHolder14)
                        bindViewHolder14((MessageAdapter.ViewHolder14) holder, message, position);
                    break;
                case GROUP_EVENT_INFO:
                    if (holder instanceof MessageAdapter.ViewHolder18)
                        bindViewHolder18((MessageAdapter.ViewHolder18) holder, message, position);
                    break;
                case WEB_LINK_RECEIVED:
                    if (holder instanceof MessageAdapter.ViewHolder15)
                        bindViewHolder15((MessageAdapter.ViewHolder15) holder, message, position);
                    break;
                case MESSAGE_OTHER_DELETED:
                    if (holder instanceof MessageAdapter.ViewHolder16)
                        bindViewHolder16((MessageAdapter.ViewHolder16) holder, message, position);
                    break;
                case TIMER_CHANGE:
                    if (holder instanceof MessageAdapter.ViewHolder4)
                        bindViewHolder4((MessageAdapter.ViewHolder4) holder, message, position);
                    break;
                case SCREEN_SHOT_TAKEN:
                    if (holder instanceof MessageAdapter.ViewHolder23) {
                        String name = getcontactname.getSendername(message.getSenderMsisdn());
                        bindViewHolder23((MessageAdapter.ViewHolder23) holder, name + " " + message.getTextMessage(), position);
                    }
                    break;
                case MISSED_CALL_INFO:
                    if (holder instanceof MessageAdapter.ViewHolder17)
                        bindViewHolder17((MessageAdapter.ViewHolder17) holder, message, position);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {

        String type = mListData.get(position).getMessageType();
        Boolean self = mListData.get(position).isSelf();
        if (type != null) {
            if (self) {

                switch (type) {
                    case ("" + MessageFactory.text):
                    case ("" + MessageFactory.nudge):
                        return MESSAGESENT;

                    case ("" + MessageFactory.picture):
                        return IMAGESENT;

                    case ("" + MessageFactory.video):
                        return VIDEOSENT;

                    case ("" + MessageFactory.location):
                        return LOCATIONSENT;

                    case ("" + MessageFactory.contact):
                        return CONTACTSENT;

                    case ("" + MessageFactory.audio):
                        return AUDIOSENT;

                    case ("" + MessageFactory.document):
                        return DOCUMENT_SENT;

                    case ("" + MessageFactory.web_link):
                        return WEB_LINK_SENT;

                    case ("" + MessageFactory.DELETE_SELF):
                        return MESSAGE_SELF_DELETED;

                /*case ("" + MessageFactory.ENCRYPTION_INFO):
                    return END_TO_ENCRYPTION_INFO;*/

                    case ("" + MessageFactory.timer_change):
                        return TIMER_CHANGE;

                    case ("" + MessageFactory.SCREEN_SHOT_TAKEN):
                        return SCREEN_SHOT_TAKEN;
                    default:
                        return WEB_LINK_SENT;
                }

            } else {

                switch (type) {
                    case ("" + MessageFactory.text):
                    case ("" + MessageFactory.nudge):
                        return MESSAGERECEIVED;

                    case ("" + MessageFactory.picture):
                        return IMAGERECEIVED;

                    case ("" + MessageFactory.video):
                        return VIDEORECEIVED;

                    case ("" + MessageFactory.location):
                        return LOCATIONRECEIVED;

                    case ("" + MessageFactory.contact):
                        return CONTACTRECEIVED;

                    case ("" + MessageFactory.audio):
                        return AUDIORECEIVED;

                    case ("" + MessageFactory.document):
                        return DOCUMENT_RECEIVED;

                    case ("" + MessageFactory.event_info_change_pin):
                    case ("" + MessageFactory.group_event_info):
                        return GROUP_EVENT_INFO;

                    case ("" + MessageFactory.web_link):
                        return WEB_LINK_RECEIVED;

                    case ("" + MessageFactory.DELETE_OTHER):
                        return MESSAGE_OTHER_DELETED;
              /*  case ("" + MessageFactory.ENCRYPTION_INFO):
                    return END_TO_ENCRYPTION_INFO;
*/
                    case ("" + MessageFactory.timer_change):
                        return TIMER_CHANGE;

                    case ("" + MessageFactory.SCREEN_SHOT_TAKEN):
                        return SCREEN_SHOT_TAKEN;

                    default:
                        return MISSED_CALL_INFO;
                }
            }
        } else {
            //Return null or empty else app crashes sometimes...
            return 0;
        }

    }

    private void bindViewHolder1(MessageAdapter.ViewHolder1 holder1, MessageItemChat message, int position) {
        if (message.isTagapplied()) {

            SpannableString ss = makeLinks(message.getEditTextmsg(), message.getSetArrayTagnames(), message.getUserId_tag());
            holder1.messageTextView.setText(ss, TextView.BufferType.SPANNABLE);

        } else if (message.getTextMessage().equalsIgnoreCase("nudge")) {
            holder1.messageTextView.setText(context.getResources().getString(R.string.sent_a_nudge));
        } else {
            MessageObjectChat messageObjectChat = message.getMessageObject();
            String textMessage = message.getTextMessage();
            if (messageObjectChat != null) {
                String translatedMessage = messageObjectChat.getTranslatedMessage();
                if (!TextUtils.isEmpty(translatedMessage)) {
                    textMessage = translatedMessage;
                }
            }
            MyLog.d(TAG, "textMessage: " + textMessage);
            holder1.messageTextView.setText(Html.fromHtml(Emoji.replaceEmoji(textMessage,
                    holder1.messageTextView.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16))
                    + ""));

        }


        configureDateLabel(holder1.tvDateLbl, holder1.encryptionlabel, holder1.tvSecretLbl, position);

        String ts = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        ts = ts.replace(".", "");
        holder1.sent_text_date_time.setText(ts);

        if (message.isSelected())
            holder1.sent_message_main_layout.setBackgroundColor(selectedItemColor);
        else
            holder1.sent_message_main_layout.setBackgroundColor(unSelectedItemColor);


        if (message.getReplyType() != null && !message.getReplyType().equals("")) {

            holder1.send_text_message_normal_layout.setVisibility(View.GONE);
            holder1.send_text_replay_message.setVisibility(View.VISIBLE);


            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                holder1.senter_text_normal_star_icon.setVisibility(View.VISIBLE);
            } else {
                holder1.senter_text_normal_star_icon.setVisibility(View.GONE);
            }


            if (message.isStatusReply()) {
                String name = getcontactname.getSendername(message.getReceiverID(), message.getMsisdn());
                if (name != null) {
                    holder1.sender_replay_message_name_text.setText(name + " . " + context.getResources().getString(R.string.Status));
                }
            } else {
                String replySenderName = message.getReplySenser();
                if (!TextUtils.isEmpty(replySenderName) && replySenderName.length() > 1)
                    replySenderName = replySenderName.substring(0, 1).toUpperCase() + replySenderName.substring(1).toLowerCase();
                holder1.sender_replay_message_name_text.setText(replySenderName);
            }

            holder1.sender_replay_main_message.setText(message.getTextMessage());
            holder1.sent_replay_message_date_time.setText(ts);

            final String replyMsgText = message.getReplyMessage();
            holder1.sender_replay_message_text.setText(replyMsgText);


            int replyMsgType = Integer.parseInt(message.getReplyType());
            switch (replyMsgType) {

                case MessageFactory.picture: {
                    holder1.senter_replay_image_view.setVisibility(View.VISIBLE);
                    holder1.image_photo_icon.setVisibility(View.VISIBLE);
                    holder1.image_photo_icon.setImageResource(R.drawable.ib_camera);
                    holder1.sender_replay_message_text.setText(context.getResources().getString(R.string.photo));

                    if (message.getreplyimagebase64() != null && !message.getreplyimagebase64().equals("")) {
                        //String thumbData = message.getreplyimagebase64();
                        try {
/*                                        Bitmap thumbBmp = ScimboImageUtils.decodeBitmapFromBase64(thumbData, 50, 50);
                                        holder1.senter_replay_image_view.setImageBitmap(thumbBmp);*/
                            AppUtils.loadBase64Image(50, context, message.getreplyimagebase64(), holder1.senter_replay_image_view);
                        } catch (Exception ex) {
                            MyLog.e(TAG, "", ex);
                        }
                    } else {
                        holder1.senter_replay_image_view.setImageBitmap(null);
                    }
                }
                break;

                case MessageFactory.audio: {
                    holder1.senter_replay_image_view.setVisibility(View.GONE);
                    holder1.image_photo_icon.setVisibility(View.VISIBLE);
                    holder1.image_photo_icon.setImageResource(R.drawable.ic_audio_storage_usage);
                    holder1.sender_replay_message_text.setText(context.getResources().getString(R.string.audio));
                }
                break;

                case MessageFactory.document: {
                    holder1.senter_replay_image_view.setVisibility(View.GONE);
                    holder1.image_photo_icon.setVisibility(View.VISIBLE);
                    holder1.image_photo_icon.setImageResource(R.drawable.ic_documents_storage_usage);

                    String replydocumentMsgText = message.getReplyMessage();
                    if (replydocumentMsgText != null && !replydocumentMsgText.equals("")) {

                        holder1.sender_replay_message_text.setText(replydocumentMsgText);
                    } else {

                        holder1.sender_replay_message_text.setText(context.getResources().getString(R.string.document));
                    }

                }
                break;

                case MessageFactory.contact: {
                    holder1.senter_replay_image_view.setVisibility(View.GONE);
                    holder1.image_photo_icon.setVisibility(View.VISIBLE);
                    holder1.image_photo_icon.setImageResource(R.drawable.ic_contacts_storage_usage);
                    final String contactName = message.getContactName();

                    String replycontactMsgText = message.getReplyMessage();
                    if (replycontactMsgText != null && !replycontactMsgText.equals("")) {
                        holder1.sender_replay_message_text.setText(replycontactMsgText);
                    } else {

                        holder1.sender_replay_message_text.setText(context.getResources().getString(R.string.contact));
                    }


                }
                break;

                case MessageFactory.location: {
                    holder1.senter_replay_image_view.setVisibility(View.VISIBLE);
                    holder1.image_photo_icon.setVisibility(View.VISIBLE);
                    holder1.image_photo_icon.setImageResource(R.drawable.ic_location_storage_usage);
                    holder1.sender_replay_message_text.setText(context.getResources().getString(R.string.location));

                    if (message.getreplyimagebase64() != null && !message.getreplyimagebase64().equals("")) {
                        //String thumbData = message.getreplyimagebase64();
                        try {
/*                                        Bitmap thumbBmp = ScimboImageUtils.decodeBitmapFromBase64(thumbData, 50, 50);
                                        holder1.senter_replay_image_view.setImageBitmap(thumbBmp);*/
                            AppUtils.loadBase64Image(50, context, message.getreplyimagebase64(), holder1.senter_replay_image_view);
                        } catch (Exception ex) {
                            MyLog.e(TAG, "", ex);
                        }
                    } else {
                        holder1.senter_replay_image_view.setImageBitmap(null);
                    }
                }
                break;

                case MessageFactory.video: {
                    holder1.senter_replay_image_view.setVisibility(View.VISIBLE);
                    holder1.image_photo_icon.setVisibility(View.VISIBLE);
                    holder1.image_photo_icon.setImageResource(R.drawable.ic_video_storage_usage);
                    try {
/*                                    Bitmap photo = ScimboImageUtils.decodeBitmapFromBase64(message.getreplyimagebase64(), 30, 30);
                                    holder1.senter_replay_image_view.setImageBitmap(photo);*/
                        AppUtils.loadBase64Image(30, context, message.getreplyimagebase64(), holder1.senter_replay_image_view);
                    } catch (Exception ex) {
                        MyLog.e(TAG, "", ex);
                    }
                }
                break;

                default:

                    holder1.senter_replay_image_view.setVisibility(View.GONE);
                    holder1.image_photo_icon.setVisibility(View.GONE);

                    break;
            }


            setMessageDeliveryStatus(message, holder1.tvReadReceipt);

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                holder1.senter_text_replay_star_icon.setVisibility(View.VISIBLE);
            } else {
                holder1.senter_text_replay_star_icon.setVisibility(View.GONE);
            }

        } else {


            holder1.send_text_replay_message.setVisibility(View.GONE);
            holder1.send_text_message_normal_layout.setVisibility(View.VISIBLE);

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                holder1.senter_text_normal_star_icon.setVisibility(View.VISIBLE);
            } else {
                holder1.senter_text_normal_star_icon.setVisibility(View.GONE);
            }

            setMessageDeliveryStatus(message, holder1.tvReadReceipt);
        }
    }

    private void bindViewHolder2(MessageAdapter.ViewHolder2 holder2, MessageItemChat message, int position) {
        configureDateLabel(holder2.tvDateLbl, holder2.encryptionlabel, holder2.tvSecretLbl, position);
        String ts = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        ts = ts.replace(".", "");
        holder2.received_text_date_time.setText(ts);

        String messageString = message.getTextMessage();
        String itemViewType = message.getMessageType();
        if (itemViewType.contains(("" + MessageFactory.nudge))) {
            holder2.received_Text_message.setText(context.getResources().getString(R.string.received_a_nudge));
        } else {
            MessageObjectChat messageObjectChat = message.getMessageObject();
            String textMessage = message.getTextMessage();
            if (messageObjectChat != null) {
                String translatedMessage = messageObjectChat.getTranslatedMessage();
                if (!TextUtils.isEmpty(translatedMessage)) {
                    textMessage = translatedMessage;
                }
            }
            holder2.received_Text_message.setText(textMessage);
        }
        //     Log.e("message.getReplyType() ", "message.getReplyType() " + message.getReplyType()+"message.getTextMessage()"+message.getTextMessage()+"message.getReplyMessage()"+message.getReplyMessage());


        if (message.isSelected())

            holder2.recieved_message_main_layout.setBackgroundColor(selectedItemColor);
        else
            holder2.recieved_message_main_layout.setBackgroundColor(unSelectedItemColor);
        //      Log.e("message.getReplyType() ", "message.getReplyType() " + message.getReplyType()+"message.getTextMessage()"+message.getTextMessage());
        if (message.getReplyType() != null && !message.getReplyType().equals("") && !message.getReplyMessage().equals("")) {

            //   if (message.getReplyType() != null  && !message.getReplyType().equals("") && Integer.parseInt(message.getReplyType())!=0) {

            //       Log.e("message.getReplyType() ", "message.getReplyType() " + message.getReplyType());
            //    Log.e("message.getReplyType() ", "message.getNewMessageCount() " + message.getNewMessageCount());

            //    Log.e("message.getReplyType() ", "message.getTextMessage() " + message.getTextMessage());
            holder2.receiver_normal_message_layout.setVisibility(View.GONE);

            holder2.receiver_replay_message_main_layout.setVisibility(View.VISIBLE);


            if (isGroupChat) {
                holder2.receiver_text_group_replay_sender_name.setVisibility(View.GONE);
                String msisdn = message.getSenderName();
                holder2.receiver_text_group_replay_sender_name.setText(getcontactname.getGroupMemberName(message.getGroupMsgFrom(), msisdn));
            } else {
                holder2.receiver_text_group_replay_sender_name.setVisibility(View.GONE);
            }


            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                holder2.receiver_text_replay_star_icon.setVisibility(View.VISIBLE);
            } else {
                holder2.receiver_text_replay_star_icon.setVisibility(View.GONE);
            }


            // TODO: :Roman: Move String to resource
            //               There are multiple lines
            if (message.isStatusReply()) {
                String name = getcontactname.getSendername(message.getReceiverID(), message.getMsisdn());
                if (name != null)
                    holder2.receiver_replay_message_name_text.setText(name + " . " + context.getResources().getString(R.string.status));
            } else {
                String replySenderName = message.getReplySenser();
                if (!TextUtils.isEmpty(replySenderName) && replySenderName.length() > 1)
                    replySenderName = replySenderName.substring(0, 1).toUpperCase() + replySenderName.substring(1).toLowerCase();
                holder2.receiver_replay_message_name_text.setText(replySenderName);
            }

            holder2.receiver_replay_main_message.setText(message.getTextMessage());
            holder2.receiver_replay_message_date_time.setText(ts);

            final String replyMsgText = message.getReplyMessage();
            holder2.receiver_replay_message_text.setText(replyMsgText);


            int replyMsgType = Integer.parseInt(message.getReplyType());
            switch (replyMsgType) {

                case MessageFactory.picture: {
                    holder2.receiver_replay_image_view.setVisibility(View.VISIBLE);
                    holder2.receiver_image_photo_icon.setVisibility(View.VISIBLE);
                    holder2.receiver_image_photo_icon.setImageResource(R.drawable.ib_camera);
                    holder2.receiver_replay_message_text.setText(context.getResources().getString(R.string.photo));

                    if (message.getreplyimagebase64() != null && !message.getreplyimagebase64().equals("")) {
                        //String thumbData = message.getreplyimagebase64();
                        try {
/*                                        Bitmap thumbBmp = ScimboImageUtils.decodeBitmapFromBase64(thumbData, 50, 50);
                                        holder2.receiver_replay_image_view.setImageBitmap(thumbBmp);*/
                            AppUtils.loadBase64Image(30, context, message.getreplyimagebase64(), holder2.receiver_replay_image_view);
                        } catch (Exception ex) {
                            MyLog.e(TAG, "", ex);
                        }
                    } else {
                        holder2.receiver_replay_image_view.setImageBitmap(null);
                    }
                }
                break;

                case MessageFactory.audio: {
                    holder2.receiver_replay_image_view.setVisibility(View.GONE);
                    holder2.receiver_image_photo_icon.setVisibility(View.VISIBLE);
                    holder2.receiver_image_photo_icon.setImageResource(R.drawable.ic_audio_storage_usage);
                    holder2.receiver_replay_message_text.setText(context.getResources().getString(R.string.audio));
                }
                break;

                case MessageFactory.document: {
                    holder2.receiver_replay_image_view.setVisibility(View.GONE);
                    holder2.receiver_image_photo_icon.setVisibility(View.VISIBLE);
                    holder2.receiver_image_photo_icon.setImageResource(R.drawable.ic_documents_storage_usage);

                    String replydocumentMsgText = message.getReplyMessage();
                    if (replydocumentMsgText != null && !replydocumentMsgText.equals("")) {

                        holder2.receiver_replay_message_text.setText(replydocumentMsgText);
                    } else {

                        holder2.receiver_replay_message_text.setText(context.getResources().getString(R.string.document));
                    }

                }
                break;

                case MessageFactory.contact: {
                    holder2.receiver_replay_image_view.setVisibility(View.GONE);
                    holder2.receiver_image_photo_icon.setVisibility(View.VISIBLE);
                    holder2.receiver_image_photo_icon.setImageResource(R.drawable.ic_contacts_storage_usage);

                    String replycontactMsgText = message.getReplyMessage();
                    if (replycontactMsgText != null && !replycontactMsgText.equals("")) {
                        holder2.receiver_replay_message_text.setText(replycontactMsgText);
                    } else {

                        holder2.receiver_replay_message_text.setText(context.getResources().getString(R.string.contact));
                    }
                }
                break;

                case MessageFactory.location: {
                    holder2.receiver_replay_image_view.setVisibility(View.VISIBLE);
                    holder2.receiver_image_photo_icon.setVisibility(View.VISIBLE);
                    holder2.receiver_image_photo_icon.setImageResource(R.drawable.ic_location_storage_usage);
                    holder2.receiver_replay_message_text.setText(context.getResources().getString(R.string.location));

                    if (message.getreplyimagebase64() != null && !message.getreplyimagebase64().equals("")) {
                        // String thumbData = message.getreplyimagebase64();
                        try {
/*                                        Bitmap thumbBmp = ScimboImageUtils.decodeBitmapFromBase64(thumbData, 50, 50);
                                        holder2.receiver_replay_image_view.setImageBitmap(thumbBmp);*/
                            AppUtils.loadBase64Image(50, context, message.getreplyimagebase64(), holder2.receiver_replay_image_view);
                        } catch (Exception ex) {
                            MyLog.e(TAG, "", ex);
                        }
                    } else {
                        holder2.receiver_replay_image_view.setImageBitmap(null);
                    }
                }
                break;

                case MessageFactory.video: {
                    holder2.receiver_replay_image_view.setVisibility(View.VISIBLE);
                    holder2.receiver_image_photo_icon.setVisibility(View.VISIBLE);
                    holder2.receiver_image_photo_icon.setImageResource(R.drawable.ic_video_storage_usage);
                    holder2.receiver_replay_message_text.setText(context.getResources().getString(R.string.video));
                    try {
                                    /*Bitmap photo = ScimboImageUtils.decodeBitmapFromBase64(message.getreplyimagebase64(), 30, 30);
                                    holder2.receiver_replay_image_view.setImageBitmap(photo);*/
                        AppUtils.loadBase64Image(30, context, message.getreplyimagebase64(), holder2.receiver_replay_image_view);
                    } catch (Exception ex) {
                        MyLog.e(TAG, "", ex);
                    }
                }
                break;

                default:

                    holder2.receiver_replay_image_view.setVisibility(View.GONE);
                    holder2.receiver_image_photo_icon.setVisibility(View.GONE);
                    break;
            }


        } else {
            if (isGroupChat) {
                MessageItemChat prev = position > 0 ? mListData.get(position - 1) : null;
                boolean isConsecutive = isContinuous(message, prev);
                if (!isConsecutive) {
                    holder2.receiver_text_group_sender_name.setVisibility(View.VISIBLE);
                    String msisdn = message.getSenderName();
                    holder2.receiver_text_group_sender_name.setText(getcontactname.getGroupMemberName(message.getGroupMsgFrom(), msisdn));
                } else
                    holder2.receiver_text_group_sender_name.setVisibility(View.GONE);

            } else {
                holder2.receiver_text_group_sender_name.setVisibility(View.GONE);
            }

            holder2.receiver_replay_message_main_layout.setVisibility(View.GONE);
            holder2.receiver_normal_message_layout.setVisibility(View.VISIBLE);

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                holder2.receiver_text_normal_star_icon.setVisibility(View.VISIBLE);
            } else {
                holder2.receiver_text_normal_star_icon.setVisibility(View.GONE);
            }
        }


        if (message.isStatusReply()) {

            holder2.recieved_message_main_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View convertView) {
                    try {
                        if (message.getRecordId() != null) {
                            String reccrdId = message.getReplyId();
                            StatusModel statusModel = CoreController.getStatusDB(context).getParticularStatusByRecordId(reccrdId);
                            if (statusModel != null) {
                                StatusUtil.startStatusViewActivity(context, statusModel, true, true, false);
                            } else {
                                Toast.makeText(context, context.getResources().getString(R.string.status_not_available), Toast.LENGTH_SHORT).show();
                            }
                        }

                    } catch (Exception e) {

                    }
                }
            });
        }

    }

    private void bindViewHolder3(MessageAdapter.ViewHolder3 holder3, MessageItemChat message, int position) {
        configureDateLabel(holder3.tvDateLbl, holder3.encryptionlabel, holder3.tvSecretLbl, position);

        String Caption_text = message.getCaption();

        if (Caption_text == null) {

            Caption_text = "";
        }

        String imgPath = message.getChatFileLocalPath();

        int imageHeight = 0;
        imageHeight = AppUtils.parseInt(message.getChatFileHeight());

        try {

            File file = new File(imgPath);

            if (file.canRead()) {
                if (imageHeight > 500) {
                    holder3.sender_side_imageview.setVisibility(View.GONE);
                    holder3.top_crop_imageview.setVisibility(View.VISIBLE);
                    AppUtils.AdapterloadLocalImage(context, imgPath, holder3.top_crop_imageview);
                } else {
                    holder3.top_crop_imageview.setVisibility(View.GONE);
                    holder3.sender_side_imageview.setVisibility(View.VISIBLE);
                    AppUtils.AdapterloadLocalImage(context, imgPath, holder3.sender_side_imageview);
                }
            } else {
                //check it is receiver or admin
                if (!mCurrentUserId.equals(message.getReceiverID())) {
                    String mServerImage = AppUtils.getValidGroupPath(message.getChatFileServerPath());
                    Glide
                            .with(context)
                            .load(AppUtils.getGlideURL(mServerImage, context))
                            .asBitmap()
                            .error(R.drawable.nav_menu_background)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .dontAnimate()
                            .fitCenter()
                            .into(new SimpleTarget<Bitmap>() {

                                @Override
                                public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                    // TODO Auto-generated method stub
                                    holder3.top_crop_imageview.setImageBitmap(arg0);
                                }

                                @Override
                                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                    super.onLoadFailed(e, errorDrawable);
                                }
                            });
                    holder3.top_crop_imageview.setVisibility(View.VISIBLE);
                    holder3.sender_side_imageview.setVisibility(View.GONE);

                } else {
                    if (message.getChatFileServerPath() != null) {
                        try {
                            String thumbData = message.getThumbnailData();
                            if (thumbData.contains(",")) {
                                thumbData = thumbData.substring(thumbData.indexOf(",") + 1);
                            }
                            AppUtils.loadBase64Image(200, context, thumbData, holder3.top_crop_imageview);
                        } catch (Exception e) {
                            Log.e(TAG, "getView: ", e);
                        }
                    } else {
                        try {
                            String thumbData = message.getThumbnailData();


                            if (thumbData.contains(",")) {
                                thumbData = thumbData.substring(thumbData.indexOf(",") + 1);
                            }
                            AppUtils.loadBase64Image(200, context, thumbData, holder3.top_crop_imageview);
                        } catch (Exception e) {
                            Log.e(TAG, "getView: ", e);
                        }
                    }
                }
            }
            String mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
            mydate = mydate.replace(".", "");


            if (message.isSelected())
                holder3.image_send_layout.setBackgroundColor(selectedItemColor);
            else
                holder3.image_send_layout.setBackgroundColor(unSelectedItemColor);


            if (Caption_text.length() > 0) {
                holder3.send_image_caption_layout.setVisibility(View.VISIBLE);
                holder3.send_image_inside_date_layout.setVisibility(View.GONE);
                holder3.send_image_caption_text.setText(Caption_text);
                holder3.sent_image_caption_date_time.setText(mydate);
                holder3.sent_text_date_time.setText(mydate);

                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    holder3.senter_image_caption_star_icon.setVisibility(View.VISIBLE);
                } else {
                    holder3.senter_image_caption_star_icon.setVisibility(View.GONE);
                }

            } else {
                holder3.send_image_caption_layout.setVisibility(View.GONE);
                holder3.send_image_inside_date_layout.setVisibility(View.VISIBLE);
                holder3.sent_text_date_time.setText(mydate);

                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    holder3.senter_text_normal_star_icon.setVisibility(View.VISIBLE);
                } else {
                    holder3.senter_text_normal_star_icon.setVisibility(View.GONE);
                }

            }


            if ((message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING &&
                    message.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_NOT_SENT))
                    || message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_DOWNLOADING) {
                try {
                    holder3.sender_forward_image.setVisibility(View.GONE);
                    holder3.senter_image_pause_resume_layout.setVisibility(View.VISIBLE);

                    boolean isFIlePaused = fileUploadDownloadManager.isFilePaused(message.getMessageId());

                    MessageDbController dbController = CoreController.getDBInstance(context);

                    String Upload_status = dbController.fileuploadStatusget(message.getMessageId());

                    if (Upload_status.equalsIgnoreCase("pause")) {
                        holder3.sender_image_upload_progress_Layout.setVisibility(View.GONE);
                        holder3.senter_image_Retry_layout.setVisibility(View.VISIBLE);
                        holder3.tvReadReceipt.setVisibility(View.GONE);

                    } else if (Upload_status.equalsIgnoreCase("completed")) {

                        holder3.sender_forward_image.setVisibility(View.GONE);
                        holder3.senter_image_pause_resume_layout.setVisibility(View.GONE);
                        holder3.senter_image_Retry_layout.setVisibility(View.GONE);
                        holder3.tvReadReceipt.setVisibility(View.VISIBLE);
                        holder3.sender_image_upload_progress_Layout.setVisibility(View.GONE);

                    } else {

                        holder3.senter_image_Retry_layout.setVisibility(View.GONE);
                        holder3.tvReadReceipt.setVisibility(View.VISIBLE);
                        holder3.sender_image_upload_progress_Layout.setVisibility(View.VISIBLE);

                        if (message.getUploadDownloadProgress() == 0) {
                            holder3.sender_image_before_upload_progress.setVisibility(View.VISIBLE);
                            holder3.senter_image_uploadprogress_bar.setVisibility(View.GONE);
                        } else if (message.getUploadDownloadProgress() > 0) {

                            holder3.sender_image_before_upload_progress.setVisibility(View.GONE);
                            holder3.senter_image_uploadprogress_bar.setVisibility(View.VISIBLE);
                            holder3.senter_image_uploadprogress_bar.setProgress(message.getUploadDownloadProgress());
                        }

                    }


                    if (message.getUploadDownloadProgress() >= 100) {

                        holder3.sender_image_before_upload_progress.setVisibility(View.VISIBLE);
                        holder3.senter_image_uploadprogress_bar.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    MyLog.e(TAG, "Error", e);
                }
            } else {

                holder3.sender_forward_image.setVisibility(View.GONE);
                holder3.senter_image_pause_resume_layout.setVisibility(View.GONE);
                holder3.senter_image_Retry_layout.setVisibility(View.GONE);
                holder3.tvReadReceipt.setVisibility(View.VISIBLE);
                holder3.sender_image_upload_progress_Layout.setVisibility(View.GONE);

            }

            setMessageDeliveryStatus(message, holder3.tvReadReceipt);

            if (fileUploadDownloadManager.isFilePaused(message.getMessageId())
                    && message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING
            ) {
                //show the pause icon
                holder3.sender_image_upload_progress_Layout.setVisibility(View.GONE);
                holder3.senter_image_Retry_layout.setVisibility(View.VISIBLE);
                holder3.tvReadReceipt.setVisibility(View.GONE);
                holder3.senter_image_Retry_layout.setTag("paused");

            }

            if (message.getUploadStatus() == 1000) {

                holder3.sender_image_upload_progress_Layout.setVisibility(View.GONE);
                holder3.senter_image_Retry_layout.setVisibility(View.VISIBLE);
                holder3.tvReadReceipt.setVisibility(View.GONE);
                holder3.senter_image_upload_text.setText(context.getResources().getString(R.string.upload_failed));
            } else {

                holder3.senter_image_upload_text.setText(context.getResources().getString(R.string.retry));
            }


            holder3.senter_image_pause_resume_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (message.getUploadStatus() != 1000) {

                        MessageDbController dbController = CoreController.getDBInstance(context);

                        String Upload_status = dbController.fileuploadStatusget(message.getMessageId());

                        if (Upload_status.equalsIgnoreCase("uploading")) {


                            holder3.sender_image_upload_progress_Layout.setVisibility(View.GONE);
                            holder3.senter_image_Retry_layout.setVisibility(View.VISIBLE);
                            holder3.tvReadReceipt.setVisibility(View.GONE);
                            JSONObject object = dbController.fileuploadobjectget(message.getMessageId());
                            dbController.fileuploadupdateMessage(message.getMessageId(), "pause", object);

                        } else if (Upload_status.equalsIgnoreCase("pause")) {


                            holder3.senter_image_Retry_layout.setVisibility(View.GONE);
                            holder3.tvReadReceipt.setVisibility(View.VISIBLE);
                            holder3.sender_image_upload_progress_Layout.setVisibility(View.VISIBLE);

                            JSONObject object = dbController.fileuploadobjectget(message.getMessageId());
                            dbController.fileuploadupdateMessage(message.getMessageId(), "uploading", object);

                            fileUploadDownloadManager.startFileUpload(EventBus.getDefault(), object);

                        }
                    }
                }
            });

            if (imageHeight > 500) {

                holder3.top_crop_imageview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final String image = message.getChatFileLocalPath();
                        File imgFile = new File(image);

                        if (imgFile.exists()) {
                            if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {
                                itemClickListener.itemClick(position);
                            } else {
                                Intent intent = new Intent(context, ImageZoom.class);
                                intent.putExtra("image", image);
                                ChatPageActivity.Companion.setNoNeedRefresh(true);
                                context.startActivity(intent);
                            }
                        }
                    }
                });

                holder3.top_crop_imageview.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        itemClickListener.itemLongClick(position);
                        return false;
                    }
                });

            } else {
                if (!mCurrentUserId.equals(message.getReceiverID())) {
                    holder3.top_crop_imageview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            final String image = message.getChatFileLocalPath();
                            File imgFile = new File(image);

                            if (imgFile.exists()) {
                                if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {
                                    itemClickListener.itemClick(position);
                                } else {
                                    Intent intent = new Intent(context, ImageZoom.class);
                                    intent.putExtra("image", image);
                                    ChatPageActivity.Companion.setNoNeedRefresh(true);
                                    context.startActivity(intent);
                                }
                            } else {
                                Intent intent = new Intent(context, ImageZoom.class);

                                builtUri = Uri.parse(AppUtils.getValidGroupPath(message.getChatFileServerPath()))
                                        .buildUpon()
                                        .appendQueryParameter("atoken", SessionManager.getInstance(context).getSecurityToken())
                                        .appendQueryParameter("au", SessionManager.getInstance(context).getCurrentUserID())
                                        .appendQueryParameter("at", "site")
                                        .build();
                                intent.putExtra("image", builtUri.toString());
                                context.startActivity(intent);
                            }
                        }
                    });

                    holder3.top_crop_imageview.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            itemClickListener.itemLongClick(position);
                            return false;
                        }
                    });

                }
                holder3.sender_side_imageview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {

                            itemClickListener.itemClick(position);
                        } else {
                            final String image = message.getChatFileLocalPath();
                            File imgFile = new File(image);

                            if (imgFile.exists()) {

                                Intent intent = new Intent(context, ImageZoom.class);
                                intent.putExtra("image", image);
                                ChatPageActivity.Companion.setNoNeedRefresh(true);
                                context.startActivity(intent);
                            } else {
                                if (!mCurrentUserId.equals(message.getReceiverID())) {
                                    Intent intent = new Intent(context, ImageZoom.class);
                                    intent.putExtra("image", image);
                                    ChatPageActivity.Companion.setNoNeedRefresh(true);
                                    context.startActivity(intent);
                                }
                            }
                        }
                    }
                });
                holder3.sender_side_imageview.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        itemClickListener.itemLongClick(position);
                        return false;
                    }
                });
            }

            holder3.sender_forward_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                    selectedChatItems.add(message);

                    Bundle fwdBundle = new Bundle();
                    //   fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                    fwdBundle.putBoolean("FromScimbo", true);
                    CommonData.setForwardedItems(selectedChatItems);

                    Intent intent = new Intent(context, ForwardContact.class);
                    intent.putExtras(fwdBundle);
                    context.startActivity(intent);
                }
            });


        } catch (Exception e) {

            //System.out.println("Exception" + e.getMessage().toString());
        }
    }

    private void bindViewHolder4(MessageAdapter.ViewHolder4 holder4, MessageItemChat message, int position) {
        configureDateLabel(holder4.tvDateLbl, holder4.encryptionlabel, holder4.tvSecretLbl, position);


        String surationasec = "";
        String Caption_text_video = message.getTextMessage();

        mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");

        if (message.isSelected())
            holder4.video_send_layout.setBackgroundColor(selectedItemColor);
        else
            holder4.video_send_layout.setBackgroundColor(unSelectedItemColor);

        MediaMetadataRetriever mdr = new MediaMetadataRetriever();

        if (message.getChatFileLocalPath() != null) {
            try {
                mdr.setDataSource(message.getChatFileLocalPath());
                String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                surationasec = getTimeString(AppUtils.parseLong(duration));

            } catch (Exception e) {

            }
        }

        if (Caption_text_video.length() > 0) {

            holder4.senter_video_icon_and_duration_layout.setVisibility(View.GONE);
            holder4.sent_video_tick_icon.setVisibility(View.GONE);
            holder4.sent_video_date_time.setVisibility(View.GONE);
            holder4.sent_video_inside_date_layout.setVisibility(View.VISIBLE);
            holder4.sent_video_caption_layout.setVisibility(View.VISIBLE);
            holder4.senter_video_normal_star_icon.setVisibility(View.GONE);
            holder4.sent_video_caption_text.setText(Caption_text_video);
            holder4.sent_video_caption_date_time.setText(mydate);
            holder4.sent_caption_video_duration_text.setText(surationasec);

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                holder4.senter_video_caption_star_icon.setVisibility(View.VISIBLE);
            } else {
                holder4.senter_video_caption_star_icon.setVisibility(View.GONE);
            }


        } else {
            holder4.senter_video_icon_and_duration_layout.setVisibility(View.VISIBLE);
            holder4.sent_video_inside_date_layout.setVisibility(View.VISIBLE);
            holder4.sent_video_tick_icon.setVisibility(View.GONE);
            holder4.sent_video_date_time.setVisibility(View.VISIBLE);
            holder4.sent_video_caption_layout.setVisibility(View.GONE);
            holder4.senter_video_caption_star_icon.setVisibility(View.GONE);
            holder4.sent_video_date_time.setText(mydate);
            holder4.sent_video_duration_text.setText(surationasec);

            if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                holder4.senter_video_normal_star_icon.setVisibility(View.VISIBLE);
            } else {
                holder4.senter_video_normal_star_icon.setVisibility(View.GONE);
            }

        }


        if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING &&
                message.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_NOT_SENT)) {
            try {

                boolean isFIlePaused = fileUploadDownloadManager.isFilePaused(message.getMessageId());


                MessageDbController dbController = CoreController.getDBInstance(context);

                String Upload_status = dbController.fileuploadStatusget(message.getMessageId());


                holder4.senter_video_forward_image.setVisibility(View.GONE);
                holder4.senter_video_Retry_layout.setVisibility(View.GONE);
                holder4.tvReadReceipt.setVisibility(View.VISIBLE);
                holder4.senter_video_icon_and_duration_layout.setVisibility(View.GONE);
                holder4.senter_video_pause_resume.setVisibility(View.VISIBLE);


                if (Upload_status.equalsIgnoreCase("pause")) {
                    holder4.sender_video_upload_progress_Layout.setVisibility(View.GONE);
                    holder4.senter_video_Retry_layout.setVisibility(View.VISIBLE);
                    holder4.tvReadReceipt.setVisibility(View.GONE);

                } else if (Upload_status.equalsIgnoreCase("completed")) {

                    holder4.senter_video_pause_resume.setVisibility(View.GONE);
                    holder4.senter_video_Retry_layout.setVisibility(View.GONE);
                    holder4.tvReadReceipt.setVisibility(View.VISIBLE);
                    holder4.sender_video_upload_progress_Layout.setVisibility(View.GONE);

                    holder4.senter_video_forward_image.setVisibility(View.GONE);

                    if (Caption_text_video.length() > 0) {

                        holder4.senter_video_icon_and_duration_layout.setVisibility(View.GONE);

                    } else {

                        holder4.senter_video_icon_and_duration_layout.setVisibility(View.VISIBLE);

                    }

                } else {

                    holder4.senter_video_Retry_layout.setVisibility(View.GONE);
                    holder4.tvReadReceipt.setVisibility(View.VISIBLE);
                    holder4.sender_video_upload_progress_Layout.setVisibility(View.VISIBLE);

                    if (message.getUploadDownloadProgress() == 0) {

                        holder4.senter_video_uploadprogress_bar.setVisibility(View.GONE);
                        holder4.sender_video_before_upload_progress.setVisibility(View.VISIBLE);

                    } else if (message.getUploadDownloadProgress() > 0) {

                        holder4.sender_video_before_upload_progress.setVisibility(View.GONE);
                        holder4.senter_video_uploadprogress_bar.setVisibility(View.VISIBLE);
                        holder4.senter_video_uploadprogress_bar.setProgress(message.getUploadDownloadProgress());
                    }

                }


                if (message.getUploadDownloadProgress() >= 100) {

                    holder4.senter_video_uploadprogress_bar.setVisibility(View.GONE);
                    holder4.sender_video_before_upload_progress.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                MyLog.e(TAG, "Error", e);
            }
        } else {

            holder4.senter_video_pause_resume.setVisibility(View.GONE);
            holder4.senter_video_Retry_layout.setVisibility(View.GONE);
            holder4.tvReadReceipt.setVisibility(View.VISIBLE);
            holder4.sender_video_upload_progress_Layout.setVisibility(View.GONE);

            holder4.senter_video_forward_image.setVisibility(View.GONE);

            if (Caption_text_video.length() > 0) {

                holder4.senter_video_icon_and_duration_layout.setVisibility(View.GONE);
            } else {

                holder4.senter_video_icon_and_duration_layout.setVisibility(View.VISIBLE);

            }

        }

        String thumbData = message.getThumbnailData();
        if (thumbData != null && thumbData.startsWith("data:image/jpeg;base64,")) {
            thumbData = thumbData.replace("data:image/jpeg;base64,", "");
        }
        try {
            if (thumbData != null)
                AppUtils.loadBase64Image(200, context, thumbData, holder4.sender_side_videoimage);
        } catch (Exception ex) {
            MyLog.e(TAG, "", ex);
        }


        setMessageDeliveryStatus(message, holder4.tvReadReceipt);

        holder4.senter_video_pause_resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MessageDbController dbController = CoreController.getDBInstance(context);

                String Upload_status = dbController.fileuploadStatusget(message.getMessageId());

                if (Upload_status.equalsIgnoreCase("uploading")) {

                    holder4.sender_video_upload_progress_Layout.setVisibility(View.GONE);
                    holder4.senter_video_Retry_layout.setVisibility(View.VISIBLE);
                    holder4.tvReadReceipt.setVisibility(View.GONE);
                    JSONObject object = dbController.fileuploadobjectget(message.getMessageId());
                    dbController.fileuploadupdateMessage(message.getMessageId(), "pause", object);

                } else {

                    holder4.senter_video_Retry_layout.setVisibility(View.GONE);
                    holder4.tvReadReceipt.setVisibility(View.VISIBLE);
                    holder4.senter_video_uploadprogress_bar.setVisibility(View.GONE);
                    holder4.sender_video_upload_progress_Layout.setVisibility(View.VISIBLE);
                    holder4.sender_video_before_upload_progress.setVisibility(View.VISIBLE);


                    JSONObject object = dbController.fileuploadobjectget(message.getMessageId());
                    dbController.fileuploadupdateMessage(message.getMessageId(), "uploading", object);

                    fileUploadDownloadManager.startFileUpload(EventBus.getDefault(), object);
                }

            }
        });


        holder4.sender_side_videoimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mPlayer != null) {

                    if (lastPlayedAt > -1) {
                        mListData.get(lastPlayedAt).setIsMediaPlaying(false);
                        mTimer.cancel();
                        mPlayer.release();
                    }
                    notifyDataSetChanged();
                }
                try {
                    if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {
                        itemClickListener.itemClick(position);
                    } else {
                        String videoPath = "";
                        videoPath = message.getChatFileLocalPath();
                        //  if(CoreController.isRaad){
                        Intent intent = new Intent(context, FullScreenVideoActivity.class);
                        intent.putExtra("filePath", AppUtils.getValidLocalVideoPath(videoPath));
                        ChatPageActivity.Companion.setNoNeedRefresh(true);
                        context.startActivity(intent);
                    }
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(context, context.getResources().getString(R.string.no_app_to_play_video), Toast.LENGTH_LONG).show();
                }


            }
        });
        holder4.sender_side_videoimage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });


        holder4.senter_video_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
                // fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);
                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);

            }
        });
        holder4.senter_video_forward_image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });
    }

    private void bindViewHolder5(MessageAdapter.ViewHolder5 holder5, MessageItemChat message, int position) {
        configureDateLabel(holder5.tvDateLbl, holder5.encryptionlabel, holder5.tvSecretLbl, position);

        String contact_mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = contact_mydate.replace(".", "");
        holder5.senter_contact_date_text.setText(mydate);

        final String contactName = message.getContactName();
        final String contactNumber = message.getContactNumber();
        final String contactAvatar = message.getAvatarImageUrl();
        final String contactScimboID = message.getContactScimboId();
        final String contactDetails = message.getDetailedContacts();


        if (contactNumber != null) {
            holder5.senter_contact_name.setText(contactName);
        } else {
            holder5.senter_contact_name.setText("");
        }

        if (message.isSelected())
            holder5.contact_send_layout.setBackgroundColor(selectedItemColor);
        else
            holder5.contact_send_layout.setBackgroundColor(unSelectedItemColor);


        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder5.senter_contact_caption_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder5.senter_contact_caption_star_icon.setVisibility(View.GONE);
        }


        if (contactNumber != null) {
            if (contactNumber.equals("")) {
                holder5.senter_contact_message_text.setVisibility(View.GONE);
                holder5.senter_side_invite_addcontact_layout.setVisibility(View.GONE);
            } else {
                if (contactScimboID != null) {

                    if (contactScimboID.equals("")) {
                        holder5.senter_side_invite_addcontact_layout.setVisibility(View.GONE);
                        holder5.senter_contact_message_text.setVisibility(View.VISIBLE);
                        holder5.senter_contact_message_text.setText(context.getResources().getString(R.string.invite_contacts));
                    } else {
                        getcontactname.configProfilepic(holder5.senter_contact_image, contactScimboID, false, true, R.drawable.contact_place_holder);
                        holder5.senter_side_invite_addcontact_layout.setVisibility(View.GONE);
                        holder5.senter_contact_message_text.setVisibility(View.VISIBLE);
                        holder5.senter_contact_message_text.setText(context.getResources().getString(R.string.message));

                        if (contactScimboID.equalsIgnoreCase(mCurrentUserId)) {
                            holder5.senter_side_invite_addcontact_layout.setVisibility(View.GONE);
                            holder5.senter_contact_message_text.setVisibility(View.GONE);
                        }
                    }

                } else {
                    holder5.senter_side_invite_addcontact_layout.setVisibility(View.GONE);
                    holder5.senter_contact_message_text.setVisibility(View.VISIBLE);
                    holder5.senter_contact_message_text.setText(context.getResources().getString(R.string.invite_contacts));
                }
            }
        }

        setMessageDeliveryStatus(message, holder5.tvReadReceipt);

        holder5.senter_contact_message_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_COMPLETED) {

                    String type = holder5.senter_contact_message_text.getText().toString();

                    if (type.equalsIgnoreCase(context.getResources().getString(R.string.invite_contacts))) {

                        performUserInvite(contactName, contactDetails);

                    } else if (type.equalsIgnoreCase(context.getResources().getString(R.string.message))) {

                        checKForChatViewNavigation(contactNumber, contactName, contactAvatar, contactScimboID, "send");

                    }

                }
            }
        });

        holder5.senter_contact_invite_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                performUserInvite(contactName, contactDetails);

            }
        });

        holder5.senter_contact_addcontact_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_COMPLETED) {

                    Intent intent = new Intent(context, Savecontact.class);
                    intent.putExtra("name", contactName);
                    intent.putExtra("number", contactNumber);
                    intent.putExtra("contactList", contactDetails);
                    context.startActivity(intent);
                }
            }
        });


        holder5.sender_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
                //   fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);

                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);

            }
        });
    }

    private void bindViewHolder6(MessageAdapter.ViewHolder6 holder6, MessageItemChat message, int position) {
        configureDateLabel(holder6.tvDateLbl, holder6.encryptionlabel, holder6.tvSecretLbl, position);

        String audio_mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = audio_mydate.replace(".", "");
        holder6.sent_audio_date_time_text.setText(mydate);

        if (message.isSelected())
            holder6.audio_send_layout.setBackgroundColor(selectedItemColor);
        else
            holder6.audio_send_layout.setBackgroundColor(unSelectedItemColor);


        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder6.senter_audio_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder6.senter_audio_star_icon.setVisibility(View.GONE);
        }

        if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
            holder6.senter_audio_image_layout.setVisibility(View.GONE);
            holder6.senter_record_image_layout.setVisibility(View.GONE);
            holder6.sent_audio_seek_duration_text.setVisibility(View.VISIBLE);
            if (message.getDuration() != null) {

                holder6.sent_audio_seek_duration_text.setText(message.getDuration());
            }

/*                        String userprofilepic = sessionmanager.getUserProfilePic();
                        AppUtils.loadImage(context, Constants.SOCKET_IP + userprofilepic, holder6.senter_record_image, 60, R.drawable.personprofile);*/

            AppUtils.loadProfilePic(context, holder6.senter_record_image, 60);

        } else {
            holder6.senter_record_image_layout.setVisibility(View.GONE);
            holder6.senter_audio_image_layout.setVisibility(View.VISIBLE);
            holder6.sent_audio_seek_duration_text.setVisibility(View.GONE);
            if (message.getDuration() != null) {
                String duration = message.getDuration();
                holder6.audio_duration_text.setText(duration);
            }
        }


        if (message.getChatFileLocalPath() == null) {
            message.setChatFileLocalPath("");
        }

        if (message.isMediaPlaying()) {
            long value = message.getPlayerCurrentPosition() * 50L;
            if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                holder6.sent_audio_seek_duration_text.setVisibility(View.VISIBLE);
                holder6.sent_audio_seek_duration_text.setText(getTimeString(value));
            } else {
                holder6.sent_audio_seek_duration_text.setVisibility(View.GONE);
                holder6.audio_duration_text.setText(getTimeString(value));
            }
            holder6.sent_audio_play_icon.setImageResource(R.drawable.ic_pause);
        } else {
            holder6.sent_audio_play_icon.setImageResource(R.drawable.ic_play_arrow);
            long value = message.getPlayerCurrentPosition() * 50L;
            if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                holder6.sent_audio_seek_duration_text.setVisibility(View.VISIBLE);
                if (value == 0) {
                    holder6.sent_audio_seek_duration_text.setText(message.getDuration());
                } else {
                    holder6.sent_audio_seek_duration_text.setText(getTimeString(value));
                }
            } else {
                holder6.sent_audio_seek_duration_text.setVisibility(View.GONE);
                holder6.audio_duration_text.setText(message.getDuration());
            }
        }


        holder6.senter_audio_seekbar.setProgress(message.getPlayerCurrentPosition());

        holder6.play_pause_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (lastPlayedAt > -1) {
                    mListData.get(lastPlayedAt).setIsMediaPlaying(false);
                    mTimer.cancel();
                    mPlayer.release();
                }

                if (lastPlayedAt != position) {
                    playAudio(position, message, holder6.senter_audio_seekbar);
                    audioPlayedItemPosition = position;
                } else {
                    lastPlayedAt = -1;
                }

                notifyDataSetChanged();
            }
        });

        holder6.senter_audio_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                if (mListData.get(position).isMediaPlaying()) {
                    mTimer.cancel();
                    mPlayer.release();
                    playAudio(position, message, holder6.senter_audio_seekbar);
                    audioPlayedItemPosition = position;
                }
                try {
                    notifyDataSetChanged();
                } catch (Exception e) {
                    MyLog.e(TAG, "Error", e);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                try {
                    if (progress == 0 || progress == 1){
                        holder6.sent_audio_seek_duration_text.setText(message.getDuration());
                    }
                    if (position != RecyclerView.NO_POSITION && fromUser) {
                        mListData.get(position).setPlayerCurrentPosition(progress);
                    }
                } catch (Exception e) {

                }
            }
        });

        //-----------------------------------------Upload Status Maintain----------------------------------
        if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING &&
                message.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_NOT_SENT)) {
            try {
                holder6.play_pause_container.setVisibility(View.GONE);
                holder6.audio_duration_text.setVisibility(View.GONE);
                holder6.sender_forward_image.setVisibility(View.GONE);

                holder6.senter_audio_pause_resume.setVisibility(View.VISIBLE);

                boolean isFIlePaused = fileUploadDownloadManager.isFilePaused(message.getMessageId());

                MessageDbController dbController = CoreController.getDBInstance(context);

                String Upload_status = dbController.fileuploadStatusget(message.getMessageId());

                if (Upload_status.equalsIgnoreCase("pause")) {

                    holder6.sender_audio_upload_progress_Layout.setVisibility(View.GONE);
                    holder6.senter_audio_Retry_layout.setVisibility(View.VISIBLE);
                    holder6.tvReadReceipt.setVisibility(View.GONE);
                } else if (Upload_status.equalsIgnoreCase("completed")) {

                    holder6.senter_audio_pause_resume.setVisibility(View.GONE);
                    holder6.play_pause_container.setVisibility(View.VISIBLE);
                    holder6.audio_duration_text.setVisibility(View.VISIBLE);
                    holder6.sender_forward_image.setVisibility(View.GONE);

                } else {

                    holder6.senter_audio_Retry_layout.setVisibility(View.GONE);
                    holder6.tvReadReceipt.setVisibility(View.VISIBLE);
                    holder6.sender_audio_upload_progress_Layout.setVisibility(View.VISIBLE);

                    if (message.getUploadDownloadProgress() == 0) {

                        holder6.sender_audio_before_upload_progress.setVisibility(View.VISIBLE);
                        holder6.senter_audio_uploadprogress_bar.setVisibility(View.GONE);

                    } else if (message.getUploadDownloadProgress() > 0) {

                        holder6.sender_audio_before_upload_progress.setVisibility(View.GONE);
                        holder6.senter_audio_uploadprogress_bar.setVisibility(View.VISIBLE);
                        holder6.senter_audio_uploadprogress_bar.setProgress(message.getUploadDownloadProgress());

                    }

                }


                if (message.getUploadDownloadProgress() >= 100) {

                    holder6.sender_audio_before_upload_progress.setVisibility(View.VISIBLE);
                    holder6.senter_audio_uploadprogress_bar.setVisibility(View.GONE);

                }

            } catch (Exception e) {
                MyLog.e(TAG, "Error", e);
            }
        } else {

            holder6.senter_audio_pause_resume.setVisibility(View.GONE);
            holder6.play_pause_container.setVisibility(View.VISIBLE);
            holder6.audio_duration_text.setVisibility(View.VISIBLE);
            holder6.sender_forward_image.setVisibility(View.GONE);
        }


        setMessageDeliveryStatus(message, holder6.tvReadReceipt);

        holder6.senter_audio_pause_resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MessageDbController dbController = CoreController.getDBInstance(context);

                String Upload_status = dbController.fileuploadStatusget(message.getMessageId());

                if (Upload_status.equalsIgnoreCase("uploading")) {

                    holder6.senter_audio_Retry_layout.setVisibility(View.VISIBLE);
                    holder6.tvReadReceipt.setVisibility(View.GONE);
                    holder6.sender_audio_upload_progress_Layout.setVisibility(View.GONE);

                    JSONObject object = dbController.fileuploadobjectget(message.getMessageId());
                    dbController.fileuploadupdateMessage(message.getMessageId(), "pause", object);


                } else if (Upload_status.equalsIgnoreCase("pause")) {

                    holder6.sender_audio_upload_progress_Layout.setVisibility(View.VISIBLE);
                    holder6.senter_audio_Retry_layout.setVisibility(View.GONE);
                    holder6.tvReadReceipt.setVisibility(View.VISIBLE);
                    JSONObject object = dbController.fileuploadobjectget(message.getMessageId());
                    dbController.fileuploadupdateMessage(message.getMessageId(), "uploading", object);

                    fileUploadDownloadManager.startFileUpload(EventBus.getDefault(), object);

                } else if (Upload_status.equalsIgnoreCase("completed")) {

                }
            }
        });

        holder6.sender_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
                //       fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);

                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);
            }
        });
    }

    private void bindViewHolder7(MessageAdapter.ViewHolder7 holder7, MessageItemChat message, int position) {
        configureDateLabel(holder7.tvDateLbl, holder7.encryptionlabel, holder7.tvSecretLbl, position);

        String mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");

        holder7.sent_location_date_time.setText(mydate);

        if (message.isSelected())
            holder7.location_send_layout.setBackgroundColor(selectedItemColor);
        else
            holder7.location_send_layout.setBackgroundColor(unSelectedItemColor);

        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder7.senter_location_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder7.senter_location_star_icon.setVisibility(View.GONE);
        }

        String thumbData ="";
        if (message.getWebLinkImgThumb() != null) {
            thumbData = message.getWebLinkImgThumb();
            if (thumbData != null) {
                if (thumbData.startsWith("data:image/jpeg;base64,")) {
                    thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                }
/*                            decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder7.senter_location_map_image.setImageBitmap(decodedByte);*/
                AppUtils.loadBase64Image(300, context, thumbData, holder7.senter_location_map_image);
            } else {
                holder7.senter_location_map_image.setImageBitmap(null);
            }
        } else {
            holder7.senter_location_map_image.setImageBitmap(null);
        }

        thumbData = message.getThumbnailData();
        AppUtils.loadBase64Image(300, context, thumbData, holder7.senter_location_map_image);

        holder7.senter_location_address_title.setText(message.getWebLinkTitle());
        holder7.senter_location_address_text.setText(message.getWebLinkDesc());

        setMessageDeliveryStatus(message, holder7.tvReadReceipt);

        holder7.senter_location_map_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {
                        itemClickListener.itemClick(position);
                    } else {
                        Uri gmmIntentUri = Uri.parse(message.getWebLink());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        ChatPageActivity.Companion.setNoNeedRefresh(true);
                        context.startActivity(mapIntent);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "onClick: ", e);

                }
            }
        });
        holder7.senter_location_map_image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });

        holder7.sender_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
                //  fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);

                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);
            }
        });
        holder7.sender_forward_image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });
    }

    private void bindViewHolder8(MessageAdapter.ViewHolder8 holder8, MessageItemChat message, int position) {
        configureDateLabel(holder8.tvDateLbl, holder8.encryptionlabel, holder8.tvSecretLbl, position);

        mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");
        holder8.senter_side_date_time_text.setText(mydate);

        if (message.isSelected())
            holder8.document_send_layout.setBackgroundColor(selectedItemColor);
        else
            holder8.document_send_layout.setBackgroundColor(unSelectedItemColor);


        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder8.senter_document_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder8.senter_document_star_icon.setVisibility(View.GONE);
        }

        try {
            String file_name = message.getTextMessage();
            if (file_name.contains(".pdf") || file_name.contains(".PDF")) {
                holder8.senter_document_image.setImageResource(R.drawable.ic_media_pdf);
            } else {

                holder8.senter_document_image.setImageResource(R.drawable.icon_file_doc);
            }

            holder8.senter_document_file_name.setText(Html.fromHtml("<u>" + message.getTextMessage() + "</u>"));

        } catch (Exception e) {

        }

        try {
            File file = new File(message.getChatFileLocalPath());
            int file_size = 0;
            if (!file.exists()) {
                file_size = Integer.parseInt(String.valueOf(message.getFileSize()));
            } else {
                file_size = Integer.parseInt(String.valueOf(file.length()));
            }
            String fileFormattedSize = FileUploadDownloadManager.formatSize(file_size);
            if (fileFormattedSize != null && !fileFormattedSize.equals("0"))
                holder8.senter_document_size_text.setText(fileFormattedSize);
        } catch (Exception e) {
            holder8.senter_document_size_text.setText("");
            MyLog.e(TAG, "configureViewHolderDocumentReceived: ", e);
        }


        if ((message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING &&
                message.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_NOT_SENT))
                || message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_DOWNLOADING) {

            boolean isFIlePaused = fileUploadDownloadManager.isFilePaused(message.getMessageId());

            holder8.senter_document_pause_resume.setVisibility(View.VISIBLE);

            MessageDbController dbController = CoreController.getDBInstance(context);

            String Upload_status = dbController.fileuploadStatusget(message.getMessageId());

            if (Upload_status.equalsIgnoreCase("pause")) {
                holder8.sender_document_upload_progress_Layout.setVisibility(View.GONE);
                holder8.senter_document_Retry_layout.setVisibility(View.VISIBLE);
            } else {

                holder8.senter_document_Retry_layout.setVisibility(View.GONE);
                holder8.sender_document_upload_progress_Layout.setVisibility(View.VISIBLE);

                if (message.getUploadDownloadProgress() == 0) {

                    holder8.senter_document_uploadprogress_bar.setVisibility(View.GONE);
                    holder8.sender_document_before_upload_progress.setVisibility(View.VISIBLE);

                } else if (message.getUploadDownloadProgress() > 0) {

                    holder8.sender_document_before_upload_progress.setVisibility(View.GONE);
                    holder8.senter_document_uploadprogress_bar.setVisibility(View.VISIBLE);

                    holder8.senter_document_uploadprogress_bar.setProgress(message.getUploadDownloadProgress());

                }

            }


            if (message.getUploadDownloadProgress() >= 100) {

                holder8.senter_document_uploadprogress_bar.setVisibility(View.GONE);
                holder8.sender_document_before_upload_progress.setVisibility(View.VISIBLE);
            }


        } else {

            holder8.senter_document_pause_resume.setVisibility(View.GONE);


        }

        setMessageDeliveryStatus(message, holder8.tvReadReceipt);


        holder8.senter_document_pause_resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MessageDbController dbController = CoreController.getDBInstance(context);

                String Upload_status = dbController.fileuploadStatusget(message.getMessageId());

                if (Upload_status.equalsIgnoreCase("uploading")) {

                    holder8.sender_document_upload_progress_Layout.setVisibility(View.GONE);
                    holder8.senter_document_Retry_layout.setVisibility(View.VISIBLE);

                    JSONObject object = dbController.fileuploadobjectget(message.getMessageId());
                    dbController.fileuploadupdateMessage(message.getMessageId(), "pause", object);


                } else if (Upload_status.equalsIgnoreCase("pause")) {

                    holder8.senter_document_Retry_layout.setVisibility(View.GONE);
                    holder8.sender_document_upload_progress_Layout.setVisibility(View.VISIBLE);

                    JSONObject object = dbController.fileuploadobjectget(message.getMessageId());
                    dbController.fileuploadupdateMessage(message.getMessageId(), "uploading", object);

                    fileUploadDownloadManager.startFileUpload(EventBus.getDefault(), object);

                }


            }
        });


        holder8.document_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_COMPLETED) {
                    DocOpenUtils.openDocument(message, context);

                }
            }
        });


        holder8.sender_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
//                            fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                CommonData.setForwardedItems(selectedChatItems);

                fwdBundle.putBoolean("FromScimbo", true);

                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);

            }
        });
    }

    private void bindViewHolder9(MessageAdapter.ViewHolder9 holder9, MessageItemChat message, int position) {
        configureDateLabel(holder9.tvDateLbl, holder9.encryptionlabel, holder9.tvSecretLbl, position);
        mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");
        holder9.senter_weblinkside_date_time_text.setText(mydate);


        holder9.senter_side_weblink.setText(message.getTextMessage());


        if (message.isSelected())
            holder9.weblink_send_layout.setBackgroundColor(selectedItemColor);
        else
            holder9.weblink_send_layout.setBackgroundColor(unSelectedItemColor);

        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder9.senter_weblink_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder9.senter_weblink_star_icon.setVisibility(View.GONE);
        }

        holder9.senter_weblink_http.setText(message.getWebLink());
//              holder9.senter_weblink_description.setText(message.getWebLinkDesc());
        holder9.senter_weblink_file_name.setText(message.getWebLinkTitle());

        if (message.getWebLink() != null) {
            String linkImgUrl;
            if (message.getWebLink().endsWith("/")) {
                linkImgUrl = message.getWebLink() + "favicon.ico";
            } else {
                linkImgUrl = message.getWebLink() + "/favicon.ico";
            }

            if (!message.getWebLink().startsWith("http")) {
                linkImgUrl = Constants.SOCKET_IP + linkImgUrl;
            }

            AppUtils.loadImage(context, linkImgUrl, holder9.senter_weblink_image, 60, R.drawable.ic_business_link);
        }

        setMessageDeliveryStatus(message, holder9.tvReadReceipt);
    }

    private void bindViewHolder10(MessageAdapter.ViewHolder10 holder10, MessageItemChat message, int position) {
        String mydate1 = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate1.replace(".", "");
        holder10.delete_senter_side_date_time_text.setText(mydate);

        if (position == 0){
            holder10.encryptionlabel.setVisibility(View.VISIBLE);
        }else{
            holder10.encryptionlabel.setVisibility(View.GONE);
        }

        if (message.isSelected())
            holder10.delete_send_layout.setBackgroundColor(selectedItemColor);
        else
            holder10.delete_send_layout.setBackgroundColor(unSelectedItemColor);
    }

    private void bindViewHolder11(MessageAdapter.ViewHolder11 holder11, MessageItemChat message, int position) {
        configureDateLabel(holder11.tvDateLbl, holder11.encryptionlabel, holder11.tvSecretLbl, position);

        String Caption_text = message.getTextMessage();
        //System.out.println("ImageCaption" + " " + Caption_text);
        if (Caption_text == null) {
            Caption_text = "";
        }
        String mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");
        //System.out.println("ImageCaption" + " " + Caption_text);
        final String image = message.getChatFileLocalPath();

        int imageHeight = 0;
        imageHeight = AppUtils.parseInt(message.getChatFileHeight());

        if (message.isSelected())
            holder11.image_received_layout.setBackgroundColor(selectedItemColor);
        else
            holder11.image_received_layout.setBackgroundColor(unSelectedItemColor);


        if (isGroupChat) {
            holder11.receiver_image_group_sender_name.setVisibility(View.VISIBLE);
            String msisdn = message.getSenderName();
            holder11.receiver_image_group_sender_name.setText(getcontactname.getGroupMemberName(message.getGroupMsgFrom(), msisdn));
        } else {
            holder11.receiver_image_group_sender_name.setVisibility(View.GONE);
        }

        File publicDirFIle = null;

        if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {

            holder11.receiver_image_pause_resume_layout.setVisibility(View.GONE);
            holder11.image_download_layout.setVisibility(View.GONE);
            holder11.receiver_image_dowload_progress_Layout.setVisibility(View.GONE);
            holder11.receiver_image_inside_date_layout.setVisibility(View.VISIBLE);

            File file = new File(image);
            try {
                String publicDirPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).getAbsolutePath();

                String[] splited = image.split("/");

                publicDirFIle = new File(publicDirPath + "/" + splited[splited.length - 1]);
            } catch (Exception e) {

            }

            if (file.exists()) {

                holder11.receiver_side_imageview.setVisibility(View.GONE);
                holder11.receiver_top_crop_imageview.setVisibility(View.VISIBLE);
//Issue loading previous image

                //         AppUtils.loadLocalImage(context, image, holder11.receiver_top_crop_imageview);
                holder11.receiver_forward_image.setVisibility(View.GONE);

                Bitmap bitmap = ScimboImageUtils.decodeBitmapFromFile(image, 500, 500);
                if (bitmap != null) {
                    holder11.receiver_top_crop_imageview.setImageBitmap(bitmap);
                } else {
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap bitmapp = BitmapFactory.decodeFile(image, bmOptions);
                    if (bitmapp == null) {
                        Glide.with(context)
                                .load(image)
                                .asBitmap()
                                .error(R.drawable.ic_placeholder_black)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .dontAnimate()
                                .fitCenter()
                                .into(new SimpleTarget<Bitmap>() {

                                    @Override
                                    public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                        // TODO Auto-generated method stub
                                        holder11.receiver_top_crop_imageview.setImageBitmap(arg0);
                                    }

                                    @Override
                                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                        super.onLoadFailed(e, errorDrawable);
                                    }
                                });
                    } else {
                        holder11.receiver_top_crop_imageview.setImageBitmap(bitmapp);
                    }
                }
            } else if (publicDirFIle != null && publicDirFIle.exists()) {
                // vh2.forward_image.setVisibility(View.VISIBLE);
                if (imageHeight > 500) {

                    holder11.receiver_side_imageview.setVisibility(View.GONE);
                    holder11.receiver_top_crop_imageview.setVisibility(View.VISIBLE);

                    AppUtils.loadLocalImage(context, publicDirFIle.getAbsolutePath(), holder11.receiver_top_crop_imageview);
                    holder11.receiver_forward_image.setVisibility(View.GONE);

                } else {

                    holder11.receiver_top_crop_imageview.setVisibility(View.GONE);
                    holder11.receiver_side_imageview.setVisibility(View.VISIBLE);

                    AppUtils.loadLocalImage(context, publicDirFIle.getAbsolutePath(), holder11.receiver_side_imageview);
                    holder11.receiver_forward_image.setVisibility(View.GONE);

                }

            } else {
                holder11.receiver_forward_image.setVisibility(View.GONE);

                //vh2.forward_image.setVisibility(View.GONE);
                String thumbnailData = message.getThumbnailData();
                try {
                    String imageDataBytes = thumbnailData;
                    if (thumbnailData.contains(","))
                        imageDataBytes = thumbnailData.substring(thumbnailData.indexOf(",") + 1);

/*                                byte[] decodedString = Base64.decode(imageDataBytes, Base64.DEFAULT);
                                Bitmap thumbBmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                holder11.receiver_top_crop_imageview.setImageBitmap(thumbBmp);*/
                    AppUtils.loadBase64Image(200, context, imageDataBytes, holder11.receiver_top_crop_imageview);

                } catch (Exception ex) {
                    MyLog.e(TAG, "", ex);
                }
                holder11.receiver_top_crop_imageview.setOnClickListener(null);
            }


            if (Caption_text != null) {

                if (Caption_text.length() > 0) {
                    holder11.receive_image_caption_layout.setVisibility(View.VISIBLE);
                    holder11.receiver_image_inside_date_layout.setVisibility(View.GONE);
                    holder11.receive_image_caption_text.setText(Caption_text);
                    holder11.receive_image_caption_date_time.setText(mydate);

                    if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                        holder11.receiver_image_caption_star_icon.setVisibility(View.VISIBLE);
                    } else {
                        holder11.receiver_image_caption_star_icon.setVisibility(View.GONE);
                    }

                } else {
                    holder11.receive_image_caption_layout.setVisibility(View.GONE);
                    holder11.receiver_image_inside_date_layout.setVisibility(View.VISIBLE);
                    holder11.received_image_date_time.setText(mydate);

                    if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {

                        holder11.receiver_image_normal_star_icon.setVisibility(View.VISIBLE);
                    } else {

                        holder11.receiver_image_normal_star_icon.setVisibility(View.GONE);
                    }

                }
            } else {
                holder11.received_image_date_time.setText(mydate);
            }


        } else {
            //thumbnail issue
            holder11.receiver_forward_image.setVisibility(View.GONE);
            holder11.receiver_top_crop_imageview.setVisibility(View.VISIBLE);

            String thumbnailData = message.getThumbnailData();
            try {
                String imageDataBytes = thumbnailData;
                if (thumbnailData.contains(","))
                    imageDataBytes = thumbnailData.substring(thumbnailData.indexOf(",") + 1);
                            /*byte[] decodedString = Base64.decode(imageDataBytes, Base64.DEFAULT);
                            Bitmap thumbBmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            holder11.receiver_top_crop_imageview.setImageBitmap(thumbBmp);*/
                AppUtils.loadBase64Image(200, context, imageDataBytes, holder11.receiver_top_crop_imageview);
            } catch (Exception ex) {
                MyLog.e(TAG, "", ex);
            }

            if (message.getUploadDownloadProgress() == 0) {

                holder11.receiver_image_pause_resume_layout.setVisibility(View.VISIBLE);
                holder11.image_download_layout.setVisibility(View.VISIBLE);
                holder11.receiver_forward_image.setVisibility(View.GONE);
                holder11.receiver_image_dowload_progress_Layout.setVisibility(View.GONE);
                holder11.receiver_image_caption_star_icon.setVisibility(View.GONE);
                holder11.receiver_image_normal_star_icon.setVisibility(View.GONE);
                long filesize = AppUtils.parseLongForFile(message.getFileSize());
                holder11.image_download_text.setText(size(filesize));

                if (Caption_text != null) {
                    if (Caption_text.length() > 0) {
                        holder11.receive_image_caption_layout.setVisibility(View.VISIBLE);
                        holder11.receiver_image_inside_date_layout.setVisibility(View.GONE);
                        holder11.receive_image_caption_text.setText(Caption_text);
                        holder11.receive_image_caption_date_time.setText(mydate);
                    } else {
                        holder11.receive_image_caption_layout.setVisibility(View.GONE);
                        holder11.receiver_image_inside_date_layout.setVisibility(View.VISIBLE);
                        holder11.received_image_date_time.setText(mydate);
                    }
                } else {
                    holder11.received_image_date_time.setText(mydate);
                }

            } else {
                holder11.receiver_image_pause_resume_layout.setVisibility(View.VISIBLE);
                holder11.image_download_layout.setVisibility(View.GONE);
                holder11.receiver_image_before_download_progress.setVisibility(View.GONE);
                holder11.receiver_image_dowload_progress_Layout.setVisibility(View.VISIBLE);
                holder11.progress_bar.setVisibility(View.VISIBLE);
                holder11.progress_bar.setProgress(message.getUploadDownloadProgress());


            }

        }

        holder11.receiver_image_pause_resume_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {

                if (message.getDownloadStatus() != MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                    //already paused --> resume the upload
                    if (convertView.getTag() != null && convertView.getTag().toString().equals("downloading")) {
                        convertView.setTag(null);

                        holder11.receiver_image_dowload_progress_Layout.setVisibility(View.GONE);
                        holder11.image_download_layout.setVisibility(View.VISIBLE);

                        fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), true);

                        MyLog.d("DownloadFileStatus", "DownloadFilePaused");


                    } else {
                        convertView.setTag("downloading");

                        holder11.image_download_layout.setVisibility(View.GONE);
                        holder11.progress_bar.setVisibility(View.GONE);
                        holder11.receiver_image_dowload_progress_Layout.setVisibility(View.VISIBLE);
                        holder11.receiver_image_dowload_progress_Layout.setVisibility(View.VISIBLE);
                        holder11.receiver_image_before_download_progress.setVisibility(View.VISIBLE);

                        String Msg_id = message.getMessageId();

                        JSONObject pauseFileObject = fileUploadDownloadManager.getDownloadResumePauseFileObject(message.getMessageId());
                        if (pauseFileObject != null && pauseFileObject.length() > 0) {

                            fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), false);

                            fileUploadDownloadManager.VideoDownloadProgress(EventBus.getDefault(), pauseFileObject);

                            MyLog.d("DownloadFileStatus", "PauseFileDownloading");

                            MyLog.d("CheckEventConnection", "Download Pause File Sending Object" + "" + pauseFileObject);

                        } else {
                            fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), false);
                            ReceiverImageDownload(position);
                        }


                    }
                }

            }
        });
        holder11.receiver_image_pause_resume_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });

        final File finalPublicFIle = publicDirFIle;

        holder11.receiver_top_crop_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                    if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {
                        itemClickListener.itemClick(position);
                    } else {
                        Intent intent = new Intent(context, ImageZoom.class);
                        if (finalPublicFIle != null && finalPublicFIle.exists()) {
                            intent.putExtra("image", finalPublicFIle.getAbsoluteFile());
                        } else
                            intent.putExtra("image", image);
                        ChatPageActivity.Companion.setNoNeedRefresh(true);
                        context.startActivity(intent);
                    }
                }
            }
        });
        holder11.receiver_top_crop_imageview.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });

        holder11.receiver_side_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                    if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {
                        itemClickListener.itemClick(position);
                    } else {
                        Intent intent = new Intent(context, ImageZoom.class);
                        if (finalPublicFIle != null && finalPublicFIle.exists()) {
                            intent.putExtra("image", finalPublicFIle.getPath());
                        } else
                            intent.putExtra("image", image);
                        ChatPageActivity.Companion.setNoNeedRefresh(true);
                        context.startActivity(intent);
                    }
                }
            }
        });
        holder11.receiver_side_imageview.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });

        holder11.receiver_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
                //    fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);

                Intent intent = new Intent(context, ForwardContact.class);

                intent.putExtras(fwdBundle);
                context.startActivity(intent);
            }
        });
        holder11.receiver_forward_image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });
    }

    private void bindViewHolder12(MessageAdapter.ViewHolder12 holder12, MessageItemChat message, int position) {
        String surationasec = "";

        String Caption_text = message.getCaption();
        String audio_Caption_text = message.getTextMessage();

        if (audio_Caption_text == null) {

            Caption_text = "";
        }

        String audio_mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = audio_mydate.replace(".", "");

        if (message.isSelected())
            holder12.video_received_layout.setBackgroundColor(selectedItemColor);
        else
            holder12.video_received_layout.setBackgroundColor(unSelectedItemColor);

        if (isGroupChat) {
            holder12.receiver_video_group_sender_name.setVisibility(View.VISIBLE);
            String msisdn = message.getSenderName();
            holder12.receiver_video_group_sender_name.setText(getcontactname.getGroupMemberName(message.getGroupMsgFrom(), msisdn));
        } else {
            holder12.receiver_video_group_sender_name.setVisibility(View.GONE);
        }

        MediaMetadataRetriever mdr = new MediaMetadataRetriever();

        if (message.getChatFileLocalPath() != null) {
            try {
                String path = AppUtils.getValidLocalVideoPath(message.getChatFileLocalPath());
                mdr.setDataSource(path);
                String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                surationasec = getTimeString(AppUtils.parseLong(duration));

            } catch (Exception e) {
                MyLog.e("Exception", "Exception" + e.getMessage());
            }
        }

        if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {

            holder12.receiver_video_pause_resume.setVisibility(View.GONE);
            holder12.video_download_layout.setVisibility(View.GONE);
            holder12.receiver_video_dowload_progress_Layout.setVisibility(View.GONE);
            holder12.receiver_video_play_icon.setVisibility(View.VISIBLE);
            holder12.receiver_video_duration_layout.setVisibility(View.VISIBLE);
            holder12.receiver_forward_image.setVisibility(View.GONE);
//Some times it is null length
            if (audio_Caption_text != null) {
                if (audio_Caption_text.length() > 0) {

                    holder12.receiver_video_inside_date_layout.setVisibility(View.GONE);
                    holder12.receiver_video_caption_layout.setVisibility(View.VISIBLE);
                    holder12.receiver_video_caption_text.setText(audio_Caption_text);
                    holder12.receiver_video_caption_date_time.setText(mydate);
                    holder12.receiver_caption_video_duration_text.setText(surationasec);


                    if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                        holder12.receiver_video_caption_star_icon.setVisibility(View.VISIBLE);
                    } else {
                        holder12.receiver_video_caption_star_icon.setVisibility(View.GONE);
                    }


                } else {

                    holder12.receiver_video_inside_date_layout.setVisibility(View.VISIBLE);
                    holder12.receiver_video_caption_layout.setVisibility(View.GONE);
                    holder12.receiver_video_date_time.setText(mydate);
                    holder12.receiver_video_duration_text.setText(surationasec);
                    holder12.receiver_video_caption_date_time.setText(mydate);

                    if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                        holder12.receiver_video_normal_star_icon.setVisibility(View.VISIBLE);
                    } else {
                        holder12.receiver_video_normal_star_icon.setVisibility(View.GONE);
                    }

                }
            } else {


                holder12.receiver_video_inside_date_layout.setVisibility(View.VISIBLE);
                holder12.receiver_video_caption_layout.setVisibility(View.GONE);
                holder12.receiver_video_date_time.setText(mydate);
                holder12.receiver_video_duration_text.setText(surationasec);
                holder12.receiver_video_caption_date_time.setText(mydate);

                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                    holder12.receiver_video_normal_star_icon.setVisibility(View.VISIBLE);
                } else {
                    holder12.receiver_video_normal_star_icon.setVisibility(View.GONE);
                }


            }

            //   new LoadVideoThumbnailTask(message.getVideoPath(), holder12.receiver_side_videoimage).execute();

            String thumbData = message.getThumbnailData();
            Log.d(TAG, "getView: videothumb1 start");
            if (thumbData.startsWith("data:image/jpeg;base64,")) {
                thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                try {
/*                                byte[] decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                                holder12.receiver_side_videoimage.setImageBitmap(decodedByte);*/
                    AppUtils.loadBase64Image(200, context, thumbData, holder12.receiver_side_videoimage);
                    Log.d(TAG, "getView: videothumb1 end");
                } catch (Exception ex) {
                    MyLog.e(TAG, "", ex);
                }
            } else {

                Log.d(TAG, "getView: videothumb2 start");
                builtUri = Uri.parse(AppUtils.getValidGroupPath(message.getChatFileServerPath()))
                        .buildUpon()
                        .appendQueryParameter("atoken", SessionManager.getInstance(context).getSecurityToken())
                        .appendQueryParameter("au", SessionManager.getInstance(context).getCurrentUserID())
                        .appendQueryParameter("at", "site")
                        .build();
                thumbData = builtUri.toString();


                try {
                    if (message.getChatFileLocalPath() != null)
                        AppUtils.loadLocalVideoThumbanail(context, message.getChatFileLocalPath(), holder12.receiver_side_videoimage);
                    //holder12.receiver_side_videoimage.setImageBitmap(retriveVideoFrameFromVideo(builtUri.toString()));
                    Log.d(TAG, "getView: videothumb2 end");
                } catch (Throwable throwable) {
                    MyLog.e(TAG, "getView: ", throwable);
                }
            }


        } else {

                       /* if (!mCurrentUserId.equals(message.getReceiverID())) {

                            holder12.receiver_video_pause_resume.setVisibility(View.GONE);
                            holder12.video_download_layout.setVisibility(View.GONE);
                            holder12.receiver_video_dowload_progress_Layout.setVisibility(View.GONE);
                            holder12.receiver_video_play_icon.setVisibility(View.VISIBLE);
                            holder12.receiver_video_duration_layout.setVisibility(View.VISIBLE);
                            holder12.receiver_forward_image.setVisibility(View.VISIBLE);
                            if (audio_Caption_text.length() > 0) {

                                holder12.receiver_video_inside_date_layout.setVisibility(View.GONE);
                                holder12.receiver_video_caption_layout.setVisibility(View.VISIBLE);
                                holder12.receiver_video_caption_text.setText(audio_Caption_text);
                                holder12.receiver_video_caption_date_time.setText(mydate);
                                holder12.receiver_caption_video_duration_text.setText(surationasec);


                                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                                    holder12.receiver_video_caption_star_icon.setVisibility(View.VISIBLE);
                                } else {
                                    holder12.receiver_video_caption_star_icon.setVisibility(View.GONE);
                                }


                            } else {

                                holder12.receiver_video_inside_date_layout.setVisibility(View.VISIBLE);
                                holder12.receiver_video_caption_layout.setVisibility(View.GONE);
                                holder12.receiver_video_date_time.setText(mydate);
                                holder12.receiver_video_duration_text.setText(surationasec);

                                if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
                                    holder12.receiver_video_normal_star_icon.setVisibility(View.VISIBLE);
                                } else {
                                    holder12.receiver_video_normal_star_icon.setVisibility(View.GONE);
                                }

                            }

                            String thumbData = message.getThumbnailData();


                            thumbData=builtUri.toString();
                            *//*if (thumbData.startsWith("data:image/jpeg;base64,")) {
                                thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                            }*//*
                            try {
                            //    byte[] decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                             //   Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                                try {
                                    holder12.receiver_side_videoimage.setImageBitmap(retriveVideoFrameFromVideo(builtUri.toString()));
                                } catch (Throwable throwable) {
                                    throwable.printStackTrace();
                                }
                            } catch (Exception ex) {
                                MyLog.e(TAG, "", ex);
                            }
                        }else {
*/
            holder12.receiver_forward_image.setVisibility(View.GONE);

            int progress = message.getUploadDownloadProgress();

            if (message.getUploadDownloadProgress() == 0) {

                holder12.receiver_video_pause_resume.setVisibility(View.VISIBLE);
                holder12.receiver_video_duration_layout.setVisibility(View.GONE);

                holder12.receiver_video_dowload_progress_Layout.setVisibility(View.GONE);
                holder12.video_download_layout.setVisibility(View.VISIBLE);
                long filesize = AppUtils.parseLongForFile(message.getFileSize());
                holder12.video_download_text.setText(size(filesize));

                String thumbData = message.getThumbnailData();
                Log.d(TAG, "getView: videothumb3 start");
                if (thumbData != null && thumbData.startsWith("data:image/jpeg;base64,")) {
                    thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                }
                if (thumbData != null && thumbData.length() > 0) {
                    try {
                                    /*final String finalThumbData = thumbData;
                                    holder12.receiver_side_videoimage.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            byte[] decodedString = Base64.decode(finalThumbData, Base64.DEFAULT);
                                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                            holder12.receiver_side_videoimage.setImageBitmap(decodedByte);
                                        }
                                    });*/

                        AppUtils.loadBase64Image(200, context, thumbData, holder12.receiver_side_videoimage);

                        Log.d(TAG, "getView: videothumb3 end");
                    } catch (Exception ex) {
                        MyLog.e(TAG, "", ex);
                    }
                } else {
                               /* Log.d(TAG, "getView: videothumb4 start");
                                builtUri = Uri.parse(AppUtils.getValidGroupPath(message.getChatFileServerPath()))
                                        .buildUpon()
                                        .appendQueryParameter("atoken", SessionManager.getInstance(context).getSecurityToken())
                                        .appendQueryParameter("au", SessionManager.getInstance(context).getCurrentUserID())
                                        .appendQueryParameter("at", "site")
                                        .build();
                                thumbData=builtUri.toString();


                                try {

                                   // holder12.receiver_side_videoimage.setImageBitmap(retriveVideoFrameFromVideo(builtUri.toString()));
                                   AppUtils.loadLiveVideoThumbanail(context,message.getChatFileServerPath(),holder12.receiver_side_videoimage);
                                    Log.d(TAG, "getView: videothumb4 end");
                                } catch (Throwable throwable) {
                                    throwable.printStackTrace();
                                } */
                    holder12.receiver_side_videoimage.setImageResource(R.drawable.play_button);
                }


                if (audio_Caption_text != null && audio_Caption_text.length() > 0) {

                    holder12.receiver_video_inside_date_layout.setVisibility(View.VISIBLE);
                    holder12.receiver_video_caption_layout.setVisibility(View.VISIBLE);
                    holder12.receiver_video_caption_text.setText(audio_Caption_text);
                    holder12.receiver_video_caption_date_time.setText(mydate);
                    holder12.receiver_caption_video_duration_text.setText(surationasec);

                    holder12.receiver_video_duration_text.setText(surationasec);

                } else {

                    holder12.receiver_video_inside_date_layout.setVisibility(View.VISIBLE);
                    holder12.receiver_video_caption_layout.setVisibility(View.GONE);
                    holder12.receiver_video_date_time.setText(mydate);
                    holder12.receiver_video_duration_text.setText(surationasec);
                }

            } else {

                Log.d(TAG, "getView: videothumb5 start");
                String thumbData = message.getThumbnailData();
                if (thumbData.startsWith("data:image/jpeg;base64,")) {
                    thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                }
                try {
/*                                byte[] decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                holder12.receiver_side_videoimage.setImageBitmap(decodedByte);*/
                    AppUtils.loadBase64Image(200, context, thumbData, holder12.receiver_side_videoimage);
                    Log.d(TAG, "getView: videothumb5 end");
                } catch (Exception ex) {
                    MyLog.e(TAG, "", ex);
                }

                holder12.receiver_video_pause_resume.setVisibility(View.VISIBLE);
                holder12.receiver_video_duration_layout.setVisibility(View.GONE);
                holder12.video_download_layout.setVisibility(View.GONE);
                holder12.receiver_video_before_download_progress.setVisibility(View.GONE);
                holder12.video_progress_bar.setVisibility(View.VISIBLE);
                holder12.receiver_video_dowload_progress_Layout.setVisibility(View.VISIBLE);

                holder12.video_progress_bar.setProgress(message.getUploadDownloadProgress());

                if (message.getUploadDownloadProgress() == 100) {
                    holder12.video_progress_bar.setVisibility(View.GONE);
                    holder12.receiver_video_before_download_progress.setVisibility(View.VISIBLE);
                }
            }
            //  }
        }

        holder12.receiver_video_pause_resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                if (message.getDownloadStatus() != MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                    //already paused --> resume the upload
                    if (convertView.getTag() != null && convertView.getTag().toString().equals("downloading")) {
                        convertView.setTag(null);

                        holder12.receiver_video_dowload_progress_Layout.setVisibility(View.GONE);
                        holder12.video_download_layout.setVisibility(View.VISIBLE);

                        fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), true);

                        MyLog.d("DownloadFileStatus", "DownloadFilePaused");


                    }
                    //first time --> pause the upload
                    else {
                        convertView.setTag("downloading");

                        holder12.video_download_layout.setVisibility(View.GONE);
                        holder12.receiver_video_dowload_progress_Layout.setVisibility(View.VISIBLE);
                        holder12.video_progress_bar.setVisibility(View.GONE);
                        holder12.receiver_video_before_download_progress.setVisibility(View.VISIBLE);

                        String Msg_id = message.getMessageId();

                        JSONObject pauseFileObject = fileUploadDownloadManager.getDownloadResumePauseFileObject(message.getMessageId());
                        if (pauseFileObject != null && pauseFileObject.length() > 0) {

                            fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), false);

                            fileUploadDownloadManager.VideoDownloadProgress(EventBus.getDefault(), pauseFileObject);

                            MyLog.d("DownloadFileStatus", "PauseFileDownloading");

                            MyLog.d("CheckEventConnection", "Download Pause File Sending Object" + "" + pauseFileObject);

                        } else {

                            fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), false);

                            ReceiverVideoDownload(position);


                        }

                    }
                }
            }
        });
        holder12.receiver_video_pause_resume.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });


        holder12.receiver_side_videoimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {

                    if (mPlayer != null) {

                        if (lastPlayedAt > -1) {
                            mListData.get(lastPlayedAt).setIsMediaPlaying(false);
                            mTimer.cancel();
                            mPlayer.release();
                        }
                        notifyDataSetChanged();
                    }

                    try {
                        if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {
                            itemClickListener.itemClick(position);
                        } else {
                            String videoPath = "";
                            videoPath = message.getChatFileLocalPath();
                            //  if(CoreController.isRaad){
                            Intent intent = new Intent(context, FullScreenVideoActivity.class);
                            intent.putExtra("filePath", AppUtils.getValidLocalVideoPath(videoPath));
                            ChatPageActivity.Companion.setNoNeedRefresh(true);
                            context.startActivity(intent);
                                       /* }
                                        else {
                                            try {
                                                File videoFile = new File(videoPath);
                                                if (!videoFile.exists()) {
                                                    String[] filePathSplited = message.getChatFileLocalPath().split(File.separator);
                                                    String fileName = filePathSplited[filePathSplited.length - 1];

                                                    String publicDirPath = Environment.getExternalStoragePublicDirectory(
                                                            Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
                                                    String filePath = publicDirPath + File.separator + fileName;
                                                    videoFile = new File(filePath);
                                                    videoPath = videoFile.getPath();
                                                }
                                            } catch (Exception e) {
                                                MyLog.e(TAG, "onClick: ", e);
                                            }

                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
                                            String path = "file://" + message.getChatFileLocalPath();
                                            intent.setDataAndType(Uri.parse(path), "video");
                                            context.startActivity(intent);
                                        }*/
                        }
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(context, context.getResources().getString(R.string.no_app_to_play_video), Toast.LENGTH_LONG).show();
                    }
                } else if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_NOT_START) {

                    if (mPlayer != null) {

                        if (lastPlayedAt > -1) {
                            mListData.get(lastPlayedAt).setIsMediaPlaying(false);
                            mTimer.cancel();
                            mPlayer.release();
                        }
                        notifyDataSetChanged();
                    }

                    try {
                        if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {
                            itemClickListener.itemClick(position);
                        } else {
                            String videopath = AppUtils.getValidProfilePath(message.getChatFileServerPath());
                            builtUri = Uri.parse(videopath)
                                    .buildUpon()
                                    .appendQueryParameter("atoken", SessionManager.getInstance(context).getSecurityToken())
                                    .appendQueryParameter("au", SessionManager.getInstance(context).getCurrentUserID())
                                    .appendQueryParameter("at", "site")
                                    .build();

                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(builtUri.toString()));
                            intent.setDataAndType(Uri.parse(builtUri.toString()), "video/*");
                            context.startActivity(intent);
                        }
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(context, context.getResources().getString(R.string.no_app_to_play_video), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
        holder12.receiver_side_videoimage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });

        holder12.receiver_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
                //  fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);

                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);
            }
        });
        holder12.receiver_forward_image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });
    }

    private void bindViewHolder13(MessageAdapter.ViewHolder13 holder13, MessageItemChat message, int position) {
        configureDateLabel(holder13.tvDateLbl, holder13.encryptionlabel, holder13.tvSecretLbl, position);

        mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");
        holder13.receiver_audio_date_time_text.setText(mydate);

        if (message.isSelected())
            holder13.Audio_received_layout.setBackgroundColor(selectedItemColor);
        else
            holder13.Audio_received_layout.setBackgroundColor(unSelectedItemColor);

        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder13.receiver_audio_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder13.receiver_audio_star_icon.setVisibility(View.GONE);
        }

        if (isGroupChat) {
            holder13.receiver_audio_group_sender_name.setVisibility(View.VISIBLE);
            String msisdn = message.getSenderName();
            holder13.receiver_audio_group_sender_name.setText(getcontactname.getGroupMemberName(message.getGroupMsgFrom(), msisdn));
        } else {
            holder13.receiver_audio_group_sender_name.setVisibility(View.GONE);
        }

        if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
            holder13.receiver_audio_image_layout.setVisibility(View.GONE);
            holder13.receiver_record_image_layout.setVisibility(View.GONE);
            holder13.receiver_audio_seek_duration_text.setVisibility(View.VISIBLE);
            if (message.getDuration() != null) {

                holder13.receiver_audio_seek_duration_text.setText(message.getDuration());
            }

/*                        String userprofilepic = sessionmanager.getUserProfilePic();
                        AppUtils.loadImage(context, Constants.SOCKET_IP + userprofilepic, holder13.receiver_record_image, 60, R.drawable.personprofile);*/
            AppUtils.loadProfilePic(context, holder13.receiver_record_image, 60);

        } else {
            holder13.receiver_audio_seek_duration_text.setVisibility(View.VISIBLE);
            holder13.receiver_record_image_layout.setVisibility(View.GONE);
            holder13.receiver_audio_image_layout.setVisibility(View.GONE);
            if (message.getDuration() != null) {
                String duration = message.getDuration();
                holder13.receiver_audio_seek_duration_text.setText(duration);
            }
        }

        if (message.getChatFileLocalPath() == null) {
            message.setChatFileLocalPath("");
        }

        if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {

            holder13.receiver_audio_dowload_progress_Layout.setVisibility(View.GONE);
            holder13.receiver_audio_image_download_layout.setVisibility(View.GONE);
            holder13.receiver_audio_download_icon.setVisibility(View.GONE);
            holder13.play_pause_container.setVisibility(View.VISIBLE);
            holder13.receiver_forward_image.setVisibility(View.GONE);
            holder13.receiver_audio_pause_resume.setEnabled(false);
            if (message.isMediaPlaying()) {
                long value = message.getPlayerCurrentPosition() * 50L;
                if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                    holder13.receiver_audio_seek_duration_text.setVisibility(View.VISIBLE);
                    holder13.receiver_audio_seek_duration_text.setText(getTimeString(value));
                } else {
                    holder13.receiver_audio_seek_duration_text.setVisibility(View.VISIBLE);
                    holder13.receiver_audio_seek_duration_text.setText(getTimeString(value));
                }
                holder13.receiver_audio_play_icon.setImageResource(R.drawable.ic_pause);
            } else {
                long value = message.getPlayerCurrentPosition() * 50L;
                holder13.receiver_audio_play_icon.setImageResource(R.drawable.ic_play_arrow);
                if (message.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                    holder13.receiver_audio_seek_duration_text.setVisibility(View.VISIBLE);
                    if (value == 0) {
                        holder13.receiver_audio_seek_duration_text.setText(message.getDuration());
                    } else {
                        holder13.receiver_audio_seek_duration_text.setText(getTimeString(value));
                    }
                } else {
                    holder13.receiver_audio_seek_duration_text.setVisibility(View.VISIBLE);
                    holder13.receiver_audio_duration_text.setText(message.getDuration());
                }
            }

            holder13.receiver_audio_seekbar.setProgress(message.getPlayerCurrentPosition());

            holder13.play_pause_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (lastPlayedAt > -1) {
                        mListData.get(lastPlayedAt).setIsMediaPlaying(false);
                        mTimer.cancel();
                        mPlayer.release();
                    }

                    if (lastPlayedAt != position) {
                        playAudio(position, message, holder13.receiver_audio_seekbar);
                        audioPlayedItemPosition = position;
                    } else {
                        lastPlayedAt = -1;
                    }

                    notifyDataSetChanged();
                }
            });

            holder13.receiver_audio_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                    if (mListData.get(position).isMediaPlaying()) {
                        mTimer.cancel();
                        mPlayer.release();
                        playAudio(position, message, holder13.receiver_audio_seekbar);
                        audioPlayedItemPosition = position;
                    }
                    try {
                        notifyDataSetChanged();
                    } catch (Exception e) {
                        MyLog.e(TAG, "Error", e);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    // TODO Auto-generated method stub
                    try {
                        if (progress == 0 || progress == 1){
                            holder13.receiver_audio_seek_duration_text.setText(message.getDuration());
                        }
                        if (position != RecyclerView.NO_POSITION && fromUser) {
                            mListData.get(position).setPlayerCurrentPosition(progress);
                        }
                    } catch (Exception e) {

                    }
                }
            });
        } else {


            holder13.receiver_forward_image.setVisibility(View.GONE);
            holder13.play_pause_container.setVisibility(View.GONE);
            holder13.receiver_audio_pause_resume.setEnabled(true);

            if (message.getUploadDownloadProgress() == 0) {

                holder13.receiver_audio_dowload_progress_Layout.setVisibility(View.GONE);
                holder13.receiver_audio_before_download_progress.setVisibility(View.GONE);
                holder13.audio_progress_bar.setVisibility(View.GONE);
                holder13.receiver_audio_download_icon.setVisibility(View.VISIBLE);
                holder13.receiver_audio_download_icon.setBackgroundResource(R.drawable.inline_audio_download_normal);

            } else {

                holder13.receiver_audio_image_download_layout.setVisibility(View.GONE);
                holder13.receiver_audio_download_icon.setVisibility(View.GONE);
                holder13.receiver_audio_dowload_progress_Layout.setVisibility(View.VISIBLE);
                holder13.receiver_audio_before_download_progress.setVisibility(View.GONE);
                holder13.audio_progress_bar.setVisibility(View.VISIBLE);

                holder13.audio_progress_bar.setProgress(message.getUploadDownloadProgress());

                if (message.getUploadDownloadProgress() == 100) {

                    holder13.audio_progress_bar.setVisibility(View.GONE);
                    holder13.receiver_audio_before_download_progress.setVisibility(View.VISIBLE);
                }

            }


            holder13.receiver_audio_pause_resume.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View convertView) {

                    if (message.getDownloadStatus() != MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                        //already paused --> resume the upload
                        if (convertView.getTag() != null && convertView.getTag().toString().equals("downloading")) {
                            convertView.setTag(null);

                            holder13.receiver_audio_dowload_progress_Layout.setVisibility(View.GONE);
                            holder13.receiver_audio_download_icon.setVisibility(View.VISIBLE);

                            fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), true);

                            MyLog.d("DownloadFileStatus", "DownloadFilePaused");


                        }
                        //first time --> pause the upload
                        else {
                            convertView.setTag("downloading");

                            holder13.receiver_audio_download_icon.setVisibility(View.GONE);
                            holder13.audio_progress_bar.setVisibility(View.GONE);
                            holder13.receiver_audio_dowload_progress_Layout.setVisibility(View.VISIBLE);
                            holder13.receiver_audio_before_download_progress.setVisibility(View.VISIBLE);

                            String Msg_id = message.getMessageId();

                            JSONObject pauseFileObject = fileUploadDownloadManager.getDownloadResumePauseFileObject(message.getMessageId());
                            if (pauseFileObject != null && pauseFileObject.length() > 0) {

                                fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), false);

                                fileUploadDownloadManager.VideoDownloadProgress(EventBus.getDefault(), pauseFileObject);

                                MyLog.d("DownloadFileStatus", "PauseFileDownloading");

                                MyLog.d("CheckEventConnection", "Download Pause File Sending Object" + "" + pauseFileObject);

                            } else {

                                fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), false);

                                ReceiverAudioDownload(position);


                            }


                        }
                    }


                }
            });


        }

        holder13.receiver_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
                //   fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);

                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);

            }
        });
    }

    private void bindViewHolder14(MessageAdapter.ViewHolder14 holder14, MessageItemChat message, int position) {
        configureDateLabel(holder14.tvDateLbl, holder14.encryptionlabel, holder14.tvSecretLbl, position);

        mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");
        holder14.receiver_side_date_time_text.setText(mydate);

        if (message.isSelected())
            holder14.document_received_layout.setBackgroundColor(selectedItemColor);
        else
            holder14.document_received_layout.setBackgroundColor(unSelectedItemColor);


        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder14.receiver_document_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder14.receiver_document_star_icon.setVisibility(View.GONE);
        }


        if (isGroupChat) {
            holder14.receiver_document_group_sender_name.setVisibility(View.VISIBLE);
            String msisdn = message.getSenderName();
            holder14.receiver_document_group_sender_name.setText(getcontactname.getGroupMemberName(message.getGroupMsgFrom(), msisdn));
        } else {
            holder14.receiver_document_group_sender_name.setVisibility(View.GONE);
        }


        try {
            String file_name = message.getTextMessage();
            MyLog.e("file_name", "file_name" + file_name);
            if (file_name.contains(".pdf") || file_name.contains(".PDF")) {
                holder14.receiver_document_image.setImageResource(R.drawable.ic_media_pdf);
            } else {
                try {
                    String serverFileName = message.getChatFileServerPath();
                    String localPath = message.getChatFileLocalPath();

                    if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                        String[] splittedPath = localPath.split("/");
                        file_name = splittedPath[splittedPath.length - 1];
                    } else {
                        String[] splittedPath = serverFileName.split("/");
                        file_name = splittedPath[splittedPath.length - 1];
                    }
                } catch (Exception e) {
                    Log.e(TAG, "getView: ", e);
                }
                if (file_name.contains(".pdf") || file_name.contains(".PDF")) {
                    holder14.receiver_document_image.setImageResource(R.drawable.ic_media_pdf);
                } else
                    holder14.receiver_document_image.setImageResource(R.drawable.icon_file_doc);
            }
            holder14.receiver_document_file_name.setText(Html.fromHtml("<u>" + file_name + "</u>"));
        } catch (Exception e) {

        }

        try {
            //File file = new File(message.getChatFileLocalPath());
            int file_size = Integer.parseInt(message.getFileSize());
            String fileFormattedSize = FileUploadDownloadManager.formatSize(file_size);
            if (file_size > 0)
                holder14.receiver_document_size_text.setText(fileFormattedSize);
        } catch (Exception e) {
            holder14.receiver_document_size_text.setText("");
            MyLog.e(TAG, "configureViewHolderDocumentReceived: ", e);
        }


        if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
            holder14.receiver_document_pause_resume_layout.setVisibility(View.GONE);
            holder14.receiver_document_dowload_Layout.setVisibility(View.GONE);
            holder14.receiver_document_dowload_progress_Layout.setVisibility(View.GONE);

        } else {
            holder14.receiver_document_pause_resume_layout.setVisibility(View.VISIBLE);
            if (message.getUploadDownloadProgress() == 0) {
                holder14.receiver_document_dowload_progress_Layout.setVisibility(View.GONE);
                holder14.receiver_document_dowload_Layout.setVisibility(View.VISIBLE);

            } else {

                holder14.receiver_document_dowload_Layout.setVisibility(View.GONE);
                holder14.receiver_document_before_download_progress.setVisibility(View.GONE);
                holder14.receiver_document_dowload_progress_Layout.setVisibility(View.VISIBLE);
                holder14.document_progress_bar.setVisibility(View.VISIBLE);

                holder14.document_progress_bar.setProgress(message.getUploadDownloadProgress());

                if (message.getUploadDownloadProgress() == 100) {

                    holder14.receiver_document_dowload_progress_Layout.setVisibility(View.VISIBLE);
                    holder14.document_progress_bar.setVisibility(View.GONE);
                    holder14.receiver_document_before_download_progress.setVisibility(View.VISIBLE);

                }
            }


            holder14.receiver_document_pause_resume_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View convertView) {

                    if (message.getDownloadStatus() != MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                        //already paused --> resume the upload
                        if (convertView.getTag() != null && convertView.getTag().toString().equals("downloading")) {
                            convertView.setTag(null);

                            holder14.receiver_document_dowload_progress_Layout.setVisibility(View.GONE);
                            holder14.receiver_document_dowload_Layout.setVisibility(View.VISIBLE);

                            fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), true);

                            //  MyLog.d("DownloadFileStatus", "DownloadFilePaused");
                        } else {
                            // if (canDisplayPdf(context)){
                            convertView.setTag("downloading");

                            holder14.receiver_document_dowload_Layout.setVisibility(View.GONE);
                            holder14.document_progress_bar.setVisibility(View.GONE);
                            holder14.receiver_document_dowload_progress_Layout.setVisibility(View.VISIBLE);
                            holder14.receiver_document_before_download_progress.setVisibility(View.VISIBLE);

                            String Msg_id = message.getMessageId();

                            JSONObject pauseFileObject = fileUploadDownloadManager.getDownloadResumePauseFileObject(message.getMessageId());
                            if (pauseFileObject != null && pauseFileObject.length() > 0) {
                                fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), false);

                                fileUploadDownloadManager.VideoDownloadProgress(EventBus.getDefault(), pauseFileObject);
                                MyLog.d("DownloadFileStatus", "PauseFileDownloading");
                                MyLog.d("CheckEventConnection", "Download Pause File Sending Object" + "" + pauseFileObject);
                            } else {
                                fileUploadDownloadManager.setDownloadResumePauseFileStatus(message.getMessageId(), false);
                                ReceiverDocumentDownload(position);
                            }
                        }
                    }

                }
            });


        }


        holder14.document_main_receive_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (message.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                    try {
                        Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW);
                        File file = null;
                        try {
                            file = new File(message.getChatFileLocalPath());
                            if (!file.exists()) {
                                try {
                                    String[] filePathSplited = message.getChatFileLocalPath().split(File.separator);
                                    String fileName = filePathSplited[filePathSplited.length - 1];

                                    String publicDirPath = Environment.getExternalStoragePublicDirectory(
                                            Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
                                    String filePath = publicDirPath + File.separator + fileName;
                                    file = new File(filePath);
                                } catch (Exception e) {
                                    MyLog.e(TAG, "configureViewHolderImageReceived: ", e);
                                }
                            }

                        } catch (Exception e) {
                            MyLog.e(TAG, "onClick: ", e);
                        }

                        myIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                        String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(message.getChatFileLocalPath());
                        String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                        if (file != null) {
                            //    Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID, file);
                            Uri uri = Uri.fromFile(file);
                            myIntent.setDataAndType(uri, mimetype);
                            if (file.exists()) {
                                context.startActivity(myIntent);
                            }
                        }
                    } catch (Exception e) {
                        MyLog.e(TAG, "onClick: ", e);
                    }
                }


            }
        });


        holder14.receiver_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);
                Bundle fwdBundle = new Bundle();
                //   fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);
                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);

            }
        });
    }

    private void bindViewHolder15(MessageAdapter.ViewHolder15 holder15, MessageItemChat message, int position) {
        configureDateLabel(holder15.tvDateLbl, holder15.encryptionlabel, holder15.tvSecretLbl, position);


        mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");
        holder15.receiver_weblinkside_date_time_text.setText(mydate);

        if (message.isSelected())
            holder15.weblink_received_layout.setBackgroundColor(selectedItemColor);
        else
            holder15.weblink_received_layout.setBackgroundColor(unSelectedItemColor);


        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder15.receiver_weblink_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder15.receiver_weblink_star_icon.setVisibility(View.GONE);
        }

        if (isGroupChat) {
            holder15.receiver_weblink_group_sender_name.setVisibility(View.VISIBLE);
            String msisdn = message.getSenderName();
            holder15.receiver_weblink_group_sender_name.setText(getcontactname.getGroupMemberName(message.getGroupMsgFrom(), msisdn));
        } else {
            holder15.receiver_weblink_group_sender_name.setVisibility(View.GONE);
        }

        holder15.receiver_side_weblink.setText(message.getTextMessage());

        holder15.receiver_weblink_http.setText(message.getWebLink());
        //      holder15.receiver_weblink_description.setText(message.getWebLinkDesc());
        holder15.receive_weblink_file_name.setText(message.getWebLinkTitle());

        if (message.getWebLink() != null) {
            String linkImgUrl;
            if (message.getWebLink().endsWith("/")) {
                linkImgUrl = message.getWebLink() + "favicon.ico";
            } else {
                linkImgUrl = message.getWebLink() + "/favicon.ico";
            }

            if (!message.getWebLink().startsWith("http")) {
                linkImgUrl = Constants.SOCKET_IP + linkImgUrl;
            }

            AppUtils.loadImage(context, linkImgUrl, holder15.receive_weblink_image, 60, R.drawable.link_icon);
        }
    }

    private void bindViewHolder16(MessageAdapter.ViewHolder16 holder16, MessageItemChat message, int position) {
        mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");
        holder16.delete_receiver_side_date_time_text.setText(mydate);

        if (position == 0){
            holder16.encryptionlabel.setVisibility(View.VISIBLE);
        }else{
            holder16.encryptionlabel.setVisibility(View.GONE);
        }

        if (message.isSelected())
            holder16.delete_received_layout.setBackgroundColor(selectedItemColor);
        else
            holder16.delete_received_layout.setBackgroundColor(unSelectedItemColor);

    }

    private void bindViewHolder17(MessageAdapter.ViewHolder17 holder17, MessageItemChat message, int position) {
        configureDateLabel(holder17.tvDateLbl, holder17.encryptionlabel, holder17.tvSecretLbl, position);

        String time = TimeStampUtils.get12HrTimeFormat(context, message.getTS());

        if (message.getCallType() != null && message.getCallType().equals(MessageFactory.video_call + "")) {
            holder17.tvCallLbl.setText(context.getResources().getString(R.string.missed_video_call_at) + time);
        } else if (message.getCallType() != null && message.getCallType().equals(MessageFactory.audio_call + "")) {
            holder17.tvCallLbl.setText(context.getResources().getString(R.string.missed_voice_call_at) + time);

        } else {
            holder17.tvCallLbl.setVisibility(View.GONE);
        }

    }

    private void bindViewHolder18(MessageAdapter.ViewHolder18 holder18, MessageItemChat message, int position) {
        configureDateLabel(holder18.tvDateLbl, holder18.encryptionlabel, holder18.tvSecretLbl, position);

        //  String time = TimeStampUtils.get12HrTimeFormat(context, message.getTS());

        String msg = null;
        String createdBy = message.getCreatedByUserId();
        String createdTo = message.getCreatedToUserId();
        msg = GroupMessageUtil.getGroupEventMsg(message, context, mCurrentUserId, getContactNameIfExists(createdBy), getContactNameIfExists(createdTo));

        if (msg != null) {
            holder18.tvgroup_info.setText(msg);
        }
    }

    private void bindViewHolder19(MessageAdapter.ViewHolder19 holder19, MessageItemChat message, int position) {
        configureDateLabel(holder19.tvDateLbl, holder19.encryptionlabel, holder19.tvSecretLbl, position);

        mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");
        holder19.receiver_contact_date_text.setText(mydate);

        final String contactName = message.getContactName();
        final String contactNumber = message.getContactNumber();
        final String contactAvatar = message.getAvatarImageUrl();
        final String contactScimboID = message.getContactScimboId();
        final String contactDetails = message.getDetailedContacts();

        if (contactNumber != null) {
            holder19.receiver_contact_name.setText(contactName);
        } else {
            holder19.receiver_contact_name.setText("");
        }

        if (message.isSelected())
            holder19.contact_received_layout.setBackgroundColor(selectedItemColor);
        else
            holder19.contact_received_layout.setBackgroundColor(unSelectedItemColor);

        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder19.receiver_contact_caption_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder19.receiver_contact_caption_star_icon.setVisibility(View.GONE);
        }

        if (isGroupChat) {
            holder19.receiver_contact_group_sender_name.setVisibility(View.VISIBLE);
            String msisdn = message.getSenderName();
            holder19.receiver_contact_group_sender_name.setText(getcontactname.getGroupMemberName(message.getGroupMsgFrom(), msisdn));
        } else {
            holder19.receiver_contact_group_sender_name.setVisibility(View.GONE);
        }

        if (contactScimboID != null) {

            if (contactScimboID.equals("")) {
                holder19.receiver_side_invite_addcontact_layout.setVisibility(View.GONE);
                holder19.receiver_contact_message_text.setVisibility(View.VISIBLE);
                holder19.receiver_contact_message_text.setText(context.getResources().getString(R.string.invite));
            } else {
                getcontactname.configProfilepic(holder19.receiver_contact_image, contactScimboID, false, true, R.drawable.contact_place_holder);
                holder19.receiver_side_invite_addcontact_layout.setVisibility(View.GONE);
                holder19.receiver_contact_message_text.setVisibility(View.VISIBLE);
                holder19.receiver_contact_message_text.setText(context.getResources().getString(R.string.message));

                if (contactScimboID.equalsIgnoreCase(mCurrentUserId)) {
                    holder19.receiver_side_invite_addcontact_layout.setVisibility(View.GONE);
                    holder19.receiver_contact_message_text.setVisibility(View.GONE);
                }
            }

        } else {
            holder19.receiver_side_invite_addcontact_layout.setVisibility(View.GONE);
            holder19.receiver_contact_message_text.setVisibility(View.VISIBLE);
            holder19.receiver_contact_message_text.setText(context.getResources().getString(R.string.invite));
        }

        holder19.receiver_contact_message_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String type = holder19.receiver_contact_message_text.getText().toString();

                if (type.equalsIgnoreCase(context.getResources().getString(R.string.invite))) {

                    performUserInvite(contactName, contactDetails);

                } else if (type.equalsIgnoreCase(context.getResources().getString(R.string.message))) {

                    checKForChatViewNavigation(contactNumber, contactName, contactAvatar, contactScimboID, "send");

                }
            }
        });

        holder19.receiver_contact_invite_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performUserInvite(contactName, contactDetails);
            }
        });

        holder19.receiver_contact_addcontact_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Intent intent = new Intent(context, Savecontact.class);
                    intent.putExtra("name", contactName);
                    intent.putExtra("number", contactNumber);
                    intent.putExtra("contactList", contactDetails);
                    context.startActivity(intent);
                } catch (Exception e) {

                }
            }
        });

        holder19.receiver_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
                //    fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);

                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);

            }
        });
    }

    private void bindViewHolder20(MessageAdapter.ViewHolder20 holder20, MessageItemChat message, int position) {
        configureDateLabel(holder20.tvDateLbl, holder20.encryptionlabel, holder20.tvSecretLbl, position);


        mydate = TimeStampUtils.get12HrTimeFormat(context, message.getTS());
        mydate = mydate.replace(".", "");

        holder20.receive_location_date_time.setText(mydate);

        if (message.isSelected())
            holder20.location_received_layout.setBackgroundColor(selectedItemColor);
        else
            holder20.location_received_layout.setBackgroundColor(unSelectedItemColor);


        if (message.getStarredStatus().equals(MessageFactory.MESSAGE_STARRED)) {
            holder20.receiver_location_star_icon.setVisibility(View.VISIBLE);
        } else {
            holder20.receiver_location_star_icon.setVisibility(View.GONE);
        }

        if (isGroupChat) {
            holder20.receiver_location_group_sender_name.setVisibility(View.VISIBLE);
            String msisdn = message.getSenderName();
            holder20.receiver_location_group_sender_name.setText(getcontactname.getGroupMemberName(message.getGroupMsgFrom(), msisdn));
        } else {
            holder20.receiver_location_group_sender_name.setVisibility(View.GONE);
        }

        if (message.getWebLinkImgThumb() != null) {
            String thumbData = message.getWebLinkImgThumb();
            if (thumbData != null) {
                if (thumbData.startsWith("data:image/jpeg;base64,")) {
                    thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                }
                try {
/*                                byte[] decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                holder20.receive_location_map_image.setImageBitmap(decodedByte);*/
                    AppUtils.loadBase64Image(300, context, thumbData, holder20.receive_location_map_image);
                } catch (Exception ex) {
                    MyLog.e(TAG, "", ex);
                }
            } else {
                holder20.receive_location_map_image.setImageBitmap(null);
            }
        } else {
            holder20.receive_location_map_image.setImageBitmap(null);
        }

        ImageLoader imageLoader = CoreController.getInstance().getImageLoader();
        imageLoader.get(message.getWebLinkImgUrl(), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() != null) {
                    holder20.receive_location_map_image.setImageBitmap(response.getBitmap());
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                if (message.getWebLinkImgThumb() != null) {
                    String thumbData = message.getWebLinkImgThumb();
                    if (thumbData != null) {
                        if (thumbData.startsWith("data:image/jpeg;base64,")) {
                            thumbData = thumbData.replace("data:image/jpeg;base64,", "");
                        }
                        try {
                                     /*   byte[] decodedString = Base64.decode(thumbData, Base64.DEFAULT);
                                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                        holder20.receive_location_map_image.setImageBitmap(decodedByte);
                                     */
                            AppUtils.loadBase64Image(300, context, thumbData, holder20.receive_location_map_image);
                        } catch (Exception ex) {
                            MyLog.e(TAG, "", ex);
                        }
                    } else {
                        holder20.receive_location_map_image.setImageBitmap(null);
                    }
                } else {
                    holder20.receive_location_map_image.setImageBitmap(null);
                }
            }
        });

        holder20.receiver_location_title.setText(message.getWebLinkTitle());
        holder20.receive_location_address_text.setText(message.getWebLinkDesc());

        holder20.receive_location_map_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (ChatPageActivity.Companion.isFirstItemSelected() || (message.isSelected())) {
                        itemClickListener.itemClick(position);
                    } else {
                        Uri gmmIntentUri = Uri.parse(message.getWebLink());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        ChatPageActivity.Companion.setNoNeedRefresh(true);
                        context.startActivity(mapIntent);
                    }

                } catch (Exception e) {

                }
            }
        });
        holder20.receive_location_map_image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });

        holder20.receiver_forward_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                selectedChatItems.add(message);

                Bundle fwdBundle = new Bundle();
                //     fwdBundle.putSerializable("MsgItemList", selectedChatItems);
                fwdBundle.putBoolean("FromScimbo", true);
                CommonData.setForwardedItems(selectedChatItems);

                Intent intent = new Intent(context, ForwardContact.class);
                intent.putExtras(fwdBundle);
                context.startActivity(intent);
            }
        });
        holder20.receiver_forward_image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                itemClickListener.itemLongClick(position);
                return false;
            }
        });
    }

    private void bindViewHolder21(MessageAdapter.ViewHolder21 holder21, MessageItemChat message, int position) {

    }

    private void bindViewHolder22(MessageAdapter.ViewHolder22 holder22, MessageItemChat message, int position) {
        configureDateLabel(holder22.tvDateLbl, holder22.encryptionlabel, holder22.tvSecretLbl, position);
    }

    private void bindViewHolder23(MessageAdapter.ViewHolder23 holder23, String message, int position) {
        holder23.tv_screen_shot_taken.setText(message);
    }

//    public View getView(final int position, View convertView, ViewGroup parent) {
//
//        // View convertView = null;
//        //MyLog.d(TAG, "getView: chatslow " + position);
//
//        if (mListData.size() == 0) {
//
//            convertView = LayoutInflater.from(context).inflate(R.layout.nlc_empty_list_item, null, false);
//
//            return convertView;
//        }
//
//
//        switch (item_type) {
//
//
//
//            case TIMER_CHANGE:
////                if (convertView == null) {
////                    convertView = LayoutInflater.from(context).inflate(R.layout.nlc_empty_list_item, null, false);
////                    holder22 = new ViewHolder22();
////
////                    holder22.tvDateLbl = convertView.findViewById(R.id.tvDateLbl);
////                    holder22.tvSecretLbl = convertView.findViewById(R.id.tvSecretLbl);
////                    holder22.encryptionlabel = convertView.findViewById(R.id.encryptionlabel);
////                    convertView.setTag(holder22);
////
////
////                } else {
////                    convertView = convertView;
////                    holder22 = (ViewHolder22) convertView.getTag();
////                }
//
////                configureDateLabel(holder22.tvDateLbl, holder22.encryptionlabel, holder22.tvSecretLbl, position);
//                break;
//
//
//            case SCREEN_SHOT_TAKEN:
////                if (convertView == null) {
////                    convertView = LayoutInflater.from(context).inflate(R.layout.vh_screen_shot_taken, null, false);
////                    holder23 = new ViewHolder23();
////                    holder23.tv_screen_shot_taken = convertView.findViewById(R.id.tv_screen_shot_taken);
////
////                    convertView.setTag(holder23);
////
////
////                } else {
////                    convertView = convertView;
////                    holder23 = (ViewHolder23) convertView.getTag();
////                }
//
////                holder23.tv_screen_shot_taken.setText(message.getTextMessage());
//                break;
//
//
//        }
//
//
////        }
//        else{ //------------Other Message-------------------
//
//            //   String type = mListData.get(position).getMessageType();
//
//            int item_type = getItemViewType(position);
//
//            String type = String.valueOf(item_type);
//
//
//            switch (item_type) {
//
//                case TIMER_CHANGE:
//                    if (convertView == null) {
//                        convertView = LayoutInflater.from(context).inflate(R.layout.nlc_empty_list_item, null, false);
//                        holder22 = new ViewHolder22();
//
//                        holder22.tvDateLbl = convertView.findViewById(R.id.tvDateLbl);
//                        holder22.tvSecretLbl = convertView.findViewById(R.id.tvSecretLbl);
//                        holder22.encryptionlabel = convertView.findViewById(R.id.encryptionlabel);
//                        convertView.setTag(holder22);
//
//
//                    } else {
//                        convertView = convertView;
//                        holder22 = (ViewHolder22) convertView.getTag();
//                    }
//
//                    configureDateLabel(holder22.tvDateLbl, holder22.encryptionlabel, holder22.tvSecretLbl, position);
//                    break;
//
//                case SCREEN_SHOT_TAKEN:
//                    if (convertView == null) {
//                        convertView = LayoutInflater.from(context).inflate(R.layout.vh_screen_shot_taken, null, false);
//                        holder23 = new ViewHolder23();
//                        holder23.tv_screen_shot_taken = convertView.findViewById(R.id.tv_screen_shot_taken);
//
//                        convertView.setTag(holder23);
//
//
//                    } else {
//                        convertView = convertView;
//                        holder23 = (ViewHolder23) convertView.getTag();
//                    }
//
//
//                    //String name = getcontactname.getSendername(message.getReceiverID(), message.getSenderMsisdn());
//                    String name = getcontactname.getSendername(message.getSenderMsisdn());
//                    holder23.tv_screen_shot_taken.setText(name + " " + message.getTextMessage());
//                    break;
//
//            }
//
//        }
//        //MyLog.d(TAG, "getView: end chatslow");
//        return convertView;
//    }

    private boolean isContinuous(MessageItemChat currentMsg, MessageItemChat precedingMsg) {
        // null check
        if (currentMsg == null || precedingMsg == null) {
            return false;
        }

        String currentMessageSenderId = currentMsg.getReceiverID();
        String prcedingMessageSenderId = precedingMsg.getReceiverID();

        return false;//!TextUtils.isEmpty(currentMessageSenderId) && !TextUtils.isEmpty(prcedingMessageSenderId) && currentMessageSenderId.equals(prcedingMessageSenderId);

    }


    private void setMessageDeliveryStatus(MessageItemChat message, TextView tvReadReceipt) {
        String status_tick = message.getDeliveryStatus();
        if (isGroupChat) {
            status_tick = MsgInfoUtils.getMessageStatus(message, mCurrentUserId);
        }
        switch (status_tick) {
            case "3":
                if (!message.isBlocked()) {
                    if (isGroupChat)
                        tvReadReceipt.setText(context.getResources().getString(R.string.viewed_by_all));
                    else
                        tvReadReceipt.setText(context.getResources().getString(R.string.viewed));
                }
                break;
            case "2":
                if (!message.isBlocked()) {
                    if (isGroupChat)
                        tvReadReceipt.setText(context.getResources().getString(R.string.viewed));
                    else
                        tvReadReceipt.setText(context.getResources().getString(R.string.delivered));
                }
                break;
            case "1":
                tvReadReceipt.setText(context.getResources().getString(R.string.delivered));
                break;
            default:
                tvReadReceipt.setText(context.getResources().getString(R.string.sending));
                break;
        }
        if (message.getUploadStatus() == MessageFactory.UPLOAD_STATUS_UPLOADING &&
                message.getDeliveryStatus().equals(MessageFactory.DELIVERY_STATUS_NOT_SENT))
            tvReadReceipt.setText(context.getResources().getString(R.string.sending));
    }


    private void configureDateLabel(TextView tvDateLbl, TextView encryptionlabel, TextView tvSecretTimerLbl, int position) {
        if (position > mListData.size())
            return;

        tvDateLbl.setVisibility(View.GONE);

        MessageItemChat item = mListData.get(position);
        String currentItemTS = item.getTS();
        Date currentItemDate = TimeStampUtils.getMessageTStoDate(context, currentItemTS);

        if (position == 0 && currentItemDate != null) {
            // Log.d(TAG, "configureDateLabel: date visible");
            tvDateLbl.setVisibility(View.VISIBLE);

            encryptionlabel.setVisibility(View.VISIBLE);
            setDateText(tvDateLbl, currentItemDate);
        } else {
            encryptionlabel.setVisibility(View.GONE);

            MessageItemChat prevItem = mListData.get(position - 1);
            String prevItemTS = prevItem.getTS();
            Date prevItemDate = TimeStampUtils.getMessageTStoDate(context, prevItemTS);

            if (currentItemDate != null && prevItemDate != null) {
                if (!currentItemDate.equals(prevItemDate)) {
                    tvDateLbl.setVisibility(View.VISIBLE);
                    setDateText(tvDateLbl, currentItemDate);
                }
            } else {
                tvDateLbl.setVisibility(View.GONE);
            }
        }
        if (isSecretChat) {
            encryptionlabel.setVisibility(View.GONE);
            configureSecretTimerLabel(tvSecretTimerLbl, position);
        }
    }

    //-------------------------------------------Date Text Set-------------------------------------------------------
    private void setDateText(TextView tvDateLbl, Date currentItemDate) {
        Date today = TimeStampUtils.getDateFormat(Calendar.getInstance().getTimeInMillis());
        Date yesterday = TimeStampUtils.getYesterdayDate(today);

        if (currentItemDate.equals(today)) {
            tvDateLbl.setText(R.string.today);
        } else if (currentItemDate.equals(yesterday)) {
            tvDateLbl.setText(R.string.yesterday);
        } else {
            DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            String formatDate = df.format(currentItemDate);
            tvDateLbl.setText(formatDate);
        }
    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf.append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

    private void performUserInvite(String contactName, String contactDetails) {

        ArrayList<String> aDetails = null;

        try {
            aDetails = new ArrayList<String>();
            JSONObject data = new JSONObject(contactDetails);
            JSONArray contactmail_List = null;
            JSONArray contactph_List = data.getJSONArray("phone_number");
            if (contactph_List.length() > 0) {
                for (int i = 0; i < contactph_List.length(); i++) {

                    JSONObject phone = contactph_List.getJSONObject(i);
                    String value = phone.getString("value");
                    aDetails.add(value);
                }

                if (data.has("email")) {
                    contactmail_List = data.getJSONArray("email");
                    if (contactmail_List.length() > 0) {
                        for (int i = 0; i < contactmail_List.length(); i++) {

                            JSONObject mail = contactmail_List.getJSONObject(i);
                            String value = mail.getString("value");
                            aDetails.add(value);
                        }
                    }
                }

            }


        } catch (JSONException e) {
            MyLog.e(TAG, "Error", e);
        }
        ShowInviteAlertForBoth(aDetails, contactName);

    }

    private void ShowInviteAlertForBoth(final ArrayList<String> detail, String name) {
        final List<MultiTextDialogPojo> labelsList = new ArrayList<>();
        MultiTextDialogPojo pojo = new MultiTextDialogPojo();
        pojo.setLabelText(context.getResources().getString(R.string.invite_contacts) + " " + name + " " + "via");
        labelsList.add(pojo);
        for (int i = 0; i < detail.size(); i++) {
            pojo = new MultiTextDialogPojo();
            pojo.setLabelText(detail.get(i));
            labelsList.add(pojo);
        }
        CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
        dialog.setLabelsList(labelsList);
        dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
            @Override
            public void onDialogItemClick(int position) {
                String appname = context.getResources().getString(R.string.app_name);
                if (!Getcontactname.isEmailValid(labelsList.get(position).getLabelText())) {
                    SMSintent(labelsList.get(position).getLabelText(), appname);
                } else {
                    performInvite(labelsList.get(position).getLabelText(), appname);
                }
            }


        });
        dialog.show(fragmentManager, "Invite contact");
    }

    private void SMSintent(String labelText, String appname) {
        Intent smsIntent;
        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
                smsIntent = new Intent(Intent.ACTION_SEND);
                String packageName = Telephony.Sms.getDefaultSmsPackage(context);
                smsIntent.setPackage(packageName);
                smsIntent.setType("text/plain");
                smsIntent.putExtra("address", labelText);
                smsIntent.putExtra(Intent.EXTRA_TEXT, Constants.getAppStoreLink(context));
            } else {
                smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", labelText);
                smsIntent.putExtra("sms_body", Constants.getAppStoreLink(context));
            }

            if (smsIntent == null) {
                // Todo: :Roman: Add string to resource
                Toast.makeText(context, context.getResources().getString(R.string.no_app_installed_to_handle_this_event), Toast.LENGTH_SHORT).show();
            } else {
                context.startActivity(smsIntent);
            }
        } catch (ActivityNotFoundException e) {
            // Todo: :Roman: Add string to resource
            Toast.makeText(context, context.getResources().getString(R.string.no_app_installed_to_handle_this_event), Toast.LENGTH_SHORT).show();
        }
    }

    public void performInvite(String labelText, String appname) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_TEXT, Constants.getAppStoreLink(context));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{labelText});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, appname + " : Android");
        context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    private void checKForChatViewNavigation(String msisdn, String contactName, String image, String perID, String fromTAG) {
        ChatLockPojo lockPojo = getChatLockdetailfromDB();

        if (sessionmanager.getLockChatEnabled().equals("1") && lockPojo != null) {
            String stat = lockPojo.getStatus();
            String pwd = lockPojo.getPassword();

            String documentid = mCurrentUserId + "-" + perID;
            if (stat.equals("1")) {
                openUnlockChatDialog(documentid, stat, pwd, contactName, image, msisdn);
            } else {

                if (fromTAG.equalsIgnoreCase("received")) {
                    for (int i = 0; i < ScimboContactsService.contactEntries.size(); i++) {
                        String myStrPhno = msisdn.replace(sessionmanager.getCountryCodeOfCurrentUser(), "");
                        if (ScimboContactsService.contactEntries.get(i).getNumberInDevice().startsWith(sessionmanager.getCountryCodeOfCurrentUser())) {
                            if (ScimboContactsService.contactEntries.get(i).getNumberInDevice().equalsIgnoreCase(msisdn)) {
                                navigateTochatviewpage(ScimboContactsService.contactEntries.get(i).getFirstName(), msisdn, image, perID);
                            }
                        } else if (ScimboContactsService.contactEntries.get(i).getNumberInDevice().equalsIgnoreCase(myStrPhno)) {
                            navigateTochatviewpage(ScimboContactsService.contactEntries.get(i).getFirstName(), msisdn, image, perID);
                        } else if (i == ScimboContactsService.contactEntries.size() - 1) {
                            navigateTochatviewpage(msisdn, msisdn, image, perID);
                        }

                    }
                } else {
                    navigateTochatviewpage(contactName, msisdn, image, perID);
                }
            }
        } else {
            if (fromTAG.equalsIgnoreCase("received")) {

                Getcontactname objContact = new Getcontactname(context);
                if (perID != null && !perID.equals("")) {
                    boolean isAlreadyContact = objContact.isContactExists(perID);
                    if (!isAlreadyContact) {
                        navigateTochatviewpage(msisdn, msisdn, image, perID);
                    } else {
                        navigateTochatviewpage(contactName, msisdn, image, perID);
                    }
                }
            } else {
                navigateTochatviewpage(contactName, msisdn, image, perID);
            }
        }
    }

    //----------------------------------------Receiver Side View Holder Class-------------------------------------

    private void navigateTochatviewpage(String contactName, String contactNumber, String image, String perSonID) {
        Intent intent = new Intent(context, ChatPageActivity.class);
        intent.putExtra("receiverUid", "");
        intent.putExtra("receiverName", "");
        intent.putExtra("documentId", perSonID);
        intent.putExtra("type", 0);
        intent.putExtra("backfrom", true);
        intent.putExtra("Username", contactName);
        intent.putExtra("msisdn", contactNumber);
        intent.putExtra("Image", image);
        context.startActivity(intent);
    }

    private ChatLockPojo getChatLockdetailfromDB() {
        String scimbocontactid = "";
        String id = mCurrentUserId.concat("-").concat(scimbocontactid);
        MessageDbController dbController = CoreController.getDBInstance(context);
        String convId = userInfoSession.getChatConvId(id);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        return dbController.getChatLockData(receiverId, MessageFactory.CHAT_TYPE_SINGLE);
    }

    public void openUnlockChatDialog(String documentid, String stat, String pwd, String contactname, String image, String msisdn) {

        String convId = userInfoSession.getChatConvId(documentid);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        // Todo: :Roman: Add string to resource
        dialog.setTextLabel1("Enter your Password");
        dialog.setEditTextdata("Enter new Password");
        dialog.setforgotpwdlabel("Forgot Password");
        dialog.setHeader("Unlock Chat");
        dialog.setButtonText("Unlock");
        Bundle bundle = new Bundle();
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("contactName", contactname);
        bundle.putString("avatar", image);
        bundle.putString("msisdn", msisdn);
        String uid = documentid.split("-")[1];
        bundle.putString("docid", uid);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "single");
        bundle.putString("from", mCurrentUserId);
        dialog.setArguments(bundle);
        dialog.show(fragmentManager, "chatunLock");
    }

    private void playAudio(final int position, MessageItemChat message, final SeekBar sbDuration) {
        boolean isAudioRecording = ((ChatPageActivity) context).isAudioRecording();
        if (isAudioRecording) {
            Toast.makeText(context, context.getString(R.string.cannot_play_audio_while_recording), Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(message.getChatFileLocalPath());
        String audioPath = message.getChatFileLocalPath();
        if (!file.exists()) {
            try {
                String[] filePathSplited = message.getChatFileLocalPath().split(File.separator);
                String fileName = filePathSplited[filePathSplited.length - 1];

                String publicDirPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MUSIC).getAbsolutePath();
                audioPath = publicDirPath + File.separator + fileName;
            } catch (Exception e) {
                MyLog.e(TAG, "configureViewHolderImageReceived: ", e);
            }
        }

        Uri uri = Uri.parse(audioPath);
        mPlayer = MediaPlayer.create(context, uri);
        mTimer = new Timer();
        //   mPlayer=new MediaPlayer();
        try {
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mPlayer.start();
                    if (mListData.get(position).getPlayerCurrentPosition() < mPlayer.getDuration()) {
                        mPlayer.seekTo(mListData.get(position).getPlayerCurrentPosition() * 50);

                    } else {
                        mPlayer.seekTo((mListData.get(position).getPlayerCurrentPosition() - 1) * 50);
                    }
                    lastPlayedAt = position;

                    final int duration = mPlayer.getDuration();

                    final int amongToupdate = 50;
                    mListData.get(position).setIsMediaPlaying(true);
                    mListData.get(position).setPlayerMaxDuration(mPlayer.getDuration());
                    final int max = mPlayer.getDuration() / 50;
                    sbDuration.setMax(max);

                    mTimer.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            context.runOnUiThread(() -> {
                                if (!(amongToupdate * mListData.get(position).getPlayerCurrentPosition() >= duration)) {
                                    int progress = mListData.get(position).getPlayerCurrentPosition();
                                    progress += 1;

                                    if (max >= progress) {
                                        mListData.get(position).setPlayerCurrentPosition(progress);
                                    }
                                }
                                notifyItemChanged(position);
                            });
                        }

                    }, amongToupdate, amongToupdate);

                    mPlayer.setOnCompletionListener(mediaPlayer1 -> {
                        sbDuration.setProgress(0);
                        pauseAudioPlay(position);

                    });
                    notifyDataSetChanged();
                }
            });
        } catch (Exception e) {
            MyLog.e(TAG, "Error", e);
        }
    }

    public void pauseAudioPlay(int pos) {
        if (mPlayer != null) {
            mPlayer.release();
            mTimer.cancel();
            if (pos == -1)
                pos = audioPlayedItemPosition;
            mListData.get(pos).setPlayerCurrentPosition(0);
            mListData.get(pos).setIsMediaPlaying(false);
            lastPlayedAt = -1;
            notifyDataSetChanged();
        }
    }

    //----------------------------------File Size Calculate---------------------------
    public String size(long size) {
        String hrSize = "";
        double k = size / 1024.0;
        double m = size / 1048576.0;
        double g = size / 1073741824.0;
        double t = size / (1073741824.0 * 1024.0);
        DecimalFormat form = new DecimalFormat("0.00");
        if (t > 1) {
            t = round(t, 1);
            hrSize = (t + "").concat(" TB");
        } else if (g > 1) {
            g = round(g, 1);
            hrSize = (g + "").concat(" GB");
        } else if (m > 1) {
            m = round(m, 1);
            hrSize = (m + "").concat(" MB");
        } else if (k > 1) {
            k = round(k, 1);
            hrSize = (k + "").concat(" KB");
        } else {
            hrSize = (size + "").concat(" Bytes");
        }

        return hrSize;
    }

//    @Override
//    public boolean isEnabled(int position) {
//        return true;
//    }

    private void ReceiverImageDownload(int position) {

        if (isSecretChat) {
            SecretChatViewActivity activity = (SecretChatViewActivity) context;
            activity.imagedownloadmethod(position);
        } else {
            ChatPageActivity activity = (ChatPageActivity) context;
            activity.imagedownloadmethod(position);
        }
    }

    //---------------------------------Group Info Layout-----------------------------

    private void ReceiverVideoDownload(int position) {
        if (isSecretChat) {
            SecretChatViewActivity activity = (SecretChatViewActivity) context;
            activity.VideoDownload(position);
        } else {
            ChatPageActivity activity = (ChatPageActivity) context;
            activity.VideoDownload(position);
        }
    }

    private void ReceiverAudioDownload(int position) {
        if (isSecretChat) {
            SecretChatViewActivity activity = (SecretChatViewActivity) context;
            activity.AudioDownload(position);
        } else {
            ChatPageActivity activity = (ChatPageActivity) context;
            activity.AudioDownload(position);
        }
    }

    private void ReceiverDocumentDownload(int position) {
        if (isSecretChat) {
            SecretChatViewActivity activity = (SecretChatViewActivity) context;
            activity.DocumentDownload(position);
        } else {
            ChatPageActivity activity = (ChatPageActivity) context;
            activity.DocumentDownload(position);
        }
    }

    private String getContactNameIfExists(String userId) {
        String userName = null;
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        String msisdn = contactDB_sqlite.getSingleData(userId, ContactDB_Sqlite.MSISDN);
//        FriendModel contact = CoreController.getContactsDbInstance(mContext).getUserDetails(userId);
        if (msisdn != null) {
            userName = getcontactname.getSendername(userId, msisdn);

        } else {

            ChatPageActivity activity = (ChatPageActivity) context;
            activity.getUserDetails(userId);
        }

        return userName;
    }

    public SpannableString makeLinks(String textView, ArrayList<String> links, ArrayList<String> userIDs) {
        SpannableString spannableString = new SpannableString(textView);

        StringBuilder myName = new StringBuilder(textView);

        for (int i = 0; i < links.size(); i++) {
            String link = links.get(i);
            int startIndexOfLink = myName.indexOf(link);
            myName.setCharAt(startIndexOfLink + 1, '}');

            if (startIndexOfLink == -1)
                startIndexOfLink = 0;
            spannableString.setSpan(new myClickableSpan(i, userIDs), startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }


        return spannableString;
    }


//-----------------------------------------------------Date Set Method----------------------------------

    private class ViewHolder1 extends RecyclerView.ViewHolder {

        public LinearLayout sent_message_main_layout;
        public TextView messageTextView;
        public TextView timeTextView;
        public FrameLayout send_text_message_normal_layout;
        public FrameLayout send_text_replay_message;
        public ImageView senter_text_normal_star_icon;
        public TextView sent_text_date_time;
        public ImageView sent_text_message_tick_icon;

        public TextView sender_replay_message_name_text;
        public ImageView image_photo_icon;
        public TextView sender_replay_message_text;
        public ImageView senter_replay_image_view;
        public TextView sender_replay_main_message;
        public ImageView senter_text_replay_star_icon;
        public TextView sent_replay_message_date_time;
        public ImageView sent_replay_message_tick_icon;
        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public TextView tvReadReceipt;

        ViewHolder1(View itemView) {
            super(itemView);

            sent_message_main_layout = itemView.findViewById(R.id.sent_message_main_layout);
            messageTextView = itemView.findViewById(R.id.send_Text_message);
            //holder1.messageTextView.setMovementMethod(LinkMovementMethod.getInstance());
            messageTextView.setHighlightColor(Color.TRANSPARENT);
            send_text_message_normal_layout = itemView.findViewById(R.id.send_text_message_normal_layout);
            send_text_replay_message = itemView.findViewById(R.id.send_text_replay_message);
            senter_text_normal_star_icon = itemView.findViewById(R.id.senter_text_normal_star_icon);
            sent_text_date_time = itemView.findViewById(R.id.sent_text_date_time);
            sent_text_message_tick_icon = itemView.findViewById(R.id.sent_text_message_tick_icon);
            sender_replay_message_name_text = itemView.findViewById(R.id.sender_replay_message_name_text);
            image_photo_icon = itemView.findViewById(R.id.image_photo_icon);
            sender_replay_message_text = itemView.findViewById(R.id.sender_replay_message_text);
            senter_replay_image_view = itemView.findViewById(R.id.senter_replay_image_view);
            sender_replay_main_message = itemView.findViewById(R.id.sender_replay_main_message);
            senter_text_replay_star_icon = itemView.findViewById(R.id.senter_text_replay_star_icon);
            sent_replay_message_date_time = itemView.findViewById(R.id.sent_replay_message_date_time);
            sent_replay_message_tick_icon = itemView.findViewById(R.id.sent_replay_message_tick_icon);
            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }

    }

    //----------------------------------Text Message Received-----------------------
    private class ViewHolder2 extends RecyclerView.ViewHolder {

        public RelativeLayout recieved_message_main_layout;
        public FrameLayout receiver_normal_message_layout;
        public FrameLayout receiver_replay_message_main_layout;
        public TextView receiver_text_group_sender_name;
        public TextView received_Text_message;
        public TextView received_text_date_time;
        public ImageView receiver_text_normal_star_icon;

        public TextView receiver_text_group_replay_sender_name;
        public TextView receiver_replay_message_name_text;
        public ImageView receiver_image_photo_icon;
        public TextView receiver_replay_message_text;
        public ImageView receiver_replay_image_view;
        public TextView receiver_replay_main_message;
        public ImageView receiver_text_replay_star_icon;
        public TextView receiver_replay_message_date_time;

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public TextView tvReadReceipt;

        ViewHolder2(View itemView) {
            super(itemView);

            recieved_message_main_layout = itemView.findViewById(R.id.recieved_message_main_layout);
            receiver_normal_message_layout = itemView.findViewById(R.id.receiver_normal_message_layout);
            receiver_replay_message_main_layout = itemView.findViewById(R.id.receiver_replay_message_main_layout);
            receiver_text_group_sender_name = itemView.findViewById(R.id.receiver_text_group_sender_name);
            received_Text_message = itemView.findViewById(R.id.received_Text_message);
            received_text_date_time = itemView.findViewById(R.id.received_text_date_time);
            receiver_text_group_replay_sender_name = itemView.findViewById(R.id.receiver_text_group_replay_sender_name);
            receiver_replay_message_name_text = itemView.findViewById(R.id.receiver_replay_message_name_text);
            receiver_image_photo_icon = itemView.findViewById(R.id.receiver_image_photo_icon);
            receiver_replay_message_text = itemView.findViewById(R.id.receiver_replay_message_text);
            receiver_replay_image_view = itemView.findViewById(R.id.receiver_replay_image_view);
            receiver_replay_main_message = itemView.findViewById(R.id.receiver_replay_main_message);
            receiver_text_replay_star_icon = itemView.findViewById(R.id.receiver_text_replay_star_icon);
            receiver_replay_message_date_time = itemView.findViewById(R.id.receiver_replay_message_date_time);
            receiver_text_normal_star_icon = itemView.findViewById(R.id.receiver_text_normal_star_icon);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);

            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //-----------------------------------Image Send------------------------------
    private class ViewHolder3 extends RecyclerView.ViewHolder {
        public PorterShapeImageView sender_side_imageview;
        public RelativeLayout senter_image_pause_resume_layout;
        public RelativeLayout sender_image_upload_progress_Layout;
        public ProgressBar sender_image_before_upload_progress;
        public CircularProgressBar senter_image_uploadprogress_bar;
        public RelativeLayout senter_image_Retry_layout;
        public LinearLayout send_image_inside_date_layout;
        public ImageView senter_text_normal_star_icon;
        public TextView sent_text_date_time;
        public ImageView sent_text_message_tick_icon;
        public RelativeLayout send_image_caption_layout;
        public TextView send_image_caption_text;
        public ImageView sent_image_caption_tick_icon;
        public TextView sent_image_caption_date_time;
        public ImageView senter_image_caption_star_icon;
        public RelativeLayout image_send_layout;
        public ImageView sender_forward_image;
        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public TextView senter_image_upload_text;
        public TopCropImageView top_crop_imageview;
        public TextView tvReadReceipt;

        ViewHolder3(View itemView) {
            super(itemView);

            sender_side_imageview = itemView.findViewById(R.id.sender_side_imageview);
            senter_image_pause_resume_layout = itemView.findViewById(R.id.senter_image_pause_resume_layout);
            sender_image_upload_progress_Layout = itemView.findViewById(R.id.sender_image_upload_progress_Layout);
            sender_image_before_upload_progress = itemView.findViewById(R.id.sender_image_before_upload_progress);
            senter_image_uploadprogress_bar = itemView.findViewById(R.id.senter_image_uploadprogress_bar);
            senter_image_Retry_layout = itemView.findViewById(R.id.senter_image_Retry_layout);
            send_image_inside_date_layout = itemView.findViewById(R.id.send_image_inside_date_layout);
            senter_text_normal_star_icon = itemView.findViewById(R.id.senter_text_normal_star_icon);
            sent_text_date_time = itemView.findViewById(R.id.sent_text_date_time);
            sent_text_message_tick_icon = itemView.findViewById(R.id.sent_text_message_tick_icon);
            send_image_caption_layout = itemView.findViewById(R.id.send_image_caption_layout);
            send_image_caption_text = itemView.findViewById(R.id.send_image_caption_text);
            sent_image_caption_tick_icon = itemView.findViewById(R.id.sent_image_caption_tick_icon);
            sent_image_caption_date_time = itemView.findViewById(R.id.sent_image_caption_date_time);
            senter_image_caption_star_icon = itemView.findViewById(R.id.senter_image_caption_star_icon);
            image_send_layout = itemView.findViewById(R.id.image_send_layout);
            sender_forward_image = itemView.findViewById(R.id.sender_forward_image);
            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            senter_image_upload_text = itemView.findViewById(R.id.senter_image_upload_text);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            top_crop_imageview = itemView.findViewById(R.id.top_crop_imageview);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //----------------------------------Video Send---------------
    private class ViewHolder4 extends RecyclerView.ViewHolder {

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView senter_video_forward_image;
        public PorterShapeImageView sender_side_videoimage;
        public LinearLayout sent_video_inside_date_layout;
        public ImageView senter_video_normal_star_icon;
        public TextView sent_video_date_time;
        public ImageView sent_video_tick_icon;
        public RelativeLayout senter_video_pause_resume;
        public RelativeLayout sender_video_upload_progress_Layout;
        public ProgressBar sender_video_before_upload_progress;
        public CircularProgressBar senter_video_uploadprogress_bar;
        public RelativeLayout senter_video_Retry_layout;
        public RelativeLayout senter_video_icon_and_duration_layout;
        public TextView sent_video_duration_text;

        public RelativeLayout sent_video_caption_layout;
        public TextView sent_video_caption_text;
        public ImageView send_video_caption_tick_icon;
        public TextView sent_video_caption_date_time;
        public ImageView senter_video_caption_star_icon;
        public ImageView sent_caption_video_duration_icon;
        public TextView sent_caption_video_duration_text;
        public RelativeLayout video_send_layout;
        public TextView tvReadReceipt;

        ViewHolder4(View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            senter_video_forward_image = itemView.findViewById(R.id.senter_video_forward_image);
            sender_side_videoimage = itemView.findViewById(R.id.sender_side_videoimage);
            sent_video_inside_date_layout = itemView.findViewById(R.id.sent_video_inside_date_layout);
            senter_video_normal_star_icon = itemView.findViewById(R.id.senter_video_normal_star_icon);
            sent_video_date_time = itemView.findViewById(R.id.sent_video_date_time);
            sent_video_tick_icon = itemView.findViewById(R.id.sent_video_tick_icon);
            senter_video_pause_resume = itemView.findViewById(R.id.senter_video_pause_resume);
            sender_video_upload_progress_Layout = itemView.findViewById(R.id.sender_video_upload_progress_Layout);
            sender_video_before_upload_progress = itemView.findViewById(R.id.sender_video_before_upload_progress);
            senter_video_uploadprogress_bar = itemView.findViewById(R.id.senter_video_uploadprogress_bar);
            senter_video_Retry_layout = itemView.findViewById(R.id.senter_video_Retry_layout);
            senter_video_icon_and_duration_layout = itemView.findViewById(R.id.senter_video_icon_and_duration_layout);
            sent_video_duration_text = itemView.findViewById(R.id.sent_video_duration_text);
            sent_video_caption_layout = itemView.findViewById(R.id.sent_video_caption_layout);
            sent_video_caption_text = itemView.findViewById(R.id.sent_video_caption_text);
            send_video_caption_tick_icon = itemView.findViewById(R.id.send_video_caption_tick_icon);
            sent_video_caption_date_time = itemView.findViewById(R.id.sent_video_caption_date_time);
            senter_video_caption_star_icon = itemView.findViewById(R.id.senter_video_caption_star_icon);
            sent_caption_video_duration_icon = itemView.findViewById(R.id.sent_caption_video_duration_icon);
            sent_caption_video_duration_text = itemView.findViewById(R.id.sent_caption_video_duration_text);
            video_send_layout = itemView.findViewById(R.id.video_send_layout);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------------Contact Send----------------------------------
    private class ViewHolder5 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView sender_forward_image;
        public CircleImageView senter_contact_image;
        public TextView senter_contact_name;
        public TextView senter_contact_number;
        public ImageView senter_contact_tick_icon;
        public TextView senter_contact_date_text;
        public ImageView senter_contact_caption_star_icon;
        public LinearLayout senter_side_invite_addcontact_layout;
        public RelativeLayout senter_contact_invite_layout;
        public TextView senter_contact_invite_text;
        public RelativeLayout senter_contact_addcontact_layout;
        public TextView senter_contact_addcontact_text;
        public TextView senter_contact_message_text;
        public RelativeLayout contact_send_layout;
        public TextView tvReadReceipt;

        public ViewHolder5(@NonNull View itemView) {
            super(itemView);
            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            sender_forward_image = itemView.findViewById(R.id.sender_forward_image);
            senter_contact_image = itemView.findViewById(R.id.senter_contact_image);
            senter_contact_name = itemView.findViewById(R.id.senter_contact_name);
            senter_contact_number = itemView.findViewById(R.id.senter_contact_number);
            senter_contact_tick_icon = itemView.findViewById(R.id.senter_contact_tick_icon);
            senter_contact_date_text = itemView.findViewById(R.id.senter_contact_date_text);
            senter_contact_caption_star_icon = itemView.findViewById(R.id.senter_contact_caption_star_icon);
            senter_side_invite_addcontact_layout = itemView.findViewById(R.id.senter_side_invite_addcontact_layout);
            senter_contact_invite_layout = itemView.findViewById(R.id.senter_contact_invite_layout);
            senter_contact_invite_text = itemView.findViewById(R.id.senter_contact_invite_text);
            senter_contact_addcontact_layout = itemView.findViewById(R.id.senter_contact_addcontact_layout);
            senter_contact_addcontact_text = itemView.findViewById(R.id.senter_contact_addcontact_text);
            senter_contact_message_text = itemView.findViewById(R.id.senter_contact_message_text);
            contact_send_layout = itemView.findViewById(R.id.contact_send_layout);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------------Audio Send----------------------------------
    private class ViewHolder6 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView sender_forward_image;
        public RelativeLayout senter_record_image_layout;
        public CircleImageView senter_record_image;
        public RelativeLayout senter_audio_image_layout;
        public RelativeLayout senter_audio_pause_resume;
        public RelativeLayout sender_audio_upload_progress_Layout;
        public ProgressBar sender_audio_before_upload_progress;
        public CircularProgressBar senter_audio_uploadprogress_bar;
        public RelativeLayout senter_audio_Retry_layout;
        public ImageView sent_audio_play_icon;
        public SeekBar senter_audio_seekbar;
        public TextView sent_audio_seek_duration_text;
        public TextView sent_audio_date_time_text;
        public ImageView senter_audio_star_icon;
        public ImageView senter_audio_tick_icon;
        public RelativeLayout audio_send_layout;
        public TextView audio_duration_text;
        public TextView tvReadReceipt;
        public View play_pause_container;

        ViewHolder6(View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            sender_forward_image = itemView.findViewById(R.id.sender_forward_image);
            senter_record_image_layout = itemView.findViewById(R.id.senter_record_image_layout);
            senter_record_image = itemView.findViewById(R.id.senter_record_image);
            senter_audio_image_layout = itemView.findViewById(R.id.senter_audio_image_layout);
            senter_audio_pause_resume = itemView.findViewById(R.id.senter_audio_pause_resume);
            sender_audio_upload_progress_Layout = itemView.findViewById(R.id.sender_audio_upload_progress_Layout);
            sender_audio_before_upload_progress = itemView.findViewById(R.id.sender_audio_before_upload_progress);
            senter_audio_uploadprogress_bar = itemView.findViewById(R.id.senter_audio_uploadprogress_bar);
            senter_audio_Retry_layout = itemView.findViewById(R.id.senter_audio_Retry_layout);
            sent_audio_play_icon = itemView.findViewById(R.id.sent_audio_play_icon);
            play_pause_container = itemView.findViewById(R.id.play_pause_container);
            senter_audio_seekbar = itemView.findViewById(R.id.senter_audio_seekbar);
            sent_audio_seek_duration_text = itemView.findViewById(R.id.sent_audio_seek_duration_text);
            sent_audio_date_time_text = itemView.findViewById(R.id.sent_audio_date_time_text);
            senter_audio_star_icon = itemView.findViewById(R.id.senter_audio_star_icon);
            senter_audio_tick_icon = itemView.findViewById(R.id.senter_audio_tick_icon);
            audio_send_layout = itemView.findViewById(R.id.audio_send_layout);
            audio_duration_text = itemView.findViewById(R.id.audio_duration_text);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------------Location Send----------------------------------
    private class ViewHolder7 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView sender_forward_image;
        public PorterShapeImageView senter_location_map_image;
        public TextView senter_location_address_title;
        public TextView senter_location_address_text;
        public ImageView sent_location_tick_icon;
        public TextView sent_location_date_time;
        public ImageView senter_location_star_icon;
        public RelativeLayout location_send_layout;
        public TextView tvReadReceipt;

        public ViewHolder7(@NonNull View itemView) {
            super(itemView);
            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            sender_forward_image = itemView.findViewById(R.id.sender_forward_image);
            senter_location_map_image = itemView.findViewById(R.id.senter_location_map_image);
            senter_location_address_title = itemView.findViewById(R.id.senter_location_address_title);
            senter_location_address_text = itemView.findViewById(R.id.senter_location_address_text);
            sent_location_tick_icon = itemView.findViewById(R.id.sent_location_tick_icon);
            sent_location_date_time = itemView.findViewById(R.id.sent_location_date_time);
            senter_location_star_icon = itemView.findViewById(R.id.senter_location_star_icon);
            location_send_layout = itemView.findViewById(R.id.location_send_layout);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------------Document Send----------------------------------
    private class ViewHolder8 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView sender_forward_image;
        public ImageView senter_document_image;
        public TextView senter_document_file_name;
        public RelativeLayout senter_document_pause_resume;
        public RelativeLayout sender_document_upload_progress_Layout;
        public ProgressBar sender_document_before_upload_progress;
        public CircularProgressBar senter_document_uploadprogress_bar;
        public RelativeLayout senter_document_Retry_layout;
        public ImageView senter_document_retry_icon;
        public TextView senter_document_size_text;
        public ImageView senter_document_tick_icon;
        public TextView senter_side_date_time_text;
        public ImageView senter_document_star_icon;
        public RelativeLayout document_send_layout;
        public RelativeLayout document_layout;
        public TextView tvReadReceipt;

        public ViewHolder8(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            sender_forward_image = itemView.findViewById(R.id.sender_forward_image);
            senter_document_image = itemView.findViewById(R.id.senter_document_image);
            senter_document_file_name = itemView.findViewById(R.id.senter_document_file_name);
            senter_document_pause_resume = itemView.findViewById(R.id.senter_document_pause_resume);
            sender_document_upload_progress_Layout = itemView.findViewById(R.id.sender_document_upload_progress_Layout);
            sender_document_before_upload_progress = itemView.findViewById(R.id.sender_document_before_upload_progress);
            senter_document_uploadprogress_bar = itemView.findViewById(R.id.senter_document_uploadprogress_bar);
            senter_document_Retry_layout = itemView.findViewById(R.id.senter_document_Retry_layout);
            senter_document_retry_icon = itemView.findViewById(R.id.senter_document_retry_icon);
            senter_document_size_text = itemView.findViewById(R.id.senter_document_size_text);
            senter_document_tick_icon = itemView.findViewById(R.id.senter_document_tick_icon);
            senter_side_date_time_text = itemView.findViewById(R.id.senter_side_date_time_text);
            senter_document_star_icon = itemView.findViewById(R.id.senter_document_star_icon);
            document_send_layout = itemView.findViewById(R.id.document_send_layout);
            document_layout = itemView.findViewById(R.id.document_layout);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------------WebLink Send----------------------------------
    private class ViewHolder9 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView sender_forward_image;
        public RelativeLayout sent_weblink_layout;
        public ImageView senter_weblink_image;
        public TextView senter_weblink_file_name;
        public TextView senter_weblink_http;
        public TextView senter_weblink_description;
        public TextView senter_side_weblink;
        public ImageView senter_weblink_tick_icon;
        public TextView senter_weblinkside_date_time_text;
        public ImageView senter_weblink_star_icon;
        public RelativeLayout weblink_send_layout;
        public TextView tvReadReceipt;

        public ViewHolder9(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            sender_forward_image = itemView.findViewById(R.id.sender_forward_image);
            sent_weblink_layout = itemView.findViewById(R.id.sent_weblink_layout);
            senter_weblink_image = itemView.findViewById(R.id.senter_weblink_image);
            senter_weblink_file_name = itemView.findViewById(R.id.senter_weblink_file_name);
            senter_weblink_http = itemView.findViewById(R.id.senter_weblink_http);
            senter_weblink_description = itemView.findViewById(R.id.senter_weblink_description);
            senter_side_weblink = itemView.findViewById(R.id.senter_side_weblink);
            senter_weblink_tick_icon = itemView.findViewById(R.id.senter_weblink_tick_icon);
            senter_weblinkside_date_time_text = itemView.findViewById(R.id.senter_weblinkside_date_time_text);
            senter_weblink_star_icon = itemView.findViewById(R.id.senter_weblink_star_icon);
            weblink_send_layout = itemView.findViewById(R.id.weblink_send_layout);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------------Sender Side Delete Message---------------------------------
    private class ViewHolder10 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel;
        public TextView delete_senter_side_date_time_text;
        public RelativeLayout delete_send_layout;
        public TextView tvReadReceipt;

        public ViewHolder10(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            delete_senter_side_date_time_text = itemView.findViewById(R.id.delete_senter_side_date_time_text);
            delete_send_layout = itemView.findViewById(R.id.delete_send_layout);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);

            itemView.setTag(this);
        }
    }

    //---------------------------------Image Received-----------------------------
    private class ViewHolder11 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView receiver_forward_image;
        public TextView delete_senter_side_date_time_text;
        public TextView receiver_image_group_sender_name;
        public PorterShapeImageView receiver_side_imageview;
        public RelativeLayout receiver_image_pause_resume_layout;
        public RelativeLayout receiver_image_dowload_progress_Layout;
        public ProgressBar receiver_image_before_download_progress;
        public CircularProgressBar progress_bar;
        public RelativeLayout image_download_layout;
        public TextView image_download_text;
        public RelativeLayout receiver_image_inside_date_layout;
        public ImageView received_image_tick_icon;
        public TextView received_image_date_time;
        public ImageView receiver_image_normal_star_icon;
        public RelativeLayout receive_image_caption_layout;
        public TextView receive_image_caption_text;
        public ImageView receive_image_caption_tick_icon;
        public TextView receive_image_caption_date_time;
        public ImageView receiver_image_caption_star_icon;
        public RelativeLayout image_received_layout;
        public TopCropImageView receiver_top_crop_imageview;
        public TextView tvReadReceipt;

        public ViewHolder11(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            receiver_forward_image = itemView.findViewById(R.id.receiver_forward_image);
            delete_senter_side_date_time_text = itemView.findViewById(R.id.delete_senter_side_date_time_text);
            receiver_image_group_sender_name = itemView.findViewById(R.id.receiver_image_group_sender_name);
            receiver_image_pause_resume_layout = itemView.findViewById(R.id.receiver_image_pause_resume_layout);
            receiver_image_dowload_progress_Layout = itemView.findViewById(R.id.receiver_image_dowload_progress_Layout);
            receiver_image_before_download_progress = itemView.findViewById(R.id.receiver_image_before_download_progress);
            progress_bar = itemView.findViewById(R.id.progress_bar);
            image_download_layout = itemView.findViewById(R.id.image_download_layout);
            image_download_text = itemView.findViewById(R.id.image_download_text);
            receiver_image_inside_date_layout = itemView.findViewById(R.id.receiver_image_inside_date_layout);
            received_image_tick_icon = itemView.findViewById(R.id.received_image_tick_icon);
            received_image_date_time = itemView.findViewById(R.id.received_image_date_time);
            receiver_image_normal_star_icon = itemView.findViewById(R.id.receiver_image_normal_star_icon);
            receive_image_caption_layout = itemView.findViewById(R.id.receive_image_caption_layout);
            receive_image_caption_text = itemView.findViewById(R.id.receive_image_caption_text);
            receive_image_caption_tick_icon = itemView.findViewById(R.id.receive_image_caption_tick_icon);
            receive_image_caption_date_time = itemView.findViewById(R.id.receive_image_caption_date_time);
            receiver_image_caption_star_icon = itemView.findViewById(R.id.receiver_image_caption_star_icon);
            image_received_layout = itemView.findViewById(R.id.image_received_layout);
            receiver_side_imageview = itemView.findViewById(R.id.receiver_side_imageview);
            receiver_top_crop_imageview = itemView.findViewById(R.id.receiver_top_crop_imageview);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------Video Received-----------------------------
    private class ViewHolder12 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView receiver_forward_image;
        public RelativeLayout video_received_layout;
        public TextView receiver_video_group_sender_name;
        public PorterShapeImageView receiver_side_videoimage;
        public RelativeLayout receiver_video_play_icon;
        public RelativeLayout receiver_video_inside_date_layout;
        public TextView receiver_video_date_time;
        public ImageView receiver_video_normal_star_icon;
        public RelativeLayout receiver_video_duration_layout;
        public TextView receiver_video_duration_text;
        public RelativeLayout receiver_video_pause_resume;
        public RelativeLayout video_download_layout;
        public TextView video_download_text;
        public RelativeLayout receiver_video_dowload_progress_Layout;
        public ProgressBar receiver_video_before_download_progress;
        public CircularProgressBar video_progress_bar;
        public RelativeLayout receiver_video_caption_layout;
        public TextView receiver_video_caption_text;
        public ImageView receiver_video_caption_tick_icon;
        public TextView receiver_video_caption_date_time;
        public ImageView receiver_video_caption_star_icon;
        public TextView receiver_caption_video_duration_text;
        public TextView tvReadReceipt;

        public ViewHolder12(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            receiver_forward_image = itemView.findViewById(R.id.receiver_forward_image);
            video_received_layout = itemView.findViewById(R.id.video_received_layout);
            receiver_video_group_sender_name = itemView.findViewById(R.id.receiver_video_group_sender_name);
            receiver_side_videoimage = itemView.findViewById(R.id.receiver_side_videoimage);
            receiver_video_play_icon = itemView.findViewById(R.id.receiver_video_play_icon);
            receiver_video_inside_date_layout = itemView.findViewById(R.id.receiver_video_inside_date_layout);
            receiver_video_date_time = itemView.findViewById(R.id.receiver_video_date_time);
            receiver_video_normal_star_icon = itemView.findViewById(R.id.receiver_video_normal_star_icon);
            receiver_video_duration_layout = itemView.findViewById(R.id.receiver_video_duration_layout);
            receiver_video_duration_text = itemView.findViewById(R.id.receiver_video_duration_text);
            receiver_video_pause_resume = itemView.findViewById(R.id.receiver_video_pause_resume);
            video_download_layout = itemView.findViewById(R.id.video_download_layout);
            video_download_text = itemView.findViewById(R.id.video_download_text);
            receiver_video_dowload_progress_Layout = itemView.findViewById(R.id.receiver_video_dowload_progress_Layout);
            receiver_video_before_download_progress = itemView.findViewById(R.id.receiver_video_before_download_progress);
            video_progress_bar = itemView.findViewById(R.id.video_progress_bar);
            receiver_video_caption_layout = itemView.findViewById(R.id.receiver_video_caption_layout);
            receiver_video_caption_text = itemView.findViewById(R.id.receiver_video_caption_text);
            receiver_video_caption_tick_icon = itemView.findViewById(R.id.receiver_video_caption_tick_icon);
            receiver_video_caption_date_time = itemView.findViewById(R.id.receiver_video_caption_date_time);
            receiver_video_caption_star_icon = itemView.findViewById(R.id.receiver_video_caption_star_icon);
            receiver_caption_video_duration_text = itemView.findViewById(R.id.receiver_caption_video_duration_text);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);

            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });

        }
    }

    //---------------------------------Audio Received-----------------------------
    private class ViewHolder13 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView receiver_forward_image;
        public RelativeLayout Audio_received_layout;
        public TextView receiver_audio_group_sender_name;
        public RelativeLayout receiver_record_image_layout;
        public ImageView receiver_record_image;
        public RelativeLayout receiver_audio_image_layout;
        public TextView receiver_audio_duration_text;
        public RelativeLayout receiver_audio_image_download_layout;
        public RelativeLayout receiver_audio_pause_resume;
        public RelativeLayout receiver_audio_dowload_progress_Layout;
        public ProgressBar receiver_audio_before_download_progress;
        public CircularProgressBar audio_progress_bar;
        public ImageView receiver_audio_play_icon;
        public View play_pause_container;
        public SeekBar receiver_audio_seekbar;
        public TextView receiver_audio_seek_duration_text;
        public TextView receiver_audio_date_time_text;
        public ImageView receiver_audio_star_icon;
        public ImageView receiver_audio_tick_icon;
        public ImageView receiver_audio_download_icon;
        public TextView tvReadReceipt;

        public ViewHolder13(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            receiver_forward_image = itemView.findViewById(R.id.receiver_forward_image);
            Audio_received_layout = itemView.findViewById(R.id.Audio_received_layout);
            receiver_audio_group_sender_name = itemView.findViewById(R.id.receiver_audio_group_sender_name);
            receiver_record_image_layout = itemView.findViewById(R.id.receiver_record_image_layout);
            receiver_record_image = itemView.findViewById(R.id.receiver_record_image);
            receiver_audio_image_layout = itemView.findViewById(R.id.receiver_audio_image_layout);
            receiver_audio_duration_text = itemView.findViewById(R.id.receiver_audio_duration_text);
            receiver_audio_image_download_layout = itemView.findViewById(R.id.receiver_audio_image_download_layout);
            receiver_audio_pause_resume = itemView.findViewById(R.id.receiver_audio_pause_resume);
            receiver_audio_dowload_progress_Layout = itemView.findViewById(R.id.receiver_audio_dowload_progress_Layout);
            receiver_audio_before_download_progress = itemView.findViewById(R.id.receiver_audio_before_download_progress);
            audio_progress_bar = itemView.findViewById(R.id.audio_progress_bar);
            receiver_audio_play_icon = itemView.findViewById(R.id.receiver_audio_play_icon);
            play_pause_container = itemView.findViewById(R.id.play_pause_container);
            receiver_audio_seekbar = itemView.findViewById(R.id.receiver_audio_seekbar);
            receiver_audio_seek_duration_text = itemView.findViewById(R.id.receiver_audio_seek_duration_text);
            receiver_audio_date_time_text = itemView.findViewById(R.id.receiver_audio_date_time_text);
            receiver_audio_star_icon = itemView.findViewById(R.id.receiver_audio_star_icon);
            receiver_audio_tick_icon = itemView.findViewById(R.id.receiver_audio_tick_icon);
            receiver_audio_download_icon = itemView.findViewById(R.id.receiver_audio_download_icon);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });

        }
    }

    //---------------------------------Document Received-----------------------------
    private class ViewHolder14 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView receiver_forward_image;
        public RelativeLayout document_received_layout;
        public TextView receiver_document_group_sender_name;
        public ImageView receiver_document_image;
        public TextView receiver_document_file_name;
        public RelativeLayout receiver_document_pause_resume_layout;
        public RelativeLayout receiver_document_dowload_Layout;
        public RelativeLayout receiver_document_dowload_progress_Layout;
        public ProgressBar receiver_document_before_download_progress;
        public CircularProgressBar document_progress_bar;
        public TextView receiver_document_size_text;
        public ImageView receiver_document_tick_icon;
        public TextView receiver_side_date_time_text;
        public ImageView receiver_document_star_icon;
        public RelativeLayout document_main_receive_layout;
        public TextView tvReadReceipt;

        public ViewHolder14(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            receiver_forward_image = itemView.findViewById(R.id.receiver_forward_image);
            document_received_layout = itemView.findViewById(R.id.document_received_layout);
            receiver_document_group_sender_name = itemView.findViewById(R.id.receiver_document_group_sender_name);
            receiver_document_image = itemView.findViewById(R.id.receiver_document_image);
            receiver_document_file_name = itemView.findViewById(R.id.receiver_document_file_name);
            receiver_document_pause_resume_layout = itemView.findViewById(R.id.receiver_document_pause_resume_layout);
            receiver_document_dowload_Layout = itemView.findViewById(R.id.receiver_document_dowload_Layout);
            receiver_document_dowload_progress_Layout = itemView.findViewById(R.id.receiver_document_dowload_progress_Layout);
            receiver_document_before_download_progress = itemView.findViewById(R.id.receiver_document_before_download_progress);
            document_progress_bar = itemView.findViewById(R.id.document_progress_bar);
            receiver_document_size_text = itemView.findViewById(R.id.receiver_document_size_text);
            receiver_document_tick_icon = itemView.findViewById(R.id.receiver_document_tick_icon);
            receiver_side_date_time_text = itemView.findViewById(R.id.receiver_side_date_time_text);
            receiver_document_star_icon = itemView.findViewById(R.id.receiver_document_star_icon);
            document_main_receive_layout = itemView.findViewById(R.id.document_main_receive_layout);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------Weblink Received-----------------------------
    private class ViewHolder15 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public ImageView receiver_forward_image;
        public RelativeLayout weblink_received_layout;
        public TextView receiver_weblink_group_sender_name;
        public ImageView receive_weblink_image;
        public TextView receive_weblink_file_name;
        public TextView receiver_weblink_http;
        public TextView receiver_weblink_description;
        public TextView receiver_side_weblink;
        public ImageView receiver_weblink_tick_icon;
        public TextView receiver_weblinkside_date_time_text;
        public ImageView receiver_weblink_star_icon;
        public TextView tvReadReceipt;

        public ViewHolder15(@NonNull View itemView) {
            super(itemView);
            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            receiver_forward_image = itemView.findViewById(R.id.receiver_forward_image);
            weblink_received_layout = itemView.findViewById(R.id.weblink_received_layout);
            receiver_weblink_group_sender_name = itemView.findViewById(R.id.receiver_weblink_group_sender_name);
            receive_weblink_image = itemView.findViewById(R.id.receive_weblink_image);
            receive_weblink_file_name = itemView.findViewById(R.id.receive_weblink_file_name);
            receiver_weblink_http = itemView.findViewById(R.id.receiver_weblink_http);
            receiver_weblink_description = itemView.findViewById(R.id.receiver_weblink_description);
            receiver_side_weblink = itemView.findViewById(R.id.receiver_side_weblink);
            receiver_weblink_tick_icon = itemView.findViewById(R.id.receiver_weblink_tick_icon);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            receiver_weblinkside_date_time_text = itemView.findViewById(R.id.receiver_weblinkside_date_time_text);
            receiver_weblink_star_icon = itemView.findViewById(R.id.receiver_weblink_star_icon);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------Receiver Side Delete Layout-----------------------------
    private class ViewHolder16 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel;
        public RelativeLayout delete_received_layout;
        public TextView delete_receiver_side_date_time_text;
        public TextView tvReadReceipt;

        public ViewHolder16(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            delete_received_layout = itemView.findViewById(R.id.delete_received_layout);
            delete_receiver_side_date_time_text = itemView.findViewById(R.id.delete_receiver_side_date_time_text);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            itemView.setTag(this);
        }
    }


    //----------------------------------Download Process-------------------------------------

    //---------------------------------Call Info Layout-----------------------------
    private class ViewHolder17 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public TextView tvCallLbl;

        public ViewHolder17(@NonNull View itemView) {
            super(itemView);

            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            tvCallLbl = itemView.findViewById(R.id.tvCallLbl);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            itemView.setTag(this);
        }
    }

    private class ViewHolder18 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public TextView tvgroup_info;
        public TextView tvReadReceipt;

        public ViewHolder18(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            tvgroup_info = itemView.findViewById(R.id.tvgroup_info);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            itemView.setTag(this);
        }
    }

    //---------------------------------Contact Receive Layout-----------------------------
    private class ViewHolder19 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public RelativeLayout contact_received_layout;
        public ImageView receiver_forward_image;
        public TextView receiver_contact_group_sender_name;
        public ImageView receiver_contact_image;
        public TextView receiver_contact_name;
        public TextView receiver_contact_number;
        public ImageView receiver_contact_tick_icon;
        public TextView receiver_contact_date_text;
        public ImageView receiver_contact_caption_star_icon;
        public LinearLayout receiver_side_invite_addcontact_layout;
        public RelativeLayout receiver_contact_invite_layout;
        public TextView receiver_contact_invite_text;
        public RelativeLayout receiver_contact_addcontact_layout;
        public TextView receiver_contact_addcontact_text;
        public TextView receiver_contact_message_text;
        public TextView tvReadReceipt;

        public ViewHolder19(@NonNull View itemView) {
            super(itemView);

            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            contact_received_layout = itemView.findViewById(R.id.contact_received_layout);
            receiver_forward_image = itemView.findViewById(R.id.receiver_forward_image);
            receiver_contact_group_sender_name = itemView.findViewById(R.id.receiver_contact_group_sender_name);
            receiver_contact_image = itemView.findViewById(R.id.receiver_contact_image);
            receiver_contact_name = itemView.findViewById(R.id.receiver_contact_name);
            receiver_contact_number = itemView.findViewById(R.id.receiver_contact_number);
            receiver_contact_tick_icon = itemView.findViewById(R.id.receiver_contact_tick_icon);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            receiver_contact_date_text = itemView.findViewById(R.id.receiver_contact_date_text);
            receiver_contact_caption_star_icon = itemView.findViewById(R.id.receiver_contact_caption_star_icon);
            receiver_side_invite_addcontact_layout = itemView.findViewById(R.id.receiver_side_invite_addcontact_layout);
            receiver_contact_invite_layout = itemView.findViewById(R.id.receiver_contact_invite_layout);
            receiver_contact_invite_text = itemView.findViewById(R.id.receiver_contact_invite_text);
            receiver_contact_addcontact_layout = itemView.findViewById(R.id.receiver_contact_addcontact_layout);
            receiver_contact_addcontact_text = itemView.findViewById(R.id.receiver_contact_addcontact_text);
            receiver_contact_message_text = itemView.findViewById(R.id.receiver_contact_message_text);

            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------Location Receive Layout-----------------------------
    private class ViewHolder20 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel, tvSecretLbl;
        public RelativeLayout location_received_layout;
        public ImageView receiver_forward_image;
        public TextView receiver_location_group_sender_name;
        public PorterShapeImageView receive_location_map_image;
        public TextView receiver_location_title;
        public TextView receive_location_address_text;
        public ImageView receive_location_tick_icon;
        public TextView receive_location_date_time;
        public ImageView receiver_location_star_icon;
        public TextView tvReadReceipt;

        public ViewHolder20(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            location_received_layout = itemView.findViewById(R.id.location_received_layout);
            receiver_forward_image = itemView.findViewById(R.id.receiver_forward_image);
            receiver_location_group_sender_name = itemView.findViewById(R.id.receiver_location_group_sender_name);
            receive_location_map_image = itemView.findViewById(R.id.receive_location_map_image);
            receiver_location_title = itemView.findViewById(R.id.receiver_location_title);
            receive_location_address_text = itemView.findViewById(R.id.receive_location_address_text);
            receive_location_tick_icon = itemView.findViewById(R.id.receive_location_tick_icon);
            receive_location_date_time = itemView.findViewById(R.id.receive_location_date_time);
            receiver_location_star_icon = itemView.findViewById(R.id.receiver_location_star_icon);
            tvReadReceipt = itemView.findViewById(R.id.text_read_receipt);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            itemView.setTag(this);

            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAbsoluteAdapterPosition()));
            itemView.setOnLongClickListener(view -> {
                itemClickListener.itemLongClick(getAbsoluteAdapterPosition());
                return false;
            });
        }
    }

    //---------------------------------Encryption  Layout-----------------------------
    private class ViewHolder21 extends RecyclerView.ViewHolder{

        public TextView tv_info;
        public TextView tvDateLbl, encryptionlabel;
        public TextView tvSecretLbl;

        public ViewHolder21(@NonNull View itemView) {
            super(itemView);
        }
    }


    //---------------------------------TIMER CHANGE  Layout-----------------------------
    private class ViewHolder22 extends RecyclerView.ViewHolder{

        public TextView tvDateLbl, encryptionlabel;
        public TextView tvSecretLbl;

        public ViewHolder22(@NonNull View itemView) {
            super(itemView);

            tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
            tvSecretLbl = itemView.findViewById(R.id.tvSecretLbl);
            encryptionlabel = itemView.findViewById(R.id.encryptionlabel);
            itemView.setTag(this);
        }
    }

    //---------------------------------TIMER CHANGE  Layout-----------------------------
    private class ViewHolder23 extends RecyclerView.ViewHolder{

        public TextView tv_screen_shot_taken;

        public ViewHolder23(@NonNull View itemView) {
            super(itemView);

            tv_screen_shot_taken = itemView.findViewById(R.id.tv_screen_shot_taken);

            itemView.setTag(this);
        }
    }


//------------------------------------------Tag Applied Checking---------------------------------

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                mOriginalValues = new ArrayList<>(mListData); // saves the original data in mOriginalValues
            }

            final ArrayList<MessageItemChat> nlist = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                results.count = mOriginalValues.size();
                results.values = mOriginalValues;
            } else {
                for (int i = 0; i < mOriginalValues.size(); i++) {
                    MessageItemChat chat_message_item = mOriginalValues.get(i);
                    if (chat_message_item.getTextMessage() != null &&
                            chat_message_item.getTextMessage().toLowerCase().contains(filterString)) {
                        //Check MessageType 25 and if it is deleted don't add it
                        if (Integer.parseInt(chat_message_item.getMessageType()) != 25) {
                            nlist.add(chat_message_item);
                        }
                        //    nlist.add(chat_message_item);
                    }
                }
                results.values = nlist;
                results.count = nlist.size();
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mListData = (ArrayList<MessageItemChat>) results.values;
            notifyDataSetChanged();
            ArrayList<MessageItemChat> filtered = (ArrayList<MessageItemChat>) results.values;
            notifyDataSetChanged();

        }

    }

    private void configureSecretTimerLabel(TextView tvSecretLbl, int position) {
        if (!isSecretChat) {
            return;
        }
        tvSecretLbl.setVisibility(View.GONE);
        MessageItemChat currentMsgItem = mListData.get(position);

        if (position == 0) {
            tvSecretLbl.setVisibility(View.VISIBLE);
        } else {
            MessageItemChat prevMsgItem = mListData.get(position - 1);
            if (!currentMsgItem.getSecretTimer().equals(prevMsgItem.getSecretTimer())) {
                tvSecretLbl.setVisibility(View.VISIBLE);
            }
        }

        if (tvSecretLbl.getVisibility() == View.VISIBLE) {
            String createdBy = mListData.get(position).getSecretTimeCreatedBy();
            String name = null;
            if (createdBy.equals(mCurrentUserId)) {
                name = "You";
            } else {
                name = getcontactname.getSendername(createdBy, mListData.get(position).getSenderMsisdn());
            }

            int time = Integer.parseInt(currentMsgItem.getSecretTimer());
            String timeStr = "";
            switch (time) {
                case 5 * 1000:
                    timeStr = context.getResources().getString(R.string.time_5sec);
                    break;

                case 10 * 1000:
                    timeStr = context.getResources().getString(R.string.time_10sec);
                    break;

                case 30 * 1000:
                    timeStr = context.getResources().getString(R.string.time_30sec);
                    break;

                case 60 * 1000:
                    timeStr = context.getResources().getString(R.string.time_1min);
                    break;

                case 60 * 60 * 1000:
                    timeStr = context.getResources().getString(R.string.time_1hr);
                    break;

                case 24 * 60 * 60 * 1000:
                    timeStr = context.getResources().getString(R.string.time_1day);
                    break;

                case 7 * 24 * 60 * 60 * 1000:
                    timeStr = context.getResources().getString(R.string.time_1week);
                    break;
            }

            if (name != null) {
                String msg = name + " " + context.getResources().getString(R.string.set_expiration_time) + " " + timeStr;
                tvSecretLbl.setText(msg);
            } else if (currentMsgItem.getSenderMsisdn() != null) {
                String msg = currentMsgItem.getSenderMsisdn() + " " + context.getResources().getString(R.string.set_expiration_time) + " " + timeStr;
                tvSecretLbl.setText(msg);
            } else {
                String msg = SecretChatViewActivity.receiverMsisdn + " " + context.getResources().getString(R.string.set_expiration_time) + " " + timeStr;
                tvSecretLbl.setText(msg);

            }
        }

    }

    public class myClickableSpan extends ClickableSpan {
        ArrayList<String> clickable_userid;
        int pos;

        public myClickableSpan(int position, ArrayList<String> userIDs) {
            this.pos = position;
            this.clickable_userid = userIDs;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(Color.BLUE); // specific color for this link
        }

        @Override
        public void onClick(View widget) {
            // TODO add check if widget instanceof TextView
            TextView tv = (TextView) widget;
            // TODO add check if tv.getText() instanceof Spanned
            Spanned s = (Spanned) tv.getText();

            if (clickable_userid != null)
                if (clickable_userid.size() > 0) {
                    for (int i = 0; i <= clickable_userid.size() - 1; i++) {
                        if (pos == i) {
                            String userid = clickable_userid.get(i);
                            if (userid != null)
                                if (!sessionmanager.getCurrentUserID().equalsIgnoreCase(userid)) {
                                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
                                    FriendModel model = contactDB_sqlite.getFriendDetails(userid);

                                    Intent infoIntent = new Intent(context, UserInfo.class);
                                    infoIntent.putExtra("UserId", model.get_id());
                                    infoIntent.putExtra("UserName", model.getFirstName());
                                    infoIntent.putExtra("UserAvatar", model.getAvatarImageUrl());
                                    infoIntent.putExtra("UserNumber", model.getMsisdn());
                                    infoIntent.putExtra("FromSecretChat", false);
                                    Log.e("infoIntent", "infoIntent" + infoIntent);
                                    context.startActivity(infoIntent);
                                }
                        }
                    }
                }


        }

    }

    public boolean canDisplayPdf(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType("application/pdf");
        return packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0;
    }

    private Bitmap getScaledBitmap(Bitmap b, int reqWidth, int reqHeight) {
        int bWidth = b.getWidth();
        int bHeight = b.getHeight();

        int nWidth = bWidth;
        int nHeight = bHeight;

        if (nWidth > reqWidth) {
            int ratio = bWidth / reqWidth;
            if (ratio > 0) {
                nWidth = reqWidth;
                nHeight = bHeight / ratio;
            }
        }

        if (nHeight > reqHeight) {
            int ratio = bHeight / reqHeight;
            if (ratio > 0) {
                nHeight = reqHeight;
                nWidth = bWidth / ratio;
            }
        }

        return Bitmap.createScaledBitmap(b, nWidth, nHeight, true);
    }

    public void pdfviewer(Context mContext, String pdfLink) {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setDataAndType(Uri.parse("http://docs.google.com/viewer?url=" + pdfLink), "text/html");

        mContext.startActivity(intent);
    }


}

