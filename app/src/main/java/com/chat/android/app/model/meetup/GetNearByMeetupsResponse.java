package com.chat.android.app.model.meetup;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetNearByMeetupsResponse {
    @SerializedName("err")
    int err;

    @SerializedName("users")
    List<NearbyUserModel> users;


    public GetNearByMeetupsResponse() {
    }

    public GetNearByMeetupsResponse(int err, List<NearbyUserModel> users) {
        this.err = err;
        this.users = users;
    }

    public int getErr() {
        return err;
    }

    public void setErr(int err) {
        this.err = err;
    }

    public List<NearbyUserModel> getUsers() {
        return users;
    }

    public void setUsers(List<NearbyUserModel> users) {
        this.users = users;
    }
}
