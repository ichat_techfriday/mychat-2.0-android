package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.chat.android.R;
import com.github.lzyzsd.circleprogress.DonutProgress;

/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredAudioSent extends RecyclerView.ViewHolder {
    public TextView senderName, time,toname,fromname,datelbl,duration;
    public ImageView singleTick, doubleTickGreen, doubleTickBlue, clock, playButton,starredindicator,userprofile;
    public SeekBar sbDuration;
    public DonutProgress pbUpload;

    /**
     * one more field to show audio data to be added
     */
    public VHStarredAudioSent(View view) {
        super(view);

        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        duration= view.findViewById(R.id.duration);
        senderName = view.findViewById(R.id.lblMsgFrom);
        playButton = view.findViewById(R.id.imageView26);
        userprofile= view.findViewById(R.id.userprofile);
        time = view.findViewById(R.id.ts);
        singleTick = view.findViewById(R.id.single_tick_green);
        doubleTickGreen = view.findViewById(R.id.double_tick_green);
        doubleTickBlue = view.findViewById(R.id.double_tick_blue);
        clock = view.findViewById(R.id.clock);
        starredindicator = view.findViewById(R.id.starredindicator);
        sbDuration = view.findViewById(R.id.sbDuration);
        pbUpload = view.findViewById(R.id.pbUpload);
        pbUpload.setMax(100);
    }

}
