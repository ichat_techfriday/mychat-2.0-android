package com.chat.android.app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Ignore;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chat.android.BuildConfig;
import com.chat.android.R;
import com.chat.android.app.adapter.FriendAdapter;
import com.chat.android.app.adapter.RItemAdapter;
import com.chat.android.app.callbacks.CallbackListener;
import com.chat.android.app.dialog.ChatLockPwdDialog;
import com.chat.android.app.dialog.CustomMultiTextItemsDialog;
import com.chat.android.app.model.BottomSheetModel;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.ChatLockPojo;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.model.MultiTextDialogPojo;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.DialogUtils;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.StorageUtility;
import com.chat.android.utils.TextUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 */
public class ContactsFragment extends Fragment {
    boolean refreshcontactsset = false;
    RecyclerView requestsRecyclerView, lvContacts;
    TextView tvContact;
    private EditText mSearchEt;
    FriendAdapter requestsAdapter;
    FriendAdapter friendsAdapter;
    EditText etSearch;
    private UserInfoSession userInfoSession;

    private TextView selectcontact;
    private TextView selectcontactmember;
    private ImageView serach;
    private ImageView backarrow;
    private ImageView backButton;
    InputMethodManager inputMethodManager;
    private Getcontactname getcontactname;
    private ImageView ivDownArrowRequests;
    private ImageView ivDownArrowContacts;
    private TextView tvRequestsCount;
    private SessionManager sessionManager;
    private TextView contact_empty, tvUserName, tvUserPinCode, tvOnlineStatus;
    private LinearLayout tvOnlineStatusLayout;
    private ImageView ivUserProfile;
    String username, profileImage;
    private SearchView searchView;
    private String receiverDocumentID, uniqueCurrentID;
    private static final String TAG = ContactsFragment.class.getSimpleName() + ">>";
    private List<FriendModel> scimboRequestsEntries = new ArrayList<>();
    private List<FriendModel> scimboFriendsEntries = new ArrayList<>();

    private View statusCircle;
    private Menu contactMenu;
    private ProgressBar pbLoader;
    private Context mContext;
    private View friendRequestContainer;

    private SwipeRefreshLayout swipeRefreshLayout;
    private int friendRequestsCount = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MyLog.d("onAttach: ");
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.nowletschat.android.contact_refresh");
        mContext.registerReceiver(contactsRefreshReceiver, intentFilter);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        userInfoSession = new UserInfoSession(mContext.getApplicationContext());
        ivUserProfile = view.findViewById(R.id.iv_main_profile_pic);
        tvUserName = view.findViewById(R.id.tv_main_title);
        tvUserPinCode = view.findViewById(R.id.tv_pin_code);
        View copyToClipBoard = view.findViewById(R.id.iv_copy_to_clipboard);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        lvContacts = view.findViewById(R.id.listContacts);
        tvContact = view.findViewById(R.id.tvContact);
        requestsRecyclerView = view.findViewById(R.id.rv_requests);
        backButton = view.findViewById(R.id.backarrow_contactsetting);
        backarrow = view.findViewById(R.id.backarrow);
        contact_empty = view.findViewById(R.id.contact_empty);
        ImageView overflow = view.findViewById(R.id.overflow);
        RelativeLayout overflowlayout = view.findViewById(R.id.overflowLayout);
        RelativeLayout contact1_RelativeLayout = view.findViewById(R.id.r1contact);
        View newGroup = view.findViewById(R.id.newGroup);
        //new_group_layout = (RelativeLayout) findViewById(R.id.new_group_layout);
        serach = view.findViewById(R.id.search);
        etSearch = view.findViewById(R.id.etSearch);
        selectcontact = view.findViewById(R.id.selectcontact);
        selectcontactmember = view.findViewById(R.id.selectcontactmember);
        friendRequestContainer = view.findViewById(R.id.rl_friend_request_header);
        View contactsContainer = view.findViewById(R.id.rl_contacts_header);
        ivDownArrowRequests = view.findViewById(R.id.iv_down_arrow_requests);
        ivDownArrowContacts = view.findViewById(R.id.iv_down_arrow_contacts);
        tvRequestsCount = view.findViewById(R.id.tv_requests_count);
        statusCircle = view.findViewById(R.id.status_circle);
        getcontactname = new Getcontactname(mContext);
        tvOnlineStatus = view.findViewById(R.id.tv_online_status);
        tvOnlineStatusLayout = view.findViewById(R.id.ll_user_status);
        View addNewContact = view.findViewById(R.id.rl_row_parent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                refreshContacts();
            }
        });

        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP && etSearch.getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                    if (event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etSearch.setText("");
                        return false;
                    }
                }
                return false;
            }
        });

        LinearLayoutManager mContactListManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        lvContacts.setLayoutManager(mContactListManager);
        lvContacts.setNestedScrollingEnabled(false);
        requestsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        requestsRecyclerView.setNestedScrollingEnabled(false);
//      lvContacts.addOnScrollListener(contactScrollListener);

        sessionManager = SessionManager.getInstance(mContext);
        uniqueCurrentID = sessionManager.getCurrentUserID();
        String username = sessionManager.getnameOfCurrentUser();
        final String pinCode = sessionManager.getPinCodeOfCurrentUser();
        profileImage = sessionManager.getUserProfilePic();
        tvUserName.setText(username);
        tvUserPinCode.setText(pinCode);
        AppUtils.loadImage(mContext, AppUtils.getValidProfilePath(profileImage), ivUserProfile, 150, R.drawable.ic_placeholder_black);
        ivUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ImageZoom.class);
                intent.putExtra("ProfilePath", AppUtils.getProfileFilePath(mContext));
                startActivity(intent);
            }
        });
        pbLoader = view.findViewById(R.id.pbLoader);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawableProgress = DrawableCompat.wrap(pbLoader.getIndeterminateDrawable());
            DrawableCompat.setTint(drawableProgress, ContextCompat.getColor(mContext, android.R.color.holo_green_light));
            pbLoader.setIndeterminateDrawable(DrawableCompat.unwrap(drawableProgress));
        } else {
            pbLoader.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mContext, R.color.white), PorterDuff.Mode.SRC_IN);
        }

        loadContactsFromDB();


        if (!SessionManager.getInstance(mContext).isContactSyncFinished()) {
            refreshContacts();
        }

        lvContacts.addOnItemTouchListener(new RItemAdapter(mContext, lvContacts, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FriendModel e = friendsAdapter.getItem(position);

                ChatLockPojo lockPojo = getChatLockdetailfromDB(position);
                if (sessionManager != null && sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {

                    String stat = "", pwd = null;
                    stat = lockPojo.getStatus();
                    pwd = lockPojo.getPassword();

                    String docID = e.get_id();
                    String documentid = uniqueCurrentID.concat("-").concat(docID);
                    if (stat.equals("1")) {
                        openUnlockChatDialog(documentid, stat, pwd, position);
                    } else {
                        //navigateToChatviewPage(e);
                        getcontactname.navigateToChatviewPageforScimboModel(e);
                    }
                } else {

//                    NewHomeScreenActivty newHomeScreenActivty = (NewHomeScreenActivty)getActivity();
//                    String sharedText = newHomeScreenActivty.sharedText;
//                    if (sharedText!=null && !sharedText.equals("")) {
//                        getcontactname.navigateToChatviewPageforScimboModel(e, sharedText);
//                        newHomeScreenActivty.sharedText = "";
//                    }else {
//                        getcontactname.navigateToChatviewPageforScimboModel(e);
//                    }

                    NewHomeScreenActivty newHomeScreenActivty = (NewHomeScreenActivty)getActivity();
                    String imageUri = newHomeScreenActivty.imageUri;
                    if (imageUri!=null && !imageUri.equals("")) {
                        getcontactname.navigateToChatviewPageforScimboModel(e, imageUri);
                        newHomeScreenActivty.imageUri = "";
                    }else {
                        getcontactname.navigateToChatviewPageforScimboModel(e);
                    }

                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        newGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, SelectPeopleForGroupChat.class);
                startActivity(i);
            }
        });

        serach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSearchActions();

                etSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                        if (friendsAdapter != null) {
                            if (cs.length() > 0) {
                                if (cs.length() == 1) {
                                    etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.cancel_normal, 0);
                                }
                                ContactsFragment.this.friendsAdapter.getFilter().filter(cs);
                            } else {
                            }
                        }

                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                        // TODO Auto-generated method stub
                    }
                });
                backarrow.setVisibility(View.VISIBLE);
                backButton.setVisibility(View.GONE);

                backarrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        etSearch.getText().clear();

                        etSearch.setVisibility(View.GONE);
                        serach.setVisibility(View.VISIBLE);
                        //overflowlayout.setVisibility(View.VISIBLE);
                        selectcontactmember.setVisibility(View.VISIBLE);
                        selectcontact.setVisibility(View.VISIBLE);
                        backarrow.setVisibility(View.GONE);
                        backButton.setVisibility(View.VISIBLE);
                        ((CoreActivity) mContext).hideKeyboard();
                    }
                });

                ((CoreActivity) mContext).showKeyboard();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
            }
        });
        friendRequestContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRecyclerViewVisibility(requestsRecyclerView, ivDownArrowRequests);
            }
        });
        contactsContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRecyclerViewVisibility(lvContacts, ivDownArrowContacts);
            }
        });
        copyToClipBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayList<BottomSheetModel> data = new ArrayList<>();
                data.add(new BottomSheetModel(getResources().getString(R.string.copy_pin), 0));
                data.add(new BottomSheetModel(getResources().getString(R.string.share_pin), 0));

                DialogUtils.showBottomSheetDialog(getContext(), "", data, new CallbackListener() {
                    @Override
                    public void callback(String result) {
                        if (result.equals(getResources().getString(R.string.copy_pin)))
                            MyChatUtils.copyToClipboard(mContext, tvUserPinCode.getText().toString());
                        if (result.equals(getResources().getString(R.string.share_pin))) {
                            String message = getResources().getString(R.string.deep_link_text) + " " + pinCode + "." + "\n \n"+BuildConfig.BASE_URL+"/?_branch_match_id=973897825708490612&utm_medium=marketing";
                            AppUtils.share(requireActivity(), message);
                        }

                    }
                });


            }
        });
        setOnlineStatus(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.ONLINE_STATUS.getValue(), AppConstants.UserStatus.ONLINE.getValue()));
        addNewContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(mContext, AddFriendActivity.class), AppConstants.RequestCodes.REQUEST_ADD_FRIEND.getCode());
            }
        });
        tvOnlineStatusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setOnlineStatus();
            }
        });

        //fetchContacts();
    }

    private void setOnlineStatus() {
        if (!StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.IS_TUTORIAL_FOR_STATUS_SEEN.getValue(), false)) {
            DialogUtils.showNativeDialog(mContext, getString(R.string.online_status), getString(R.string.online_status_tutorial), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    StorageUtility.saveDataInPreferences(mContext, AppConstants.SPKeys.IS_TUTORIAL_FOR_STATUS_SEEN.getValue(), true);
                    showDialogToChangeStatus();
                }
            }, 17);
            return;
        }
        showDialogToChangeStatus();
    }

    private void setOnlineStatus(String onlineStatus) {
        String status = MyChatUtils.parseStatusToLocal(mContext, onlineStatus);

        int color = R.color.dpBorderGreen, bg = R.drawable.online_status_circle;
        if (onlineStatus.equalsIgnoreCase("Online")) {
            color = R.color.dpBorderGreen;
            bg = R.drawable.online_status_circle;
        } else if (onlineStatus.equalsIgnoreCase("Busy")) {
            color = R.color.busy;
            bg = R.drawable.busy_status_circle;
        } else if (onlineStatus.equalsIgnoreCase("Offline")) {
            color = R.color.offline;
            bg = R.drawable.offline_status_circle;
        } else if (onlineStatus.equalsIgnoreCase("Away")) {
            color = R.color.away;
            bg = R.drawable.away_status_circle;
        }
        tvOnlineStatus.setTextColor(MyChatUtils.getColor(mContext, color));
        statusCircle.setBackgroundResource(bg);
        tvOnlineStatus.setText(status);
    }

    private void showDialogToChangeStatus() {
        List<MultiTextDialogPojo> labelsList = new ArrayList<>();
        MultiTextDialogPojo label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.ic_online_status);
        label.setLabelText(mContext.getString(R.string.online));
        labelsList.add(label);

        label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.ic_busy_status);
        label.setLabelText(mContext.getString(R.string.busy));
        labelsList.add(label);

        label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.ic_away_status);
        label.setLabelText(mContext.getString(R.string.away));
        labelsList.add(label);

        label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.ic_offline_status);
        label.setLabelText(mContext.getString(R.string.offline));
        labelsList.add(label);

        CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
        dialog.setTitleText(getString(R.string.online_status));
        dialog.setLabelsList(labelsList);

        dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
            @Override
            public void onDialogItemClick(int position) {
                String status = "";
                int color = R.color.dpBorderGreen, bg = R.drawable.online_status_circle;
                switch (position) {
                    case 0:
                        status = "Online";
                        break;
                    case 1:
                        color = R.color.busy;
                        bg = R.drawable.busy_status_circle;
                        status = "Busy";
                        break;
                    case 2:
                        color = R.color.away;
                        bg = R.drawable.away_status_circle;
                        status = "Away";
                        break;
                    case 3:
                        color = R.color.offline;
                        bg = R.drawable.offline_status_circle;
                        status = "Offline";
                        break;
                }
                tvOnlineStatus.setTextColor(MyChatUtils.getColor(mContext, color));
                statusCircle.setBackgroundResource(bg);
                tvOnlineStatus.setText(MyChatUtils.parseStatusToLocal(mContext, status));
                StorageUtility.saveDataInPreferences(mContext, AppConstants.SPKeys.ONLINE_STATUS.getValue(), status);
                emitOnlineStatusEvent(status);
            }
        });
        dialog.show(((CoreActivity) mContext).getSupportFragmentManager(), "Online Status");
    }

    private void emitOnlineStatusEvent(String status) {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_USER_ONLINE_STATUS);
        String statusForServer = MyChatUtils.parseStatusForServer(mContext, status);
        JSONObject obj = new JSONObject();
        try {
            obj.put("onlineStatus", statusForServer);
            obj.put("userId", SessionManager.getInstance(mContext).getCurrentUserID());
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        MyLog.e(TAG, "emitOnlineStatusEvent : " + obj);
        event.setMessageObject(obj);
        EventBus.getDefault().post(event);
    }

    private void convertFriendsListToHeaderList(List<FriendModel> friendModels) {
        convertFriendsListToHeaderList(friendModels, true);
    }

    private void convertFriendsListToHeaderList(List<FriendModel> friendModels, boolean addContact) {
        if (friendModels == null)
            return;
        friendRequestsCount = 0;
        String keyFriends = mContext.getString(R.string.friends);
        String keyRequests = mContext.getString(R.string.requests);
        HashMap<String, ArrayList<FriendModel>> map = new HashMap<>();
        map.put(keyRequests, new ArrayList<FriendModel>());
        map.put(keyFriends, new ArrayList<FriendModel>());

        for (FriendModel friendModel : friendModels) {
            String status = friendModel.getRequestStatus();
            if (TextUtils.isEmpty(status))
                continue;
            String key = "";
            if (status.equals(AppConstants.FriendStatus.DELETED.getValue()))
                continue;
            else if (status.equals(AppConstants.FriendStatus.FRIENDS.getValue()) || status.equals(AppConstants.FriendStatus.DISABLED.getValue())) {
                key = keyFriends;
            } else if (status.equals(AppConstants.FriendStatus.REQUEST_SENT.getValue()) || status.equals(AppConstants.FriendStatus.REQUEST_RECEIVED.getValue())) {
                key = keyRequests;
            }
            if (addContact && status.equals(AppConstants.FriendStatus.REQUEST_RECEIVED.getValue()))
                friendRequestsCount++;

            ArrayList<FriendModel> models = map.get(key);
            if (models == null)
                models = new ArrayList<>();
            models.add(friendModel);
        }

        ArrayList<FriendModel> requests = map.get(keyRequests);
        ArrayList<FriendModel> friends = map.get(keyFriends);
        if (requests == null) requests = new ArrayList<>();
        if (friends == null) friends = new ArrayList<>();

        this.scimboRequestsEntries = requests;
        this.scimboFriendsEntries = friends;
    }

    private ArrayList<FriendModel> sortFriendsList(ArrayList<FriendModel> friendModels) {
        if (friendModels == null)
            friendModels = new ArrayList<>();
        Collections.sort(friendModels, new Comparator<FriendModel>() {
            @Override
            public int compare(FriendModel o1, FriendModel o2) {
                return o1.getFirstName().toLowerCase().compareTo(o2.getFirstName().toLowerCase());
            }
        });
        return friendModels;
    }

    private void setRecyclerViewVisibility(RecyclerView recyclerView, ImageView downArrow) {
        if (recyclerView.getVisibility() == View.VISIBLE) {
            recyclerView.setVisibility(View.GONE);
            downArrow.setImageResource(R.drawable.ic_right_arrow_white);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            downArrow.setImageResource(R.drawable.white_arrow_down);
        }
    }

    private void fetchContacts() {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_FRIEND_LIST);
        JSONObject obj = new JSONObject();
        try {
            Log.d(TAG, "Contact list response: " + obj.toString());
            obj.put("userId", SessionManager.getInstance(mContext).getCurrentUserID());
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        MyLog.e(TAG, "fetchContacts : " + obj);
        event.setMessageObject(obj);
        EventBus.getDefault().post(event);
    }

    private long lastUpdatedMillis = 0;

    private <E> boolean isDuplicateList(final List<E> l1, final List<E> l2) {
        final Set<E> set = new HashSet<>(l1);
        final Set<E> set2 = new HashSet<>(l2);
        return l1.size() == l2.size() && set.containsAll(set2);
    }

    private void loadContactsFromDB() {
        long currentMillis = System.currentTimeMillis();
        long diffMillis = currentMillis - lastUpdatedMillis;

        if (isRefreshRequested || (diffMillis > 20000)) {
            isRefreshRequested = false;
            lastUpdatedMillis = System.currentTimeMillis();
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
            List<FriendModel> updatedContactList = contactDB_sqlite.getSavedFriendModels();
            Collections.sort(updatedContactList, Getcontactname.nameAsComparator);
            Log.d(TAG, "loadContactsFromDB: start");
            List<FriendModel> combinedList = new ArrayList<>();
            combinedList.addAll(scimboRequestsEntries);
            combinedList.addAll(scimboFriendsEntries);
            if (isDuplicateList(updatedContactList, combinedList) && (requestsAdapter != null || friendsAdapter != null)) {
                Log.d(TAG, "loadContactsFromDB: no need refresh");
                return;
            } else {
                Log.d(TAG, "loadContactsFromDB: need refresh");
                Log.d(TAG, "isDuplicateList" + isDuplicateList(updatedContactList, combinedList));
            }
            convertFriendsListToHeaderList(updatedContactList);
            setupAdapter();
        }
    }

    private void setupAdapterRequests() {
        MyLog.d(TAG, "ContactsTest loadContactsFromDB: size " + scimboRequestsEntries.size());
        Collections.sort(scimboRequestsEntries, Getcontactname.nameAsComparator);
        if (scimboRequestsEntries.size() > 0){
            friendRequestContainer.setVisibility(View.VISIBLE);
        }else{
            friendRequestContainer.setVisibility(View.GONE);
        }
        if (requestsAdapter == null && scimboRequestsEntries.size() > 0) {
            ArrayList<FriendModel> requestsAdapterCopy = new ArrayList<>();
            for (FriendModel requests : scimboRequestsEntries) {
                requestsAdapterCopy.add(requests);
            }
            requestsAdapter = new FriendAdapter(mContext, requestsAdapterCopy);
            requestsRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 100);
            requestsAdapter.setHasStableIds(true);
            requestsRecyclerView.setAdapter(requestsAdapter);
        } else if (requestsAdapter != null){
            ArrayList<FriendModel> requestsAdapterCopy = new ArrayList<>();
            for (FriendModel requests : scimboRequestsEntries) {
                requestsAdapterCopy.add(requests);
            }
            requestsAdapter.updateAdapterList(requestsAdapterCopy);
        }

        contact_empty.setVisibility(View.GONE);
        selectcontactmember.setText(scimboRequestsEntries.size() + " " + getString(R.string.Contacts));

        if (refreshcontactsset) {
            Toast.makeText(mContext, getString(R.string.your_contact_list_has_been_updated), Toast.LENGTH_SHORT).show();
            refreshcontactsset = false;
        }

    }

    private void setupAdapter() {
        showRequestCount();
        setupAdapterRequests();
        setupAdapterFriends();
    }

    private void setupAdapterFriends() {
        MyLog.d(TAG, "ContactsTest loadContactsFromDB: size " + scimboFriendsEntries.size());
        Collections.sort(scimboFriendsEntries, Getcontactname.nameAsComparator);

        if (friendsAdapter == null && scimboFriendsEntries.size() > 0) {
            ArrayList<FriendModel> friendsAdapterCopy = new ArrayList<>();
            for (FriendModel friends : scimboFriendsEntries) {
                friendsAdapterCopy.add(friends);
            }
            friendsAdapter = new FriendAdapter(mContext, friendsAdapterCopy);
            lvContacts.getRecycledViewPool().setMaxRecycledViews(0, 100);
            friendsAdapter.setHasStableIds(true);
            lvContacts.setAdapter(friendsAdapter);
        } else if (friendsAdapter != null){
            ArrayList<FriendModel> friendsAdapterCopy = new ArrayList<>();
            for (FriendModel friends : scimboFriendsEntries) {
                friendsAdapterCopy.add(friends);
            }
            friendsAdapter.updateAdapterList(friendsAdapterCopy);
        }

        lvContacts.setVisibility(View.VISIBLE);
        contact_empty.setVisibility(View.GONE);
        selectcontactmember.setText(scimboFriendsEntries.size() + " " + getString(R.string.Contacts));
        tvContact.setText(getString(R.string.contacts) + "(" + scimboFriendsEntries.size() + ")");
        if (refreshcontactsset) {
            Toast.makeText(mContext, getString(R.string.your_contact_list_has_been_updated), Toast.LENGTH_SHORT).show();
            refreshcontactsset = false;
        }
    }

    private void handleFriendRequestsCountFromStatus(String friendStatus, int toAdd, int toRemove) {
//        friendRequestsCount += toAdd;
//        if (friendRequestsCount > 0  && toRemove > 0)
//            friendRequestsCount -= toRemove;
        if (scimboRequestsEntries == null)
            scimboRequestsEntries = new ArrayList<>();
        friendRequestsCount = scimboRequestsEntries.size();
        showRequestCount();
    }


    private void showRequestCount() {
        if (friendRequestsCount == 0)
            tvRequestsCount.setVisibility(View.GONE);
        else
            tvRequestsCount.setVisibility(View.VISIBLE);
        tvRequestsCount.setText(String.valueOf(friendRequestsCount));
        //((MainActivity) context).setUnreadCountOnTab(AppConstants.Tab.CONTACTS.getValue(), friendRequestsCount);
    }

    private void updateFriendProfileImage(ReceviceMessageEvent event) {

        Object[] array = event.getObjectsArray();
        String path = "";
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            Log.e(TAG, "objects" + objects);
            String from = objects.getString("from");
            String type = objects.getString("type");

            String removePhoto = null;
            if (objects.has("removePhoto")) {
                removePhoto = objects.getString("removePhoto");
            }

            if (from.equalsIgnoreCase(SessionManager.getInstance(mContext).getCurrentUserID()) && type.equalsIgnoreCase("single")) {
                if (!objects.getString("file").equals("") || removePhoto != null) {
                    path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                    final String finalPath = AppUtils.getValidProfilePath(path);
                    if (removePhoto.equalsIgnoreCase("yes")) {
                        SessionManager.getInstance(mContext).setUserProfilePic("");
                        ivUserProfile.setImageResource(R.drawable.ic_placeholder_black);
                    } else {
                        SessionManager.getInstance(mContext).setUserProfilePic(finalPath);
                        Glide
                                .with(mContext)
                                .load(/*profilePicPath*/AppUtils.getGlideURL(finalPath, mContext))
                                .asBitmap()
                                .error(R.drawable.ic_placeholder_black)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .dontAnimate()
                                .into(new SimpleTarget<Bitmap>() {

                                    @Override
                                    public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                        ivUserProfile.setImageBitmap(arg0);
                                    }
                                });
                    }
                }
            } else {
                Object[] array1 = event.getObjectsArray();
                try {
                    JSONObject objects1 = new JSONObject(array1[0].toString());
                    String from1 = objects1.getString("from");
                    String type1 = objects1.getString("type");
                    if (type1.equalsIgnoreCase("single")) {
                        boolean isUpdated = updateProfileImageInList(scimboRequestsEntries, requestsAdapter, from1);
                        if (!isUpdated)
                            updateProfileImageInList(scimboFriendsEntries, friendsAdapter, from1);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "updateProfileImage: ", e);
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    private boolean updateProfileImageInList(List<FriendModel> list, FriendAdapter adapter, String from) {
        int index = 0;
        for (FriendModel contactModel : list) {
            if (contactModel.get_id().equalsIgnoreCase(from)) {
                if (contactModel.get_id().equalsIgnoreCase(from)) {
                    adapter.notifyItemChanged(index);
                    return true;
                }
            }
            index++;
        }
        return false;
    }


    private void showSearchActions() {

        backarrow.setVisibility(View.VISIBLE);
        serach.setVisibility(View.GONE);
        backButton.setVisibility(View.GONE);
        selectcontact.setVisibility(View.GONE);
        selectcontactmember.setVisibility(View.GONE);
        //contact1_RelativeLayout.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        contactMenu = menu;
        inflater.inflate(R.menu.new_chat, menu);

        MenuItem searchItem = menu.findItem(R.id.contacts_searchIcon);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.equals("") && query.isEmpty()) {
                    searchView.clearFocus();
                }
                if (requestsAdapter != null) {
                    requestsAdapter.getFilter().filter(query);
                }else{
                    setupAdapterRequests();
                }
                friendsAdapter.getFilter().filter(query);
                if (friendsAdapter != null) {
                    friendsAdapter.getFilter().filter(query);
                }
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.equals("") && newText.isEmpty()) {
                    searchView.clearFocus();
                }

                if (requestsAdapter != null) {
                    requestsAdapter.getFilter().filter(newText);
                }

                if (friendsAdapter != null) {
                    friendsAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });

        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
        try {
            searchTextView.setTextColor(getResources().getColor(R.color.white));
            searchTextView.setHintTextColor(getResources().getColor(R.color.white));
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ((CoreActivity) mContext).hideKeyboard();
        switch (item.getItemId()) {
            case R.id.contacts_searchIcon:

                break;
            case R.id.menuinvitefriends:
                AppUtils.shareApp(mContext);
                break;

            case R.id.menucontacts:
                /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivity(intent);*/
                String data = "content://contacts/people/";
                Intent contactIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
                startActivity(contactIntent);
                break;

            case R.id.menurefresh:
                if (isNetworkConnected()) {
                    refreshContacts();
                    refreshcontactsset = true;
                } else {
                    Toast.makeText(mContext, getResources().getString(R.string.check_your_internet_connection), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.menuaboutHelp:
                ActivityLauncher.launchAbouthelp((Activity) mContext);
                break;

            case R.id.chats_contactIcon:
                startActivityForResult(new Intent(mContext, AddFriendActivity.class), AppConstants.RequestCodes.REQUEST_ADD_FRIEND.getCode());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private boolean isRefreshRequested = false;

    private void refreshContacts() {
        isRefreshRequested = true;
        MyLog.d(TAG, "ContactsTest refreshContacts CLICK: ");
        pbLoader.setVisibility(View.VISIBLE);

        ScimboContactsService.bindContactService(mContext, true);
    }


    private void openUnlockChatDialog(String docId, String status, String pwd, int position) {
        String convId = userInfoSession.getChatConvId(docId);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1(getResources().getString(R.string.enter_your_pwd_label));
        dialog.setEditTextdata(getResources().getString(R.string.hint_newPwd));
        dialog.setforgotpwdlabel(getResources().getString(R.string.forgotChatpwd));
        dialog.setHeader(getResources().getString(R.string.unlock_chat));
        dialog.setButtonText(getResources().getString(R.string.unlock));
        Bundle bundle = new Bundle();
        bundle.putSerializable("socketitems", friendsAdapter.getItem(position));
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "single");
        bundle.putString("from", uniqueCurrentID);
        dialog.setArguments(bundle);
        dialog.show(getChildFragmentManager(), "chatunLock");
    }

    private ChatLockPojo getChatLockdetailfromDB(int position) {

        FriendModel e = friendsAdapter.getItem(position);
        if (e == null)
            return null;
        String docID = e.get_id();
        String id = uniqueCurrentID.concat("-").concat(docID);
        MessageDbController dbController = CoreController.getDBInstance(mContext);
        String convId = userInfoSession.getChatConvId(id);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo pojo = dbController.getChatLockData(receiverId, MessageFactory.CHAT_TYPE_SINGLE);
        return pojo;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        String eventName = event.getEventName();
        JSONObject jsonObject = new JSONObject();
        try {
            Object[] response = event.getObjectsArray();
            if (response.length > 0)
                jsonObject = (JSONObject) response[0];
            MyLog.d(TAG, eventName + "///" + "response in contacts is : " + jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String currentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {

        } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_GET_USER_DETAILS)) {
//            loadUserDetails(event.getObjectsArray()[0].toString());
        } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_FRIEND_LIST)) {
            try {
                int error = jsonObject.getInt("err");
                if (error == 0) {
                    JSONArray friendsArray = jsonObject.getJSONArray("friends");
                    List<FriendModel> scimboEntries = FriendModel.parseJSON(friendsArray);

                    Log.d(TAG, "Friends List: " + friendsArray.toString());

                    convertFriendsListToHeaderList(scimboEntries);
                    setupAdapter();
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                    //TODO instead of removing all , only remove those which are not in friend list
                    contactDB_sqlite.removeAllFriends();
                    for (FriendModel friendModel : scimboEntries) {
                        contactDB_sqlite.updateUserDetails(friendModel.get_id(), friendModel);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_ACCEPT_FRIEND_REQUEST)) {

            try {
                FriendModel friendModel = FriendModel.parseJSON(jsonObject.getJSONObject("friends"));
                String friendId = friendModel.get_id();
                int error = jsonObject.getInt("err");
                String message = jsonObject.getString("message");
                if (error != 0) {
                    CoreActivity.showToast(mContext, message);
                    return;
                }
                if (friendId.equals(currentUserId))
                    return;
                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                contactDB_sqlite.updateUserDetails(friendModel.get_id(), friendModel);
                removeModelFromList(scimboRequestsEntries, requestsAdapter, friendModel, true);
                addModelToList(scimboFriendsEntries, friendsAdapter, friendModel, false);
                handleFriendRequestsCountFromStatus(friendModel.getRequestStatus(), 0, 1);
                if (scimboRequestsEntries.size() > 0){
                    friendRequestContainer.setVisibility(View.VISIBLE);
                }else{
                    friendRequestContainer.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_DELETE_FRIEND_REQUEST)) {
            try {
                FriendModel friendModel = FriendModel.parseJSON(jsonObject.getJSONObject("friends"));
                int error = jsonObject.getInt("err");
                String message = jsonObject.getString("message");
                String friendId = friendModel.get_id();
                if (error != 0) {
                    CoreActivity.showToast(mContext, message);
                    return;
                }
                if (friendId.equals(currentUserId))
                    return;
                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                contactDB_sqlite.updateUserDetails(friendModel.get_id(), friendModel);
                removeModelFromList(scimboRequestsEntries, requestsAdapter, friendModel, true);
                handleFriendRequestsCountFromStatus(friendModel.getRequestStatus(), 0, 1);
                if (scimboRequestsEntries.size() > 0){
                    friendRequestContainer.setVisibility(View.VISIBLE);
                }else{
                    friendRequestContainer.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_SEND_FRIEND_REQUEST)) {
            try {
                FriendModel friendModel = FriendModel.parseJSON(jsonObject.getJSONObject("friends"));
                int error = jsonObject.getInt("err");
                String message = jsonObject.getString("message");
                String friendId = friendModel.get_id();
                if (error != 0) {
                    CoreActivity.showToast(mContext, message);
                    return;
                }
                if (friendId.equals(currentUserId)) {

                    return;
                }
                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                contactDB_sqlite.updateUserDetails(friendModel.get_id(), friendModel);
                removeModelFromList(scimboFriendsEntries, friendsAdapter, friendModel, false);
                addModelToList(scimboRequestsEntries, requestsAdapter, friendModel, true);
                if (friendModel.getRequestStatus().equals(AppConstants.FriendStatus.REQUEST_RECEIVED.getValue()))
                    handleFriendRequestsCountFromStatus(friendModel.getStatus(), 1, 0);
                if (scimboRequestsEntries.size() > 0){
                    friendRequestContainer.setVisibility(View.VISIBLE);
                    setRecyclerViewVisibility(requestsRecyclerView, ivDownArrowRequests);
                }else{
                    friendRequestContainer.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_UN_FRIEND_REQUEST)) {
            try {
                FriendModel friendModel = FriendModel.parseJSON(jsonObject.getJSONObject("friends"));
                int error = jsonObject.getInt("err");
                String friendId = friendModel.get_id();
                if (error != 0) {
                    return;
                }
                if (friendId.equals(currentUserId))
                    return;

                removeModelFromList(scimboFriendsEntries, friendsAdapter, friendModel, false);
                removeModelFromList(scimboRequestsEntries, requestsAdapter, friendModel, true);
                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                contactDB_sqlite.updateUserDetails(friendModel.get_id(), friendModel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (eventName.equalsIgnoreCase(SocketManager.EVENT_REAL_TIME_ONLINE_STATUS)) {
            try {
                String userId = "", status = "";
                if (jsonObject.has("userId"))
                    userId = jsonObject.getString("userId");
                if (jsonObject.has("Status"))
                    status = jsonObject.getString("Status");
                if (TextUtils.isEmpty(userId) || currentUserId.equalsIgnoreCase(userId))
                    return;
//                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
//                contactDB_sqlite.updateOnlineStatus(userId, status);
                updateFriendModel(userId, status);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_USER_NAME)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String err = object.getString("err");

                if (err.equals("0")) {

                    String from = object.getString("from");
                    String name = object.getString("name");
                    String pinCode = object.getString("pinCode");
                    String mCurrentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
                    if (!from.equals(mCurrentUserId)) {
                        FriendModel friendModel = CoreController.getContactSqliteDBintstance(mContext).getFriendDetails(from);
                        if (friendModel != null) {
                            String friendPinCode = friendModel.getPinCode();
                            if (!TextUtils.isEmpty(friendPinCode) && !friendPinCode.equals(pinCode)) {
                                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                                contactDB_sqlite.updateUserDetails(friendModel.get_id(), friendModel);
                                removeModelFromList(scimboRequestsEntries, requestsAdapter, friendModel, true);
                                removeModelFromList(scimboFriendsEntries, friendsAdapter, friendModel, false);
                            }
                        }
                    } else {
                        tvUserName.setText(name);
                        tvUserPinCode.setText(pinCode);
                    }
                    refreshContacts();
                }
            } catch (JSONException ex) {
                CoreActivity.showToast(mContext, getString(R.string.call_fail_error));
                ex.printStackTrace();
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            updateFriendProfileImage(event);
        }
    }

    private void updateFriendModel(String userId, String status) {
        int index = getFriendModelFromList(scimboRequestsEntries, userId);
        if (index > -1) {
            updateListAndAdapter(index, status, scimboRequestsEntries, requestsAdapter);
            return;
        }
        index = getFriendModelFromList(scimboFriendsEntries, userId);
        if (index > -1) {
            updateListAndAdapter(index, status, scimboFriendsEntries, friendsAdapter);
        }
    }

    private int getFriendModelFromList(List<FriendModel> list, String userId) {
        int index = 0;
        for (FriendModel model : list) {
            String friendId = model.get_id();
            if (friendId.equalsIgnoreCase(userId)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    private void updateListAndAdapter(int index, String status, List<FriendModel> models, FriendAdapter adapter) {
        FriendModel friendModel = adapter.getItem(index);
        if (friendModel == null)
            return;
        friendModel.setOnlineStatus(status);

        adapter.notifyItemChanged(index);
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
        contactDB_sqlite.updateUserDetails(friendModel.get_id(), friendModel);
    }

    private void removeModelFromList(List<FriendModel> list, FriendAdapter adapter, FriendModel friendModel, boolean removingFriendRequest) {
        if (list == null || list.size() == 0)
            return;
        Iterator<FriendModel> iterator = list.iterator();
        while (iterator.hasNext()) {
            FriendModel model = iterator.next();
            if (model.get_id().equalsIgnoreCase(friendModel.get_id())) {
                iterator.remove();
                adapter.removeFriend(model);
                adapter.notifyDataSetChanged();
//                adapter.updateAdapterList(list);
                setRvVisibilityWithSize();
                if (!removingFriendRequest)
                    tvContact.setText("Contacts" + "(" + scimboFriendsEntries.size() + ")");
                return;
            }
        }
    }

    private void setRvVisibilityWithSize() {
        if (scimboRequestsEntries.size() == 0)
            requestsRecyclerView.setVisibility(View.GONE);
        if (scimboFriendsEntries.size() == 0)
            lvContacts.setVisibility(View.GONE);
    }

    private void addModelToList(List<FriendModel> list, FriendAdapter adapter, FriendModel friendModel, boolean addingFriendRequest) {
        if (list == null)
            return;
        int index = 0;
        for (FriendModel model : list) {
            if (model.get_id().equalsIgnoreCase(friendModel.get_id())) {
                list.set(index, friendModel);
                adapter.notifyItemChanged(index);
                return;
            }
            index++;
        }

        list.add(friendModel);
        if (!addingFriendRequest)
            tvContact.setText("Contacts " + "(" + scimboFriendsEntries.size() + ")");

        Collections.sort(list, Getcontactname.nameAsComparator);
        if (adapter == null) {
            setupAdapter();
        } else
            adapter.updateAdapterList(list);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((CoreActivity) mContext).hideKeyboard();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((CoreActivity) mContext).hideKeyboard();
        mContext.unregisterReceiver(contactsRefreshReceiver);
        EventBus.getDefault().unregister(this);
    }

    private ContactsRefreshReceiver contactsRefreshReceiver = new ContactsRefreshReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            pbLoader.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            loadContactsFromDB();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.RequestCodes.REQUEST_ADD_FRIEND.getCode()) {
            expandAddFriendView();
        }
    }

    private void expandAddFriendView() {
        requestsRecyclerView.setVisibility(View.VISIBLE);
        ivDownArrowRequests.setImageResource(R.drawable.white_arrow_down);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sessionManager != null) {
            tvUserPinCode.setText(sessionManager.getPinCodeOfCurrentUser());
        }
        tvUserName.setText(SessionManager.getInstance(mContext).getnameOfCurrentUser());
    }

}
