package com.chat.android.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.chat.android.app.activity.NewHomeScreenActivty;
import com.chat.android.core.ActivityLauncher;

public class Dialer extends BroadcastReceiver {
    private static final String TAG = "Dialer";
    @Override
    public void onReceive(Context context, Intent intent) {
        //ActivityLauncher.launchHomeScreen(context);
        Log.d(TAG, "onReceive: ");
        Intent intent1=new Intent(context, NewHomeScreenActivty.class);
        context.startActivity(intent1);
    }
}
