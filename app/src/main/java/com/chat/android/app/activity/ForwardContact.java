package com.chat.android.app.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.adapter.ForwardContactAdapter;
import com.chat.android.app.adapter.RItemAdapter;
import com.chat.android.app.utils.ChatListUtil;
import com.chat.android.app.utils.CommonData;
import com.chat.android.app.utils.ForwardFileUtil;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.GroupInfoSession;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.AudioMessage;
import com.chat.android.core.message.ContactMessage;
import com.chat.android.core.message.DocumentMessage;
import com.chat.android.core.message.LocationMessage;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.message.PictureMessage;
import com.chat.android.core.message.TextMessage;
import com.chat.android.core.message.VideoMessage;
import com.chat.android.core.message.WebLinkMessage;
import com.chat.android.core.model.GroupInfoPojo;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.core.uploadtoserver.FileUploadDownloadManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.TextUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 */
public class ForwardContact extends CoreActivity {
    private static final String TAG = "ForwardContact";
    RecyclerView lvContacts, rvFreqContact;
    ForwardContactAdapter adapter, frequentAdapter;
    InputMethodManager inputMethodManager;
    ProgressDialog dialog;
    TextView tvFrequentLbl;
    TextView resevernameforward;
    private List<FriendModel> selectedContactsList;
    private ArrayList<FriendModel> dataList;
    ImageView sendmessage;
    private FileUploadDownloadManager uploadDownloadManager;
    Session session;
    GroupInfoSession groupInfoSession;
    private SearchView searchView;
    RelativeLayout Sendlayout;
    String mCurrentUserId, textMsgFromVendor;
    private boolean forwardFromScimbo;
    private String contact;
    List<MessageItemChat> aSelectedMessageInfo;
    ArrayList<FriendModel> scimboEntries;
    ArrayList<FriendModel> frequentList = new ArrayList<>();
    ArrayList<FriendModel> grouplist = new ArrayList<>();

    ArrayList<String> userIdList = new ArrayList<>();
    Getcontactname getcontactname;
    ContactDB_Sqlite contactDB_sqlite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forward_with_freq_contact);
        contactDB_sqlite = CoreController.getContactSqliteDBintstance(ForwardContact.this);
        sendmessage = findViewById(R.id.overlapImage);
        resevernameforward = findViewById(R.id.chat_text_view);
        uploadDownloadManager = new FileUploadDownloadManager(ForwardContact.this);
        groupInfoSession=new GroupInfoSession(this);
        getcontactname = new Getcontactname(ForwardContact.this);
        mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();
        session = new Session(ForwardContact.this);
        setTitle(getString(R.string.forward));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        lvContacts = findViewById(R.id.listContacts);
        LinearLayoutManager mediaManager = new LinearLayoutManager(ForwardContact.this, RecyclerView.VERTICAL, false);
        lvContacts.setLayoutManager(mediaManager);

        rvFreqContact = findViewById(R.id.rvFreqContact);
        LinearLayoutManager freqManager = new LinearLayoutManager(ForwardContact.this, RecyclerView.VERTICAL, false);
        rvFreqContact.setLayoutManager(freqManager);

        tvFrequentLbl = findViewById(R.id.tvFrequentLbl);

        List<String> lists = session.getBlockedIds();

        final ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

        scimboEntries = contactDB_sqlite.getSavedFriendModels();
        filterContactsList(scimboEntries);
        List<FriendModel> frequentContact = contactDB_sqlite.getFrequentContacts(this, mCurrentUserId);
        List<FriendModel> frequentGroup = contactDB_sqlite.getFrequentGroups(this, mCurrentUserId);

        //grouplist
        grouplist= ChatListUtil.getGroupList(this);

//

        if (frequentContact.size() > 0) {
            tvFrequentLbl.setVisibility(View.VISIBLE);
            for (FriendModel data : frequentContact) {
                frequentList.add(data);
                userIdList.add(data.get_id());
            }

        }

        if (frequentGroup.size() > 0) {
            tvFrequentLbl.setVisibility(View.VISIBLE);
            for (FriendModel data : frequentGroup) {
                String docId= mCurrentUserId+"-"+data.get_id()+"-g";
                data.setGroupDocID(docId);
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
                if(infoPojo.isLiveGroup()) {
                    Log.d(TAG, "onCreate isLiveGroup: " + infoPojo.isLiveGroup());
                    frequentList.add(data);
                    userIdList.add(data.get_id());
                }
            }

        }

        frequentAdapter = new ForwardContactAdapter(this, frequentList);
        rvFreqContact.setAdapter(frequentAdapter);

        dataList = new ArrayList<>();
        if (lists != null && lists.size() != 0) {
            for (FriendModel contact : scimboEntries) {
                if (!lists.contains(contact.get_id()) && !userIdList.contains(contact.get_id())) {
                    if(!contact.getFirstName().isEmpty())
                        dataList.add(contact);
                }
            }
            //
            for (FriendModel contact : grouplist) {
                if (!lists.contains(contact.get_id()) && !userIdList.contains(contact.get_id())) {
                    if(!contact.getFirstName().isEmpty())
                        dataList.add(contact);
                }
            }
            //

        } else {
            for (FriendModel contact : scimboEntries) {
                if (!userIdList.contains(contact.get_id())) {
                    if(!contact.getFirstName().isEmpty())
                        dataList.add(contact);
                }
            }
            //
            for (FriendModel contact : grouplist) {
                if (!userIdList.contains(contact.get_id())) {
                    if(!contact.getFirstName().isEmpty()) {
                        contact.setGroup(true);
                        dataList.add(contact);
                    }
                }
            }
            //
        }

        List<FriendModel> updatedContactList = contactDB_sqlite.getSavedFriendModels();
        Collections.sort(updatedContactList, Getcontactname.nameAsComparator);

        Collections.sort(dataList, Getcontactname.nameAscComparator);
        adapter = new ForwardContactAdapter(ForwardContact.this, dataList);
        lvContacts.setAdapter(adapter);


        lvContacts.addOnItemTouchListener(new RItemAdapter(this, lvContacts, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position > -1) {
                    int index = dataList.indexOf(adapter.getItem(position));
                    FriendModel userData = dataList.get(index);
                    userData.setSelected(!userData.isSelected());
                    if (contactDB_sqlite.getBlockedStatus(userData.get_id(), false).equals("0")) {
                        if (userData.isSelected()) {
                            selectedContactsList.add(userData);
                        } else {
                            selectedContactsList.remove(userData);
                        }
                    } else {
                        toast(getResources().getString(R.string.first_unblock_to_select_this_contact));
                    }


                    dataList.set(index, userData);
                    adapter.notifyDataSetChanged();

                    if (selectedContactsList.size() == 0) {
                        Sendlayout.setVisibility(View.GONE);
                        sendmessage.setVisibility(View.GONE);
                    } else {
                        int visibility = Sendlayout.getVisibility();
                        if (visibility != View.VISIBLE) {
                            Sendlayout.setVisibility(View.VISIBLE);
                            sendmessage.setVisibility(View.VISIBLE);
                            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_up);
                            Sendlayout.setAnimation(animation);
                        }

                        StringBuilder sb = new StringBuilder();
                        int nameIndex = 0;
                        for (FriendModel contact : selectedContactsList) {
                            sb.append(contact.getFirstName());
                            nameIndex++;
                            if (selectedContactsList.size() > nameIndex) {
                                sb.append(", ");
                            }
                        }

                        resevernameforward.setText(sb);

                    }
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        rvFreqContact.addOnItemTouchListener(new RItemAdapter(this, rvFreqContact, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position > -1) {
                    int index = frequentList.indexOf(frequentAdapter.getItem(position));
                    FriendModel userData = frequentList.get(index);
                    userData.setSelected(!userData.isSelected());
                    if (contactDB_sqlite.getBlockedStatus(userData.get_id(), false).equals("0")) {
                        if (userData.isSelected()) {
                            selectedContactsList.add(userData);
                        } else {
                            selectedContactsList.remove(userData);
                        }
                    } else {
                        toast(getResources().getString(R.string.first_unblock_to_select_this_contact));
                    }

                    frequentList.set(index, userData);
                    frequentAdapter.notifyDataSetChanged();

                    if (selectedContactsList.size() == 0) {
                        Sendlayout.setVisibility(View.GONE);
                        sendmessage.setVisibility(View.GONE);
                    } else {

                        int visibility = Sendlayout.getVisibility();
                        if (visibility != View.VISIBLE) {
                            Sendlayout.setVisibility(View.VISIBLE);
                            sendmessage.setVisibility(View.VISIBLE);
                            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_up);
                            Sendlayout.setAnimation(animation);
                        }

                        StringBuilder sb = new StringBuilder();
                        int nameIndex = 0;
                        for (FriendModel contact : selectedContactsList) {
                            sb.append(contact.getFirstName());
                            nameIndex++;
                            if (selectedContactsList.size() > nameIndex) {
                                sb.append(", ");
                            }
                        }

                        resevernameforward.setText(sb);

                    }
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
        selectedContactsList = new ArrayList<>();

        final Intent intent = getIntent();

        String shareAction = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(shareAction) && type != null) {
            if ("text/plain".equals(type)) {
                textMsgFromVendor = intent.getExtras().getString(Intent.EXTRA_TEXT, "");
            }
        }

        Sendlayout = findViewById(R.id.sendlayout);

        forwardFromScimbo = getIntent().getBooleanExtra("FromScimbo", false);

        if (getIntent() != null) {
            //   aSelectedMessageInfo = (List<MessageItemChat>) intent.getSerializableExtra("MsgItemList");
            aSelectedMessageInfo = CommonData.forwardedItems;
            MyLog.e("aSelectedMessageInfo", "aSelectedMessageInfo" + aSelectedMessageInfo.size());
        }

        sendmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int msgIndex = 0; msgIndex < aSelectedMessageInfo.size(); msgIndex++) {

                    MessageItemChat msgItem = aSelectedMessageInfo.get(msgIndex);

                    switch (msgItem.getMessageType()) {

                        case (MessageFactory.text + ""):

                            for (int contactIndex = 0; contactIndex < selectedContactsList.size(); contactIndex++) {
                                FriendModel userData = selectedContactsList.get(contactIndex);

                                SendMessageEvent messageEvent = new SendMessageEvent();
                                TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, ForwardContact.this);
                                JSONObject msgObj;
                                if (userData.isGroup()) {
                                    messageEvent.setEventName(SocketManager.EVENT_GROUP);
                                    msgObj = (JSONObject) message.getGroupMessageObject(userData.get_id(), msgItem.getTextMessage(), userData.getFirstName());

                                    try {
                                        msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
                                        msgObj.put("userName", userData.getFirstName());
                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    }
                                    messageEvent.setMessageObject(msgObj);
                                    MessageItemChat item = message.createMessageItem(true, msgItem.getTextMessage(), MessageFactory.DELIVERY_STATUS_NOT_SENT, userData.get_id(), userData.getFirstName());
                                    item.setGroupName(userData.getFirstName());
                                    MessageDbController db = CoreController.getDBInstance(ForwardContact.this);
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);


                                } else {
                                    msgObj = (JSONObject) message.getMessageObject(userData.get_id(), msgItem.getTextMessage(), false);
                                    messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                                    messageEvent.setMessageObject(msgObj);

                                    MessageItemChat item = message.createMessageItem(true, msgItem.getTextMessage(), MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                            userData.get_id(), userData.getFirstName());
                                    item.setSenderMsisdn(userData.getNumberInDevice());
                                    item.setSenderName(userData.getFirstName());

                                    MessageDbController db = CoreController.getDBInstance(ForwardContact.this);
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                                }

                                EventBus.getDefault().post(messageEvent);
                            }

                            break;

                        case (MessageFactory.contact + ""):
                            //   ArrayList<String> selected_data = getIntent().getStringArrayListExtra("message");
                            for (int contactIndex = 0; contactIndex < selectedContactsList.size(); contactIndex++) {
                                FriendModel userData = selectedContactsList.get(contactIndex);
                                SendMessageEvent messageEvent = new SendMessageEvent();
                                ContactMessage message = (ContactMessage) MessageFactory.getMessage(MessageFactory.contact, ForwardContact.this);

                                String contactName = msgItem.getContactName();
                                String contactNumber = msgItem.getContactNumber();
                                String contactScimboId = msgItem.getContactScimboId();
                                String contactDetails = msgItem.getDetailedContacts();

                                JSONObject msgObj;
                                if (userData.isGroup()) {
                                    messageEvent.setEventName(SocketManager.EVENT_GROUP);
                                    msgObj = (JSONObject) message.getGroupMessageObject(userData.get_id(), "", userData.getFirstName(), contactScimboId, contactName, contactNumber, contactDetails);

                                } else {
                                    messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                                    msgObj = (JSONObject) message.getMessageObject(userData.get_id(), "", contactScimboId, contactName, contactNumber, contactDetails, false);
                                }

                                messageEvent.setMessageObject(msgObj);
                                MessageItemChat item = message.createMessageItem(true, "", MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                        userData.get_id(), userData.getFirstName(), contactName, contactNumber, contactScimboId, contactDetails);
                                item.setGroupName(userData.getFirstName());
                                item.setSenderMsisdn(userData.getNumberInDevice());
//                                item.setSenderName(userData.getFirstName());
                                item.setAvatarImageUrl(userData.getAvatarImageUrl());
                                item.setDetailedContacts(contactDetails);
                                item.setContactScimboId(contactScimboId);
                                MessageDbController db = CoreController.getDBInstance(ForwardContact.this);

                                if (userData.isGroup()) {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                                } else {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                                }

                                EventBus.getDefault().post(messageEvent);
                            }
                            break;

                        case (MessageFactory.audio + ""):

                            for (int contactIndex = 0; contactIndex < selectedContactsList.size(); contactIndex++) {
                                FriendModel userData = selectedContactsList.get(contactIndex);

                                String filePath = msgItem.getChatFileLocalPath();
                                String duration = msgItem.getDuration();
                                String serverPath = msgItem.getChatFileServerPath();
                                File file= ForwardFileUtil.getValidDocumentFile(filePath);
                                filePath=file.getPath();
                                AudioMessage message = (AudioMessage) MessageFactory.getMessage(MessageFactory.audio, ForwardContact.this);
                                if (userData.isGroup()) {
                                    message.getGroupMessageObject(userData.get_id(), filePath, userData.getFirstName());
                                } else {
                                    message.getMessageObject(userData.get_id(), filePath, false);
                                }

                                MessageItemChat item = message.createMessageItem(true, filePath, duration, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                        userData.get_id(), userData.getFirstName(), msgItem.getaudiotype());

                                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(filePath);
                                String audioName = item.getMessageId() + fileExtension;

                                String docId;
                                if (userData.isGroup()) {

                                    docId = mCurrentUserId + "-" + userData.get_id() + "-g";
                                } else {
                                    docId = mCurrentUserId + "-" + userData.get_id();
                                }
                                try {
                                    if (serverPath == null || serverPath.isEmpty()) {
                                        serverPath = isUploadedFile(msgItem.getMessageId());
                                    }
                                } catch (Exception e) {
                                    MyLog.e(TAG, "isUploadedFile: ", e);
                                }
                                if(!ForwardFileUtil.isUploadedFile(file,serverPath,message,msgItem,item,userData.isGroup(),userData.getFirstName())) {
                                    JSONObject uploadObj;
                                    if (userData.isGroup()) {
                                        item.setGroupName(userData.getFirstName());
                                        uploadObj = (JSONObject) message.createAudioUploadObject(item.getMessageId(), docId, audioName, filePath,
                                                duration, userData.getFirstName(), msgItem.getaudiotype(), MessageFactory.CHAT_TYPE_GROUP, false);
                                    } else {
                                        uploadObj = (JSONObject) message.createAudioUploadObject(item.getMessageId(), docId, audioName, filePath,
                                                duration, userData.getFirstName(), msgItem.getaudiotype(), MessageFactory.CHAT_TYPE_SINGLE, false);
                                    }

                                    uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
                                }
                                else{
                                    item.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                                    item.setChatFileServerPath(serverPath);
                                }
                                item.setSenderMsisdn(userData.getNumberInDevice());
                                item.setSenderName(userData.getFirstName());
                                item.setaudiotype(msgItem.getaudiotype());

                                MessageDbController db = CoreController.getDBInstance(ForwardContact.this);
                                if (userData.isGroup()) {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                                } else {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                                }

                            }
                            break;

                        case (MessageFactory.video + ""):

                            for (int contactIndex = 0; contactIndex < selectedContactsList.size(); contactIndex++) {
                                FriendModel userData = selectedContactsList.get(contactIndex);

                                String videoPath = msgItem.getChatFileLocalPath();

                                String serverPath = msgItem.getChatFileServerPath();
                                File file= ForwardFileUtil.getValidVideoFile(videoPath);
                                if(!file.exists()){
                                    file=ForwardFileUtil.getValidVideoFile(msgItem.getVideoPath());
                                }

                                videoPath=file.getPath();
                                VideoMessage message = (VideoMessage) MessageFactory.getMessage(MessageFactory.video, ForwardContact.this);
                                if (userData.isGroup()) {
                                    message.getGroupMessageObject(userData.get_id(), videoPath, userData.getFirstName());
                                } else {
                                    message.getMessageObject(userData.get_id(), videoPath, false);
                                }

                                MessageItemChat item = message.createMessageItem(true, videoPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                        userData.get_id(), userData.getFirstName(), "");


                                try {
                                    Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND);
                                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                                    thumbBmp.compress(Bitmap.CompressFormat.PNG, 10, out);
                                    byte[] thumbArray = out.toByteArray();
                                    try {
                                        out.close();
                                    } catch (IOException e) {
                                        MyLog.e(TAG, "", e);
                                    }
                                    String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
                                    if (thumbData != null) {
                                        item.setThumbnailData(thumbData);
                                    }
                                }
                                catch (Exception e){
                                    Log.e(TAG, "onClick: ",e );
                                }

                                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(videoPath);
                                String videoName = item.getMessageId() + fileExtension;
                                try {
                                    if (serverPath == null || serverPath.isEmpty()) {
                                        serverPath = isUploadedFile(msgItem.getMessageId());
                                    }
                                } catch (Exception e) {
                                    MyLog.e(TAG, "isUploadedFile: ", e);
                                }
                                if(!ForwardFileUtil.isUploadedFile(file,serverPath,message,msgItem,item,userData.isGroup(),userData.getFirstName())) {
                                    JSONObject uploadObj;
                                    String docId;
                                    if (userData.isGroup()) {
                                        item.setGroupName(userData.getFirstName());
                                        docId = mCurrentUserId + "-" + userData.get_id() + "-g";
                                        item.setVideoPath(videoPath);
                                        uploadObj = (JSONObject) message.createVideoUploadObject(item.getMessageId(), docId,
                                                videoName, videoPath, userData.getFirstName(), "", MessageFactory.CHAT_TYPE_GROUP, false);
                                    } else {
                                        docId = mCurrentUserId + "-" + userData.get_id();
                                        uploadObj = (JSONObject) message.createVideoUploadObject(item.getMessageId(), docId,
                                                videoName, videoPath, userData.getFirstName(), "", MessageFactory.CHAT_TYPE_SINGLE, false);
                                    }


                                    uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
                                }
                                else{
                                    item.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                                    item.setChatFileServerPath(serverPath);
                                }
                                item.setSenderMsisdn(userData.getNumberInDevice());
                                item.setSenderName(userData.getFirstName());
                                MessageDbController db = CoreController.getDBInstance(ForwardContact.this);
                                if (userData.isGroup()) {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                                } else {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                                }
                            }
                            break;

                        case (MessageFactory.picture + ""):

                            for (int contactIndex = 0; contactIndex < selectedContactsList.size(); contactIndex++) {
                                FriendModel userData = selectedContactsList.get(contactIndex);

                                String imgPath = msgItem.getChatFileLocalPath();

                                String serverPath = msgItem.getChatFileServerPath();

                                MyLog.d(TAG, "onClicksend directForward serverpath: " + serverPath);

                                PictureMessage message = (PictureMessage) MessageFactory.getMessage(MessageFactory.picture, ForwardContact.this);



                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;
                                BitmapFactory.decodeFile(imgPath, options);
                                int imageHeight = options.outHeight;
                                int imageWidth = options.outWidth;

                                File file= ForwardFileUtil.getValidPictureFile(imgPath);
                                if (userData.isGroup()) {
                                    message.getGroupMessageObject(userData.get_id(), file.getPath(), userData.getFirstName());
                                } else {
                                    message.getMessageObject(userData.get_id(), file.getPath(), false);
                                }

                                MessageItemChat item = message.createMessageItem(true, "", file.getPath(), MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                        userData.get_id(), userData.getFirstName(), imageWidth, imageHeight);

                                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(imgPath);
                                String imgName = item.getMessageId() + fileExtension;

                                String docId;
                                try {
                                    if (serverPath == null || serverPath.isEmpty()) {
                                        serverPath = isUploadedFile(msgItem.getMessageId());
                                    }
                                } catch (Exception e) {
                                    MyLog.e(TAG, "isUploadedFile: ", e);
                                }
                                if(!ForwardFileUtil.isUploadedFile(file,serverPath,message,msgItem,item,userData.isGroup(),userData.getFirstName())) {
                                    JSONObject uploadObj;
                                    if (userData.isGroup()) {
                                        item.setGroupName(userData.getFirstName());
                                        docId = mCurrentUserId + "-" + userData.get_id() + "-g";
                                        uploadObj = (JSONObject) message.createImageUploadObject(item.getMessageId(), docId,
                                                imgName, file.getPath(), userData.getFirstName(), "", MessageFactory.CHAT_TYPE_GROUP, false);
                                        item.setImagePath(imgPath);
                                    } else {
                                        docId = mCurrentUserId + "-" + userData.get_id();
                                        uploadObj = (JSONObject) message.createImageUploadObject(item.getMessageId(), docId,
                                                imgName, file.getPath(), userData.getFirstName(), "", MessageFactory.CHAT_TYPE_SINGLE, false);
                                    }

                                    uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
                                }
                                else{
                                    item.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                                    item.setChatFileServerPath(serverPath);
                                }
                                item.setSenderMsisdn(userData.getNumberInDevice());
                                item.setSenderName(userData.getFirstName());
                                MessageDbController db = CoreController.getDBInstance(ForwardContact.this);
                                if (userData.isGroup()) {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                                } else {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                                }

                            }
                            break;

                        case (MessageFactory.document + ""):

                            for (int contactIndex = 0; contactIndex < selectedContactsList.size(); contactIndex++) {
                                FriendModel userData = selectedContactsList.get(contactIndex);

                                String filePath = msgItem.getChatFileLocalPath();
                                String serverPath = msgItem.getChatFileServerPath();
                                File file= ForwardFileUtil.getValidDocumentFile(filePath);
                                filePath=file.getPath();
                                DocumentMessage message = (DocumentMessage) MessageFactory.getMessage(MessageFactory.document, ForwardContact.this);

                                if (userData.isGroup()) {
                                    message.getGroupMessageObject(userData.get_id(), filePath, userData.getFirstName());
                                } else {
                                    message.getMessageObject(userData.get_id(), filePath, false);
                                }
                                MessageItemChat item = message.createMessageItem(true, filePath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                        userData.get_id(), userData.getFirstName());

                                String fileExtension = FileUploadDownloadManager.getFileExtnFromPath(filePath);
                                String docName = item.getMessageId() + fileExtension;
                                String docId;
                                try {
                                    if (serverPath == null || serverPath.isEmpty()) {
                                        serverPath = isUploadedFile(msgItem.getMessageId());
                                    }
                                } catch (Exception e) {
                                    MyLog.e(TAG, "isUploadedFile: ", e);
                                }
                                if(!ForwardFileUtil.isUploadedFile(file,serverPath,message,msgItem,item,userData.isGroup(),userData.getFirstName())) {
                                    JSONObject uploadObj;
                                    if (userData.isGroup()) {
                                        item.setGroupName(userData.getFirstName());
                                        docId = mCurrentUserId + "-" + userData.get_id() + "-g";
                                        uploadObj = (JSONObject) message.createDocUploadObject(item.getMessageId(), docId,
                                                docName, filePath, userData.getFirstName(), MessageFactory.CHAT_TYPE_GROUP, false);
                                    } else {
                                        docId = mCurrentUserId + "-" + userData.get_id();
                                        uploadObj = (JSONObject) message.createDocUploadObject(item.getMessageId(), docId,
                                                docName, filePath, userData.getFirstName(), MessageFactory.CHAT_TYPE_SINGLE, false);
                                    }

                                    uploadDownloadManager.uploadFile(EventBus.getDefault(), uploadObj);
                                }
                                else{
                                    item.setUploadStatus(MessageFactory.UPLOAD_STATUS_COMPLETED);
                                    item.setChatFileServerPath(serverPath);
                                }
                                item.setChatFileLocalPath(filePath);
                                item.setSenderMsisdn(userData.getNumberInDevice());
                                item.setSenderName(userData.getFirstName());
                                MessageDbController db = CoreController.getDBInstance(ForwardContact.this);
                                if (userData.isGroup()) {

                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                                } else {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                                }
                            }
                            break;

                        case (MessageFactory.web_link + ""):

                            for (int contactIndex = 0; contactIndex < selectedContactsList.size(); contactIndex++) {
                                FriendModel userData = selectedContactsList.get(contactIndex);

                                String data = msgItem.getTextMessage();
                                String webLink = msgItem.getWebLink();
                                String webLinkTitle = msgItem.getWebLinkTitle();
                                String webLinkDesc = msgItem.getWebLinkDesc();
                                String webLinkImgUrl = msgItem.getWebLinkImgUrl();
                                String webLinkThumb = msgItem.getWebLinkImgThumb();

                                SendMessageEvent messageEvent = new SendMessageEvent();
                                WebLinkMessage message = (WebLinkMessage) MessageFactory.getMessage(MessageFactory.web_link, ForwardContact.this);
                                JSONObject msgObj;
                                if (userData.isGroup()) {
                                    msgObj = (JSONObject) message.getGroupMessageObject(userData.get_id(), data, userData.getFirstName());
                                } else {
                                    msgObj = (JSONObject) message.getMessageObject(userData.get_id(), data, false);
                                }

                                MessageItemChat item = message.createMessageItem(true, data, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                        userData.get_id(), userData.getFirstName(), webLink, webLinkTitle, webLinkDesc, webLinkImgUrl, webLinkThumb);
                                msgObj = (JSONObject) message.getWebLinkObject(msgObj, webLink, webLinkTitle, webLinkDesc, webLinkImgUrl, webLinkThumb);
                                messageEvent.setMessageObject(msgObj);

                                item.setSenderMsisdn(userData.getNumberInDevice());
                                item.setSenderName(userData.getFirstName());
                                MessageDbController db = CoreController.getDBInstance(ForwardContact.this);
                                if (userData.isGroup()) {
                                    item.setGroupName(userData.getFirstName());
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                                    messageEvent.setEventName(SocketManager.EVENT_GROUP);
                                } else {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                                    messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                                }
                                EventBus.getDefault().post(messageEvent);
                            }

                            break;

                        case (MessageFactory.location + ""):

                            for (int contactIndex = 0; contactIndex < selectedContactsList.size(); contactIndex++) {
                                FriendModel userData = selectedContactsList.get(contactIndex);

                                String data = msgItem.getTextMessage();
                                String webLink = msgItem.getWebLink();
                                String webLinkTitle = msgItem.getWebLinkTitle();
                                String webLinkDesc = msgItem.getWebLinkDesc();
                                String webLinkImgUrl = msgItem.getWebLinkImgUrl();
                                String webLinkThumb = msgItem.getWebLinkImgThumb();

                                SendMessageEvent messageEvent = new SendMessageEvent();
                                LocationMessage message = (LocationMessage) MessageFactory.getMessage(MessageFactory.location, ForwardContact.this);
                                JSONObject msgObj;
                                if (userData.isGroup()) {
                                    msgObj = (JSONObject) message.getGroupMessageObject(userData.get_id(), data, userData.getFirstName());
                                } else {
                                    msgObj = (JSONObject) message.getMessageObject(userData.get_id(), data, false);
                                }

                                MessageItemChat item = message.createMessageItem(true, data, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                        userData.get_id(), userData.getFirstName(), webLinkTitle, webLinkDesc, webLink, webLinkImgUrl, webLinkThumb);
                                msgObj = (JSONObject) message.getLocationObject(msgObj, webLinkTitle, webLinkDesc, webLink, webLinkImgUrl, webLinkThumb);
                                messageEvent.setMessageObject(msgObj);

                                item.setSenderMsisdn(userData.getNumberInDevice());
                                item.setSenderName(userData.getFirstName());
                                MessageDbController db = CoreController.getDBInstance(ForwardContact.this);
                                if (userData.isGroup()) {
                                    item.setGroupName(userData.getFirstName());
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_GROUP);
                                    messageEvent.setEventName(SocketManager.EVENT_GROUP);
                                } else {
                                    db.updateChatMessage(item, MessageFactory.CHAT_TYPE_SINGLE);
                                    messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                                }

                                EventBus.getDefault().post(messageEvent);
                            }

                            break;
                    }

                }

                if (selectedContactsList.size() == 1) {
                    Intent intent = new Intent(ForwardContact.this, ChatPageActivity.class);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    FriendModel userData = selectedContactsList.get(0);
                    intent.putExtra("receiverUid", userData.get_id());
                    intent.putExtra("documentId", userData.get_id());
                    intent.putExtra("receiverName", userData.getFirstName());
                    intent.putExtra("Username", userData.getFirstName());

                    intent.putExtra("Image", userData.getAvatarImageUrl());

                    intent.putExtra("type", 0);
                    intent.putExtra("msisdn", userData.getNumberInDevice());
                    startActivity(intent);
                    finish();
                } else {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("MultiForward", true);
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }

            }

        });
        /* Variables for serch */

    }

    private void filterContactsList(List<FriendModel> friendModels) {
        if (friendModels == null)
            return;
        String keyFriends = this.getString(R.string.friends);
        HashMap<String, ArrayList<FriendModel>> map = new HashMap<>();
        map.put(keyFriends, new ArrayList<FriendModel>());

        for (FriendModel friendModel : friendModels) {
            String status = friendModel.getRequestStatus();
            if (TextUtils.isEmpty(status))
                continue;
            String key = "";
            if (status.equals(AppConstants.FriendStatus.DELETED.getValue()))
                continue;
            else if (status.equals(AppConstants.FriendStatus.FRIENDS.getValue()) || status.equals(AppConstants.FriendStatus.DISABLED.getValue())) {
                key = keyFriends;
            }

            ArrayList<FriendModel> models = map.get(key);
            if (models == null)
                models = new ArrayList<>();
            models.add(friendModel);
        }

        ArrayList<FriendModel> friends = map.get(keyFriends);
        if (friends == null) friends = new ArrayList<>();

        this.scimboEntries = friends;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {
        }
    }

    public void getGroupDetails(String groupId) {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_GROUP_DETAILS);

        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            object.put("convId", groupId);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private String isUploadedFile(String msgId) {


        try {
            MessageDbController messageDbController = CoreController.getDBInstance(this);
            String uploadStatus = messageDbController.fileuploadStatusget(msgId);
            if (uploadStatus.equals("completed")) {
                JSONObject object = messageDbController.fileuploadobjectget(msgId);

                if (object != null && object.has("filename")) {
                    return object.getString("filename");
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "isUploadedFile: ", e);
        }

        return null;

    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_forward_contact, menu);
        MenuItem searchItem = menu.findItem(R.id.chats_searchIcon);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.equals("") && query.isEmpty()) {
                    searchView.clearFocus();
                }
                adapter.getFilter().filter(query);
                if (frequentAdapter != null) {
                    frequentAdapter.getFilter().filter(query);
                }

                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.equals("") && newText.isEmpty()) {
                    searchView.clearFocus();
                }
                adapter.getFilter().filter(newText);
                if (frequentAdapter != null) {
                    frequentAdapter.getFilter().filter(newText);
                }
                return false;
            }
        });

        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*etSearch.getText().clear();
         *//*if (adapter != null) {
                            adapter.updateInfo(scimboEntries);
                        }*//*
        etSearch.setVisibility(View.GONE);
        serach.setVisibility(View.VISIBLE);
        //overflowlayout.setVisibility(View.VISIBLE);
        selectcontactmember.setVisibility(View.VISIBLE);
        selectcontact.setVisibility(View.VISIBLE);
        backarrow.setVisibility(View.GONE);
        backButton.setVisibility(View.VISIBLE);*/
        hideKeyboard();
    }

    public void toast(String msg) {
        showToast(ForwardContact.this, msg);
    }

}
