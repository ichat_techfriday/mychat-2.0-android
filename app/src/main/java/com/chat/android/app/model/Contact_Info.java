package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Contact_Info implements Serializable {

    @SerializedName("Contact_ID")
    String contact_ID;
    @SerializedName("Label")
    String label;
    @SerializedName("First")
    String first;
    @SerializedName("Middle")
    String middle;
    @SerializedName("Last")
    String last;
    @SerializedName("Country_Code")
    String country_Code;
    @SerializedName("Nationality_Code")
    String nationality_Code;
    @SerializedName("Mobile")
    String mobile;
    @SerializedName("City")
    String city;
    @SerializedName("Address")
    String address;
    @SerializedName("Banks")
    List<Save_Contact_Bank> banks;
    @SerializedName("Wallets")
    List<WalletModel> Wallets;

    public String getContact_ID() {
        return contact_ID;
    }

    public void setContact_ID(String contact_ID) {
        this.contact_ID = contact_ID;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getMiddle() {
        return middle;
    }

    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getCountry_Code() {
        return country_Code;
    }

    public void setCountry_Code(String country_Code) {
        this.country_Code = country_Code;
    }

    public String getNationality_Code() {
        return nationality_Code;
    }

    public void setNationality_Code(String nationality_Code) {
        this.nationality_Code = nationality_Code;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public List<Save_Contact_Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Save_Contact_Bank> banks) {
        this.banks = banks;
    }

    public void setWallets(List<WalletModel> wallets) {
        Wallets = wallets;
    }

    public List<WalletModel> getWallets() {
        return Wallets;
    }
}

