package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class MobileUserVerification {
    @SerializedName("MobileNumber")
    String mobileNumber;
    @SerializedName("IMEI")
    String imei;

    public MobileUserVerification(String mobileNumber, String imei, Integer pincode, String company) {
        this.mobileNumber = mobileNumber;
        this.imei = imei;
        this.pincode = pincode;
        this.company = company;
    }

    @SerializedName("PinCode")
    Integer pincode;

    public MobileUserVerification() {

    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @SerializedName("Company")
    String company;

}
