package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class VerificationPinResponseModel {
    @SerializedName("PWD")
    public String pWD;
    @SerializedName("UNAME")
    public String uNAME;
    @SerializedName("MobileNumber")
    public String mobileNumber;
    @SerializedName("DeviceIMEI")
    public String deviceIMEI;
    @SerializedName("ResponseID")
    public String responseID;

    public VerificationPinResponseModel(String pWD, String uNAME, String mobileNumber, String deviceIMEI, String responseID, String errorCode, String errorMessage) {
        this.pWD = pWD;
        this.uNAME = uNAME;
        this.mobileNumber = mobileNumber;
        this.deviceIMEI = deviceIMEI;
        this.responseID = responseID;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getpWD() {
        return pWD;
    }

    public void setpWD(String pWD) {
        this.pWD = pWD;
    }

    public String getuNAME() {
        return uNAME;
    }

    public void setuNAME(String uNAME) {
        this.uNAME = uNAME;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDeviceIMEI() {
        return deviceIMEI;
    }

    public void setDeviceIMEI(String deviceIMEI) {
        this.deviceIMEI = deviceIMEI;
    }

    public String getResponseID() {
        return responseID;
    }

    public void setResponseID(String responseID) {
        this.responseID = responseID;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorMessage")
    public String errorMessage;
}
