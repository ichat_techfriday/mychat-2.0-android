package com.chat.android.app.activity;


import android.content.ActivityNotFoundException;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;

import com.chat.android.app.model.StatusRefresh;
import com.chat.android.app.utils.DocOpenUtils;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.uploadtoserver.FileUploadDownloadManager;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.TextPaint;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chat.android.R;
import com.chat.android.app.adapter.CommonInGroupAdapter;
import com.chat.android.app.adapter.RItemAdapter;
import com.chat.android.app.calls.CallMessage;
import com.chat.android.app.calls.CallUtil;
import com.chat.android.app.calls.CallsActivity;
import com.chat.android.app.dialog.ChatLockPwdDialog;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.dialog.MuteAlertDialog;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.BlockUserUtils;
import com.chat.android.app.utils.CommonData;
import com.chat.android.app.utils.ConnectivityInfo;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.GroupInfoSession;
import com.chat.android.app.utils.MuteUnmute;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.app.widget.AvnNextLTProDemiTextView;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.ChatLockPojo;
import com.chat.android.core.model.CommonInGroupPojo;
import com.chat.android.core.model.GroupInfoPojo;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.MuteStatusPojo;
import com.chat.android.core.model.MuteUserPojo;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;
import com.kyleduo.switchbutton.SwitchButton;

import org.appspot.apprtc.CallActivity;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by CAS60 on 12/26/2016.
 */
public class UserInfo extends CoreActivity implements View.OnClickListener, MuteAlertDialog.MuteAlertCloseListener {

    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView ivProfilePic;
    private TextView tvStatus, tvPinCode, tvPhone, tvgroup_count, tvCommonGroup, head, media, mute, custom_notification, mediacount;
    private RecyclerView rvMedia, rvCommonGroup;
    SessionManager sessionManager;
    private Session session;
    CommonInGroupAdapter adapter;
    private GroupInfoSession groupInfoSession;
    SwitchButton swMute;
    private Typeface avnRegFont, avnDemiFont;
    String value, contactid26, mRawContactId, mDataId, secretType;
    MessageDbController db;
    private Boolean ismutecheckchange = false, isSecretUserInfo;
    int messagetype;
    private ArrayList<String> imgzoompath;
    private ArrayList<Uri> uriList;
    ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
    public static final String INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED = "finishActivityOnSaveCompleted";
    private final int ADD_CONTACT = 21;
    private static final int PICK_CONTACT_REQUEST = 10;
    String phNo, userId, userName, userAvatar, name, UserNumber, chatType;

    final Context context = this;
    final int REQUEST_CODE_PICK_CONTACTS = 1;
    final int PICK_CONTACT = 5;
    private String mCurrentUserId, mLocDbDocId;
    RelativeLayout medialayout;
    LinearLayout media_lineralayout;
    UserInfoSession userInfoSession;
    private ArrayList<MessageItemChat> horizontalList;
    private HorizontalAdapter horizontalAdapter;
    final int REQUEST_CODE_CONTACTS = 2;
    private ArrayList<MessageItemChat> mChatData;
    private ArrayList<CommonInGroupPojo> commonGroupList;
    String status = "";
    String pincode = "";
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 14;
    int screenWidth = 0;
    int screenHeight = 0;
    private static final String TAG = "UserInfo";
    private TextView user_name_title, tvBlock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        db = CoreController.getDBInstance(this);
        groupInfoSession = new GroupInfoSession(UserInfo.this);
        imgzoompath = new ArrayList<String>();
        uriList = new ArrayList<>();
        ScimboContactsService.savedName = "";
        initView();
    }

    private void initView() {
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        userInfoSession = new UserInfoSession(this);
        Typeface typeface = CoreController.getInstance().getAvnNextLTProDemiTypeface();
        collapsingToolbarLayout.setCollapsedTitleTypeface(typeface);
        collapsingToolbarLayout.setExpandedTitleTypeface(typeface);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
        mediacount = findViewById(R.id.mediacount);
        ivProfilePic = findViewById(R.id.ivProfilePic);
        user_name_title = findViewById(R.id.user_name_title);
        horizontalList = new ArrayList<MessageItemChat>();
        tvStatus = findViewById(R.id.tvStatus);
        tvPinCode = findViewById(R.id.tv_pincode);
        tvPhone = findViewById(R.id.tvPhone);
        medialayout = findViewById(R.id.medialayout);
        tvgroup_count = findViewById(R.id.tvCommonGroup_Count);
        //  String id = tvPhone.getText().toString();
        tvCommonGroup = findViewById(R.id.tvCommonGroup);
        head = findViewById(R.id.head);
        media = findViewById(R.id.media);
        mute = findViewById(R.id.mute);
        custom_notification = findViewById(R.id.custom_notification);
        media_lineralayout = findViewById(R.id.media_lineralayout);
        rvMedia = findViewById(R.id.rvMedia);
//        rvMedia.setNestedScrollingEnabled(false);
        LinearLayoutManager mediaManager = new LinearLayoutManager(UserInfo.this, LinearLayoutManager.HORIZONTAL, false);
        rvMedia.setLayoutManager(mediaManager);

        rvCommonGroup = findViewById(R.id.rvCommonGroup);

//        rvCommonGroup.setNestedScrollingEnabled(false);
        LinearLayoutManager groupManager = new LinearLayoutManager(UserInfo.this);
        rvCommonGroup.setLayoutManager(groupManager);
        sessionManager = SessionManager.getInstance(UserInfo.this);
        session = new Session(UserInfo.this);
        mCurrentUserId = sessionManager.getCurrentUserID();
        tvBlock = findViewById(R.id.tv_block);

        swMute = findViewById(R.id.swMute);
        findViewById(R.id.block_layout).setOnClickListener(this);
        findViewById(R.id.iv_msg).setOnClickListener(this);
        findViewById(R.id.iv_audio_call).setOnClickListener(this);
        findViewById(R.id.iv_video_call).setOnClickListener(this);
        findViewById(R.id.tv_start_secret_chat).setOnClickListener(this);
        initProgress(getString(R.string.loading_in), true);

        setItemTouchListener();

        initData();
        ivProfilePic.setOnClickListener(UserInfo.this);
        swMute.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (ConnectivityInfo.isInternetConnected(UserInfo.this)) {
                            if (isChecked) {
                                ismutecheckchange = true;
                                ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(UserInfo.this);

                                String convId = userInfoSession.getChatConvId(mLocDbDocId);
                                MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, userId, convId, isSecretUserInfo);
//Check  mute status is empty assign to 0
                                if (AppUtils.isEmpty(muteData.getMuteStatus())) {
                                    if (ismutecheckchange)
                                        muteData.setMuteStatus("0");
                                }
                                if (muteData == null || muteData.getMuteStatus().equals("0")) {
                                    MuteUserPojo muteUserPojo = new MuteUserPojo();
                                    muteUserPojo.setReceiverId(userId);
                                    muteUserPojo.setChatType(MessageFactory.CHAT_TYPE_SINGLE);

                                    if (isSecretUserInfo) {
                                        muteUserPojo.setSecretType("yes");
                                    } else {
                                        muteUserPojo.setSecretType("no");
                                    }

                                    ArrayList<MuteUserPojo> muteUserList = new ArrayList<>();
                                    muteUserList.add(muteUserPojo);

                                    Bundle putBundle = new Bundle();
                                    putBundle.putSerializable("MuteUserList", muteUserList);

                                    MuteAlertDialog dialog = new MuteAlertDialog();
                                    dialog.setArguments(putBundle);
                                    dialog.setCancelable(false);
                                    dialog.setMuteAlertCloseListener(UserInfo.this);
                                    dialog.show(getSupportFragmentManager(), "Mute");
                                }
                            } else {
                                ismutecheckchange = true;

                                MuteUnmute.performUnMute(UserInfo.this, EventBus.getDefault(), userId, MessageFactory.CHAT_TYPE_SINGLE,
                                        secretType);
                                showProgressDialog();
                            }
                        } else {
                            swMute.setChecked(!isChecked);
                            Toast.makeText(UserInfo.this, getResources().getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );
    }

    private void initData() {
        userId = getIntent().getStringExtra("UserId");
        userName = getIntent().getStringExtra("UserName");
        UserNumber = getIntent().getStringExtra("UserNumber");
        userAvatar = getIntent().getStringExtra("UserAvatar");
        isSecretUserInfo = getIntent().getBooleanExtra("FromSecretChat", false);

//        userAvatar = userAvatar.concat("?id=").concat(String.valueOf(Calendar.getInstance().getTimeInMillis()));
        mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();
        Log.e("userAvatar", "userAvatar" + userAvatar);
        Log.e("userId", "userId" + userId);
        Log.e("mLocDbDocId", "mLocDbDocId" + mLocDbDocId);

        userAvatar = new Getcontactname(this).getAvatarUrl(userId);
        mLocDbDocId = mCurrentUserId.concat("-").concat(userId);
        if (isSecretUserInfo) {
            mLocDbDocId = mLocDbDocId + "-secret";
            secretType = "yes";
            chatType = MessageFactory.CHAT_TYPE_SECRET;
        } else {
            secretType = "no";
            chatType = MessageFactory.CHAT_TYPE_SINGLE;
        }

        mChatData = new ArrayList<>();
        loadFromDB();
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        String blockStatus = contactDB_sqlite.getBlockedStatus(userId, false);
        String convId = userInfoSession.getChatConvId(mLocDbDocId);
        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, userId, convId, isSecretUserInfo);

        if (muteData != null && muteData.getMuteStatus().equals("1")) {
            swMute.setChecked(true);
        } else {
            swMute.setChecked(false);
        }

        if (blockStatus.equals("1")) {
            tvBlock.setText(getResources().getString(R.string.unblock_contact));
        } else {
            tvBlock.setText(getResources().getString(R.string.block));
        }

        avnDemiFont = CoreController.getInstance().getAvnNextLTProDemiTypeface();
        makeCollapsingToolbarLayoutLooksGood(collapsingToolbarLayout);
        collapsingToolbarLayout.setTitle(userName);
        //user_name_title.setText(userName);
        getUserDetails(userId);

        profilepicUpdation();
        //String from = SessionManager.getInstance(UserInfo.this).getCurrentUserID();

        MessageDbController db = CoreController.getDBInstance(this);

        commonGroupList = new ArrayList<>();
        ArrayList<MessageItemChat> groupChats = db.selectChatList(MessageFactory.CHAT_TYPE_GROUP);
        Getcontactname getcontactname = new Getcontactname(this);

        for (MessageItemChat chat : groupChats) {
            String docID = mCurrentUserId + "-" + chat.getReceiverID() + "-g";

            if (groupInfoSession.hasGroupInfo(docID)) {
                GroupInfoPojo groupInfoPojo = groupInfoSession.getGroupInfo(docID);
                if (groupInfoPojo.getGroupMembers() != null && groupInfoPojo.getGroupMembers().contains(userId)
                        && groupInfoPojo.isLiveGroup()) {
                    StringBuilder sb = new StringBuilder();
                    String memername = "";

                    String[] contacts = groupInfoPojo.getGroupMembers().split(",");

                    for (int i = 0; i < contacts.length; i++) {
                        if (!contacts[i].equalsIgnoreCase(mCurrentUserId)) {
//                            FriendModel info = contactsDB.getUserDetails(contacts[i]);
                            FriendModel info = contactDB_sqlite.getFriendDetails(contacts[i]);

                            if (info != null) {
                                memername = getcontactname.getSendername(contacts[i], info.getMsisdn());
                                sb.append(memername);
                                if (contacts.length - 1 != i) {
                                    sb.append(", ");
                                }

                            }
                        } else {
                            memername = "You";
                            sb.append(memername);
                            if (contacts.length - 1 != i) {
                                sb.append(", ");
                            }
                        }
                    }

//                    statusTextView.setText(sb);

                    CommonInGroupPojo commonGroupPojo = new CommonInGroupPojo();
                    commonGroupPojo.setGroupName(groupInfoPojo.getGroupName());
                    commonGroupPojo.setAvatarPath(groupInfoPojo.getAvatarPath());
                    commonGroupPojo.setGroupContactNames(sb.toString());
                    commonGroupPojo.setGroupId(groupInfoPojo.getGroupId());
                    commonGroupList.add(commonGroupPojo);
                }
            }
        }
        if (commonGroupList == null || commonGroupList.size() == 0) {
            tvgroup_count.setText("");
            findViewById(R.id.group_common_layout).setVisibility(View.GONE);
        } else {
            Integer groupCount = commonGroupList.size();
            tvgroup_count.setText("" + groupCount);
        }
        adapter = new CommonInGroupAdapter(UserInfo.this, commonGroupList);
        rvCommonGroup.setAdapter(adapter);

        statusUpdation();
    }

    private void setUserName() {
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(UserInfo.this);
                    contactDB_sqlite.getNameByUserId(userId);
                    collapsingToolbarLayout.setTitle(userName);
                }
            }, 2000);
        } catch (Exception e) {
            Log.e(TAG, "setUserName: ", e);
        }
    }

    public static Date addDay(Date date, int i) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, i);
        return cal.getTime();
    }

    public static Date addhour(Date date, int i) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, i);
        return cal.getTime();
    }

    public static Date addYear(Date date, int i) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, i);
        return cal.getTime();
    }

    private void loadFromDB() {
        ArrayList<MessageItemChat> items;
        items = db.selectAllChatMessages(mLocDbDocId, chatType);
        mChatData.clear();
        mChatData.addAll(items);
        mediafile();

        if (horizontalList.size() > 0) {
            media_lineralayout.setVisibility(View.VISIBLE);
        }

    }

    protected void mediafile() {
        for (int i = 0; i < mChatData.size(); i++) {
            String type = mChatData.get(i).getMessageType();
            int mtype = Integer.parseInt(type);
            if (MessageFactory.picture == mtype) {
                MessageItemChat msgItem = mChatData.get(i);
                if (msgItem.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                    if (msgItem.getImagePath() != null) {
                        String path = msgItem.getImagePath();
                        File file = new File(path);
                        if (file.exists()) {
                            // Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                            //  uriList.add(pathuri);
                        }

                    } else if (msgItem.getChatFileLocalPath() != null) {
                        String path = msgItem.getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
                            // Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                            //   uriList.add(pathuri);
                        }

                    }
                }

            }
            if (MessageFactory.video == mtype) {
                MessageItemChat msgItem = mChatData.get(i);
                if (msgItem.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                    if (msgItem.getChatFileLocalPath() != null) {
                        String path = msgItem.getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
                            //Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                            // uriList.add(pathuri);
                        }
                    } else if (msgItem.getVideoPath() != null) {
                        String path = msgItem.getVideoPath();
                        File file = new File(path);
                        if (file.exists()) {
                            // Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                            //    uriList.add(pathuri);
                        }

                    }
                }
            }
            if (MessageFactory.document == mtype) {
                MessageItemChat msgItem = mChatData.get(i);
                if (msgItem.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                    if (msgItem.getVideoPath() != null) {
                        String path = msgItem.getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
                            //Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                            //  uriList.add(pathuri);
                        }
                    } else if (msgItem.getChatFileLocalPath() != null) {
                        String path = msgItem.getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
                            //Uri pathuri = Uri.fromFile(file);
                            horizontalList.add(msgItem);
                            imgzoompath.add(path);
                            //   uriList.add(pathuri);
                        }
                    }
                }
            }
            if (MessageFactory.audio == mtype) {
                MessageItemChat msgItem = mChatData.get(i);
                if (msgItem.getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                    MyLog.e(TAG, "audio" + msgItem.getaudiotype());
                    if (msgItem.getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                        if (msgItem.getAudioPath() != null) {
                            String path = msgItem.getAudioPath();
                            File file = new File(path);
                            if (file.exists()) {
                                //Uri pathuri = Uri.fromFile(file);
                                horizontalList.add(msgItem);
                                imgzoompath.add(path);
                                //   uriList.add(pathuri);
                            }
                        } else if (msgItem.getChatFileLocalPath() != null) {
                            String path = msgItem.getChatFileLocalPath();
                            File file = new File(path);
                            if (file.exists()) {
                                // Uri pathuri = Uri.fromFile(file);
                                horizontalList.add(msgItem);
                                imgzoompath.add(path);
                                //     uriList.add(pathuri);
                            }
                        }

                    } else if (msgItem.getaudiotype() == MessageFactory.AUDIO_FROM_ATTACHMENT) {
                        if (msgItem.getAudioPath() != null) {
                            String path = msgItem.getAudioPath();
                            File file = new File(path);
                            if (file.exists()) {
                                // Uri pathuri = Uri.fromFile(file);
                                horizontalList.add(msgItem);
                                imgzoompath.add(path);
                                //  uriList.add(pathuri);
                            }
                        } else if (msgItem.getChatFileLocalPath() != null) {
                            String path = msgItem.getChatFileLocalPath();
                            File file = new File(path);
                            if (file.exists()) {
                                // Uri pathuri = Uri.fromFile(file);
                                horizontalList.add(msgItem);
                                imgzoompath.add(path);
                                // uriList.add(pathuri);
                            }
                        }
                    }
                }
            }
        }
        horizontalAdapter = new HorizontalAdapter(horizontalList);
        rvMedia.setAdapter(horizontalAdapter);
        int count = horizontalList.size();
        mediacount.setText((String.valueOf(count)));
        rvMedia.addOnItemTouchListener(new RItemAdapter(UserInfo.this, rvMedia, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.picture)) {
                    Intent intent = new Intent(getApplication(), ImageZoom.class);
                    intent.putExtra("from", "media");
                    intent.putExtra("image", imgzoompath.get(position));
                    startActivity(intent);
                } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.video)) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(imgzoompath.get(position)));
                        intent.setDataAndType(Uri.parse(imgzoompath.get(position)), "video/*");
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(UserInfo.this, getResources().getString(R.string.no_app_to_play_video), Toast.LENGTH_SHORT).show();
                    }
                } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.audio)) {
                    try {
                        File file = new File(imgzoompath.get(position));
                        boolean isExists = file.exists();
                        Log.d(TAG, "onItemClick: " + isExists);
                        Uri uri = Uri.fromFile(file);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.setDataAndType(uri, "audio/*");
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(UserInfo.this, getResources().getString(R.string.no_app_to_play_video), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        MyLog.e(TAG, "onItemClick: ", e);
                    }

                } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.document)) {
                    DocOpenUtils.openDocument(horizontalList.get(position), UserInfo.this);
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
        medialayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserInfo.this, MediaAcitivity.class);
                intent.putExtra("username", userName);
                intent.putExtra("docid", mLocDbDocId);
                startActivity(intent);
            }
        });
        if (horizontalList.size() == 0) {
            media_lineralayout.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.user_screen, menu);
        MenuItem addcontact_menu = menu.findItem(R.id.add_contact);
        MenuItem add_existing_contact = menu.findItem(R.id.add_existing_contact);
        MenuItem share = menu.findItem(R.id.share);
        MenuItem edit = menu.findItem(R.id.edit);
        MenuItem view = menu.findItem(R.id.view);

        Getcontactname objContact = new Getcontactname(UserInfo.this);
        boolean isAlreadyContact = objContact.isContactExists(userId);

        if (!isAlreadyContact) {
            addcontact_menu.setVisible(true);
            add_existing_contact.setVisible(true);
            share.setVisible(false);
            edit.setVisible(false);
            view.setVisible(false);

            addcontact_menu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    intent.putExtra(INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED, true);
                    // Sets the MIME type to match the Contacts Provider
                    intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    intent.putExtra(ContactsContract.Intents.Insert.PHONE, userName);
//                startActivity(intent);
                    startActivityForResult(intent, ADD_CONTACT);
                    return false;
                }
            });
            add_existing_contact.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), PICK_CONTACT_REQUEST);

                    return false;
                }
            });

        } else {
            share.setVisible(false);
            edit.setVisible(false);
            view.setVisible(false);
        }
        return true;
    }

    private void editContact(String phone, String name) {
        try {
            // CONTENT_FILTER_URI allow to search contact by phone number
            Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
// This query will return NAME and ID of conatct, associated with phone //number.
            Cursor mcursor = getContentResolver().query(lookupUri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
//Now retrive _ID from query result
            long idPhone = 0;
            try {
                if (mcursor != null) {
                    if (mcursor.moveToFirst()) {
                        idPhone = Long.valueOf(mcursor.getString(mcursor.getColumnIndex(ContactsContract.PhoneLookup._ID)));
                        MyLog.d("", "Contact id::" + idPhone);
                        if (idPhone != 0) {
                            Intent intent = new Intent(Intent.ACTION_EDIT);
                            intent.setData(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, idPhone));
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                            intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
                            intent.putExtra(ContactsContract.Intents.Insert.PHONE, phone);
                            startActivityForResult(intent, REQUEST_CODE_PICK_CONTACTS);
                        }
                    }
                }
            } finally {
                mcursor.close();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "editContact: ", e);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
          /*  if (session.getmute(userId + "mute")) {
                swMute.setChecked(false);
            } else {
                swMute.setChecked(true);
            }*/
            changemute();
            finish();
        } else if (item.getItemId() == R.id.edit) {
            editContact(phNo, userName);
            // getContactName(context,phNo);

        } else if (item.getItemId() == R.id.view) {
            displayContacts();
            /*Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, 3824);
            intent.setData(uri);
            context.startActivity(intent);*/
        } else if (item.getItemId() == R.id.share) {
            String contactScimboId = "";

            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);


            ArrayList<FriendModel> scimboEntries = contactDB_sqlite.getSavedFriendModels();

            if (scimboEntries != null) {
                for (FriendModel scimboMOdel : scimboEntries) {
                    if ((scimboMOdel.getNumberInDevice() != null && scimboMOdel.getNumberInDevice().equalsIgnoreCase(phNo))
                            || (scimboMOdel.getMsisdn() != null && scimboMOdel.getMsisdn().equalsIgnoreCase(phNo))) {
                        contactScimboId = scimboMOdel.get_id();
                        break;
                    }
                }
            }

            messagetype = MessageFactory.contact;
            MessageItemChat msgItem = new MessageItemChat();
            msgItem.setContactName(userName);
            msgItem.setContactNumber(phNo);
            msgItem.setContactScimboId(contactScimboId);
            msgItem.setMessageType("" + messagetype);
            selectedChatItems.add(msgItem);

            Bundle bundle = new Bundle();
            //   bundle.putSerializable("MsgItemList", selectedChatItems);
            bundle.putBoolean("FromScimbo", true);
            CommonData.setForwardedItems(selectedChatItems);

            Intent intent = new Intent(context, ForwardContact.class);
            intent.putExtras(bundle);
            context.startActivity(intent);

           /* Intent intent = new Intent(context, ForwardContact.class);
            // intent.putExtra("message", newtext);
            intent.putExtra("message1", new Gson().toJson(selectedChatItems));
            startActivity(intent);*/
        }
        return super.onOptionsItemSelected(item);
    }

    private void displayContacts() {

        contactid26 = null;

        ContentResolver contentResolver = getContentResolver();

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phNo));

        Cursor cursor =
                contentResolver.query(
                        uri,
                        new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID},
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                String contactName = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup.DISPLAY_NAME));
                contactid26 = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));

            }
            cursor.close();
        }
        if (contactid26 == null) {
            Toast.makeText(UserInfo.this, getResources().getString(R.string.no_contact_found_associated_with_this_number), Toast.LENGTH_SHORT).show();
        } else {
            Intent intent_contacts = new Intent(Intent.ACTION_VIEW, Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactid26)));
            //Intent intent_contacts = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people/" + contactid26));
            startActivity(intent_contacts);
        }

    }


    private String getContactName(Context context, String number) {

        name = null;

        // define the columns I want the query to return
        String[] projection = new String[]{
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID};

        // encode the phone number and build the filter URI
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        // query tsNextLine
        Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));

            } else {
            }
            cursor.close();
        }
        return name;

    }

    private String getNameByURI(Uri contactUri) {
        Cursor cursor = context.getContentResolver().query(contactUri, null, null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));

            } else {
            }
            cursor.close();
        }
        return name;
    }

    private void setItemTouchListener() {
        rvCommonGroup.addOnItemTouchListener(new RItemAdapter(this, rvCommonGroup, new RItemAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {

                ChatLockPojo lockPojo = getChatLockdetailfromDB(position);
                if (sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {

                    String stat = lockPojo.getStatus();
                    String pwd = lockPojo.getPassword();

                    CommonInGroupPojo e = adapter.getItem(position);
                    String docID = e.getGroupId();
                    String documentid = mCurrentUserId + "-" + docID + "-g";
                    if (stat.equals("1")) {
                        openUnlockChatDialog(documentid, stat, pwd, position);
                    } else {
                        navigateTochatviewpage(position);
                    }
                } else {
                    navigateTochatviewpage(position);
                }
            }


            @Override
            public void onItemLongClick(View view, int position) {

            }

        }


        ));
    }


    private void openUnlockChatDialog(String docId, String status, String pwd, int position) {
        String convId = docId.split("-")[1];
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1(getResources().getString(R.string.enter_your_pwd_label));
        dialog.setEditTextdata(getResources().getString(R.string.hint_newPwd));
        dialog.setforgotpwdlabel(getResources().getString(R.string.forgotChatpwd));
        dialog.setHeader(getResources().getString(R.string.unlock_chat));
        dialog.setButtonText(getResources().getString(R.string.unlock));
        Bundle bundle = new Bundle();
        if (position >= 0)
            bundle.putSerializable("commoningroup", adapter.getItem(position));
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "group");
        bundle.putString("from", mCurrentUserId);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatunLock");
    }


    private void navigateTochatviewpage(int position) {
        Intent intent = new Intent(UserInfo.this, ChatPageActivity.class);
        CommonInGroupPojo data = commonGroupList.get(position);
        String username = data.getGroupName();
        String profileimage = data.getAvatarPath();
        intent.putExtra("receiverUid", data.getGroupId());
        intent.putExtra("receiverName", username);
        intent.putExtra("documentId", data.getGroupId());

        intent.putExtra("Username", username);
        intent.putExtra("Image", profileimage);
        intent.putExtra("msisdn", "");
        intent.putExtra("type", 0);

        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_PICK_CONTACTS && resultCode == RESULT_OK && data != null) {
            getContactName(context, phNo);
            registerReceiverForName();

        } else if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            loadContactInfo(data.getData());
            registerReceiverForName();
        } else if (requestCode == ADD_CONTACT && resultCode == RESULT_OK) {
            //getNameByURI(data.getData());
            registerReceiverForName();
        }

    }

    private void registerReceiverForName() {
        setUserName();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                ScimboContactsService.savedNumber = Objects.requireNonNull(collapsingToolbarLayout.getTitle()).toString();
                ScimboContactsService.bindContactService(UserInfo.this, false);
                ScimboContactsService.setBroadCastSavedName(new ScimboContactsService.BroadCastSavedName() {
                    @Override
                    public void savedName(final String name) {
                        collapsingToolbarLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                collapsingToolbarLayout.setTitle(name);
                                ScimboContactsService.savedName = name;

                            }
                        });


                    }
                });
            }
        });
    }

    private void loadContactInfo(Uri contactUri) {

        /*
         * We should always run database queries on a background thread. The database may be
         * locked by some process for a long tsNextLine.  If we locked up the UI thread while waiting
         * for the query to come back, we might get an "Application Not Responding" dialog.
         */
        AsyncTask<Uri, Void, Boolean> task = new AsyncTask<Uri, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Uri... uris) {
                Log.v("Retreived ContactURI", uris[0].toString());

                return doesContactContainHomeEmail(uris[0]);
            }

            @Override
            protected void onPostExecute(Boolean exists) {
                if (exists) {
                    Log.v("", "Updating...");
                    updateContact();
                } else {
                    Log.v("", "Inserting...");
                    insertEmailContact();
                }
            }
        };

        task.execute(contactUri);
    }

    private Boolean doesContactContainHomeEmail(Uri contactUri) {
        boolean returnValue = false;
        Cursor mContactCursor = getContentResolver().query(contactUri, null, null, null, null);
        Log.v("Contact", "Got Contact Cursor");

        try {
            if (mContactCursor.moveToFirst()) {
                String mContactId = getCursorString(mContactCursor,
                        ContactsContract.Contacts._ID);

                Cursor mRawContactCursor = getContentResolver().query(
                        ContactsContract.RawContacts.CONTENT_URI,
                        null,
                        ContactsContract.Data.CONTACT_ID + " = ?",
                        new String[]{mContactId},
                        null);

                Log.v("RawContact", "Got RawContact Cursor");

                try {
                    ArrayList<String> mRawContactIds = new ArrayList<String>();
                    while (mRawContactCursor.moveToNext()) {
                        String rawId = getCursorString(mRawContactCursor, ContactsContract.RawContacts._ID);
                        Log.v("RawContact", "ID: " + rawId);
                        mRawContactIds.add(rawId);
                    }

                    for (String rawId : mRawContactIds) {
                        // Make sure the "last checked" RawContactId is set locally for use in insert & update.
                        mRawContactId = rawId;
                        Cursor mDataCursor = getContentResolver().query(
                                ContactsContract.Data.CONTENT_URI,
                                null,
                                ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.Email.TYPE + " = ?",
                                new String[]{mRawContactId, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE_OTHER)},
                                null);

                        if (mDataCursor.getCount() > 0) {
                            mDataCursor.moveToFirst();
                            mDataId = getCursorString(mDataCursor, ContactsContract.Data._ID);
                            Log.v("Data", "Found data item with MIMETYPE and EMAIL.TYPE");
                            mDataCursor.close();
                            returnValue = true;
                            break;
                        } else {
                            Log.v("Data", "Data doesn't contain MIMETYPE and EMAIL.TYPE");
                            mDataCursor.close();
                        }
                        returnValue = false;
                    }
                } finally {
                    mRawContactCursor.close();
                }
            }
        } catch (Exception e) {
            Log.w("UpdateContact", e.getMessage());
            for (StackTraceElement ste : e.getStackTrace()) {
                Log.w("UpdateContact", "\t" + ste.toString());
            }
            throw new RuntimeException();
        } finally {
            mContactCursor.close();
        }

        return returnValue;
    }

    private static String getCursorString(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index != -1) return cursor.getString(index);
        return null;
    }


    public void insertEmailContact() {
        try {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            ops.add(ContentProviderOperation
                    .newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, mRawContactId)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, userName)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_OTHER)
                    // .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, "Phone")
                    .build());


            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

        } catch (Exception e) {
            // Display warning
            Log.w("UpdateContact", e.getMessage());
            for (StackTraceElement ste : e.getStackTrace()) {
                Log.w("UpdateContact", "\t" + ste.toString());
            }
            Context ctx = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(ctx, "Update failed", duration);
            MyLog.e(TAG, "", e);
            toast.show();
        }
    }

    public void updateContact() {
        try {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                    .withSelection(ContactsContract.Data.RAW_CONTACT_ID + " = ?", new String[]{mRawContactId})
                    .withSelection(ContactsContract.Data._ID + " = ?", new String[]{mDataId})
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, userName)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_OTHER)

                    .build());


            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

        } catch (Exception e) {
            // Display warning
            Log.w("UpdateContact", e.getMessage());
            for (StackTraceElement ste : e.getStackTrace()) {
                Log.w("UpdateContact", "\t" + ste.toString());
            }
            Context ctx = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(ctx, getResources().getString(R.string.update_failed), duration);
            toast.show();
        }
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ivProfilePic:
                if (userAvatar != null && !userAvatar.isEmpty()) {
                    ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(UserInfo.this);

                    String profilePicVisibility = contactDB_sqlite.getProfilePicVisibility(userId);

                    if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_TO_NOBODY)) {
                        userAvatar = "";
                    }
                    Intent imgIntent = new Intent(UserInfo.this, ImageZoom.class);
                    imgIntent.putExtra("ProfilePath", userAvatar);
                    startActivity(imgIntent);
                    break;
                }

                break;

            case R.id.block_layout:
                blockUser();
                break;
            case R.id.iv_msg:
                openChatPage();
                break;

            case R.id.iv_audio_call:
                makeCall(false);
                break;

            case R.id.iv_video_call:
                makeCall(true);
                break;

            case R.id.tv_start_secret_chat:
                startSecretChat();
                break;

        }
    }

    private void startSecretChat() {
        Intent intent = new Intent(UserInfo.this, SecretChatViewActivity.class);
        intent.putExtra("receiverUid", userId);
        intent.putExtra("receiverName", userName);
        intent.putExtra("documentId", userId);
        intent.putExtra("Username", userName);
        intent.putExtra("Image", userAvatar);
        intent.putExtra("msisdn", phNo);
        intent.putExtra("type", 0);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void openChatPage() {
        Intent intent = new Intent(getApplicationContext(), ChatPageActivity.class);
        intent.putExtra("msisdn", "");
        intent.putExtra("Username", userName);
        intent.putExtra("documentId", userId);
        intent.putExtra("receiverUid", "");
        intent.putExtra("Image", userAvatar);
        intent.putExtra("type", 0);
        intent.putExtra("receiverName", userName);
        intent.putExtra("msisdn", phNo);
        if (sessionManager != null && sessionManager.getLockChatEnabled().equals("1")) {
            final String docId;
            chatType = MessageFactory.CHAT_TYPE_SINGLE;
            docId = mCurrentUserId + "-" + userId;
            String convId = userInfoSession.getChatConvId(docId);
            String receiverId = userInfoSession.getReceiverIdByConvId(convId);
            ChatLockPojo lockPojo = db.getChatLockData(receiverId, chatType);
            if (lockPojo != null) {
/*                String stat = lockPojo.getStatus();
                String pwd = lockPojo.getPassword();
                openUnlockChatDialog(mLocDbDocId,stat,pwd,-1);*/
                intent.putExtra("isLockChat", true);
            }
        }
        startActivity(intent);
    }

    private void blockUser() {
        String msg;
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        if (contactDB_sqlite.getBlockedStatus(userId, false).equals("1")) {
            msg = String.format(getResources().getString(R.string.you_want_unblock), userName);
        } else {
            msg = String.format(getResources().getString(R.string.you_want_block), userName);
        }

        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(msg);
        dialog.setPositiveButtonText(getResources().getString(R.string.ok));
        dialog.setNegativeButtonText(getResources().getString(R.string.cancel));
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                dialog.dismiss();

                putBlockUser();
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });

        dialog.show(getSupportFragmentManager(), "Block alert");
    }

    private void makeCall(boolean isVideoCall) {

        if (AppUtils.isNetworkAvailable(this)) {
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
            if (contactDB_sqlite.getBlockedStatus(userId, false).equals("1")) {
                DisplayAlert(getResources().getString(R.string.unblock) + " " + userName + " " + getResources().getString(R.string.to_place_a) + " "
                        + getString(R.string.app_name) + " " + getResources().getString(R.string.call));
            } else {
                // performCall(isVideoCall);
                //Activecall(isVideoCall);
                new CallUtil().performCall(this, isVideoCall, userId, phNo);
            }
        } else {
            Toast.makeText(context, getString(R.string.check_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }


    private void Activecall(boolean isVideoCall) {

        CallMessage message = new CallMessage(UserInfo.this);
        boolean isOutgoingCall = true;

        String roomid = message.getroomid();
        String timestamp = message.getroomid();
        String callid = mCurrentUserId + "-" + userId + "-" + timestamp;
//        CallMessage.openCallScreen(ChatViewActivity.this, mCurrentUserId, receiverUid, callid,
//                roomid, "", receiverMsisdn, "999", isVideoCall, true, timestamp);


        MyLog.d(TAG, "openCallScreen: start");
        if (!CallsActivity.isStarted) {
            if (isOutgoingCall) {
                CallsActivity.opponentUserId = userId;
            }

            PreferenceManager.setDefaultValues(context, org.appspot.apprtc.R.xml.preferences, false);
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

            String keyprefRoomServerUrl = context.getString(org.appspot.apprtc.R.string.pref_room_server_url_key);
            String roomUrl = sharedPref.getString(
                    keyprefRoomServerUrl, context.getString(org.appspot.apprtc.R.string.pref_room_server_url_default));
            MyLog.d(TAG, "openCallScreen: 1");

            int videoWidth = 0;
            int videoHeight = 0;
            String resolution = context.getString(org.appspot.apprtc.R.string.pref_resolution_default);
            String[] dimensions = resolution.split("[ x]+");
            if (dimensions.length == 2) {
                try {
                    videoWidth = Integer.parseInt(dimensions[0]);
                    videoHeight = Integer.parseInt(dimensions[1]);
                } catch (NumberFormatException e) {
                    videoWidth = 0;
                    videoHeight = 0;
                    MyLog.e("ScimboCallError", "Wrong video resolution setting: " + resolution);
                }
            }
            MyLog.d(TAG, "openCallScreen: 2");
            Uri uri = Uri.parse(roomUrl);
            Intent intent = new Intent(context, CallsActivity.class);
//            Intent intent = new Intent(context, CallNotifyService.class);

            intent.setData(uri);
            intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall);
            intent.putExtra(CallsActivity.EXTRA_DOC_ID, callid);
            intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, mCurrentUserId);
            intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, userId);
            intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, phNo);
            intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, "");
            intent.putExtra(CallsActivity.EXTRA_NAVIGATE_FROM, context.getClass().getSimpleName()); // For navigating from call activity
            intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, "0");
            intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, timestamp);

            intent.putExtra(CallsActivity.EXTRA_ROOMID, roomid);
            intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall);
            intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false);
            intent.putExtra(CallsActivity.EXTRA_CAMERA2, true);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0);
            intent.putExtra(CallsActivity.EXTRA_VIDEOCODEC, context.getString(org.appspot.apprtc.R.string.pref_videocodec_default));
            intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true);
            intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false);
            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false);
            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false);
            intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false);
            intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false);
            intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0);
            intent.putExtra(CallsActivity.EXTRA_AUDIOCODEC, context.getString(org.appspot.apprtc.R.string.pref_audiocodec_default));
            intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false);
            intent.putExtra(CallsActivity.EXTRA_TRACING, false);
            intent.putExtra(CallsActivity.EXTRA_CMDLINE, false);
            intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0);

            intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true);
            intent.putExtra(CallActivity.EXTRA_ORDERED, true);
            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1);
            intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1);
            intent.putExtra(CallActivity.EXTRA_PROTOCOL, context.getString(org.appspot.apprtc.R.string.pref_data_protocol_default));
            intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false);
            intent.putExtra(CallActivity.EXTRA_ID, -1);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            MyLog.d(TAG, "openCallScreen: 3");


        }


    }


    private void performCall(boolean isVideoCall) {

        if (ConnectivityInfo.isInternetConnected(this)) {

            if (checkAudioRecordPermission()) {
                if (!CallMessage.isAlreadyCallClick) {
                    int callType;
                    if (isVideoCall) {
                        callType = MessageFactory.video_call;
                        //callType_global = MessageFactory.video_call;
                    } else {
                        callType = MessageFactory.audio_call;
                        // callType_global = MessageFactory.audio_call;
                    }

                    CallMessage message = new CallMessage(UserInfo.this);
                    JSONObject object = (JSONObject) message.getMessageObject(userId, callType);

                    if (object != null) {
                        SendMessageEvent callEvent = new SendMessageEvent();
                        callEvent.setEventName(SocketManager.EVENT_CALL);
                        callEvent.setMessageObject(object);
                        EventBus.getDefault().post(callEvent);
                    }
                    CallMessage.setCallClickTimeout();
                } else {
                    Toast.makeText(UserInfo.this, getResources().getString(R.string.call_in_progress), Toast.LENGTH_SHORT).show();
                }

            /*Intent i = new Intent(this, CallHistoryActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT |
                    Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);

            startActivity(i);*/

            } else {
                requestAudioRecordPermission();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.check_your_internet_connection), Toast.LENGTH_SHORT).show();
        }

    }

    private void requestAudioRecordPermission() {
        ActivityCompat.requestPermissions(UserInfo.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);
    }

    private void DisplayAlert(final String txt) {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(txt);
        dialog.setNegativeButtonText(getResources().getString(R.string.cancel));
        dialog.setPositiveButtonText(getResources().getString(R.string.unblock));
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                putBlockUser();
                dialog.dismiss();
            }

            @Override

            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Unblock a person");

    }

    private void putBlockUser() {
        BlockUserUtils.changeUserBlockedStatus(UserInfo.this, EventBus.getDefault(),
                mCurrentUserId, userId, false);
    }

    private void blockunblockcontact(ReceviceMessageEvent event) {
        String toid = "", fromid = "";
        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());

            String stat = object.getString("status");
            toid = object.getString("to");
            fromid = object.getString("from");

            if (mCurrentUserId.equalsIgnoreCase(fromid) && toid.equalsIgnoreCase(userId)) {

                if (stat.equalsIgnoreCase("1")) {
                    tvBlock.setText("Unblock");
                    Toast.makeText(this, getResources().getString(R.string.number_is_blocked), Toast.LENGTH_SHORT).show();
                } else {
                    tvBlock.setText("Block");
                    Toast.makeText(this, getResources().getString(R.string.number_is_unblocked), Toast.LENGTH_SHORT).show();
                }
            } else if (mCurrentUserId.equalsIgnoreCase(toid) && fromid.equalsIgnoreCase(userId)) {
               /* getcontactname.configProfilepic(ivProfilePic, userId, true, false, R.drawable.ic_placeholder_black);

                if (stat.equalsIgnoreCase("1")) {
                    statusTextView.setVisibility(View.GONE);

                } else {
                    statusTextView.setVisibility(View.VISIBLE);
                }*/
            }

        } catch (Exception e) {
            MyLog.e(TAG, "blockunblockcontact: ", e);
        }
    }

    private void changemute() {
        Intent muteintant = new Intent();
        muteintant.putExtra("muteactivity", ismutecheckchange);
        setResult(RESULT_OK, muteintant);
    }

    @Override
    public void onMuteDialogClosed(boolean isMuted) {
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        String convId = userInfoSession.getChatConvId(mLocDbDocId);
        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(mCurrentUserId, userId, convId, isSecretUserInfo);

        if (muteData != null && muteData.getMuteStatus().equals("1")) {
            swMute.setChecked(true);
        } else {
            swMute.setChecked(false);

        }
    }

    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

        private List<MessageItemChat> horizontalList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView image, arrow, Vido;
            public AvnNextLTProDemiTextView duration;
            public View durationBg;

            public MyViewHolder(View view) {
                super(view);
                image = view.findViewById(R.id.Image);
                arrow = view.findViewById(R.id.arrow);
                Vido = view.findViewById(R.id.Vido);
                duration = view.findViewById(R.id.duration);
                durationBg = view.findViewById(R.id.duration_background);

            }
        }


        public HorizontalAdapter(List<MessageItemChat> horizontalList) {
            this.horizontalList = horizontalList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.horizontal_item_view, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.Vido.setVisibility(View.GONE);
            holder.duration.setVisibility(View.GONE);
            holder.durationBg.setVisibility(View.GONE);
            if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.picture)) {


                if (horizontalList.get(position).getImagePath() != null) {
                    String path = horizontalList.get(position).getImagePath();

                    AppUtils.loadLocalImage(UserInfo.this, path, holder.image);
                } else if (horizontalList.get(position).getChatFileLocalPath() != null) {
                    String path = horizontalList.get(position).getChatFileLocalPath();

                    AppUtils.loadLocalImage(UserInfo.this, path, holder.image);
                }
            } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.video)) {
                try {
                    if (horizontalList.get(position).getVideoPath() != null) {
                        String path = horizontalList.get(position).getVideoPath();
                        AppUtils.loadLocalVideoThumbanail(UserInfo.this, path, holder.image);
                        try {
                            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                            mdr.setDataSource(horizontalList.get(position).getChatFileLocalPath());
                            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            String setduration = getTimeString(Long.parseLong(duration));
                            holder.Vido.setVisibility(View.VISIBLE);
                            holder.duration.setVisibility(View.VISIBLE);
                            holder.durationBg.setVisibility(View.VISIBLE);
                            holder.duration.setText(setduration);
                        } catch (Exception e) {
                            MyLog.e(TAG, "onBindViewHolder: ", e);
                        }


                    } else if (horizontalList.get(position).getChatFileLocalPath() != null) {
                        String path = horizontalList.get(position).getChatFileLocalPath();
                        AppUtils.loadLocalVideoThumbanail(UserInfo.this, path, holder.image);
                        try {
                            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                            mdr.setDataSource(horizontalList.get(position).getChatFileLocalPath());
                            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            String setduration = getTimeString(Long.parseLong(duration));
                            holder.Vido.setVisibility(View.VISIBLE);
                            holder.duration.setVisibility(View.VISIBLE);
                            holder.durationBg.setVisibility(View.VISIBLE);
                            holder.duration.setText(setduration);
                        } catch (Exception e) {
                            MyLog.e(TAG, "onBindViewHolder: ", e);
                        }
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
            } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.audio)) {
                if (horizontalList.get(position).getaudiotype() == MessageFactory.AUDIO_FROM_RECORD) {
                    holder.Vido.setVisibility(View.GONE);
                    holder.duration.setVisibility(View.VISIBLE);
                    holder.durationBg.setVisibility(View.VISIBLE);
                    holder.image.setImageResource(R.drawable.ic_placeholder_message);
                    String path = horizontalList.get(position).getChatFileLocalPath();
                    File file = new File(path);
                    String duration = "";
                    if (file.exists()) {
                        duration = horizontalList.get(position).getDuration();
                    }
                    holder.duration.setText(duration);
                } else if (horizontalList.get(position).getaudiotype() == MessageFactory.AUDIO_FROM_ATTACHMENT) {
                    holder.Vido.setVisibility(View.GONE);
                    holder.duration.setVisibility(View.VISIBLE);
                    holder.durationBg.setVisibility(View.VISIBLE);
                    holder.image.setImageResource(R.drawable.ic_placeholder_message);
                    String path = horizontalList.get(position).getChatFileLocalPath();
                    File file = new File(path);
                    String duration = "";
                    if (file.exists()) {
                        duration = horizontalList.get(position).getDuration();
                    }
                    holder.duration.setText(duration);
                }
            } else if (Integer.parseInt(horizontalList.get(position).getMessageType()) == (MessageFactory.document)) {
                holder.Vido.setVisibility(View.GONE);
                holder.duration.setVisibility(View.GONE);
                holder.durationBg.setVisibility(View.GONE);
                String path = horizontalList.get(position).getChatFileLocalPath();
                String extension = FileUploadDownloadManager.getFileExtnFromPath(path);
                if (extension.contains("txt")) {
                    holder.image.setImageResource(R.drawable.ic_media_txt);
                } else if (extension.contains("doc")) {
                    holder.image.setImageResource(R.drawable.ic_media_doc);
                } else if (extension.contains("ppt")) {
                    holder.image.setImageResource(R.drawable.ic_media_ppt);
                } else if (extension.contains("xls")) {
                    holder.image.setImageResource(R.drawable.ic_media_xls);
                } else if (extension.contains("pdf")) {
                    holder.image.setImageResource(R.drawable.ic_media_pdf);
                }


            }

            if (horizontalList.size() >= 5) {
                if (horizontalList.size() - 1 == position) {
                    holder.arrow.setVisibility(View.VISIBLE);
                    holder.arrow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(UserInfo.this, MediaAcitivity.class);
                            intent.putExtra("username", userName);
                            intent.putExtra("docid", mLocDbDocId);
                            startActivity(intent);
                            // Toast.makeText(UserInfo.this,holder.txtView.getText().toString(),Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    holder.arrow.setVisibility(View.GONE);
                    holder.arrow.setOnClickListener(null);
                }

            }

            // holder.image.setText(horizontalList.get(position));
        }


        //--------------------Rotate------------------------------------

        public Bitmap RotateBitmap(Bitmap source, float angle) {
            try {
                if (source != null) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(angle);
                    int wid = source.getWidth();
                    int hig = source.getHeight();

                    return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
                }
            } catch (Exception e) {
                MyLog.e(TAG, "RotateBitmap: ", e);
            }
            return null;
        }

        @Override
        public int getItemCount() {
            return horizontalList.size();
        }

    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf

                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_PRIVACY_SETTINGS)) {
            loadPrivacySetting(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_USER_DETAILS)) {
            loadUserDetails(event);
            statusUpdation();
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_MUTE)) {
            loadMuteMessage(event);
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_BLOCK_USER)) {
            blockunblockcontact(event);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(StatusRefresh event) {
        if (event != null && event.getUserId() != null && event.getStatus() != null) {
            if (userId != null && userId.equalsIgnoreCase(event.getUserId())) {
                //ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
                //contactDB_sqlite.updateMyContactStatus(userId,event.getStatus());
                statusUpdation();
            }
        }
    }

    public void getUserDetails(String userId) {
        JSONObject eventObj = Getcontactname.getUserDetailsObject(this, userId);
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_GET_USER_DETAILS);
        event.setMessageObject(eventObj);
        EventBus.getDefault().post(event);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void loadMuteMessage(ReceviceMessageEvent event) {

        try {
            JSONObject object = new JSONObject(event.getObjectsArray()[0].toString());
            String from = object.getString("from");

            if (from.equalsIgnoreCase(mCurrentUserId)) {
                String convId = object.getString("convId");

                String to;
                if (object.has("to")) {
                    to = object.getString("to");
                } else {
                    to = userInfoSession.getReceiverIdByConvId(convId);
                }

                if (to != null && !to.equals("") && to.equalsIgnoreCase(userId)) {
                    String secretType = "no";
                    if (object.has("secret_type")) {
                        secretType = object.getString("secret_type");
                    }
                    boolean isSecretChat = secretType.equalsIgnoreCase("yes");

                    String status = object.getString("status");
                    if (status.equals("1")) {
                        if (isSecretChat == isSecretUserInfo) {
                            swMute.setChecked(true);
                        }
                    } else {
                        if (isSecretChat == isSecretUserInfo) {
                            swMute.setChecked(false);
                        }
                    }

                    hideProgressDialog();
                }
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void loadUserDetails(ReceviceMessageEvent event) {

        Object[] data = event.getObjectsArray();

        try {
            JSONObject object = new JSONObject(data[0].toString());
            Log.d("response: ", "sc_get_user_Details: " + object.toString());
            if (object.has("Status")) {
                status = object.getString("Status");
                byte[] nameBuffer = Base64.decode(status, Base64.DEFAULT);
                status = new String(nameBuffer, StandardCharsets.UTF_8);
            } else {
                status = getResources().getString(R.string.default_user_status);
            }
            if (object.has("pinCode")) {
                pincode = object.getString("pinCode");
//                byte[] nameBuffer = Base64.decode(status, Base64.DEFAULT);
//                pincode = new String(nameBuffer, StandardCharsets.UTF_8);
            } else {
                pincode = getResources().getString(R.string.default_user_status);
            }

        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }

    }

    private void loadPrivacySetting(ReceviceMessageEvent event) {
        Object[] object = event.getObjectsArray();
        //System.out.println("OBJECt--->" + object);
        try {
            JSONObject jsonObject = new JSONObject(object[0].toString());
            String status = (String) jsonObject.get("status");
            String lastseen = String.valueOf(jsonObject.get("last_seen"));
            String profile = String.valueOf(jsonObject.get("profile_photo"));
            JSONArray contactUserList = jsonObject.getJSONArray("contactUserList");
            Boolean iscontact = false;
            if (contactUserList != null) {
                iscontact = contactUserList.toString().contains(mCurrentUserId);
            }
            if (userAvatar != null && !userAvatar.equals("")) {
                if (profile.equalsIgnoreCase("nobody")) {
                    ivProfilePic.setImageResource(R.drawable.ic_placeholder_black);
                } else if (profile.equalsIgnoreCase("everyone")) {

                    //Glide.with(UserInfo.this).load(userAvatar).into(ivProfilePic);
                    AppUtils.loadImage(UserInfo.this, AppUtils.getValidProfilePath(userAvatar), ivProfilePic, 0, R.drawable.ic_placeholder_black);
                } else if (profile.equalsIgnoreCase("mycontacts") && iscontact) {
                    AppUtils.loadImage(UserInfo.this, AppUtils.getValidProfilePath(userAvatar), ivProfilePic, 0, R.drawable.ic_placeholder_black);
                } else {
                    ivProfilePic.setImageResource(R.drawable.ic_placeholder_black);
                }
            }
            if (status.equalsIgnoreCase("nobody")) {
                tvStatus.setVisibility(View.GONE);
            } else if (status.equalsIgnoreCase("everyone")) {
                tvStatus.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("mycontacts") && iscontact) {
                tvStatus.setVisibility(View.VISIBLE);
            } else {
                tvStatus.setVisibility(View.GONE);
            }

        } catch (Exception e) {

        }
    }

    private void profilepicUpdation() {

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

        if (contactDB_sqlite.getBlockedMineStatus(userId, isSecretUserInfo).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
            Glide.with(context).load(R.drawable.ic_placeholder_black).into(ivProfilePic);
        } else {

            String profilePicVisibility = contactDB_sqlite.getProfilePicVisibility(userId);

            if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_NOBODY)) {
                Glide.with(context).load(R.drawable.ic_placeholder_black).into(ivProfilePic);
            } else {

                boolean canShow = false;
                if (profilePicVisibility.equals(ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE)) {
                    canShow = true;
                } else {
                    if (contactDB_sqlite.getMyContactStatus(userId).equals("1")) {
                        canShow = true;
                    }
                }

                if (canShow) {
                    if (userAvatar != null) {

/*                        Picasso picasso = Picasso.with(context);
                        RequestCreator requestCreator = picasso.load(userAvatar).error(
                                R.drawable.ic_placeholder_black);
                        requestCreator.into(ivProfilePic);*/
                        AppUtils.loadImage(context, AppUtils.getValidProfilePath(userAvatar), ivProfilePic, 0, R.drawable.ic_placeholder_black);
                    } else {
                        Glide.with(context).load(R.drawable.ic_placeholder_black).into(ivProfilePic);
                    }
                } else {

                    Glide.with(context).load(R.drawable.ic_placeholder_black).into(ivProfilePic);
                }
            }
        }

    }


    private void statusUpdation() {

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        ArrayList<FriendModel> scimboEntries = contactDB_sqlite.getSavedFriendModels();

        boolean isScimboContact = false;

        if (scimboEntries != null) {
            for (int i = 0; i < scimboEntries.size(); i++) {
                FriendModel contactModel = scimboEntries.get(i);
                if (userId.equalsIgnoreCase(contactModel.get_id())) {
                    status = contactModel.getStatus();
                    phNo = contactModel.getNumberInDevice();
                    isScimboContact = true;

                    String msisdn = contactModel.getMsisdn();
                    if (msisdn != null && !msisdn.equals("")) {
                        tvPhone.setText(msisdn);
                    } else {
                        tvPhone.setText(phNo);
                    }
                    break;
                }
            }
        }

        if (!isScimboContact) {

            FriendModel contact = contactDB_sqlite.getFriendDetails(userId);
            if (contact != null) {
                status = contact.getStatus();
                tvPhone.setText(contact.getMsisdn());
                phNo = contact.getMsisdn();
            }
        }
        tvPinCode.setText(pincode);
        Getcontactname objGetContactName = new Getcontactname(this);
        boolean isDisplayed = objGetContactName.setProfileStatusText(tvStatus, userId, status, isSecretUserInfo);
        if (!isDisplayed) {
            tvStatus.setVisibility(View.GONE);
        } else {
            tvStatus.setVisibility(View.VISIBLE);
        }
    }

    private void makeCollapsingToolbarLayoutLooksGood(CollapsingToolbarLayout collapsingToolbarLayout) {
        try {
            final Field field = collapsingToolbarLayout.getClass().getDeclaredField("mCollapsingTextHelper");
            field.setAccessible(true);
            final Object object = field.get(collapsingToolbarLayout);
            final Field tpf = object.getClass().getDeclaredField("mTextPaint");
            tpf.setAccessible(true);
            avnDemiFont = CoreController.getInstance().getAvnNextLTProDemiTypeface();
            ((TextPaint) tpf.get(object)).setTypeface(avnDemiFont);
            ((TextPaint) tpf.get(object)).setColor(getResources().getColor(R.color.white));
        } catch (Exception ignored) {
        }
    }

    private ChatLockPojo getChatLockdetailfromDB(int position) {
        CommonInGroupPojo e = adapter.getItem(position);
        String groupId = e.getGroupId();
        String id = mCurrentUserId.concat("-").concat(groupId).concat("-g");
        MessageDbController dbController = CoreController.getDBInstance(this);
        String convId = userInfoSession.getChatConvId(id);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo pojo = dbController.getChatLockData(receiverId, MessageFactory.CHAT_TYPE_GROUP);
        return pojo;
    }

    /*
    Target profileImageTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            ivProfilePic.setImageBitmap(bitmap);
            //BitmapDrawable bit=new BitmapDrawable(bitmap);
            //collapsingToolbarLayout.setBackgroundDrawable(bit);
            ivProfilePic.setOnClickListener(UserInfo.this);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };*/

    SimpleTarget profileImageTarget = new SimpleTarget<Bitmap>() {
        @Override
        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
            ivProfilePic.setImageBitmap(resource);
        }
    };
}





