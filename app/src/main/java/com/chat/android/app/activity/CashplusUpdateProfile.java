package com.chat.android.app.activity;


import static com.chat.android.backend.Constants.OccupationTypes;
import static com.chat.android.core.CoreActivity.showToast;
import static com.chat.android.utils.DialogUtils.dismiss;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Layout;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.dialog.CustomMultiTextItemsDialog;
import com.chat.android.app.model.ApiRequestModel;
import com.chat.android.app.model.CashplusUpdateUserProfile;
import com.chat.android.app.model.GetBankPayoutLocations;
import com.chat.android.app.model.GetCities;
import com.chat.android.app.model.GetDestinations;
import com.chat.android.app.model.GetIDCardTypes;
import com.chat.android.app.model.GetOccupationTypes;
import com.chat.android.app.model.NationalityModel;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.ConnectivityInfo;
import com.chat.android.app.utils.MyLog;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.MultiTextDialogPojo;
import com.chat.android.core.uploadtoserver.FileUploadDownloadManager;
import com.chat.android.interfaces.INetworkResponseListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kosalgeek.android.imagebase64encoder.ImageBase64;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class CashplusUpdateProfile extends AppCompatActivity implements INetworkResponseListener {
    private Button mBtnSave;
    private SessionManager sessionManager;
    private EditText spinnerIdentity;
    private EditText spinnerCountry;
    private EditText spinnerOccupation;
    private ImageView backimg_uprofile;
    private int SELECTED_IMAGE = 0;
    final Calendar DobCalendar = Calendar.getInstance();
    final Calendar issueDateCalender = Calendar.getInstance();
    final Calendar expiryDateCalender = Calendar.getInstance();
    private EditText firstName;
    private EditText lastName;
    private EditText middleName;
    private EditText userName;
    private EditText address;
    private EditText nationality;
    private EditText Id_SerialNumber;
    private EditText issuePlace;
    private EditText spinnerCity;
    private EditText dob;
    private EditText expiry_date;
    private EditText issue_date;
    private RadioGroup gender, expiry;
    private ProgressDialog progressDialog;
    private String mFrontImage;
    private String mBackImage;
    private String mPortraitImage;
    private String mGender;
    private String mExpiry = "Y";
    private String mCountryCode;
    private String mNationality;
    private String firstName_;
    private String lastName_;
    private String middleName_;
    private String userName_;
    private String address_;
    private String nationality_;
    private String Id_SerialNumber_;
    private String issuePlace_;
    private String occupation_;
    private TextView lbl_all_set;
    private GetIDCardTypes.IDCardType idCardType;
    private GetOccupationTypes.OccupationType occupationType;
    private List<NationalityModel> nationalityModelList = new ArrayList<>();
    private List<GetOccupationTypes.OccupationType> OccupationList = new ArrayList<>();
    private List<GetIDCardTypes.IDCardType> cardList = new ArrayList<>();
    private ImageView backMark;
    private ImageView frontMark;
    private ImageView portraitMark;
    private TextView idFront;
    private TextView idBack;
    private TextView idPortrait;
    RadioButton rbYes;
    AlertDialog.Builder builderSingle;
    String json;
    ArrayAdapter<NationalityModel> filter_adapter;
    ArrayAdapter<GetOccupationTypes.OccupationType> occupation_filter_adapter;
    AlertDialog ad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashplus_update_profile);
        getSupportActionBar().hide();
        initViews();
        initValues();
        progressDialog.show();
        ApiRequestModel apiRequestModel = new ApiRequestModel(SessionManager.getInstance(this).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getApplicationContext()).getCashPlusToken());
        ApiCalls.getInstance(getApplicationContext()).GetIDCardTypes(getApplicationContext(), apiRequestModel, this);
        ApiCalls.getInstance(getApplicationContext()).GetOccupationTypes(getApplicationContext(), apiRequestModel, this);
        readExcelFileFromAssets();
    }

    private void initViews() {
        progressDialog = new ProgressDialog(CashplusUpdateProfile.this);
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getInstance(CashplusUpdateProfile.this);
        mBtnSave = findViewById(R.id.btn_account_save);
        spinnerCountry = findViewById(R.id.spinnerCountry);
        spinnerCity = findViewById(R.id.city);
        lbl_all_set = findViewById(R.id.lbl_all_set);
        backimg_uprofile = findViewById(R.id.backarrow_userprofile);
        dob = findViewById(R.id.dob);
        expiry_date = findViewById(R.id.id_expiry);
        issue_date = findViewById(R.id.issuedate);
        firstName = findViewById(R.id.f_name);
        middleName = findViewById(R.id.m_name);
        lastName = findViewById(R.id.l_name);
        userName = findViewById(R.id.user_name_cashplus);
        address = findViewById(R.id.address);
        spinnerOccupation = findViewById(R.id.spinnerOccupation);
        spinnerIdentity = findViewById(R.id.spinnerIdentity);

        nationality = findViewById(R.id.nationality);
        Id_SerialNumber = findViewById(R.id.serial_number);
        issuePlace = findViewById(R.id.issueplace);
        gender = findViewById(R.id.rgGender);
        expiry = findViewById(R.id.rbExpiry);
        frontMark = findViewById(R.id.frontMark);
        backMark = findViewById(R.id.backMark);
        portraitMark = findViewById(R.id.portraitMark);
        idFront = (TextView) findViewById(R.id.uploadidfront);
        idBack = (TextView) findViewById(R.id.uploadidback);
        idPortrait = (TextView) findViewById(R.id.uploadportrait);

        rbYes = (RadioButton) findViewById(R.id.rbYes);
        rbYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b){
                    expiry_date.setVisibility(View.GONE);
                    issuePlace.setVisibility(View.GONE);
                    issue_date.setVisibility(View.GONE);
                }else{
                    expiry_date.setVisibility(View.VISIBLE);
                    issuePlace.setVisibility(View.VISIBLE);
                    issue_date.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initValues() {
        userName.setText(SessionManager.getInstance(getApplicationContext()).getCashplusUserName());
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInput(view)) {
                    UpdateProfile();
                }else {
                    Toast.makeText(CashplusUpdateProfile.this, getResources().getString(R.string.field_must_not_be_empty), Toast.LENGTH_SHORT).show();
                }
            }
        });
        idFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SELECTED_IMAGE = 1;
                selectImage();
            }
        });
        idBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SELECTED_IMAGE = 2;
                selectImage();

            }

        });
        idPortrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SELECTED_IMAGE = 3;
                selectImage();

            }
        });
        spinnerCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCountry();

            }
        });
        spinnerOccupation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOccupation();

            }
        });
        spinnerIdentity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setIdentity();

            }
        });

        backimg_uprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final DatePickerDialog.OnDateSetListener dobListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
               
                DobCalendar.set(Calendar.YEAR, year);
                DobCalendar.set(Calendar.MONTH, monthOfYear);
                DobCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                dob.setText(sdf.format(DobCalendar.getTime()));

            }
        };
        final DatePickerDialog.OnDateSetListener issueListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                issueDateCalender.set(Calendar.YEAR, year);
                issueDateCalender.set(Calendar.MONTH, monthOfYear);
                issueDateCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                issue_date.setText(sdf.format(issueDateCalender.getTime()));


            }
        };
        final DatePickerDialog.OnDateSetListener expiryListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                expiryDateCalender.set(Calendar.YEAR, year);
                expiryDateCalender.set(Calendar.MONTH, monthOfYear);
                expiryDateCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                expiry_date.setText(sdf.format(expiryDateCalender.getTime()));


            }
        };
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                DatePickerDialog datePickerDialog = new DatePickerDialog(CashplusUpdateProfile.this, R.style.DialogTheme, dobListener, DobCalendar
                        .get(Calendar.YEAR), DobCalendar.get(Calendar.MONTH),
                        DobCalendar.get(Calendar.DAY_OF_MONTH));
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.YEAR, -10);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });
        expiry_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                DatePickerDialog datePickerDialog = new DatePickerDialog(CashplusUpdateProfile.this, R.style.DialogTheme, expiryListener, expiryDateCalender
                        .get(Calendar.YEAR), expiryDateCalender.get(Calendar.MONTH),
                        expiryDateCalender.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        issue_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                DatePickerDialog datePickerDialog = new DatePickerDialog(CashplusUpdateProfile.this, R.style.DialogTheme, issueListener, issueDateCalender
                        .get(Calendar.YEAR), issueDateCalender.get(Calendar.MONTH),
                        issueDateCalender.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });


    }

    private void UpdateProfile() {
        RadioButton genderButton = (RadioButton) findViewById(gender.getCheckedRadioButtonId());
        if (genderButton.getText().toString().equals("female")) {
            mGender = "F";
        } else {
            mGender = "M";
        }
        RadioButton ExpiryButton = (RadioButton) findViewById(expiry.getCheckedRadioButtonId());
        if (ExpiryButton.getText().toString().equals("yes")) {
            mExpiry = "Y";
        } else {
            mExpiry = "N";
        }

        progressDialog.show();

        CashplusUpdateUserProfile cashplusUpdateUserProfile = new CashplusUpdateUserProfile();
        cashplusUpdateUserProfile.setUserName(userName.getText().toString());
        cashplusUpdateUserProfile.setAddress(address.getText().toString());
        cashplusUpdateUserProfile.setCity(spinnerCity.getText().toString());
        cashplusUpdateUserProfile.setDob(dob.getText().toString());
        cashplusUpdateUserProfile.setFname(firstName.getText().toString());
        cashplusUpdateUserProfile.setLname(lastName.getText().toString());
        cashplusUpdateUserProfile.setmName(middleName.getText().toString());
        cashplusUpdateUserProfile.setFullName(firstName.getText().toString() + " " + middleName.getText().toString() + " " + lastName.getText().toString());
        cashplusUpdateUserProfile.setGender(mGender);
        cashplusUpdateUserProfile.setiD_EXPIRY(expiry_date.getText().toString());
        cashplusUpdateUserProfile.setiD_HAS_NO_EXPIRY(mExpiry);
        cashplusUpdateUserProfile.setiD_ISSUE_PLACE(issuePlace.getText().toString());
        cashplusUpdateUserProfile.setiD_TYPE(idCardType.name);
        cashplusUpdateUserProfile.setiD_SERIAL(Id_SerialNumber.getText().toString());
        cashplusUpdateUserProfile.setNationality(mCountryCode);
        cashplusUpdateUserProfile.setiD_FACE_MIME_IMAGE(mFrontImage);
        cashplusUpdateUserProfile.setPortrait_MIME_IMAGE(mPortraitImage);
        cashplusUpdateUserProfile.setiD_BACK_MIME_IMAGE(mBackImage);
        cashplusUpdateUserProfile.setiD_ISSUE_DATE(issue_date.getText().toString());
        cashplusUpdateUserProfile.setOccupation(occupationType.name);
        cashplusUpdateUserProfile.setToken(SessionManager.getInstance(getApplicationContext()).getCashPlusToken());
        ApiCalls.getInstance(getApplicationContext()).CashPLusUpdateUserProfile(getApplicationContext(), cashplusUpdateUserProfile, this);
    }

    private void setCountry() {
        builderSingle = new AlertDialog.Builder(CashplusUpdateProfile.this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.listview_dialog, null);
        final ListView listview = (ListView) dialogView.findViewById(R.id.select_dialog_listview);
        final TextView title = (TextView)dialogView.findViewById(R.id.title);
        title.setText("Select Nationality");
        final EditText search_edit = (EditText)dialogView.findViewById(R.id.search_edit);
        search_edit.setHint("Select Nationality");
        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                if (!editable.toString().trim().equals("")) {

                    Gson gson = new Gson();
                    json = gson.toJson(nationalityModelList);
                    JSONArray newArray = new JSONArray();
                    try {
                        JSONArray jsonArray = new JSONArray(json);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject currentElement = jsonArray.getJSONObject(i);
                            String country = currentElement.getString("Nationalty");
                            String country_lwr = country.toLowerCase();
                            String query = editable.toString().toLowerCase();
                            if (country_lwr.contains(query)) {
                                newArray.put(currentElement);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Type listType = new TypeToken<List<NationalityModel>>() {
                    }.getType();
                    List<NationalityModel> myModelList = new Gson().fromJson(String.valueOf(newArray), listType);

                    filter_adapter = new ArrayAdapter<>(CashplusUpdateProfile.this, android.R.layout.select_dialog_item, myModelList);
                    listview.setAdapter(filter_adapter);
                }
            }
        });
        final ImageView search_icon = (ImageView)dialogView.findViewById(R.id.search_icon);
        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setVisibility(View.GONE);
                search_edit.setVisibility(View.VISIBLE);
                search_icon.setVisibility(View.GONE);
            }
        });
        final ArrayAdapter<NationalityModel> adapter = new ArrayAdapter<>(CashplusUpdateProfile.this, android.R.layout.select_dialog_item, nationalityModelList);

        listview.setAdapter(adapter);
        builderSingle.setView(dialogView);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                NationalityModel nationalityModel;
                if (json!=null && !json.equals("")){
                    nationalityModel = filter_adapter.getItem(i);
                }else {
                    nationalityModel = adapter.getItem(i);
                }
                mNationality = nationalityModel.getNationalty();
                nationality.setText(mNationality);
                spinnerCountry.setText(nationalityModel.getCountryName());
                json = "";
                ad.dismiss();
            }
        });


//        AlertDialog.Builder builderSingle = new AlertDialog.Builder(CashplusUpdateProfile.this);
//        builderSingle.setTitle("Select Country");
//        final ArrayAdapter<NationalityModel> adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, nationalityModelList);
////        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialog, int which) {
////                dialog.dismiss();
////            }
////        });
//        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                NationalityModel nationalityModel = adapter.getItem(which);
//                mNationality = nationalityModel.getNationalty();
//                nationality.setText(mNationality);
//                spinnerCountry.setText(nationalityModel.getCountryName());
//                dismiss();
//            }
//        });
       ad = builderSingle.show();


    }

    private void setIdentity() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(CashplusUpdateProfile.this);
        builderSingle.setTitle("Select Identity");
        final ArrayAdapter<GetIDCardTypes.IDCardType> adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, cardList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                idCardType = adapter.getItem(which);
                spinnerIdentity.setText(idCardType.name);
                dismiss();
            }
        });
        builderSingle.show();


    }

    private void setOccupation() {

        builderSingle = new AlertDialog.Builder(CashplusUpdateProfile.this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.listview_dialog, null);
        final ListView listview = (ListView) dialogView.findViewById(R.id.select_dialog_listview);
        final TextView title = (TextView)dialogView.findViewById(R.id.title);
        title.setText("Select Occupation");
        final EditText search_edit = (EditText)dialogView.findViewById(R.id.search_edit);
        search_edit.setHint("Select Occupation");
        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                if (!editable.toString().trim().equals("")) {

                    Gson gson = new Gson();
                    json = gson.toJson(OccupationList);
                    JSONArray newArray = new JSONArray();
                    try {
                        JSONArray jsonArray = new JSONArray(json);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject currentElement = jsonArray.getJSONObject(i);
                            String country = currentElement.getString("Name");
                            String country_lwr = country.toLowerCase();
                            String query = editable.toString().toLowerCase();
                            if (country_lwr.contains(query)) {
                                newArray.put(currentElement);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Type listType = new TypeToken<List<GetOccupationTypes.OccupationType>>() {
                    }.getType();
                    List<GetOccupationTypes.OccupationType> myModelList = new Gson().fromJson(String.valueOf(newArray), listType);

                    occupation_filter_adapter = new ArrayAdapter<>(CashplusUpdateProfile.this, android.R.layout.select_dialog_item, myModelList);
                    listview.setAdapter(occupation_filter_adapter);
                }
            }
        });
        final ImageView search_icon = (ImageView)dialogView.findViewById(R.id.search_icon);
        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setVisibility(View.GONE);
                search_edit.setVisibility(View.VISIBLE);
                search_icon.setVisibility(View.GONE);
            }
        });
        final ArrayAdapter<GetOccupationTypes.OccupationType> adapter = new ArrayAdapter<>(CashplusUpdateProfile.this, android.R.layout.select_dialog_item, OccupationList);

        listview.setAdapter(adapter);
        builderSingle.setView(dialogView);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (json!=null && !json.equals("")){
                    occupationType = occupation_filter_adapter.getItem(i);
                }else {
                    occupationType = adapter.getItem(i);
                }
                spinnerOccupation.setText(occupationType.name);
                json = "";
                ad.dismiss();
            }
        });


//        AlertDialog.Builder builderSingle = new AlertDialog.Builder(CashplusUpdateProfile.this);
//        builderSingle.setTitle("Select Occupation");
//        final ArrayAdapter<GetOccupationTypes.OccupationType> adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, OccupationList);
//        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                occupationType = adapter.getItem(which);
//                spinnerOccupation.setText(occupationType.name);
//                dismiss();
//            }
//        });
        ad = builderSingle.show();


    }


    public boolean validateInput(View view) {
        if (view.getId() == R.id.btn_account_save) {
            firstName_ = firstName.getText().toString();
            lastName_ = lastName.getText().toString();
            middleName_ = middleName.getText().toString();
            userName_ = userName.getText().toString();
            address_ = address.getText().toString();
            nationality_ = nationality.getText().toString();
            occupation_ = spinnerOccupation.getText().toString();
            Id_SerialNumber_ = Id_SerialNumber.getText().toString();
            issuePlace_ = issuePlace.getText().toString();
            if (firstName_.equals("") || middleName_.equals("") || lastName_.equals("") || userName_.equals("") || address_.equals("") || spinnerCountry.getText().toString().equals("") || spinnerCity.getText().toString().equals("") || nationality_.equals("") || occupation_.equals("") || Id_SerialNumber.equals("") || dob.getText().toString().equals("")) {
                Toast.makeText(CashplusUpdateProfile.this, getResources().getString(R.string.field_must_not_be_empty), Toast.LENGTH_SHORT).show();
                return false;

            }

            if (rbYes.isChecked()){
                if(issuePlace_.equals("") || expiry_date.getText().toString().equals("") || issue_date.getText().toString().equals(""))
                {
                    Toast.makeText(CashplusUpdateProfile.this, getResources().getString(R.string.field_must_not_be_empty), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }

            if (TextUtils.isEmpty(mFrontImage) || TextUtils.isEmpty(mBackImage) || TextUtils.isEmpty(mPortraitImage)){
                return false;
            }
        } else if (view.getId() == R.id.backarrow_userprofile) {
            finish();
            return false;
        }
        return true;
    }


    public void readExcelFileFromAssets() {
        try {
            InputStream myInput;
            AssetManager assetManager = getAssets();
            myInput = assetManager.open("nationalities.xls");
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);
            Iterator<Row> rowIter = mySheet.rowIterator();
            int rowno = 0;
            lbl_all_set.append("\n");
            while (rowIter.hasNext()) {
                HSSFRow myRow = (HSSFRow) rowIter.next();
                if (rowno != 0) {
                    Iterator<Cell> cellIter = myRow.cellIterator();
                    int colno = 0;
                    String code = "", name = "", nationality = "";
                    while (cellIter.hasNext()) {
                        HSSFCell myCell = (HSSFCell) cellIter.next();
                        if (colno == 0) {
                            code = myCell.toString();
                        } else if (colno == 1) {
                            name = myCell.toString();
                        } else if (colno == 2) {
                            nationality = myCell.toString();
                        }
                        colno++;
                    }
                    NationalityModel nationalityModel = new NationalityModel(code, name, nationality);
                    nationalityModelList.add(nationalityModel);
                }

                rowno++;
            }

        } catch (Exception e) {
            Log.e("+++", "error " + e.toString());
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(CashplusUpdateProfile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    File fileImagePath = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(CashplusUpdateProfile.this, BuildConfig.APPLICATION_ID, fileImagePath));
//                    startActivityForResult(intent, 1);
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    try {
                        startActivityForResult(takePictureIntent, 1);
                    } catch (ActivityNotFoundException e) {
                        // display error state to the user
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                try {

                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    //BitmapToBase64(imageBitmap);
                    //encodeImage(imageBitmap);
                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);

                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File filepath = new File(getRealPathFromURI(tempUri));
                    convertToBase64(filepath.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                //BitmapToBase64(thumbnail);
                convertToBase64(picturePath);
                //encodeImage(picturePath);
            }
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        Bitmap OutImage = Bitmap.createScaledBitmap(inImage, 1000, 1000,true);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), OutImage, "Title", null);
        return Uri.parse(path);
    }
    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }
    private void convertToBase64(String filepath){
        try {
            String encImage = ImageBase64.with(CashplusUpdateProfile.this).encodeFile(filepath);

            if (SELECTED_IMAGE == 1) {
                mFrontImage = encImage;
                frontMark.setVisibility(View.VISIBLE);
            }
            if (SELECTED_IMAGE == 2) {
                mBackImage = encImage;
                backMark.setVisibility(View.VISIBLE);
            }
            if (SELECTED_IMAGE == 3) {
                mPortraitImage = encImage;
                portraitMark.setVisibility(View.VISIBLE);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    private void encodeImage(Bitmap bm)
    {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        if (SELECTED_IMAGE == 1) {
            mFrontImage = encImage;
            frontMark.setVisibility(View.VISIBLE);
        }
        if (SELECTED_IMAGE == 2) {
            mBackImage = encImage;
            backMark.setVisibility(View.VISIBLE);
        }
        if (SELECTED_IMAGE == 3) {
            mPortraitImage = encImage;
            portraitMark.setVisibility(View.VISIBLE);
        }
    }
    private void encodeImage(String path)
    {

        File imagefile = new File(path);
        FileInputStream fis = null;
        try{
            fis = new FileInputStream(imagefile);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        if (SELECTED_IMAGE == 1) {
            mFrontImage = encImage;
            frontMark.setVisibility(View.VISIBLE);
        }
        if (SELECTED_IMAGE == 2) {
            mBackImage = encImage;
            backMark.setVisibility(View.VISIBLE);
        }
        if (SELECTED_IMAGE == 3) {
            mPortraitImage = encImage;
            portraitMark.setVisibility(View.VISIBLE);
        }
    }

    private void BitmapToBase64(Bitmap bitmap) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//        byte[] byteArray = byteArrayOutputStream.toByteArray();
//        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
//        Log.e("modee", encoded);


        String encoded = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg==";
        if (SELECTED_IMAGE == 1) {
            mFrontImage = encoded;
            frontMark.setVisibility(View.VISIBLE);
        }
        if (SELECTED_IMAGE == 2) {
            mBackImage = encoded;
            backMark.setVisibility(View.VISIBLE);
        }
        if (SELECTED_IMAGE == 3) {
            mPortraitImage = encoded;
            portraitMark.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        progressDialog.dismiss();
        switch (responseForRequest) {
            case Constants
                    .CashPLusUpdateUserProfile:
                if (status) {
                    sessionManager.setCashplusVerified(true);
                    ActivityLauncher.launchHomeScreenPaymentTab(CashplusUpdateProfile.this);
                } else
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                break;
            case Constants
                    .IDTypes:
                if (status) {

                    GetIDCardTypes idCardTypes = (GetIDCardTypes) body;
                    if (idCardTypes.getErrorCode().equals("0")) {
                        cardList = idCardTypes.getIdCardTypeList();
                    } else
                        Toast.makeText(getApplicationContext(), idCardTypes.getErrorDescription(), Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                break;
            case Constants
                    .OccupationTypes:
                if (status) {

                    GetOccupationTypes occupationTypes = (GetOccupationTypes) body;
                    if (occupationTypes.getErrorCode().equals("0")) {
                        OccupationList = occupationTypes.getOccupationTypes();
                    } else
                        Toast.makeText(getApplicationContext(), occupationTypes.getErrorDescription(), Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                break;


        }
    }
}