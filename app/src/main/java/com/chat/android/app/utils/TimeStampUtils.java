package com.chat.android.app.utils;

import static com.chat.android.utils.AppConstants.KEY_USER_LANGUAGE;

import android.content.Context;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.chat.android.R;
import com.chat.android.core.SessionManager;
import com.chat.android.utils.StorageUtility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by CAS60 on 5/12/2017.
 */
public class TimeStampUtils {

    private static final String TAG = "TimeStampUtils";
    public static String get12HrTimeFormat(Context context, String ts) {
        try {
            long givenTS = Long.parseLong(ts);
            long serverDiff = SessionManager.getInstance(context).getServerTimeDifference();
            long deviceTS = givenTS + serverDiff;
            Date dateObj = new Date(deviceTS);
            String selectedLang = StorageUtility.getDataFromPreferences(context, KEY_USER_LANGUAGE, "en");
            Locale locale = Locale.ENGLISH;
            if (selectedLang.equals("ar")) {
                locale = new Locale("ar", "DZ");
            }
            String timeStamp = new SimpleDateFormat("h:mm a", locale).format(dateObj);
            return timeStamp;
        } catch (NumberFormatException e) {
            MyLog.e(TAG,"",e);
            return "";
        }
    }

    public static Date getMessageTStoDate(Context context, String ts) {
        try {
            long givenTS = Long.parseLong(ts);
            long serverDiff = SessionManager.getInstance(context).getServerTimeDifference();
            long deviceTS = givenTS + serverDiff;

            DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date dateTemp = new Date(deviceTS);
            return df.parse(df.format(dateTemp));
        } catch (Exception e) {
            MyLog.e(TAG,"",e);
            return null;
        }
    }

    public static Date getDateFormat(long ts) {
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date dateTemp = new Date(ts);
            return df.parse(df.format(dateTemp));
        } catch (Exception e) {
            MyLog.e(TAG,"",e);
            return null;
        }
    }

    public static String getDate(long ts) {
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date dateTemp = new Date(ts);
            return df.format(dateTemp);
        } catch (Exception e) {
            return "";
        }
    }
    public static String getTimeStamp() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ");
        String ts = timeStampFormat.format(new Date());
        return ts;
    }

    public static Date getYesterdayDate(Date today) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.add(Calendar.DATE, -1);
        Date yesterday = cal.getTime();
        return yesterday;
    }

    public static String getServerTimeStamp(Context context, long deviceTS) {
        long serverDiff = SessionManager.getInstance(context).getServerTimeDifference();
        long serverTS = deviceTS + serverDiff;
        return String.valueOf(serverTS);
    }

    public static Date addDay(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, count);
        return cal.getTime();
    }

    public static Date addHour(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, count);
        return cal.getTime();
    }

    public static Date addYear(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, count);
        return cal.getTime();
    }
    public static long getTimestampDifferenceMinutes(String from, String to) {

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ");
        String inputString1 = from;
        String inputString2 = to;

        try {
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            long diff = date2.getTime() - date1.getTime();
            return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
        } catch (
                ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
