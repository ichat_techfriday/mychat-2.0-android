package com.chat.android.app.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.adapter.GroupMessageInfoAdapter;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.GroupMessageInfoPojo;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.scimbohelperclass.ScimboImageUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CAS63 on 3/14/2017.
 */
public class GroupMessageInfoActivity extends CoreActivity {

    private RecyclerView rvMessageseen, rvDelivered;
    private GroupMessageInfoAdapter msgInfoAdapter;
    private GroupMessageInfoAdapter deliveryInfoAdapter;
    private RelativeLayout single, group;
    private ImageView backnavigator;
    public ImageView imgshow, play, vidshow, ivPlay;
    private View imgShowContainer;
    private String imagePath, message, myDocName, contact, DocPath, videoPath, Audiopath;
    private MessageItemChat msgItem;
    private List<GroupMessageInfoPojo> infoList;
    private List<GroupMessageInfoPojo> deliveryList;
    private Uri uri;
    private Getcontactname getcontactname;
    private RelativeLayout contact_layout, audio_layout, video_layout, document_layout, map_layout;
    private TextView txtMsg, contactName, docname, tvDeliverPending, duration, videocaption, imagecaption;
    private int readPendingCount = 0, deliverPendingCount = 0;
    private static final String TAG = GroupMessageInfoActivity.class.getSimpleName();
    private TextView tvReadByPending;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_info_screen);
        getcontactname = new Getcontactname(GroupMessageInfoActivity.this);

        tvDeliverPending = findViewById(R.id.tvDeliverPending);
        txtMsg = findViewById(R.id.txtMsg);
        backnavigator = findViewById(R.id.backnavigator);
        single = findViewById(R.id.single);
        group = findViewById(R.id.group);
        document_layout = findViewById(R.id.document_layout);
        map_layout = findViewById(R.id.map_layout);
        imgshow = findViewById(R.id.imgshow);
        imgShowContainer = findViewById(R.id.imgshowContainer);
        duration = findViewById(R.id.duration);
        contactName = findViewById(R.id.contactName);
        docname = findViewById(R.id.docname);
        play = findViewById(R.id.imageView26);
        ivPlay = findViewById(R.id.ivPlay);
        contact_layout = findViewById(R.id.contact_layout);
        videocaption = findViewById(R.id.videocaption);
        imagecaption = findViewById(R.id.imagecaption);
        audio_layout = findViewById(R.id.audio_layout);
        video_layout = findViewById(R.id.video_layout);
        vidshow = findViewById(R.id.vidshow);
        tvReadByPending = findViewById(R.id.tvReadPending);
        rvMessageseen = findViewById(R.id.rvMessageseen);
        rvDelivered = findViewById(R.id.rvDelivered);
        // deliveredlist = (RecyclerView) findViewById(R.id.deliveredlist);

        backnavigator = findViewById(R.id.backnavigator);
        backnavigator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        single.setVisibility(View.GONE);
        group.setVisibility(View.VISIBLE);
        initData();
    }

    private void initData() {
        Bundle bundle = getIntent().getBundleExtra("bundle");
        Bitmap bitmap = bundle.getParcelable("PARAM_BITMAP");
        msgItem = (MessageItemChat) getIntent().getSerializableExtra("selectedData");
        msgItem.setThumb_bitmap_image(bitmap);

        String msgId = msgItem.getMessageId();
        String[] splitIds = msgId.split("-");
        String docId = splitIds[0] + "-" + splitIds[1] + "-g";
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

        MessageDbController dbController = CoreController.getDBInstance(this);
        MessageItemChat dbItem = dbController.getParticularMessage(msgId);
        if (dbItem != null) {
            msgItem = dbItem;
        }
        String infoData = msgItem.getGroupMsgDeliverStatus();
        String currentUserId = SessionManager.getInstance(GroupMessageInfoActivity.this).getCurrentUserID();

        try {
            JSONObject object = new JSONObject(infoData);
            JSONArray arrMembers = object.getJSONArray("GroupMessageStatus");
            infoList = new ArrayList<>();
            deliveryList = new ArrayList<>();

            for (int i = 0; i < arrMembers.length(); i++) {
                JSONObject userObj = arrMembers.getJSONObject(i);
                String receiverId = userObj.getString("UserId");

                if (!receiverId.equals(currentUserId)) {
                    String deliverStatus = userObj.getString("DeliverStatus");
                    String deliverTime = userObj.getString("DeliverTime");
                    String readTime = userObj.getString("ReadTime");
                    if (!deliverStatus.equals(MessageFactory.DELIVERY_STATUS_DELIVERED) &&
                            !deliverStatus.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                        deliverPendingCount += 1;
                        readPendingCount += 1;

                    } else if (!deliverStatus.equals(MessageFactory.DELIVERY_STATUS_READ)) {
                        readPendingCount += 1;
                    }

                    // Setting name and mobile number of receivers
                    FriendModel contact = contactDB_sqlite.getFriendDetails(receiverId);
                    if (!deliverTime.equalsIgnoreCase("")) {
                        if (contact != null) {
                            GroupMessageInfoPojo msgInfoPojo = new GroupMessageInfoPojo();
                            msgInfoPojo.setReceiverId(receiverId);

                            String receiverName = null;
                            receiverName = getcontactname.getSendername(receiverId, contact.getMsisdn());

                            if (receiverName != null) {
                                msgInfoPojo.setReceiverMsisdn(receiverName);
                                msgInfoPojo.setReceiverName("");
                            } else {
                                msgInfoPojo.setReceiverMsisdn(contact.getMsisdn());
                                msgInfoPojo.setReceiverName(contact.getFirstName());
                            }

                            msgInfoPojo.setDeliverTS(deliverTime);
                            msgInfoPojo.setReadTS(readTime);
                            if (!readTime.equalsIgnoreCase(""))
                                infoList.add(msgInfoPojo);
                            else
                                deliveryList.add(msgInfoPojo);
                        }
                    }
                }
            }

            displayGroupMessageInfo();
        } catch (Exception e) {
            MyLog.e(TAG, "initData: ", e);
        }
    }

    private void displayGroupMessageInfo() {

        // myadapter = new GroupMessageInfoAdapter(this,selectedItem);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvMessageseen.setLayoutManager(mLayoutManager);
        rvDelivered.setLayoutManager(AppUtils.getDefaultLayoutManger(this));

        msgInfoAdapter = new GroupMessageInfoAdapter(GroupMessageInfoActivity.this, infoList, readPendingCount, false);
        deliveryInfoAdapter = new GroupMessageInfoAdapter(GroupMessageInfoActivity.this, deliveryList, readPendingCount, true);
        rvMessageseen.setAdapter(msgInfoAdapter);
        rvDelivered.setAdapter(deliveryInfoAdapter);
        if (readPendingCount > 0) {
            tvReadByPending.setText(readPendingCount + " "+getResources().getString(R.string.remaining));
        } else
            tvReadByPending.setVisibility(View.GONE);
        if (deliveryList.size() > 0 || deliverPendingCount > 0) {
            findViewById(R.id.delivery_info_container).setVisibility(View.VISIBLE);


            if (deliverPendingCount > 0) {
                String pendingText = deliverPendingCount + " "+getResources().getString(R.string.remaining);
                tvDeliverPending.setText(pendingText);
            }
            else {
                tvDeliverPending.setVisibility(View.GONE);
            }

        } else {
            findViewById(R.id.delivery_info_container).setVisibility(View.GONE);
        }

        Integer type = Integer.parseInt(msgItem.getMessageType());
        if (type.equals(MessageFactory.text)) {
            txtMsg.setVisibility(View.VISIBLE);
            message = msgItem.getTextMessage();
            txtMsg.setText(message);
        } else if (type.equals(MessageFactory.nudge)) {
            txtMsg.setVisibility(View.VISIBLE);
            message = getResources().getString(R.string.sent_a_nudge);
            txtMsg.setText(message);
        } else if (type.equals(MessageFactory.contact)) {
            contact_layout.setVisibility(View.VISIBLE);
            contact = msgItem.getContactName();
            contactName.setText(contact);
        } else if (type.equals(MessageFactory.document)) {
            document_layout.setVisibility(View.VISIBLE);

            DocPath = msgItem.getChatFileLocalPath();
            myDocName = msgItem.getTextMessage();
            docname.setText(myDocName);

            document_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String extension = MimeTypeMap.getFileExtensionFromUrl(DocPath);
                    String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                    try {
                        File file = new File(DocPath);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(file), mimeType);
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(GroupMessageInfoActivity.this, getResources().getString(R.string.no_app_installed_to_view_this_document), Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else if (type.equals(MessageFactory.audio)) {
            audio_layout.setVisibility(View.VISIBLE);
            Audiopath = msgItem.getChatFileLocalPath();
            if (msgItem.getDuration() != null) {
                duration.setText(msgItem.getDuration());
            }
            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playaudio(Audiopath);
                }
            });

        } else if (type.equals(MessageFactory.video)) {
            video_layout.setVisibility(View.VISIBLE);
            videoPath = msgItem.getChatFileLocalPath();
            if (msgItem.getTextMessage() != null) {
                if (!msgItem.getTextMessage().equalsIgnoreCase("")) {
                    videocaption.setVisibility(View.VISIBLE);
                    videocaption.setText(msgItem.getTextMessage());
                } else {
                    videocaption.setVisibility(View.GONE);
                }
            }
            if (!videoPath.equals("")) {
                uri = Uri.parse(videoPath);
            }
            try {
                vidshow.setImageBitmap(ThumbnailUtils.createVideoThumbnail(videoPath,
                        MediaStore.Images.Thumbnails.MINI_KIND));

            } catch (OutOfMemoryError e) {
                MyLog.e(TAG,"",e);
            }
            ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setDataAndType(uri, "video/*");
                    startActivity(intent);
                }
            });
        } else if (type.equals(MessageFactory.picture)) {
            imgShowContainer.setVisibility(View.VISIBLE);

            imagePath = msgItem.getChatFileLocalPath();
            imgshow.setImageBitmap(ScimboImageUtils.decodeBitmapFromFile(imagePath, 220, 150));


            if (msgItem.getTextMessage() != null) {
                if (!msgItem.getTextMessage().equalsIgnoreCase("")) {
                    imagecaption.setVisibility(View.VISIBLE);
                    imagecaption.setText(msgItem.getTextMessage());
                } else {
                    imagecaption.setVisibility(View.GONE);
                }
            }
            imgshow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GroupMessageInfoActivity.this, ImageZoom.class);
//                            intent.putExtra("IsSelf", isSelf);
                    intent.putExtra("image", imagePath);
                    startActivity(intent);
                }
            });
        }
    }

    public void playaudio(final String path) {

        try {
            Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW);
            File file = new File(path);
            String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
            String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            myIntent.setDataAndType(Uri.fromFile(file), mimetype);
            startActivity(myIntent);
        } catch (Exception e) {
            MyLog.e(TAG, "playaudio: ", e);
        }

    }


}
