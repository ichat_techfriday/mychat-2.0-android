package com.chat.android.app.model.meetup;

import com.google.gson.annotations.SerializedName;

public class MeetUpProfile {
    @SerializedName("bio")
    String bio;
    @SerializedName("dob")
    String dob;

    public MeetUpProfile() {
    }

    public MeetUpProfile(String bio, String dob) {
        this.bio = bio;
        this.dob = dob;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
