package com.chat.android.app.model;

public class StatusRefresh {
private String userId;
private String status;

    public StatusRefresh(String from, String status) {
        this.userId=from;
        this.status=status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
