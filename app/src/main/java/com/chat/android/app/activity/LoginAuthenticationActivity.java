package com.chat.android.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.utils.LoginAuthUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.SessionManager;
import com.chat.android.core.service.Constants;
import com.facebook.stetho.Stetho;
import com.multidots.fingerprintauth.FingerPrintAuthCallback;
import com.multidots.fingerprintauth.FingerPrintAuthHelper;


public class LoginAuthenticationActivity extends Activity implements FingerPrintAuthCallback {
    private FingerPrintAuthHelper fingerPrintAuthHelper;
    private  int INTENT_AUTHENTICATE = 10;
    private boolean isDeviceHaveAuth=false;

    private Button btnPinUnlock;
    private View tvOr;

    private ImageView ivFingerPrint;
    private TextView tvFingerPrintDesc;
    private static final String TAG = "LoginAuthenticationActi";
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.d(TAG, "onCreate: ");
        Stetho.initializeWithDefaults(this);

        Constants.IS_FROM_PASSWORD_PAGE=true;
        SessionManager.getInstance(this).setIsDeviceLocked(false);
        setContentView(R.layout.actiivity_login_auth);
        btnPinUnlock=findViewById(R.id.btn_unlock_with_pin) ;
        tvOr=findViewById(R.id.tv_or);
        ivFingerPrint=findViewById(R.id.iv_finger_print);
        tvFingerPrintDesc=findViewById(R.id.tv_finger_print_unlock);
        if(SessionManager.getInstance(this).isDeviceLockEnabled()) {
            init();
        }
        else{
            startNextPage();
        }

    }

    private void hideFingerPrintViews(){
        ivFingerPrint.setVisibility(View.GONE);
        tvFingerPrintDesc.setVisibility(View.GONE);
    }




    private void init() {
        fingerPrintAuthHelper = FingerPrintAuthHelper.getHelper(this, this);


        if(LoginAuthUtils.isDeviceHasLock(this)){
            if(!isDeviceHaveAuth){
                isDeviceHaveAuth=true;
            }
            else{
                tvOr.setVisibility(View.VISIBLE);
            }
            btnPinUnlock.setVisibility(View.VISIBLE);

            btnPinUnlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoginAuthUtils.startDefaultPasswordPage(LoginAuthenticationActivity.this, INTENT_AUTHENTICATE);
                }
            });
        }
        if(!isDeviceHaveAuth){
            startNextPage();
        }
        else {
            if (!SessionManager.getInstance(this).getlogin()) {
                startNextPage();
            }
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (fingerPrintAuthHelper != null)
            fingerPrintAuthHelper.stopAuth();
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onResume() {


        if (fingerPrintAuthHelper != null) {
            fingerPrintAuthHelper.startAuth();
        }

        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == INTENT_AUTHENTICATE) {
            if (resultCode == RESULT_OK) {
                //do something you want when pass the security
                startNextPage();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onNoFingerPrintHardwareFound() {
        hideFingerPrintViews();
    }

    @Override
    public void onNoFingerPrintRegistered() {
        hideFingerPrintViews();
    }

    @Override
    public void onBelowMarshmallow() {
        hideFingerPrintViews();
    }

    @Override
    public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
        Toast.makeText(this,getResources().getString(R.string.Verification_Successful),Toast.LENGTH_SHORT).show();
        startNextPage();
    }

    @Override
    public void onAuthFailed(int errorCode, String errorMessage) {
        //  Toast.makeText(this, "" + errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void startNextPage() {
        MyLog.d(TAG, "performance startNextPage: ");
        Intent intent = new Intent(this, IntialLoaderActivityScimbo.class);
        startActivity(intent);
        finish();

    }
}
