package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chat.android.R;


/**
 */
public class VHservermessage extends RecyclerView.ViewHolder {

    public TextView tvServerMsgLbl, tvSecretLbl, tvDateLbl;


    public VHservermessage(View view) {
        super(view);

        tvServerMsgLbl = view.findViewById(R.id.tvServerMsgLbl);
        tvSecretLbl = view.findViewById(R.id.tvSecretLbl);
        tvDateLbl = view.findViewById(R.id.tvDateLbl);

    }
}
