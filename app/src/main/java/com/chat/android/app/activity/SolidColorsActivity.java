package com.chat.android.app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.adapter.SolidColorsAdapter;
import com.chat.android.core.Session;
import com.chat.android.utils.StorageUtility;
import com.google.android.material.snackbar.Snackbar;

public class SolidColorsActivity extends AppCompatActivity {
    Session session;
    ImageView iv_back;
    GridView solid_colors_grid;

    String colors[] = {"#000000", "#FFFFFF", "#4F607A", "#C4C4C6", "#FF8C00",
            "#EEFFAD35", "#A5D6A7", "#64B5F6", "#7E57C2", "#FFCDD2",
    "#EF9A9A", "#00695C"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solid_colors);

        session = new Session(SolidColorsActivity.this);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        solid_colors_grid = (GridView)findViewById(R.id.solid_colors_grid);
        SolidColorsAdapter solidColorsAdapter = new SolidColorsAdapter(SolidColorsActivity.this, colors);
        solid_colors_grid.setAdapter(solidColorsAdapter);

        solid_colors_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String itemValue = colors[i];
                session.putgalleryPrefs("");
                session.putColor(itemValue);
                finish();
            }
        });

    }
}