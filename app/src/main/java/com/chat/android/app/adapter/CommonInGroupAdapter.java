package com.chat.android.app.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.app.widget.CircleTransform;
import com.chat.android.core.model.CommonInGroupPojo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by CAS60 on 12/27/2016.
 */
public class CommonInGroupAdapter extends RecyclerView.Adapter<CommonInGroupAdapter.CommonInGroupHolder> {

    private Context mContext;
    private ArrayList<CommonInGroupPojo> dataList;
    public CommonInGroupAdapter(Context mContext, ArrayList<CommonInGroupPojo> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }

    public class CommonInGroupHolder extends RecyclerView.ViewHolder {

        public CircleImageView ivGroupDp;
        public AvnNextLTProRegTextView tvGroupName, tvGroupContacts;

        public CommonInGroupHolder(View itemView) {
            super(itemView);

            ivGroupDp = itemView.findViewById(R.id.ivGroupDp);
            tvGroupName = itemView.findViewById(R.id.tvGroupName);
            tvGroupContacts = itemView.findViewById(R.id.tvGroupContacts);
        }
    }

    @Override
    public CommonInGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_common_in_group,
                            parent, false);
        CommonInGroupHolder holder = new CommonInGroupHolder(view);
        return holder;
    }

    public CommonInGroupPojo getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public void onBindViewHolder(CommonInGroupHolder holder, int position) {

        String avatarPath = dataList.get(position).getAvatarPath();
        if (avatarPath != null && !avatarPath.equals("")) {
        /*    Picasso.with(mContext).load(AppUtils.getValidProfilePath(avatarPath)).error(
                    R.drawable.ic_placeholder_black).transform(new CircleTransform()).into(holder.ivGroupDp);
        */
            AppUtils.loadImage(mContext,AppUtils.getValidGroupPath(avatarPath),holder.ivGroupDp,100,R.mipmap.group_chat_attachment_profile_icon);
        } else {
            holder.ivGroupDp.setImageResource(R.drawable.ic_placeholder_black);
        }
        holder.tvGroupName.setText(dataList.get(position).getGroupName());
        if(dataList.get(position).getGroupContactNames() != null) {
            holder.tvGroupContacts.setText(dataList.get(position).getGroupContactNames());
        } else {
            holder.tvGroupContacts.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

}
