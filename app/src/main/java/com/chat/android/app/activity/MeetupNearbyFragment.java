package com.chat.android.app.activity;

import static android.content.Context.SENSOR_SERVICE;

import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.model.meetup.GetNearByMeetupsResponse;
import com.chat.android.app.model.meetup.LocationNearByMeetups;
import com.chat.android.app.model.meetup.NearbyUserModel;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.backend.ApiCalls;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeetupNearbyFragment extends MeetupBaseFragment implements SensorEventListener {
    // device sensor manager
    private SensorManager SensorManage;
    // define the compass picture that will be use
    private ImageView compassImage;
    private View compassView;
    String profileImage;
    ImageView ivUserProfile;
    private Context mContext;
    private List<NearbyUserModel> nearbyUserModelList;
    private RelativeLayout usersLayout;
    private Handler handler = new Handler();
    private Runnable runnable;
    private final int delay = 10 * 1000;
    private float currentDegree = 0f;
    private final String TAG = "MeetupNearby";
    private BottomSheetDialog bottomSheetDialog;
    private boolean isActive = true;
    private boolean isNearByTimerWorking = true;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MyLog.d("onAttach: ");
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meetup_nearby, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SessionManager sessionManager = SessionManager.getInstance(getContext());
        compassImage = (ImageView) view.findViewById(R.id.iv_search);
        compassView = view.findViewById(R.id.compass_layout);
        SensorManage = (SensorManager) mContext.getSystemService(SENSOR_SERVICE);
        ivUserProfile = (ImageView) view.findViewById(R.id.iv_main_profile_pic);
        usersLayout = view.findViewById(R.id.users_layout);
        profileImage = sessionManager.getUserProfilePic();
        AppUtils.loadImage(getContext(), AppUtils.getValidProfilePath(profileImage), ivUserProfile, 150, R.drawable.ic_placeholder_black);
        view.findViewById(R.id.back_btn).setOnClickListener(v -> {
            deactivateMeetup();
            Intent intent = new Intent(
                    mActivity,
                    NewHomeScreenActivty.class
            );
            intent.putExtra(NewHomeScreenActivty.OPEN_TAB_INDEX, 1);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            mActivity.finish();
        });
        updateMeetup();
        fetchLocation();
        handler.postDelayed( runnable = () -> {
            MyLog.d(TAG, "Meetup handler isActive " + isActive);
            if (!isActive)
                return;
            MyLog.d(TAG, "Meetup handler fetching users");
            fetchNearByUsersList();
            handler.postDelayed(runnable, delay);
        }, 500);
    }

    private void fetchNearByUsersList() {
        if (lat == 0.0 && lng == 0.0)
            return;
        MyLog.d(TAG, "fetchNearByUsersList lat lng : " + lat + " " + lng);
        ApiCalls.getInstance(mContext).getNearByMeetups(mContext, mLatitude, mLongitude, maxDistance, limit, SessionManager.getInstance(getContext()).getCurrentUserID(), new Callback<GetNearByMeetupsResponse>() {
            @Override
            public void onResponse(Call<GetNearByMeetupsResponse> call, Response<GetNearByMeetupsResponse> response) {
                MyLog.d(TAG, "fetchNearByUsersList in onResponse ");
                if (response.isSuccessful()) {
                    GetNearByMeetupsResponse getNearByMeetup = response.body();
                    if (getNearByMeetup != null) {
                        nearbyUserModelList = getNearByMeetup.getUsers();
                        MyLog.d(TAG, "fetchNearByUsersList list size : " + nearbyUserModelList.size());
                        addUsersOnMap();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetNearByMeetupsResponse> call, Throwable t) {
                MyLog.d(TAG, "fetchNearByUsersList in onFailure ");
            }
        });
    }

    private void addUsersOnMap() {
        if (nearbyUserModelList == null)
            return;
        if (nearbyUserModelList.size() == 0) {
            usersLayout.removeAllViews();
            return;
        }
        int i = 0;
        usersLayout.removeAllViews();
        PointF origin = new PointF(compassImage.getPivotX(), compassImage.getPivotY());

        for (NearbyUserModel userModel: nearbyUserModelList) {
            LocationNearByMeetups locationNearByMeetups = userModel.getLocationNearByMeetups();
            ArrayList<Double> coordinates = locationNearByMeetups.getCoordinates();
            double heading = heading(coordinates);
            double radius =  (double) compassView.getWidth() / 2;
            // Cover distance inside the allowed radius
            double totalCompassSpan = this.maxDistance;
            double coverDistanceInsideRadius = ((userModel.getDistance()) / (totalCompassSpan / radius));
            // Mark point in Compass
            PointF points = markPoint(origin, coverDistanceInsideRadius, heading);

            RelativeLayout inflatedView = (RelativeLayout) View.inflate(mContext, R.layout.meetup_profile_image, null);
            CircleImageView imgView = inflatedView.findViewById(R.id.ic_user_profile);
            int color = userModel.getFriend() ? mContext.getResources().getColor(R.color.green_500) : mContext.getResources().getColor(R.color.material_blue);
            imgView.setBorderColor(color);

            imgView.setBorderWidth(5);
            imgView.setId(i+1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (mContext.getResources().getDimension(R.dimen._30sdp)), (int) (mContext.getResources().getDimension(R.dimen._48sdp)));
            try {
                BigDecimal bigDecimal1 = BigDecimal.valueOf(points.x);
                BigDecimal bigDecimal2 = BigDecimal.valueOf(points.y);
                layoutParams.leftMargin = bigDecimal1.intValue() - layoutParams.width / 2;
                layoutParams.topMargin = bigDecimal2.intValue() - layoutParams.height / 2;
            } catch (Exception e) {
                e.printStackTrace();
            }
            String path =  AppUtils.getValidProfilePath(userModel.getProfilePic());
            AppUtils.loadImage(mContext,path, imgView, 150, R.drawable.ic_placeholder_black);
            usersLayout.addView(inflatedView, layoutParams);

            inflatedView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showBottomSheetDialog(userModel, path);
                }
            });
            i++;
        }
    }

    private void showBottomSheetDialog(NearbyUserModel user, String path) {
        if (bottomSheetDialog != null && bottomSheetDialog.isShowing())
            return;
        if (bottomSheetDialog == null)
            bottomSheetDialog = new BottomSheetDialog(mContext);
        bottomSheetDialog.setContentView(R.layout.bottomsheet_nearby_user);

        //init
        TextView tvUserName = bottomSheetDialog.findViewById(R.id.tv_user_name);
        CircleImageView profileImg = bottomSheetDialog.findViewById(R.id.iv_main_profile_pic);
        TextView tvUserBio = bottomSheetDialog.findViewById(R.id.tv_user_bio);
        TextView dob = bottomSheetDialog.findViewById(R.id.tv_dob);
        Button btn = bottomSheetDialog.findViewById(R.id.btn_chat_now);

        String buttonText = "";
        int color;
        if (user.getFriend()){
            buttonText = mContext.getString(R.string.chat_now);
            color = mContext.getResources().getColor(R.color.green_500);
        } else {
            buttonText = mContext.getString(R.string.poke_now);
            color = mContext.getResources().getColor(R.color.material_blue);
        }

        if (profileImg != null) {
            profileImg.setBorderColor(color);
            AppUtils.loadImage(mContext,path, profileImg, 150, R.drawable.ic_placeholder_black);
        }
        if (tvUserBio != null) {
            tvUserBio.setText(user.getMeetUpProfile().getBio());
        }
        if (tvUserName != null) {
            tvUserName.setText(user.getMeetup_name());
        }
        if (dob != null) {
            dob.setText(user.getMeetUpProfile().getDob());
        }
        if (btn != null) {
            btn.setText(buttonText);
            btn.setOnClickListener(v -> {
                if (user.getFriend()) {
                    navigateToChat(user);
                } else {
                    addFriend(user.getPinCode());
                }
            });
        }
        bottomSheetDialog.show();
    }

    private void navigateToChat(NearbyUserModel user) {
        Intent intent = new Intent(getActivity(), ChatPageActivity.class);
        intent.putExtra("receiverUid", user.getId());
        intent.putExtra("receiverName", user.getMeetup_name());
        intent.putExtra("documentId", user.getId());
        intent.putExtra("Image", user.getProfilePic());
        intent.putExtra("msisdn", user.getMsisdn());
        intent.putExtra("isFrom", "meetup");
        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        mActivity.finish();
    }

    private PointF markPoint(PointF origin, double distance, double degrees) {
        PointF endPoint = new PointF();
        endPoint.x = (float) (distance * Math.cos(degrees) + origin.x);
        endPoint.y = (float) (distance * Math.sin(degrees) + origin.y);
        return endPoint;
    }

    private double heading(ArrayList<Double> coordinates) {
        double lat1 = Math.toRadians(lat);
        double lon1 = Math.toRadians(lng);

        double lat2 = Math.toRadians(coordinates.get(1));
        double lon2 = Math.toRadians(coordinates.get(0));

        double dLon = lon2 - lon1;
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

        double headingDegrees = Math.toDegrees(Math.atan2(y, x));
        if (headingDegrees >= 0) {
            return headingDegrees;
        } else {
            return headingDegrees + 360;
        }
    }

    private void addFriend(String pinCode) {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_SEND_FRIEND_REQUEST);
        JSONObject obj = new JSONObject();
        try {
            obj.put("userId", SessionManager.getInstance(getContext()).getCurrentUserID());
            obj.put("pinCode", pinCode);
            obj.put("companyId", "");
        } catch (JSONException e) {
            MyLog.e("AddFriend","",e);
        }
        MyLog.e("AddFriend","AddFriend " + obj);
        messageEvent.setMessageObject(obj);
        EventBus.getDefault().post(messageEvent);
    }
    
    @Override
    public void onSensorChanged(SensorEvent event) {
        float degree = Math.round(event.values[0]);
        // create a rotation animation (reverse turn degree degrees)
        RotateAnimation ra = new RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        // how long the animation will take place
        ra.setDuration(210);

        // set the animation after the end of the reservation status
        ra.setFillAfter(true);

        // Start the animation
        compassView.startAnimation(ra);
        currentDegree = -degree;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onPause() {
        super.onPause();
        MyLog.d(TAG, "Meetup onPause");
        // to stop the listener and save battery
        SensorManage.unregisterListener(this);
        handler.removeCallbacks(runnable);
        isActive = false;
        isNearByTimerWorking = false;
    }
    @Override
    public void onResume() {
        super.onResume();
        MyLog.d(TAG, "Meetup onResume");
        // code for system's orientation sensor registered listeners
        SensorManage.registerListener(this, SensorManage.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
        if (!isNearByTimerWorking)
            handler.postDelayed(runnable, delay);
        isNearByTimerWorking = true;
        isActive = true;
    }

    @Override
    public void onStart() {
        super.onStart();
        MyLog.d(TAG, "Meetup onStart");
        EventBus.getDefault().register(this);
        userActivationStatus = "activate";
        isActive = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        MyLog.d(TAG, "Meetup onStop");
        EventBus.getDefault().unregister(this);
        deactivateMeetup();
        isActive = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyLog.d(TAG, "Meetup onDestroy");
        if (runnable != null)
            handler.removeCallbacks(runnable);
        handler = null;
        isActive = false;
        isNearByTimerWorking = false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        MyLog.d(TAG, "onMessageEvent response " + event.getEventName());

        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_SEND_FRIEND_REQUEST)) {
            try {
                Object[] response = event.getObjectsArray();
                JSONObject jsonObject = (JSONObject) response[0];
                MyLog.d("AddFriend", "friendRequest response " + jsonObject.toString());
                int error = jsonObject.getInt("err");
                String message = jsonObject.getString("message");
                MyLog.d("Response", "Success Response");

                if (error == 1) {
                    MyLog.d("Response", "Success Response");
                    CoreActivity.showToast(mContext, message);
                } else {
                    CoreActivity.showToast(mContext, message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}