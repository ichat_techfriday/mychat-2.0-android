package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetIDCardTypes implements Serializable {
    @SerializedName("IDCardTypes")
    public List<IDCardType> idCardTypeList;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public List<IDCardType> getIdCardTypeList() {
        return idCardTypeList;
    }

    public void setIdCardTypeList(List<IDCardType> idCardTypeList) {
        this.idCardTypeList = idCardTypeList;
    }

    public class IDCardType implements Serializable{
        @SerializedName("ID")
        public int iD;
        @SerializedName("Name")
        public String name;

        @Override
        public String toString() {
            return name;
        }
    }



}
