package com.chat.android.app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.chat.android.MyFirebaseInstanceIDService;
import com.chat.android.R;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.scimbohelperclass.ScimboDialogUtils;
import com.chat.android.core.service.Constants;
import com.chat.android.core.service.ServiceRequest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class RenewSubscriptionActivity extends OnBoardingHomeParentActivity {

    Button btnRenewSubscription;
    private String GCM_Id = "";
    private GPSTracker gpsTracker;
    private final String TAG = "RenewSubscriptionActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renew_subscription);

        initViews();
    }

    private void initViews() {
        btnRenewSubscription = findViewById(R.id.btn_renew_sub);
        sessionManager = SessionManager.getInstance(RenewSubscriptionActivity.this);
        btnRenewSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeVerificationRequest();
            }
        });
    }

    public double getLatitude() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(this);
        if (gpsTracker.getLocation() != null && gpsTracker.getLatitude() > 0) {
            return gpsTracker.getLatitude();
        }
        return 0;
    }

    public double getLongitude() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(this);
        if (gpsTracker.getLocation() != null && gpsTracker.getLongitude() > 0) {
            return gpsTracker.getLongitude();
        }
        return 0;
    }

    private void makeVerificationRequest() {
        String uPhone = SessionManager.getInstance(RenewSubscriptionActivity.this).getUserMobileNoWithoutCountryCode();
        String cCode = SessionManager.getInstance(RenewSubscriptionActivity.this).getCountryCodeOfCurrentUser();
        code = cCode.replace("+", "");
        phoneNumberTxt = uPhone;
        String number = "+" + code + uPhone;
        number = number.replace(" ", "");

        if (MyFirebaseInstanceIDService.token != null && MyFirebaseInstanceIDService.token.length() > 0)
            GCM_Id = MyFirebaseInstanceIDService.token;
        else
            GCM_Id = MyFirebaseInstanceIDService.getFCMToken(this);

        if (GCM_Id == null || GCM_Id.isEmpty()) {
            GCM_Id = "121";
        }

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("msisdn", number);
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_Id);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("PhNumber", uPhone);
        params.put("CountryCode", "+" + code);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        params.put("callToken", "Android");

        MyLog.d(TAG, "params: " + params);
        ServiceRequest request = new ServiceRequest(this);
        request.makeServiceRequest(Constants.VERIFY_NUMBER_REQUEST, Request.Method.POST, params, verifcationListener);
        showProgres();
    }

    private void skipOTP(final String OTP) {

        if (MyFirebaseInstanceIDService.token != null && MyFirebaseInstanceIDService.token.length() > 0)
            GCM_Id = MyFirebaseInstanceIDService.token;
        else
            GCM_Id = MyFirebaseInstanceIDService.getFCMToken(this);

        if (GCM_Id == null || GCM_Id.isEmpty()) {

            GCM_Id = "121";
        }

        String msisdn = SessionManager.getInstance(this).getPhoneNumberOfCurrentUser();
        String mobileNo = SessionManager.getInstance(this).getUserMobileNoWithoutCountryCode();
        String countryCode = SessionManager.getInstance(this).getUserCountryCode();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("msisdn", msisdn);
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_Id);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("PhNumber", mobileNo);
        params.put("CountryCode", countryCode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        params.put("code", OTP);
        params.put("callToken", "Android");
        params.put("isZainUser", "false");

        params.put("pushToken", SessionManager.getInstance(this).getCurrentUserID());
        ServiceRequest request = new ServiceRequest(this);

        MyLog.d(TAG, "params: " + params);

        request.makeServiceRequest(Constants.VERIFY_SMS_CODE, Request.Method.POST, params, verifyCodeListener);
        showProgres();

    }

    @Override
    protected void verifyPhoneCompleted(String message) {
        hidepDialog();
        if (SKIP_OTP_VERIFICATION) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    skipOTP(otp);
                }
            }, 500);
        } else {
            hideProgressDialog();
            ActivityLauncher.launchSMSVerificationScreen(RenewSubscriptionActivity.this, message, "" + code, phoneNumberTxt, otp, GCM_Id,false);
        }
    }

    @Override
    protected void verifyCodeListenerCompleted() {
        hidepDialog();
        String msisdn = SessionManager.getInstance(this).getPhoneNumberOfCurrentUser();
        String mobileNo = SessionManager.getInstance(this).getUserMobileNoWithoutCountryCode();
        ActivityLauncher.launchSMSVerificationScreen(this, msisdn, "" + code,mobileNo, otp, GCM_Id,false);
    }

    @Override
    protected void verifyPhoneError(String message) {
        hideProgressDialog();
        ScimboDialogUtils.showCheckInternetDialog(RenewSubscriptionActivity.this);
        hidepDialog();
    }
}