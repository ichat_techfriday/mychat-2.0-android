package com.chat.android.app.activity

import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import com.android.billingclient.api.*
import com.chat.android.app.web.IChatWithoutHeaderAPIController
import com.chat.android.core.ActivityLauncher
import com.chat.android.core.CoreActivity
import com.chat.android.core.SessionManager
import com.chat.android.core.model.SaveSubDataResponse
import com.chat.android.databinding.ActivityBuySubscriptionBinding
import com.chat.android.utils.AppConstants
import com.google.common.collect.ImmutableList
import com.google.firebase.crashlytics.FirebaseCrashlytics
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BuySubscription : OnBoardingHomeParentActivity() {
    lateinit var  mBinding : ActivityBuySubscriptionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityBuySubscriptionBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        val renewSub = intent.getBooleanExtra("renewSub", false)
        if (renewSub)
            mBinding.btnBuySub.text = "ReSubscribe (\$5 USD)"

        mBinding.btnBuySub.setOnClickListener{initBillingClient()}
//        initBillingClient()
    }

    private fun saveSubPurchaseData(purchaseData : Purchase?) {
        val userID = SessionManager.getInstance(this@BuySubscription).currentUserID
        val deviceID = Settings.Secure.getString(
            contentResolver,
            Settings.Secure.ANDROID_ID
        )
        Log.d("Taha", "saveSubPurchaseData: purchaseToken ${purchaseData?.purchaseToken}")
        IChatWithoutHeaderAPIController.saveSubPurchaseData(this@BuySubscription,
            userID, AppConstants.ONE_MONTH_SUB_ID, purchaseData?.purchaseToken, deviceID,
            purchaseData?.purchaseTime ?: 0L
            , object : Callback<SaveSubDataResponse> {

                override fun onResponse(call: Call<SaveSubDataResponse>, response: Response<SaveSubDataResponse>) {
                    if (!response.isSuccessful)
                        return
                    val saveSubDataResponse :SaveSubDataResponse? = response.body()
                    saveSubDataResponse?.let {
                        if (it.status) {
                            val sessionManager = SessionManager.getInstance(this@BuySubscription)
                            sessionManager.IsBackupRestored(true)
                            sessionManager.IsprofileUpdate(true)
                            sessionManager.Islogedin(true)
                            ActivityLauncher.launchHomeScreen(this@BuySubscription)
                            finishAffinity()
                        }
                    }
                }
                override fun onFailure(call: Call<SaveSubDataResponse>, t: Throwable) {
                    FirebaseCrashlytics.getInstance().log("SaveSubPurchaseData onFailure ${t.message} ")
                    CoreActivity.showToast(this@BuySubscription, t.message)
                }
            })
    }

    private var mBillingClient: BillingClient? = null
    private fun initBillingClient() {
        val purchasesUpdatedListener =
            PurchasesUpdatedListener { billingResult: BillingResult?, purchases: List<Purchase?>? ->

                if (billingResult?.responseCode == BillingClient.BillingResponseCode.OK) {
                    val size = purchases?.size
                    size?.let{
                        var purchaseData : Purchase? = purchases[0]
                        Toast.makeText(this, "Subscription Successful", Toast.LENGTH_LONG).show()
                        Log.d("Taha", "initBillingClient: purchaseToken ${purchaseData?.purchaseToken}")
                        saveSubPurchaseData(purchaseData)
                    }
                } else {
                    Toast.makeText(this, "Error occurred while proceeding with the transaction", Toast.LENGTH_LONG).show()
                }
            }

        mBillingClient = BillingClient.newBuilder(this)
            .setListener(purchasesUpdatedListener)
            .enablePendingPurchases()
            .build()

        mBillingClient!!.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    queryProductDetails()
                }
            }

            override fun onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                mBillingClient?.startConnection(this)
            }
        })
    }

    private fun runPurchaseFlow(productDetails: ProductDetails) {
        val billingFlowParams = BillingFlowParams.newBuilder()
            .setProductDetailsParamsList(
                ImmutableList.of(
                    BillingFlowParams.ProductDetailsParams.newBuilder()
                        .setProductDetails(productDetails)
                        .setOfferToken(productDetails.subscriptionOfferDetails?.get(0)?.offerToken ?: "")
                        .build()
        )).build()

        // Launch the billing flow
        mBillingClient?.launchBillingFlow(this@BuySubscription, billingFlowParams)

    }

    fun queryProductDetails() {
        val queryProductDetailsParams = QueryProductDetailsParams.newBuilder()
            .setProductList(
                ImmutableList.of(
                    QueryProductDetailsParams.Product.newBuilder()
                        .setProductId(AppConstants.ONE_MONTH_SUB_ID)
                        .setProductType(BillingClient.ProductType.SUBS)
                        .build()
                )
            )
            .build()

        mBillingClient!!.queryProductDetailsAsync(
            queryProductDetailsParams
        ) { billingResult: BillingResult, productDetailsList: List<ProductDetails?>? ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                productDetailsList?.let {
                    productDetailsList[0]?.let {
                        runPurchaseFlow(it)
                    }
                }
            }}
    }

    fun queryPurchaseHistory() {
        mBillingClient!!.queryPurchaseHistoryAsync(QueryPurchaseHistoryParams.newBuilder()
            .setProductType(BillingClient.ProductType.SUBS)
            .build()
        ) { billingResult, purchaseHistoryList ->

            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                var size = purchaseHistoryList?.size
                size?.let {
                    if (it > 0) {
                        //TODO check subscription status
                    } else {
                        //TODO redirect user to buy subscription
                    }
                }
            }
        }
    }
}