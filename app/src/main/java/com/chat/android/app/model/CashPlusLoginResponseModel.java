package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class CashPlusLoginResponseModel {
    @SerializedName("UserName")
    public String userName;
    @SerializedName("Company")
    public String company;
    @SerializedName("Token")
    public String token;
    @SerializedName("LoggedIP")
    public String loggedIP;
    @SerializedName("LastLogin")
    public String lastLogin;
    @SerializedName("TokenExpiry")
    public String tokenExpiry;
    @SerializedName("CreditLimit_LBP")
    public int creditLimit_LBP;
    @SerializedName("CreditLimit_USD")
    public int creditLimit_USD;
    @SerializedName("QrCodeData")
    public String qrCodeData;
    @SerializedName("IsVerified")
    public boolean isVerified;
    @SerializedName("API_VERSION")
    public String aPI_VERSION;
    @SerializedName("APP_VERSION")
    public String aPP_VERSION;
    @SerializedName("APP_FORCED_UPDATE")
    public boolean aPP_FORCED_UPDATE;
    @SerializedName("App_Update_Link_PlayStore")
    public Object app_Update_Link_PlayStore;
    @SerializedName("App_Update_Link_AppleStore")
    public Object app_Update_Link_AppleStore;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getToken() {
        return token;
    }


    public void setToken(String token) {
        this.token = token;
    }

    public String getLoggedIP() {
        return loggedIP;
    }

    public void setLoggedIP(String loggedIP) {
        this.loggedIP = loggedIP;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getTokenExpiry() {
        return tokenExpiry;
    }

    public void setTokenExpiry(String tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }

    public int getCreditLimit_LBP() {
        return creditLimit_LBP;
    }

    public void setCreditLimit_LBP(int creditLimit_LBP) {
        this.creditLimit_LBP = creditLimit_LBP;
    }

    public int getCreditLimit_USD() {
        return creditLimit_USD;
    }

    public void setCreditLimit_USD(int creditLimit_USD) {
        this.creditLimit_USD = creditLimit_USD;
    }

    public String getQrCodeData() {
        return qrCodeData;
    }

    public void setQrCodeData(String qrCodeData) {
        this.qrCodeData = qrCodeData;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public String getaPI_VERSION() {
        return aPI_VERSION;
    }

    public void setaPI_VERSION(String aPI_VERSION) {
        this.aPI_VERSION = aPI_VERSION;
    }

    public String getaPP_VERSION() {
        return aPP_VERSION;
    }

    public void setaPP_VERSION(String aPP_VERSION) {
        this.aPP_VERSION = aPP_VERSION;
    }

    public boolean isaPP_FORCED_UPDATE() {
        return aPP_FORCED_UPDATE;
    }

    public void setaPP_FORCED_UPDATE(boolean aPP_FORCED_UPDATE) {
        this.aPP_FORCED_UPDATE = aPP_FORCED_UPDATE;
    }

    public Object getApp_Update_Link_PlayStore() {
        return app_Update_Link_PlayStore;
    }

    public void setApp_Update_Link_PlayStore(Object app_Update_Link_PlayStore) {
        this.app_Update_Link_PlayStore = app_Update_Link_PlayStore;
    }

    public Object getApp_Update_Link_AppleStore() {
        return app_Update_Link_AppleStore;
    }

    public void setApp_Update_Link_AppleStore(Object app_Update_Link_AppleStore) {
        this.app_Update_Link_AppleStore = app_Update_Link_AppleStore;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }


}
