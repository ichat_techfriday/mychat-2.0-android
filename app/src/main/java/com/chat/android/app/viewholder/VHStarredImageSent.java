package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.widget.AvnNextLTProRegTextView;


/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredImageSent extends RecyclerView.ViewHolder {

    public AvnNextLTProRegTextView captiontext;
    public TextView senderName, time, fromname, toname, datelbl,ts_abovecaption;
    public RelativeLayout caption;
    public ImageView singleTick, doubleTickGreen, doubleTickBlue, clock, imageView, starredindicator_below, userprofile;
    public RelativeLayout ts_abovecaption_layout,rlMsgStatus_above,time_layout,rlMsgStatus;
    public ImageView  starredindicator_above,single_tick_green_above,double_tick_green_above,double_tick_blue_above,clock_above;

    public VHStarredImageSent(View view) {
        super(view);
        senderName = view.findViewById(R.id.lblMsgFrom);

        imageView = view.findViewById(R.id.imgshow);
        starredindicator_below = view.findViewById(R.id.starredindicator_below);
        userprofile = view.findViewById(R.id.userprofile);
        time = view.findViewById(R.id.ts);

        singleTick = view.findViewById(R.id.single_tick_green);

        doubleTickGreen = view.findViewById(R.id.double_tick_green);

        doubleTickBlue = view.findViewById(R.id.double_tick_blue);
        caption= view.findViewById(R.id.caption);
        clock = view.findViewById(R.id.clock);
        toname = view.findViewById(R.id.toname);
        fromname = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        captiontext = view.findViewById(R.id.captiontext);


        single_tick_green_above = view.findViewById(R.id.single_tick_green_above);
        double_tick_green_above = view.findViewById(R.id.double_tick_green_above);
        double_tick_blue_above = view.findViewById(R.id.double_tick_blue_above);
        clock_above = view.findViewById(R.id.clock_above);
        starredindicator_above = view.findViewById(R.id.starredindicator_above);
        ts_abovecaption = view.findViewById(R.id.ts_abovecaption);
        ts_abovecaption_layout= view.findViewById(R.id.ts_abovecaption_layout);
        rlMsgStatus_above= view.findViewById(R.id.rlMsgStatus_above);
        time_layout= view.findViewById(R.id.time);
        rlMsgStatus= view.findViewById(R.id.rlMsgStatus);

    }
}
