package com.chat.android.app.activity;



import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.model.CashPlusLoginResponseModel;
import com.chat.android.app.model.CashplusLoginModel;
import com.chat.android.app.model.SetUserPasswordModel;
import com.chat.android.app.model.PasswordResetModel;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.interfaces.INetworkResponseListener;

public class CashpluPasswordActivity extends AppCompatActivity implements INetworkResponseListener {
    private Button btn_account_save;
    private ImageView backimg_uprofile;
    private EditText password, confirmPassword, userName;
    private ProgressDialog progressDialog;
    private String mToken;
    private boolean PasswordFetched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chasplus_password_login);
        getSupportActionBar().hide();
        progressDialog = new ProgressDialog(CashpluPasswordActivity.this);
        progressDialog.setMessage("Please Wait ...");
        String Password = getIntent().getStringExtra("Password");


        if (!Password.equals("")) {
            progressDialog.show();
            CashplusLoginModel cashplusLoginModel = new CashplusLoginModel();
            cashplusLoginModel.setPassword(Password);
            cashplusLoginModel.setRefreshToken(true);
            cashplusLoginModel.setUserName(SessionManager.getInstance(this).getPhoneNumberOfCurrentUser());
            Login(cashplusLoginModel);
        }
        btn_account_save = findViewById(R.id.btn_account_save);
        backimg_uprofile = findViewById(R.id.backarrow_userprofile);
        password = findViewById(R.id.password_);
        confirmPassword = findViewById(R.id.confirm_password);
        userName = findViewById(R.id.et_account_phone_number);
        userName.setText(SessionManager.getInstance(this).getPhoneNumberOfCurrentUser());
        btn_account_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.btn_account_save) {
                    String pass = password.getText().toString();
                    String cpass = confirmPassword.getText().toString();
                    if (pass.equals("") || cpass.equals("")) {
                        Toast.makeText(CashpluPasswordActivity.this, getResources().getString(R.string.field_must_not_be_empty), Toast.LENGTH_SHORT).show();
                    } else if (!pass.equals(cpass)) {
                        Toast.makeText(CashpluPasswordActivity.this, getResources().getString(R.string.password_does_not_match), Toast.LENGTH_SHORT).show();
                    } else {

                        ChangePassword();

                    }

                } else if (view.getId() == R.id.backarrow_userprofile) {
                    finish();
                }

            }
        });
        backimg_uprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void Login(CashplusLoginModel cashplusLoginModel) {
        ApiCalls.getInstance(getApplicationContext()).Login(getApplicationContext(), cashplusLoginModel, this);

    }


    private void ChangePassword() {
        progressDialog.show();
        SetUserPasswordModel setUserPasswordModel = new SetUserPasswordModel(SessionManager.getInstance(this).getPhoneNumberOfCurrentUser(), mToken, password.getText().toString());
        ApiCalls.getInstance(getApplicationContext()).SetUserPassword(getApplicationContext(), setUserPasswordModel, this);

    }

    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        progressDialog.dismiss();

        switch (responseForRequest) {

            case Constants.Login:
                if (status) {
                    CashPlusLoginResponseModel cashPlusLoginResponseModel = (CashPlusLoginResponseModel) body;
                    if (cashPlusLoginResponseModel.getErrorCode().equals("0")) {
                        mToken = cashPlusLoginResponseModel.getToken();
                        if (PasswordFetched) {
                            SessionManager.getInstance(getApplicationContext()).setCashPlusToken(mToken);
                            ActivityLauncher.launchCashplusUpdateprofile(CashpluPasswordActivity.this);
                        }
                    } else
                        Toast.makeText(getApplicationContext(), cashPlusLoginResponseModel.getErrorDescription(), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                break;
            case Constants.NewPassword:
                if (status) {

                    PasswordResetModel passwordResetModel = (PasswordResetModel) body;
                    CashplusLoginModel cashplusLoginModel = new CashplusLoginModel();
                    cashplusLoginModel.setPassword(password.getText().toString());
                    cashplusLoginModel.setRefreshToken(true);
                    cashplusLoginModel.setUserName(SessionManager.getInstance(this).getPhoneNumberOfCurrentUser());
                    PasswordFetched = true;
                    Login(cashplusLoginModel);
                } else
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                break;


        }


    }

}