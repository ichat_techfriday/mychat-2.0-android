package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class ApiRequestModel {
    @SerializedName("UserName")
    String userName;
    @SerializedName("token")
    String token;
    @SerializedName("ContactID")
    String ContactID;
    @SerializedName("NewPassword")
    String newPassword;
    @SerializedName("Selected_DTM")
    int SelectedDTM;
    @SerializedName("City_Code")
    String City_Code;
    @SerializedName("bank_code")
    String bank_code;
    @SerializedName("Bank_Code")
    String Bank_Code;
    @SerializedName("payoutCurrency")
    String PayoutCurrency;
    @SerializedName("payingCurrency")
    String PayingCurrency;

    @SerializedName("payout_Location_ID")
    String payout_Location_ID;
    @SerializedName("Service_Provider_Code")
    String Service_Provider_Code;
    @SerializedName("PromoCode")
    String PromoCode;
    @SerializedName("country_Code")
    String country_Code;
    @SerializedName("country_code")
    String country_code;
    @SerializedName("chargesIncluded")
    Boolean ChargesIncluded;

    public String getContactID() {
        return ContactID;
    }

    public void setContactID(String contactID) {
        ContactID = contactID;
    }

    public ApiRequestModel(String userName, String token, String newPassword) {
        this.userName = userName;
        this.token = token;
        this.newPassword = newPassword;
    }

    public ApiRequestModel() {

    }

    public ApiRequestModel(String userName, String token, String country_Code, boolean country) {
        this.userName = userName;
        this.token = token;
        this.country_Code = country_Code;
    }

    public ApiRequestModel(String userName, String token) {
        this.userName = userName;
        this.token = token;
    }


    public ApiRequestModel(String userName, String token, String country_Code, String City_Code) {
        this.userName = userName;
        this.token = token;
        this.country_Code = country_Code;
        this.City_Code = City_Code;
    }

    public ApiRequestModel(String userName, String token, String country_Code, String bank_code, boolean branch) {
        this.userName = userName;
        this.token = token;
        this.country_code = country_Code;
        this.bank_code = bank_code;
    }



    public int getSelectedDTM() {
        return SelectedDTM;
    }

    public void setSelectedDTM(int selectedDTM) {
        SelectedDTM = selectedDTM;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getCity_Code() {
        return City_Code;
    }

    public void setCity_Code(String city_Code) {
        City_Code = city_Code;
    }

    public String getPayoutCurrency() {
        return PayoutCurrency;
    }

    public void setPayoutCurrency(String payoutCurrency) {
        PayoutCurrency = payoutCurrency;
    }

    public String getPayingCurrency() {
        return PayingCurrency;
    }

    public void setPayingCurrency(String payingCurrency) {
        PayingCurrency = payingCurrency;
    }


    public String getPayout_Location_ID() {
        return payout_Location_ID;
    }

    public void setPayout_Location_ID(String payout_Location_ID) {
        this.payout_Location_ID = payout_Location_ID;
    }

    public String getService_Provider_Code() {
        return Service_Provider_Code;
    }

    public void setService_Provider_Code(String service_Provider_Code) {
        Service_Provider_Code = service_Provider_Code;
    }

    public String getPromoCode() {
        return PromoCode;
    }

    public void setPromoCode(String promoCode) {
        PromoCode = promoCode;
    }

    public Boolean getChargesIncluded() {
        return ChargesIncluded;
    }

    public void setChargesIncluded(Boolean chargesIncluded) {
        ChargesIncluded = chargesIncluded;
    }


}
