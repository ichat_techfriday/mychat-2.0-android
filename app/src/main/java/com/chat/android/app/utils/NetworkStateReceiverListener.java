package com.chat.android.app.utils;

public interface NetworkStateReceiverListener {
    public void networkAvailable();
    public void networkUnavailable();
}
