package com.chat.android.app.CustomFontTextView;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

public class NewRobotoFont extends AppCompatTextView {

    public NewRobotoFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NewRobotoFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NewRobotoFont(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/robotoregular.ttf");
        setTypeface(tf);
    }
}
