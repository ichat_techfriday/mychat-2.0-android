package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetContactsModel implements Serializable {
    @SerializedName("AllContacts")
    public List<Contact_Info> saveContactModels;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;

    public List<Contact_Info> getSaveContactModels() {
        return saveContactModels;
    }

    public void setSaveContactModels(List<Contact_Info> saveContactModels) {
        this.saveContactModels = saveContactModels;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }




}
