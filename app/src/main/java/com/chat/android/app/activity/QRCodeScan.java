package com.chat.android.app.activity;

import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import android.view.MenuItem;

import com.chat.android.R;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.service.Constants;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;



/**
 * Created by CAS60 on 11/11/2016.
 */

public class QRCodeScan extends CoreActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private QRCodeReaderView qrCodeReaderView;
    private AvnNextLTProRegTextView tv_header;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcode_scan);
        qrCodeReaderView = findViewById(R.id.scanner);
        tv_header = findViewById(R.id.texthead);
        qrCodeReaderView.setOnQRCodeReadListener(this);
        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);

        // Use this function to change the autofocus interval (default is 5 secs)
        qrCodeReaderView.setAutofocusInterval(2000L);
        setTitle(R.string.scan_code_RQ);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
//        initScanListener();
        tv_header.setText(getString(R.string.drawer_open) + " " + Constants.SOCKET_IP+"web " + getResources().getString(R.string.url_on_computer_scan_QR));
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        qrCodeReaderView.stopCamera();
        MyLog.d("QRScanner", text);
        Intent intent = new Intent();
        intent.putExtra("QRData", text);
        setResult(RESULT_OK, intent);
        finish();
    }


//    public void initScanListener() {
//
//        scanner.setScannerViewEventListener(new ScannerLiveView.ScannerViewEventListener() {
//            @Override
//            public void onScannerStarted(ScannerLiveView scanner) {
//            }
//
//            @Override
//            public void onScannerStopped(ScannerLiveView scanner) {
//            }
//
//            @Override
//            public void onScannerError(Throwable err) {
//                Toast.makeText(QRCodeScan.this, "Error occurred", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onCodeScanned(String data) {
//                scanner.stopScanner();
//                Log.d("QRScanner",data);
//                Intent intent = new Intent();
//                intent.putExtra("QRData", data);
//                setResult(RESULT_OK, intent);
//                finish();
//            }
//        });
//
//    }

    @Override
    protected void onResume() {
        super.onResume();
        qrCodeReaderView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }

    public void onDestroy() {
        super.onDestroy();
        qrCodeReaderView.stopCamera();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}