package com.chat.android.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.chat.android.R;
import com.chat.android.app.activity.AboutHelp;
import com.chat.android.app.dialog.CustomMultiTextItemsDialog;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.model.MultiTextDialogPojo;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.TextUtils;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 *
 */
public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.MyViewHolder> implements Filterable, FastScrollRecyclerView.SectionedAdapter {
    private final Context context;
    public List<FriendModel> mDisplayedValues = new ArrayList<>();
    private List<FriendModel> mOriginalValues = new ArrayList<>();
    Session session;
    private Getcontactname getcontactname;

    private static final String TAG = FriendAdapter.class.getSimpleName() + ">>";
    private ChatListItemClickListener listener;

    public FriendAdapter(Context context, List<FriendModel> data) {
        MyLog.d(TAG, "ScimboCASocket: ");
        this.context = context;
        this.mDisplayedValues = data;
        this.mOriginalValues = data;
        session = new Session(context);
        getcontactname = new Getcontactname(context);

    }

    private static class FriendsDiffUtilCallback extends DiffUtil.Callback {

        List<FriendModel> oldList;
        List<FriendModel> newList;

        FriendsDiffUtilCallback(List<FriendModel> oldList, List<FriendModel> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).get_id().equals(newList.get(newItemPosition).get_id());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
        }
    }

    public FriendModel getItem(int position) {
        return mDisplayedValues != null && mDisplayedValues.size() > 0 ? mDisplayedValues.get(position) : null;
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        FriendModel contact = mDisplayedValues.get(position);
        return contact.getFirstName().substring(0, 1);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        //Typeface face2 = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        TextView tvName, tvStatus, mobileText;

        CircleImageView ivUser;
        public ImageView tick;
        public int pos;
        TextView tvPin;
        View friendStatesContainer;
        View statusCircle;
        ImageView ivUserOnlineStatus;
        View friendRequestAccept;
        View friendRequestReject;
        TextView tvResend;
        TextView tvDelete;

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.userName_contacts);
            tvStatus = view.findViewById(R.id.status_contacts);
            statusCircle = view.findViewById(R.id.status_circle);
            ivUserOnlineStatus = view.findViewById(R.id.iv_on_status);
            tick = view.findViewById(R.id.tick);

            ivUser = view.findViewById(R.id.userPhoto_contacts);
            mobileText = view.findViewById(R.id.mobileText);
            tvPin = view.findViewById(R.id.tv_friend_row_pin);
            friendStatesContainer = view.findViewById(R.id.friend_states_container);
            statusCircle = view.findViewById(R.id.status_circle);
            ivUserOnlineStatus = view.findViewById(R.id.iv_on_status);
            friendRequestAccept = view.findViewById(R.id.iv_friend_request_accept);
            friendRequestReject = view.findViewById(R.id.iv_friend_request_reject);
            tvResend = view.findViewById(R.id.tv_resend);
            tvDelete = view.findViewById(R.id.tv_delete);
        }
    }

    public void updateAdapterList(List<FriendModel> newList) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new FriendsDiffUtilCallback(mDisplayedValues, newList));
        mDisplayedValues.clear();
        for (FriendModel newFriends: newList )
            mDisplayedValues.add(newFriends);
        diffResult.dispatchUpdatesTo(this);
    }

    public void removeFriend(FriendModel model) {
        int index = mDisplayedValues.indexOf(model);
        if(index != -1) {
            mDisplayedValues.remove(model);
            notifyItemRemoved(index);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyLog.d(TAG, "onCreateViewHolder: ");
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contacts_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder viewHolder, final int position) {
        MyLog.d(TAG, "onBindViewHolder: " + position);
        FriendModel contact = mDisplayedValues.get(position);
        //viewHolder.tvStatus.setTextSize(13);
        viewHolder.mobileText.setText(contact.getType());
        viewHolder.tvName.setText(contact.getFirstName());
        viewHolder.pos = position;


        try {
            String userId = contact.get_id();
            if (!contact.getStatus().contentEquals("")) {
                getcontactname.setProfileStatusText(viewHolder.tvStatus, userId, contact.getStatus(), false);
            }
        } catch (Exception e) {
            viewHolder.tvStatus.setText(context.getResources().getString(R.string.default_user_status));
        }

        try {
            String userAvatar = contact.getAvatarImageUrl();
            AppUtils.loadImage(context, AppUtils.getValidProfilePath(userAvatar), viewHolder.ivUser, 0, R.drawable.ic_placeholder_black);

        } catch (Exception e) {
            viewHolder.ivUser.setImageResource(R.drawable.ic_placeholder_black);
            MyLog.e(TAG, "", e);
        }
        setPinCodeView(viewHolder, contact);
        setUIBasedOnStatus(viewHolder, contact);
        handleClickListeners(viewHolder, contact);
        handleOnlineStatus(viewHolder, contact);
    }

    private void handleOnlineStatus(MyViewHolder viewHolder, FriendModel model) {
        String onlineStatus = model.getOnlineStatus();
        int resource = MyChatUtils.getOnlineStatusResource(onlineStatus);
        int statusResource = MyChatUtils.getOnlineStatusImageResource(onlineStatus);
        viewHolder.statusCircle.setBackgroundResource(resource);
        viewHolder.ivUserOnlineStatus.setImageResource(statusResource);
    }

    private void setPinCodeView(MyViewHolder mViewHolderItem, FriendModel contact) {
        mViewHolderItem.tvPin.setVisibility(View.VISIBLE);
        String pinCode = contact.getPinCode();
        mViewHolderItem.tvPin.setText(pinCode);
        boolean isGolden = !TextUtils.isEmpty(pinCode) && (TextUtils.isGoldPin(pinCode) || pinCode.length() == 8);
        int bgRes = R.drawable.grey_rounded_bg_dark;
//        if (isGolden)
//            bgRes = R.drawable.blue_rounded_bg;
        mViewHolderItem.tvPin.setBackgroundResource(bgRes);
    }

    private void setUIBasedOnStatus(MyViewHolder mViewHolderItem, FriendModel model) {
        mViewHolderItem.tvResend.setEnabled(true);
        mViewHolderItem.tvResend.setVisibility(View.GONE);
        mViewHolderItem.tvDelete.setVisibility(View.GONE);
        mViewHolderItem.friendStatesContainer.setVisibility(View.VISIBLE);
        mViewHolderItem.friendRequestAccept.setVisibility(View.GONE);
        mViewHolderItem.friendRequestReject.setVisibility(View.GONE);
        mViewHolderItem.tvPin.setVisibility(View.VISIBLE);
        String status = model.getRequestStatus();
        if (status.equals(AppConstants.FriendStatus.DISABLED.getValue())) {
            mViewHolderItem.tvResend.setText(context.getResources().getString(R.string.resend));
            mViewHolderItem.tvResend.setVisibility(View.VISIBLE);
            mViewHolderItem.tvPin.setVisibility(View.GONE);

            mViewHolderItem.tvResend.setBackgroundResource(R.drawable.all_small_buttons_selector_dark);
            mViewHolderItem.tvResend.setTextColor(MyChatUtils.getColor(context, R.color.colorAccent));
        } else if (status.equals(AppConstants.FriendStatus.REQUEST_RECEIVED.getValue())) {
            mViewHolderItem.friendRequestAccept.setVisibility(View.VISIBLE);
            mViewHolderItem.friendRequestReject.setVisibility(View.VISIBLE);
            mViewHolderItem.tvPin.setVisibility(View.GONE);
        } else if (status.equals(AppConstants.FriendStatus.REQUEST_SENT.getValue())) {
            mViewHolderItem.tvResend.setText(context.getResources().getString(R.string.sent));
            mViewHolderItem.tvResend.setVisibility(View.VISIBLE);
            mViewHolderItem.tvResend.setEnabled(false);
            mViewHolderItem.tvResend.setBackgroundResource(R.drawable.all_grey_bg);
            mViewHolderItem.tvResend.setTextColor(MyChatUtils.getColor(context, R.color.grey_50));
            mViewHolderItem.tvPin.setVisibility(View.GONE);
            mViewHolderItem.tvDelete.setVisibility(View.VISIBLE);
        }
    }

    private void handleClickListeners(final MyViewHolder mViewHolderItem, final FriendModel model) {

        mViewHolderItem.friendRequestAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptRequest(model.getRequestId());
            }
        });
        mViewHolderItem.friendRequestReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteRequest(model.getRequestId());
            }
        });
        mViewHolderItem.tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // emit(SocketConfig.Events.ADD_FRIEND, SocketConfig.PARAM_KEY.PIN_CODE, model.getPinCode(), model);
            }
        });
        mViewHolderItem.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteRequest(model.getRequestId());
            }
        });

        mViewHolderItem.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onItemClick(mViewHolderItem.itemView, mViewHolderItem.getAdapterPosition());
                }
            }
        });
        mViewHolderItem.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                String requestStatus = model.getRequestStatus();
                if (requestStatus.equals(AppConstants.FriendStatus.FRIENDS.getValue()))
                    showDialog(mViewHolderItem, model);
                return false;
            }
        });
    }

    private void showDialog(MyViewHolder mViewHolderItem, final FriendModel model) {
        List<MultiTextDialogPojo> labelsList = new ArrayList<>();
        MultiTextDialogPojo label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.ic_copy_to_clipboard);
        label.setLabelText(context.getResources().getString(R.string.copy_pin));
        labelsList.add(label);

        label = new MultiTextDialogPojo();
        label.setImageResource(R.drawable.ic_remove_contact);
        label.setLabelText(context.getResources().getString(R.string.remove_contact));
        labelsList.add(label);

        CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
        dialog.setLabelsList(labelsList);

        dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
            @Override
            public void onDialogItemClick(int position) {
                switch (position) {
                    case 0:
                        MyChatUtils.copyToClipboard(context, model.getPinCode());
                        break;
                    case 1:
                        unfriendRequest(model.get_id());
                        break;
                }
            }
        });
        dialog.show(((CoreActivity) context).getSupportFragmentManager(), "Friend options");
    }

    private void acceptRequest(String requestId) {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_ACCEPT_FRIEND_REQUEST);
        JSONObject obj = new JSONObject();
        try {
            obj.put("requestId", requestId);
            obj.put("userId", SessionManager.getInstance(context).getCurrentUserID());
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        MyLog.e(TAG, "acceptRequest : " + obj);
        event.setMessageObject(obj);
        EventBus.getDefault().post(event);
    }

    private void deleteRequest(String requestId) {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_DELETE_FRIEND_REQUEST);
        JSONObject obj = new JSONObject();
        try {
            obj.put("requestId", requestId);
            obj.put("userId", SessionManager.getInstance(context).getCurrentUserID());
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        MyLog.e(TAG, "deleteRequest : " + obj);
        event.setMessageObject(obj);
        EventBus.getDefault().post(event);
    }

    private void unfriendRequest(String friendId) {
        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_UN_FRIEND_REQUEST);
        JSONObject obj = new JSONObject();
        try {
            obj.put("userId", SessionManager.getInstance(context).getCurrentUserID());
            obj.put("friendId", friendId);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
        MyLog.e(TAG, "unfriendRequest : " + obj);
        event.setMessageObject(obj);
        EventBus.getDefault().post(event);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<FriendModel>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
//                    Toast.makeText(context, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<FriendModel> FilteredArrList = new ArrayList<>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (FriendModel friendModel : mOriginalValues) {


                        String contactName = friendModel.getFirstName();
                        String contactNo = friendModel.getNumberInDevice();

                        if (contactName.toLowerCase().contains(constraint) || contactNo.toLowerCase().contains(constraint)) {
                            FriendModel mycontact = new FriendModel();
                            mycontact.setFirstName(friendModel.getFirstName());

                            mycontact.set_id(friendModel.get_id());
                            mycontact.setStatus(friendModel.getStatus());
                            mycontact.setAvatarImageUrl(friendModel.getAvatarImageUrl());
                            mycontact.setNumberInDevice(friendModel.getNumberInDevice());
                            mycontact.setPinCode(friendModel.getPinCode());
                            mycontact.setRequestStatus(friendModel.getRequestStatus());
                            mycontact.setRequestId(friendModel.getRequestId());
                            MyLog.e("ScimboCASocket", "setNumberInDevice" + friendModel.getNumberInDevice());

                            FilteredArrList.add(mycontact);
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }


    public void setChatListItemClickListener(ChatListItemClickListener listener) {
        this.listener = listener;
    }

    public interface ChatListItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

}


