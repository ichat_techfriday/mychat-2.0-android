package com.chat.android.app.web;

import android.content.Context;

import com.chat.android.core.model.SaveSubDataResponse;
import com.chat.android.core.model.ValidateSubResponse;
import com.chat.android.core.service.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class IChatWithoutHeaderAPIController {
    private static IChatWithoutHeaderAPI apiInstance;

    private static void init(Context context) {
        try {

            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
            builder.readTimeout(1, TimeUnit.MINUTES);
            builder.connectTimeout(15, TimeUnit.SECONDS);
            builder.writeTimeout(1, TimeUnit.MINUTES);

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);

            Retrofit retrofit = new Retrofit.Builder()
                    .client(builder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .build();

            apiInstance = retrofit.create(IChatWithoutHeaderAPI.class);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static IChatWithoutHeaderAPI getApiInstance(Context context) {
        if (apiInstance == null) {
            init(context);
        }
        return apiInstance;
    }

    public static void translateText(Context context, String url, Callback<ResponseBody> callback) {
        if (apiInstance == null)
            apiInstance = getApiInstance(context);
        Call<ResponseBody> updateRequestStatus = getApiInstance(context).translateText(url);
        updateRequestStatus.enqueue(callback);
    }

    public static void disconnectCall(Context context, String url, Callback<ResponseBody> callback) {
        if (apiInstance == null)
            apiInstance = getApiInstance(context);
        Call<ResponseBody> updateRequestStatus = getApiInstance(context).disconnectCall(url);
        updateRequestStatus.enqueue(callback);
    }

    public static void verifybyEmail(Context context, String url, Callback<ResponseBody> callback) {
        if (apiInstance == null)
            apiInstance = getApiInstance(context);
        Call<ResponseBody> updateRequestStatus = getApiInstance(context).disconnectCall(url);
        updateRequestStatus.enqueue(callback);
    }

    public static void saveSubPurchaseData(Context context, String userId, String subId,
                                           String purchaseToken, String deviceId, long purchaseTime,
                                           Callback<SaveSubDataResponse> callback) {
        if (apiInstance == null)
            apiInstance = getApiInstance(context);
        Call<SaveSubDataResponse> updateRequestStatus = getApiInstance(context).saveSubPurchaseData(userId,
                subId, purchaseToken, deviceId, purchaseTime);
        updateRequestStatus.enqueue(callback);
    }

    public static synchronized void validateSubscription(Context context, String userId, String subId,
                                            String purchaseToken, String deviceId, long purchaseTime,
                                            Callback<ValidateSubResponse> callback) {
        if (apiInstance == null)
            apiInstance = getApiInstance(context);
        Call<ValidateSubResponse> updateRequestStatus = getApiInstance(context)
                .validateSubscription(userId,subId, purchaseToken, deviceId, purchaseTime);
        updateRequestStatus.enqueue(callback);
    }
}

