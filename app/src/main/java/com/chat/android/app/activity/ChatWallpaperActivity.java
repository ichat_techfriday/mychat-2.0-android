package com.chat.android.app.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.Session;
import com.chat.android.utils.AppConstants;

public class ChatWallpaperActivity extends AppCompatActivity {
    Session session;
    ImageView iv_back;
    String imgDecodableString;
    private static final int RESULT_WALLPAPER = 8;
    private static final int SELECT_VIDEO = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_wallpaper);
        session = new Session(ChatWallpaperActivity.this);
        RelativeLayout rl_gallery_container = (RelativeLayout)findViewById(R.id.rl_gallery_container);
        rl_gallery_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(photoPickerIntent, RESULT_WALLPAPER);
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent, RESULT_WALLPAPER);
                }
            }
        });

        RelativeLayout rl_video_wallpaper_container = (RelativeLayout)findViewById(R.id.rl_video_wallpaper_container);
        rl_video_wallpaper_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                }else{
                    intent = new Intent(Intent.ACTION_GET_CONTENT);
                }
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setType("video/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_VIDEO);
            }
        });
        RelativeLayout rl_solid_colors = (RelativeLayout)findViewById(R.id.rl_solid_colors_container);
        rl_solid_colors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChatWallpaperActivity.this, SolidColorsActivity.class);
                startActivity(i);
            }
        });

        RelativeLayout rl_default_container = (RelativeLayout)findViewById(R.id.rl_default_container);
        rl_default_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String setbgdef = "def";
                session.putgalleryPrefs(setbgdef);
            }
        });

        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @SuppressLint("WrongConstant")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_WALLPAPER && resultCode == RESULT_OK && null != data) {
            try {
                Uri selectedImageUri = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                if (imgDecodableString == null) {
                    imgDecodableString = getRealFilePath(data);
                }
                cursor.close();

                session.putgalleryPrefs(imgDecodableString);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == SELECT_VIDEO && resultCode == RESULT_OK && null != data){
            Uri uri = data.getData();
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                final int takeFlags = data.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
                ContentResolver resolver = getApplicationContext().getContentResolver();
                resolver.takePersistableUriPermission(uri, takeFlags);
                String video_str = "video#@" + uri;
                session.putgalleryPrefs(video_str);
            }

        }
    }
    private String getRealFilePath(Intent data) {
        Uri selectedImage = data.getData();
        String wholeID = DocumentsContract.getDocumentId(selectedImage);
        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);
        String filePath = "";
        int columnIndex = 0;
        if (cursor != null) {
            columnIndex = cursor.getColumnIndex(column[0]);
        }
        if (cursor != null && cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }
}