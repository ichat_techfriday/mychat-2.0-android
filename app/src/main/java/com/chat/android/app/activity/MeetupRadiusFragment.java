package com.chat.android.app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.utils.MyLog;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.StorageUtility;

public class MeetupRadiusFragment extends Fragment {

    private SeekBar mRadiusSeekbar;
    private Bundle mBundle;
    private Context mContext;
    private Activity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meetup_radius, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //init
        ImageView imageView = (ImageView) view.findViewById(R.id.ic_search);
        imageView.setColorFilter(getContext().getResources().getColor(R.color.white));
        TextView radius_text = (TextView) view.findViewById(R.id.radius_text);
        RelativeLayout continue_btn = (RelativeLayout) view.findViewById(R.id.continue_btn);
        mRadiusSeekbar = (SeekBar) view.findViewById(R.id.radius_seekbar);
        mBundle = new Bundle();
        int step = 10;
        int max = 1000;
        final int min = 40;

        mRadiusSeekbar.setMax( (max - min) / step );
        continue_btn.setOnClickListener(v -> {
            int distance = (mRadiusSeekbar.getProgress() * step) + min;
            if (StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.MEETUP_INTRO_COMPLETED.getValue(), false) && !(mActivity instanceof MeetupActivity)){
                Intent intent = new Intent(getActivity(), MeetupActivity.class);
                intent.putExtra(AppConstants.IntentKeys.MEETUP_SCREEN_TYPE.getValue(), AppConstants.MEETUP_SCREEN_NAME.MeetupProfileFragment.getValue());
                intent.putExtra(AppConstants.IntentKeys.MEETUP_DISTANCE_IN_METERS.getValue(), distance);

                startActivity(intent);
            } else {
                MeetupProfileFragment meetupProfileFragment = new MeetupProfileFragment();
                mBundle.putInt(AppConstants.IntentKeys.MEETUP_DISTANCE_IN_METERS.getValue(), distance);
                meetupProfileFragment.setArguments(mBundle);
                OpenFragment(meetupProfileFragment);
            }
        });
        //setting seekbar colors and change listener
        mRadiusSeekbar.getProgressDrawable().setColorFilter(getResources().getColor(R.color.seekbar_progress_color), PorterDuff.Mode.MULTIPLY);
        mRadiusSeekbar.getThumb().setColorFilter(getResources().getColor(R.color.seekbar_progress_color), PorterDuff.Mode.SRC_ATOP);
        mRadiusSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int value = min + (progress * step);
                radius_text.setText(value + " meters");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void OpenFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;
        FragmentManager manager = getActivity().getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(R.id.frame_layout, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}