package com.chat.android.app.adapter;


/**
 * Controller class to feed the chatlist view based on chatlist gettter-setter item values
 */


import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chat.android.R;
import com.chat.android.app.activity.GroupChatList;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.TimeStampUtils;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.ShortcutBadgeManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.MuteStatusPojo;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Casperon Technologyon 15/04/16.
 */

public class ChatListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable, FastScrollRecyclerView.SectionedAdapter {

    private static final String TAG = "ChatListAdapter";
    private List<MessageItemChat> mListData;
    public List<MessageItemChat> mDisplayedValues;
    private ShortcutBadgeManager shortcutBadgeManager;
    private ChatListItemClickListener listener;
    public Getcontactname getcontactname;
    private String currentUserId;
    private Context mContext;
    private Session session;
    private UserInfoSession userInfoSession;
    private long imageTS;
    private GroupChatList callback;
    private MessageDbController messageDbController;
    public ChatListAdapter(Context mContext, ArrayList<MessageItemChat> mListData) {
        this.mListData = mListData;
        this.mDisplayedValues = mListData;
        this.mListData = mListData;
        this.mContext = mContext;
        session = new Session(mContext);
        userInfoSession = new UserInfoSession(mContext);
        messageDbController=new MessageDbController(mContext);
        shortcutBadgeManager = new ShortcutBadgeManager(mContext);
        currentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        getcontactname = new Getcontactname(mContext);
        imageTS = Calendar.getInstance().getTimeInMillis();
    }


    @Override
    public int getItemCount() {
        return this.mDisplayedValues.size();
    }


    @Override
    public int getItemViewType(int position) {
        return 1;
    }


    public MessageItemChat getItem(int position) {
        return mDisplayedValues.get(position);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());


        View v = inflater.inflate(R.layout.lp_f3_chat, viewGroup, false);
        viewHolder = new ViewHolderChat(v);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final ViewHolderChat vh2 = (ViewHolderChat) viewHolder;

        configureViewHolderChat(vh2, position);
        setItemClickListener(vh2, position);
    }

    private void setItemClickListener(final ViewHolderChat vh2, final int position) {
        if (listener != null) {
            vh2.rlChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(mDisplayedValues.get(position),vh2.rlChat, position, 0);
                }
            });

            vh2.rlChat.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.onItemLongClick(mDisplayedValues.get(position),vh2.itemView, position);
                    return false;
                }
            });

            vh2.storeImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(mDisplayedValues.get(position),vh2.storeImage, position, imageTS);
                }
            });

            vh2.storeImage.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    listener.onItemLongClick(mDisplayedValues.get(position),vh2.itemView, position);
                    return false;
                }
            });
        }
    }


    private void configureViewHolderChat(ViewHolderChat vh, int position) {
        final MessageItemChat chat = mDisplayedValues.get(position);

        vh.ivMsgType.setVisibility(View.GONE);

        if(chat.isSecretChat())
            vh.ivChatIcon.setVisibility(View.VISIBLE);
        else
            vh.ivChatIcon.setVisibility(View.GONE);
        String Name=chat.getSenderName();

        if(Name!=null && !Name.equalsIgnoreCase("")){

            vh.storeName.setText(chat.getSenderName());

        }else{

            vh.storeName.setText(chat.getSenderMsisdn());
        }

        if(chat.isSecretChat()){
            vh.storeName.setTextColor(ContextCompat.getColor(mContext,R.color.secret_chat_list_color));
        }
        else {
            vh.storeName.setTextColor(ContextCompat.getColor(mContext,R.color.chat_list_header));
        }

        if (chat.getMessageType() != null) {
            vh.newMessage.setVisibility(View.VISIBLE);
            vh.tvTyping.setVisibility(View.GONE);
            if (!chat.getMessageType().equals("" + MessageFactory.text)) {
                vh.ivMsgType.setVisibility(View.VISIBLE);
            }

            if (chat.getTypingAt() != 0) {
                vh.newMessage.setVisibility(View.GONE);
                vh.ivMsgType.setVisibility(View.GONE);
                vh.tvTyping.setVisibility(View.VISIBLE);
                if (chat.getMessageId().contains("-g-")) {
                    vh.tvTyping.setText(chat.getTypePerson().concat(mContext.getResources().getString(R.string.typing_dots)));
                } else {
                    vh.tvTyping.setText(mContext.getResources().getString(R.string.typing_dots));
                }
            }

            if (chat.getMessageType().equals("" + MessageFactory.text)) {
                vh.newMessage.setTextColor(ContextCompat.getColor(mContext, R.color.chatlist_messagecolor));
                vh.newMessage.setText(chat.getTextMessage());
                vh.ivMsgType.setImageResource(0);
            } else if (chat.getMessageType().equals("" + MessageFactory.picture)) {
                vh.newMessage.setText(mContext.getResources().getString(R.string.image));
                vh.ivMsgType.setImageResource(R.drawable.camera_iconnn);
            } else if (chat.getMessageType().equals("" + MessageFactory.contact)) {
                vh.newMessage.setText(mContext.getResources().getString(R.string.Contact));
                vh.ivMsgType.setImageResource(R.drawable.contact);
            } else if (chat.getMessageType().equals("" + MessageFactory.video)) {
                vh.newMessage.setText(mContext.getResources().getString(R.string.video));
                vh.ivMsgType.setImageResource(R.drawable.video);
            } else if (chat.getMessageType().equals("" + MessageFactory.audio)) {
                vh.newMessage.setText(mContext.getResources().getString(R.string.audio));
                vh.ivMsgType.setImageResource(R.drawable.audio);
            } else if (chat.getMessageType().equals("" + MessageFactory.document)) {
                vh.newMessage.setText(mContext.getResources().getString(R.string.document));
                vh.ivMsgType.setImageResource(R.drawable.document);
            } else if (chat.getMessageType().equals("" + MessageFactory.web_link)) {
                vh.newMessage.setText(mContext.getResources().getString(R.string.weblink));
                vh.ivMsgType.setImageResource(R.drawable.link);
            } else if (chat.getMessageType().equals("" + MessageFactory.location)) {
                vh.newMessage.setText(mContext.getResources().getString(R.string.location));
                vh.ivMsgType.setImageResource(R.drawable.map);
            } else if (chat.getMessageType().equals("" + MessageFactory.missed_call)) {
                String callType = chat.getCallType();

//                String tsNextLine = TimeStampUtils.get12HrTimeFormat(mContext, chat.getTS());

                if (callType != null && callType.equals(MessageFactory.video_call + "")) {
                    vh.newMessage.setText(mContext.getResources().getString(R.string.missed_video_call));
                } else {
                    vh.newMessage.setText(mContext.getResources().getString(R.string.missed_voice_call));
                }

                vh.ivMsgType.setImageResource(R.drawable.ic_missed_call);
            } else if (chat.getMessageType().equals("" + MessageFactory.group_event_info) ||
                    chat.getMessageType().equals("" + MessageFactory.event_info_change_pin)) {
                vh.ivMsgType.setImageResource(0);

                String createdBy = chat.getCreatedByUserId();
                String createdTo = chat.getCreatedToUserId();
                String groupName = chat.getGroupName();

                String createdByName = null, createdToName = null;
                String msg = null;


                switch (chat.getGroupEventType()) {

                    case "" + MessageFactory.join_new_group:
                        if (createdBy.equalsIgnoreCase(currentUserId)) {
                            createdByName = mContext.getResources().getString(R.string.you);
                        } else {
                            createdByName = getContactNameIfExists(createdBy);
                        }
                        msg = createdByName + " "+mContext.getResources().getString(R.string.create_group)+" '" + groupName + "'";
                        break;

                    case "" + MessageFactory.add_group_member:
                        if (createdBy.equalsIgnoreCase(currentUserId)) {
                            createdByName =  mContext.getResources().getString(R.string.you);
                        } else {
                            createdByName = getContactNameIfExists(createdBy);
                        }
                        if (createdTo.equalsIgnoreCase(currentUserId)) {
                            createdToName =  mContext.getResources().getString(R.string.you);
                        } else {
                            createdToName = getContactNameIfExists(createdTo);
                        }
                        msg = createdByName + " "+mContext.getResources().getString(R.string.added)+" " + createdToName;
                        break;

                    case "" + MessageFactory.change_group_icon:
                        if (createdBy.equalsIgnoreCase(currentUserId)) {
                            createdByName =  mContext.getResources().getString(R.string.you);
                        } else {
                            createdByName = getContactNameIfExists(createdBy);
                        }

                        msg = createdByName + " "+mContext.getResources().getString(R.string.changed_group_icon);
                        break;

                    case "" + MessageFactory.change_group_name:
                        if (createdBy.equalsIgnoreCase(currentUserId)) {
                            createdByName =  mContext.getResources().getString(R.string.you);
                        } else {
                            createdByName = getContactNameIfExists(createdBy);
                        }

                        msg = createdByName + " "+mContext.getResources().getString(R.string.changed_group_name)+" " + chat.getPrevGroupName() + "' "+mContext.getResources().getString(R.string.to)+" '"
                                + chat.getGroupName() + "'";
                        break;

                    case "" + MessageFactory.delete_member_by_admin:
                        if (createdBy.equalsIgnoreCase(currentUserId)) {
                            createdByName =  mContext.getResources().getString(R.string.you);
                        } else {
                            createdByName = getContactNameIfExists(createdBy);
                        }

                        if (createdTo.equalsIgnoreCase(currentUserId)) {
                            createdToName = mContext.getResources().getString(R.string.you);
                        } else {
                            createdToName = getContactNameIfExists(createdTo);
                        }
                        msg = createdByName + " "+mContext.getResources().getString(R.string.removed)+" " + createdToName;
                        break;

                    case "" + MessageFactory.make_admin_member:
                        if (createdTo.equalsIgnoreCase(currentUserId)) {
                            createdToName = mContext.getResources().getString(R.string.you_are)+" ";
                        } else {
                            createdToName = getContactNameIfExists(createdTo);
                        }
                        msg = createdToName + " "+mContext.getResources().getString(R.string.now_admin);
                        break;

                    case "" + MessageFactory.exit_group:
                        if (createdBy.equalsIgnoreCase(currentUserId)) {
                            createdByName =  mContext.getResources().getString(R.string.you)+" ";
                        } else {
                            createdByName = getContactNameIfExists(createdBy);
                        }
                        msg = createdByName + " "+mContext.getResources().getString(R.string.left);
                        break;

                }

                if (msg != null) {
                    vh.newMessage.setText(msg);
                } else {
                    vh.newMessage.setText("");
                }

            } else if (chat.getMessageType().equals("" + MessageFactory.DELETE_SELF)) {
                vh.newMessage.setText(mContext.getResources().getString(R.string.you_deleted_text));
                vh.ivMsgType.setImageResource(R.drawable.icon_deleted);

            } else if (chat.getMessageType().equals("" + MessageFactory.DELETE_OTHER)) {
                String msgId=chat.getMessageId();
                MyLog.d(TAG, "configureViewHolderChat msgID: "+msgId);
                if(messageDbController.getMessageCount(chat.getMessageId())>0) {
                    vh.newMessage.setText(mContext.getResources().getString(R.string.other_deleted_text));
                    vh.ivMsgType.setImageResource(R.drawable.icon_deleted);
                }
            }
            else if (chat.getMessageType().equals("" + MessageFactory.SCREEN_SHOT_TAKEN)) {
                vh.newMessage.setTextColor(ContextCompat.getColor(mContext, R.color.chatlist_messagecolor));
                if(chat.isSelf())
                    vh.newMessage.setText(chat.getTextMessage());
                else
                    vh.newMessage.setText(vh.storeName.getText());
                vh.ivMsgType.setImageResource(0);
            }
            else {
                vh.newMessage.setText("");
                vh.ivMsgType.setImageResource(0);
            }
        } else {
            vh.newMessage.setText("");
        }




        try {

            configureDateLabel(vh.newMessageDate, position);
        } catch (Exception e) {
            MyLog.e(TAG,"",e);
        }

        vh.newMessageCount.setVisibility(View.GONE);
        String[] arrDocId = chat.getMessageId().split("-");
        String toUserId = arrDocId[1];
        String docId = currentUserId.concat("-").concat(toUserId);
        MuteStatusPojo muteData = null;
        String convId = null;
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);

        if (chat.getMessageId().contains("-g-")) {
            docId = docId.concat("-g");
            convId = toUserId;
            muteData = contactDB_sqlite.getMuteStatus(currentUserId, null, arrDocId[1], false);

            String path = chat.getAvatarImageUrl();
            if (path != null && !path.equals("")) {

                path = AppUtils.getValidProfilePath(path);

                //RequestOptions options=new RequestOptions().error(R.mipmap.group_chat_attachment_profile_icon);
                /*Glide.with(mContext).load(path)
                        .into(vh.storeImage);*/
                AppUtils.loadImage(mContext,path,vh.storeImage,100,0);
            } else {
                Glide.with(mContext).load(R.mipmap.group_chat_attachment_profile_icon)
                        .into(vh.storeImage);
            }
        } else {
            convId = userInfoSession.getChatConvId(docId);
            muteData = contactDB_sqlite.getMuteStatus(currentUserId, toUserId, convId, false);
            getcontactname.configProfilepic(vh.storeImage, toUserId, false, true, R.drawable.ic_placeholder_black);
        }

        if (muteData != null && muteData.getMuteStatus().equals("1")) {
            if(!chat.isSecretChat())
                vh.mute_chatlist.setVisibility(View.VISIBLE);
        } else {
            vh.mute_chatlist.setVisibility(View.GONE);
        }


        if (convId != null && !convId.equals("")) {
            int countMsg = shortcutBadgeManager.getSingleBadgeCount(convId);
            if (countMsg > 0 || !session.getmark(toUserId)) {
                if(!chat.isSecretChat())
                    vh.newMessageCount.setVisibility(View.VISIBLE);
                if (countMsg > 0) {
                    vh.newMessageCount.setText("" + countMsg);
                } else {
                    vh.newMessageCount.setText("");
                }
            } else {
                vh.newMessageCount.setVisibility(View.GONE);
            }
        } else {
            vh.newMessageCount.setVisibility(View.GONE);
        }

        if (chat.isSelected()) {
            vh.tick.setVisibility(View.VISIBLE);
        } else {
            vh.tick.setVisibility(View.GONE);
        }


    }

    private void configureDateLabel(TextView tvDateLbl, int position) {

        MessageItemChat item = mDisplayedValues.get(position);
        if (item.getTS() != null && !item.getTS().equals("")) {
            String currentItemTS = item.getTS();

            if (currentItemTS.equals("0")) {
                tvDateLbl.setText("");
            } else {

                Date currentItemDate = TimeStampUtils.getMessageTStoDate(mContext, currentItemTS);
                if (currentItemDate != null) {
                    String mydate = TimeStampUtils.get12HrTimeFormat(mContext, item.getTS());
                    mydate = mydate.replace(".", "");
                    setDateText(tvDateLbl, currentItemDate, currentItemTS, mydate);
                } else {
                    tvDateLbl.setText("");
                }
            }
        } else {
            tvDateLbl.setText("");
        }
    }

    private void setDateText(TextView tvDateLbl, Date currentItemDate, String ts, String time) {
        Date today = TimeStampUtils.getDateFormat(Calendar.getInstance().getTimeInMillis());
        Date yesterday = TimeStampUtils.getYesterdayDate(today);

        if (currentItemDate.equals(today)) {
            tvDateLbl.setText(time);
        } else if (currentItemDate.equals(yesterday)) {
            tvDateLbl.setText(mContext.getResources().getString(R.string.yesterday));
        } else {
            DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            String formatDate = df.format(currentItemDate);
            tvDateLbl.setText(formatDate);
        }
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<MessageItemChat>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
                    // Toast.makeText(mContext, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<MessageItemChat> FilteredArrList = new ArrayList<>();

                if (mListData == null) {
                    mListData = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mListData.size();
                    results.values = mListData;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mListData.size(); i++) {


                        String senderName = mListData.get(i).getSenderName();
                        if (senderName.toLowerCase().contains(constraint)) {
                            FilteredArrList.add(mListData.get(i));
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public void setChatListItemClickListener(ChatListItemClickListener listener) {
        this.listener = listener;
    }

    private String getContactNameIfExists(String userId) {
        String userName = null;
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
        String msisdn=contactDB_sqlite.getSingleData(userId,ContactDB_Sqlite.MSISDN);
        if (msisdn != null) {
            userName = getcontactname.getSendername(userId, msisdn);
        } else {
            if (callback != null) {
                callback.getUserDetails(userId);
            }
        }
        return userName;
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        return mListData.get(position).getSenderName().substring(0, 1);
    }

    public void setCallback(GroupChatList groupChatList) {
        callback = groupChatList;
    }

    public interface ChatListItemClickListener {
        void onItemClick(MessageItemChat messageItemChat,View view, int position, long imageTS);

        void onItemLongClick(MessageItemChat messageItemChat,View view, int position);
    }

}




