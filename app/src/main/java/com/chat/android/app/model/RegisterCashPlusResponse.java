package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class RegisterCashPlusResponse {
    @SerializedName("ResponseID")
    public String responseID;
    @SerializedName("ErrorCode")
    public String errorCode;

    public String getResponseID() {
        return responseID;
    }

    public void setResponseID(String responseID) {
        this.responseID = responseID;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @SerializedName("ErrorMessage")
    public String errorMessage;

    public RegisterCashPlusResponse(String responseID, String errorCode, String errorMessage) {
        this.responseID = responseID;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}
