package com.chat.android.app.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chat.android.R;
import com.chat.android.app.adapter.LanguageSelectionAdapter;
import com.chat.android.core.CoreActivity;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.StorageUtility;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LanguageSelectionActivity extends CoreActivity implements View.OnClickListener{

    TextView mHeaderTitle;
    View mBack;
    RecyclerView mRecyclerView;
    EditText mSearch;
    private String languageKey;
    private List<LanguageModel> languageModels;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_selection);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            languageKey = bundle.getString(AppConstants.IntentKeys.EXTRA_KEY_LANGUAGE_KEY.getValue());
        }
        mContext = this;
        initViews(this);
    }

    public void initViews(Context context) {
        mHeaderTitle = findViewById(R.id.tv_title);
        mBack = findViewById(R.id.iv_back);
        mRecyclerView = findViewById(R.id.recycler_view);
        mSearch = findViewById(R.id.et_search);

        Bundle bundle = getIntent().getExtras();
        mBack.setOnClickListener(this);
        mHeaderTitle.setText(mContext.getText(R.string.outgoing_message_language));
        populateList();
        setAdapter(languageModels);
        mSearch.addTextChangedListener(textWatcher);
        KeyboardVisibilityEvent.setEventListener(
                (Activity) mContext,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if(isOpen)
                            mSearch.setCursorVisible(true);
                        else
                            mSearch.setCursorVisible(false);
                    }
                });
    }

    private void populateList() {
        languageModels = new ArrayList<>();
        List<String> originalLanguageNamesApp = Arrays.asList(getResources().getStringArray(R.array.originalLanguageNames));
        List<String> englishLanguageNamesApp  = Arrays.asList(getResources().getStringArray(R.array.englishLanguageNames));
        List<String> languageCodeNames  = Arrays.asList(getResources().getStringArray(R.array.languageCodes));
        String alreadySelected = StorageUtility.getDataFromPreferences(this, languageKey, "");

        for(int i = 0; i < originalLanguageNamesApp.size(); i++) {
            String original = originalLanguageNamesApp.get(i);
            String english  = englishLanguageNamesApp.get(i);
            String code     = languageCodeNames.get(i);
            LanguageModel model = new LanguageModel(original, english, code, alreadySelected.equals(english));
            languageModels.add(model);
        }
    }
    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            handleSearchLocally(arg0.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {
        }

        @Override
        public void afterTextChanged(Editable arg0) {
        }
    };

    private void handleSearchLocally(String arg0) {
        if(arg0.length() == 0) {
            populateList();
            setAdapter(languageModels);
        } else if( arg0.length() > 0) {
            List<LanguageModel> filteredContacts = checkFilter(arg0.toLowerCase());
            if (filteredContacts != null) {
                setAdapter(filteredContacts);
            }
        }
    }

    private void setAdapter(List<LanguageModel> languageModels) {
        LanguageSelectionAdapter mAdapter = new LanguageSelectionAdapter(mContext, languageModels);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                ((Activity)mContext).onBackPressed();
                break;
        }
    }

    private List<LanguageModel> checkFilter(String toMatch) {
        List<LanguageModel> filteredModels = new ArrayList<>();
        if(languageModels != null) {
            for (int i = 0; i < languageModels.size(); i++) {
                LanguageModel item = languageModels.get(i);
                String englishName = item.getEnglishName().toLowerCase();
                String originalName = item.getOriginalName().toLowerCase();
                if (englishName.contains(toMatch) || originalName.contains(toMatch)) {
                    filteredModels.add(item);
                }
            }
        }
        return filteredModels;
    }

    public class LanguageModel {
        String originalName;
        String englishName;
        String languageCode;
        boolean isSelected;

        LanguageModel(String originalName, String englishName, String languageCode, boolean isSelected) {
            this.originalName = originalName;
            this.englishName = englishName;
            this.languageCode = languageCode;
            this.isSelected = isSelected;
        }

        public String getOriginalName() {
            return originalName;
        }

        public void setOriginalName(String originalName) {
            this.originalName = originalName;
        }

        public String getEnglishName() {
            return englishName;
        }

        public void setEnglishName(String englishName) {
            this.englishName = englishName;
        }

        public String getLanguageCode() {
            return languageCode;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
