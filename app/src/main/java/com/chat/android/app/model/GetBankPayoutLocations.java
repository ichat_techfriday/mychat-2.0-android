package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetBankPayoutLocations implements Serializable {
    @SerializedName("Banks")
    public Banks Banks;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public GetBankPayoutLocations.Banks getBanks() {
        return Banks;
    }

    public void setBanks(GetBankPayoutLocations.Banks banks) {
        Banks = banks;
    }

    public class Banks implements Serializable {
        @SerializedName("AllBanks")
        public List<AvailableBanks> allBanks;
    }


    public class AvailableBanks implements Serializable {





        @SerializedName("BankName")
        public String bankName;
        @SerializedName("BankCode")
        public String bankCode;
        @SerializedName("IBAN_Required")
        public boolean iBAN_Required;
        @SerializedName("RoutingNoABA_Code_Required")
        public boolean routingNoABA_Code_Required;
        @SerializedName("Sort_Code_Required")
        public boolean sort_Code_Required;
        @SerializedName("BIC_or_Swift_Code_Required")
        public boolean bIC_or_Swift_Code_Required;
        @SerializedName("Bank_Branch_Required")
        public boolean bank_Branch_Required;
        @SerializedName("Bank_ID")
        public String bank_ID;
        @SerializedName("Bank_Address")
        public String bank_Address;
        @SerializedName("Bank_Phone")
        public String bank_Phone;
        @SerializedName("Service_Provider_Code")
        public String service_Provider_Code;
        @SerializedName("Service_Provider_Name")
        public String service_Provider_Name;
        @SerializedName("Bank_Account_Number_IBAN")
        public String Bank_Account_Number_IBAN;
        @SerializedName("Bank_Swift_BIC")
        public String Bank_Swift_BIC;
        @SerializedName("Bank_Routing_ABA")
        public String Bank_Routing_ABA;
        @SerializedName("Bank_SortCode")
        public String Bank_SortCode;

        @Override
        public String toString() {
            return bankName;
        }
    }


}
