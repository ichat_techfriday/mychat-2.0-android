package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetCities implements Serializable {
    @SerializedName("Cities")
    public Cities Cities;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;


    public GetCities.Cities getCities() {
        return Cities;
    }

    public void setCities(GetCities.Cities cities) {
        Cities = cities;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public static class AllCity implements Serializable {
        @SerializedName("CityCode")
        public String cityCode;
        @SerializedName("CityName")
        public String cityName;
        @SerializedName("CityID")
        public String cityID;


        @Override
        public String toString() {
            return cityName;
        }
    }

    public class Cities implements Serializable {
        @SerializedName("AllCities")
        public List<AllCity> allCities;

    }



}
