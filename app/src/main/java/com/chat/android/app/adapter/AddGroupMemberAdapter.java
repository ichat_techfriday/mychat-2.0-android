package com.chat.android.app.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chat.android.R;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.CoreController;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.FriendModel;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Created by CAS60 on 11/30/2016.
 */
public class AddGroupMemberAdapter extends RecyclerView.Adapter<AddGroupMemberAdapter.ViewHolderAddGroupMember> implements Filterable {

    private List<FriendModel> contactList;
    Getcontactname getcontactname;
    private int blockedContactColor, unblockedContactColor, textColor, detailTextColor;
    ContactDB_Sqlite contactDB_sqlite;
    private List<FriendModel> mDisplayedValues;
    private Context context;

    public AddGroupMemberAdapter(Context mContext, List<FriendModel> contactList) {
        this.contactList = contactList;
        getcontactname = new Getcontactname(mContext);
        mDisplayedValues=contactList;
        context = mContext;

        blockedContactColor = ContextCompat.getColor(mContext, R.color.blocked_user_bg);
        unblockedContactColor = ContextCompat.getColor(mContext, android.R.color.transparent);
        textColor = ContextCompat.getColor(mContext, R.color.white);
        detailTextColor = ContextCompat.getColor(mContext, R.color.white_t80);
        contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
    }

    @Override
    public ViewHolderAddGroupMember onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_group_info, parent, false);
        ViewHolderAddGroupMember holder = new ViewHolderAddGroupMember(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolderAddGroupMember holder, int position) {

        FriendModel contact = mDisplayedValues.get(position);

        holder.tvName.setText(contact.getFirstName());
        holder.tvName.setTextColor(textColor);
        holder.tvStatus.setTextColor(detailTextColor);
//        holder.tvStatus.setText(contact.getStatus());
        //getcontactname.setProfileStatusText(holder.tvStatus, contact.get_id(), contact.getStatus(), false);

        byte[] bytes = tryDecodeBase64(contact.getStatus());
        if (bytes!=null){
            String str = new String(bytes, StandardCharsets.UTF_8);
            holder.tvStatus.setText(str);
        }else{
            holder.tvStatus.setText(contact.getStatus());
        }

        String toID = mDisplayedValues.get(position).get_id();
        String imageUrl = mDisplayedValues.get(position).getAvatarImageUrl();
        if(imageUrl != null && imageUrl.isEmpty()) {
            Glide.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.ic_placeholder_black)
                    .error(R.drawable.ic_placeholder_black)
                    .into(holder.ivUserDp);
        } else {
            holder.ivUserDp.setImageResource(R.drawable.ic_placeholder_black);
        }


//        getcontactname.configProfilepic(holder.ivUserDp, toID, false, false, R.drawable.ic_placeholder_black);

        if (contactDB_sqlite.getBlockedStatus(toID, false).equals("1")) {
            holder.itemView.setBackgroundColor(blockedContactColor);
        } else {
            holder.itemView.setBackgroundColor(unblockedContactColor);
        }
    }
    public byte[] tryDecodeBase64(String path) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                return Base64.getDecoder().decode(path);
            }
            return null;
        } catch(IllegalArgumentException e) {
            return null;
        }
    }
    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }
    public FriendModel getItem(int position) {
        return mDisplayedValues.get(position);
    }


    public class ViewHolderAddGroupMember extends RecyclerView.ViewHolder {

        CircleImageView ivUserDp;
        TextView tvName, tvAdmin, tvStatus;

        public ViewHolderAddGroupMember(View itemView) {
            super(itemView);

            ivUserDp =  itemView.findViewById(R.id.ivUserDp);
            tvName = itemView.findViewById(R.id.tvName);
            tvAdmin = itemView.findViewById(R.id.tvAdmin);
            tvStatus = itemView.findViewById(R.id.tvStatus);
        }
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<FriendModel>) results.values; // has the filtered values
                if (mDisplayedValues.size() == 0) {
//                    Toast.makeText(context, "No Contacts Matching Your Query...", Toast.LENGTH_SHORT).show();
                }

                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<FriendModel> FilteredArrList = new ArrayList<>();

                if (contactList == null) {
                    contactList = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mDisplayedValues.size();
                    results.values = mDisplayedValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < contactList.size(); i++) {

                        String name = contactList.get(i).getFirstName();
                        String msisdn = contactList.get(i).getMsisdn();
                        if (name.toLowerCase().contains(constraint) || msisdn.contains(constraint)) {
                            FilteredArrList.add(contactList.get(i));
                        }


                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }


    public void updateInfo(List<FriendModel> aitem) {
        this.contactList = aitem;
        notifyDataSetChanged();
    }

}
