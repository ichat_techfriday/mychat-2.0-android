package com.chat.android.app.model;

import android.icu.text.StringSearch;

import com.google.gson.annotations.SerializedName;

public class RegisterMobileUser {
    @SerializedName("MobileNumber")
    String mobileNumber;
    @SerializedName("IMEI")
    String imei;

    public RegisterMobileUser(String mobileNumber, String imei) {
        this.mobileNumber = mobileNumber;
        this.imei = imei;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public RegisterMobileUser() {
    }

}
