package com.chat.android.app.model.meetup;

import com.google.gson.annotations.SerializedName;

public class Dist {
    @SerializedName("calculated")
    Double calculated;
    @SerializedName("location")
    LocationNearByMeetups locationDist;

    public Dist() {
    }

    public Dist(Double calculated, LocationNearByMeetups locationDist) {
        this.calculated = calculated;
        this.locationDist = locationDist;
    }

    public Double getCalculated() {
        return calculated;
    }

    public void setCalculated(Double calculated) {
        this.calculated = calculated;
    }

    public LocationNearByMeetups getLocationDist() {
        return locationDist;
    }

    public void setLocationDist(LocationNearByMeetups locationDist) {
        this.locationDist = locationDist;
    }


}
