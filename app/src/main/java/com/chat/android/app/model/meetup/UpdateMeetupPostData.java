package com.chat.android.app.model.meetup;

import com.google.gson.annotations.SerializedName;

public class UpdateMeetupPostData {

    @SerializedName("user_id")
    String userId;
    @SerializedName("name")
    String fullName;
    @SerializedName("lat")
    double latitude;
    @SerializedName("lng")
    double longitude;
    @SerializedName("status")
    String userActivationStatus;
    @SerializedName("bio")
    String bio;
    @SerializedName("dob")
    String dob;

    public UpdateMeetupPostData() {
    }

    public UpdateMeetupPostData(String userId, String fullName, double latitude, double longitude, String userActivationStatus, String bio, String dob) {
        this.userId = userId;
        this.fullName = fullName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.userActivationStatus = userActivationStatus;
        this.bio = bio;
        this.dob = dob;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUserActivationStatus() {
        return userActivationStatus;
    }

    public void setUserActivationStatus(String userActivationStatus) {
        this.userActivationStatus = userActivationStatus;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
