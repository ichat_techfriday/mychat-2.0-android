package com.chat.android.app.utils;

public final class MobileNumberUtils {
    private static MobileNumberUtils instance;

    private MobileNumberUtils(){}

    public static MobileNumberUtils getInstance() {
        if (instance == null) {
            instance = new MobileNumberUtils();
        }

        return instance;
    }

    public String getCountry(String[] argStringArray, String argText){
        String country="";

        if (argText.length() >= 4){
            for(int i=0;i<argStringArray.length;i++){
                String[] g=argStringArray[i].split(",");
                if(g[0].equals(getFirstFourChar(argText))){
                    country=g[1];
                    break;
                }
                if (g[0].equals(getFirstThreeChar(argText))){
                    country=g[1];
                    break;
                }
                if (g[0].equals(getFirstTwoChar(argText))){
                    country=g[1];
                    break;
                }
            }
        }

        return country;
    }

    public String getFirstFourChar(String argText){
        String threeChar;
        String text = argText;
        threeChar = text.substring(0,4);

        return threeChar;
    }

    public String getFirstThreeChar(String argText){
        String twoChar;
        String text = argText;
        twoChar = text.substring(0,3);

        return twoChar;
    }

    public String getFirstTwoChar(String argText){
        String oneChar;
        String text = argText;
        oneChar = text.substring(0,2);

        return oneChar;
    }
}