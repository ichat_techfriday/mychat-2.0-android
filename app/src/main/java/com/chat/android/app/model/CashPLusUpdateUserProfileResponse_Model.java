package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class CashPLusUpdateUserProfileResponse_Model {
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public CashPLusUpdateUserProfileResponse_Model(String errorCode, String errorDescription) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }
}
