package com.chat.android.app.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.adapter.RItemAdapter;
import com.chat.android.app.adapter.ScimboCASocket;
import com.chat.android.app.dialog.ChatLockPwdDialog;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.app.widget.AvnNextLTProDemiTextView;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.ChatLockPojo;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 */
public class SettingContact extends CoreActivity {
    boolean refreshcontactsset = false;
    RecyclerView lvContacts;
    private EditText mSearchEt;
    ScimboCASocket adapter;
    EditText etSearch;
    private UserInfoSession userInfoSession;

    TextView selectcontact;
    TextView selectcontactmember;
    ImageView serach, overflow, backarrow, backButton;
    InputMethodManager inputMethodManager;
    View newGroup;
    Getcontactname getcontactname;
    RelativeLayout overflowlayout, contact1_RelativeLayout;

    private SessionManager sessionManager;
    AvnNextLTProRegTextView contact_empty;
    String username, profileimage;
    private SearchView searchView;
    String receiverDocumentID, uniqueCurrentID;
    private static final String TAG = SettingContact.class.getSimpleName() + ">>";
    List<FriendModel> scimboEntries = new ArrayList<>();

    private Menu contactMenu;
    private ProgressBar pbLoader;


    private LinearLayoutManager mContactListManager;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.d(TAG, "onCreate: ");
        setContentView(R.layout.contactsetting);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.nowletschat.android.contact_refresh");
        registerReceiver(contactsRefreshReceiver, intentFilter);

        userInfoSession = new UserInfoSession(getApplicationContext());

        swipeRefreshLayout=findViewById(R.id.swipe_refresh_layout);
        lvContacts = findViewById(R.id.listContacts);
        backButton = findViewById(R.id.backarrow_contactsetting);
        backarrow = findViewById(R.id.backarrow);
        contact_empty = findViewById(R.id.contact_empty);
        overflow = findViewById(R.id.overflow);
        overflowlayout = findViewById(R.id.overflowLayout);
        contact1_RelativeLayout = findViewById(R.id.r1contact);
        newGroup = findViewById(R.id.newGroup);
        //new_group_layout = (RelativeLayout) findViewById(R.id.new_group_layout);
        serach = findViewById(R.id.search);
        etSearch = findViewById(R.id.etSearch);
        selectcontact = findViewById(R.id.selectcontact);
        selectcontactmember = findViewById(R.id.selectcontactmember);

        getcontactname = new Getcontactname(SettingContact.this);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                refreshContacts();
            }
        });

        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP && etSearch.getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                    if (event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etSearch.setText("");
                        return false;
                    }
                }
                return false;
            }
        });

        mContactListManager = new LinearLayoutManager(SettingContact.this, LinearLayoutManager.VERTICAL, false);
        lvContacts.setLayoutManager(mContactListManager);
        lvContacts.setNestedScrollingEnabled(false);
//        lvContacts.addOnScrollListener(contactScrollListener);

        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        sessionManager = SessionManager.getInstance(this);

//        pbLoader = findViewById(R.id.pbLoader);
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//            Drawable drawableProgress = DrawableCompat.wrap(pbLoader.getIndeterminateDrawable());
//            DrawableCompat.setTint(drawableProgress, ContextCompat.getColor(this, android.R.color.holo_green_light));
//            pbLoader.setIndeterminateDrawable(DrawableCompat.unwrap(drawableProgress));
//        } else {
//            pbLoader.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_IN);
//        }

        loadContactsFromDB();


        if (!SessionManager.getInstance(this).isContactSyncFinished()) {
            refreshContacts();
        }

        lvContacts.addOnItemTouchListener(new RItemAdapter(this, lvContacts, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FriendModel e = adapter.getItem(position);

                if (e.getStatus() != null && e.getStatus().length() > 0) {

                    ChatLockPojo lockPojo = getChatLockdetailfromDB(position);
                    if (sessionManager != null && sessionManager.getLockChatEnabled().equals("1") && lockPojo != null) {

                        String stat = "", pwd = null;
                        stat = lockPojo.getStatus();
                        pwd = lockPojo.getPassword();

                        String docID = e.get_id();
                        String documentid = uniqueCurrentID.concat("-").concat(docID);
                        if (stat.equals("1")) {
                            openUnlockChatDialog(documentid, stat, pwd, position);
                        } else {
                            //navigateToChatviewPage(e);
                            getcontactname.navigateToChatviewPageforScimboModel(e);
                            finish();
                        }
                    } else {
                        getcontactname.navigateToChatviewPageforScimboModel(e);
                        finish();
                    }
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        newGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplication(), SelectPeopleForGroupChat.class);
                startActivity(i);
            }
        });

        serach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSearchActions();

                etSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                        if (adapter != null) {
                            if (cs.length() > 0) {
                                if (cs.length() == 1) {
                                    etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.cancel_normal, 0);
                                }
                                SettingContact.this.adapter.getFilter().filter(cs);
                            } else {
                                etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                adapter.updateInfo(scimboEntries);
                            }
                        }

                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                        // TODO Auto-generated method stub
                    }
                });
                backarrow.setVisibility(View.VISIBLE);
                backButton.setVisibility(View.GONE);

                backarrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        etSearch.getText().clear();
                        /*if (adapter != null) {
                            adapter.updateInfo(scimboEntries);
                        }*/
                        etSearch.setVisibility(View.GONE);
                        serach.setVisibility(View.VISIBLE);
                        //overflowlayout.setVisibility(View.VISIBLE);
                        selectcontactmember.setVisibility(View.VISIBLE);
                        selectcontact.setVisibility(View.VISIBLE);
                        backarrow.setVisibility(View.GONE);
                        backButton.setVisibility(View.VISIBLE);
                        hideKeyboard();
                    }
                });

                showKeyboard();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /* Variables for serch */

    }

    private long lastUpdatedMillis=0;

    private <E> boolean isDuplicateList(final List<E> l1, final List<E> l2) {
        final Set<E> set = new HashSet<>(l1);
        final Set<E> set2 = new HashSet<>(l2);
        return l1.size() == l2.size() && set.containsAll(set2);
    }
    private void loadContactsFromDB() {
        long currentMillis=System.currentTimeMillis();
        long diffMillis=currentMillis-lastUpdatedMillis;

        if(isRefreshRequested || (diffMillis>20000)) {
            isRefreshRequested=false;
            lastUpdatedMillis=System.currentTimeMillis();
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
            List<FriendModel> updatedContactList = contactDB_sqlite.getSavedFriendModels();
            Collections.sort(updatedContactList, Getcontactname.nameAscComparator);
            Log.d(TAG, "loadContactsFromDB: start");
            if(updatedContactList!=null && isDuplicateList(updatedContactList,scimboEntries)){
                Log.d(TAG, "loadContactsFromDB: no need refresh");
                return;
            }else {
                Log.d(TAG, "loadContactsFromDB: need refresh");
                scimboEntries=updatedContactList;
                filterFriendList();
            }

            if (scimboEntries != null && scimboEntries.size() > 0) {
                MyLog.d(TAG, "ContactsTest loadContactsFromDB: size " + scimboEntries.size());
                Collections.sort(scimboEntries, Getcontactname.nameAscComparator);
                adapter = new ScimboCASocket(SettingContact.this, scimboEntries);

                lvContacts.setVisibility(View.VISIBLE);
                contact_empty.setVisibility(View.GONE);
                lvContacts.getRecycledViewPool().setMaxRecycledViews(0, 100);
                adapter.setHasStableIds(true);
                lvContacts.setAdapter(adapter);
                selectcontactmember.setText(scimboEntries.size() + " " + getString(R.string.Contacts));
//            notifyScrollChanged();
                if (refreshcontactsset) {
                    Toast.makeText(SettingContact.this, getString(R.string.your_contact_list_has_been_updated), Toast.LENGTH_SHORT).show();
                    refreshcontactsset = false;
                }

            } else {
                MyLog.d(TAG, "loadContactsFromDB: no contacts in DB");
                contact_empty.setVisibility(View.VISIBLE);
                lvContacts.setVisibility(View.GONE);

                // TODO: Translate the below str
                contact_empty.setText(getResources().getString(R.string.no_contacts_available_for_chat));
                selectcontactmember.setText("0 " + getResources().getString(R.string.Contacts));
            }
        }
    }

    private void filterFriendList() {
        if (scimboEntries != null && scimboEntries.size() > 0) {
            Iterator<FriendModel> friendsIterator = scimboEntries.iterator();
            while (friendsIterator.hasNext()) {
                FriendModel model = friendsIterator.next();
                if (!model.getRequestStatus().equals(AppConstants.FriendStatus.FRIENDS.getValue()))
                    friendsIterator.remove();
            }
        }
    }


    private void updateProfileImage(ReceviceMessageEvent event) {
        Object[] array = event.getObjectsArray();
        try {
            JSONObject objects = new JSONObject(array[0].toString());
            String from = objects.getString("from");
            String type = objects.getString("type");
            //String docId = uniqueCurrentID + "-" + from;
            if (scimboEntries == null)
                return;
            for (FriendModel friendModel : scimboEntries) {
                if (type.equalsIgnoreCase("single") && friendModel.get_id().equalsIgnoreCase(from)) {
                    adapter.notifyDataSetChanged();
                }
            }


        } catch (Exception e) {
            MyLog.e(TAG, "updateProfileImage: ", e);
        }
    }


    private void showSearchActions() {

        backarrow.setVisibility(View.VISIBLE);
        serach.setVisibility(View.GONE);
        backButton.setVisibility(View.GONE);
        selectcontact.setVisibility(View.GONE);
        selectcontactmember.setVisibility(View.GONE);
        //contact1_RelativeLayout.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        contactMenu = menu;
        getMenuInflater().inflate(R.menu.new_chat, menu);
        return super.onCreateOptionsMenu(contactMenu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        hideKeyboard();
        switch (item.getItemId()) {

            case R.id.menuinvitefriends:
                AppUtils.shareApp(SettingContact.this);
                break;

            case R.id.menucontacts:
                /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivity(intent);*/
                String data = "content://contacts/people/";
                Intent contactIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
                startActivity(contactIntent);
                break;

            case R.id.menurefresh:
                if (isNetworkConnected()) {
                    refreshContacts();
                    refreshcontactsset = true;
                } else {
                    Toast.makeText(SettingContact.this, getResources().getString(R.string.please_check_your_connection), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.menuaboutHelp:
                ActivityLauncher.launchAbouthelp(SettingContact.this);
                break;

            case R.id.chats_contactIcon:
                try {
                    Intent newIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    // Sets the MIME type to match the Contacts Provider
                    newIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    startActivity(newIntent);
                }
                catch (Exception e){
                    MyLog.e(TAG, "onOptionsItemSelected: ",e );
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private boolean isRefreshRequested=false;
    private void refreshContacts() {
        isRefreshRequested=true;
        MyLog.d(TAG, "ContactsTest refreshContacts CLICK: ");
  //      pbLoader.setVisibility(View.VISIBLE);

        ScimboContactsService.bindContactService(this, true);
    }


    private void openUnlockChatDialog(String docId, String status, String pwd, int position) {
        String convId = userInfoSession.getChatConvId(docId);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1(getResources().getString(R.string.enter_your_pwd_label));
        dialog.setEditTextdata(getResources().getString(R.string.hint_newPwd));
        dialog.setforgotpwdlabel(getResources().getString(R.string.forgotChatpwd));
        dialog.setHeader(getResources().getString(R.string.unlock_chat));
        dialog.setButtonText(getResources().getString(R.string.unlock));
        Bundle bundle = new Bundle();
        bundle.putSerializable("socketitems", adapter.getItem(position));
        bundle.putString("convID", convId);
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatlist");
        bundle.putString("type", "single");
        bundle.putString("from", uniqueCurrentID);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatunLock");
    }

    private ChatLockPojo getChatLockdetailfromDB(int position) {

        FriendModel e = adapter.getItem(position);
        String docID = e.get_id();
        String id = uniqueCurrentID.concat("-").concat(docID);
        MessageDbController dbController = CoreController.getDBInstance(this);
        String convId = userInfoSession.getChatConvId(id);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo pojo = dbController.getChatLockData(receiverId, MessageFactory.CHAT_TYPE_SINGLE);
        return pojo;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (SocketManager.EVENT_GET_CONTACTS.equalsIgnoreCase(event.getEventName())) {

        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GET_USER_DETAILS)) {
//            loadUserDetails(event.getObjectsArray()[0].toString());
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            updateProfileImage(event);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        hideKeyboard();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideKeyboard();
        unregisterReceiver(contactsRefreshReceiver);
    }

    ContactsRefreshReceiver contactsRefreshReceiver = new ContactsRefreshReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
         //   pbLoader.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            loadContactsFromDB();
        }
    };

    @Override
    public void onBackPressed() {
        if (etSearch.getVisibility() == View.GONE) {
            hideKeyboard();
            super.onBackPressed();
        } else {
            backarrow.performClick();
        }
    }




}
