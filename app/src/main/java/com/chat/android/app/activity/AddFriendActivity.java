package com.chat.android.app.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.chat.android.R;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.utils.TextUtils;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddFriendActivity extends CoreActivity implements View.OnClickListener{
    EditText etPinCode;
    ImageView btnAdd;
    RelativeLayout parentView;
    LinearLayout bg_layout;
    FrameLayout fl_register_phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);

        initViews();
    }

    private void initViews() {
        etPinCode = findViewById(R.id.et_pin_code);
        btnAdd = findViewById(R.id.iv_add);
        parentView = findViewById(R.id.add_parent_view);
        bg_layout = findViewById(R.id.bg_layout);
        fl_register_phone = findViewById(R.id.fl_register_phone);
        fl_register_phone.setOnClickListener(this);
        bg_layout.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        parentView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_parent_view:
                Intent responseIntent = new Intent();
                AddFriendActivity.this.setResult(RESULT_OK, responseIntent);
                finish();
                break;
            case R.id.iv_add:
                actionAdd();
                break;
            case R.id.bg_layout:
                finish();
                break;
            case R.id.fl_register_phone:
                break;
        }
    }

    private void actionAdd() {
        String pinCode  = etPinCode.getText().toString();
        if(!TextUtils.isEmpty(pinCode)) {
            addFriend();
            btnAdd.setEnabled(false);
        }
        else
            CoreActivity.showToast(this, getString(R.string.empty_pincode_msg));

    }

    private void addFriend() {
        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_SEND_FRIEND_REQUEST);
        JSONObject obj = new JSONObject();
        try {
            obj.put("userId", SessionManager.getInstance(this).getCurrentUserID());
            obj.put("pinCode", etPinCode.getText().toString());
            obj.put("companyId", "");
        } catch (JSONException e) {
            MyLog.e("AddFriend","",e);
        }
        MyLog.e("AddFriend","AddFriend "+obj);
        messageEvent.setMessageObject(obj);
        EventBus.getDefault().post(messageEvent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_SEND_FRIEND_REQUEST)) {
            try {
                Object[] response = event.getObjectsArray();
                JSONObject jsonObject = (JSONObject) response[0];
                MyLog.d("AddFriend", "friendRequest response " + jsonObject.toString());
                int error = jsonObject.getInt("err");
                String message = jsonObject.getString("message");
                if (error == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent resultIntent = new Intent();
                            setResult(RESULT_OK, resultIntent);
                            finish();
                        }
                    });
                } else {
                    CoreActivity.showToast(AddFriendActivity.this, message);
                    btnAdd.setEnabled(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        etPinCode = null;
        btnAdd = null;
        parentView = null;
    }
}
