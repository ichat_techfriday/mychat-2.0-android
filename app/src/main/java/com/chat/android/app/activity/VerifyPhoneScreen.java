package com.chat.android.app.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chat.android.BuildConfig;
import com.chat.android.MyFirebaseInstanceIDService;
import com.chat.android.R;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.dialog.InstantTranslationPopup;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.SharedPreference;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.ShortcutBadgeManager;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.scimbohelperclass.ScimboDialogUtils;
import com.chat.android.core.scimbohelperclass.ScimboPermissionValidator;
import com.chat.android.core.service.Constants;
import com.chat.android.core.service.ServiceRequest;
import com.chat.android.core.socket.SocketManager;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.socket.client.Socket;

import com.chat.android.interfaces.PermissionGrantListener;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.TextUtils;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.hbb20.CountryCodePicker;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by  CASPERON TECH on 10/5/2016.
 */
public class VerifyPhoneScreen extends OnBoardingHomeParentActivity implements View.OnClickListener {
    private Context mContext;
    private TextView supMessenger, conformCountyCode;
    TextView choseCountry, tvTermsAndConditions;
    private ImageView ivTermsAndConditions;
    private ImageView okButton;
    public EditText phoneNumber;
    private EditText edReferalCode, edEmail;
    private Button btn_invitation;
    private final int selectedCountry = 11;
    private boolean termsAndConditionsAccepted = true;
    public static String phoneNumberCh;
    boolean validNumber=false;

    Boolean isagree = false;

    private List<String> codeList;
    private List<String> countryList;
    CheckBox tvTermsAndConditions_checkbox;

    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 123;
    private ArrayList<ScimboPermissionValidator.Constants> myPermissionConstantsArrayList;
    private boolean isDeninedRTPs = false;
    private boolean showRationaleRTPs = false;
    private SocketManager mSocketManager;
    private boolean country_wise_filter = false;
    private boolean isKeyBoardVisible = false;
    static String[] country;
    static String[] codes;

    private String GCM_Id = "";

    private ArrayList<String> list;
    private ArrayList<String> codelist;
    private static final String LETTER_SPACING = " ";
    private ImageView scroll_country;
    private LinearLayout code_fetch_layout;
    private static final String TAG = VerifyPhoneScreen.class.getSimpleName();
    private boolean isMassChat, isTwitgo, isnobelnet;
    private ProgressBar pbCountryCode;
    private ImageView ivCountryFlag, bgImage, ivBack;
    private RelativeLayout rlGoBackContainer;
    private Country selectedCountryModel;
    private TextView tvEncryptionText;

    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private SettingsClient mSettingsClient;
    private LocationCallback mLocationCallback;
    private LocationSettingsRequest mLocationSettingsRequest;
    private Location mLocation;
    private CountryCodePicker ccpicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
        isTwitgo = getResources().getBoolean(R.bool.is_twitgo);
        isnobelnet = getResources().getBoolean(R.bool.is_nobelnet);

        if (isMassChat)
            setContentView(R.layout.activity_verify_phone_screen_mass_chat);
        else if (isTwitgo)
            setContentView(R.layout.activity_verify_phone_screen_twitgo);
        else
            setContentView(R.layout.activity_verify_phone_screen);

        mContext = VerifyPhoneScreen.this;

        initView();
        initData();
        setCountryCode();
        fetchLocation();
        phoneNumber.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == EditorInfo.IME_ACTION_DONE) {
                    findViewById(R.id.okButton).performClick();
                    return true;
                }
                return false;
            }
        });
        phoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    findViewById(R.id.okButton).performClick();
                    return true;
                }
                return false;
            }
        });
        phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(charSequence)) {
                    phoneNumber.setHint("");
                } else {
                    phoneNumber.setHint(R.string.enter_your_mobile_no);
                }
                phoneNumberTxt = charSequence.toString();
                checkAndEnableGoButton(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        code_fetch_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCountryPickerDialog();
                //ActivityLauncher.launchChooseCountryScreen(VerifyPhoneScreen.this, selectedCountry);
            }
        });

    }

    private void checkAndEnableGoButton(String number) {
            phoneNumberCh = number;
            phoneNumberCh = "+" + VerifyPhoneScreen.this.code + phoneNumberCh;
            if (!TextUtils.isEmpty(number) && MyChatUtils.isPhoneNumberValid(VerifyPhoneScreen.this, String.valueOf(ccpicker.getSelectedCountryNameCode()), number)
                    && tvTermsAndConditions_checkbox.isChecked()) {
                okButton.setImageResource(R.drawable.go_onboarding);
                okButton.setEnabled(true);
            } else {
                okButton.setEnabled(false);
                okButton.setImageResource(R.drawable.go_disabled);
            }
    }

    private void initData() {
        Resources res = getResources();
//        String[] code = res.getStringArray(R.array.country_code);
//        String[] country = res.getStringArray(R.array.country_list);
//        codeList = Arrays.asList(code);
//        countryList = Arrays.asList(country);
        //    Log.e("" + code.length, "" + country.length);

        ShortcutBadgeManager manager = new ShortcutBadgeManager(this);
        manager.clearBadgeCount();
        manager.setContactLastRefreshTime(0L);
        SessionManager.getInstance(VerifyPhoneScreen.this).setnameOfCurrentUser("");
        if (getResources().getString(R.string.app_name).equalsIgnoreCase("myChat")) {
            SessionManager.getInstance(VerifyPhoneScreen.this).setEmail("");

        }

        checkAndRequestPermissions();
        initSocketCallback();
        mSocketManager.connect();
        getSettings();
        getSettingsAPI();

    }

    private void initView() {
        ccpicker = findViewById(R.id.ccpicker);
        tvEncryptionText = findViewById(R.id.tv_mychat_encryption);
        rlGoBackContainer = findViewById(R.id.rl_go_view);
        choseCountry = findViewById(R.id.selectCountry);
        bgImage = findViewById(R.id.bgimage);
        pbCountryCode = findViewById(R.id.pb_verify_phone);
        ivCountryFlag = findViewById(R.id.iv_register_country_code);
        ivBack = findViewById(R.id.iv_back);
        okButton = findViewById(R.id.okButton);
        sessionManager = SessionManager.getInstance(VerifyPhoneScreen.this);
        phoneNumber = findViewById(R.id.phoneNumber);

        ccpicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                checkAndEnableGoButton(phoneNumber.getText().toString());
            }
        });
        if (isMassChat)
            btn_invitation = findViewById(R.id.btn_invitation);
        edReferalCode = findViewById(R.id.ed_invitation_code);
        if (isTwitgo)
            edEmail = findViewById(R.id.etEmailId);
        tvTermsAndConditions = findViewById(R.id.tvTermsAndConditions);

        tvTermsAndConditions_checkbox = findViewById(R.id.tvTermsAndConditions_checkbox);
        scroll_country = findViewById(R.id.scroll_country);
        code_fetch_layout = findViewById(R.id.code_fetch_layout);
        okButton.setEnabled(false);
        okButton.setImageResource(R.drawable.go_disabled);
        setTermsAndConditionsText();

        Context c = getApplicationContext();
        Resources res = c.getResources();

        country = res.getStringArray(R.array.country_list);
        codes = res.getStringArray(R.array.country_code);

        list = new ArrayList<>();
        codelist = new ArrayList<>();
        list.clear();
        codelist.clear();
        for (int i = 0; i < country.length; i++) {
            list.add(country[i]);
            codelist.add(codes[i]);
        }

        tvTermsAndConditions_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    isagree = true;
                    tvTermsAndConditions_checkbox.setChecked(true);
                    checkAndEnableGoButton(phoneNumber.getText().toString());
                } else {
                    isagree = false;
                    tvTermsAndConditions_checkbox.setChecked(false);
                    okButton.setImageResource(R.drawable.go_disabled);
                    okButton.setEnabled(false);
                }
                setOkButtonOnHideKeyboard();
            }
        });
        choseCountry.setOnClickListener(this);
        okButton.setOnClickListener(this);

        KeyboardVisibilityEvent.setEventListener(
                (Activity) mContext,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if (isOpen) {
                            bgImage.setVisibility(View.GONE);
                            rlGoBackContainer.setVisibility(View.VISIBLE);
                            tvEncryptionText.setVisibility(View.GONE);
                            isKeyBoardVisible = true;
                        } else {
                            bgImage.setVisibility(View.VISIBLE);
                            isKeyBoardVisible = false;
                            setOkButtonOnHideKeyboard();
                        }
                    }
                });

    }

    private void setOkButtonOnHideKeyboard() {
        if (okButton.isEnabled()) {
            rlGoBackContainer.setVisibility(View.VISIBLE);
            tvEncryptionText.setVisibility(View.GONE);
        } else {

            if (!isKeyBoardVisible) {
                rlGoBackContainer.setVisibility(View.GONE);
                tvEncryptionText.setVisibility(View.VISIBLE);
            }
        }
    }

    private void startInstantTranslationPopup() {
        if (!SharedPreference.getInstance().getBool(this, AppConstants.INSTANT_TRANSLATION_POPUP_VERIFY_PHONE_SCREEN)) {
            SharedPreference.getInstance().saveBool(this, AppConstants.INSTANT_TRANSLATION_POPUP_VERIFY_PHONE_SCREEN, true);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    InstantTranslationPopup dialog = new InstantTranslationPopup();
                    dialog.show(getSupportFragmentManager(), "itp");
                }
            }, 500);
        }

    }

    private void setTermsAndConditionsText() {
        String termsString = getString(R.string.terms_conditions_msg);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(termsString);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Intent httpIntent = new Intent(Intent.ACTION_VIEW);
                httpIntent.setData(Uri.parse(BuildConfig.BASE_URL+"/eula"));
                startActivity(httpIntent);
            }
        };
        spannableStringBuilder.setSpan(clickableSpan, termsString.indexOf(getString(R.string.terms_)), termsString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTermsAndConditions.setText(spannableStringBuilder);
        tvTermsAndConditions.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void verifyCodeListenerCompleted() {
        ActivityLauncher.launchProfileInfoScreen(VerifyPhoneScreen.this, SessionManager.getInstance(VerifyPhoneScreen.this).getPhoneNumberOfCurrentUser());
    }

    @Override
    protected void verifyPhoneCompleted(String message) {
        hidepDialog();
        okButton.setEnabled(true);
        if (SKIP_OTP_VERIFICATION) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    skipOTP(otp);
                }
            }, 500);
        } else {
            hideProgressDialog();
            ActivityLauncher.launchSMSVerificationScreen(VerifyPhoneScreen.this, message, "" + code, phoneNumber.getText().toString(), otp, GCM_Id,false);
        }
    }

    @Override
    protected void verifyPhoneError(String message) {
        hidepDialog();
        hideProgressDialog();
        okButton.setEnabled(true);
        showToast(this, message);
    }

    private void fetchLocation() {
        choseCountry.setVisibility(View.VISIBLE);
        ivCountryFlag.setVisibility(View.GONE);
        pbCountryCode.setVisibility(View.GONE);

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);
        mSettingsClient = LocationServices.getSettingsClient(mContext);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                MyLog.d("ENTERED LocationCallback::onLocationResult");
                super.onLocationResult(locationResult);
                mLocation = locationResult.getLastLocation();
                if (mLocation == null)
                    return;
                String countryName = getCountryNameFromLocation(mLocation);
                selectedCountryModel = Country.getCountryByName(countryName);
                try {
                    ccpicker.setCountryForNameCode(selectedCountryModel.getCode());
                    populateUIFromCountry();

                } catch (Exception e) {
                    Log.d(TAG, "onLocationResult: " + e.toString());
                }
            }
        };

        // set up the request. note that you can use setNumUpdates(1) and
        // setInterval(0) to get one request.
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
        requestLocationPermission();
    }

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener((Activity) mContext, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mFusedLocationProviderClient.requestLocationUpdates(
                                mLocationRequest,
                                mLocationCallback,
                                Looper.getMainLooper()
                        );
                        choseCountry.setVisibility(View.GONE);
                        ivCountryFlag.setVisibility(View.GONE);
                        pbCountryCode.setVisibility(View.GONE);
                        if (mLocation != null) {
                            String countryName = getCountryNameFromLocation(mLocation);
                            selectedCountryModel = Country.getCountryByName(countryName);
                            populateUIFromCountry();
                        }
                    }
                })
                .addOnFailureListener((Activity) mContext, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    choseCountry.setVisibility(View.GONE);
                                    ivCountryFlag.setVisibility(View.GONE);
                                    pbCountryCode.setVisibility(View.GONE);
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    int requestCheckSettings = 100;  //?
                                    rae.startResolutionForResult((Activity) mContext, requestCheckSettings);
                                } catch (IntentSender.SendIntentException sie) {
                                    sie.printStackTrace();
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings.";
                                MyLog.d(errorMessage);
                        }
                    }
                });
    }

    private void requestLocationPermission() {
        CoreActivity baseActivity = (CoreActivity) mContext;
        baseActivity.requestForPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, new PermissionGrantListener() {
            @Override
            public void onPermissionGranted(boolean granted) {
                //  startInstantTranslationPopup();
                if (granted) {
                    startLocationUpdates();
                } else {
                    populateUIFromCountry();
                }
            }
        });
    }

    private String getCountryNameFromLocation(Location location) {
        String countryName = "";
        try {
            Geocoder myLocation = new Geocoder(mContext);
            List<Address> myList = myLocation.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            countryName = myList.get(0).getCountryName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countryName;
    }

    @Override
    public void onStop() {
        super.onStop();
        //mSocketManager.clearCallBack();
    }

    private void checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            myPermissionConstantsArrayList = new ArrayList<>();




            /*if (ScimboPermissionValidator.checkPermission(VerifyPhoneScreen.this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE)) {
                onPermissionGranted();
            }*/
        } else {
            onPermissionGranted();
        }
    }

    private void onPermissionGranted() {


    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            isDeninedRTPs = true;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                showRationaleRTPs = shouldShowRequestPermissionRationale(permission);
                            }
                        }
                        break;
                    }
                    onPermissionResult();
                } else {

                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
     /*   if(!AppUtils.isServiceRunning(this,MessageService.class)){
            AppUtils.startService(this, MessageService.class);
        }*/
    }

    private void onPermissionResult() {
        if (isDeninedRTPs) {
            if (!showRationaleRTPs) {
                //goToSettings();
                ScimboDialogUtils.showPermissionDeniedDialog(VerifyPhoneScreen.this);
            } else {
                isDeninedRTPs = false;
                ScimboPermissionValidator.checkPermission(this,
                        myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE);
            }
        } else {
            onPermissionGranted();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.selectCountry || view.getId() == R.id.countryDropDownMain) {
            // showCountryPickerDialog();
        }
        if (view.getId() == R.id.okButton) {
            if (AppUtils.isNetworkAvailable(mContext)) {
                String code = ccpicker.getSelectedCountryCode();
                if (!code.equalsIgnoreCase("Code") && !code.equalsIgnoreCase("")) {
                    if (isagree) {
                        makeVerificationRequest();
                    } else {
                        Toast.makeText(VerifyPhoneScreen.this, R.string.acceptterms, Toast.LENGTH_SHORT).show();
                        okButton.setEnabled(true);
                        return;
                    }
                } else {
                    Toast.makeText(VerifyPhoneScreen.this, R.string.selectcontrycode, Toast.LENGTH_SHORT).show();
                    okButton.setEnabled(true);
                    return;
                }
                getSettings();
                getSettingsAPI();
                if (mSocketManager.isConnected()) {
                    hideKeyboard();
                    okButton.setEnabled(false);
                } else {

                    boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
                    if (isMassChat) {
                        if (edReferalCode.getText().toString().length() == 0) {
                            showToast(VerifyPhoneScreen.this, R.string.invitationcode);

                        } else {
                            mSocketManager.connect();
                        }
                    } else {
                        mSocketManager.connect();
                    }
                }
            } else {
                showToast(mContext, getString(R.string.networkerror));
            }
        }
    }

    private void showCountryPickerDialog() {
        final CountryPicker picker = CountryPicker.newInstance(getString(R.string.select_country));
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                VerifyPhoneScreen.this.code = dialCode.replace("+", "");
                pbCountryCode.setVisibility(View.GONE);
                ivCountryFlag.setVisibility(View.GONE);
                choseCountry.setVisibility(View.GONE);
                choseCountry.setText(dialCode);
                ivCountryFlag.setImageResource(flagDrawableResID);
                ivCountryFlag.setTag(code);
                picker.dismiss();
            }
        });
    }

    private void populateUIFromCountry() {
        pbCountryCode.setVisibility(View.GONE);
        choseCountry.setVisibility(View.VISIBLE);
        if (selectedCountryModel != null) {
            String dialCode = selectedCountryModel.getDialCode();
            int flagDrawableResID = selectedCountryModel.getFlag();
            VerifyPhoneScreen.this.code = dialCode.replace("+", "");
            pbCountryCode.setVisibility(View.GONE);
            ivCountryFlag.setVisibility(View.GONE);
            choseCountry.setVisibility(View.GONE);
            choseCountry.setText(dialCode);
            ivCountryFlag.setImageResource(flagDrawableResID);
            ivCountryFlag.setTag(selectedCountryModel.getCode());
            ivCountryFlag.setVisibility(View.GONE);
            choseCountry.setText(selectedCountryModel.getDialCode());
            if (mFusedLocationProviderClient != null && mLocationCallback != null)
                mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        } else {
            ivCountryFlag.setVisibility(View.GONE);
            choseCountry.setVisibility(View.GONE);
            ivCountryFlag.setImageResource(R.drawable.flag_def);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed here it is 2
        if (resultCode != RESULT_CANCELED && requestCode == selectedCountry) {
            String message = data.getStringExtra("MESSAGE");
            code = data.getStringExtra("CODE");
            if (code.equalsIgnoreCase("263")) {
                country_wise_filter = true;

                choseCountry.setText("  (+" + code + ")");
            } else {
                country_wise_filter = false;
                choseCountry.setText("  (+" + code + ")");
            }
            sessionManager.setCountryCodeOfCurrentUser("+" + code);
        } else if (100 == requestCode) {
            if (Activity.RESULT_OK == resultCode) {
                startLocationUpdates();
            } else
                populateUIFromCountry();
        }
    }


    /* This API checks if the user has entered a number of not.
     * In case the number is not entered, an alert is displayed to the user
     * else send this number to the server and execute the login API
     */
    private void showAlertDialog(String title, String msg) {
        String number = phoneNumber.getText().toString().trim();
        number = number.replace(" ", "");
        if (number.length() < 5) {
            CustomAlertDialog dialog = new CustomAlertDialog();
            dialog.setMessage(getResources().getString(R.string.validno));
            dialog.setPositiveButtonText(getResources().getString(R.string.ok));
            dialog.setNegativeButtonText(getResources().getString(R.string.cancel));
            dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                @Override
                public void onPositiveButtonClick() {

                }

                @Override
                public void onNegativeButtonClick() {

                }
            });
            dialog.show(getSupportFragmentManager(), "CustomAlert");
        } else {
            CustomAlertDialog dialog = new CustomAlertDialog();
            if (!termsAndConditionsAccepted) {
                dialog.setMessage(getResources().getString(R.string.please_accept_terms_and_conditions));
                dialog.setPositiveButtonText(getResources().getString(R.string.ok));
                dialog.setNegativeButtonText(getResources().getString(R.string.cancel));
                dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                    @Override
                    public void onPositiveButtonClick() {

                    }

                    @Override
                    public void onNegativeButtonClick() {

                    }
                });
            } else {

                dialog.setPositiveButtonText(getResources().getString(R.string.zain_dialog_btn_txt));
                dialog.setNegativeButtonText(getResources().getString(R.string.eDIT));

                // String msg2 = "<b>We will be verifying the phone number:\n\n" + "+" + code + " " + phoneNumber.getText().toString() + "\n\nIs this OK, or would you like to edit the number?<b>";
                String msg2 = getResources().getString(R.string.we_will_be_verifying_the_phone_number) + "<br><br>" + "<b>+" + code + " " + phoneNumber.getText().toString() + "</b><br><br>" + getResources().getString(R.string.is_this_ok_or_you_like_to_edit_number);
                dialog.setMessage(msg2);

                dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                    @Override
                    public void onPositiveButtonClick() {

                        makeVerificationRequest();
                    }

                    @Override
                    public void onNegativeButtonClick() {

                    }
                });
            }
            dialog.show(getSupportFragmentManager(), "CustomAlert");
        }
    }

    private void makeVerificationRequest() {


        String number = phoneNumber.getText().toString().trim();
        number = number.replace(" ", "");
        if (isMassChat && edReferalCode.getText().toString().trim().isEmpty()) {
            showToast(VerifyPhoneScreen.this, getResources().getString(R.string.enter_invitation_code));
            okButton.setEnabled(true);
            return;
        } else if (isTwitgo && edEmail.getText().toString().trim().isEmpty()) {
            showToast(VerifyPhoneScreen.this, getResources().getString(R.string.enteremail));
            okButton.setEnabled(true);
            return;
        } else if (number.equals("") || number.length() < 4) {
            showToast(VerifyPhoneScreen.this, R.string.validno);
            okButton.setEnabled(true);
            return;
        }
        if (MyFirebaseInstanceIDService.token != null && MyFirebaseInstanceIDService.token.length() > 0)
            GCM_Id = MyFirebaseInstanceIDService.token;
        else
            GCM_Id = MyFirebaseInstanceIDService.getFCMToken(this);

        if (GCM_Id == null || GCM_Id.isEmpty()) {
            GCM_Id = "121";
        }


        //   showProgressDialog();
        HashMap<String, String> params = new HashMap<String, String>();
        code = ccpicker.getSelectedCountryCode();
        sessionManager.setCountryCodeOfCurrentUser("+" + code);

        String cCode = code;
        String uPhone = phoneNumber.getText().toString();
        //System.out.println("Country_code" + "" + cCode);
        params.put("msisdn", "+" + cCode + uPhone);
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_Id);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("userEmail", SessionManager.getInstance(VerifyPhoneScreen.this).getEmailofCurrentUser());
        params.put("PhNumber", uPhone);
        params.put("emailVerified", "2");
        params.put("verification_type", "phone");
        params.put("CountryCode", "+" + cCode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        params.put("callToken", "Android");
        if (isMassChat) {
            //add invitatation code
            String referralCode = edReferalCode.getText().toString();
            params.put("invite_code", referralCode);
        }

        if (isTwitgo) {
            String email = edEmail.getText().toString();
            params.put("email", email);
        }
        if (CoreController.isRaad) {
            params.put("server_security_key", "tawari123");//raad
            params.put("Appversion", "1");//raad
            params.put("brand", "" + Build.MANUFACTURER);//raad
            params.put("model", "" + Build.MODEL);//raad
            params.put("lat", "" + getLatitude());//raad
            params.put("lng", "" + getLongitude());//raad
        }
        MyLog.d(TAG, "params: " + params);
        ServiceRequest request = new ServiceRequest(this);
        request.makeServiceRequest(Constants.VERIFY_NUMBER_REQUEST, Request.Method.POST, params, verifcationListener);
        showProgres();
    }

    private ServiceRequest.ServiceListener resendInviteCodeListner = new ServiceRequest.ServiceListener() {

        @Override
        public void onCompleteListener(String response) {
            hidepDialog();

            try {
                JSONObject result = new JSONObject(response);
                Log.e(TAG, "result : " + result);
                if (result.has("message"))
                    showToast(VerifyPhoneScreen.this, result.getString("message"));
                else
                    showToast(VerifyPhoneScreen.this, getResources().getString(R.string.invalid_phone_number));
            } catch (Exception e) {
                Log.e(TAG, "onCompleteListener: ", e);
            }
        }

        @Override
        public void onErrorListener(int state) {
            Log.e(TAG, "onErrorListener" + state);
            hidepDialog();
        }
    };


    private void getSettings() {
        SendMessageEvent settingsEvent = new SendMessageEvent();
        settingsEvent.setMessageObject(new JSONObject());
        settingsEvent.setEventName(SocketManager.EVENT_GET_SETTINGS);
        EventBus.getDefault().post(settingsEvent);
    }

    private void getSettingsAPI() {
        // prepare the Request
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, Constants.SETTINGSS, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject object) {
                        // display response
                        Log.e("Response", object.toString());
                        try {
                            String twilioMode = "";
                            if (object.has("twilio"))
                                twilioMode = object.optString("twilio");
                            if (!twilioMode.equals(SessionManager.TWILIO_DEV_MODE)) {
                                //Twilio Sms
                                SKIP_OTP_VERIFICATION = false;

                                findViewById(R.id.send_message).setVisibility(View.VISIBLE);
                                findViewById(R.id.charge).setVisibility(View.VISIBLE);

                            } else if (object.has("jawal")) {
                                //Raad Sms
                                twilioMode = object.optString("jawal");
                                if (!twilioMode.equals(SessionManager.TWILIO_DEV_MODE))
                                    SKIP_OTP_VERIFICATION = false;
                            }


                            Log.e("SKIP_OTP_VERIFICATION", "SKIP_OTP_VERIFICATION" + SKIP_OTP_VERIFICATION);

                          /*  if (isnobelnet) {
                                if (object.has("twilio"))
                                    twilioMode = object.optString("twilio");
                                if (twilioMode.equals(SessionManager.TWILIO_PROD_MODE)) {
                                    SKIP_OTP_VERIFICATION = false;

                                }
                                Log.e("SKIP_OTP_VERIFICATION", "SKIP_OTP_VERIFICATION" + SKIP_OTP_VERIFICATION);
                            }*/
                            SessionManager sessionManager = SessionManager.getInstance(VerifyPhoneScreen.this);
                            sessionManager.setTwilioMode(twilioMode);
                            // mSocketManager.disconnect();
                            //Check Encryption is available or not
                            if (object.has("is_encryption_available")) {
                                String is_encryption_available = object.getString("is_encryption_available");
                                if (Integer.parseInt(is_encryption_available) == 1) {

                                    SessionManager.getInstance(mContext).setIsEncryptionEnabled(true);
                                } else if (Integer.parseInt(is_encryption_available) == 0) {

                                    SessionManager.getInstance(mContext).setIsEncryptionEnabled(false);
                                }
                            } else {
                                SessionManager.getInstance(mContext).setIsEncryptionEnabled(false);
                            }
                            String contactUsEmailId = object.getString("contactus_email_address");
                            String chatLock = object.getString("chat_lock");
                            String secretChat = object.getString("secret_chat");
                            sessionManager.setContactUsEMailId(contactUsEmailId);
                            sessionManager.setLockChatEnabled(chatLock);
                            sessionManager.setSecretChatEnabled(secretChat);
                            //     Log.e("SKIP_OTP_VERIFICATION", "SKIP_OTP_VERIFICATION" + SKIP_OTP_VERIFICATION);
                        } catch (Exception e) {
                            Log.e("Exception", "Exception" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //     Log.d("Error.Response", error);
                    }
                }
        );

// add it to the RequestQueue
        queue.add(getRequest);
    }

    private void initSocketCallback() {
        if (mSocketManager != null)
            return;
        mSocketManager = SocketManager.getInstance();
        mSocketManager.init(VerifyPhoneScreen.this, new SocketManager.SocketCallBack() {
            @Override
            public void onSuccessListener(String eventName, Object... response) {
                if (eventName.equalsIgnoreCase(SocketManager.EVENT_GET_SETTINGS)) {

                    // hideProgressDialog();
                    //   ((CoreActivity) getActivity()).hidepDialog();
                   /* runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initProgress("Loading....", true);
                        }
                    });*/

                    String data = response[0].toString();
                    Log.e("EVENT_GET_SETTINGS", "EVENT_GET_SETTINGS" + data);
                    try {
                        JSONObject object = new JSONObject(data);

                        String twilioMode = "";
                        if (object.has("twilio"))
                            twilioMode = object.optString("twilio");
                        if (!twilioMode.equals(SessionManager.TWILIO_DEV_MODE)) {
                            //Twilio Sms
                            SKIP_OTP_VERIFICATION = false;
                            findViewById(R.id.send_message).setVisibility(View.VISIBLE);
                            findViewById(R.id.charge).setVisibility(View.VISIBLE);

                        } else if (object.has("jawal")) {
                            //Raad Sms
                            twilioMode = object.optString("jawal");
                            if (!twilioMode.equals(SessionManager.TWILIO_DEV_MODE))
                                SKIP_OTP_VERIFICATION = false;
                        }
                      /*  if (isnobelnet){
                            SKIP_OTP_VERIFICATION = false;
                            if (!SKIP_OTP_VERIFICATION) {
                                findViewById(R.id.send_message).setVisibility(View.VISIBLE);
                                findViewById(R.id.charge).setVisibility(View.VISIBLE);
                            }
                        }*/
                        SessionManager sessionManager = SessionManager.getInstance(VerifyPhoneScreen.this);
                        sessionManager.setTwilioMode(twilioMode);
                        // mSocketManager.disconnect();
                        //Check Encryption is available or not
                        if (object.has("is_encryption_available")) {
                            String is_encryption_available = object.getString("is_encryption_available");
                            if (Integer.parseInt(is_encryption_available) == 1) {

                                SessionManager.getInstance(mContext).setIsEncryptionEnabled(true);
                            } else if (Integer.parseInt(is_encryption_available) == 0) {

                                SessionManager.getInstance(mContext).setIsEncryptionEnabled(false);
                            }
                        } else {
                            SessionManager.getInstance(mContext).setIsEncryptionEnabled(false);
                        }
                        String contactUsEmailId = object.getString("contactus_email_address");
                        String chatLock = object.getString("chat_lock");
                        String secretChat = object.getString("secret_chat");
                        sessionManager.setContactUsEMailId(contactUsEmailId);
                        sessionManager.setLockChatEnabled(chatLock);
                        sessionManager.setSecretChatEnabled(secretChat);
                    } catch (Exception e) {
                        //   Log.e(TAG, "", e);
                    }
                } else if (eventName.equalsIgnoreCase(Socket.EVENT_CONNECT)) {
                    JSONObject object = new JSONObject();
                    //    Log.e("Verify", "Verify" + object + Constants.IS_ENCRYPTION_ENABLED);
                    //  if (!Constants.IS_ENCRYPTION_ENABLED)
                      /*  initProgress("Configure settings....", false);
                    showProgressDialog();*/
                    // CoreActiviy.showProgres(mContext);
                    mSocketManager.send(object, SocketManager.EVENT_GET_SETTINGS);

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //  mSocketManager.disconnect();
    }


    private void skipOTP(final String OTP) {

        if (MyFirebaseInstanceIDService.token != null && MyFirebaseInstanceIDService.token.length() > 0)
            GCM_Id = MyFirebaseInstanceIDService.token;
        else
            GCM_Id = MyFirebaseInstanceIDService.getFCMToken(this);

        if (GCM_Id == null || GCM_Id.isEmpty()) {

            // Toast.makeText(getApplicationContext(),"GCM Empty",Toast.LENGTH_LONG).show();

            GCM_Id = "121";
        }
        code = ccpicker.getSelectedCountryCode();

        //  Log.d(TAG, "skipOTP: " + OTP);

        String msisdn = SessionManager.getInstance(this).getPhoneNumberOfCurrentUser();
        String mobileNo = SessionManager.getInstance(this).getUserMobileNoWithoutCountryCode();
        String countryCode = SessionManager.getInstance(this).getUserCountryCode();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("msisdn", msisdn);
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_Id);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("PhNumber", mobileNo);
        params.put("userEmail", SessionManager.getInstance(VerifyPhoneScreen.this).getEmailofCurrentUser());

        params.put("CountryCode", countryCode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        params.put("code", OTP);
        params.put("callToken", "Android");
        params.put("isZainUser", "true");
        if (CoreController.isRaad) {
            params.put("server_security_key", "tawari123");//raad
            params.put("Appversion", "1");//raad
            params.put("brand", "" + Build.MANUFACTURER);//raad
            params.put("model", "" + Build.MODEL);//raad
            params.put("lat", "" + getLatitude());//raad
            params.put("lng", "" + getLongitude());//raad
        }

        if (isTwitgo) {
            params.put("email_otp", SessionManager.getInstance(this).getLoginEmailOTP());
        }
        params.put("pushToken", SessionManager.getInstance(this).getCurrentUserID());
        ServiceRequest request = new ServiceRequest(this);

        MyLog.d(TAG, "params: " + params);

        request.makeServiceRequest(Constants.VERIFY_SMS_CODE, Request.Method.POST, params, verifyCodeListener);
        showProgres();

    }

    public String getCountryZipCode() {
        String CountryID = "";
        String CountryZipCode = "";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
//Check sim is available or not
        if (isSimSupport(mContext)) {
            CountryID = manager.getSimCountryIso().toUpperCase();
        } else {
            CountryID = Locale.getDefault().getCountry();
        }
        selectedCountryModel = Country.getCountryByISO(CountryID);
        String networkCoutry = Locale.getDefault().getISO3Country();
        MyLog.e(TAG, "getCountryZipCode: " + networkCoutry);
        MyLog.e(TAG, "CountryID: " + CountryID);

        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);

        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                MyLog.e(TAG, "CountryZipCode: " + CountryZipCode);
                break;
            }
        }
        return CountryZipCode;
    }

    public boolean isSimSupport(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
        return !(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT);

    }

    private void setCountryCode() {
        String zipCode = "";//getCountryZipCode();
        if (!TextUtils.isEmpty(zipCode)) {
            code = zipCode;
            choseCountry.setVisibility(View.VISIBLE);
            pbCountryCode.setVisibility(View.GONE);
            choseCountry.setText("+" + code);
            if (selectedCountryModel != null) {
                ivCountryFlag.setVisibility(View.VISIBLE);
                ivCountryFlag.setImageResource(selectedCountryModel.getFlag());
                ivCountryFlag.setTag(selectedCountryModel.getCode());
            }
            sessionManager.setCountryCodeOfCurrentUser("+" + code);
        }
    }


    private GPSTracker gpsTracker;

    public double getLatitude() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(this);
        if (gpsTracker.getLocation() != null && gpsTracker.getLatitude() > 0) {
            return gpsTracker.getLatitude();
        }
        return 0;
    }

    public double getLongitude() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(this);
        if (gpsTracker.getLocation() != null && gpsTracker.getLongitude() > 0) {
            return gpsTracker.getLongitude();
        }
        return 0;
    }

    public void resend_code(View view) {
        try {
            hideProgressDialog();
            String phoneNumberStr = phoneNumber.getText().toString();
            if (phoneNumberStr.isEmpty()) {
                showToast(VerifyPhoneScreen.this, getResources().getString(R.string.please_enter_phone_number));
                return;
            }
            String phNumberWithCountryCode = "+" + code + phoneNumber.getText().toString();
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("msisdn", phNumberWithCountryCode);
            Log.e(TAG, "params" + params);
            ServiceRequest request = new ServiceRequest(this);
            request.makeServiceRequest(Constants.RESEND_INVITE_CODE_REQUEST, Request.Method.POST, params, resendInviteCodeListner);
            showProgres();
        } catch (Exception e) {
            Log.e(TAG, "resend_code: ", e);
        }
    }

    public void invitation_code(View view) {
        Intent intent = new Intent(mContext, AddInvitationScreen.class);
        mContext.startActivity(intent);
    }
}


