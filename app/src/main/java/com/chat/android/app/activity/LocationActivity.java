package com.chat.android.app.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.chat.android.R;
import com.chat.android.app.adapter.PlaceAutocompleteAdapter;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.SessionManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.DialogUtils;
import com.chat.android.utils.MediaUtility;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class LocationActivity extends CoreActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback, View.OnClickListener {
    RelativeLayout locationViewParent;
    View mBack;
    ImageView profileImage;
    View ivStatusCircle;
    TextView mHeaderTitle;
    Button mShare;
    View mMyLocation;
    TextView mLocation;
    TextView tvLocationHeader;
    ImageView mMarkerPin;
    AutoCompleteTextView mSearchLocation;
    LinearLayout searchContainer;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;

    LatLng latLng, mCurrentLatLng;
    GoogleMap mGoogleMap;
    SupportMapFragment mFragment;
    //    Marker mCurrLocation;

    //UserDataModel userModel;
    private Integer conversationId;
    private double altitude;
    private float accuracy, speed, heading;
    private String locationTitle = "";
    private HashMap<String, Object> fieldMap;
    private PlaceAutocompleteAdapter mAdapter;
    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(new LatLng(0, 0), new LatLng(0, 0));
    private String friendName, friendImage, onlineStatus;
    private String isFrom;
    private String TAG = "LocationActivity";
    private SessionManager sessionManager;
    private Getcontactname getcontactname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        initViews();
        sessionManager = SessionManager.getInstance(LocationActivity.this);
        getcontactname = new Getcontactname(LocationActivity.this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            friendName        = bundle.getString(AppConstants.IntentKeys.FRIEND_NAME.getValue());
            onlineStatus      = bundle.getString(AppConstants.IntentKeys.ONLINE_STATUS.getValue());
            friendImage       = bundle.getString(AppConstants.IntentKeys.FRIEND_IMAGE.getValue());
            isFrom = bundle.getString(AppConstants.IntentKeys.IS_FROM.getValue(), "");

            tvLocationHeader.setText(getResources().getString(R.string.share_your_loacation));
        }
        enableShareLocationButton(false);
        mSearchLocation.setVisibility(View.VISIBLE);
        String userId = sessionManager.getCurrentUserID();

        if(onlineStatus!=null && !TextUtils.isEmpty(onlineStatus)){
            ivStatusCircle.setVisibility(View.VISIBLE);
            int resource = MyChatUtils.getOnlineStatusResource(onlineStatus);
            ivStatusCircle.setBackgroundResource(resource);
        }
        mBack.setOnClickListener(this);
        mShare.setOnClickListener(this);
        mMyLocation.setOnClickListener(this);

        if (MyChatUtils.isNetworkAvailable(this)) {
            mFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mFragment.getMapAsync(this);
        } else {

        }
        searchHandling();
    }

    private void initViews() {
        locationViewParent = findViewById(R.id.activity_location);
        mBack = findViewById(R.id.iv_back);
        profileImage = findViewById(R.id.ivUserProfile);
        ivStatusCircle = findViewById(R.id.status_circle);
        mHeaderTitle = findViewById(R.id.tvName);
        mShare = findViewById(R.id.btn_share_location);
        mMyLocation = findViewById(R.id.img_my_location);
        mLocation = findViewById(R.id.tv_location);
        tvLocationHeader = findViewById(R.id.tv_location_header);
        mMarkerPin = findViewById(R.id.marker_pin);
        mSearchLocation = findViewById(R.id.et_search);
        searchContainer = findViewById(R.id.ll_search_container);
    }

    private void searchHandling() {
        searchContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSearchLocation.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
            }
        });
        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        int leftDrawable = 0;
                        String hint = "";
                        int width = 0;
                        int height = mSearchLocation.getHeight();
                        int delay = 0;
                        if(isOpen) {
                            width = ViewGroup.LayoutParams.MATCH_PARENT;
                            leftDrawable = 0;
                            hint = "";
                        } else {
                            width = ViewGroup.LayoutParams.WRAP_CONTENT;
                            leftDrawable = R.drawable.auto_search;
                            hint = getResources().getString(R.string.search);
                            delay = 350;
                        }
                        final String finalHint = hint;
                        final int finalLeftDrawable = leftDrawable;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mSearchLocation.setCompoundDrawablesWithIntrinsicBounds(finalLeftDrawable, 0, 0, 0);
                                mSearchLocation.setHint(finalHint);
                            }
                        }, delay);
                        mSearchLocation.setCursorVisible(isOpen);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
                        mSearchLocation.setLayoutParams(params);
                    }
                });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(false);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //get latlng at the center by calling
                latLng = mGoogleMap.getCameraPosition().target;
                setLocationText(latLng.latitude, latLng.longitude);
            }
        });
        buildGoogleApiClient();

        mGoogleApiClient.connect();
    }

    private void enableShareLocationButton(boolean enable) {
        if (enable) {
            mShare.setEnabled(true);
            mShare.setBackgroundResource(R.drawable.btn_selecter_dark);
        } else {
            mShare.setEnabled(false);
            mShare.setBackgroundResource(R.drawable.all_button_disabled_bg);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //Unregister for location callbacks:
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyChatUtils.isNetworkAvailable(this)) {
            mFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mFragment.getMapAsync(this);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            return;
        }


        setupAutoCompleteForLocation();
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            mGoogleMap.clear();
            mCurrentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            setLocationText(mCurrentLatLng.latitude, mCurrentLatLng.longitude);
            animateCamera();
        } else {
            MyLog.d(TAG, "Last location not found.");
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        MyLog.d(TAG, "on connection suspended google api client location.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        MyLog.d(TAG, "on connection failed google api client location.");
    }

    private void animateCamera() {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latLng.latitude, latLng.longitude)).zoom(14).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                enableShareLocationButton(true);
            }
        }, 1000);
    }

    @Override
    public void onLocationChanged(Location location) {
        altitude = location.getAltitude();
        accuracy = location.getAccuracy();
        speed = location.getSpeed();
        heading = location.getBearing();
    }

    private void setLocationText(double latitude, double longitude) {
        latLng = new LatLng(latitude, longitude);

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                locationTitle = address;
                mLocation.setText(address);
            }
        } catch (IOException e) {
            e.printStackTrace();
            MyChatUtils.notifyException(e);
        }
    }

    @Override
    public void onClick(View view) {
        MyChatUtils.hideKeyboard(this);
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_share_location:
                if (MyChatUtils.isLocationEnabled(this))
                    requestPermission();
                else {
                    showLocationNotEnabledError(getResources().getString(R.string.location_not_enabled_error));
                }
                break;
            case R.id.img_my_location:
                getMyLocation();
                break;
        }
    }

    private void getMyLocation() {
        if (MyChatUtils.isLocationEnabled(this)) {
            if (mCurrentLatLng != null) {
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(mCurrentLatLng, 14);
                mGoogleMap.animateCamera(cameraUpdate);
                latLng = null;
            } else {
                if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) return;

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {

                    MyLog.d(TAG, "Last location found.");
                    mGoogleMap.clear();
                    mCurrentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    setLocationText(mCurrentLatLng.latitude, mCurrentLatLng.longitude);
                    animateCamera();
                }
            }
            return;
        }
        showLocationNotEnabledError(getResources().getString(R.string.location_not_enabled_updated));
    }

    private void showLocationNotEnabledError(String message) {
        DialogUtils.showAlertWithButtons(this, getResources().getString(R.string.location_not_enabled_title), message, getString(R.string.ok),
                getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DialogUtils.dismiss();
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }

                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DialogUtils.dismiss();
                        finish();
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        DialogUtils.dismiss();
                        finish();
                    }
                });
    }

    public void requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.RequestCodes.REQUEST_LOCATION.getCode());
        } else
            shareLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode == AppConstants.RequestCodes.REQUEST_LOCATION.getCode()) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                shareLocation();
            } else {
                MyLog.d(TAG, "permission not granted, finishing activity");
                finish();
            }
        }
    }

    private void shareLocation() {

        if(!MyChatUtils.isNetworkAvailable(this)) {
            CoreActivity.showToast(this, getResources().getString(R.string.internet_no_connected));
            return;
        }
        captureScreen();
    }


    public void captureScreen() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if(latLng==null) return;

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);

        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
        mMarkerPin.setVisibility(View.GONE);
        mGoogleMap.addMarker(markerOptions);
        final String userId = sessionManager.getCurrentUserID();

        setResultAndNavigate("", "", "image/"+AppConstants.MimeTypes.JPG.name());

//        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
//            @Override
//            public void onSnapshotReady(Bitmap snapshot) {
//
//                showProgres();
//                final String key = conversationId +"/"+  (String.valueOf(new Date().getTime())) + userId;
//                String pathOriginal = MediaUtility.saveFileAndGetPath(LocationActivity.this, snapshot, (String.valueOf(new Date().getTime())) + "-original", "."+AppConstants.MimeTypes.JPG.name());// full size image
//
//                if(!TextUtils.isEmpty(pathOriginal))
//                    setResultAndNavigate(key, pathOriginal, "image/"+AppConstants.MimeTypes.JPG.name());
//                else {
//                    hidepDialog();
//                    showToast(LocationActivity.this, getString(R.string.share_error));
//                }
//            }
//        };
//        if (mGoogleMap != null)
//            mGoogleMap.snapshot(callback);
    }

    private void setResultAndNavigate(final String key, String path, String mimeType) {

        Intent data = new Intent();
        //data.putExtra(AppConstants.IntentKeys.FILE_PATH.getValue(), path);
        data.putExtra(AppConstants.IntentKeys.FILE_TYPE.getValue(), "location");
        data.putExtra(AppConstants.IntentKeys.LOCATION_ADDRESS.getValue(), mLocation.getText().toString());
        if(latLng != null) {
            data.putExtra(AppConstants.IntentKeys.LOCATION_LATITUDE.getValue(), latLng.latitude);
            data.putExtra(AppConstants.IntentKeys.LOCATION_LONGITUDE.getValue(), latLng.longitude);
        } else {
            data.putExtra(AppConstants.IntentKeys.LOCATION_LATITUDE.getValue(), mCurrentLatLng.latitude);
            data.putExtra(AppConstants.IntentKeys.LOCATION_LONGITUDE.getValue(), mCurrentLatLng.longitude);
        }

        setResult(Activity.RESULT_OK, data);
        finish();
    }

    private void setupAutoCompleteForLocation() {

        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder().build();
        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY, autocompleteFilter);

        mSearchLocation.setAdapter(mAdapter);
        mSearchLocation.setOnItemClickListener(mAutocompleteClickListener);

    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            MyChatUtils.hideKeyboard(LocationActivity.this);
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {

            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                places.release();
                return;
            }

            final Place place = places.get(0);
            mSearchLocation.setAdapter(null);
            mSearchLocation.setText(place.getName());
            mSearchLocation.setSelection(mSearchLocation.getText().length());
            latLng = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
            places.release();
            mSearchLocation.setAdapter(mAdapter);
            setLocationText(latLng.latitude, latLng.longitude);
            animateCamera();
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
