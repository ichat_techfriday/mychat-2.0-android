package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class CashplusUpdateUserProfile {
    @SerializedName("UserName")
    String userName;
    @SerializedName("token")
    String token;
    @SerializedName("fullName")
    String fullName;
    @SerializedName("first_Name")
    String fname;
    @SerializedName("middle_Name")
    String mName;
    @SerializedName("last_Name")
    String lname;
    @SerializedName("address")
    String address;
    @SerializedName("city")
    String city;
    @SerializedName("dob")
    String dob;
    @SerializedName("gender")
    String gender;
    @SerializedName("occupation")
    String occupation;
    @SerializedName("nationality")
    String nationality;
    @SerializedName("iD_TYPE")
    String iD_TYPE;
    @SerializedName("iD_SERIAL")
    String iD_SERIAL;
    @SerializedName("iD_EXPIRY")
    String iD_EXPIRY;
    @SerializedName("iD_ISSUE_PLACE")
    String iD_ISSUE_PLACE;
    @SerializedName("iD_ISSUE_DATE")
    String iD_ISSUE_DATE;
    @SerializedName("iD_HAS_NO_EXPIRY")
    String iD_HAS_NO_EXPIRY;
    @SerializedName("iD_FACE_MIME_IMAGE")
    String iD_FACE_MIME_IMAGE;
    @SerializedName("iD_BACK_MIME_IMAGE")
    String iD_BACK_MIME_IMAGE;
    @SerializedName("portrait_MIME_IMAGE")
    String portrait_MIME_IMAGE;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getiD_TYPE() {
        return iD_TYPE;
    }

    public void setiD_TYPE(String iD_TYPE) {
        this.iD_TYPE = iD_TYPE;
    }

    public String getiD_SERIAL() {
        return iD_SERIAL;
    }

    public void setiD_SERIAL(String iD_SERIAL) {
        this.iD_SERIAL = iD_SERIAL;
    }

    public String getiD_EXPIRY() {
        return iD_EXPIRY;
    }

    public void setiD_EXPIRY(String iD_EXPIRY) {
        this.iD_EXPIRY = iD_EXPIRY;
    }

    public String getiD_ISSUE_PLACE() {
        return iD_ISSUE_PLACE;
    }

    public void setiD_ISSUE_PLACE(String iD_ISSUE_PLACE) {
        this.iD_ISSUE_PLACE = iD_ISSUE_PLACE;
    }

    public String getiD_ISSUE_DATE() {
        return iD_ISSUE_DATE;
    }

    public void setiD_ISSUE_DATE(String iD_ISSUE_DATE) {
        this.iD_ISSUE_DATE = iD_ISSUE_DATE;
    }

    public String getiD_HAS_NO_EXPIRY() {
        return iD_HAS_NO_EXPIRY;
    }

    public void setiD_HAS_NO_EXPIRY(String iD_HAS_NO_EXPIRY) {
        this.iD_HAS_NO_EXPIRY = iD_HAS_NO_EXPIRY;
    }

    public String getiD_FACE_MIME_IMAGE() {
        return iD_FACE_MIME_IMAGE;
    }

    public void setiD_FACE_MIME_IMAGE(String iD_FACE_MIME_IMAGE) {
        this.iD_FACE_MIME_IMAGE = iD_FACE_MIME_IMAGE;
    }

    public String getiD_BACK_MIME_IMAGE() {
        return iD_BACK_MIME_IMAGE;
    }

    public void setiD_BACK_MIME_IMAGE(String iD_BACK_MIME_IMAGE) {
        this.iD_BACK_MIME_IMAGE = iD_BACK_MIME_IMAGE;
    }

    public String getPortrait_MIME_IMAGE() {
        return portrait_MIME_IMAGE;
    }

    public void setPortrait_MIME_IMAGE(String portrait_MIME_IMAGE) {
        this.portrait_MIME_IMAGE = portrait_MIME_IMAGE;
    }

    public CashplusUpdateUserProfile(String userName, String token, String fullName, String fname, String mName, String lname, String address, String city, String dob, String gender, String occupation, String nationality, String iD_TYPE, String iD_SERIAL, String iD_EXPIRY, String iD_ISSUE_PLACE, String iD_ISSUE_DATE, String iD_HAS_NO_EXPIRY, String iD_FACE_MIME_IMAGE, String iD_BACK_MIME_IMAGE, String portrait_MIME_IMAGE) {
        this.userName = userName;
        this.token = token;
        this.fullName = fullName;
        this.fname = fname;
        this.mName = mName;
        this.lname = lname;
        this.address = address;
        this.city = city;
        this.dob = dob;
        this.gender = gender;
        this.occupation = occupation;
        this.nationality = nationality;
        this.iD_TYPE = iD_TYPE;
        this.iD_SERIAL = iD_SERIAL;
        this.iD_EXPIRY = iD_EXPIRY;
        this.iD_ISSUE_PLACE = iD_ISSUE_PLACE;
        this.iD_ISSUE_DATE = iD_ISSUE_DATE;
        this.iD_HAS_NO_EXPIRY = iD_HAS_NO_EXPIRY;
        this.iD_FACE_MIME_IMAGE = iD_FACE_MIME_IMAGE;
        this.iD_BACK_MIME_IMAGE = iD_BACK_MIME_IMAGE;
        this.portrait_MIME_IMAGE = portrait_MIME_IMAGE;
    }

    public CashplusUpdateUserProfile() {

    }
}
