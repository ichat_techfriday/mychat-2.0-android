package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chat.android.R;


public class VHCallReceived extends RecyclerView.ViewHolder {

    public TextView tvCallLbl, tvDateLbl;


    public VHCallReceived(View itemView) {
        super(itemView);

        tvCallLbl =  itemView.findViewById(R.id.tvCallLbl);
        tvDateLbl = itemView.findViewById(R.id.tvDateLbl);
    }
}
