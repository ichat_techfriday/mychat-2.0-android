package com.chat.android.app.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.SessionManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.StorageUtility;
import com.chat.android.utils.TextUtils;

import java.util.Calendar;
import java.util.Objects;

public class MeetupProfileFragment extends MeetupBaseFragment {

    //static
    private static final String TAG = "MeetupProfileFragment";

    private ImageView mImageUploadBtn;
    private AppCompatButton mContinueBtn;
    private EditText mFullName;
    private EditText mBio;
    private TextView mDob;
    private Context mContext;
    private Activity mActivity;
    private View mClearName;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        MyLog.d("onAttach: ");
        mActivity = activity;
    }

    //bundle
    private Bundle mBundle = new Bundle();

    private int distanceInMeters;

    private DatePickerDialog mDatePickerDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meetup_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //getting bundle
        mBundle = this.getArguments();
        if (mBundle != null) {
            distanceInMeters = mBundle.getInt("distanceInMeters");
        }

        //init views
        View mView = view.findViewById(R.id.app_bar_meetup);
        mImageUploadBtn = (ImageView) view.findViewById(R.id.image_upload_btn);
        mFullName = (EditText) view.findViewById(R.id.et_f_name);
        mBio = (EditText) view.findViewById(R.id.et_bio);
        mDob = (TextView) view.findViewById(R.id.tv_dob);
        mContinueBtn = (AppCompatButton) view.findViewById(R.id.btn_continue);
        mClearName = view.findViewById(R.id.iv_clear_name);
        mBio.setText(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.MEETUP_BIO.getValue(), "").trim());
        mBio.post(() -> {
            int editTextRowCount = mBio.getLineCount();
            if (editTextRowCount > 2) {
                mBio.setSelection(mBio.getText().toString().length());
                mBio.getText().delete(mBio.getSelectionEnd() - 1, mBio.getSelectionStart());
            }
        });
        mDob.setText(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.MEETUP_DOB.getValue(), ""));
        SessionManager sessionManager = SessionManager.getInstance(mContext);
        String profileImage = sessionManager.getUserProfilePic();

        AppUtils.loadImage(mContext, AppUtils.getValidProfilePath(profileImage), mImageUploadBtn, 150, R.drawable.ic_placeholder_black);

        //click listeners
        mView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
        mFullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0)
                    mClearName.setVisibility(View.VISIBLE);
                else
                    mClearName.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBio.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int editTextRowCount = mBio.getLineCount();
                if (editTextRowCount > 2) {
                    mBio.getText().delete(mBio.getSelectionEnd() - 1, mBio.getSelectionStart());
                }
            }
        });
        mFullName.setText(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.MEETUP_FULL_NAME.getValue(), ""));

        mClearName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFullName.setText("");
            }
        });

        mContinueBtn.setOnClickListener(v -> {
            if (TextUtils.isEmpty(mFullName.getText().toString())) {
                CoreActivity.showToast(mContext, mActivity.getString(R.string.please_enter_name));
                return;
            }

            StorageUtility.saveDataInPreferences(mContext, AppConstants.SPKeys.MEETUP_FULL_NAME.getValue(), mFullName.getText().toString());
            StorageUtility.saveDataInPreferences(mContext, AppConstants.SPKeys.MEETUP_BIO.getValue(), mBio.getText().toString().trim());
            StorageUtility.saveDataInPreferences(mContext, AppConstants.SPKeys.MEETUP_DOB.getValue(), mDob.getText().toString());
            boolean isPermissionAllowed =  StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.MEETUP_PERMISSION_ALLOWED.getValue(), false);

            mBundle.putString("fullName", mFullName.getText().toString());
            mBundle.putString("bio", mBio.getText().toString().trim());
            mBundle.putString("dob", mDob.getText().toString());
            mBundle.putInt("distanceInMeters", distanceInMeters);

            if (isPermissionAllowed && MyChatUtils.isLocationPermissionAllowed(mContext)) {
                MeetupNearbyFragment meetupNearbyFragment = new MeetupNearbyFragment();
                mBundle.putDouble("latitude", lat);
                mBundle.putDouble("longitude", lng);
                mBundle.putInt("limit", limit);
                mBundle.putString("userActivationStatus", userActivationStatus);
                meetupNearbyFragment.setArguments(mBundle);
                OpenFragment(meetupNearbyFragment);
            } else {
                MeetupPermissionFragment meetupPermissionFragment = new MeetupPermissionFragment();
                //setting bundle
                meetupPermissionFragment.setArguments(mBundle);
                OpenFragment(meetupPermissionFragment);
            }

        });

        mDob.setOnClickListener(v -> {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR); // current year
            int mMonth = c.get(Calendar.MONTH); // current month
            int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

            mDatePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                    (view1, year, monthOfYear, dayOfMonth) -> {
                        mDob.setText(dayOfMonth + "/"
                                + (monthOfYear + 1) + "/" + year);
                    }, mYear, mMonth, mDay);
            mDatePickerDialog.show();
        });
    }

    public void OpenFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;
        FragmentManager manager = getActivity().getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_layout, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}