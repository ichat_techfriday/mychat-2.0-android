package com.chat.android.app.utils;

import android.content.Context;

import com.chat.android.core.CoreController;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CAS60 on 8/11/2017.
 */
public class BlockUserUtils {

    private static final String TAG = "BlockUserUtils";
    public static void changeUserBlockedStatus(Context context, EventBus eventBus, String currentUserId,
                                               String toUserId, boolean isSecretChat) {

        String status = "0";
        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);

        if (contactDB_sqlite.getBlockedStatus(toUserId, isSecretChat) != null) {
            status = contactDB_sqlite.getBlockedStatus(toUserId, isSecretChat);
        }

        try {
            JSONObject myobj = new JSONObject();

            if (status.equals("1")) {
                myobj.put("status", "0");
            } else {
                myobj.put("status", "1");
            }

            if (isSecretChat) {
                myobj.put("secret_type", "yes");
            }

            myobj.put("from", currentUserId);
            myobj.put("to", toUserId);

            SendMessageEvent messageEvent = new SendMessageEvent();
            messageEvent.setEventName(SocketManager.EVENT_BLOCK_USER);
            messageEvent.setMessageObject(myobj);
            eventBus.post(messageEvent);
        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }
    }

}
