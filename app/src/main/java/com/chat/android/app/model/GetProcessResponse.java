package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetProcessResponse implements Serializable {
    @SerializedName("_XML_DATA")
    public XML_DATA _XML_DATA;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;
    @SerializedName("PDF_Data")
    public String PDF_Data;
    @SerializedName("Pin_Code")
    public String Pin_Code;
    @SerializedName("Reference_Code")
    public String Reference_Code;

    public XML_DATA get_XML_DATA() {
        return _XML_DATA;
    }

    public void set_XML_DATA(XML_DATA _XML_DATA) {
        this._XML_DATA = _XML_DATA;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getPDF_Data() {
        return PDF_Data;
    }

    public void setPDF_Data(String PDF_Data) {
        this.PDF_Data = PDF_Data;
    }

    public String getPin_Code() {
        return Pin_Code;
    }

    public void setPin_Code(String pin_Code) {
        Pin_Code = pin_Code;
    }

    public String getReference_Code() {
        return Reference_Code;
    }

    public void setReference_Code(String reference_Code) {
        Reference_Code = reference_Code;
    }

    public class XML_DATA implements Serializable {


        @SerializedName("Request_Date")
        public String Request_Date;
        @SerializedName("Response_Date")
        public String Response_Date;
        @SerializedName("Request")
        public String Request;
        @SerializedName("Response")
        public String Response;
        @SerializedName("RequestResultCode")
        public String RequestResultCode;
        @SerializedName("RequestResultMessage")
        public String RequestResultMessage;

        public String getRequest_Date() {
            return Request_Date;
        }

        public void setRequest_Date(String request_Date) {
            Request_Date = request_Date;
        }

        public String getResponse_Date() {
            return Response_Date;
        }

        public void setResponse_Date(String response_Date) {
            Response_Date = response_Date;
        }

        public String getRequest() {
            return Request;
        }

        public void setRequest(String request) {
            Request = request;
        }

        public String getResponse() {
            return Response;
        }

        public void setResponse(String response) {
            Response = response;
        }

        public String getRequestResultCode() {
            return RequestResultCode;
        }

        public void setRequestResultCode(String requestResultCode) {
            RequestResultCode = requestResultCode;
        }

        public String getRequestResultMessage() {
            return RequestResultMessage;
        }

        public void setRequestResultMessage(String requestResultMessage) {
            RequestResultMessage = requestResultMessage;
        }
    }


}
