package com.chat.android.app.activity;



import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.chat.android.R;
import com.chat.android.app.utils.LocationUtil;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;


public class GPSTracker extends Service implements LocationListener {

    private static final String TAG = "GPSTracker";
    private  Context mContext=null;

    // Flag for GPS status
    boolean isGPSEnabled = false;

    // Flag for network status
    boolean isNetworkEnabled = false;

    // Flag for GPS status
    boolean canGetLocation = false;

    Location location; // Location
    double latitude; // Latitude
    double longitude; // Longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 20; // 20 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 5 ; // 30 seconds

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GPSTracker(Context context) {
        this.mContext = context;

    }

    public GPSTracker(){
        mContext=this;
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            // Getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // Getting network status
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // No network provider is enabled
            } else {
                this.canGetLocation = true;
                try {
                    if (isNetworkEnabled) {
                        /*locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);*/
                        if (ContextCompat.checkSelfPermission(mContext,
                                Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,  MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            MyLog.d("Network", "Network");
                            if (locationManager != null) {
                                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                }
                            }
                        }
                    }
                } catch (SecurityException e) {
                    MyLog.e(TAG,"",e);
                }
                // If GPS enabled, get latitude/longitude using GPS Services
                try {
                    if (isGPSEnabled) {
                        if (location == null) {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this, Looper.myLooper());
                            MyLog.d("GPS Enabled", "GPS Enabled");
                            if (locationManager != null) {
                                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                }
                            }
                        }
                    }
                } catch (SecurityException e) {
                    MyLog.e(TAG,"",e);
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG,"",e);
        }
        if(location==null || location.getLatitude()==0){
            location=LocationUtil.getLastKnownLocation(true, mContext);
        }

        return location;
    }

    /**
     * Stop using GPS listener Calling this function will stop using GPS in your
     * app.
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            //locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Function to get latitude
     */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/Wi-Fi enabled
     *
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public boolean isgpsenabled() {
        return this.isGPSEnabled;
    }

    public boolean isnetworksenabled() {
        return this.isNetworkEnabled;
    }


    /**
     * Function to show settings alert dialog. On pressing the Settings button
     * it will launch Settings Options.
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle(getResources().getString(R.string.gps_is_settings));
		alertDialog.setCancelable(false);
        // Setting Dialog Message
        alertDialog
                .setMessage(getResources().getString(R.string.gps_is_not_enabled_please_enable_gps));

        // On pressing the Settings button.
        alertDialog.setPositiveButton(getResources().getString(R.string.action_settings),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mContext.startActivity(intent);
                    }
                });

        // On pressing the cancel button

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: ");
        this.location=location;
        sendLocation(location);
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void sendLocation(Location location){
        try {
            Log.d(TAG, "sendLocation: ");
            if(location==null || location.getLatitude()==0)
                return;
            Log.d(TAG, "sendLocation: not null");
            JSONObject object = new JSONObject();
            String from= SessionManager.getInstance(mContext).getCurrentUserID();
            boolean isAdminRequested=SessionManager.getInstance(mContext).isAdminLocationRequested();
            Log.d(TAG, "sendLocation: "+isAdminRequested);
            if(isAdminRequested) {
                object.put("from", from);
                object.put("lat", location.getLatitude());
                object.put("lng", location.getLongitude());
                SendMessageEvent postEvent = new SendMessageEvent();
                postEvent.setEventName(SocketManager.EVENT_ADMIN_TRACK_START);
                postEvent.setMessageObject(object);
                EventBus.getDefault().post(postEvent);
            }
            }
        catch (Exception e){
            Log.e(TAG, "sendLocation: ",e );
        }
        }
}
