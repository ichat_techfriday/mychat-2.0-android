package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WalletModel  implements Serializable {

    @SerializedName("Wallet_ID")
    int Wallet_ID;

    public int getWallet_ID() {
        return Wallet_ID;
    }

    public void setWallet_ID(int wallet_ID) {
        Wallet_ID = wallet_ID;
    }


}
