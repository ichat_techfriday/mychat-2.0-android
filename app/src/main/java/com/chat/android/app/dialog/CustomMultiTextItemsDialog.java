package com.chat.android.app.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.adapter.CustomMultiItemsAdapter;
import com.chat.android.app.adapter.RItemAdapter;
import com.chat.android.app.widget.AvnNextLTProDemiButton;
import com.chat.android.app.widget.AvnNextLTProDemiTextView;
import com.chat.android.core.model.MultiTextDialogPojo;

import java.util.List;

/**
 * Created by CAS60 on 2/16/2017.
 */
public class CustomMultiTextItemsDialog extends DialogFragment implements View.OnClickListener {

    private RecyclerView rvListItems;
    private TextView tvTitle;
    private AvnNextLTProDemiButton btnNegative;

    private List<MultiTextDialogPojo> labelsList;
    private DialogItemClickListener listener;
    private String titleText, negativeBtnText;

    public interface DialogItemClickListener {
        void onDialogItemClick(int position);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        View view = inflater.inflate(R.layout.dialog_custom_multi_text_items, container, false);

        tvTitle = view.findViewById(R.id.tvTitle);
        btnNegative = view.findViewById(R.id.btnNegative);

        rvListItems = view.findViewById(R.id.rvListItems);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        rvListItems.setLayoutManager(manager);

        rvListItems.addOnItemTouchListener(new RItemAdapter(getActivity(), rvListItems, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if(listener != null) {
                    getDialog().dismiss();
                    listener.onDialogItemClick(position);
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        if(titleText != null && !titleText.equalsIgnoreCase("")) {
            tvTitle.setText(titleText);
            tvTitle.setVisibility(View.VISIBLE);
        }

        if(negativeBtnText != null && !negativeBtnText.equalsIgnoreCase("")) {
            btnNegative.setVisibility(View.VISIBLE);
            btnNegative.setOnClickListener(CustomMultiTextItemsDialog.this);
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(labelsList != null) {
            CustomMultiItemsAdapter adapter = new CustomMultiItemsAdapter(getActivity(), labelsList);
            rvListItems.setAdapter(adapter);
        }
    }

    public void setDialogItemClickListener(DialogItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnNegative) {
            getDialog().dismiss();
        }
    }

    public void setLabelsList(List<MultiTextDialogPojo> labelsList) {
        this.labelsList = labelsList;
    }

    public void setTitleText(String text) {
        this.titleText = text;
    }

    public void setNegativeButtonText(String text) {
        this.negativeBtnText = text;
    }
}
