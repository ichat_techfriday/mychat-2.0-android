package com.chat.android.app.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.QueryPurchaseHistoryParams;
import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chat.android.MyFirebaseInstanceIDService;
import com.chat.android.R;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.dialog.CustomMultiTextItemsDialog;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.MobileNumberUtils;
import com.chat.android.app.web.IChatWithoutHeaderAPIController;
import com.chat.android.app.widget.DrawArc;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.model.MultiTextDialogPojo;
import com.chat.android.core.model.PictureModel;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SCLoginModel;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.model.ValidateSubResponse;
import com.chat.android.core.scimbohelperclass.ScimboDialogUtils;
import com.chat.android.core.scimbohelperclass.ScimboImageUtils;
import com.chat.android.core.scimbohelperclass.ScimboPermissionValidator;
import com.chat.android.core.service.Constants;
import com.chat.android.core.service.ContactsSync;
import com.chat.android.core.service.ServiceRequest;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.PincodesCacheUtility;
import com.chat.android.utils.StorageUtility;
import com.chat.android.utils.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveId;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mukesh.countrypicker.Country;
import com.soundcloud.android.crop.Crop;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import androidx.core.content.ContextCompat;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import id.zelory.compressor.Compressor;
import io.socket.client.Socket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScimboProfileInfoScreen extends OnBoardingHomeParentActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final int REQUEST_CODE_GALLERY = 1;
    private static final int REQUEST_CODE_CAMERA = 2;
    private Uri cameraImageUri;
    private Context mcontext;
    private RoundedImageView choose_photo;
    private GoogleApiClient mGoogleApiClient;
    private Handler mHandler = null;
    private Runnable mRunnableHandler = null;
    private boolean mshow;
    private static final int REQUEST_CODE_RESOLUTION = 1;
    private Activity mActivity;
    private String tag_string_req = "string_req", mChatDbName;
    protected static final String TAG = "ActionBarActivity";
    private PictureModel pictureModel = null;
    private SCLoginModel SCLoginModel = null;
    private boolean mClicked = false;
    private String pictureUrl = "", mCurrentUserId;
    private EditText nameEditText, emailEditText, pinEditText;
    private DrawArc drac;
    private SessionManager msessionmanager;
    private static boolean isEnterClicked = false;
    private boolean isDeninedRTPs = false;
    String mDriveFileName, mBackupGmailId;
    private boolean showRationaleRTPs = false;
    private ArrayList<ScimboPermissionValidator.Constants> myPermissionConstantsArrayList;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 123;
    /* Global variables for empjis */
    private FrameLayout emoji;
    private TextView title;
    private ImageButton happyFace;
    ImageView back;
    final Context context = this;
    private ProgressDialog progressDialog;
    private DriveId mDriveId;
    private ImageView tvNext;
    private ScrollView profileScrollview;
    private boolean isNextBtnPressed;
    private SocketManager mSocketManager = null;
    private boolean isHomeScreenCalled = false;
    View selectImage_profileInfo_layout;
    private String GCM_Id;
    private String Handy_username = "";
    private String Handy_email = "";
    private String Handy_phone = "";
    private String Handy_password = "";
    private boolean isZainStatusChecked = false;
    PincodesCacheUtility pincodesCacheUtility;
    EditText phoneNumberEditText;
    String msisdn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_info);
        emailEditText = findViewById(R.id.email);
        pinEditText = findViewById(R.id.pin_code);
        phoneNumberEditText=findViewById(R.id.phoneNumber);
        profileScrollview=findViewById(R.id.profileScroll);
        pincodesCacheUtility=PincodesCacheUtility.getInstance(ScimboProfileInfoScreen.this);
        sessionManager = SessionManager.getInstance(this);
        MyLog.d(TAG, "onCreate: ");
        mActivity = ScimboProfileInfoScreen.this;
        mcontext = ScimboProfileInfoScreen.this;

//        //TODO move this to response of profileinfo socket event
//        initBillingClient();

        mHandler = new Handler();
        if (savedInstanceState != null) {
            cameraImageUri = Uri.parse(savedInstanceState.getString("ImageUri"));
        } else {
            cameraImageUri = Uri.parse("");
        }


        mCurrentUserId = SessionManager.getInstance(ScimboProfileInfoScreen.this).getCurrentUserID();

        selectImage_profileInfo_layout = findViewById(R.id.selectImage_profileInfo_layout);
        drac = findViewById(R.id.drac);
        nameEditText = findViewById(R.id.typeName_profileInfo);
        tvNext = findViewById(R.id.tvNext);
        back = findViewById(R.id.back);
        tvNext.setOnClickListener(ScimboProfileInfoScreen.this);
        back.setOnClickListener(ScimboProfileInfoScreen.this);

        msessionmanager = SessionManager.getInstance(ScimboProfileInfoScreen.this);
        pincodesCacheUtility.init(this);

        msisdn = StorageUtility.getDataFromPreferences(this, AppConstants.SPKeys.BRANCH_MSISDN_ID.getValue(), "");
        MyLog.d(TAG, " msisdn : " + msisdn);
        if (!TextUtils.isEmpty(msisdn)) {
            showProgres();
            setGCMId();
            this.makeVerificationRequest(msisdn);
        } else {
            initProgress(getResources().getString(R.string.loading_in), true);
            populateView();
        }

        KeyboardVisibilityEvent.setEventListener((Activity) context, isOpen -> {
            if (isOpen)
                profileScrollview.smoothScrollTo(0, profileScrollview.getBottom());
        });
    }

    private BillingClient mBillingClient;
    public void initBillingFlow() {
        PurchasesUpdatedListener purchasesUpdatedListener = (billingResult, purchases) -> {};

        mBillingClient = BillingClient.newBuilder(context)
                .setListener(purchasesUpdatedListener)
                .enablePendingPurchases()
                .build();

        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                if (billingResult.getResponseCode() ==  BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    queryPurchaseHistory();
                }
            }
            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.

                //TODO yet to be verified if it works like expected
                mBillingClient.startConnection(this);
            }
        });
    }

    private void queryPurchaseHistory() {
        QueryPurchaseHistoryParams queryParams = QueryPurchaseHistoryParams.newBuilder()
                .setProductType(BillingClient.ProductType.SUBS)
                .build();
        mBillingClient.queryPurchaseHistoryAsync(
                queryParams,
                (billingResult, list) -> {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        if (list == null)
                            return;
                        int size = list.size();
                        Log.d("Taha", "queryPurchaseHistory: size of purchase history list " + size);
                        if (size > 0) {
                            PurchaseHistoryRecord historyRecord= list.get(0);
                            Log.d("Taha", "queryPurchaseHistory: historyRecord timestamp" + historyRecord.getPurchaseTime());
                            validateSubscription(historyRecord);
                        } else {
                            ActivityLauncher.launchBuySubscription(ScimboProfileInfoScreen.this, false);
                        }
                    }
                }
        );
    }

    private void validateSubscription(PurchaseHistoryRecord historyRecord) {
        String userID = SessionManager.getInstance(this).getCurrentUserID();
        String deviceID = Settings.Secure.getString(
                getContentResolver(),
                Settings.Secure.ANDROID_ID
        );

        showProgressDialog();
        IChatWithoutHeaderAPIController.validateSubscription(this,
        userID, AppConstants.ONE_MONTH_SUB_ID, historyRecord.getPurchaseToken(), deviceID,
                historyRecord.getPurchaseTime(), new Callback<ValidateSubResponse> (){

                    @Override
                    public void onResponse(@NonNull Call<ValidateSubResponse> call, @NonNull Response<ValidateSubResponse> response) {
                        hideProgressDialog();
                        if (!response.isSuccessful())
                            return;

                        ValidateSubResponse validateSubResponse = response.body();
                        if (validateSubResponse != null && validateSubResponse.getStatus()) {
                            if (checkIfSubscriptionIsExpired(historyRecord.getPurchaseTime())) {
                                ActivityLauncher.launchBuySubscription(ScimboProfileInfoScreen.this, true);
                            } else {
                                SessionManager sessionManager = SessionManager.getInstance(ScimboProfileInfoScreen.this);
                                sessionManager.IsBackupRestored(true);
                                sessionManager.IsprofileUpdate(true);
                                sessionManager.Islogedin(true);
                                ActivityLauncher.launchHomeScreen(ScimboProfileInfoScreen.this);
                            }
                        } else {
                            ActivityLauncher.launchBuySubscription(ScimboProfileInfoScreen.this, false);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ValidateSubResponse> call, @NonNull Throwable t) {
                        hideProgressDialog();
                        showToast(ScimboProfileInfoScreen.this, getString(R.string.failed_to_validate_user_sub));
                    }
        });
    }

    public boolean checkIfSubscriptionIsExpired(long purchaseTime/*InMillis*/) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date currentDate = calendar.getTime();
        calendar.setTimeInMillis(purchaseTime);

        long _30daysInMillis = 2592000000L;
        long _2daysInMillis = 172800000L;

        // 30 days time subscription duration and 2 days extended trial time added to the purchase time
        long expiryTime = purchaseTime + _30daysInMillis + _2daysInMillis;
        long currentTime = currentDate.getTime();
        long difference = expiryTime - currentTime;

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        int hours = (int) (difference / hoursInMilli);
        if ( hours <= 0) {
            MyLog.d("Taha", "Renew Subscription Required");
            return true;
        } else {
            Log.d("Taha", "checkIfSubscriptionIsExpired: homeScreenLaunched");
            return false;
        }
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }


    private void populateView() {

        initSocketManagerCallback();
        mSocketManager.connect();

        boolean is_telpon_chat = mcontext.getResources().getBoolean(R.bool.is_telpon_chat);
        Log.e("is_telpon_chat", "is_telpon_chat" + is_telpon_chat);

        if (emailEditText != null) {
            emailEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        //do here your stuff f
                        hideKeyboard();
                        loadnext();
                        return true;
                    }
                    return false;
                }


            });
        }
        if (pinEditText != null) {
            pinEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        //do here your stuff f
                        hideKeyboard();
                        loadnext();
                        return true;
                    }
                    return false;
                }


            });

        }
        Bundle bundle = getIntent().getExtras();
        /* Get the mobile number from the previous activity */
        choose_photo = findViewById(R.id.selectImage_profileInfo);
        if (is_telpon_chat) {
            choose_photo.setImageResource(R.drawable.ic_placeholder_black);
            drac.setVisibility(View.GONE);
        } else {
            selectImage_profileInfo_layout.setBackground(ContextCompat.getDrawable(mcontext, R.drawable.circle_image_shadow));
            choose_photo.setImageResource(R.drawable.ic_placeholder_black);
            drac.setVisibility(View.VISIBLE);

        }
        Log.e("getnameOfCurrentUser", "getnameOfCurrentUser" + msessionmanager.getnameOfCurrentUser());
        String username = msessionmanager.getnameOfCurrentUser();
        String email = msessionmanager.getnameOfEmail();
        String pinCode = msessionmanager.getPinCodeOfCurrentUser();
        if (!TextUtils.isEmpty(username)) {
            nameEditText.setText(username);
        }
        if (!TextUtils.isEmpty(email)) {
            emailEditText.setText(msessionmanager.getnameOfEmail());
        }
        if (!TextUtils.isEmpty(pinCode))
            pinEditText.setText(pinCode);
        String profilePicPath = AppUtils.getProfileFilePath(this);
        if (profilePicPath != null && !profilePicPath.isEmpty()) {
            Glide.with(context)
                    .load(/*profilePicPath*/AppUtils.getGlideURL(profilePicPath, context))
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .into(new SimpleTarget<Bitmap>() {

                        @Override
                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                            choose_photo.setImageBitmap(arg0);
                        }
                    });
        } else {
            choose_photo.setImageResource(R.drawable.ic_profile_nav_header);
            if (is_telpon_chat) {
                choose_photo.setImageResource(R.drawable.ic_placeholder_black);
            } else {
                choose_photo.setImageResource(R.drawable.ic_profile_nav_header);


            }
        }
        checkAndRequestPermissions();
        if (!mSocketManager.isConnected()) {
            MyLog.d(TAG, "Socket is not connected, connecting....");
            mSocketManager.connect();
        } else {
            MyLog.d(TAG, "Socket is connected, fetching zain status....");
        }
    }

    private void makeVerificationRequest(String msisdn) {
        MobileNumberUtils phoneNumberUtils = MobileNumberUtils.getInstance();
        String phoneCode = phoneNumberUtils.getCountry(getResources().getStringArray(R.array.CountryCodes), msisdn);
        Country country = Country.getCountryByISO(phoneCode);

        if (country == null) {
            hidepDialog();
            return;
        }
        String number = msisdn.replace(country.getDialCode(), "");
        this.phoneNumberTxt = number;
        this.code = country.getDialCode().replace("+", "");
        MyLog.d(TAG, " phoneCode  : " + phoneCode + ",, + number : " + number  + ",,, " + country.getDialCode());
        if (MyFirebaseInstanceIDService.token != null && MyFirebaseInstanceIDService.token.length() > 0)
            GCM_Id = MyFirebaseInstanceIDService.token;
        else
            GCM_Id = MyFirebaseInstanceIDService.getFCMToken(this);

        if (GCM_Id == null || GCM_Id.isEmpty()) {
            GCM_Id = "121";
        }


        //   showProgressDialog();
        HashMap<String, String> params = new HashMap<String, String>();
        String cCode = code;
        //String uPhone = phoneNumber.getText().toString();
        //System.out.println("Country_code" + "" + cCode);
        params.put("msisdn", msisdn);
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_Id);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("PhNumber", number);
        params.put("CountryCode", "+" + cCode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        params.put("callToken", "Android");

        MyLog.d(TAG, " params: " + params);
        ServiceRequest request = new ServiceRequest(this);
        request.makeServiceRequest(Constants.VERIFY_NUMBER_REQUEST, Request.Method.POST, params, verifcationListener);
    }

    @Override
    protected void verifyPhoneCompleted(String message) {
        //String code = SessionManager.getInstance(this).getLoginOTP();
        MyLog.d(TAG,"phone verification done  : " + otp);
        verifyCode(this.otp);
    }

    private void verifyCode(final String otp) {

        if (com.chat.android.fcm.MyFirebaseInstanceIDService.token != null && com.chat.android.fcm.MyFirebaseInstanceIDService.token.length() > 0)
            GCM_Id = com.chat.android.fcm.MyFirebaseInstanceIDService.token;
        else
            GCM_Id = com.chat.android.fcm.MyFirebaseInstanceIDService.getFCMToken(this);

        String msisdn = "+" + code + phoneNumberTxt;///SessionManager.getInstance(this).getPhoneNumberOfCurrentUser();
        String mobileNo = phoneNumberTxt;//SessionManager.getInstance(this).getUserMobileNoWithoutCountryCode();
        String countryCode = "+" + code;///SessionManager.getInstance(this).getUserCountryCode();

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("user_name", Handy_username);
        params.put("email", Handy_email);
        params.put("password", Handy_password);

        params.put("msisdn", msisdn);
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        params.put("DeviceId", android_id);
        params.put("gcm_id", GCM_Id);
        params.put("manufacturer", Build.MANUFACTURER);
        params.put("Version", Build.VERSION.RELEASE);
        params.put("OS", "android");
        params.put("PhNumber", mobileNo);
        params.put("CountryCode", countryCode);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String currentDateandTime = sdf.format(new Date());
        params.put("DateTime", currentDateandTime);
        params.put("code", otp);
        params.put("isZainUser", "true");
        params.put("callToken", "Android");
        params.put("Appversion", "");//raad
        params.put("brand", "" + Build.MANUFACTURER);//raad
        params.put("model", "" + Build.MODEL);//raad
        params.put("pushToken", SessionManager.getInstance(this).getCurrentUserID());
        MyLog.d(TAG,"verify code params  : " + params);

        ServiceRequest request = new ServiceRequest(this);
        request.makeServiceRequest(Constants.VERIFY_SMS_CODE, Request.Method.POST, params, verifyCodeListener);

    }

    @Override
    protected void verifyCodeListenerCompleted() {
        hideProgressDialog();
        populateView();
    }


    private void setGCMId() {
        if (MyFirebaseInstanceIDService.token != null && MyFirebaseInstanceIDService.token.length() > 0)
            GCM_Id = MyFirebaseInstanceIDService.token;
        else
            GCM_Id = MyFirebaseInstanceIDService.getFCMToken(this);

        if (GCM_Id == null || GCM_Id.isEmpty()) {

            // Toast.makeText(getApplicationContext(),"GCM Empty",Toast.LENGTH_LONG).show();

            GCM_Id = "121";
        }
    }

    private void createUser() {
        try {
            MyLog.d(TAG, "createUser: ");
            JSONObject object = new JSONObject();
            object.put("_id", mCurrentUserId);
            object.put("mode", "phone");
            object.put("chat_type", "single");
            String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();
            object.put("token", securityToken);
            object.put("device", Constants.DEVICE);

            MyLog.e(TAG, "object: " + object);
            mSocketManager.send(object, SocketManager.EVENT_CREATE_USER);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void fetchSecretKeys() {
        try {

            JSONObject fetchKeysObj = new JSONObject();
            fetchKeysObj.put("userId", SessionManager.getInstance(this).getCurrentUserID());
            MyLog.e(TAG, "fetchSecretKeys: " + fetchKeysObj);
            mSocketManager.send(fetchKeysObj, SocketManager.EVENT_GET_SECRET_KEYS);
        } catch (Exception e) {
            MyLog.e(TAG, "fetchSecretKeys: ", e);
        }
    }

    private void initSocketManagerCallback() {
        if (mSocketManager == null) {
//Check call back is the same screen

            if (SocketManager.callBack == null) {
                SocketManager.callBack = callBack;
            } else if (SocketManager.callBack instanceof ContactsSync) {
                SocketManager.callBack = callBack;
            } else {
                SocketManager.callBack = callBack;
            }
            mSocketManager = SocketManager.getInstance();
            mSocketManager.init(mActivity, callBack);
        }

    }

    SocketManager.SocketCallBack callBack = new SocketManager.SocketCallBack() {
        @Override
        public void onSuccessListener(String eventName, Object... response) {
            MyLog.d(TAG, "in callback function for event: " + eventName);
            ReceviceMessageEvent me = new ReceviceMessageEvent();
            me.setEventName(eventName);
            if (AppUtils.isEncryptionEnabled(mcontext)) {
                try {
                    if (response != null && !SocketManager.excludedList.contains(eventName)) {
                        MyLog.d(TAG, "invokeCallBack: event name" + eventName);
                        response[0] = SocketManager.getDecryptedMessage(mcontext, response[0].toString(), eventName);
                        String decrypted = null;
                        if (response[0] != null) {
                            decrypted = response[0].toString();
                        }
                        MyLog.d(TAG, "onSuccessListener: " + decrypted);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "onSuccessListener: ", e);
                }
            }
            me.setObjectsArray(response);
            if (eventName != null && !eventName.equals("sc_change_online_status") && !eventName.equals("sc_get_offline_status"))
                MyLog.d(TAG, "onSuccessListener: " + eventName);
            MyLog.d(TAG, "Event_response"+" "+" "+eventName+" "+" "+response.toString());

            switch (eventName) {
                case SocketManager.EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION:

                    break;
                case SocketManager.EVENT_USER_CREATED:
                    if (AppUtils.isEncryptionEnabled(mcontext)) {
                        boolean isLoginKeySent = !msessionmanager.isLoginKeySent();
                        if (msessionmanager != null && isLoginKeySent) {
                            attachDeviceWithAccount();
                        }


                    }
                    break;
                case SocketManager.EVENT_USER_AUTHENTICATED: {
                    MyLog.e(TAG, "EVENT_USER_AUTHENTICATED");

                    if (AppUtils.isEncryptionEnabled(mcontext)) {
                        String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();
                        MyLog.e(TAG, "securityToken" + securityToken);
                        if (AppUtils.isEmpty(securityToken)) {
                            fetchSecretKeys();
                        }
                    }
                }
                break;
                case Socket.EVENT_CONNECT: {
                    createUser();
                    MyLog.e(TAG, "EVENT_CONNECT");
                }
                break;

                case Socket.EVENT_DISCONNECT:
                    //mSocketManager.disconnect();
                    break;
                case SocketManager.EVENT_CHANGE_USER_NAME: {
                    MyLog.e(TAG, "EVENT_CHANGE_USER_NAME");
                    try {


                        JSONObject object = new JSONObject(response[0].toString());

                        String err = object.getString("err");
                        final String message = object.getString("message");

                        if (Integer.parseInt(err) == 0) {

                            final String from = object.getString("from");

                            if (object.has("name")) {
                                final String mName = object.getString("name");
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        if (from.equalsIgnoreCase(mCurrentUserId)) {
                                            nameEditText.setText(mName);
                                        }                                        // Stuff that updates the UI
                                    }
                                });
                            }
                            if (object.has("email")) {
                                final String email = object.getString("email");
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        if (from.equalsIgnoreCase(mCurrentUserId)) {
                                            emailEditText.setText(email);
                                            SessionManager.getInstance(mcontext).setEmail(email);

                                        }                                        // Stuff that updates the UI
                                    }
                                });
                            }
                            if (object.has("pinCode")) {
                                final String mPinCode = object.getString("pinCode");
                                msessionmanager.setUserPinCode(mPinCode);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        if (from.equalsIgnoreCase(mCurrentUserId)) {
                                            pinEditText.setText(mPinCode);
                                        }                                        // Stuff that updates the UI
                                    }
                                });
                            }
                            if (mClicked) {
                                hideProgressDialog();
                                //TODO uncomment the actual check and remove this stub check for testing
//                                if (!checkIfNonZainIraqUser()) {
                                if (checkIfNonZainIraqUser()) {
                                    //TODO fetch if user active subs are returned
                                    initBillingFlow();
                                } else {
                                    goBackupRestoreScreen();
                                }
                            }
                        } else if (Integer.parseInt(err) == 1) {
                            hideProgressDialog();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showToast(mcontext, message);
                                }
                            });
                        }
                    } catch (JSONException e) {
                        hideProgressDialog();
                        MyLog.e(TAG, "", e);
                    }
                }
                break;

                case SocketManager.EVENT_IMAGE_UPLOAD: {
                    try {
                        JSONObject objects = new JSONObject(response[0].toString());
                        final String message = objects.getString("message");
                        String from = objects.getString("from");
                        String type = objects.getString("type");

                        if (from.equalsIgnoreCase(mCurrentUserId) && type.equalsIgnoreCase("single")) {
                            String path = "";
                            String fileName = objects.getString("file");

                            if (fileName == null || fileName.isEmpty()) {
                                String imageName = objects.getString("ImageName");
                                path = Constants.USER_PROFILE_URL + imageName + "?id=" + AppUtils.eodMillis();
                            } else {
                                path = fileName + "?id=" + AppUtils.eodMillis();
                            }
                            path = AppUtils.getValidProfilePath(path);
                            SessionManager.getInstance(ScimboProfileInfoScreen.this).setUserProfilePic(path);

                            //  hideProgressDialog();
                            hidepDialog();
                            final String finalImgPath = path;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //Toast.makeText(ScimboProfileInfoScreen.this, message, Toast.LENGTH_SHORT).show();
                                    AppUtils.loadImageSmooth(ScimboProfileInfoScreen.this, finalImgPath, choose_photo, 150, R.drawable.ic_profile_nav_header);
                                    //Toast.makeText(ScimboProfileInfoScreen.this, "Profile Image Successfully Updated", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    } catch (Exception e) {
                        MyLog.e(TAG, "", e);
                    }
                    MyLog.e(TAG, "EVENT_IMAGE_UPLOAD");
                }
                break;
                case SocketManager.EVENT_FILE_RECEIVED: {
                    try {
                        JSONObject object = new JSONObject(response[0].toString());
                        String from = object.getString("from");
                        String imgName = object.getString("ImageName");

                        if (from.equalsIgnoreCase(mCurrentUserId)) {
                            JSONObject imgObject = new JSONObject();

                            imgObject.put("from", mCurrentUserId);
                            imgObject.put("type", "single");
                            imgObject.put("ImageName", imgName);
                            imgObject.put("removePhoto", "no");
                            MyLog.e("EVENT_IMAGE_UPLOAD", "EVENT_IMAGE_UPLOAD" + imgObject);
                            mSocketManager.send(imgObject, SocketManager.EVENT_IMAGE_UPLOAD);
                        }
                    } catch (JSONException e) {
                        MyLog.e(TAG, "", e);
                    }
                    MyLog.e(TAG, "EVENT_FILE_RECEIVED");
                }
                break;
                case SocketManager.EVENT_GET_SECRET_KEYS: {
                    try {
                        MyLog.d(TAG, "EVENT_GET_SECRET_KEYS: ");
                        String data = response[0].toString();
                        JSONObject object = new JSONObject(data);
                        String publicKey = "", privateKey = "";
                        if (object.has("public_key")) {
                            publicKey = object.getString("public_key");
                            privateKey = object.getString("private_key");
                            SessionManager.getInstance(ScimboProfileInfoScreen.this).setPublicEncryptionKey(publicKey);
                            SessionManager.getInstance(ScimboProfileInfoScreen.this).setPrivateEncryptionKey(privateKey);
                        }
                        if (isNextBtnPressed) {
                            hideProgressDialog();
                            uploadDataToServer();
                            isNextBtnPressed = false;
                        }

                    } catch (Exception e) {
                        hideProgressDialog();
                        MyLog.e(TAG, "onSuccessListener: ", e);
                    }
                    MyLog.e(TAG, "EVENT_GET_SECRET_KEYS");
                }
                break;
            }
        }

    };

    private boolean checkIfNonZainIraqUser() {
        //Iraq country code == +964
        SessionManager sessionManager = SessionManager.getInstance(this);
        String countryCode = sessionManager.getCountryCodeOfCurrentUser();

        if (sessionManager.getZainUser() == 1) {
            return false;
        }

        if (countryCode != null && !countryCode.equals("+964")) {
            return false;
        }

        return true;
    }

    private void checkAndRequestPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            myPermissionConstantsArrayList = new ArrayList<>();
            myPermissionConstantsArrayList.add(ScimboPermissionValidator.Constants.PERMISSION_CAMERA);
            myPermissionConstantsArrayList.add(ScimboPermissionValidator.Constants.PERMISSION_READ_EXTERNAL_STORAGE);
//          myPermissionConstantsArrayList.add(ScimboPermissionValidator.Constants.Record_Audio);
            myPermissionConstantsArrayList.add(ScimboPermissionValidator.Constants.Record_setting);
            myPermissionConstantsArrayList.add(ScimboPermissionValidator.Constants.PERMISSION_WRITE_EXTERNAL_STORAGE);
            if (ScimboPermissionValidator.checkPermission(ScimboProfileInfoScreen.this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE)) {
                onPermissionGranted();
            }
        } else {
            onPermissionGranted();
        }
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("ImageUri", cameraImageUri.toString());
    }


    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                            isDeninedRTPs = true;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                showRationaleRTPs = shouldShowRequestPermissionRationale(permission);
                            }
                        }
                        break;
                    }
                    onPermissionResult();
                } else {
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isZainStatusChecked = false;
        isEnterClicked = false;
        if (SocketManager.callBack == null) {
            SocketManager.callBack = callBack;
        } else if (SocketManager.callBack instanceof ContactsSync) {
            SocketManager.callBack = callBack;
        }
        if (mGoogleApiClient == null && mBackupGmailId != null && !mBackupGmailId.equals("")) {
            // Create the API client and bind it to an instance variable.
            // We use this instance as the callback for connection and connection
            // failures.
            // Since no account name is passed, the user is prompted to choose.
            connectGoogleApiClient();
        } else if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            connectGoogleApiClient();
        }
    }

    private void onPermissionResult() {
        if (isDeninedRTPs) {
            if (!showRationaleRTPs) {
                //goToSettings();
                ScimboDialogUtils.showPermissionDeniedDialog(ScimboProfileInfoScreen.this);
            } else {
                isDeninedRTPs = false;
                ScimboPermissionValidator.checkPermission(this,
                        myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE);
            }
        } else {
            onPermissionGranted();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    private void onPermissionGranted() {
        choose_photo.setOnClickListener(ScimboProfileInfoScreen.this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//      getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
          /*  case R.id.action_settings1:
                loadNext();
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void uploadDataToServer() {
        String changedName = nameEditText.getText().toString().trim();
        if (nameEditText.getText().toString().trim().length() > 0) {
            Log.e("getnameOfCurrentUser", "getnameOfCurrentUser" + SessionManager.getInstance(ScimboProfileInfoScreen.this).getnameOfCurrentUser());

            Log.e("getnameOfCurrentUser", "changedName" + changedName);
            if (!SessionManager.getInstance(ScimboProfileInfoScreen.this).getnameOfCurrentUser().equals(changedName)) {
                showProgressDialog();

                try {
                    JSONObject object = new JSONObject();
                    object.put("from", mCurrentUserId);
                    object.put("name", changedName);
                    object.put("pinCode", pinEditText.getText().toString());
                    if (mcontext.getResources().getString(R.string.app_name).equalsIgnoreCase("myChat")) {
                        object.put("email", emailEditText.getText().toString().trim());
                        Log.e("uploadDataToServer", "equals" + object);

                    } else {
                        Log.e("uploadDataToServer", "equals" + object);

                    }
                    Log.e("EVENT_CHANGE_USER_NAME", "EVENT_CHANGE_USER_NAME" + object);
                    mSocketManager.send(object, SocketManager.EVENT_CHANGE_USER_NAME);

                } catch (JSONException e) {
                    hideProgressDialog();
                    MyLog.e(TAG, "Exceptionnn", e);
                }
            } else {

                if (mcontext.getResources().getString(R.string.app_name).equalsIgnoreCase("myChat")) {
                    showProgressDialog();
                    try {

                        JSONObject object = new JSONObject();
                        object.put("from", mCurrentUserId);
                        object.put("name", changedName);
                        object.put("pinCode", pinEditText.getText().toString());
                        if (mcontext.getResources().getString(R.string.app_name).equalsIgnoreCase("myChat")) {
                            object.put("email", emailEditText.getText().toString().trim());
                        }
                        mSocketManager.send(object, SocketManager.EVENT_CHANGE_USER_NAME);

                    } catch (Exception e) {
                        hideProgressDialog();
                        e.getMessage();
                    }
                }
            }
        } else {
            showToast(getBaseContext(), getResources().getString(R.string.please_enter_name));
        }
    }

    private void goBackupRestoreScreen() {
        //mSocketManager.disconnect();
        if (!isHomeScreenCalled) {
            isHomeScreenCalled = true;
            SessionManager sessionMAnager = SessionManager.getInstance(ScimboProfileInfoScreen.this);
            sessionMAnager.setnameOfCurrentUser(nameEditText.getText().toString());
            //   sessionMAnager.setEmail(emailEditText.getText().toString());
            if (mcontext.getResources().getString(R.string.app_name).equalsIgnoreCase("myChat")) {
                sessionMAnager.setEmail(emailEditText.getText().toString());

            }

            String loginCount = sessionMAnager.getLoginCount();
            if (loginCount.equalsIgnoreCase("0")) {
                sessionMAnager.IsBackupRestored(true);
                sessionMAnager.IsprofileUpdate(true);
                SessionManager.getInstance(context).Islogedin(true);
                ActivityLauncher.launchHomeScreen(ScimboProfileInfoScreen.this);
            } else {
                mChatDbName = MessageDbController.DB_NAME;
                sessionMAnager.IsprofileUpdate(true);
                SessionManager.getInstance(context).Islogedin(true);
                goHomeScreen();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideProgressDialog();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);

        }
    }

    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();
    }

    private void connectGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addScope(Drive.SCOPE_APPFOLDER)
                .setAccountName(mBackupGmailId)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }


    private void goHomeScreen() {
        CoreController.setDBInstance(ScimboProfileInfoScreen.this);
        SessionManager sessionManager = SessionManager.getInstance(ScimboProfileInfoScreen.this);
        sessionManager.IsBackupRestored(true);
        //  mSocketManager.disconnect();
        Intent intent = new Intent(ScimboProfileInfoScreen.this, NewHomeScreenActivty.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in,R.anim.left_out) ;
        mActivity.finishAffinity();
    }

    private void checkBackupDetailsExists() {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            MyLog.d(TAG, "getDriveSettings: ");
            mSocketManager.send(object, SocketManager.EVENT_GET_GOOGLE_DRIVE_SETTINGS);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void uploadImage(String selectedFilePath) {

        //showProgressDialog();
        showProgres();
        String currentUserId = SessionManager.getInstance(ScimboProfileInfoScreen.this).getCurrentUserID();
        String serverFileName = currentUserId.concat(".jpg");

        try {
            File file = new File(selectedFilePath);
            File compressFile = Compressor.getDefault(ScimboProfileInfoScreen.this).compressToFile(file);
            byte[] buffer = new byte[(int) compressFile.length()];

            FileInputStream fis = new FileInputStream(compressFile);
            fis.read(buffer); //read file into bytes[]
            fis.close();
            JSONObject imgObj = new JSONObject();
            imgObj.put("ImageName", serverFileName);
            imgObj.put("buffer", buffer);
            imgObj.put("bufferAt", 0);
            imgObj.put("from", currentUserId);
            imgObj.put("uploadType", "single");
            imgObj.put("removePhoto", "");
            imgObj.put("FileEnd", 1);
            MyLog.e("uploadImage", "imgObj" + imgObj);
            mSocketManager.send(imgObj, SocketManager.EVENT_FILE_UPLOAD);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                beginCrop(cameraImageUri);
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    showToast(this, getResources().getString(R.string.failed_to_capture_image));
                }
            }
        } else if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {
            if (data != null) {
                Uri selectedImageUri = data.getData();
                beginCrop(selectedImageUri);
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            Uri uri = Crop.getOutput(data);
            String selectedFilePath = uri.getPath();

            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(selectedFilePath, options);
                Bitmap alignedBitmap = ScimboImageUtils.getAlignedBitmap(bitmap, selectedFilePath);
                /*try {
                    // make a new bitmap from your file
                    OutputStream outStream = null;
                    File file=new File(selectedFilePath);

                    outStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                    outStream.flush();
                    outStream.close();
                    uploadImage(file.getPath());

                } catch (Exception e) {
                    uploadImage(selectedFilePath);
                }*/
                choose_photo.setImageBitmap(alignedBitmap);



               /* BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap);
                choose_photo.setBackgroundDrawable(ob);*/


                uploadImage(selectedFilePath);
            } catch (IOException e) {
                MyLog.e(TAG, "", e);
            }
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            choose_photo.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            showToast(this, Crop.getError(result).getMessage());
        }
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.back) {
            // onBackPressed();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            SessionManager sessionMAnager = SessionManager.getInstance(ScimboProfileInfoScreen.this);
            SessionManager.getInstance(ScimboProfileInfoScreen.this).Islogedin(false);
            finish();

        } else if (view.getId() == R.id.selectImage_profileInfo) {
            hideKeyboard();
            List<MultiTextDialogPojo> labelsList = new ArrayList<>();
            MultiTextDialogPojo label = new MultiTextDialogPojo();
            label.setImageResource(R.drawable.chat_add_dialog_camera);
            label.setLabelText(getResources().getString(R.string.take_image_from_camera));
            labelsList.add(label);

            label = new MultiTextDialogPojo();
            label.setImageResource(R.drawable.chat_add_dialog_gallery);
            label.setLabelText(getResources().getString(R.string.add_image_from_gallery));
            labelsList.add(label);

            CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
            dialog.setTitleText(getResources().getString(R.string.profile_picture));
            dialog.setNegativeButtonText(getResources().getString(R.string.cancel));
            dialog.setLabelsList(labelsList);

            dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
                @Override
                public void onDialogItemClick(int position) {
                    switch (position) {

                        case 0:
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                StrictMode.setVmPolicy(builder.build());
                            }
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File cameraImageOutputFile = new File(
                                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                    createCameraImageFileName());
                            cameraImageUri = Uri.fromFile(cameraImageOutputFile);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
                            intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                            startActivityForResult(intent, REQUEST_CODE_CAMERA);
                            break;

                        case 1:
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
                            break;

                    }
                }
            });

            dialog.show(getSupportFragmentManager(), "Profile Pic");
        } else if (view.getId() == R.id.tvNext) {
            String msisdn = SessionManager.getInstance(this).getPhoneNumberOfCurrentUser();
            String pinCodeFromPhone=pincodesCacheUtility.getPinCodeFromPhone(msisdn);
            Log.d(TAG, "pinCodeFromPhone: "+pinCodeFromPhone);
            if(pinCodeFromPhone.equals(pinEditText.getText().toString())){
                loadnext();
            }
            else if (!pinCodeFromPhone.equals(pinEditText.getText().toString())){
                if(!pincodesCacheUtility.doesPinCodeExists(pinEditText.getText().toString())){
                    loadnext();
                }
                else{
                    showToast(this,getResources().getString(R.string.pin_code_already_exists));
                }
            }
            else{
                showToast(this,getResources().getString(R.string.call_fail_error));
            }
        }
    }


    private void loadnext() {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        //Check your Network connection
        if (AppUtils.isNetworkAvailable(context)) {
            if (mSocketManager.isConnected()) {

                // loadNext();
                mClicked = true;
                isNextBtnPressed = true;
                if (nameEditText.getText().toString().trim().length() > 0) {

                    if (getResources().getString(R.string.app_name).equalsIgnoreCase("myChat")) {
                        if (pinEditText.getText().toString().trim().length() > 0) {
                            CheckEncryption();
                        } else {
                            isNextBtnPressed = false;
                            showToast(this, R.string.enter_pin_cde);
                        }
                    } else {
                        isNextBtnPressed = false;
                        showToast(this, R.string.app_error);
                    }

                } else {
                    isNextBtnPressed = false;
                    showToast(this, R.string.entername);
                }
            } else {
                mSocketManager.connect();
                mshow = true;
                mHandler.postDelayed(mRunnableHandler, 1000);
            }
        } else {
            showToast(context, R.string.networkerror);
        }
    }

    public void CheckEncryption() {
        if (AppUtils.isEncryptionEnabled(mcontext)) {
            showProgressDialog();
            String publicKey = SessionManager.getInstance(ScimboProfileInfoScreen.this).getPublicEncryptionKey();
            if (publicKey == null || publicKey.isEmpty()) {
                if (AppUtils.isEncryptionEnabled(mcontext)) {
                    String securityToken = SessionManager.getInstance(getApplicationContext()).getSecurityToken();
                    if (AppUtils.isEmpty(securityToken)) {
                        isNextBtnPressed = true;
                        fetchSecretKeys();
                    }
                }
            }
        } else {
            uploadDataToServer();
        }
    }

    private String createCameraImageFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return timeStamp + ".jpg";
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        MyLog.d("BackUpRestoreError - " + result.getErrorCode(), result.toString());

        switch (result.getErrorCode()) {
            case ConnectionResult.INVALID_ACCOUNT:
                showGmailLoginAlert(getResources().getString(R.string.login_with)+" " + mBackupGmailId + " "+getResources().getString(R.string.in_gmail_app_and_then_try_to_backup_your)+"  "
                        +getResources().getString(R.string.chat_history));
                break;

            case ConnectionResult.INTERNAL_ERROR:
                showToast(this, getResources().getString(R.string.call_fail_error));
                break;

            case ConnectionResult.TIMEOUT:
                MyLog.d("Timeout");
                showToast(this, getResources().getString(R.string.call_fail_error));
                break;

            case ConnectionResult.NETWORK_ERROR:
                showToast(this, getResources().getString(R.string.no_internet_connection));
                break;

            default:
                if (!result.hasResolution()) {
                    // show the localized error dialog.
                    if (result.getErrorCode() == ConnectionResult.INVALID_ACCOUNT) {
                        showGmailLoginAlert(getResources().getString(R.string.login_with)+" " + mBackupGmailId + " "+getResources().getString(R.string.in_gmail_app_and_then_try_to_backup_your) +"  "
                                +getResources().getString(R.string.chat_history)+getResources().getString(R.string.if_skip_this_step_cant_restore_your_messages));
                    } else {
                        if (result.hasResolution()) {
                            try {
                                result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
                            } catch (Exception e) {
                                MyLog.e("Restore", "Exception while starting resolution activity", e);
                            }
                        } else {
//                            GoogleApiAvailability.getInstance().getErrorDialog(this, result.getErrorCode(), 0).show();
                            new AlertDialog.Builder(ScimboProfileInfoScreen.this)
                                    .setTitle(getResources().getString(R.string.signin_failed))
                                    .setMessage(getResources().getString(R.string.try_with_different_account))
                                    .setCancelable(false)
                                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Whatever...
                                            dialog.dismiss();
                                        }
                                    }).show();
                            return;
                        }
                    }
                    return;
                } else {
                    // The failure has a resolution. Resolve it.
                    // Called typically when the app is not yet authorized, and an
                    // authorization
                    // dialog is displayed to the user.
                    try {
                        result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
                    } catch (Exception e) {
                        MyLog.e("Chat restore", "Exception while starting resolution activity", e);
                    }
                }
        }
    }

    // Remove old messages from server if user skips message back up restore
    private void performSkipBackupMessages() {
        try {
            JSONObject object = new JSONObject();
            object.put("from", mCurrentUserId);
            mSocketManager.send(object, SocketManager.EVENT_SKIP_BACKUP_MESSAGES);

            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.show();
            }
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

    private void showGmailLoginAlert(String msg) {
        CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(msg);
        dialog.setPositiveButtonText(getResources().getString(R.string.proceed));
        dialog.setNegativeButtonText(getResources().getString(R.string.skip));
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                Intent addAccountIntent = new Intent(Settings.ACTION_ADD_ACCOUNT)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    addAccountIntent.putExtra(Settings.EXTRA_ACCOUNT_TYPES, new String[]{"com.google"});
                }
                startActivity(addAccountIntent);
            }

            @Override
            public void onNegativeButtonClick() {
                performSkipBackupMessages();
//                goHomeScreen();
            }
        });
        dialog.show(getSupportFragmentManager(), "Account Alert");
    }

    private void attachDeviceWithAccount() {

        String deviceId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        String loginKey = msessionmanager.getLoginKey();
        mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();
        try {
            JSONObject msgObj = new JSONObject();
            msgObj.put("from", mCurrentUserId);
            msgObj.put("DeviceId", deviceId);
            msgObj.put("login_key", loginKey);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_UPDATE_MOBILE_LOGIN_NOTIFICATION);
            event.setMessageObject(msgObj);
            EventBus.getDefault().post(event);
        } catch (JSONException e) {
            MyLog.e(TAG, "", e);
        }
    }

}
