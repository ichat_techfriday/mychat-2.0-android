package com.chat.android.app.utils;

import android.content.Context;
import androidx.annotation.NonNull;

import com.chat.android.core.socket.MessageService;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;

/**
 * Created by user145 on 6/23/2018.
 */
public class JobAlarmSingle extends Job {

    public static final String TAG = ">>>> job_demo_tag";


    @Override
    protected boolean isRequirementDeviceIdleMet() {
        MyLog.d(TAG, "isRequirementDeviceIdleMet: ");
        return super.isRequirementDeviceIdleMet();
    }

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        // run your job here
        MyLog.d(TAG, "onRunJob: ");


        final Context context=this.getContext();

        if(!AppUtils.isMyServiceRunning(context, MessageService.class)){
            MyLog.e("isMyServiceRunning","isMyServiceRunning started");
            AppUtils.startService(context,MessageService.class);
        }else {
            MyLog.e("isMyServiceRunning","isMyServiceRunning running already");
        }

        return Result.SUCCESS;
    }

    public static void scheduleJob() {
         new JobRequest.Builder(JobAlarmSingle.TAG)
                .setExecutionWindow(2000,2000)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                 .setUpdateCurrent(true)
                .build()
                .schedule();

        new JobRequest.Builder(JobAlarmSingle.TAG)
                .setExecutionWindow(10000,10000)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setUpdateCurrent(true)
                .build()
                .schedule();

        new JobRequest.Builder(JobAlarmSingle.TAG)
                .setExecutionWindow(60000,60000)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setUpdateCurrent(true)
                .build()
                .schedule();


        new JobRequest.Builder(JobAlarmSingle.TAG)
                .setExecutionWindow(120000,120000)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    private static void cancelJob(int jobId) {
        JobManager.instance().cancel(jobId);
    }
}
