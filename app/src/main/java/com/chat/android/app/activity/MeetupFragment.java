package com.chat.android.app.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.utils.MyLog;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.StorageUtility;

public class MeetupFragment extends Fragment {
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MyLog.d("onAttach: ");
        mContext = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meetup, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView how_it_works = (TextView)view.findViewById(R.id.how_it_works);
        how_it_works.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MeetupActivity.class);
                intent.putExtra(AppConstants.IntentKeys.MEETUP_SCREEN_TYPE.getValue(), AppConstants.MEETUP_SCREEN_NAME.MeetupInfoFragment.getValue());
                startActivity(intent);
            }
        });

        RelativeLayout continue_btn = (RelativeLayout) view.findViewById(R.id.continue_btn);
        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StorageUtility.saveDataInPreferences(mContext, AppConstants.SPKeys.MEETUP_INTRO_COMPLETED.getValue(), true);
                Intent intent = new Intent(getActivity(), MeetupActivity.class);
                intent.putExtra(AppConstants.IntentKeys.MEETUP_SCREEN_TYPE.getValue(), AppConstants.MEETUP_SCREEN_NAME.MeetupRadiusFragment.getValue());
                startActivity(intent);
            }
        });

    }
}