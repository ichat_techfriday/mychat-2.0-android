package com.chat.android.app.model.meetup;

import com.google.gson.annotations.SerializedName;


public class UpdateMeetupResponseData {

    @SerializedName("n")
    String n;
    @SerializedName("nModified")
    String modified;
    @SerializedName("ok")
    String ok;

    public UpdateMeetupResponseData() {
    }

    public UpdateMeetupResponseData(String n, String k, String modified) {
        this.n = n;
        this.ok = ok;
        this.modified = modified;
    }


    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }
}


