package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetSendingReasons implements Serializable {

    @SerializedName("SendingReasons")
    public SendingReasons sendingReasons;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public SendingReasons getSendingReasons() {
        return sendingReasons;
    }

    public void setSendingReasons(SendingReasons sendingReasons) {
        this.sendingReasons = sendingReasons;
    }

    public class SendingReasons {
        @SerializedName("AvailableOptions")
        public List<AvailableOption> availableOptions;
    }

    public class AvailableOption {
        @SerializedName("ID")
        public String iD;
        @SerializedName("Name")
        public String name;



        @Override
        public String toString() {
            return name;
        }
    }

    public class Root {
        @SerializedName("SendingReasons")
        public SendingReasons sendingReasons;
        @SerializedName("ErrorCode")
        public String errorCode;
        @SerializedName("ErrorDescription")
        public String errorDescription;
    }
}

