package com.chat.android.app.activity;


import static org.webrtc.ContextUtils.getApplicationContext;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.model.ApiRequestModel;
import com.chat.android.app.model.Contact_Info;
import com.chat.android.app.model.GetContactsModel;
import com.chat.android.app.model.GetPaymentMethods;
import com.chat.android.app.model.GetPriceOffer;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.FriendModel;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.StorageUtility;
import com.google.gson.Gson;

import java.util.List;

public class SendPaymentActivity extends AppCompatActivity implements INetworkResponseListener {

    private SendPaymentFragmentStep1 sendPaymentFragmentStep1;
    private SendPaymentFragmentStep3 sendPaymentFragmentStep3;
    private TextView toolbar_title;
    private ProgressDialog progressDialog;
    private boolean mSaved = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_payment);
        initViews();
    }

    private void initViews() {

        toolbar_title = findViewById(R.id.toolbar_title);
        progressDialog = new ProgressDialog(SendPaymentActivity.this);
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiRequestModel apiRequestModel = new ApiRequestModel(SessionManager.getInstance(this).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getApplicationContext()).getCashPlusToken());
        ApiCalls.getInstance(getApplicationContext()).GetContacts(getApplicationContext(), apiRequestModel, this);
        sendPaymentFragmentStep1 = new SendPaymentFragmentStep1();
        sendPaymentFragmentStep3 = new SendPaymentFragmentStep3();

    }

    public void OpenFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;
        FragmentManager manager = getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_layout, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    public void OpenFragmentwithData(Fragment fragment, String data) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;
        FragmentManager manager = getSupportFragmentManager();

        Bundle args = new Bundle();
        args.putString("file", data);
        fragment.setArguments(args);
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_layout, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        }else if (getSupportFragmentManager().getBackStackEntryCount() == 3){
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            String data = StorageUtility.getDataFromPreferences(getApplicationContext(), "send_pay", "");

            Gson gson = new Gson();
            FriendModel friendModel = gson.fromJson(data, FriendModel.class);
            Intent intent = new Intent(getApplicationContext(), ChatPageActivity.class);
            intent.putExtra("receiverUid", friendModel.getNumberInDevice());
            intent.putExtra("receiverName", friendModel.getFirstName());
            intent.putExtra("documentId", friendModel.get_id());
            intent.putExtra("Username", friendModel.getFirstName());
            intent.putExtra("Image", friendModel.getAvatarImageUrl());
            intent.putExtra("msisdn", friendModel.getMsisdn());
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void UpdateTitle(String text) {
        toolbar_title.setText(text);
    }

    private void CheckContact(List<Contact_Info> saveContactModelList) {
        if (SessionManager.getInstance(getApplicationContext()).getCashplusUserPhoneNumber() != null || !SessionManager.getInstance(getApplicationContext()).getCashplusUserPhoneNumber().isEmpty()) {
            String msdin = SessionManager.getInstance(getApplicationContext()).getCashplusUserPhoneNumber();
            for (Contact_Info saveContact : saveContactModelList) {
                if (saveContact.getMobile() != null) {
                    if (saveContact.getMobile().equals(msdin)) {
                        mSaved = true;
                        break;
                    }
                }
            }
            if (mSaved)
                OpenFragment(sendPaymentFragmentStep3);
            else
                OpenFragment(sendPaymentFragmentStep1);
        } else {
            Toast.makeText(getApplicationContext(), "Phone number not returned against this user", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    @Override
    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        progressDialog.dismiss();

        switch (responseForRequest) {

            case Constants.GetContacts:
                GetContactsModel getContactsModel = (GetContactsModel) body;
                if (getContactsModel.getErrorCode().equals("0")) {
                    CheckContact(getContactsModel.getSaveContactModels());
                } else
                    Toast.makeText(getApplicationContext(), getContactsModel.getErrorDescription(), Toast.LENGTH_SHORT).show();
                break;


        }
    }

}