package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class SetUserPasswordModel {
    @SerializedName("UserName")
    String userName;
    @SerializedName("token")
    String token;
    @SerializedName("NewPassword")
    String newPassword;

    public SetUserPasswordModel(String userName, String token, String newPassword) {
        this.userName = userName;
        this.token = token;
        this.newPassword = newPassword;
    }  public SetUserPasswordModel(String userName, String token) {
        this.userName = userName;
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
