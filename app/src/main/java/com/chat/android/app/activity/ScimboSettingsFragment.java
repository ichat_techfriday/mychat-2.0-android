package com.chat.android.app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chat.android.R;
import com.chat.android.app.adapter.UltimateSettingAdapter;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.ScimboSettingsModel;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.StorageUtility;
import com.chat.android.utils.TextUtils;
import com.google.android.material.snackbar.Snackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ScimboSettingsFragment extends Fragment implements View.OnClickListener {
    private TextView tvPinCode;
    private Context mContext;
    private View statusCircle, parentView;

    private ArrayList<ScimboSettingsModel> dataList = new ArrayList<>();
    private CheckBox cbSounds;
    private TextView username;
    private ImageView ivUserProfile;
    private static final String TAG = ScimboSettingsFragment.class.getSimpleName() + ">>";
    private SessionManager sessionManager;

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        MyLog.d("onAttach: ");
        mContext = context;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        parentView = view.findViewById(R.id.parent_view);
        RelativeLayout rlAccounts = view.findViewById(R.id.rl_settings_account_container);
        RelativeLayout rlChatWallpaper = view.findViewById(R.id.rl_chat_wallpaper_container);
        RelativeLayout rlTellFriend = view.findViewById(R.id.rl_settings_tell_friend_container);
        RelativeLayout rlChangePin = view.findViewById(R.id.rl_settings_change_pin_container);
        RelativeLayout rlLanguage = view.findViewById(R.id.rl_settings_language);
        RelativeLayout rlNotification = view.findViewById(R.id.rl_settings_notification_container);
        cbSounds = view.findViewById(R.id.cb_settings_sounds);
        statusCircle = view.findViewById(R.id.status_circle);
        username = view.findViewById(R.id.username);
        tvPinCode = view.findViewById(R.id.user_pin);
        ivUserProfile = (CircleImageView) view.findViewById(R.id.userprofile);
        RelativeLayout headerlayout = view.findViewById(R.id.header);
        View copyToClipBoard = view.findViewById(R.id.iv_copy_to_clipboard);

        rlAccounts.setOnClickListener(this);
        rlChatWallpaper.setOnClickListener(this);
        rlTellFriend.setOnClickListener(this);
        rlChangePin.setOnClickListener(this);
        rlLanguage.setOnClickListener(this);
        rlNotification.setOnClickListener(this);
        cbSounds.setOnClickListener(this);
        sessionManager = SessionManager.getInstance(mContext);

        if (sessionManager != null) {
            tvPinCode.setText(sessionManager.getPinCodeOfCurrentUser());
        }
        cbSounds.setChecked(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.SOUNDS.getValue(), true));
        username.setText(SessionManager.getInstance(mContext).getnameOfCurrentUser());
        String uname = username.getText().toString();

        username.setText(uname);
        headerlayout.setOnClickListener(this);
        copyToClipBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyChatUtils.copyToClipboard(mContext, tvPinCode.getText().toString());
            }
        });
        String pic = SessionManager.getInstance(mContext).getUserProfilePic();
        if (pic != null && !pic.isEmpty()) {
            AppUtils.loadImage(mContext, AppUtils.getValidProfilePath(pic), ivUserProfile, 150, R.drawable.ic_placeholder_black);
        }
        ivUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ImageZoom.class);
                intent.putExtra("ProfilePath", AppUtils.getProfileFilePath(mContext));
                startActivity(intent);
            }
        });
        setOnlineStatus(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.ONLINE_STATUS.getValue(), AppConstants.UserStatus.ONLINE.getValue()));
    }

    private void setOnlineStatus(String onlineStatus) {
        String status = MyChatUtils.parseStatusToLocal(mContext, onlineStatus);
        int color = R.color.dpBorderGreen, bg = R.drawable.online_status_circle;
        if (onlineStatus.equalsIgnoreCase(getResources().getString(R.string.busy))) {
            color = R.color.busy;
            bg = R.drawable.busy_status_circle;
        } else if (onlineStatus.equalsIgnoreCase(getResources().getString(R.string.offline))) {
            color = R.color.offline;
            bg = R.drawable.offline_status_circle;
        } else if (onlineStatus.equalsIgnoreCase(getResources().getString(R.string.away))) {
            color = R.color.away;
            bg = R.drawable.away_status_circle;
        }
        statusCircle.setBackgroundResource(bg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case  R.id.header:
                ActivityLauncher.launchUserProfile((Activity) mContext);
                break;
            case R.id.rl_settings_account_container:
                ActivityLauncher.launchUserProfile((Activity) mContext);
                break;
            case R.id.rl_chat_wallpaper_container:
                ActivityLauncher.launchChatWallpaper((Activity) mContext);
                break;
            case R.id.rl_settings_tell_friend_container:
                ActivityLauncher.launchSharePin((Activity) mContext);
                break;
            case R.id.rl_settings_change_pin_container:
                ActivityLauncher.launchChangePin((Activity) mContext);
                break;
            case R.id.rl_settings_language:
                Intent intent = new Intent(mContext, LanguageActivity.class);
                intent.putExtra(AppConstants.ISFROM, AppConstants.IS_FROM.SettingsFragment.toString());
                startActivity(intent);
                break;
            case R.id.cb_settings_sounds:
                showConfirmationForNotification(view);
                break;
        }
    }

    private void showConfirmationForNotification(View parentView) {
        String message = "";
        cbSounds.setChecked(!cbSounds.isChecked());
        if(!cbSounds.isChecked()) {
            message = getResources().getString(R.string.sounds_on);
        } else {
            message = getResources().getString(R.string.sounds_off);
        }
        final Snackbar snackBar = Snackbar.make(parentView, message, Snackbar.LENGTH_LONG).setActionTextColor(Color.RED);
        snackBar.setAction(getResources().getString(R.string.yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();
                cbSounds.setChecked(!cbSounds.isChecked());
                StorageUtility.saveDataInPreferences(mContext, AppConstants.SPKeys.SOUNDS.getValue(), cbSounds.isChecked());
            }
        });
        snackBar.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((CoreActivity)mContext).hideKeyboard();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sessionManager != null) {
            tvPinCode.setText(sessionManager.getPinCodeOfCurrentUser());
        }
        username.setText(SessionManager.getInstance(mContext).getnameOfCurrentUser());
        setOnlineStatus(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.ONLINE_STATUS.getValue(), AppConstants.UserStatus.ONLINE.getValue()));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        String eventName = event.getEventName();
        JSONObject jsonObject = new JSONObject();
        try {
            Object[] response = event.getObjectsArray();
            if (response.length > 0)
                jsonObject = (JSONObject) response[0];
            MyLog.d(TAG, eventName + "///" + "response in contacts is : " + jsonObject.toString());
        } catch (Exception e) {
            // e.printStackTrace();
        }
        String currentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        if (eventName.equalsIgnoreCase(SocketManager.EVENT_USER_ONLINE_STATUS)) {
            try {
                String userId = "", status = "";
                if (jsonObject.has("userId"))
                    userId = jsonObject.getString("userId");
                if (jsonObject.has("Status"))
                    status = jsonObject.getString("Status");
//                if (TextUtils.isEmpty(userId) || !currentUserId.equalsIgnoreCase(userId))
//                    return;
                setOnlineStatus(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.ONLINE_STATUS.getValue(), AppConstants.UserStatus.ONLINE.getValue()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            Object[] array = event.getObjectsArray();
            String path = "";
            try {
                JSONObject objects = new JSONObject(array[0].toString());
                Log.e(TAG, "objects" + objects);
                String from = objects.getString("from");
                String type = objects.getString("type");

                String removePhoto = null;
                if (objects.has("removePhoto")) {
                    removePhoto = objects.getString("removePhoto");
                }

                if (from.equalsIgnoreCase(SessionManager.getInstance(mContext).getCurrentUserID()) && type.equalsIgnoreCase("single")) {
                    if (!objects.getString("file").equals("") || removePhoto != null) {
                        path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                        final String finalPath = AppUtils.getValidProfilePath(path);
                        if (removePhoto.equalsIgnoreCase("yes")) {
                            SessionManager.getInstance(mContext).setUserProfilePic("");
                            ivUserProfile.setImageResource(R.drawable.ic_placeholder_black);
                        } else {
                            SessionManager.getInstance(mContext).setUserProfilePic(finalPath);
                            Glide
                                    .with(mContext)
                                    .load(/*profilePicPath*/AppUtils.getGlideURL(finalPath, mContext))
                                    .asBitmap()
                                    .error(R.drawable.ic_placeholder_black)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .into(new SimpleTarget<Bitmap>() {

                                        @Override
                                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                            ivUserProfile.setImageBitmap(arg0);
                                        }
                                    });
                        }
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }

        }
    }
}
