package com.chat.android.app.activity;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chat.android.R;


public class SendPaymentFragmentPaymentTransferred extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send_payment_payment_transferred, container, false);
        initViews(view);
        return view;
    }
    private void initViews(View view) {

    }
    private void setTitle() {
        ((SendPaymentActivity) getActivity()).UpdateTitle("");
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

}