package com.chat.android.app.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chat.android.R;



/**
 */
public class ViewHolderChat extends RecyclerView.ViewHolder {
    public TextView newMessage;
    public TextView storeName,newMessageCount,newMessageDate,tvTyping;
    public ImageView storeImage;
    public ImageView ivMsgType,mute_chatlist, tick,ivChatIcon;
    public RelativeLayout rlChat,chatlist;


    public ViewHolderChat(View view) {
        super(view);
        ivChatIcon= view.findViewById(R.id.iv_chat_icon);
        newMessage =  view.findViewById(R.id.newMessage);
        newMessageDate =  view.findViewById(R.id.newMessageDate);
        storeName =  view.findViewById(R.id.storeName);
        storeImage = view.findViewById(R.id.storeImage);
        newMessageCount =  view.findViewById(R.id.newMessageCount);
        tvTyping =  view.findViewById(R.id.tvTyping);

        mute_chatlist= view.findViewById(R.id.mute_chatlist);
        ivMsgType = view.findViewById(R.id.ivMsgType);
        tick = view.findViewById(R.id.tick);
        rlChat = view.findViewById(R.id.rlChat);
        chatlist = view.findViewById(R.id.chatlist);
    }
}
