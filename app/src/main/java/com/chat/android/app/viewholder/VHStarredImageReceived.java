package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.budiyev.android.circularprogressbar.CircularProgressBar;
import com.chat.android.R;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.app.widget.TopCropImageView;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;


/**
 * Created by CAS63 on 2/6/2017.
 */
public class VHStarredImageReceived extends RecyclerView.ViewHolder {

    public TextView datelbl, encryptionlabel, tvSecretLbl, senderName;
    public ImageView receiver_forward_image, userprofile;
    public TextView delete_senter_side_date_time_text;
    public TextView receiver_image_group_sender_name;
    public PorterShapeImageView receiver_side_imageview;
    public RelativeLayout receiver_image_pause_resume_layout;
    public RelativeLayout receiver_image_dowload_progress_Layout;
    public ProgressBar receiver_image_before_download_progress;
    public CircularProgressBar progress_bar;
    public RelativeLayout image_download_layout;
    public TextView image_download_text;
    public RelativeLayout receiver_image_inside_date_layout;
    public ImageView received_image_tick_icon;
    public TextView received_image_date_time;
    public ImageView receiver_image_normal_star_icon;
    public RelativeLayout receive_image_caption_layout;
    public TextView receive_image_caption_text;
    public ImageView receive_image_caption_tick_icon;
    public TextView receive_image_caption_date_time;
    public ImageView receiver_image_caption_star_icon;
    public RelativeLayout image_received_layout;
    public TopCropImageView receiver_top_crop_imageview;
    public TextView tvReadReceipt;

    public VHStarredImageReceived(View view) {
        super(view);
        senderName = view.findViewById(R.id.fromname);
        datelbl = view.findViewById(R.id.datelbl);
        userprofile = view.findViewById(R.id.userprofile);
        receiver_forward_image = view.findViewById(R.id.receiver_forward_image);
        delete_senter_side_date_time_text = view.findViewById(R.id.delete_senter_side_date_time_text);
        receiver_image_group_sender_name = view.findViewById(R.id.receiver_image_group_sender_name);
        receiver_image_pause_resume_layout = view.findViewById(R.id.receiver_image_pause_resume_layout);
        receiver_image_dowload_progress_Layout = view.findViewById(R.id.receiver_image_dowload_progress_Layout);
        receiver_image_before_download_progress = view.findViewById(R.id.receiver_image_before_download_progress);
        progress_bar = view.findViewById(R.id.progress_bar);
        image_download_layout = view.findViewById(R.id.image_download_layout);
        image_download_text = view.findViewById(R.id.image_download_text);
        receiver_image_inside_date_layout = view.findViewById(R.id.receiver_image_inside_date_layout);
        received_image_tick_icon = view.findViewById(R.id.received_image_tick_icon);
        received_image_date_time = view.findViewById(R.id.received_image_date_time);
        receiver_image_normal_star_icon = view.findViewById(R.id.receiver_image_normal_star_icon);
        receive_image_caption_layout = view.findViewById(R.id.receive_image_caption_layout);
        receive_image_caption_text = view.findViewById(R.id.receive_image_caption_text);
        receive_image_caption_tick_icon = view.findViewById(R.id.receive_image_caption_tick_icon);
        receive_image_caption_date_time = view.findViewById(R.id.receive_image_caption_date_time);
        receiver_image_caption_star_icon = view.findViewById(R.id.receiver_image_caption_star_icon);
        image_received_layout = view.findViewById(R.id.image_received_layout);

        receiver_side_imageview = view.findViewById(R.id.receiver_side_imageview);
        receiver_top_crop_imageview = view.findViewById(R.id.receiver_top_crop_imageview);
        encryptionlabel = view.findViewById(R.id.encryptionlabel);
        tvSecretLbl = view.findViewById(R.id.tvSecretLbl);
        tvReadReceipt = view.findViewById(R.id.text_read_receipt);
    }
}
