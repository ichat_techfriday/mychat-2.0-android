package com.chat.android.app.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.chat.android.R;
import com.chat.android.app.activity.LanguageSelectionActivity;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.MyChatUtils;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;


public class LanguageSelectionAdapter extends RecyclerView.Adapter<LanguageSelectionAdapter.ViewHolderItem> {
    List<LanguageSelectionActivity.LanguageModel> data = Collections.emptyList();

    private LayoutInflater inflater;
    private Context mContext;

    public LanguageSelectionAdapter(Context context, List<LanguageSelectionActivity.LanguageModel> data) {
        this.mContext = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @NotNull
    @Override
    public ViewHolderItem onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_language_selection, parent, false);
        return new ViewHolderItem(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolderItem mViewHolderItem, int position) {
        final LanguageSelectionActivity.LanguageModel model = data.get(position);
        mViewHolderItem.mLanguageEnglish.setText(model.getEnglishName());
        mViewHolderItem.mLanguageOriginal.setText(model.getOriginalName());
        if (model.isSelected()) {
            mViewHolderItem.mParent.setSelected(true);
            mViewHolderItem.mTick.setVisibility(View.VISIBLE);
        } else {
            mViewHolderItem.mParent.setSelected(false);
            mViewHolderItem.mTick.setVisibility(View.GONE);
        }
        mViewHolderItem.mParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyChatUtils.hideKeyboard((Activity) mContext);
                String intentKeyLanguage = model.getEnglishName();

                Intent intent = new Intent();
                ((Activity)mContext).setResult(Activity.RESULT_OK, intent);
                intent.putExtra(AppConstants.IntentKeys.EXTRA_KEY_LANGUAGE.getValue(), intentKeyLanguage);
                intent.putExtra(AppConstants.IntentKeys.EXTRA_KEY_LANGUAGE_CODE.getValue(), model.getLanguageCode());
                ((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        TextView mLanguageOriginal;
        TextView mLanguageEnglish;
        ImageView mTick;
        View mParent;
        ViewHolderItem(View itemView) {
            super(itemView);
            mParent   =            itemView.findViewById(R.id.parent_view);
            mLanguageOriginal = (TextView) itemView.findViewById(R.id.tv_language_orignal_name);
            mLanguageEnglish = (TextView) itemView.findViewById(R.id.tv_language_english_name);
            mTick     = (ImageView) itemView.findViewById(R.id.language_selection_tick);
        }
    }
}