

package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class SaveContactModel {

    @SerializedName("UserName")
    String userName;
    @SerializedName("token")
    String token;
    @SerializedName("ErrorCode")
    String ErrorCode;
    @SerializedName("ErrorDescription")
    String ErrorDescription;
    @SerializedName("Contact_Info")
    Contact_Info contact_info;

    public Contact_Info getContact_info() {
        return contact_info;
    }

    public void setContact_info(Contact_Info contact_info) {
        this.contact_info = contact_info;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public String getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(String errorCode) {
        ErrorCode = errorCode;
    }

    public String getErrorDescription() {
        return ErrorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        ErrorDescription = errorDescription;
    }
}

