package com.chat.android.app.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.SharedPreference;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.model.SCLoginModel;
import com.chat.android.core.scimbohelperclass.ScimboDialogUtils;
import com.chat.android.core.service.ServiceRequest;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.StorageUtility;
import com.chat.android.utils.TextUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class OnBoardingHomeParentActivity extends CoreActivity {
    protected SCLoginModel SCLoginModel;
    protected SessionManager sessionManager;
    private static final String TAG = OnBoardingHomeParentActivity.class.getSimpleName();
    protected static boolean SKIP_OTP_VERIFICATION = true;
    protected String code = "", otp = "", phoneNumberTxt = "";
    protected String userEmail = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    ServiceRequest.ServiceListener verifyCodeListener = new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
            MyLog.d("Loginrequest", response);

            //if (SCLoginModel == null)
            SCLoginModel = new SCLoginModel();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            SCLoginModel = gson.fromJson(response, SCLoginModel.class);
            Log.e("response", response);

            MyLog.d(TAG, "onCompleteListener: loginCount: " + SCLoginModel.getLoginCount());
            // success
            if (SCLoginModel.getErrNum().equals("0")) {

                setToken(SCLoginModel);

                SessionManager.getInstance(OnBoardingHomeParentActivity.this).IsnumberVerified(true);
//                SessionManager.getInstance(OnBoardingHomeParentActivity.this).Islogedin(true);
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setLoginCount(SCLoginModel.getLoginCount());
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setUserPinCode(SCLoginModel.getPinCode());
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setZainUser(SCLoginModel.getZainUser());

                StorageUtility.saveDataInPreferences(OnBoardingHomeParentActivity.this, AppConstants.SPKeys.ONLINE_STATUS.getValue(), SCLoginModel.getOnlineStatus());
                SharedPreferences shPref = getSharedPreferences("global_settings", MODE_PRIVATE);
                SharedPreferences.Editor et = shPref.edit();
                //Save my token
                SharedPreference.getInstance().save(OnBoardingHomeParentActivity.this, "securitytoken", SCLoginModel.getToken());
                et.putString("userId", SessionManager.getInstance(OnBoardingHomeParentActivity.this).getPhoneNumberOfCurrentUser());
                et.apply();
                Session session = new Session(OnBoardingHomeParentActivity.this);
                session.putgalleryPrefs("def");

                verifyCodeListenerCompleted();

            } else {

                try {
                    JSONObject ob = new JSONObject(response);
                    Log.e("ob", " ob" + ob);
                    String message = ob.getString("message");
                    showToast(OnBoardingHomeParentActivity.this, message);
                    SharedPreferences shPref = getSharedPreferences("global_settings",
                            MODE_PRIVATE);
                    SharedPreferences.Editor et = shPref.edit();
                    et.putString("userId", SessionManager.getInstance(OnBoardingHomeParentActivity.this).getPhoneNumberOfCurrentUser());
                    et.apply();
                } catch (JSONException e) {
                    e.printStackTrace();
                    showToast(OnBoardingHomeParentActivity.this, getResources().getString(R.string.response_catch_error));
                }

            }
        }

        @Override
        public void onErrorListener(int state) {
            hideProgressDialog();
        }
    };


    protected ServiceRequest.ServiceListener verifcationListener = new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {

            if (SCLoginModel == null)
                SCLoginModel = new SCLoginModel();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            try {
                SCLoginModel = gson.fromJson(response, SCLoginModel.class);
            } catch (Exception e) {
                e.printStackTrace();
                verifyPhoneError(getString(R.string.call_fail_error));
                return;
            }
            otp = SCLoginModel.getCode();
            Log.d(TAG, "onCompleteListener: otp = " + otp);
            if (SCLoginModel.getZainSmsSendingResponse() == 202)
                SKIP_OTP_VERIFICATION = true;
            if (SCLoginModel.getProfilePic() != null && !SCLoginModel.getProfilePic().isEmpty()) {
                String imgPath = SCLoginModel.getProfilePic() + "?id=" + AppUtils.eodMillis();
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setUserProfilePic(AppUtils.getValidProfilePath(imgPath));
            }

            //Save my token
            SharedPreference.getInstance().save(OnBoardingHomeParentActivity.this, "securitytoken", SCLoginModel.getToken());
            if (SCLoginModel.getStatus() != null && !SCLoginModel.getStatus().equalsIgnoreCase("")) {
                try {
                    String status = new String(Base64.decode(SCLoginModel.getStatus(), Base64.DEFAULT));
                    SessionManager.getInstance(OnBoardingHomeParentActivity.this).setcurrentUserstatus(status);
                } catch (Exception e) {
                    SessionManager.getInstance(OnBoardingHomeParentActivity.this).setcurrentUserstatus(SCLoginModel.getStatus());
                }


            } else {
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setcurrentUserstatus("Hey there! I am using "
                        + getResources().getString(R.string.app_name));
            }
            if (SCLoginModel.get_id() != null) {
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setCurrentUserID(SCLoginModel.get_id());
                SharedPreference.getInstance().save(OnBoardingHomeParentActivity.this, "userid", SCLoginModel.get_id());
                clearDataIfUserChanged(SCLoginModel.get_id());
            }
            if (SCLoginModel.getErrNum().equals("0")) {
                //If it is true mobile dont skip otp for email
//                if (SKIP_OTP_VERIFICATION)
//                    SessionManager.getInstance(OnBoardingHomeParentActivity.this).Islogedin(true);
                String message = "+" + code + phoneNumberTxt;
                if (!TextUtils.isEmpty(SCLoginModel.getMsisdn())) {
                    message = SCLoginModel.getMsisdn();
                }
                if (!TextUtils.isEmpty(phoneNumberTxt)) {
                    phoneNumberTxt = SCLoginModel.getPhNumber();
                    VerifyPhoneScreen.phoneNumberCh = phoneNumberTxt;
                }
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setPhoneNumberOfCurrentUser(message);

                Log.e(TAG, "SCLoginModel" + SCLoginModel.getName());

                if (SCLoginModel.getName() != null && !SCLoginModel.getName().isEmpty()) {
                    SessionManager.getInstance(OnBoardingHomeParentActivity.this).setnameOfCurrentUser(((SCLoginModel.getName())));
                }
                Log.e(TAG, "code " + code + ",, " + phoneNumberTxt);

                SessionManager sessionManager = SessionManager.getInstance(OnBoardingHomeParentActivity.this);
                sessionManager.setUserCountryCode("+" + code);
                sessionManager.setUserMobileNoWithoutCountryCode(phoneNumberTxt);
                sessionManager.setPhoneNumberOfCurrentUser(message);


                String msisdn = sessionManager.getPhoneNumberOfCurrentUser();
                String mobileNo = sessionManager.getUserMobileNoWithoutCountryCode();
                String countryCode = sessionManager.getUserCountryCode();


                MyLog.d(TAG, "msisdn : " + msisdn + ",, " + mobileNo + ",, " + countryCode);

                MyLog.d(TAG, "message : " + message);
                try {
                    JSONObject object = new JSONObject(response);
                    if (SessionManager.getInstance(OnBoardingHomeParentActivity.this).getTwilioMode().equalsIgnoreCase(
                            SessionManager.TWILIO_DEV_MODE)) {
                        SessionManager.getInstance(OnBoardingHomeParentActivity.this).setLoginOTP(object.getString("code"));
                    }
                    if (object.has("email_otp")) {
                        SessionManager.getInstance(OnBoardingHomeParentActivity.this).setLoginEmailOTP(object.getString("email_otp"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "", e);
                }
                verifyPhoneCompleted(message);
            } else if (SCLoginModel.getErrNum().equals("1")) {
                String error = SCLoginModel.getMessage();
                Log.e("verifcationListener", "error : " + error);
                verifyPhoneError(error);
            }
        }

        @Override
        public void onErrorListener(int state) {
            verifyPhoneError(getString(R.string.call_fail_error));
        }
    };
    protected ServiceRequest.ServiceListener emailverifcationListener = new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
            Log.e("emailverifcatio", " response" + response.toString());

            if (SCLoginModel == null)
                SCLoginModel = new SCLoginModel();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            try {
                SCLoginModel = gson.fromJson(response, SCLoginModel.class);
            } catch (Exception e) {
                e.printStackTrace();
                verifyEmailError(getString(R.string.call_fail_error));
                return;
            }
            otp = SCLoginModel.getCode();
            if (SCLoginModel.getZainSmsSendingResponse() == 202)
                SKIP_OTP_VERIFICATION = true;
            if (SCLoginModel.getProfilePic() != null && !SCLoginModel.getProfilePic().isEmpty()) {
                String imgPath = SCLoginModel.getProfilePic() + "?id=" + AppUtils.eodMillis();
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setUserProfilePic(AppUtils.getValidProfilePath(imgPath));
            }

            //Save my token
            SharedPreference.getInstance().save(OnBoardingHomeParentActivity.this, "securitytoken", SCLoginModel.getToken());
            if (SCLoginModel.getStatus() != null && !SCLoginModel.getStatus().equalsIgnoreCase("")) {
                try {
                    String status = new String(Base64.decode(SCLoginModel.getStatus(), Base64.DEFAULT));
                    SessionManager.getInstance(OnBoardingHomeParentActivity.this).setcurrentUserstatus(status);
                } catch (Exception e) {
                    SessionManager.getInstance(OnBoardingHomeParentActivity.this).setcurrentUserstatus(SCLoginModel.getStatus());
                }


            } else {
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setcurrentUserstatus("Hey there! I am using "
                        + getResources().getString(R.string.app_name));
            }
            if (SCLoginModel.get_id() != null) {
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setCurrentUserID(SCLoginModel.get_id());
                SharedPreference.getInstance().save(OnBoardingHomeParentActivity.this, "userid", SCLoginModel.get_id());
                clearDataIfUserChanged(SCLoginModel.get_id());
            }
            if (SCLoginModel.getErrNum().equals("0")) {
                //If it is true mobile dont skip otp for email
//                if (SKIP_OTP_VERIFICATION)
//                    SessionManager.getInstance(OnBoardingHomeParentActivity.this).Islogedin(true);
                String message = "+" + code + phoneNumberTxt;
                if (!TextUtils.isEmpty(SCLoginModel.getMsisdn())) {
                    message = SCLoginModel.getMsisdn();
                }
                if (!TextUtils.isEmpty(phoneNumberTxt)) {
                    phoneNumberTxt = SCLoginModel.getPhNumber();
                    VerifyPhoneScreen.phoneNumberCh = phoneNumberTxt;
                }
                SessionManager.getInstance(OnBoardingHomeParentActivity.this).setPhoneNumberOfCurrentUser(message);

                Log.e(TAG, "SCLoginModel" + SCLoginModel.getName());

                if (SCLoginModel.getName() != null && !SCLoginModel.getName().isEmpty()) {
                    SessionManager.getInstance(OnBoardingHomeParentActivity.this).setnameOfCurrentUser(((SCLoginModel.getName())));
                }
                Log.e(TAG, "code " + code + ",, " + phoneNumberTxt);

                SessionManager sessionManager = SessionManager.getInstance(OnBoardingHomeParentActivity.this);
                sessionManager.setUserCountryCode("+" + code);
                sessionManager.setUserMobileNoWithoutCountryCode(phoneNumberTxt);
                sessionManager.setPhoneNumberOfCurrentUser(message);
                sessionManager.setEmailCurrentUser(userEmail);

                try {
                    JSONObject object = new JSONObject(response);
                    if (SessionManager.getInstance(OnBoardingHomeParentActivity.this).getTwilioMode().equalsIgnoreCase(
                            SessionManager.TWILIO_DEV_MODE)) {
                        SessionManager.getInstance(OnBoardingHomeParentActivity.this).setLoginOTP(object.getString("code"));
                    }
                    if (object.has("email_otp")) {
                        SessionManager.getInstance(OnBoardingHomeParentActivity.this).setLoginEmailOTP(object.getString("email_otp"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, "", e);
                }
                verifyEmailCompleted(message);
            } else if (SCLoginModel.getErrNum().equals("1")) {
                String error = SCLoginModel.getMessage();
                Log.e("verifcationListener", "error : " + error);
                verifyEmailError(error);
            }
        }

        @Override
        public void onErrorListener(int state) {
            verifyEmailError(getString(R.string.call_fail_error));
        }
    };

    protected void clearDataIfUserChanged(String userId) {
        String prevLoginUserId = sessionManager.getPrevLoginUserId();
        if (!prevLoginUserId.equals("") && !prevLoginUserId.equalsIgnoreCase(userId)) {
            MessageDbController msgDb = CoreController.getDBInstance(OnBoardingHomeParentActivity.this);
            msgDb.deleteDatabase();

            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(OnBoardingHomeParentActivity.this);
            contactDB_sqlite.deleteDatabase();

            UserInfoSession userInfoSession = new UserInfoSession(OnBoardingHomeParentActivity.this);
            userInfoSession.clearData();

            Session session = new Session(OnBoardingHomeParentActivity.this);
            session.clearData();

        }
    }


    public void setToken(SCLoginModel SCLoginModel) {
        if (SCLoginModel.getToken() != null) {
            String tokenOriginal = SCLoginModel.getToken();
            String encodedToken = AppUtils.SHA256(tokenOriginal);
            MyLog.d(TAG, "onCompleteListener securityToken: " + tokenOriginal);

            sessionManager.setUserSecurityToken(tokenOriginal.replace("\n", ""));
            if (encodedToken != null)
                sessionManager.setUserSecurityTokenHash(encodedToken.replace("\n", ""));
        }
    }

    protected void verifyCodeListenerCompleted() {

    }

    protected void verifyPhoneCompleted(String message) {

    }

    protected void verifyEmailCompleted(String message) {

    }

    protected void verifyPhoneError(String message) {

    }protected void verifyEmailError(String message) {

    }
}
