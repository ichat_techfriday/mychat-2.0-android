package com.chat.android.app.activity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import android.widget.MediaController;
import android.widget.VideoView;
import com.chat.android.R;
import com.chat.android.app.widget.FullScreenMediaController;
import com.chat.android.core.CoreActivity;

import java.io.File;

public class FullScreenVideoActivity extends CoreActivity {

    private VideoView videoView;
    private static final String TAG = "FullScreenVideoActivity";
    private MediaController mediaController;
    private boolean paused=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        videoView = findViewById(R.id.videoView);

        String videoPath=getIntent().getStringExtra("filePath");

        Uri videoUri = Uri.fromFile(new File(videoPath));

        videoView.setVideoURI(videoUri);

        mediaController = new FullScreenMediaController(this);
        mediaController.setAnchorView(videoView);

        mediaController=new MediaController(this){
            @Override
            public void hide() {
                mediaController.show();
            }
            @Override
            public boolean dispatchKeyEvent(KeyEvent event){
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    super.hide();
                    ((Activity) getContext()).finish();
                    return true;
                }
                return super.dispatchKeyEvent(event);
            }
        };

        videoView.setMediaController(mediaController);
        videoView.setMediaController(new MediaController(this){
            public boolean dispatchKeyEvent(KeyEvent event)
            {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    pausePlayer();
                    finish();
                }
                return super.dispatchKeyEvent(event);
            }
        });
        //videoView.setZOrderOnTop(false);
        videoView.start();
    }

    @Override
    public void onBackPressed() {
        pausePlayer();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumePlayer();
    }

    private void resumePlayer() {
        if(paused){
            if(videoView!=null ){
                videoView.resume();
            }
        }
    }

    private void pausePlayer() {
        try {
            if (videoView != null) {
                if (videoView.isPlaying()) {
                    paused=true;
                    videoView.pause();
                }
                videoView.stopPlayback();

            }
        }
        catch (Exception e){
            Log.e(TAG, "onBackPressed: ",e );
        }
    }
}
