package com.chat.android.app.utils;

import android.media.MediaMetadataRetriever;
import android.os.Environment;

import com.chat.android.core.message.AudioMessage;
import com.chat.android.core.message.DocumentMessage;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.message.PictureMessage;
import com.chat.android.core.message.VideoMessage;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.File;

public class
ForwardFileUtil {
    private static final String TAG = "ForwardFileUtil";

    public static boolean isUploadedFile(File localFile, String serverPath, Object message,
                                         MessageItemChat forwardedItem,
                                         MessageItemChat newMsgItem, boolean isGroup, String receiverName) {


        MyLog.d(TAG, "isUploadedFile: directForward serverPath from db: " + serverPath);
        if (serverPath != null && !serverPath.isEmpty()) {
            try {


                if (localFile.exists()) {
                    SendMessageEvent sendMessageEvent = new SendMessageEvent();
                    String caption = forwardedItem.getCaption();
                    if (caption == null)
                        caption = "";
                    JSONObject msgObj = null;
                    if (isGroup) {
                        sendMessageEvent.setEventName(SocketManager.EVENT_GROUP);
                    } else {
                        sendMessageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                    }
                    switch (forwardedItem.getMessageType()) {

                        case (MessageFactory.picture + ""): {
                            PictureMessage pictureMessage = (PictureMessage) message;
                            if (isGroup) {
                                msgObj = (JSONObject) pictureMessage.getGroupPictureMessageObject(caption, newMsgItem.getMessageId(),
                                        AppUtils.parseInt(localFile.length() + ""),
                                        AppUtils.parseInt(forwardedItem.getChatFileWidth()),
                                        AppUtils.parseInt(forwardedItem.getChatFileHeight()), serverPath, receiverName);
                            } else {
                                msgObj = (JSONObject) pictureMessage.getPictureMessageObject(caption,
                                        newMsgItem.getMessageId(), AppUtils.parseInt(localFile.length() + ""),
                                        AppUtils.parseInt(forwardedItem.getChatFileWidth()),
                                        AppUtils.parseInt(forwardedItem.getChatFileHeight()), serverPath, false);
                            }
                          }
                            break;


                        case (MessageFactory.video + ""): {
                            VideoMessage videoMessage = (VideoMessage) message;
                            String thumbData = forwardedItem.getThumbnailData();
                            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                            mdr.setDataSource(localFile.getPath());
                            int videoHeight = AppUtils.parseInt(mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
                            int videoWidth = AppUtils.parseInt(mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
                            int originalFileSize = AppUtils.parseInt(forwardedItem.getFileSize());
                            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);


                            if (isGroup) {
                                msgObj = (JSONObject) videoMessage.getGroupVideoMessageObject(newMsgItem.getMessageId(),
                                        originalFileSize, thumbData, videoWidth, videoHeight, duration,
                                        serverPath, receiverName, caption);
                            } else {
                                msgObj = (JSONObject) videoMessage.getVideoMessageObject(newMsgItem.getMessageId(),
                                        originalFileSize, thumbData, videoWidth, videoHeight, duration, serverPath,
                                        caption, false, false, "");


                            }
                        }
                        break;


                        case (MessageFactory.document + ""): {
                            DocumentMessage documentMessage = (DocumentMessage) message;
                            String msgId = newMsgItem.getMessageId();
                            int fileSize = AppUtils.parseInt(localFile.length() + "");
                            String originalFileName = localFile.getName();
                            String localPath = localFile.getPath();
                            if (isGroup) {

                                msgObj = (JSONObject) documentMessage.getGroupDocumentMessageObject(msgId, fileSize,
                                        originalFileName, localPath, serverPath, receiverName);
                            } else {
                                msgObj = (JSONObject) documentMessage.getDocumentMessageObject(msgId, fileSize,
                                        originalFileName, localPath, serverPath, false);
                            }
                        }
                        break;


                        case (MessageFactory.audio + ""): {

                            AudioMessage audioMessage = (AudioMessage) message;
                            String audioDuration = forwardedItem.getDuration();
                            int audioFrom = forwardedItem.getaudiotype();
                            int fileSize = AppUtils.parseInt(localFile.length() + "");
                            String audioMsgId = newMsgItem.getMessageId();

                            if (isGroup) {
                                msgObj = (JSONObject) audioMessage.getGroupAudioMessageObject(audioMsgId, fileSize,
                                        audioDuration, serverPath
                                        , receiverName, audioFrom);

                            } else {
                                msgObj = (JSONObject) audioMessage.getAudioMessageObject(audioMsgId, fileSize,
                                        audioDuration, serverPath, audioFrom, false);
                            }
                        }
                            break;

                    }

                    if (msgObj == null)
                        return false;

                    if (!msgObj.has("payload")) {
                        msgObj.put("payload", "");
                    }

                    sendMessageEvent.setMessageObject(msgObj);
                    MyLog.d(TAG, "isUploadedFile directForward msgObj: " + msgObj);
                    EventBus.getDefault().post(sendMessageEvent);
                    return true;
                } else {
                    MyLog.d(TAG, "isUploadedFile: directForward local file not exits");
                    return false;
                }
            } catch (Exception e) {
                MyLog.e(TAG, "isUploadedFile directForward onClick: ", e);
            }


        }
        MyLog.d(TAG, "isUploadedFile: directForward server image not available");
        return false;
    }

    public static File getValidPictureFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            MyLog.d(TAG, "onClick: file not exists");
            String publicDirPath = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES).getAbsolutePath();
            String[] splitedFilePath = path.split("/");
            file = new File(publicDirPath, splitedFilePath[splitedFilePath.length - 1]);
            MyLog.d(TAG, "onClick: " + file.getAbsolutePath());
        }
        return file;
    }

    public static File getValidVideoFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            MyLog.d(TAG, "onClick: file not exists");
            String publicDirPath = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
            String[] splitedFilePath = path.split("/");
            file = new File(publicDirPath, splitedFilePath[splitedFilePath.length - 1]);
            MyLog.d(TAG, "onClick: " + file.getAbsolutePath());
        }
        return file;
    }

    public static File getValidDocumentFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            MyLog.d(TAG, "onClick: file not exists");
            String publicDirPath = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
            String[] splitedFilePath = path.split("/");
            file = new File(publicDirPath, splitedFilePath[splitedFilePath.length - 1]);
            MyLog.d(TAG, "onClick: " + file.getAbsolutePath());
        }
        return file;
    }
}
