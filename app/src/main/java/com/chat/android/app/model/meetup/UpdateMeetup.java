package com.chat.android.app.model.meetup;

import com.google.gson.annotations.SerializedName;

public class UpdateMeetup {

    @SerializedName("err")
    String error;
    @SerializedName("msg")
    String message;
    @SerializedName("data")
    UpdateMeetupResponseData data;

    public UpdateMeetup() {
    }

    public UpdateMeetup(String error, String message, UpdateMeetupResponseData data) {
        this.error = error;
        this.message = message;
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UpdateMeetupResponseData getData() {
        return data;
    }

    public void setData(UpdateMeetupResponseData data) {
        this.data = data;
    }
}
