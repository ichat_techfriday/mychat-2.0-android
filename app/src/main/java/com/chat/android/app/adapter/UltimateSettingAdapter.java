package com.chat.android.app.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.model.ScimboSettingsModel;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.StorageUtility;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

/*
*  * Created by CAS60 on 12/26/2016.
* */
public class UltimateSettingAdapter extends BaseAdapter {

    private ArrayList<ScimboSettingsModel> dataList;
    private TextView textView_settings;
    private CheckBox cbSounds;
    private Context context;
    private ImageView imageView_settings;

    public UltimateSettingAdapter(Context c, ArrayList<ScimboSettingsModel> dataList) {
        context = c;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ScimboSettingsModel model = dataList.get(position);
        if(model == null)
            return null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = null;
        if(model.getTitle().equalsIgnoreCase(context.getString(R.string.sound))) {
            row = inflater.inflate(R.layout.settings_list_view_cb, parent, false);
            cbSounds = row.findViewById(R.id.cb_settings_sounds);
            cbSounds.setChecked(StorageUtility.getDataFromPreferences(context, AppConstants.SPKeys.SOUNDS.getValue(), true));
            setCheckBoxClickListener(parent);
        } else {
            row = inflater.inflate(R.layout.settings_list_view, parent, false);
        }
        textView_settings = row.findViewById(R.id.textView_settings);
        imageView_settings = row.findViewById(R.id.imageView_settings);
        textView_settings.setText(dataList.get(position).getTitle());
        imageView_settings.setImageResource(dataList.get(position).getResourceId());
        imageView_settings.setColorFilter(ContextCompat.getColor(context, R.color.white));
        return row;
    }

    private void setCheckBoxClickListener(final View parentView) {
        cbSounds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CheckBox cbSounds = (CheckBox) view;
                String message = "";
                cbSounds.setChecked(!cbSounds.isChecked());
                if(!cbSounds.isChecked()) {
                    message = context.getResources().getString(R.string.sounds_on);
                } else {
                    message = context.getResources().getString(R.string.sounds_off);
                }
                final Snackbar snackBar = Snackbar.make(parentView, message, Snackbar.LENGTH_LONG).setActionTextColor(Color.RED);
                snackBar.setAction(context.getResources().getString(R.string.yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cbSounds.setChecked(!cbSounds.isChecked());
                        snackBar.dismiss();
                        StorageUtility.saveDataInPreferences(context, AppConstants.SPKeys.SOUNDS.getValue(), cbSounds.isChecked());
                    }
                });
                snackBar.show();
            }
        });
    }
}