package com.chat.android.app.activity;



import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.model.CashPlusLoginResponseModel;
import com.chat.android.app.model.CashplusLoginModel;
import com.chat.android.app.model.PasswordResetModel;
import com.chat.android.app.model.SetUserPasswordModel;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.SessionManager;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.StorageUtility;

public class SendPaymentLoginActivity extends AppCompatActivity implements INetworkResponseListener {
    private Button btn_account_save;
    private ImageView backimg_uprofile;
    private EditText password, userName;
    private ProgressDialog progressDialog;
    private String mToken;
    private String Username, model;
    private String receiverMsisdn;
    private TextView titleBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_payment_login);
        getSupportActionBar().hide();
        Intent intent = getIntent();
        Username = intent.getStringExtra("Username");
        receiverMsisdn = intent.getStringExtra("msisdn");
        model = intent.getStringExtra("model");

        progressDialog = new ProgressDialog(SendPaymentLoginActivity.this);
        progressDialog.setMessage("Please Wait ...");
        btn_account_save = findViewById(R.id.btn_account_save);
        backimg_uprofile = findViewById(R.id.backarrow_userprofile);
        password = findViewById(R.id.password_);
        userName = findViewById(R.id.et_account_phone_number);
        titleBar = findViewById(R.id.notification_actionbar_1);
        titleBar.setText("Send payment to " + Username);
       // userName.setText(LebononPhone);

        btn_account_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.btn_account_save) {
                    String pass = password.getText().toString();
                    if (!pass.equals("")) {
                        progressDialog.show();
                        CashplusLoginModel cashplusLoginModel = new CashplusLoginModel();
                        cashplusLoginModel.setPassword(pass);
                        cashplusLoginModel.setRefreshToken(true);
                        cashplusLoginModel.setUserName(SessionManager.getInstance(getApplicationContext()).getPhoneNumberOfCurrentUser());
                        Login(cashplusLoginModel);
                    }
                    ;

                } else if (view.getId() == R.id.backarrow_userprofile) {
                    finish();
                }

            }
        });
        backimg_uprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void Login(CashplusLoginModel cashplusLoginModel) {
        ApiCalls.getInstance(getApplicationContext()).Login(getApplicationContext(), cashplusLoginModel, this);

    }


    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        progressDialog.dismiss();

        switch (responseForRequest) {

            case Constants.Login:
                CashPlusLoginResponseModel cashPlusLoginResponseModel = (CashPlusLoginResponseModel) body;
                if (cashPlusLoginResponseModel.errorCode.equals("0")) {
                    mToken = cashPlusLoginResponseModel.getToken();
                    SessionManager.getInstance(getApplicationContext()).setCashPlusToken(mToken);
                    SessionManager.getInstance(getApplicationContext()).setCashplusUserName(Username);
                    SessionManager.getInstance(getApplicationContext()).setCashplusUserPhoneNumber(receiverMsisdn);
                    model = StorageUtility.getDataFromPreferences(SendPaymentLoginActivity.this, "send_pay", "");
                    ActivityLauncher.launchSendPayment(SendPaymentLoginActivity.this);
                } else
                    Toast.makeText(getApplicationContext(), cashPlusLoginResponseModel.getErrorDescription(), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}