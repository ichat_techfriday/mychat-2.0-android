package com.chat.android.app.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.BuildConfig;
import com.chat.android.R;
import com.chat.android.app.adapter.ShareIntentListAdapter;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.SessionManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.StorageUtility;

import java.util.ArrayList;
import java.util.List;

public class SharePinActivity extends CoreActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_pin);

        initViews();
    }

    private void initViews() {
        View back = findViewById(R.id.iv_back);
        TextView tvPinCode = findViewById(R.id.tv_pin_code);
        Button btnSharePin = findViewById(R.id.btn_share_pin);
        getSupportActionBar().hide();
        tvPinCode.setText(SessionManager.getInstance(SharePinActivity.this).getPinCodeOfCurrentUser());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnSharePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pinCode = SessionManager.getInstance(SharePinActivity.this).getPinCodeOfCurrentUser();


                String message = getResources().getString(R.string.deep_link_text) + " " + pinCode + "."+"\n \n"+BuildConfig.BASE_URL +"/?_branch_match_id=973897825708490612&utm_medium=marketing";
                //AppUtils.share(SharePinActivity.this, message);
                //showDialog(SharePinActivity.this, message);
                //share(SharePinActivity.this, message);
                shareViaIntent(SharePinActivity.this, message+"&&&ABC&&&123");
            }
        });
    }

    public void showDialog(Activity activity, final String msg) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.basiclistview);
        dialog.setTitle("Share with");

//        RelativeLayout mychat_layout = (RelativeLayout) dialog.findViewById(R.id.mychat_layout);
//        mychat_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(SharePinActivity.this, "mychat", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        RelativeLayout other_apps = (RelativeLayout) dialog.findViewById(R.id.other_apps);
//        other_apps.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AppUtils.share(SharePinActivity.this, msg);
//            }
//        });


        dialog.show();

    }

    public void share(Context context, String message) {
        final Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        List activities = context.getPackageManager().queryIntentActivities(sendIntent, 0);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Share with...");
        final ShareIntentListAdapter adapter = new ShareIntentListAdapter(SharePinActivity.this , R.layout.basiclistview, activities.toArray());
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ResolveInfo info = (ResolveInfo) adapter.getItem(which);
                if(info.activityInfo.packageName.contains("com.tech.ichat")) {
                    Toast.makeText(SharePinActivity.this, info.activityInfo.packageName, Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(SharePinActivity.this, info.activityInfo.packageName, Toast.LENGTH_SHORT).show();

                    SharePinActivity.this.startActivity(Intent.createChooser(sendIntent, "Share Text"));
                }
            }
        });
        builder.create().show();
    }

    public void shareViaIntent(Context context, CharSequence shareTitle) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read file from temp cache path of our app
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareTitle);
        //shareIntent.putExtra(Intent.EXTRA_STREAM, uri);

        final ArrayList<Intent> shareIntents;
        try {
            shareIntents = getExternalShareIntents(context, shareIntent);

            Intent chooserIntent = Intent.createChooser(shareIntent, shareTitle);
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, shareIntents.toArray(new Parcelable[]{}));
            context.startActivity(chooserIntent);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private ArrayList<Intent> getExternalShareIntents(Context context, Intent shareIntent) throws Throwable {
        final List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(shareIntent, 0);

        final ArrayList<Intent> shareIntents = new ArrayList<>();
        for (ResolveInfo resolveInfo : resolveInfos) {
            final String packageName = resolveInfo.activityInfo.packageName;

            if (packageName.equals(BuildConfig.APPLICATION_ID)) {
                continue;
            }

            Intent intent = new Intent(shareIntent);
            intent.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));
            shareIntents.add(intent);
        }

        return shareIntents;
    }
}
