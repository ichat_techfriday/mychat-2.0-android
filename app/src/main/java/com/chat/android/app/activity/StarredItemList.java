package com.chat.android.app.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.adapter.StarredMessageAdapter;
import com.chat.android.app.dialog.ChatLockPwdDialog;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.ChatLockPojo;
import com.chat.android.core.model.MessageItemChat;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by CAS63 on 2/3/2017.
 */
public class StarredItemList extends CoreActivity implements StarredMessageAdapter.ChatMessageItemClickListener {
    private static final String TAG = "StarredItemList";
    MessageItemChat myitems;
    StarredMessageAdapter mAdapter;
    private Boolean isGroupChat;
    ArrayList<MessageItemChat> items;
    ArrayList<MessageItemChat> dbItems;
    MessageDbController db;
    private String from, receiverDocumentID;
    RecyclerView rvdata;
    TextView tvTapStarred;
    String uniqueCurrentID;
    private LinearLayoutManager mLayoutManager;
    private SearchView searchView;
    private UserInfoSession userInfoSession;
    private Getcontactname getcontactname;
//    public TextView tv_starredmsg_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.starred_listview);
        db = CoreController.getDBInstance(this);
        userInfoSession = new UserInfoSession(StarredItemList.this);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.starred_messages));
        uniqueCurrentID = SessionManager.getInstance(this).getCurrentUserID();
        getcontactname = new Getcontactname(StarredItemList.this);
        actionBar.setDisplayHomeAsUpEnabled(true);
        from = SessionManager.getInstance(this).getCurrentUserID();
        rvdata = findViewById(R.id.rvstarred);
        tvTapStarred=findViewById(R.id.tv_tap_starred);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvdata.setLayoutManager(mLayoutManager);
        rvdata.setItemAnimator(new DefaultItemAnimator());
        rvdata.setHasFixedSize(true);
        dbItems = new ArrayList<>();
        mAdapter = new StarredMessageAdapter(StarredItemList.this, dbItems, StarredItemList.this.getSupportFragmentManager());
        updatestaredmessages_fromDB();
    }

    private void updatestaredmessages_fromDB() {
        dbItems = db.selectAllStarredMessages();
        if (dbItems.size() > 0 && mAdapter != null) {
            mAdapter.setItemClickListener(StarredItemList.this);
            rvdata.setAdapter(mAdapter);
            tvTapStarred.setVisibility(View.GONE);
            rvdata.setVisibility(View.VISIBLE);
        } else {
            tvTapStarred.setVisibility(View.VISIBLE);
            rvdata.setVisibility(View.GONE);
        }
    }

    public class CustomSearchView extends SearchView{
        public CustomSearchView(final Context context) {
            super(context);
            this.setIconifiedByDefault(true);
        }

        @Override
        public boolean dispatchKeyEventPreIme(KeyEvent event) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK &&
                    event.getAction() == KeyEvent.ACTION_UP) {
                this.onActionViewCollapsed();
            }
            return super.dispatchKeyEventPreIme(event);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.select_people_for_group, menu);
        MenuItem searchItem = menu.findItem(R.id.menuSearch);

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do whatever you need
                return true; // KEEP IT TO TRUE OR IT DOESN'T OPEN !!
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do whatever you need
                return true; // OR FALSE IF YOU DIDN'T WANT IT TO CLOSE!
            }
        });
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText.equals("") && newText.isEmpty()) {
                    searchView.clearFocus();
                }
                if (newText.length() > 0) {
                    mAdapter.getFilter().filter(newText);
                    if (StarredMessageAdapter.starredserach_result) {
                    } else {
                    }
                } else {
                    mAdapter.updateInfo(dbItems);
                }

                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.findItem(R.id.menuSearch).setVisible(true);
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.findItem(R.id.menuSearch).setVisible(false);
            }
        });


        searchView.setIconifiedByDefault(true);
        searchView.setQuery("", false);
        searchView.clearFocus();
        searchView.setIconified(true);

        AutoCompleteTextView searchTextView = searchView.findViewById(R.id.search_src_text);
        searchTextView.setTextColor(Color.WHITE);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0);
        } catch (Exception e) {
            MyLog.e(TAG,"",e);
        }

        MenuItemCompat.setActionView(searchItem, searchView);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(event.getKeyCode() == KeyEvent.KEYCODE_BACK)
        {  //do you back event work here
            searchView.onActionViewCollapsed();

        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View itemView, int position) {

        ChatLockPojo lockPojo = getChatLockdetailfromDB(position);
        if (SessionManager.getInstance(StarredItemList.this).getLockChatEnabled().equals("1")
                && lockPojo != null) {
            String stat = "", pwd = null;

            stat = lockPojo.getStatus();
            pwd = lockPojo.getPassword();

            MessageItemChat e = mAdapter.getItem(position);
            String docID = e.getMessageId();
            String[] ids = docID.split("-");
            String documentID = "";
            if (e.getMessageId().contains("-g")) {
                documentID = ids[0] + "-" + ids[1] + "-g";
            } else {
                documentID = ids[0] + "-" + ids[1];
            }
            if (stat.equals("1")) {
                openUnlockChatDialog(documentID, stat, pwd, position);
            } else {
                navigateTochatviewpage(position);
            }
        } else {
            navigateTochatviewpage(position);
        }

        mAdapter.stopAudioOnNavigate();
    }

    @Override
    public void onItemLongClick(View itemView, int position) {

    }


    private void navigateTochatviewpage(int position) {
        MessageItemChat msgItem = mAdapter.getItem(position);
        String msgId = msgItem.getMessageId();

        if (msgId.contains("-g")) {
            msgItem.setSenderName(msgItem.getGroupName());
        }

        getcontactname.navigateToChatViewpagewithmessageitems(msgItem, "star");
        finish();
    }


    private void openUnlockChatDialog(String docId, String status, String pwd, int position) {
        String convId = userInfoSession.getChatConvId(docId);
        ChatLockPwdDialog dialog = new ChatLockPwdDialog();
        dialog.setTextLabel1(getResources().getString(R.string.enter_your_pwd_label));
        dialog.setEditTextdata(getResources().getString(R.string.hint_newPwd));
        dialog.setforgotpwdlabel(getResources().getString(R.string.forgotChatpwd));
        dialog.setHeader(getResources().getString(R.string.unlock_chat));
        dialog.setButtonText(getResources().getString(R.string.unlock));
        Bundle bundle = new Bundle();
        bundle.putSerializable("MessageItem", mAdapter.getItem(position));
        if (docId.contains("-g")) {
            bundle.putString("convID", docId.split("-")[1]);
        } else {
            bundle.putString("convID", convId);
        }
        bundle.putString("status", "1");
        bundle.putString("pwd", pwd);
        bundle.putString("page", "chatlist");
        if (docId.contains("-g")) {
            bundle.putString("type", "group");
        } else {
            bundle.putString("type", "single");
        }
        bundle.putString("from", uniqueCurrentID);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "chatunLock");
    }


    private ChatLockPojo getChatLockdetailfromDB(int position) {

        MessageItemChat e = mAdapter.getItem(position);
        String docID = e.getMessageId();
        String[] id = docID.split("-");
        String documentID = "";
        String chatType = MessageFactory.CHAT_TYPE_SINGLE;
        if (e.getMessageId().contains("-g")) {
            documentID = id[0] + "-" + id[1] + "-g";
            chatType = MessageFactory.CHAT_TYPE_GROUP;
        } else {
            documentID = id[0] + "-" + id[1];
        }
        MessageDbController dbController = CoreController.getDBInstance(this);
        String convId = userInfoSession.getChatConvId(documentID);
        String receiverId = userInfoSession.getReceiverIdByConvId(convId);
        ChatLockPojo pojo = dbController.getChatLockData(receiverId, chatType);
        return pojo;
    }
}




