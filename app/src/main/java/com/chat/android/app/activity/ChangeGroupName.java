package com.chat.android.app.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconsPopup;


/**
 * Created by CAS60 on 11/29/2016.
 */
public class ChangeGroupName extends CoreActivity implements View.OnClickListener {

    private EmojiconEditText etGroupName;
    private AvnNextLTProRegTextView tvCount;
    private Button btnOk;
    private FrameLayout emoji;
    private EmojiconsPopup popup;
    EmojIconActions emojIcon;
    private String newGroupName;
    private String mCurrentUserId;
    private String mGroupId;
    private boolean status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_group_name);
        getSupportActionBar().hide();

        initView();
        initData();
    }

    private void initView() {
        etGroupName = findViewById(R.id.etGroupName);

        Typeface face = CoreController.getInstance().getAvnNextLTProRegularTypeface();
        etGroupName.setTypeface(face);
        etGroupName.addTextChangedListener(groupNameWatcher);

        tvCount = findViewById(R.id.tvCount);

        etGroupName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* remove the emojis in case edit text is clicked */
                status = !status;
                emoji.setVisibility(View.GONE);
            }
        });

        ImageButton ibSmiley = findViewById(R.id.ibSmiley);
        ibSmiley.setOnClickListener(this);

        ImageView ibBack = findViewById(R.id.ibBack);
        ibBack.setOnClickListener(this);

        Button btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(ChangeGroupName.this);

        btnOk = findViewById(R.id.btnOk);
        btnOk.setOnClickListener(ChangeGroupName.this);

        emoji = findViewById(R.id.emojicons);
        setEmojiconFragment(false);

        View rootView = findViewById(R.id.llTitle);
        emojIcon = new EmojIconActions(this, rootView, etGroupName, ibSmiley);

        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {

                MyLog.e("", "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                MyLog.e("", "Keyboard Closed!");

            }
        });
        emojIcon.setUseSystemEmoji(false);
       /* popup = new EmojiconsPopup(rootView, this);

        popup.setSizeForSoftKeyboard();


        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {

            }
        });


        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });


        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(github.ankushsachdeva.emojicon.emoji.Emojicon emojicon) {
                if (etGroupName == null || emojicon == null) {
                    return;
                }

                int start = etGroupName.getSelectionStart();
                int end = etGroupName.getSelectionEnd();
                if (start < 0) {
                    etGroupName.append(emojicon.getEmoji());
                } else {
                    etGroupName.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });


        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                etGroupName.dispatchKeyEvent(event);
            }
        });*/
    }

    TextWatcher groupNameWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence cs, int a, int b, int c) {
            // TODO Auto-generated method stub
            int countgroupname = 25 - cs.length();
            tvCount.setText(String.valueOf(countgroupname));
        }
    };

    private void setEmojiconFragment(boolean useSystemDefault) {
        getSupportFragmentManager()
                .beginTransaction()
                // .replace(com.layer.atlas.R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();
    }

    private void initData() {
        Bundle getBundle = getIntent().getExtras();
        mGroupId = getBundle.getString("GroupId", "");
        String mOldName = getBundle.getString("GroupName", "");

        etGroupName.setText(mOldName);

        mCurrentUserId = SessionManager.getInstance(ChangeGroupName.this).getCurrentUserID();
    }

    private Boolean internetcheck() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnCancel:
                Intent cancelIntent = new Intent();
                cancelIntent.putExtra("NameChanged", false);
                setResult(RESULT_CANCELED, cancelIntent);
                finish();
                break;

            case R.id.btnOk:
                newGroupName = etGroupName.getText().toString().trim();
                if (internetcheck()) {
                    if (newGroupName.equals("")) {
                        Toast.makeText(ChangeGroupName.this, getResources().getString(R.string.please_enter_group_name), Toast.LENGTH_SHORT).show();
                    } else {
                        if(AppUtils.isNetworkAvailable(this)) {
                            if(SocketManager.isConnected) {
                                performChangeGroupName(newGroupName);
                                btnOk.setEnabled(false);
                                btnOk.setClickable(false);
                            }
                            else{
                                Toast.makeText(this,getResources().getString(R.string.server_connection_problem_try_after_something),Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                        else{
                            Toast.makeText(this,getResources().getString(R.string.no_internet_connection),Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(ChangeGroupName.this, getResources().getString(R.string.check_your_internet_connection), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ibBack:
                finish();
                break;

            case R.id.ibSmiley:
                emojIcon.ShowEmojIcon();
                break;

        }

    }

    private void performChangeGroupName(String newGroupName) {

        long ts = Calendar.getInstance().getTimeInMillis();
        String msgId = mCurrentUserId + "-" + mGroupId + "-g-" + ts;

        try {
            JSONObject object = new JSONObject();
            object.put("groupType", SocketManager.ACTION_CHANGE_GROUP_NAME);
            object.put("from", mCurrentUserId);
            object.put("groupId", mGroupId);
            object.put("groupNewName", newGroupName);
            object.put("id", ts);
            object.put("toDocId", msgId);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GROUP);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP)) {

            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String groupAction = object.getString("groupType");

                if (groupAction.equalsIgnoreCase(SocketManager.ACTION_CHANGE_GROUP_NAME)) {
                    loadChangeGroupNameMessage(object);
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }

        }
    }

    private void loadChangeGroupNameMessage(JSONObject object) {
        try {
            String err = object.getString("err");
            if (err.equalsIgnoreCase("0")) {
                Intent okIntent = new Intent();
                okIntent.putExtra("NameChanged", true);
                okIntent.putExtra("newGroupName",newGroupName);
                setResult(RESULT_OK, okIntent);

                Intent intent=new Intent();
                intent.setAction("com.groupname.change");
                intent.putExtra("object",object.toString());
                sendBroadcast(intent);


                finish();
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(ChangeGroupName.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(ChangeGroupName.this);
    }

    @Override
    public void onBackPressed() {
        if (status) {
            /* Remove the emojicon */
            status = !status;
            emoji.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }
}
