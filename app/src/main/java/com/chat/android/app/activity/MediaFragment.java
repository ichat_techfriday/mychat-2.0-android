package com.chat.android.app.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.adapter.GridViewAdapter;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.ConstantMethods;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.ImageItem;
import com.chat.android.core.model.MessageItemChat;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by CAS56 on 3/10/2017.
 */
public class MediaFragment extends Fragment {
    MessageDbController db;
    private ArrayList<MessageItemChat> mChatData;
    private String docid;
    GridView grid;
    private GridViewAdapter gridlistadapter;
    private ArrayList<MessageItemChat> gridlist;
    private ArrayList<String> imgzoompath;

    private static final String TAG = "MediaFragment";
    public MediaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mediafragement_layout, container, false);
        Session session = new Session(getActivity());
        db = CoreController.getDBInstance(getActivity());
        grid = view.findViewById(R.id.grid);
        docid = session.getMediaDocid();

        mChatData = new ArrayList<>();
        gridlist = new ArrayList<MessageItemChat>();
        imgzoompath = new ArrayList<String>();
        loadFromDB();

        return view;
    }

    private void loadFromDB() {
        ArrayList<MessageItemChat> items;
        items = db.selectAllChatMessages(docid, ConstantMethods.getChatType(docid));
        mChatData.clear();
        mChatData.addAll(items);
        mediafile();

    }

    protected void mediafile() {
        for (int i = 0; i < mChatData.size(); i++) {
            String type = mChatData.get(i).getMessageType();
            int mtype = Integer.parseInt(type);
            if (MessageFactory.picture == mtype) {
                MessageItemChat msgItem = mChatData.get(i);
                if (msgItem.getImagePath() != null) {
                    String path = msgItem.getImagePath();
                    File file = new File(path);
                    if (file.exists()) {
                        imgzoompath.add(path);
                        gridlist.add(msgItem);
                    }
                } else if (msgItem.getChatFileLocalPath() != null) {
                    String path = msgItem.getChatFileLocalPath();
                    File file = new File(path);
                    if (file.exists()) {
                        imgzoompath.add(path);
                        gridlist.add(msgItem);
                    }
                }

            } else if (MessageFactory.video == mtype) {
                MessageItemChat msgItem = mChatData.get(i);
                if (msgItem.getVideoPath() != null) {
                    String path = msgItem.getVideoPath();
                    File file = new File(path);
                    if (file.exists()) {
                        imgzoompath.add(path);
                        gridlist.add(msgItem);
                    }
                } else if (msgItem.getChatFileLocalPath() != null) {
                    String path = msgItem.getChatFileLocalPath();
                    File file = new File(path);
                    if (file.exists()) {
                        imgzoompath.add(path);
                        gridlist.add(msgItem);
                    }
                }
            } else if (MessageFactory.audio == mtype) {

                    MessageItemChat msgItem = mChatData.get(i);
                    if (msgItem.getAudioPath() != null) {
                        String path = msgItem.getAudioPath();
                        File file = new File(path);
                        if (file.exists()) {
                            imgzoompath.add(path);
                            gridlist.add(msgItem);
                        }
                    } else if (msgItem.getChatFileLocalPath() != null) {
                        String path = msgItem.getChatFileLocalPath();
                        File file = new File(path);
                        if (file.exists()) {
                            imgzoompath.add(path);
                            gridlist.add(msgItem);
                        }
                    }

            }

        }
        gridlistadapter = new GridViewAdapter(getActivity(), R.layout.grid_item_layout, getData());
        grid.setAdapter(gridlistadapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                ImageItem item = (ImageItem) parent.getItemAtPosition(position);

                if (Integer.parseInt(gridlist.get(position).getMessageType()) == (MessageFactory.picture)) {
                    Intent intent = new Intent(getActivity(), ImageZoom.class);
                    intent.putExtra("from", "media");
                    intent.putExtra("image", imgzoompath.get(position));
                    startActivity(intent);
                }
                if (Integer.parseInt(gridlist.get(position).getMessageType()) == (MessageFactory.video)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(imgzoompath.get(position)));
                    intent.setDataAndType(Uri.parse(imgzoompath.get(position)), "video/*");
                    startActivity(intent);

                }
                if (Integer.parseInt(mChatData.get(position).getMessageType()) == (MessageFactory.audio)) {
                    try {
                        File file= new File(imgzoompath.get(position));
                        boolean isExists=file.exists();
                        Log.d(TAG, "onItemClick: "+isExists);
                        Uri uri=Uri.fromFile(file);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.setDataAndType(uri, "audio/*");
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(getContext(), getResources().getString(R.string.no_app_installed_to_play_this_audio), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        MyLog.e(TAG, "onItemClick: ", e);
                    }
                }


            }
        });

    }

    private ArrayList<ImageItem> getData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();
        //Bitmap audioBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_placeholder_message);
//        audioBitmap = ScimboImageUtils.getResizedBitmap(audioBitmap, 100);

        for (int i = 0; i < mChatData.size(); i++) {
            if (Integer.parseInt(mChatData.get(i).getMessageType()) == (MessageFactory.picture)) {
                MessageItemChat msgItem = mChatData.get(i);

                if (msgItem.getImagePath() != null) {
                    String path = msgItem.getImagePath();
                    File file = new File(path);
                    if (file.exists()) {
                        //Bitmap myBitmap = ScimboImageUtils.decodeBitmapFromFile(path, 100, 100);
                        imageItems.add(new ImageItem(path, "picture" + i, ""));
                    }
                } else if (msgItem.getChatFileLocalPath() != null) {
                    String path = msgItem.getChatFileLocalPath();
                    File file = new File(path);
                    if (file.exists()) {
                      //  Bitmap myBitmap = ScimboImageUtils.decodeBitmapFromFile(path, 100, 100);// BitmapFactory.decodeFile(path);
                        imageItems.add(new ImageItem(path, "picture" + i, ""));
                    }
                }
            }

            if (Integer.parseInt(mChatData.get(i).getMessageType()) == (MessageFactory.audio)) {
                MessageItemChat msgItem = mChatData.get(i);
                if (msgItem.getAudioPath() != null) {
                    String path = msgItem.getAudioPath();
                    File file = new File(path);
                    if (file.exists()) {
                        imageItems.add(new ImageItem(path, "audio" + i, msgItem.getDuration()));
                    }
                } else if (msgItem.getChatFileLocalPath() != null) {
                    String path = msgItem.getChatFileLocalPath();
                    File file = new File(path);
                    if (file.exists()) {
                        /*MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                        mdr.setDataSource(path);
                        String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        String setduration = getTimeString(AppUtils.parseLong(duration));*/
                        imageItems.add(new ImageItem(path, "audio" + i, msgItem.getDuration()));
                    }
                }
            }

            if (Integer.parseInt(mChatData.get(i).getMessageType()) == (MessageFactory.video)) {
                MessageItemChat msgItem = mChatData.get(i);
                if (msgItem.getChatFileLocalPath() != null) {
                    String path = msgItem.getChatFileLocalPath();
                    File file = new File(path);
                    if (file.exists()) {
                        try {
                            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                            mdr.setDataSource(path);
                            String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                            String setduration = getTimeString(Long.parseLong(duration));
                            //Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
                            imageItems.add(new ImageItem(path, "video" + i, setduration));
                        }
                        catch (Exception e){
                            Log.e("media", "getData: ",e );
                        }
                    }
                }
                 else if (msgItem.getVideoPath() != null) {
                    String path = msgItem.getVideoPath();
                    File file = new File(path);
                    if (file.exists()) {
                        MediaMetadataRetriever mdr = new MediaMetadataRetriever();
                        mdr.setDataSource(path);
                        String duration = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        String setduration = getTimeString(AppUtils.parseLong(duration));
                        //Bitmap thumbBmp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MICRO_KIND);
                        imageItems.add(new ImageItem(path, "video" + i, setduration));
                    }
                }
            }
        }
        return imageItems;
    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf

                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

}
