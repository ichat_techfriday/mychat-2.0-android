package com.chat.android.app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.utils.SharedPreference;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.interfaces.CallbackListener;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.StorageUtility;
import com.chat.android.utils.TextUtils;
import com.google.android.gms.auth.api.signin.internal.Storage;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LanguageActivity extends CoreActivity {
    View ivBack;
    TextView tvMainTitle;
    private ArrayList<LanguageModel> languageModels;
    RecyclerView mRecyclerView;
    private String isFrom;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            isFrom = bundle.getString(AppConstants.ISFROM, "");
        }

        initViews();
        populateList();
    }

    private void initViews() {
        getSupportActionBar().hide();
        ivBack = findViewById(R.id.iv_back);
        tvMainTitle = findViewById(R.id.tv_main_title);
        mRecyclerView = findViewById(R.id.recyclerView);
//        tvMainTitle.setText(getString(R.string.language));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void populateList() {
        languageModels = new ArrayList<>();
        List<String> originalLanguageNames = Arrays.asList(getResources().getStringArray(R.array.originalLanguageNamesDL));
        List<String> englishLanguageNames  = Arrays.asList(getResources().getStringArray(R.array.englishLanguageNamesDL));
        List<String> languageCodeNames  = Arrays.asList(getResources().getStringArray(R.array.languageCodesDL));

        for(int i = 0; i < originalLanguageNames.size(); i++) {
            String original = originalLanguageNames.get(i);
            String english  = englishLanguageNames.get(i);
            String code     = languageCodeNames.get(i);
            LanguageModel model = new LanguageModel(original, english, code);
            languageModels.add(model);
        }
        setAdapter(languageModels);
    }




    private void setAdapter(List<LanguageModel> languageModels) {

        LanguageSelectionAdapter mAdapter = new LanguageSelectionAdapter(this, languageModels, new CallbackListener() {
            @Override
            public void callback(String event) {

                if (!TextUtils.isEmpty(isFrom) && isFrom.equalsIgnoreCase(AppConstants.IS_FROM.SettingsFragment.toString())) {
                    Intent intent = new Intent(LanguageActivity.this, NewHomeScreenActivty.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                } else {
                    ActivityLauncher.launchVerifyPhoneScreen(LanguageActivity.this);
                    finishAffinity();
                }

            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }



    public class LanguageModel {
        String originalName;
        String englishName;
        String languageCode;

        LanguageModel(String originalName, String englishName, String languageCode) {
            this.originalName = originalName;
            this.englishName = englishName;
            this.languageCode = languageCode;
        }

        public String getOriginalName() {
            return originalName;
        }

        public void setOriginalName(String originalName) {
            this.originalName = originalName;
        }

        public String getEnglishName() {
            return englishName;
        }

        public void setEnglishName(String englishName) {
            this.englishName = englishName;
        }

        public String getLanguageCode() {
            return languageCode;
        }
    }

    class LanguageSelectionAdapter extends RecyclerView.Adapter<LanguageSelectionAdapter.ViewHolderItem> {
        List<LanguageActivity.LanguageModel> data = Collections.emptyList();

        private LayoutInflater inflater;
        private Context mContext;
        private CallbackListener callbackListener;
        public LanguageSelectionAdapter(Context context, List<LanguageActivity.LanguageModel> data, CallbackListener callbackListener) {
            this.mContext = context;
            inflater = LayoutInflater.from(context);
            this.data = data;
            this.callbackListener = callbackListener;
        }

        @Override
        public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.layout_language_selection_item, parent, false);
            return new ViewHolderItem(view);

        }

        @Override
        public void onBindViewHolder(final ViewHolderItem mViewHolderItem, final int position) {
            final LanguageActivity.LanguageModel model = data.get(position);
            mViewHolderItem.mLanguageEnglish.setText(model.getEnglishName());
            mViewHolderItem.mLanguageOriginal.setText(model.getOriginalName());
            mViewHolderItem.mParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String currentLanguage= StorageUtility.getDataFromPreferences(LanguageActivity.this,AppConstants.KEY_USER_LANGUAGE,"");

                    if(currentLanguage!=model.getLanguageCode()){
                        MyChatUtils.setLocale(mContext,model.getLanguageCode());
                        StorageUtility.saveDataInPreferences(LanguageActivity.this,AppConstants.KEY_USER_LANGUAGE,model.getLanguageCode());
                        StorageUtility.saveDataInPreferences(LanguageActivity.this, AppConstants.SPKeys.LANGUAGE.getValue(), true);

                        callbackListener.callback(model.getOriginalName());
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return data != null ? data.size() : 0;
        }


        class ViewHolderItem extends RecyclerView.ViewHolder {
            TextView mLanguageOriginal;
            TextView mLanguageEnglish;

            View mParent;
            ViewHolderItem(View itemView) {
                super(itemView);
                mParent   =            itemView.findViewById(R.id.parent_view);
                mLanguageOriginal = (TextView) itemView.findViewById(R.id.tv_language_orignal_name);
                mLanguageEnglish = (TextView) itemView.findViewById(R.id.tv_language_english_name);
            }
        }
    }
}