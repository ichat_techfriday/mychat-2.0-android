package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetPaymentMethods implements Serializable {
    @SerializedName("PaymentMethods")
    public List<PaymentMethods> PaymentMethods;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;

    public List<GetPaymentMethods.PaymentMethods> getPaymentMethods() {
        return PaymentMethods;
    }

    public void setPaymentMethods(List<GetPaymentMethods.PaymentMethods> paymentMethods) {
        PaymentMethods = paymentMethods;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public class PaymentMethods implements Serializable {
        @SerializedName("PM_LABEL")
        public String PM_LABEL;
        @SerializedName("PM_ID")
        public String PM_ID;
        @SerializedName("PM_TYPE")
        public int PM_TYPE;
        @SerializedName("PayingCurrencies")
        public List<PayingCurrency> payingCurrency;

        public String getPM_LABEL() {
            return PM_LABEL;
        }

        public void setPM_LABEL(String PM_LABEL) {
            this.PM_LABEL = PM_LABEL;
        }

        public String getPM_ID() {
            return PM_ID;
        }

        public void setPM_ID(String PM_ID) {
            this.PM_ID = PM_ID;
        }

        public int getPM_TYPE() {
            return PM_TYPE;
        }

        public void setPM_TYPE(int PM_TYPE) {
            this.PM_TYPE = PM_TYPE;
        }

        public List<PayingCurrency> getPayingCurrency() {
            return payingCurrency;
        }

        public void setPayingCurrency(List<PayingCurrency> payingCurrency) {
            this.payingCurrency = payingCurrency;
        }

        @Override
        public String toString() {
            return PM_LABEL;
        }
    }

    public class PayingCurrency implements Serializable {
        @SerializedName("PayingCurrency")
        public String PayingCurrency;
        @SerializedName("PayoutCurrencies")
        public List<String> payoutCurrencies;

        public String getPayingCurrency() {
            return PayingCurrency;
        }

        public void setPayingCurrency(String payingCurrency) {
            PayingCurrency = payingCurrency;
        }

        public List<String> getPayoutCurrencies() {
            return payoutCurrencies;
        }

        public void setPayoutCurrencies(List<String> payoutCurrencies) {
            this.payoutCurrencies = payoutCurrencies;
        }
        @Override
        public String toString() {
            return PayingCurrency;
        }
    }


}
