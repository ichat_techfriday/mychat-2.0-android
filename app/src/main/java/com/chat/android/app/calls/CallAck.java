package com.chat.android.app.calls;

import android.content.Context;

import com.chat.android.app.utils.MyLog;
import com.chat.android.core.message.BaseMessage;
import com.chat.android.core.message.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 */
public class CallAck extends BaseMessage implements Message {
    public CallAck(Context context) {
        super(context);
    }

    private static final String TAG = "CallAck";
    @Override
    public Object getMessageObject(String to, String doc_id, Boolean isSecretchat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("msgIds", new JSONArray(Arrays.asList(id)));
            object.put("doc_id", doc_id);
            object.put("status", "3");
        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }
        return object;

    }

    @Override
    public Object getGroupMessageObject(String to, String payload, String groupName) {
        return null;
    }

    public Object getMessageObject(String to, String doc_id, String status, String _id) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("msgIds", new JSONArray(Arrays.asList(_id)));
            object.put("doc_id", doc_id);
            object.put("status", status);
        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }
        return object;

    }

}
