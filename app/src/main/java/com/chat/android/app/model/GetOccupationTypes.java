package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetOccupationTypes implements Serializable {
    @SerializedName("OccupationTypes")
    public List<OccupationType> occupationTypes;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public List<OccupationType> getOccupationTypes() {
        return occupationTypes;
    }

    public void setOccupationTypes(List<OccupationType> occupationTypes) {
        this.occupationTypes = occupationTypes;
    }

    public class OccupationType{
        @SerializedName("ID")
        public int iD;
        @SerializedName("Name")
        public String name;
        @Override
        public String toString() {
            return name;
        }
    }




}
