package com.chat.android.app.model.meetup;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LocationNearByMeetups {
    @SerializedName("type")
    String type;
    @SerializedName("coordinates")
    ArrayList<Double> coordinates;

    public LocationNearByMeetups() {
    }

    public LocationNearByMeetups(String type, ArrayList<Double> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }
}
