package com.chat.android.app.activity;


import static com.chat.android.utils.DialogUtils.dismiss;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.model.ApiRequestModel;
import com.chat.android.app.model.CashPlusLoginResponseModel;
import com.chat.android.app.model.GetBankPayoutLocations;
import com.chat.android.app.model.GetCities;
import com.chat.android.app.model.GetDestinations;
import com.chat.android.app.model.GetIDCardTypes;
import com.chat.android.app.model.GetOccupationTypes;
import com.chat.android.app.model.PasswordResetModel;
import com.chat.android.app.model.SetUserPasswordModel;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.FriendModel;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.StorageUtility;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;


public class SendPaymentFragmentStep1 extends Fragment implements View.OnClickListener, INetworkResponseListener {

    private ImageButton icNext;
    private EditText spinnerDestination;
    private EditText spinnerMode;
    private ProgressDialog progressDialog;
    private EditText etPayingAmount;
    private TextView tvPayingCurrency;
    private GetDestinations.AvailableTransferMethod selectedTransferMethod;
    private List<GetDestinations.AvailablePayingDetail> selectedPayingDetails = new ArrayList<>();
    private List<GetDestinations.Country> destinationsList = new ArrayList<>();
    private List<GetDestinations.Country> filtereddestinationsList = new ArrayList<>();
    private GetDestinations.AvailablePayingDetail selectedExchangeBlock;
    private GetDestinations.Country selectedCountry;
    GetBankPayoutLocations.AvailableBanks selectedBank;
    private List<GetDestinations.AvailableTransferMethod> availableTransferMethods = new ArrayList<>();
    private GetIDCardTypes.IDCardType idCardType;
    private GetOccupationTypes.OccupationType occupationType;
    private AutoCompleteTextView spinnerIdentity;
    private AutoCompleteTextView spinnerOccupation;
    private List<GetOccupationTypes.OccupationType> OccupationList = new ArrayList<>();
    private List<GetIDCardTypes.IDCardType> cardList = new ArrayList<>();
    private int mNetworkCalls = 0;
    private EditText spinnerCity;
    private EditText spinnerBankLocation;
    AlertDialog.Builder builderSingle;
    AlertDialog ad;
    String json;
    private Button cancel_btn;
    private List<GetBankPayoutLocations.AvailableBanks> bankPayoutList = new ArrayList<>();
    private GetBankPayoutLocations.AvailableBanks selectedBankLocation;
    private GetCities.AllCity selectedCity= new GetCities.AllCity();
    ArrayAdapter<GetDestinations.Country> filter_adapter;
    ArrayAdapter<GetBankPayoutLocations.AvailableBanks> bank_filter_adapter;
    ArrayAdapter<GetOccupationTypes.OccupationType> occupation_filter_adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send_payment_step1, container, false);
        initViews(view);
        initValues();
        return view;
    }

    private void initViews(View view) {

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCancelable(false);
        spinnerCity = view.findViewById(R.id.city);
        cancel_btn = view.findViewById(R.id.cancel_btn);
        spinnerBankLocation = view.findViewById(R.id.spinnerBankLocation);
        icNext = view.findViewById(R.id.icNext);
        spinnerDestination = view.findViewById(R.id.spinnerDestination);
        spinnerMode = view.findViewById(R.id.spinnerMode);
        etPayingAmount = view.findViewById(R.id.etPayingAmount);
        tvPayingCurrency = view.findViewById(R.id.tv_payingCurrency);
        etPayingAmount.addTextChangedListener(textWatcher);
        spinnerOccupation = view.findViewById(R.id.spinnerOccupation);
        spinnerIdentity = view.findViewById(R.id.spinnerIdentity);
        spinnerBankLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBankPayoutSpinner();
            }
        });

        icNext.setOnClickListener(this);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_exit_dialog();
            }
        });
    }

    public void show_exit_dialog(){
        final CustomAlertDialog dialog = new CustomAlertDialog();
        String msg = "Do you want to cancel the transaction?";
        dialog.setMessage(msg);

        dialog.setPositiveButtonText("Yes");
        dialog.setNegativeButtonText("No");

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                //String data = SendPaymentActivity.model;
                String model = StorageUtility.getDataFromPreferences(getContext(), "send_pay", "");
                Gson gson = new Gson();
                FriendModel friendModel = gson.fromJson(model, FriendModel.class);
                Intent intent = new Intent(getContext(), ChatPageActivity.class);
                intent.putExtra("receiverUid", friendModel.getNumberInDevice());
                intent.putExtra("receiverName", friendModel.getFirstName());
                intent.putExtra("documentId", friendModel.get_id());
                intent.putExtra("Username", friendModel.getFirstName());
                intent.putExtra("Image", friendModel.getAvatarImageUrl());
                intent.putExtra("msisdn", friendModel.getMsisdn());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getFragmentManager(), "exit transaction");
    }

    private void initValues() {
        spinnerDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                json="";
                setDestination();
            }
        });
        spinnerMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMode();
            }
        });
        spinnerOccupation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOccupation();

            }
        });
        spinnerIdentity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setIdentity();

            }
        });
        ApiRequestModel apiRequestModel = new ApiRequestModel(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getContext()).getCashPlusToken());
        progressDialog.show();
        ApiCalls.getInstance(getContext()).GetDestinations(getContext(), apiRequestModel, this);
        ApiCalls.getInstance(getContext()).GetIDCardTypes(getContext(), apiRequestModel, this);
        ApiCalls.getInstance(getContext()).GetOccupationTypes(getContext(), apiRequestModel, this);
    }

    private void setDestination() {


        builderSingle = new AlertDialog.Builder(getContext());

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.listview_dialog, null);
        final ListView listview = (ListView) dialogView.findViewById(R.id.select_dialog_listview);
        final TextView title = (TextView)dialogView.findViewById(R.id.title);
        title.setText("Select Destinations");
        final EditText search_edit = (EditText)dialogView.findViewById(R.id.search_edit);
        search_edit.setHint("Enter destination");
        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                if (!editable.toString().trim().equals("")) {

                    Gson gson = new Gson();
                    json = gson.toJson(destinationsList);
                    JSONArray newArray = new JSONArray();
                    try {
                        JSONArray jsonArray = new JSONArray(json);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject currentElement = jsonArray.getJSONObject(i);
                            String country = currentElement.getString("countryName");
                            String country_lwr = country.toLowerCase();
                            String query = editable.toString().toLowerCase();
                            if (country_lwr.contains(query)) {
                                newArray.put(currentElement);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Type listType = new TypeToken<List<GetDestinations.Country>>() {
                    }.getType();
                    List<GetDestinations.Country> myModelList = new Gson().fromJson(String.valueOf(newArray), listType);

                    filter_adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, myModelList);
                    listview.setAdapter(filter_adapter);
                }
            }
        });
        final ImageView search_icon = (ImageView)dialogView.findViewById(R.id.search_icon);
        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setVisibility(View.GONE);
                search_edit.setVisibility(View.VISIBLE);
                search_icon.setVisibility(View.GONE);
            }
        });
        final ArrayAdapter<GetDestinations.Country> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, destinationsList);

        listview.setAdapter(adapter);
        builderSingle.setView(dialogView);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GetDestinations.Country country;
                if (json!=null && !json.equals("")){
                    country = filter_adapter.getItem(i);
                }else {
                    country = adapter.getItem(i);
                }
                selectedCountry = country;

                RequestLocations();
                spinnerDestination.setEnabled(true);

                spinnerMode.setText("");
                etPayingAmount.setText("");
                selectedExchangeBlock = null;
                selectedPayingDetails = null;
                tvPayingCurrency.setText("");
                spinnerDestination.setText(country.countryName);
                availableTransferMethods = country.transferMethods.availableTransferMethods;
                json = "";
                ad.dismiss();
            }
        });
//        final ArrayAdapter<GetDestinations.Country> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, destinationsList);
//
//        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                GetDestinations.Country country = adapter.getItem(which);
//                selectedCountry = country;
//
//                RequestLocations();
//                spinnerDestination.setEnabled(true);
//
//                spinnerMode.setText("");
//                etPayingAmount.setText("");
//                selectedExchangeBlock = null;
//                selectedPayingDetails = null;
//                tvPayingCurrency.setText("");
//                spinnerDestination.setText(country.countryName);
//                availableTransferMethods = country.transferMethods.availableTransferMethods;
//                dismiss();
//            }
//        });
        ad = builderSingle.show();


    }

    private void RequestLocations() {
        progressDialog.show();

        ApiRequestModel apiRequestModel2 = new ApiRequestModel(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getContext()).getCashPlusToken(), selectedCountry.countryCode, true);
        ApiCalls.getInstance(getContext()).GetBanksPayoutLocations(getContext(), apiRequestModel2, this);

    }
    private void setBankPayoutSpinner() {

        builderSingle = new AlertDialog.Builder(getContext());

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.listview_dialog, null);
        final ListView listview = (ListView) dialogView.findViewById(R.id.select_dialog_listview);
        final TextView title = (TextView)dialogView.findViewById(R.id.title);
        title.setText("Select Bank Payout Location");
        final EditText search_edit = (EditText)dialogView.findViewById(R.id.search_edit);
        search_edit.setHint("Select BankPayout Location");
        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                if (!editable.toString().trim().equals("")) {

                    Gson gson = new Gson();
                    json = gson.toJson(bankPayoutList);
                    JSONArray newArray = new JSONArray();
                    try {
                        JSONArray jsonArray = new JSONArray(json);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject currentElement = jsonArray.getJSONObject(i);
                            String country = currentElement.getString("BankName");
                            String country_lwr = country.toLowerCase();
                            String query = editable.toString().toLowerCase();
                            if (country_lwr.contains(query)) {
                                newArray.put(currentElement);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Type listType = new TypeToken<List<GetBankPayoutLocations.AvailableBanks>>() {
                    }.getType();
                    List<GetBankPayoutLocations.AvailableBanks> myModelList = new Gson().fromJson(String.valueOf(newArray), listType);

                    bank_filter_adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, myModelList);
                    listview.setAdapter(bank_filter_adapter);
                }
            }
        });
        final ImageView search_icon = (ImageView)dialogView.findViewById(R.id.search_icon);
        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setVisibility(View.GONE);
                search_edit.setVisibility(View.VISIBLE);
                search_icon.setVisibility(View.GONE);
            }
        });
        final ArrayAdapter<GetBankPayoutLocations.AvailableBanks> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, bankPayoutList);

        listview.setAdapter(adapter);
        builderSingle.setView(dialogView);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GetBankPayoutLocations.AvailableBanks country;
                if (json!=null && !json.equals("")){
                    country = bank_filter_adapter.getItem(i);
                }else {
                    country = adapter.getItem(i);
                }
                selectedBankLocation = country;
                spinnerBankLocation.setText(selectedBankLocation.bankName);
                json = "";
                ad.dismiss();
            }
        });
//        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
//        builderSingle.setTitle("Select BankPayout Location");
//        final ArrayAdapter<GetBankPayoutLocations.AvailableBanks> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, bankPayoutList);
//
//        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                selectedBankLocation = adapter.getItem(which);
//                spinnerBankLocation.setText(selectedBankLocation.bankName);
//                dismiss();
//            }
//        });
        ad = builderSingle.show();

    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (!etPayingAmount.getText().toString().equals("")) {

                double payingamount = Double.parseDouble(etPayingAmount.getText().toString());

                for (GetDestinations.AvailablePayingDetail element : selectedPayingDetails) {
                    double max = element.maximum_Sending_Amount;
                    double min = element.minimum_Sending_Amount;

                    if (payingamount > min && payingamount < max) {
                        selectedExchangeBlock = element;
                        tvPayingCurrency.setText(selectedExchangeBlock.paying_Currency);
                        break;
                    } else if (payingamount == min || payingamount == max) {
                        selectedExchangeBlock = element;
                        tvPayingCurrency.setText(selectedExchangeBlock.paying_Currency);
                        break;
                    } else {
                        tvPayingCurrency.setText("");
                        selectedExchangeBlock = null;
                    }
                }
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };


    private void setMode() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select Mode");
        final ArrayAdapter<GetDestinations.AvailableTransferMethod> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, availableTransferMethods);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedTransferMethod = adapter.getItem(which);
                selectedPayingDetails = selectedTransferMethod.availablePayingDetails;
                selectedExchangeBlock = null;
                etPayingAmount.setText("");
                tvPayingCurrency.setText("");
                dismiss();
            }
        });
        builderSingle.show();


    }


    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle();

    }

    private void setTitle() {
        ((SendPaymentActivity) getActivity()).UpdateTitle("Payment Details");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.icNext:

//                SendPaymentFragmentStep2 sendPaymentFragmentStep2 = new SendPaymentFragmentStep2();
//                Bundle b = new Bundle();
//                b.putSerializable("selectedCountry", selectedCountry);
//                sendPaymentFragmentStep2.setArguments(b);
//                ((SendPaymentActivity) getActivity()).OpenFragment(sendPaymentFragmentStep2);
//                break;

                if (!spinnerCity.getText().toString().equals("")) {

                    if (selectedBankLocation != null) {
                        selectedCity.cityName = spinnerCity.getText().toString();
                        SendPaymentFragmentStep4 sendPaymentFragmentStep4 = new SendPaymentFragmentStep4();
                        Bundle b1 = new Bundle();
                        b1.putSerializable("selectedCountry", selectedCountry);
                        b1.putSerializable("selectedBankLocation", selectedBankLocation);
                        b1.putSerializable("selectedCity", selectedCity);
                        sendPaymentFragmentStep4.setArguments(b1);
                        ((SendPaymentActivity) getActivity()).OpenFragment(sendPaymentFragmentStep4);
                    } else {

                        Toast.makeText(getContext(), "Select Payout Location", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.field_must_not_be_empty), Toast.LENGTH_SHORT).show();

                }
                break;
        }
    }

    private void setIdentity() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select Identity");
        final ArrayAdapter<GetIDCardTypes.IDCardType> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, cardList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                idCardType = adapter.getItem(which);
                spinnerIdentity.setText(idCardType.name);
                dismiss();
            }
        });
        builderSingle.show();


    }

    private void setOccupation() {
        builderSingle = new AlertDialog.Builder(getContext());

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.listview_dialog, null);
        final ListView listview = (ListView) dialogView.findViewById(R.id.select_dialog_listview);
        final TextView title = (TextView)dialogView.findViewById(R.id.title);
        title.setText("Select Occupation");
        final EditText search_edit = (EditText)dialogView.findViewById(R.id.search_edit);
        search_edit.setHint("Select Occupation");
        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                if (!editable.toString().trim().equals("")) {

                    Gson gson = new Gson();
                    json = gson.toJson(OccupationList);
                    JSONArray newArray = new JSONArray();
                    try {
                        JSONArray jsonArray = new JSONArray(json);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject currentElement = jsonArray.getJSONObject(i);
                            String country = currentElement.getString("Name");
                            String country_lwr = country.toLowerCase();
                            String query = editable.toString().toLowerCase();
                            if (country_lwr.contains(query)) {
                                newArray.put(currentElement);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Type listType = new TypeToken<List<GetOccupationTypes.OccupationType>>() {
                    }.getType();
                    List<GetOccupationTypes.OccupationType> myModelList = new Gson().fromJson(String.valueOf(newArray), listType);

                    occupation_filter_adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, myModelList);
                    listview.setAdapter(occupation_filter_adapter);
                }
            }
        });
        final ImageView search_icon = (ImageView)dialogView.findViewById(R.id.search_icon);
        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setVisibility(View.GONE);
                search_edit.setVisibility(View.VISIBLE);
                search_icon.setVisibility(View.GONE);
            }
        });
        final ArrayAdapter<GetOccupationTypes.OccupationType> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, OccupationList);

        listview.setAdapter(adapter);
        builderSingle.setView(dialogView);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (json!=null && !json.equals("")){
                    occupationType = occupation_filter_adapter.getItem(i);
                }else {
                    occupationType = adapter.getItem(i);
                }
                spinnerOccupation.setText(occupationType.name);
                json = "";
                ad.dismiss();
            }
        });



//        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
//        builderSingle.setTitle("Select Occupation");
//        final ArrayAdapter<GetOccupationTypes.OccupationType> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, OccupationList);
//        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                occupationType = adapter.getItem(which);
//                spinnerOccupation.setText(occupationType.name);
//                dismiss();
//            }
//        });
       ad = builderSingle.show();


    }


    @Override
    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        if (mNetworkCalls == 2) {
            progressDialog.dismiss();
            mNetworkCalls = 0;
        }
        switch (responseForRequest) {
            case Constants
                    .Destinations:
                if (status) {

                    GetDestinations destinations = (GetDestinations) body;
                    destinationsList = destinations.getCountries().countries;
                    mNetworkCalls++;
                } else
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;
            case Constants
                    .IDTypes:
                if (status) {

                    GetIDCardTypes idCardTypes = (GetIDCardTypes) body;
                    if (idCardTypes.getErrorCode().equals("0")) {
                        cardList = idCardTypes.getIdCardTypeList();
                        mNetworkCalls++;

                    } else
                        Toast.makeText(getContext(), idCardTypes.getErrorDescription(), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;
            case Constants
                    .OccupationTypes:
                if (status) {

                    GetOccupationTypes occupationTypes = (GetOccupationTypes) body;
                    if (occupationTypes.getErrorCode().equals("0")) {
                        OccupationList = occupationTypes.getOccupationTypes();
                        mNetworkCalls++;

                    } else
                        Toast.makeText(getContext(), occupationTypes.getErrorDescription(), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;

            case Constants.BankPayoutLocations:
                if (status) {
                    progressDialog.dismiss();
                    spinnerBankLocation.setText("");
                    GetBankPayoutLocations bankPayoutLocations = (GetBankPayoutLocations) body;
                    bankPayoutList = bankPayoutLocations.getBanks().allBanks;
                    if (bankPayoutList.size()>0){
                        spinnerBankLocation.setEnabled(true);
                    }else {
                        spinnerBankLocation.setEnabled(false);
                    }
                } else
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;

        }
    }
}