package com.chat.android.app.utils;

import android.util.Log;

import com.chat.android.BuildConfig;


/**
 * Created by user134 on 2/27/2018.
 */

public class MyLog {

private static final  boolean LOG_ENABLED=true;
    public static void  d(String TAG,String msg){
        if(LOG_ENABLED && BuildConfig.DEBUG)
            Log.d(TAG, ""+msg);
    }
    public static void  d(String TAG,String msg,Throwable t){
        if(LOG_ENABLED && BuildConfig.DEBUG)
            Log.d(TAG, ""+msg,t);
    }

    public static void  e(String TAG,String msg,Throwable throwable){
        throwable.printStackTrace();
        if(LOG_ENABLED && BuildConfig.DEBUG)
            Log.e(TAG, ""+msg,throwable);
    }
    public static void  e(String TAG,String msg){
        if(LOG_ENABLED && BuildConfig.DEBUG)
            Log.e(TAG, ""+msg);
    }

    public static void d(String msg){
        if(LOG_ENABLED && BuildConfig.DEBUG)
            Log.d("MyLog", ""+msg);
    }

    public static void  e(String TAG, Exception e){
        if(LOG_ENABLED && BuildConfig.DEBUG) {
             e.printStackTrace();
        }
    }

}
