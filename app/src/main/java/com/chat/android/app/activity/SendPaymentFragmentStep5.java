package com.chat.android.app.activity;


import static com.chat.android.utils.DialogUtils.dismiss;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.model.ApiRequestModel;
import com.chat.android.app.model.CashplusUpdateUserProfile;
import com.chat.android.app.model.Contact_Info;
import com.chat.android.app.model.GetBankBranches;
import com.chat.android.app.model.GetBankPayoutLocations;
import com.chat.android.app.model.GetCashPayoutLocations;
import com.chat.android.app.model.GetCities;
import com.chat.android.app.model.GetDestinations;
import com.chat.android.app.model.GetIDCardTypes;
import com.chat.android.app.model.GetPaymentMethods;
import com.chat.android.app.model.GetPriceOffer;
import com.chat.android.app.model.GetProcessResponse;
import com.chat.android.app.model.GetSendingReasons;
import com.chat.android.app.model.NationalityModel;
import com.chat.android.app.model.ProcessTransactionModel;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.FriendModel;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.StorageUtility;
import com.chat.android.utils.TextUtils;

import com.google.gson.Gson;
//import com.shockwave.pdfium.PdfDocument;
//import com.shockwave.pdfium.PdfiumCore;

//import org.apache.poi.hpsf.Section;
//import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFRow;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.poifs.filesystem.POIFSFileSystem;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Font;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.wp.usermodel.Paragraph;
import org.w3c.dom.Document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class SendPaymentFragmentStep5 extends Fragment implements View.OnClickListener, INetworkResponseListener {


    private Button icNext;
    private ImageButton icBack;
    Button cancel_btn;
    boolean selected = false;
    private GetPaymentMethods.PaymentMethods selectedMethod;
    private String payingAmount;
    private String purpose;
    private String contactID;
    private com.chat.android.app.model.GetPriceOffer GetPriceOffer;
    private ProgressDialog progressDialog;
    private List<NationalityModel> nationalityModelList = new ArrayList<>();
    private List<GetSendingReasons.AvailableOption> SendingList = new ArrayList<>();
    private Contact_Info contactDetails;
    ImageView iv;
    private TextView tvPayingAmount;
    private TextView tvPayingCurrency;
    private TextView tvTotalCharges;
    private TextView tvTotalPaying;
    private TextView tvExchangeRate;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send_payment_step5, container, false);
        initViews(view);
        return view;
    }
    private void setPriceValues(GetPriceOffer priceOffer) {

        GetPriceOffer = priceOffer;
        tvTotalPaying.setText(String.valueOf(priceOffer.getPrice_Offer().total_Paying));
        tvTotalCharges.setText(String.valueOf(priceOffer.getPrice_Offer().total_Charges));
        tvExchangeRate.setText(String.valueOf(priceOffer.getPrice_Offer().exchange_Rate));
        tvPayingCurrency.setText(String.valueOf(priceOffer.getPrice_Offer().paying_Currency));
        tvPayingAmount.setText(String.valueOf(priceOffer.getPrice_Offer().paying_Amount));
        tvExchangeRate.setText(String.valueOf(priceOffer.getPrice_Offer().exchange_Rate));

    }

    private void initViews(View view) {
        icNext = view.findViewById(R.id.icNext);
        icBack = view.findViewById(R.id.icBack);

        iv = (ImageView)view.findViewById(R.id.imageView);
        tvPayingCurrency = view.findViewById(R.id.tvPayingCurrency);
        tvTotalCharges = view.findViewById(R.id.tvTotalCharges);
        tvTotalPaying = view.findViewById(R.id.tvTotalPaying);
        tvExchangeRate = view.findViewById(R.id.tvExchangeRate);
        tvPayingAmount = view.findViewById(R.id.tvPayingAmount);

        icNext.setOnClickListener(this);
        icBack.setOnClickListener(this);
        Bundle b = getArguments();
        GetPriceOffer = (GetPriceOffer) b.getSerializable("GetPriceOffer");
        selectedMethod = (GetPaymentMethods.PaymentMethods) b.getSerializable("selectedMethod");
        contactDetails = (Contact_Info) b.getSerializable("contactDetails");
        contactID = contactDetails.getContact_ID();
        payingAmount = (String) b.getString("payingAmount");
        purpose = (String) b.getString("purpose");
        setPriceValues(GetPriceOffer);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCancelable(false);
        cancel_btn = view.findViewById(R.id.cancel_btn);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_exit_dialog();
            }
        });

    }


    public void show_exit_dialog() {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        String msg = "Do you want to cancel the transaction?";
        dialog.setMessage(msg);

        dialog.setPositiveButtonText("Yes");
        dialog.setNegativeButtonText("No");

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                //String data = SendPaymentActivity.model;
                String data = StorageUtility.getDataFromPreferences(getContext(), "send_pay", "");

                Gson gson = new Gson();
                FriendModel friendModel = gson.fromJson(data, FriendModel.class);
                Intent intent = new Intent(getContext(), ChatPageActivity.class);
                intent.putExtra("receiverUid", friendModel.getNumberInDevice());
                intent.putExtra("receiverName", friendModel.getFirstName());
                intent.putExtra("documentId", friendModel.get_id());
                intent.putExtra("Username", friendModel.getFirstName());
                intent.putExtra("Image", friendModel.getAvatarImageUrl());
                intent.putExtra("msisdn", friendModel.getMsisdn());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getFragmentManager(), "exit transaction");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle();
    }

    private void setTitle() {
        ((SendPaymentActivity) getActivity()).UpdateTitle("Transaction Amount");
    }


    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }



    private void viewPDF(String mStringPDF) {

        String path = Environment.getExternalStorageDirectory() + "/" + "MyFirstApp/";
        //String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String fullName = path + "sample.pdf";
        File file = new File(fullName);
        byte[] pdfAsBytes = Base64.decode(mStringPDF, 0);

        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);

            fOut.write(pdfAsBytes);
            fOut.flush();
            fOut.close();

            viewPdf(file);

        } catch (IOException e) {
            Log.i("error", e.getLocalizedMessage());
        }
    }

    // Method for opening a pdf file
    private void viewPdf(File file) {

        PdfViewerFragment pdfViewerFragment = new PdfViewerFragment();
        ((SendPaymentActivity) getActivity()).OpenFragmentwithData(pdfViewerFragment, String.valueOf(file));

//        Uri path = Uri.fromFile(file);
//        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
//        pdfIntent.setDataAndType(path, "application/pdf");
//        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        try {
//            startActivity(pdfIntent);
//        } catch (ActivityNotFoundException e) {
//            Toast.makeText(getActivity(), "Can't read pdf file", Toast.LENGTH_SHORT).show();
//        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.icNext:

                    ProcessTransactionModel processTransactionModel = new ProcessTransactionModel();
                    processTransactionModel.setUserName(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser());
                    processTransactionModel.setToken(SessionManager.getInstance(getContext()).getCashPlusToken());
                    processTransactionModel.setCustomerMessage("Hello");
                    processTransactionModel.setPayingAmount(payingAmount == null || payingAmount.isEmpty() ? 0.0 : Double.parseDouble(payingAmount));
                    processTransactionModel.setPaymentMethodType(selectedMethod.getPM_TYPE());
                    processTransactionModel.setPayingCurrencyCode(GetPriceOffer.getPrice_Offer().paying_Currency);
                    processTransactionModel.setPaymentMethod_ID(selectedMethod.PM_ID == null || selectedMethod.PM_ID.isEmpty() ? 0 : Integer.parseInt(selectedMethod.PM_ID));
                    processTransactionModel.setPayoutCurrencyCode(GetPriceOffer.getPrice_Offer().payout_Currency);
                    processTransactionModel.setPromoCode("");
                    processTransactionModel.setTransferReason(purpose);
                    processTransactionModel.setPayoutAgentID(0);
                    processTransactionModel.setPayoutAmount(0.0);
                    processTransactionModel.setContactID(contactID == null || contactID.isEmpty() ? 0 : Integer.parseInt(contactID));
                    progressDialog.show();
                    ApiCalls.getInstance(getActivity()).ProcessTransaction(getContext(), processTransactionModel, this);

                    break;
            case R.id.icBack:
                ((SendPaymentActivity) getActivity()).onBackPressed();
                break;

        }


    }

    @Override
    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        progressDialog.dismiss();
        switch (responseForRequest) {
            case Constants.ProcessTransactions:
                if (status) {

                    GetProcessResponse getProcessResponse = (GetProcessResponse) body;
                    if (getProcessResponse.getErrorCode().equals("0")) {
                        viewPDF(getProcessResponse.getPDF_Data());

                    } else
                        Toast.makeText(getContext(), getProcessResponse.getErrorDescription(), Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                //        ((SendPaymentActivity) getActivity()).OpenFragment(new SendPaymentFragmentPaymentTransferred());
                break;
        }
    }
}