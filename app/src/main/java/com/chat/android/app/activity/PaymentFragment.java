package com.chat.android.app.activity;

import static com.chat.android.utils.AppConstants.KEY_USER_LANGUAGE;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chat.android.R;
import com.chat.android.app.model.CashPLusUpdateUserProfileResponse_Model;
import com.chat.android.app.model.CashPlusLoginResponseModel;
import com.chat.android.app.model.CashplusLoginModel;
import com.chat.android.app.model.RegisterMobileUser;
import com.chat.android.app.model.RegisterCashPlusResponse;
import com.chat.android.app.model.PasswordResetModel;
import com.chat.android.app.model.VerificationPinResponseModel;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.ScimboSettingsModel;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.StorageUtility;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;


public class PaymentFragment extends Fragment implements View.OnClickListener, INetworkResponseListener {

    private TextView tvPinCode;
    private Context mContext;
    private View statusCircle, parentView;

    private ArrayList<ScimboSettingsModel> dataList = new ArrayList<>();
    private TextView username;
    private TextView tvTitleAccount;
    private TextView tvsubTitleAccount;
    private ImageView ivUserProfile;
    private ImageView verifiedimg;
    private ImageView moveArrow;
    private static final String TAG = ScimboSettingsFragment.class.getSimpleName() + ">>";
    private SessionManager sessionManager;
    private    RelativeLayout rlVerifyAccount;
    private FloatingActionButton ibPaymenticon;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        MyLog.d("onAttach: ");
        mContext = context;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    private void initViews(View view) {

        parentView = view.findViewById(R.id.parent_view);
        rlVerifyAccount = view.findViewById(R.id.rl_verify_account_container);
        statusCircle = view.findViewById(R.id.status_circle);
        username = view.findViewById(R.id.username);
        tvPinCode = view.findViewById(R.id.user_pin);
        ivUserProfile = (CircleImageView) view.findViewById(R.id.userprofile);
        moveArrow = view.findViewById(R.id.moveArrow);
        verifiedimg = view.findViewById(R.id.verifiedimg);
        tvTitleAccount = view.findViewById(R.id.tvTitleAccount);
        tvsubTitleAccount = view.findViewById(R.id.tvsubTitleAccount);
        ibPaymenticon = view.findViewById(R.id.ibPaymenticon);
        RelativeLayout headerlayout = view.findViewById(R.id.header);
        View copyToClipBoard = view.findViewById(R.id.iv_copy_to_clipboard);
        ibPaymenticon.setOnClickListener(this);

        rlVerifyAccount.setOnClickListener(this);
        sessionManager = SessionManager.getInstance(mContext);

        if (sessionManager != null) {
            tvPinCode.setText(sessionManager.getPinCodeOfCurrentUser());
        }
        username.setText(SessionManager.getInstance(mContext).getnameOfCurrentUser());
        String uname = username.getText().toString();

        username.setText(uname);
        headerlayout.setOnClickListener(this);
        copyToClipBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyChatUtils.copyToClipboard(mContext, tvPinCode.getText().toString());
            }
        });
        String pic = SessionManager.getInstance(mContext).getUserProfilePic();
        if (pic != null && !pic.isEmpty()) {
            AppUtils.loadImage(mContext, AppUtils.getValidProfilePath(pic), ivUserProfile, 150, R.drawable.ic_placeholder_black);
        }
        ivUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ImageZoom.class);
                intent.putExtra("ProfilePath", AppUtils.getProfileFilePath(mContext));
                startActivity(intent);
            }
        });
        setOnlineStatus(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.ONLINE_STATUS.getValue(), AppConstants.UserStatus.ONLINE.getValue()));
        if (sessionManager.getCashplusVerified()) {
            verifiedimg.setVisibility(View.VISIBLE);
            tvTitleAccount.setText(getString(R.string.account_is_verified));
            tvsubTitleAccount.setText(getString(R.string.your_account_is_verified_and_your_profile_is_updated));
            moveArrow.setVisibility(View.GONE);
            rlVerifyAccount.setClickable(false);
        }

        String selectedLang = StorageUtility.getDataFromPreferences(requireActivity(), KEY_USER_LANGUAGE, "en");
        if (selectedLang.equals("ar")) {
            LinearLayout llVerifyAlertContainer = requireActivity().findViewById(R.id.ll_verify_alert_container);
            llVerifyAlertContainer.setHorizontalGravity(Gravity.END);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {
        String eventName = event.getEventName();
        JSONObject jsonObject = new JSONObject();
        try {
            Object[] response = event.getObjectsArray();
            if (response.length > 0)
                jsonObject = (JSONObject) response[0];
            MyLog.d(TAG, eventName + "///" + "response in contacts is : " + jsonObject.toString());
        } catch (Exception e) {
            // e.printStackTrace();
        }
        String currentUserId = SessionManager.getInstance(mContext).getCurrentUserID();
        if (eventName.equalsIgnoreCase(SocketManager.EVENT_USER_ONLINE_STATUS)) {
            try {
                String userId = "", status = "";
                if (jsonObject.has("userId"))
                    userId = jsonObject.getString("userId");
                if (jsonObject.has("Status"))
                    status = jsonObject.getString("Status");
//                if (TextUtils.isEmpty(userId) || !currentUserId.equalsIgnoreCase(userId))
//                    return;
                setOnlineStatus(StorageUtility.getDataFromPreferences(mContext, AppConstants.SPKeys.ONLINE_STATUS.getValue(), AppConstants.UserStatus.ONLINE.getValue()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            Object[] array = event.getObjectsArray();
            String path = "";
            try {
                JSONObject objects = new JSONObject(array[0].toString());
                Log.e(TAG, "objects" + objects);
                String from = objects.getString("from");
                String type = objects.getString("type");

                String removePhoto = null;
                if (objects.has("removePhoto")) {
                    removePhoto = objects.getString("removePhoto");
                }

                if (from.equalsIgnoreCase(SessionManager.getInstance(mContext).getCurrentUserID()) && type.equalsIgnoreCase("single")) {
                    if (!objects.getString("file").equals("") || removePhoto != null) {
                        path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                        final String finalPath = AppUtils.getValidProfilePath(path);
                        if (removePhoto.equalsIgnoreCase("yes")) {
                            SessionManager.getInstance(mContext).setUserProfilePic("");
                            ivUserProfile.setImageResource(R.drawable.ic_placeholder_black);
                        } else {
                            SessionManager.getInstance(mContext).setUserProfilePic(finalPath);
                            Glide
                                    .with(mContext)
                                    .load(/*profilePicPath*/AppUtils.getGlideURL(finalPath, mContext))
                                    .asBitmap()
                                    .error(R.drawable.ic_placeholder_black)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .into(new SimpleTarget<Bitmap>() {

                                        @Override
                                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                            ivUserProfile.setImageBitmap(arg0);
                                        }
                                    });
                        }
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }

        }
    }
    @Override
    public void onResume() {
        super.onResume();
        username.setText(SessionManager.getInstance(mContext).getnameOfCurrentUser());
        if (sessionManager.getCashplusVerified()) {
            verifiedimg.setVisibility(View.VISIBLE);
            tvTitleAccount.setText(getString(R.string.account_is_verified));
            tvsubTitleAccount.setText(getString(R.string.your_account_is_verified_and_your_profile_is_updated));
            moveArrow.setVisibility(View.GONE);
            rlVerifyAccount.setClickable(false);
        }
    }

    private void setOnlineStatus(String onlineStatus) {
        String status = MyChatUtils.parseStatusToLocal(mContext, onlineStatus);
        int color = R.color.dpBorderGreen, bg = R.drawable.online_status_circle;
        if (onlineStatus.equalsIgnoreCase(getResources().getString(R.string.busy))) {
            color = R.color.busy;
            bg = R.drawable.busy_status_circle;
        } else if (onlineStatus.equalsIgnoreCase(getResources().getString(R.string.offline))) {
            color = R.color.offline;
            bg = R.drawable.offline_status_circle;
        } else if (onlineStatus.equalsIgnoreCase(getResources().getString(R.string.away))) {
            color = R.color.away;
            bg = R.drawable.away_status_circle;
        }
        statusCircle.setBackgroundResource(bg);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header:
                ActivityLauncher.launchUserProfile((Activity) mContext);
                break;
            case R.id.rl_verify_account_container:
                ActivityLauncher.launchPinActivity((Activity) mContext);
                break;
            case R.id.ibPaymenticon:
                ActivityLauncher.launchPaymentContactScreen(getActivity());
                break;

            // ApiCalls.getInstance(getContext()).GetDestinations("96171000040","062364a7e91943909764bed27bea63ad",this);


        }
    }

    @Override
    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {

        switch (responseForRequest) {
           /* case Constants
                    .Destinations:
                GetDestinations response = (GetDestinations) body;
            break;*/
            case Constants
                    .Login:
                CashPlusLoginResponseModel cashPlusLoginResponseModel = (CashPlusLoginResponseModel) body;
//                Toast.makeText(getContext(), cashPlusLoginResponseModel.getUserName(), Toast.LENGTH_SHORT).show();
                break;
            case Constants.RegisterMobileUser:
                RegisterCashPlusResponse registerMobileUserResponse_model = (RegisterCashPlusResponse) body;
                break;
            case Constants.Verification:
                VerificationPinResponseModel verificationMobileUserResponse_model = (VerificationPinResponseModel) body;
                break;
            case Constants.NewPassword:
                PasswordResetModel setUserPasswordResponse_model = (PasswordResetModel) body;
                break;
            case Constants.CashPLusUpdateUserProfile:
                CashPLusUpdateUserProfileResponse_Model model = (CashPLusUpdateUserProfileResponse_Model) body;
                //Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;


        }


    }
}