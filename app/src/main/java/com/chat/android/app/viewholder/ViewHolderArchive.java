package com.chat.android.app.viewholder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.widget.CircleImageView;


/**
 * Created by CAS56 on 1/10/2017.
 */
public class ViewHolderArchive extends RecyclerView.ViewHolder {
    public TextView newMessage, storeName, newMessageDate, newMessageCount,archived;
    public CircleImageView storeImage;
    public ImageView ivMsgType, mute_chatlist, tick;

    public ViewHolderArchive(View aView) {
        super(aView);
        newMessage = aView.findViewById(R.id.newMessage);
        newMessageDate = aView.findViewById(R.id.newMessageDate);
        storeName = aView.findViewById(R.id.storeName);
        storeImage = aView.findViewById(R.id.storeImage);
        newMessageCount = aView.findViewById(R.id.newMessageCount);
        archived = aView.findViewById(R.id.archived);
        tick = aView.findViewById(R.id.tick);
        mute_chatlist = aView.findViewById(R.id.mute_chatlist);
        ivMsgType = aView.findViewById(R.id.ivMsgType);
    }
}
