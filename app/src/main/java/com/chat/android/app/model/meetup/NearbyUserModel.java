package com.chat.android.app.model.meetup;

import com.google.gson.annotations.SerializedName;

public class NearbyUserModel {

    @SerializedName("_id")
    String id;
    @SerializedName("onlineStatus")
    String onlineStatus;
    @SerializedName("msisdn")
    String msisdn;
    @SerializedName("pinCode")
    String pinCode;
    @SerializedName("location")
    LocationNearByMeetups locationNearByMeetups;
    @SerializedName("meetup_name")
    String meetup_name;
    @SerializedName("meetup_profile")
    MeetUpProfile meetUpProfile;
    @SerializedName("dist")
    Dist dist;
    @SerializedName("ProfilePic")
    String profilePic;
    @SerializedName("is_friend")
    Boolean isFriend;
    @SerializedName("distance")
    double distance;

    public NearbyUserModel(String id, String onlineStatus, String msisdn, String pinCode, LocationNearByMeetups locationNearByMeetups, String meetup_name, MeetUpProfile meetUpProfile, Dist dist, String profilePic, Boolean isFriend) {
        this.id = id;
        this.onlineStatus = onlineStatus;
        this.msisdn = msisdn;
        this.pinCode = pinCode;
        this.locationNearByMeetups = locationNearByMeetups;
        this.meetup_name = meetup_name;
        this.meetUpProfile = meetUpProfile;
        this.dist = dist;
        this.profilePic = profilePic;
        this.isFriend = isFriend;
    }

    public NearbyUserModel() {
    }

    public String getMeetup_name() {
        return meetup_name;
    }

    public void setMeetup_name(String meetup_name) {
        this.meetup_name = meetup_name;
    }

    public MeetUpProfile getMeetUpProfile() {
        return meetUpProfile;
    }

    public void setMeetUpProfile(MeetUpProfile meetUpProfile) {
        this.meetUpProfile = meetUpProfile;
    }

    public Dist getDist() {
        return dist;
    }

    public void setDist(Dist dist) {
        this.dist = dist;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Boolean getFriend() {
        return isFriend;
    }

    public void setFriend(Boolean friend) {
        isFriend = friend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(String onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public LocationNearByMeetups getLocationNearByMeetups() {
        return locationNearByMeetups;
    }

    public void setLocationNearByMeetups(LocationNearByMeetups locationNearByMeetups) {
        this.locationNearByMeetups = locationNearByMeetups;
    }

    public double getDistance() {
        return distance;
    }
}
