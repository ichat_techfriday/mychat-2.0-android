package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class ProcessTransactionModel {
    @SerializedName("UserName")
    String userName;
    @SerializedName("token")
    String token;
    @SerializedName("contactID")
    public int contactID;
    @SerializedName("paymentMethod_ID")
    public int paymentMethod_ID;
    @SerializedName("paymentMethodType")
    public int paymentMethodType;
    @SerializedName("payingAmount")
    public double payingAmount;
    @SerializedName("payingCurrencyCode")
    public String payingCurrencyCode;
    @SerializedName("payoutAmount")
    public double payoutAmount;
    @SerializedName("payoutCurrencyCode")
    public String payoutCurrencyCode;
    @SerializedName("payoutAgentID")
    public int payoutAgentID;
    @SerializedName("transferReason")
    public String transferReason;
    @SerializedName("customerMessage")
    public String customerMessage;
  @SerializedName("promoCode")
    public String promoCode;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getContactID() {
        return contactID;
    }

    public void setContactID(int contactID) {
        this.contactID = contactID;
    }

    public int getPaymentMethod_ID() {
        return paymentMethod_ID;
    }

    public void setPaymentMethod_ID(int paymentMethod_ID) {
        this.paymentMethod_ID = paymentMethod_ID;
    }

    public int getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(int paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public double getPayingAmount() {
        return payingAmount;
    }

    public void setPayingAmount(double payingAmount) {
        this.payingAmount = payingAmount;
    }

    public String getPayingCurrencyCode() {
        return payingCurrencyCode;
    }

    public void setPayingCurrencyCode(String payingCurrencyCode) {
        this.payingCurrencyCode = payingCurrencyCode;
    }

    public double getPayoutAmount() {
        return payoutAmount;
    }

    public void setPayoutAmount(double payoutAmount) {
        this.payoutAmount = payoutAmount;
    }

    public String getPayoutCurrencyCode() {
        return payoutCurrencyCode;
    }

    public void setPayoutCurrencyCode(String payoutCurrencyCode) {
        this.payoutCurrencyCode = payoutCurrencyCode;
    }

    public int getPayoutAgentID() {
        return payoutAgentID;
    }

    public void setPayoutAgentID(int payoutAgentID) {
        this.payoutAgentID = payoutAgentID;
    }

    public String getTransferReason() {
        return transferReason;
    }

    public void setTransferReason(String transferReason) {
        this.transferReason = transferReason;
    }

    public String getCustomerMessage() {
        return customerMessage;
    }

    public void setCustomerMessage(String customerMessage) {
        this.customerMessage = customerMessage;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
}
