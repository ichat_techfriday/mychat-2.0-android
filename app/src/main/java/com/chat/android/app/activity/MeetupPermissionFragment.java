package com.chat.android.app.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.chat.android.R;
import com.chat.android.app.model.meetup.UpdateMeetupResponseData;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.backend.ApiCalls;
import com.chat.android.core.SessionManager;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.DialogUtils;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.StorageUtility;

import java.util.Objects;

public class MeetupPermissionFragment extends MeetupBaseFragment {

    private Bundle mBundle = new Bundle();
    private int maxDistance;
    private final int limit = 20;
    private TextView txtLocationPermission = null;
    private View mBtnContinue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meetup_permission, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        StorageUtility.saveDataInPreferences(mContext, AppConstants.SPKeys.MEETUP_PERMISSION_ALLOWED.getValue(), true);
        
        //init
        mBtnContinue = view.findViewById(R.id.continue_btn);
        txtLocationPermission = view.findViewById(R.id.txt_location_permission);
        //getting mBundle info
        mBundle = this.getArguments();
        if (mBundle != null) {
            mFullName = mBundle.getString("fullName");
            mBio = mBundle.getString("bio");
            mDob = mBundle.getString("dob");
            maxDistance = mBundle.getInt("distanceInMeters");
        }

        fetchLocation();

        mBtnContinue.setOnClickListener(v -> {
            if (MyChatUtils.isLocationPermissionAllowed(mContext)) {
                MeetupNearbyFragment meetupNearbyFragment = new MeetupNearbyFragment();
                mBundle.putString("fullName", mFullName);
                mBundle.putString("bio", mBio);
                mBundle.putString("dob", mDob);
                mBundle.putDouble("latitude", lat);
                mBundle.putDouble("longitude", lng);
                mBundle.putInt("limit", limit);
                mBundle.putString("userActivationStatus", userActivationStatus);
                mBundle.putInt("distanceInMeters", maxDistance);
                meetupNearbyFragment.setArguments(mBundle);
                OpenFragment(meetupNearbyFragment);
            } else {
                showLocationNotEnabledError();
            }
        });
    }

    protected void checkLocationPermissionState() {

        int fineLocation = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarseLocation = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION);
        String permission = "";
        boolean shouldEnable = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

            int bgLocation = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_BACKGROUND_LOCATION);

            boolean isAppLocationPermissionGranted = (bgLocation == PackageManager.PERMISSION_GRANTED) &&
                    (coarseLocation == PackageManager.PERMISSION_GRANTED);

            boolean preciseLocationAllowed = (fineLocation == PackageManager.PERMISSION_GRANTED)
                    && (coarseLocation == PackageManager.PERMISSION_GRANTED);

            if (preciseLocationAllowed) {
                permission = mActivity.getString(R.string.while_using_app);
                Log.e("PERMISSION", "Precise location is enabled in Android 12");
            } else {
                permission = mActivity.getString(R.string.never);
                Log.e("PERMISSION", "Precise location is disabled in Android 12");
                shouldEnable = false;
            }

            if (isAppLocationPermissionGranted) {
                permission = mActivity.getString(R.string.use_all_time);
                Log.e("PERMISSION", "Location is allowed all the time");
            } else if (coarseLocation == PackageManager.PERMISSION_GRANTED) {
                permission = mActivity.getString(R.string.while_using_app);
                Log.e("PERMISSION", "Location is allowed while using the app");
            } else {
                permission = mActivity.getString(R.string.never);
                shouldEnable = false;
                Log.e("PERMISSION", "Location is not allowed.");
            }

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

            int bgLocation = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_BACKGROUND_LOCATION);

            boolean isAppLocationPermissionGranted = (bgLocation == PackageManager.PERMISSION_GRANTED) &&
                    (coarseLocation == PackageManager.PERMISSION_GRANTED);

            if (isAppLocationPermissionGranted) {
                permission = mActivity.getString(R.string.use_all_time);
                Log.e("PERMISSION", "Location is allowed all the time");
            } else if (coarseLocation == PackageManager.PERMISSION_GRANTED) {
                permission = mActivity.getString(R.string.while_using_app);
                Log.e("PERMISSION", "Location is allowed while using the app");
            } else {
                permission = mActivity.getString(R.string.never);
                shouldEnable = false;
                Log.e("PERMISSION", "Location is not allowed.");
            }

        } else {

            boolean isAppLocationPermissionGranted = (fineLocation == PackageManager.PERMISSION_GRANTED) &&
                    (coarseLocation == PackageManager.PERMISSION_GRANTED);

            if (isAppLocationPermissionGranted) {
                permission = mActivity.getString(R.string.while_using_app);
                Log.e("PERMISSION", "Location permission is granted");
            } else {
                permission = mActivity.getString(R.string.never);
                shouldEnable = false;
                Log.e("PERMISSION", "Location permission is not granted");
            }
        }
        txtLocationPermission.setText(permission);
    }

    private void showLocationNotEnabledError() {
        DialogUtils.showAlertWithButtons(mContext, mContext.getResources().getString(R.string.location_permission_not_required_title), mContext.getResources().getString(R.string.location_permission_not_enabled_error), getString(R.string.ok),
                getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DialogUtils.dismiss();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }

                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DialogUtils.dismiss();
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        DialogUtils.dismiss();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        checkLocationPermissionState();
    }
}