package com.chat.android.app.utils;

import com.chat.android.core.message.MessageFactory;

/**
 * Created by CAS60 on 12/21/2017.
 */
public class ConstantMethods {

    public static String getChatType(String docId) {
        if (docId.contains("-g")) {
            return MessageFactory.CHAT_TYPE_GROUP;
        } else if (docId.contains("-secret")) {
            return MessageFactory.CHAT_TYPE_SECRET;
        } else {
            return MessageFactory.CHAT_TYPE_SINGLE;
        }
    }

}
