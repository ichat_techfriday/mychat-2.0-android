package com.chat.android.app.activity;


import static com.chat.android.backend.Constants.SaveContact;
import static com.chat.android.utils.DialogUtils.dismiss;

import static org.webrtc.ContextUtils.getApplicationContext;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.model.ApiRequestModel;
import com.chat.android.app.model.Contact_Info;
import com.chat.android.app.model.GetBankBranches;
import com.chat.android.app.model.GetBankPayoutLocations;
import com.chat.android.app.model.GetCashPayoutLocations;
import com.chat.android.app.model.GetCities;
import com.chat.android.app.model.GetContactsModel;
import com.chat.android.app.model.GetDestinations;
import com.chat.android.app.model.GetPaymentMethods;
import com.chat.android.app.model.GetPriceOffer;
import com.chat.android.app.model.GetSendingReasons;
import com.chat.android.app.model.SaveContactModel;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.FriendModel;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.StorageUtility;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class SendPaymentFragmentStep3 extends Fragment implements View.OnClickListener, INetworkResponseListener {

    private EditText spinnerMethod;
    private GetPaymentMethods.PaymentMethods selectedMethod;
    private GetPaymentMethods.PayingCurrency selectedPayingCurrency;
    private LinearLayout priceLayout;
    private ImageButton icNext;
    private ImageButton icBack;
    private GetDestinations.AvailablePayingDetail selectedExchangeBlock;
    private GetDestinations.AvailableTransferMethod selectedTransferMethod;
    private GetBankPayoutLocations.AvailableBanks selectedBankLocation;
    private GetCashPayoutLocations.AvailableLocation selectedCashLocation;
    private GetBankBranches.AllBranches selectedBranchBranch;
    private boolean BankNavigation;
    private GetCities.AllCity selectedCity;
    private ProgressDialog progressDialog;
    private EditText tvPromoCode;
    private TextView tvPayingAmount;
    private TextView tvPayingCurrency;
    private TextView tvTotalCharges;
    private TextView tvTotalPaying;
    private TextView tvExchangeRate;
    private EditText tvpaying;
    private EditText spinnerPayingCurrency;
    private EditText spinnerPayoutCurrency;
    private Button priceButton, cancel_btn;
    private String payingAmount;
    private GetPriceOffer GetPriceOffer;
    private Contact_Info contactDetails;
    private List<GetPaymentMethods.PaymentMethods> methodsList = new ArrayList<>();
    private List<GetPaymentMethods.PayingCurrency> payingCurrencyList = new ArrayList<>();
    private List<String> payoutCurrencyList = new ArrayList<>();
    String name, data;
    private List<GetSendingReasons.AvailableOption> SendingList = new ArrayList<>();
    private AutoCompleteTextView spinnerPurpose;
    boolean selected = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_send_payment_step3, container, false);
        data = StorageUtility.getDataFromPreferences(getContext(), "send_pay", "");

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(data);
            name = jsonObject.getString("Name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initViews(view);
        return view;
    }

    private void initViews(View view) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCancelable(false);
        icNext = view.findViewById(R.id.icNext);
        tvPromoCode = view.findViewById(R.id.tvPromoCode);
        tvPayingAmount = view.findViewById(R.id.tvPayingAmount);
        spinnerPurpose = view.findViewById(R.id.spinnerPurpose);
        tvPayingCurrency = view.findViewById(R.id.tvPayingCurrency);
        tvTotalCharges = view.findViewById(R.id.tvTotalCharges);
        tvTotalPaying = view.findViewById(R.id.tvTotalPaying);
        tvExchangeRate = view.findViewById(R.id.tvExchangeRate);
        spinnerMethod = view.findViewById(R.id.spinnerMethodType);
        tvExchangeRate = view.findViewById(R.id.tvExchangeRate);
        tvpaying = view.findViewById(R.id.tvpayingamount);
        spinnerPayingCurrency = view.findViewById(R.id.spinnerPayingCurrency);
        priceLayout = view.findViewById(R.id.priceLayout);
        cancel_btn = view.findViewById(R.id.cancel_btn);
        spinnerPayoutCurrency = view.findViewById(R.id.spinnerPayoutCurrency);
        icBack = view.findViewById(R.id.icBack);
        icNext.setOnClickListener(this);
        icBack.setOnClickListener(this);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiRequestModel apiRequestModel = new ApiRequestModel(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getContext()).getCashPlusToken());
        ApiCalls.getInstance(getContext()).GetContacts(getContext(), apiRequestModel, this);

        spinnerMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMethods();
            }
        });
        spinnerPayingCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPayingCurrency();
            }
        });
        spinnerPurpose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPurpose();
            }
        });
        spinnerPayoutCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPayoutCurrency();
            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               show_exit_dialog();
            }
        });
    }
    private void setPurpose() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select Purpose");
        final ArrayAdapter<GetSendingReasons.AvailableOption> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, SendingList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GetSendingReasons.AvailableOption availableOption = adapter.getItem(which);
                spinnerPurpose.setText(availableOption.name);
                selected = true;
                dismiss();
            }
        });
        builderSingle.show();


    }
    public boolean validateViews(){
        if (TextUtils.isEmpty(spinnerMethod.getText().toString())) {
            return false;
        }
        if (TextUtils.isEmpty(spinnerPayingCurrency.getText().toString())) {
            return false;
        }
        if (TextUtils.isEmpty(spinnerPayoutCurrency.getText().toString())) {
            return false;
        }
        if (TextUtils.isEmpty(tvpaying.getText().toString())) {
            return false;
        }

        return true;
    }

    public void show_exit_dialog(){
        final CustomAlertDialog dialog = new CustomAlertDialog();
        String msg = "Do you want to cancel the transaction?";
        dialog.setMessage(msg);

        dialog.setPositiveButtonText("Yes");
        dialog.setNegativeButtonText("No");

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                //String data = SendPaymentActivity.model;
                String model = StorageUtility.getDataFromPreferences(getContext(), "send_pay", "");
                Gson gson = new Gson();
                FriendModel friendModel = gson.fromJson(model, FriendModel.class);
                Intent intent = new Intent(getContext(), ChatPageActivity.class);
                intent.putExtra("receiverUid", friendModel.getNumberInDevice());
                intent.putExtra("receiverName", friendModel.getFirstName());
                intent.putExtra("documentId", friendModel.get_id());
                intent.putExtra("Username", friendModel.getFirstName());
                intent.putExtra("Image", friendModel.getAvatarImageUrl());
                intent.putExtra("msisdn", friendModel.getMsisdn());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getFragmentManager(), "exit transaction");
    }

    private void setMethods() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select Transfer Method");
        final ArrayAdapter<GetPaymentMethods.PaymentMethods> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, methodsList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GetPaymentMethods.PaymentMethods method = adapter.getItem(which);
                spinnerMethod.setText(method.PM_LABEL);
                selectedMethod = method;
                payingCurrencyList = selectedMethod.getPayingCurrency();
                dismiss();
            }
        });
        builderSingle.show();


    }

    private void setPayingCurrency() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select Paying Currency");
        final ArrayAdapter<GetPaymentMethods.PayingCurrency> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, payingCurrencyList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GetPaymentMethods.PayingCurrency method = adapter.getItem(which);
                spinnerPayingCurrency.setText(method.PayingCurrency);
                selectedPayingCurrency = method;
                payoutCurrencyList = selectedPayingCurrency.getPayoutCurrencies();
                dismiss();
            }
        });
        builderSingle.show();


    }

    private void setPayoutCurrency() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select Payout Currency");
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, payoutCurrencyList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String payout = adapter.getItem(which);
                spinnerPayoutCurrency.setText(payout);
                dismiss();
            }
        });
        builderSingle.show();


    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle();
    }

    private void setTitle() {
        ((SendPaymentActivity) getActivity()).UpdateTitle("Making transaction to "+name);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.icNext:
                if (validateViews()) {
                    MyChatUtils.hideKeyboard(getActivity());
                    progressDialog.show();
                    payingAmount = tvpaying.getText().toString();
                    SetPriceModel setPriceModel = new SetPriceModel(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getContext()).getCashPlusToken(), Integer.parseInt(contactDetails.getContact_ID()), 0, selectedMethod.PM_TYPE, spinnerPayoutCurrency.getText().toString(), 0.0, spinnerPayingCurrency.getText().toString(), payingAmount == null || payingAmount.isEmpty() ? 0.0 : Double.parseDouble(payingAmount), false, tvPromoCode.getText().toString());
                    ApiCalls.getInstance(getContext()).GetPriceOffer(getContext(), setPriceModel, SendPaymentFragmentStep3.this);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.field_must_not_be_empty), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.icBack:
                getActivity().onBackPressed();
                break;


        }


    }




    private void CheckContact(List<Contact_Info> saveContactModelList) {
        if (SessionManager.getInstance(getApplicationContext()).getCashplusUserPhoneNumber() != null || !SessionManager.getInstance(getApplicationContext()).getCashplusUserPhoneNumber().isEmpty()) {
            String msdin = SessionManager.getInstance(getApplicationContext()).getCashplusUserPhoneNumber();
            for (Contact_Info saveContact : saveContactModelList) {
                if (saveContact.getMobile() != null) {

                    if (saveContact.getMobile().equals(msdin)) {
                        contactDetails = saveContact;
                        progressDialog.show();
                        ApiRequestModel apiRequestModel = new ApiRequestModel(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getContext()).getCashPlusToken());
                        apiRequestModel.setContactID(contactDetails.getContact_ID());
                        ApiCalls.getInstance(getContext()).GetContactsPaymentMethod(getContext(), apiRequestModel, this);
                        ApiRequestModel apiRequestModel2 = new ApiRequestModel(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getContext()).getCashPlusToken(), contactDetails.getCountry_Code(), true);
                        ApiCalls.getInstance(getContext()).GetSendingReasonsByCountry(getContext(), apiRequestModel2, this);
                        break;
                    }

                }
            }
        }
    }


    @Override
    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        progressDialog.dismiss();

        switch (responseForRequest) {
            case Constants.GetPriceOffer:
                if (status) {
                    GetPriceOffer priceOffer = (GetPriceOffer) body;
                    if (priceOffer.getErrorCode().equals("00")) {
                        if (priceOffer.getPrice_Offer().has_Errors)
                            Toast.makeText(getContext(), priceOffer.getErrorDescription(), Toast.LENGTH_SHORT).show();
                        else {
                            GetPriceOffer = priceOffer;
                            SendPaymentFragmentStep5 sendPaymentFragmentStep5 = new SendPaymentFragmentStep5();
                            Bundle b1 = new Bundle();
                            b1.putSerializable("GetPriceOffer", GetPriceOffer);
                            b1.putString("payingAmount", payingAmount);
                            b1.putSerializable("selectedMethod", selectedMethod);
                            b1.putSerializable("contactDetails", contactDetails);
                            b1.putSerializable("purpose", spinnerPurpose.getText().toString());
                            sendPaymentFragmentStep5.setArguments(b1);
                            ((SendPaymentActivity) getActivity()).OpenFragment(sendPaymentFragmentStep5);
                        }
                    }else
                        Toast.makeText(getContext(), priceOffer.getErrorDescription(), Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
                break;
            case Constants.SendingReasons:
                if (status) {

                    GetSendingReasons sendingReasons = (GetSendingReasons) body;
                    if (sendingReasons.getErrorCode().equals("00") || sendingReasons.getErrorCode().equals("0")) {
                        SendingList = sendingReasons.getSendingReasons().availableOptions;
                    } else
                        Toast.makeText(getContext(), sendingReasons.errorDescription, Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;
            case Constants.GetContacts:
                if (status) {

                    GetContactsModel getContactsModel = (GetContactsModel) body;
                    if (getContactsModel.getErrorCode().equals("0")) {
                        CheckContact(getContactsModel.getSaveContactModels());
                    } else
                        Toast.makeText(getContext(), getContactsModel.getErrorDescription(), Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
                break;
            case Constants.GetContactPaymentMethod:
                if (status) {

                    GetPaymentMethods paymentMethods = (GetPaymentMethods) body;
                    if (paymentMethods.getErrorCode().equals("0")) {
                        methodsList = paymentMethods.getPaymentMethods();
                    } else
                        Toast.makeText(getContext(), paymentMethods.getErrorDescription(), Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
                break;

        }
    }

    public class SetPriceModel {
        @SerializedName("UserName")
        String userName;
        @SerializedName("token")
        String token;
        @SerializedName("NewPassword")
        String newPassword;
        @SerializedName("Selected_DTM")
        int SelectedDTM;
        @SerializedName("City_Code")
        String City_Code;
        @SerializedName("bank_code")
        String bank_code;
        @SerializedName("Bank_Code")
        String Bank_Code;
        @SerializedName("payoutCurrency")
        String PayoutCurrency;
        @SerializedName("payingCurrency")
        String PayingCurrency;
        @SerializedName("payoutAmount")
        double PayoutAmount;
        @SerializedName("payout_Location_ID")
        String payout_Location_ID;
        @SerializedName("Service_Provider_Code")
        String Service_Provider_Code;
        @SerializedName("PromoCode")
        String PromoCode;
        @SerializedName("country_Code")
        String country_Code;
        @SerializedName("chargesIncluded")
        Boolean ChargesIncluded;
        @SerializedName("promoCode")
        String promoCode;
        @SerializedName("payingAmount")
        double payingAmount;
        @SerializedName("paymentMethodType")
        int paymentMethodType;
        @SerializedName("paymentMethod_ID")
        int paymentMethod_ID;
        @SerializedName("contactID")
        int contactID;


        public SetPriceModel(String userName, String token, int contactID, int paymentMethod_ID, int paymentMethodType, String payoutCurrency, double payoutAmount, String payingCurrency, double payingAmount, Boolean chargesIncluded, String promoCode) {
            this.userName = userName;
            this.token = token;
            this.contactID = contactID;
            this.paymentMethod_ID = paymentMethod_ID;
            this.paymentMethodType = paymentMethodType;
            this.PayingCurrency = payingCurrency;
            this.PayoutAmount = payoutAmount;
            this.payingAmount = payingAmount;
            this.ChargesIncluded = chargesIncluded;
            this.promoCode = promoCode;
            this.PayoutCurrency = payoutCurrency;
        }

        public int getContactID() {
            return contactID;
        }

        public void setContactID(int contactID) {
            this.contactID = contactID;
        }

        public int getSelectedDTM() {
            return SelectedDTM;
        }

        public void setSelectedDTM(int selectedDTM) {
            SelectedDTM = selectedDTM;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getNewPassword() {
            return newPassword;
        }

        public void setNewPassword(String newPassword) {
            this.newPassword = newPassword;
        }

        public String getCity_Code() {
            return City_Code;
        }

        public void setCity_Code(String city_Code) {
            City_Code = city_Code;
        }

        public String getPayoutCurrency() {
            return PayoutCurrency;
        }

        public void setPayoutCurrency(String payoutCurrency) {
            PayoutCurrency = payoutCurrency;
        }

        public String getPayingCurrency() {
            return PayingCurrency;
        }

        public void setPayingCurrency(String payingCurrency) {
            PayingCurrency = payingCurrency;
        }


        public String getPayout_Location_ID() {
            return payout_Location_ID;
        }

        public void setPayout_Location_ID(String payout_Location_ID) {
            this.payout_Location_ID = payout_Location_ID;
        }

        public String getService_Provider_Code() {
            return Service_Provider_Code;
        }

        public void setService_Provider_Code(String service_Provider_Code) {
            Service_Provider_Code = service_Provider_Code;
        }

        public String getPromoCode() {
            return PromoCode;
        }

        public void setPromoCode(String promoCode) {
            PromoCode = promoCode;
        }

        public Boolean getChargesIncluded() {
            return ChargesIncluded;
        }

        public void setChargesIncluded(Boolean chargesIncluded) {
            ChargesIncluded = chargesIncluded;
        }


    }


}

