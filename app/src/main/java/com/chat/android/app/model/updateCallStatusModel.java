package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class updateCallStatusModel {

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    @SerializedName("errNum")
    String errNum;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("message")
    String message;

}
