package com.chat.android.app.model;

public class RejectCallBroadCast {
    private String data;
    public RejectCallBroadCast(String data) {
        this.data = data;
    }
    public String getData() {
        return data;
    }

    public void setRejectCallData(String data) {
        this.data = data;
    }
}
