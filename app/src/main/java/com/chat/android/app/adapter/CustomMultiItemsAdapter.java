package com.chat.android.app.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.chat.android.R;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.core.model.MultiTextDialogPojo;

import java.util.List;

/**
 * Created by CAS60 on 2/16/2017.
 */
public class CustomMultiItemsAdapter extends RecyclerView.Adapter<CustomMultiItemsAdapter.MultiItemsViewHolder> {

    private List<MultiTextDialogPojo> dataList;
    private Context mContext;

    public CustomMultiItemsAdapter(Context mContext, List<MultiTextDialogPojo> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }

    public class MultiItemsViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivLabelIcon;
        public AvnNextLTProRegTextView tvLabel;
        public View dividerLine;

        public MultiItemsViewHolder(View itemView) {
            super(itemView);

            ivLabelIcon = itemView.findViewById(R.id.ivLabelIcon);
            tvLabel = itemView.findViewById(R.id.tvLabel);
            dividerLine = itemView.findViewById(R.id.dividerLine);
        }
    }

    @Override
    public MultiItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_multi_items, parent, false);
        MultiItemsViewHolder holder = new MultiItemsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MultiItemsViewHolder holder, int position) {

        MultiTextDialogPojo data = dataList.get(position);

        holder.tvLabel.setText(data.getLabelText());

        // make image view visibility and set resource if required
        holder.ivLabelIcon.setVisibility(View.GONE);
        if(data.getImageResource() != null) {
            holder.ivLabelIcon.setVisibility(View.VISIBLE);
            holder.ivLabelIcon.setImageResource(data.getImageResource());
        }

        int lastItem = dataList.size() - 1;
        if(lastItem == position) {
            holder.dividerLine.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


}
