package com.chat.android.app.activity;

public interface ItemClickListener {
    void itemClick(int position);
    void itemLongClick(int position);
}