package com.chat.android.app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;

import com.chat.android.R;
import com.chat.android.core.CoreActivity;
import com.chat.android.utils.AppConstants;

public class MeetupActivity extends CoreActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meetup);

        Intent intent = getIntent();
        String type = intent.getStringExtra(AppConstants.IntentKeys.MEETUP_SCREEN_TYPE.getValue());

        if (type != null && type.equalsIgnoreCase(AppConstants.MEETUP_SCREEN_NAME.MeetupInfoFragment.getValue())) {
            MeetupInfoFragment meetupInfoFragment = new MeetupInfoFragment();
            OpenFragment(meetupInfoFragment);
        } else if(type != null && type.equalsIgnoreCase(AppConstants.MEETUP_SCREEN_NAME.MeetupProfileFragment.getValue())) {
            int distanceInMeters = intent.getIntExtra(AppConstants.IntentKeys.MEETUP_DISTANCE_IN_METERS.getValue(), 0);
            MeetupProfileFragment meetupProfileFragment = new MeetupProfileFragment();
            Bundle mBundle = new Bundle();
            mBundle.putInt(AppConstants.IntentKeys.MEETUP_DISTANCE_IN_METERS.getValue(), distanceInMeters);
            meetupProfileFragment.setArguments(mBundle);
            OpenFragment(meetupProfileFragment);
        } else {
            MeetupRadiusFragment meetupRadiusFragment = new MeetupRadiusFragment();
            OpenFragment(meetupRadiusFragment);
        }
    }

    public void OpenFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;
        FragmentManager manager = getSupportFragmentManager();

        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(R.id.frame_layout, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            finish();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }
}