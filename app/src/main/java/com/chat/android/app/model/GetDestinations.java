package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetDestinations implements Serializable {

    @SerializedName("Countries")
    public Countries Countries;

    public GetDestinations.Countries getCountries() {
        return Countries;
    }

    public void setCountries(GetDestinations.Countries countries) {
        Countries = countries;
    }

    public class Countries implements Serializable {
        @SerializedName("countries")
        public List<Country> countries;

    }


    public class AvailablePayingDetail implements Serializable {
        @SerializedName("DTM_ID")
        public int dTM_ID;
        @SerializedName("Paying_Currency")
        public String paying_Currency;
        @SerializedName("Payout_Currency")
        public String payout_Currency;
        @SerializedName("Paying_Rounding")
        public int paying_Rounding;
        @SerializedName("Payout_Rounding")
        public int payout_Rounding;
        @SerializedName("Exchange_Rate")
        public double exchange_Rate;
        @SerializedName("Exchange_Rate_Margin")
        public double exchange_Rate_Margin;
        @SerializedName("Fixed_Fees")
        public double fixed_Fees;
        @SerializedName("Fixed_Agent_Commission_Percentage")
        public double fixed_Agent_Commission_Percentage;
        @SerializedName("Minimum_Sending_Amount")
        public double minimum_Sending_Amount;
        @SerializedName("Maximum_Sending_Amount")
        public double maximum_Sending_Amount;
        @SerializedName("Fixed_Additional_Fake_FeesAmoun_t")
        public int fixed_Additional_Fake_FeesAmoun_t;
        @SerializedName("Fixed_Additional_Fake_Fees_Percentage")
        public int fixed_Additional_Fake_Fees_Percentage;
        @SerializedName("Use_Provider_Data")
        public boolean use_Provider_Data;
    }

    public class AvailableTransferMethod implements Serializable {
        @SerializedName("TransferMethodID")
        public int transferMethodID;
        @SerializedName("TransferMethodName")
        public String transferMethodName;
        @SerializedName("TransferMethodCode")
        public String transferMethodCode;
        public List<AvailablePayingDetail> availablePayingDetails;

        @Override
        public String toString() {
            return transferMethodName;
        }
    }

    public class TransferMethods implements Serializable {
        @SerializedName("AvailableTransferMethods")
        public List<AvailableTransferMethod> availableTransferMethods;
    }

    public class Country implements Serializable {
        @SerializedName("Country_ID")
        public int country_ID;
        public String countryCode;
        public String countryISOCode;
        public String countryName;
        public Object requiredMiddleName;
        public TransferMethods transferMethods;
        @SerializedName("NationalityName")
        public String nationalityName;
        @SerializedName("DialingCode")
        public String dialingCode;

        @Override
        public String toString() {
            return countryName;
        }
    }


    public class Root implements Serializable {
        @SerializedName("Countries")
        public Countries countries;
        @SerializedName("ErrorCode")
        public String errorCode;
        @SerializedName("ErrorDescription")
        public String errorDescription;
    }

}
