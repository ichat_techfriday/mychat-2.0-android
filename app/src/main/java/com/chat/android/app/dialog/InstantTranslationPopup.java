package com.chat.android.app.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.chat.android.R;

public class InstantTranslationPopup extends DialogFragment implements View.OnClickListener{

    private ImageView ivCancel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getDialog().setCancelable(true);
        View view = inflater.inflate(R.layout.instant_translation_popup, container, false);
        ivCancel = view.findViewById(R.id.ivCancel);
        ivCancel.setOnClickListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        DialogFragment dialogFragment=new DialogFragment();
        if(dialogFragment!=null){
            int width=ViewGroup.LayoutParams.MATCH_PARENT;
            int height=ViewGroup.LayoutParams.MATCH_PARENT;
            getDialog().getWindow().setLayout(width,height);
        }
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimationBottomTop;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ivCancel:
                dismiss();
                break;
        }
    }
}
