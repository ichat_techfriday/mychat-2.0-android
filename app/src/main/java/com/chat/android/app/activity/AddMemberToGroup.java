package com.chat.android.app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.adapter.AddGroupMemberAdapter;
import com.chat.android.app.adapter.RItemAdapter;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.utils.BlockUserUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.message.GroupEventInfoMessage;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.FriendModel;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.PincodesCacheUtility;
import com.chat.android.utils.TextUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by CAS60 on 11/30/2016.
 */
public class AddMemberToGroup extends CoreActivity implements RItemAdapter.OnItemClickListener, View.OnClickListener {

    private RecyclerView rvContacts;
    private ImageView ibBack;
    private TextView tvNoContacts;
    private static final String TAG = "AddMemberToGroup";
    private List<FriendModel> scimboContacts, filterContacts;
    private String mCurrentUserId, mGroupId, mGroupName, mGroupUserIds;
    private AddGroupMemberAdapter adapter;
    private ImageView search;
    private EditText etSearch;
    private TextView add_participant_headertext;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member_group);

        context=AddMemberToGroup.this;

        initView();
        initData();
    }

    private void initData() {

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);
        scimboContacts = contactDB_sqlite.getSavedScimboContacts();
        filterContactsList(scimboContacts);
        mCurrentUserId = SessionManager.getInstance(AddMemberToGroup.this).getCurrentUserID();

        Bundle bundle = getIntent().getExtras();
        mGroupId = bundle.getString("GroupId", "");
        mGroupName = bundle.getString("GroupName", "");
        mGroupUserIds = bundle.getString("GroupUserIds", "");

        initProgress(getResources().getString(R.string.loading_three_dots), false);
        filterContacts = new ArrayList<>();

        if (scimboContacts != null && scimboContacts.size() > 0) {

            for (FriendModel item : scimboContacts) {
                String userId = item.get_id();
                if (!mGroupUserIds.contains(userId) && !contactDB_sqlite.getBlockedMineStatus(userId, false).equals("1")) {
//                if (!mGroupUserIds.contains(userId) && !contactsDB.getBlockedMineStatus(userId, false).equals("1")) {
                    filterContacts.add(item);
                }
            }

            Collections.sort(filterContacts, Getcontactname.nameAscComparator);

            adapter = new AddGroupMemberAdapter(AddMemberToGroup.this, filterContacts);
            rvContacts.setAdapter(adapter);
            rvContacts.addOnItemTouchListener(new RItemAdapter(AddMemberToGroup.this,
                    rvContacts, AddMemberToGroup.this));
        } else {
            showToast(AddMemberToGroup.this, getResources().getString(R.string.your_contacts_are_not_available_in) + getResources().getString(R.string.app_name));
        }

        if (filterContacts.size() == 0) {
            rvContacts.setVisibility(View.GONE);
            tvNoContacts.setVisibility(View.VISIBLE);
        }
    }

    private void filterContactsList(List<FriendModel> list) {
        Iterator<FriendModel> iterator = list.iterator();
        while (iterator.hasNext()) {
            FriendModel friendModel = iterator.next();
            String requestStatus = friendModel.getRequestStatus();
            if (TextUtils.isEmpty(requestStatus))
                continue;
            if (!requestStatus.equalsIgnoreCase(AppConstants.FriendStatus.FRIENDS.getValue()))
                iterator.remove();
        }
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ibBack = findViewById(R.id.ibBack);
        ibBack.setOnClickListener(AddMemberToGroup.this);

        rvContacts = findViewById(R.id.rvContacts);

        LinearLayoutManager manager = new LinearLayoutManager(AddMemberToGroup.this);
        rvContacts.setLayoutManager(manager);

        tvNoContacts = findViewById(R.id.tvNoContacts);
        search= findViewById(R.id.search);
        etSearch= findViewById(R.id.etSearch);
        add_participant_headertext= findViewById(R.id.add_participant_headertext);

        Onclick();
    }


    private void Onclick(){

        search.setOnClickListener(this);

    }

    @Override
    public void onItemClick(View view, int position) {
        showAddAlert(position);
    }

    @Override
    public void onItemLongClick(View view, int position) {

    }

    private void showAddAlert(final int position) {

        final FriendModel e = adapter.getItem(position);

        String userId = e.get_id();
        String name = e.getMsisdn();

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(this);

        if (!contactDB_sqlite.getBlockedStatus(userId, false).equals("1")) {
            final CustomAlertDialog dialog = new CustomAlertDialog();
            String msg = getResources().getString(R.string.add)+ " " + e.getFirstName() + " "+getResources().getString(R.string.to)+" \"" + mGroupName + "\" "+getResources().getString(R.string.group);
            dialog.setMessage(msg);

            dialog.setPositiveButtonText(getResources().getString(R.string.zain_dialog_btn_txt));
            dialog.setNegativeButtonText(getResources().getString(R.string.cancel));

            dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
                @Override
                public void onPositiveButtonClick() {
                    if (internetcheck()) {
                        performAddMemberGroup(e.get_id());
                    } else {
                        showToast(AddMemberToGroup.this, getResources().getString(R.string.check_internet_connection));
                    }
                }

                @Override
                public void onNegativeButtonClick() {
                    dialog.dismiss();
                }
            });
            dialog.show(getSupportFragmentManager(), "Add member");
        } else {
            Getcontactname getcontactname = new Getcontactname(this);
            String message = getResources().getString(R.string.unblock) + " " + getcontactname.getSendername(userId, name) + " " + getResources().getString(R.string.to_add_group);
            displayAlert(message, userId);
        }
    }

    private void displayAlert(final String txt, final String userId) {
        final CustomAlertDialog dialog = new CustomAlertDialog();
        dialog.setMessage(txt);
        dialog.setNegativeButtonText(getResources().getString(R.string.cancel));
        dialog.setPositiveButtonText(getResources().getString(R.string.unblock));
        dialog.setCancelable(false);
        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                BlockUserUtils.changeUserBlockedStatus(AddMemberToGroup.this, EventBus.getDefault(),
                        mCurrentUserId, userId, false);
                dialog.dismiss();
            }

            @Override

            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "Unblock a person");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {

        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_GROUP)) {
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String groupAction = object.getString("groupType");

                if (groupAction.equalsIgnoreCase(SocketManager.ACTION_ADD_GROUP_MEMBER)) {
                    loadAddMemberMessage(object);
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_BLOCK_USER)) {
            loadBlockUserMessage(event);
        }
    }

    private void loadBlockUserMessage(ReceviceMessageEvent event) {
        try {
            Object[] obj = event.getObjectsArray();
            JSONObject object = new JSONObject(obj[0].toString());

            String from = object.getString("from");
            String to = object.getString("to");

            if (mCurrentUserId.equalsIgnoreCase(from)) {
                for (int i = 0; i < filterContacts.size(); i++) {
                    String userId = filterContacts.get(i).get_id();
                    if (userId.equals(to)) {
                        String stat = object.getString("status");
                        if (stat.equalsIgnoreCase("1")) {
                            showToast(this, getResources().getString(R.string.number_is_blocked));
                        } else {
                            showToast(this, getResources().getString(R.string.number_is_unblocked));
                        }
                        adapter.notifyDataSetChanged();
                        break;
                    }
                }
            }

        } catch (Exception e) {
            MyLog.e(TAG,"",e);
        }
    }

    private Boolean internetcheck() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;

    }

    private void loadAddMemberMessage(JSONObject object) {

        try {
            String msg = object.getString("message");
            String err = object.getString("err");
            String groupId = object.getString("groupId");
            String msgId = object.getString("id");
            String timeStamp = object.getString("timeStamp");
            String from = object.getString("from");

            JSONObject newUserObj = object.getJSONObject("newUser");
            String newUserId = newUserObj.getString("_id");
            String newUserMsisdn = newUserObj.getString("msisdn");
            String newUserPhNo = newUserObj.getString("PhNumber");
            String newUserName = newUserObj.getString("Name");
            if (object.has("Status")) {
                String newUserStatus = newUserObj.getString("Status");
            }
            PincodesCacheUtility pincodesCacheUtility = PincodesCacheUtility.getInstance(this);
            pincodesCacheUtility.savePinCodeItem(context, newUserId, newUserMsisdn, newUserName);

            GroupEventInfoMessage message = (GroupEventInfoMessage) MessageFactory.getMessage(MessageFactory.group_event_info, this);
            MessageItemChat item = message.createMessageItem(MessageFactory.add_group_member, false, msg, MessageFactory.DELIVERY_STATUS_READ,
                    mGroupId, mGroupName, from, newUserId);
            String docId = mCurrentUserId.concat("-").concat(mGroupId).concat("-g");
            item.setSenderName(mGroupName);
            item.setGroupName(mGroupName);
            item.setIsInfoMsg(true);
            item.setMessageId(docId.concat("-").concat(timeStamp));
//            db.updateChatMessage(docId, item);

            Intent exitIntent = new Intent();
            exitIntent.putExtra("MemberAdded", true);
            setResult(RESULT_OK, exitIntent);
            hideProgressDialog();
            finish();

        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }

    }

    private void performAddMemberGroup(String newUserId) {
        long ts = Calendar.getInstance().getTimeInMillis();
        String msgId = mCurrentUserId + "-" + mGroupId + "-g-" + ts;

        try {
            JSONObject object = new JSONObject();
            object.put("groupType", SocketManager.ACTION_ADD_GROUP_MEMBER);
            object.put("from", SessionManager.getInstance(AddMemberToGroup.this).getCurrentUserID());
            object.put("id", ts);
            object.put("toDocId", msgId);
            object.put("groupId", mGroupId);
            object.put("newuser", newUserId);
            object.put("add_new_group_name", true);

            SendMessageEvent event = new SendMessageEvent();
            event.setEventName(SocketManager.EVENT_GROUP);
            event.setMessageObject(object);
            EventBus.getDefault().post(event);

            showProgressDialog();
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(AddMemberToGroup.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(AddMemberToGroup.this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ibBack:
                BackButtonPush();
                break;

            case R.id.search:
                SearchClickFunction();
                break;

        }

    }


    private void SearchClickFunction(){


        add_participant_headertext.setVisibility(View.GONE);
        search.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });


    }


    private void BackButtonPush(){

        if(etSearch.getVisibility()==View.VISIBLE){

            etSearch.setText("");
            etSearch.setVisibility(View.GONE);
            search.setVisibility(View.VISIBLE);
            add_participant_headertext.setVisibility(View.VISIBLE);
            adapter.updateInfo(filterContacts);
            if (AddMemberToGroup.this.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getApplicationContext().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(
                        AddMemberToGroup.this.getCurrentFocus().getWindowToken(), 0);

            }
        }else{

            finish();
        }
    }
}
