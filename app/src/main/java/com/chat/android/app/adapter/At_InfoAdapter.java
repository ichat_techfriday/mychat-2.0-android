package com.chat.android.app.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.chat.android.R;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.app.widget.CircleTransform;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.GroupMembersPojo;
import com.chat.android.core.service.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by CAS60 on 11/29/2016.
 */

public class At_InfoAdapter extends RecyclerView.Adapter<At_InfoAdapter.ViewHolderGroupInfo> {

    private List<GroupMembersPojo> membersList;
    Session session;
    private Context mContext;
    SessionManager sessionmanager;
    Getcontactname getcontactname;
    private final Pattern sPattern = Pattern.compile("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$");
    private static final String TAG = "GroupInfoAdapter";
    private AtInfoAdapterItemClickListener listener;

    public At_InfoAdapter(Context mContext, List<GroupMembersPojo> list) {
        this.membersList = list;
        this.mContext = mContext;
        session = new Session(mContext);
        sessionmanager = SessionManager.getInstance(mContext);
        getcontactname = new Getcontactname(mContext);
    }

    @Override
    public int getItemCount() {
        if(membersList==null)
            return 0;
        return membersList.size();
    }

    @Override
    public ViewHolderGroupInfo onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.at_group_info, parent, false);
        return new ViewHolderGroupInfo(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolderGroupInfo holder, final int position) {

        final GroupMembersPojo member = membersList.get(position);
        String memmberid = member.getUserId();
        MyLog.d(TAG, "onBindViewHolder:  "+position+": "+member.getName());
        if(member.getContactName()!=null && !member.getContactName().trim().isEmpty())
        holder.tvName.setText(member.getContactName());
        else
            holder.tvName.setText(member.getName());

        holder.tvAdmin.setVisibility(View.GONE);

        if (member.getIsAdminUser().equalsIgnoreCase("1")) {
            holder.tvAdmin.setVisibility(View.VISIBLE);
        }

        String status = membersList.get(position).getStatus();

        if (sessionmanager.getCurrentUserID().equalsIgnoreCase(memmberid)) {
//            holder.tvStatus.setText(sessionmanager.getcurrentUserstatus());

            String userprofilepic = sessionmanager.getUserProfilePic();
            Picasso.with(mContext).load(Constants.SOCKET_IP + userprofilepic).error(
                    R.drawable.personprofile)
                    .transform(new CircleTransform()).into(holder.ivUserDp);
        } else {
            getcontactname.setProfileStatusText(holder.tvStatus, memmberid, status, false);
            getcontactname.configProfilepic(holder.ivUserDp, memmberid, false, false, R.drawable.ic_placeholder_black);
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(member, position);
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolderGroupInfo extends RecyclerView.ViewHolder {
        CircleImageView ivUserDp;
        AvnNextLTProRegTextView tvName, tvAdmin, tvStatus;
        LinearLayout linearLayout;

        public ViewHolderGroupInfo(View itemView) {
            super(itemView);

            linearLayout = itemView.findViewById(R.id.linear);
            ivUserDp = itemView.findViewById(R.id.ivUserDp);
            tvName = itemView.findViewById(R.id.tvName);
            tvAdmin = itemView.findViewById(R.id.tvAdmin);
            tvStatus = itemView.findViewById(R.id.tvStatus);
        }
    }

    public interface AtInfoAdapterItemClickListener {
        void onItemClick(GroupMembersPojo member, int position);
    }

    public void setChatListItemClickListener(At_InfoAdapter.AtInfoAdapterItemClickListener listener) {
        this.listener = listener;
    }

}

