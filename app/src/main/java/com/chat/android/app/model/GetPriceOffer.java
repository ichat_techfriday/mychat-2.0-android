package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetPriceOffer implements Serializable{
    @SerializedName("Price_Offer")
    public PriceOffer Price_Offer;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public PriceOffer getPrice_Offer() {
        return Price_Offer;
    }

    public void setPrice_Offer(PriceOffer price_Offer) {
        Price_Offer = price_Offer;
    }

    public class PriceOffer implements Serializable {

        @SerializedName("Error_Code")
        public String error_Code;
        @SerializedName("Has_Errors")
        public boolean has_Errors;
        @SerializedName("Error_Description")
        public String error_Description;
        @SerializedName("Price_Offer_Code")
        public String price_Offer_Code;
        @SerializedName("DTM_ID")
        public int dTM_ID;
        @SerializedName("Payout_Currency")
        public String payout_Currency;
        @SerializedName("Payout_Amount")
        public int payout_Amount;
        @SerializedName("Paying_Currency")
        public String paying_Currency;
        @SerializedName("Paying_Amount")
        public int paying_Amount;
        @SerializedName("Total_Charges")
        public int total_Charges;
        @SerializedName("Exchange_Rate")
        public int exchange_Rate;
        @SerializedName("Available_Limit")
        public int available_Limit;
        @SerializedName("Total_Paying")
        public int total_Paying;
        @SerializedName("Total_Discount")
        public int total_Discount;
        @SerializedName("Agent_Charges_Share")
        public int agent_Charges_Share;
        @SerializedName("Agent_FX_Charges_Share")
        public int agent_FX_Charges_Share;

    }



}


