package com.chat.android.app.activity;


import static com.chat.android.utils.DialogUtils.dismiss;

import static org.webrtc.ContextUtils.getApplicationContext;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.model.ApiRequestModel;
import com.chat.android.app.model.Contact_Info;
import com.chat.android.app.model.GetBankBranches;
import com.chat.android.app.model.GetBankPayoutLocations;
import com.chat.android.app.model.GetCities;
import com.chat.android.app.model.GetDestinations;
import com.chat.android.app.model.NationalityModel;
import com.chat.android.app.model.SaveContactModel;
import com.chat.android.app.model.Save_Contact_Bank;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.FriendModel;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.StorageUtility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class SendPaymentFragmentStep4 extends Fragment implements View.OnClickListener, INetworkResponseListener {

    private ImageButton icNext;
    private ImageButton icBack;
    private TextView tv_City;
    private TextView tv_Bank;
    private EditText tvFirstName;
    private EditText tvLastName;
    private Button cancel_btn;

    private EditText tvMiddleName;
    private EditText tvMobile;
    private EditText tvAddress;
    private EditText tvAccountNumber;
    private EditText tvRoutingABA;
    private EditText tvSwiftCode;
    private EditText tvSortCode;
    private GetDestinations.Country selectedCountry;
    private GetBankPayoutLocations.AvailableBanks selectedBankLocation;
    private GetBankBranches.AllBranches selectedBranchBranch;
    private GetCities.AllCity selectedCity;
    private EditText spinnerBranch;
    private ProgressDialog progressDialog;
    private String payingAmount;
    private List<GetBankBranches.AllBranches> branchesList = new ArrayList<>();
    ArrayAdapter<NationalityModel> bank_filter_adapter;
    private AutoCompleteTextView spinnerNationality;
    private List<NationalityModel> nationalityModelList = new ArrayList<>();
    private String mCountryCode;
    private boolean mBranchesAvailable = false;
    public AlertDialog.Builder builderSingle;
    String json;
    AlertDialog ad;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_send_payment_step4, container, false);
        initViews(view);
        initValues();
        return view;
    }

    private void initViews(View view) {
        icNext = view.findViewById(R.id.icNext);
        icBack = view.findViewById(R.id.icBack);
        tv_City = view.findViewById(R.id.city);
        cancel_btn = view.findViewById(R.id.cancel_btn);
        tv_Bank = view.findViewById(R.id.bank);
        tvFirstName = view.findViewById(R.id.tvFirstName);
        tvLastName = view.findViewById(R.id.tvLastName);
        tvAddress = view.findViewById(R.id.tvAddress);
        tvMiddleName = view.findViewById(R.id.tvMiddleName);
        tvMobile = view.findViewById(R.id.tvMobile);
        spinnerNationality = view.findViewById(R.id.spinnerNationality);
        tvAccountNumber = view.findViewById(R.id.et_account_number);
        tvRoutingABA = view.findViewById(R.id.routing_ABA);
        tvSwiftCode = view.findViewById(R.id.swift_code);
        tvSortCode = view.findViewById(R.id.sort_code);
        spinnerBranch = view.findViewById(R.id.spinnerBranch);
        icNext.setOnClickListener(this);
        icBack.setOnClickListener(this);
        Bundle b = getArguments();
        selectedCountry = (GetDestinations.Country) b.getSerializable("selectedCountry");
        selectedBankLocation = (GetBankPayoutLocations.AvailableBanks) b.getSerializable("selectedBankLocation");
        selectedCity = (GetCities.AllCity) b.getSerializable("selectedCity");
        payingAmount = (String) b.getString("payingAmount");
        tv_City.setText(selectedCity.cityName);
        tv_Bank.setText(selectedBankLocation.bankName);
        readExcelFileFromAssets();
        resetOptions();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        ApiRequestModel apiRequestModel = new ApiRequestModel(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getContext()).getCashPlusToken(), selectedCountry.countryCode, selectedBankLocation.bankCode, true);
        ApiCalls.getInstance(getContext()).GetBankBranches(getContext(), apiRequestModel, this);
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_exit_dialog();
            }
        });
    }

    public void show_exit_dialog(){
        final CustomAlertDialog dialog = new CustomAlertDialog();
        String msg = "Do you want to cancel the transaction?";
        dialog.setMessage(msg);

        dialog.setPositiveButtonText("Yes");
        dialog.setNegativeButtonText("No");

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                //String data = SendPaymentActivity.model;
                String model = StorageUtility.getDataFromPreferences(getContext(), "send_pay", "");
                Gson gson = new Gson();
                FriendModel friendModel = gson.fromJson(model, FriendModel.class);
                Intent intent = new Intent(getContext(), ChatPageActivity.class);
                intent.putExtra("receiverUid", friendModel.getNumberInDevice());
                intent.putExtra("receiverName", friendModel.getFirstName());
                intent.putExtra("documentId", friendModel.get_id());
                intent.putExtra("Username", friendModel.getFirstName());
                intent.putExtra("Image", friendModel.getAvatarImageUrl());
                intent.putExtra("msisdn", friendModel.getMsisdn());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getFragmentManager(), "exit transaction");
    }


    private void resetOptions() {
        if (selectedBankLocation != null) {
            if (selectedBankLocation.routingNoABA_Code_Required)
                tvRoutingABA.setVisibility(View.VISIBLE);
            if (selectedBankLocation.bank_Branch_Required)
                spinnerBranch.setVisibility(View.VISIBLE);
            if (selectedBankLocation.bIC_or_Swift_Code_Required)
                tvSwiftCode.setVisibility(View.VISIBLE);
            if (selectedBankLocation.sort_Code_Required)
                tvSortCode.setVisibility(View.VISIBLE);
        }
    }

    private void initValues() {
        spinnerBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBranch();
            }
        });
        spinnerNationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNationality();
            }
        });

    }

    public void readExcelFileFromAssets() {
        try {
            InputStream myInput;
            AssetManager assetManager = getActivity().getAssets();
            myInput = assetManager.open("nationalities.xls");
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);
            Iterator<Row> rowIter = mySheet.rowIterator();
            int rowno = 0;
            while (rowIter.hasNext()) {
                HSSFRow myRow = (HSSFRow) rowIter.next();
                if (rowno != 0) {
                    Iterator<Cell> cellIter = myRow.cellIterator();
                    int colno = 0;
                    String code = "", name = "", nationality = "";
                    while (cellIter.hasNext()) {
                        HSSFCell myCell = (HSSFCell) cellIter.next();
                        if (colno == 0) {
                            code = myCell.toString();
                        } else if (colno == 1) {
                            name = myCell.toString();
                        } else if (colno == 2) {
                            nationality = myCell.toString();
                        }
                        colno++;
                    }
                    NationalityModel nationalityModel = new NationalityModel(code, name, nationality);
                    nationalityModelList.add(nationalityModel);
                }

                rowno++;
            }

        } catch (Exception e) {
            Log.e("", "error " + e.toString());
        }
    }


    private void setNationality() {
        builderSingle = new AlertDialog.Builder(getContext());

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.listview_dialog, null);
        final ListView listview = (ListView) dialogView.findViewById(R.id.select_dialog_listview);
        final TextView title = (TextView)dialogView.findViewById(R.id.title);
        title.setText("Select Country");
        final EditText search_edit = (EditText)dialogView.findViewById(R.id.search_edit);
        search_edit.setHint("Select Country");
        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                if (!editable.toString().trim().equals("")) {

                    Gson gson = new Gson();
                    json = gson.toJson(nationalityModelList);
                    JSONArray newArray = new JSONArray();
                    try {
                        JSONArray jsonArray = new JSONArray(json);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject currentElement = jsonArray.getJSONObject(i);
                            String country = currentElement.getString("Nationalty");
                            String country_lwr = country.toLowerCase();
                            String query = editable.toString().toLowerCase();
                            if (country_lwr.contains(query)) {
                                newArray.put(currentElement);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Type listType = new TypeToken<List<NationalityModel>>() {
                    }.getType();
                    List<NationalityModel> myModelList = new Gson().fromJson(String.valueOf(newArray), listType);

                    bank_filter_adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, myModelList);
                    listview.setAdapter(bank_filter_adapter);
                }
            }
        });
        final ImageView search_icon = (ImageView)dialogView.findViewById(R.id.search_icon);
        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setVisibility(View.GONE);
                search_edit.setVisibility(View.VISIBLE);
                search_icon.setVisibility(View.GONE);
            }
        });
        final ArrayAdapter<NationalityModel> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, nationalityModelList);

        listview.setAdapter(adapter);
        builderSingle.setView(dialogView);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                NationalityModel nationalityModel;
                if (json!=null && !json.equals("")){
                    nationalityModel = bank_filter_adapter.getItem(i);
                }else {
                    nationalityModel = adapter.getItem(i);
                }
                spinnerNationality.setText(nationalityModel.getNationalty());
                mCountryCode = nationalityModel.getCountryCode();
                json = "";
                ad.dismiss();
            }
        });

//        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
//        builderSingle.setTitle("Select Nationality");
//        final ArrayAdapter<NationalityModel> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, nationalityModelList);
//
//        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                NationalityModel nationalityModel = adapter.getItem(which);
//                spinnerNationality.setText(nationalityModel.getNationalty());
//                mCountryCode = nationalityModel.getCountryCode();
//                dismiss();
//            }
//        });
       ad = builderSingle.show();


    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }

    private void setBranch() {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select Branch");
        final ArrayAdapter<GetBankBranches.AllBranches> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, branchesList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedBranchBranch = adapter.getItem(which);
                spinnerBranch.setText(selectedBranchBranch.BranchName);
                dismiss();
            }
        });
        builderSingle.show();

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle();
    }

    private void setTitle() {
        ((SendPaymentActivity) getActivity()).UpdateTitle("Enter Bank Details");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.icNext:
                if (validateViews()) {

                    SaveContactModel saveContactModel = new SaveContactModel();
                    saveContactModel.setUserName(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser());
                    saveContactModel.setToken(SessionManager.getInstance(getContext()).getCashPlusToken());
                    Contact_Info contact_info = new Contact_Info();
                    contact_info.setAddress(tvAddress.getText().toString());
                    contact_info.setCity(selectedCity.cityName);
                    contact_info.setFirst(tvFirstName.getText().toString());
                    contact_info.setLast(tvLastName.getText().toString());
                    contact_info.setMiddle(tvMiddleName.getText().toString());
                    contact_info.setNationality_Code(mCountryCode);
                    contact_info.setMobile(SessionManager.getInstance(getApplicationContext()).getCashplusUserPhoneNumber());
                    contact_info.setLabel(tvFirstName.getText().toString() + tvLastName.getText().toString());
                    contact_info.setCountry_Code(mCountryCode);
                    contact_info.setContact_ID("");
                    new ArrayList<GetBankPayoutLocations.AvailableBanks>().add(selectedBankLocation);
                    List<Save_Contact_Bank> banklist = new ArrayList<>();
                    Save_Contact_Bank save_contact_bank = new Save_Contact_Bank();
                    save_contact_bank.setBank_Code(selectedBankLocation.bankCode);
                    save_contact_bank.setBank_ID(selectedBankLocation.bank_ID);
                    save_contact_bank.setBank_Service_Provider_Code(selectedBankLocation.service_Provider_Code);
                    save_contact_bank.setBank_Name(selectedBankLocation.bankName);
                    save_contact_bank.setBank_Branch_Required(selectedBankLocation.bank_Branch_Required);
                    save_contact_bank.setBank_SORT_CODE_Required(selectedBankLocation.sort_Code_Required);
                    save_contact_bank.setBank_IBAN_Required(selectedBankLocation.iBAN_Required);
                    save_contact_bank.setBank_SORT_CODE_Required(selectedBankLocation.sort_Code_Required);
                    save_contact_bank.setBank_Service_Provider_Name(selectedBankLocation.service_Provider_Name);
                    save_contact_bank.setBank_Swift_BIC(tvSwiftCode.getText().toString().isEmpty() ? "" : tvSwiftCode.getText().toString());
                    save_contact_bank.setBank_Routing_ABA(tvRoutingABA.getText().toString().isEmpty() ? "" : tvRoutingABA.getText().toString());
                    save_contact_bank.setBank_Account_Number_IBAN(tvAccountNumber.getText().toString().isEmpty() ? "" : tvAccountNumber.getText().toString());
                    save_contact_bank.setBank_SortCode(tvSortCode.getText().toString().isEmpty() ? "" : tvSortCode.getText().toString());
                    save_contact_bank.setBank_BranchID("");
                    save_contact_bank.setBank_Branch_Name("");
                    save_contact_bank.setBank_Branch_Code("");
                    if (mBranchesAvailable) {
                        save_contact_bank.setBank_BranchID(selectedBranchBranch.Branch_ID);
                        save_contact_bank.setBank_Branch_Name(selectedBranchBranch.BranchName);
                        save_contact_bank.setBank_Branch_Code(selectedBranchBranch.BranchCode);
                    }
                    banklist.add(save_contact_bank);
                    contact_info.setBanks(banklist);
                    saveContactModel.setContact_info(contact_info);
                    progressDialog = new ProgressDialog(getContext());
                    progressDialog.setMessage("Saving Contact ...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    Gson gson = new Gson();
                    String json = gson.toJson(saveContactModel);
                    ApiCalls.getInstance(getContext()).SaveContact(getContext(), saveContactModel, this);
                    SendPaymentFragmentStep3 sendPaymentFragmentStep3 = new SendPaymentFragmentStep3();
                    ((SendPaymentActivity) getActivity()).OpenFragment(sendPaymentFragmentStep3);
                }else {
                    Toast.makeText(getContext(), getResources().getString(R.string.field_must_not_be_empty), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.icBack:
                ((SendPaymentActivity) getActivity()).onBackPressed();
                break;

        }

    }

    public boolean validateViews(){
        if (TextUtils.isEmpty(tvFirstName.getText().toString())) {
            return false;
        }
//        if (TextUtils.isEmpty(tvMiddleName.getText().toString())) {
//            return false;
//        }
        if (TextUtils.isEmpty(tvLastName.getText().toString())) {
            return false;
        }
        if (TextUtils.isEmpty(spinnerNationality.getText().toString())) {
            return false;
        }
        if (TextUtils.isEmpty(tvMobile.getText().toString())) {
            return false;
        }
        if (TextUtils.isEmpty(selectedCity.cityName)){
            return false;
        }
        if (TextUtils.isEmpty(selectedBankLocation.bankName)) {
            return false;
        }
        if (TextUtils.isEmpty(tvAddress.getText().toString())) {
            return false;
        }
        if (TextUtils.isEmpty(tvAccountNumber.getText().toString())) {
            return false;
        }
        return true;
    }

    @Override
    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        progressDialog.dismiss();
        switch (responseForRequest) {
            case Constants.Branches:
                if (status) {

                    GetBankBranches bankBranches = (GetBankBranches) body;
                    if (bankBranches.getErrorCode().equals("0")) {
                        if (bankBranches.getBankBranches() != null) {
                            branchesList = bankBranches.getBankBranches().allBankBranches;
                            if (selectedBankLocation.bank_Branch_Required)
                                spinnerBranch.setVisibility(View.VISIBLE);
                            mBranchesAvailable = true;
                        }
                    } else
                        Toast.makeText(getContext(), bankBranches.getErrorDescription(), Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
                break;
            case Constants.SaveContact:
                if (status) {
                    Gson gson = new Gson();
                    String response_str = gson.toJson(body);
                    Log.e("ServerResp", response_str);

                    SaveContactModel saveContactModel = (SaveContactModel) body;
                    String model = gson.toJson(body);
                    Log.e("ModelResp", model);

                    if (saveContactModel.getErrorCode().equals("0")) {
                        SendPaymentFragmentStep3 sendPaymentFragmentStep3 = new SendPaymentFragmentStep3();
                        Bundle b1 = new Bundle();
                        b1.putSerializable("name", tvFirstName.getText().toString());
                        b1.putSerializable("selectedCountry", selectedCountry);
                        b1.putSerializable("selectedBankLocation", selectedBankLocation);
                        b1.putSerializable("selectedCity", selectedCity);
                        sendPaymentFragmentStep3.setArguments(b1);
                        ((SendPaymentActivity) getActivity()).OpenFragment(sendPaymentFragmentStep3);

                    } else
                        Toast.makeText(requireContext(), saveContactModel.getErrorDescription(), Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
                break;

        }
    }
}