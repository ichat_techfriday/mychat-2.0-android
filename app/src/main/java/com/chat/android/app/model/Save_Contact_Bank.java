package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Save_Contact_Bank implements Serializable {

    @SerializedName("Bank_Account_Number_IBAN")
    String Bank_Account_Number_IBAN;
    @SerializedName("Bank_BranchID")
    String Bank_BranchID;
    @SerializedName("Bank_Branch_Code")
    String Bank_Branch_Code;
    @SerializedName("Bank_Branch_Name")
    String Bank_Branch_Name;
    @SerializedName("Bank_Code")
    String Bank_Code;
    @SerializedName("Bank_ID")
    String Bank_ID;
    @SerializedName("Bank_Name")
    String Bank_Name;
    @SerializedName("Bank_Routing_ABA")
    String Bank_Routing_ABA;
    @SerializedName("Bank_Service_Provider_Code")
    String Bank_Service_Provider_Code;
    @SerializedName("Bank_Service_Provider_Name")
    String Bank_Service_Provider_Name;
    @SerializedName("Bank_SortCode")
    String Bank_SortCode;
    @SerializedName("Bank_Swift_BIC")
    String Bank_Swift_BIC;
    @SerializedName("Bank_SWIFT_Required")
    boolean Bank_SWIFT_Required;
    @SerializedName("Bank_Branch_Required")
    boolean Bank_Branch_Required;
    @SerializedName("Bank_IBAN_Required")
    boolean Bank_IBAN_Required;
    @SerializedName("Bank_SORT_CODE_Required")
    boolean Bank_SORT_CODE_Required;
    @SerializedName("Bank_ROUTING_Required")
    boolean Bank_ROUTING_Required;

    public String getBank_Account_Number_IBAN() {
        return Bank_Account_Number_IBAN;
    }

    public void setBank_Account_Number_IBAN(String bank_Account_Number_IBAN) {
        Bank_Account_Number_IBAN = bank_Account_Number_IBAN;
    }

    public String getBank_BranchID() {
        return Bank_BranchID;
    }

    public void setBank_BranchID(String bank_BranchID) {
        Bank_BranchID = bank_BranchID;
    }

    public String getBank_Branch_Code() {
        return Bank_Branch_Code;
    }

    public void setBank_Branch_Code(String bank_Branch_Code) {
        Bank_Branch_Code = bank_Branch_Code;
    }

    public String getBank_Branch_Name() {
        return Bank_Branch_Name;
    }

    public void setBank_Branch_Name(String bank_Branch_Name) {
        Bank_Branch_Name = bank_Branch_Name;
    }

    public String getBank_Code() {
        return Bank_Code;
    }

    public void setBank_Code(String bank_Code) {
        Bank_Code = bank_Code;
    }

    public String getBank_ID() {
        return Bank_ID;
    }

    public void setBank_ID(String bank_ID) {
        Bank_ID = bank_ID;
    }

    public String getBank_Name() {
        return Bank_Name;
    }

    public void setBank_Name(String bank_Name) {
        Bank_Name = bank_Name;
    }

    public String getBank_Routing_ABA() {
        return Bank_Routing_ABA;
    }

    public void setBank_Routing_ABA(String bank_Routing_ABA) {
        Bank_Routing_ABA = bank_Routing_ABA;
    }

    public String getBank_Service_Provider_Code() {
        return Bank_Service_Provider_Code;
    }

    public void setBank_Service_Provider_Code(String bank_Service_Provider_Code) {
        Bank_Service_Provider_Code = bank_Service_Provider_Code;
    }

    public String getBank_SortCode() {
        return Bank_SortCode;
    }

    public void setBank_SortCode(String bank_SortCode) {
        Bank_SortCode = bank_SortCode;
    }

    public String getBank_Swift_BIC() {
        return Bank_Swift_BIC;
    }

    public void setBank_Swift_BIC(String bank_Swift_BIC) {
        Bank_Swift_BIC = bank_Swift_BIC;
    }

    public boolean isBank_SWIFT_Required() {
        return Bank_SWIFT_Required;
    }

    public void setBank_SWIFT_Required(boolean bank_SWIFT_Required) {
        Bank_SWIFT_Required = bank_SWIFT_Required;
    }

    public boolean isBank_Branch_Required() {
        return Bank_Branch_Required;
    }

    public void setBank_Branch_Required(boolean bank_Branch_Required) {
        Bank_Branch_Required = bank_Branch_Required;
    }

    public boolean isBank_IBAN_Required() {
        return Bank_IBAN_Required;
    }

    public void setBank_IBAN_Required(boolean bank_IBAN_Required) {
        Bank_IBAN_Required = bank_IBAN_Required;
    }

    public boolean isBank_SORT_CODE_Required() {
        return Bank_SORT_CODE_Required;
    }

    public void setBank_SORT_CODE_Required(boolean bank_SORT_CODE_Required) {
        Bank_SORT_CODE_Required = bank_SORT_CODE_Required;
    }

    public boolean isBank_ROUTING_Required() {
        return Bank_ROUTING_Required;
    }

    public String getBank_Service_Provider_Name() {
        return Bank_Service_Provider_Name;
    }

    public void setBank_Service_Provider_Name(String bank_Service_Provider_Name) {
        Bank_Service_Provider_Name = bank_Service_Provider_Name;
    }

    public void setBank_ROUTING_Required(boolean bank_ROUTING_Required) {
        Bank_ROUTING_Required = bank_ROUTING_Required;
    }
}
