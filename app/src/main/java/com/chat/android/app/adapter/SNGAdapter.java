package com.chat.android.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.model.FriendModel;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


/**
 */
public class SNGAdapter extends RecyclerView.Adapter<SNGAdapter.MyViewHolder>
        implements Filterable {

    private Context context;
    private List<FriendModel> mDisplayedValues;
    private List<FriendModel> mOriginalValues;
    private Session session;
    private static final String TAG = "SNGAdapter";
    Getcontactname getcontactname;
    private int blockedContactColor, unblockedContactColor;


    public SNGAdapter(Context context, List<FriendModel> data) {
        this.context = context;
        this.mDisplayedValues = data;
        this.mOriginalValues = data;
        session = new Session(context);
        getcontactname = new Getcontactname(context);
        blockedContactColor = ContextCompat.getColor(context, R.color.blocked_user_bg);
        unblockedContactColor = ContextCompat.getColor(context, android.R.color.transparent);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

//        if (position == 0 || position > 0) {
        final FriendModel contact = mDisplayedValues.get(position);

        Log.d(TAG, "Name: "+contact.getFirstName()+"ID"+contact.get_id());


        holder.tvName.setText(contact.getFirstName());
        holder.ivUser.setImageResource(R.drawable.ic_placeholder_black);
        String userId = contact.get_id();

        try {
            if (contact.getStatus().contentEquals("")) {
            /* Default Status of each user */
                holder.tvStatus.setText(context.getResources().getString(R.string.status_not_available));
            } else if (userId != null && !userId.equals("")) {
                //getcontactname.setProfileStatusText(holder.tvStatus, userId, contact.getStatus(), false);
                byte[] bytes = tryDecodeBase64(contact.getStatus());
                if (bytes!=null){
                    String str = new String(bytes, StandardCharsets.UTF_8);
                    holder.tvStatus.setText(str);
                }else{
                    holder.tvStatus.setText(contact.getStatus());
                }
            } else {
                holder.tvStatus.setText("");
            }
        } catch (Exception e) {
            holder.tvStatus.setText(context.getResources().getString(R.string.status_not_available));
        }

        String imageUrl = mDisplayedValues.get(position).getAvatarImageUrl();
        if(imageUrl != null && !imageUrl.isEmpty()) {
            imageUrl = AppUtils.getValidProfilePath(imageUrl);
            AppUtils.loadImageSmooth(context, imageUrl, holder.ivUser, 100, R.drawable.ic_placeholder_black);
        } else {
            holder.ivUser.setImageResource(R.drawable.ic_placeholder_black);
        }

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        if (contactDB_sqlite.getBlockedStatus(userId, false).equals("1")) {
            holder.itemView.setBackgroundColor(blockedContactColor);
        } else {
            holder.itemView.setBackgroundColor(unblockedContactColor);
        }

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_item_selected, parent, false);
        return new MyViewHolder(itemView);
    }


    public FriendModel getItem(int position) {
        return mDisplayedValues.get(position);
    }

    public List<FriendModel> getDisplayedList(int position) {
        return mDisplayedValues;
    }


    @Override
    public int getItemCount() {
        return mDisplayedValues.size();
    }


    public void updateInfo(List<FriendModel> aitem) {
        this.mDisplayedValues = aitem;
        notifyDataSetChanged();
    }

    public byte[] tryDecodeBase64(String path) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                return Base64.getDecoder().decode(path);
            }
            return null;
        } catch(IllegalArgumentException e) {
            return null;
        }
    }




    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mDisplayedValues = mOriginalValues;
                } else {
                    List<FriendModel> filteredList = new ArrayList<>();
                    for (FriendModel row : mOriginalValues) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getFirstName().toLowerCase().contains(charString.toLowerCase()) || row.getMsisdn().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    mDisplayedValues = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDisplayedValues;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDisplayedValues = (ArrayList<FriendModel>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        protected TextView tvName;
        protected EmojiconTextView tvStatus;
        protected CircleImageView ivUser;
        CheckBox selected;

        public MyViewHolder(View view) {
            super(view);
            Typeface face2 = CoreController.getInstance().getAvnNextLTProRegularTypeface();
            tvName = view.findViewById(R.id.userName_contacts);
            tvStatus = view.findViewById(R.id.status_contacts);
            selected = view.findViewById(R.id.selectedmember);
            ivUser = view.findViewById(R.id.userPhoto_contacts);
            selected = view.findViewById(R.id.selectedmember);
            //tvName.setTextColor(Color.parseColor("#3f3f3f"));
            //tvStatus.setTypeface(face2);
            //tvStatus.setTextColor(Color.parseColor("#808080"));

        }
    }



}