package com.chat.android.app.activity;


import static com.chat.android.utils.DialogUtils.dismiss;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.dialog.CustomAlertDialog;
import com.chat.android.app.model.ApiRequestModel;
import com.chat.android.app.model.GetBankPayoutLocations;
import com.chat.android.app.model.GetCashPayoutLocations;
import com.chat.android.app.model.GetCities;
import com.chat.android.app.model.GetDestinations;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.FriendModel;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.StorageUtility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class SendPaymentFragmentStep2 extends Fragment implements View.OnClickListener, INetworkResponseListener {


    private ImageButton icNext;
    private ImageButton icBack;
    private Button cancel_btn;

    private EditText spinnerBankLocation;
    private EditText spinnerCashLocation;
    private EditText spinnerCity;
    private ProgressDialog progressDialog;
    private GetCities.AllCity selectedCity= new GetCities.AllCity();
    private GetDestinations.AvailablePayingDetail selectedExchangeBlock;
    private GetDestinations.Country selectedCountry;
    private GetDestinations.AvailableTransferMethod selectedTransferMethod;
    private GetCashPayoutLocations.AvailableLocation selectedCashLocation;
    private GetBankPayoutLocations.AvailableBanks selectedBankLocation;
    private List<GetCashPayoutLocations.AvailableLocation> cashPayoutList = new ArrayList<>();
    private List<GetBankPayoutLocations.AvailableBanks> bankPayoutList = new ArrayList<>();
    private List<GetCities.AllCity> citiesList = new ArrayList<>();
    private String payingAmount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send_payment_step2, container, false);
        initViews(view);
        initValues();
        return view;
    }

    private void initViews(View view) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCancelable(false);
        cancel_btn = view.findViewById(R.id.cancel_btn);
        spinnerCity = view.findViewById(R.id.city);
        spinnerBankLocation = view.findViewById(R.id.spinnerBankLocation);
        spinnerCashLocation = view.findViewById(R.id.spinnerCashLocation);
        icNext = view.findViewById(R.id.icNext);
        icBack = view.findViewById(R.id.icBack);
        icNext.setOnClickListener(this);
        icBack.setOnClickListener(this);
        Bundle b = getArguments();
        selectedCountry = (GetDestinations.Country) b.getSerializable("selectedCountry");
        progressDialog.show();
        ApiRequestModel apiRequestModel = new ApiRequestModel(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getContext()).getCashPlusToken(), selectedCountry.countryCode, true);
        ApiCalls.getInstance(getContext()).GetCities(getContext(), apiRequestModel, this);
        RequestLocations();
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_exit_dialog();
            }
        });
    }

    public void show_exit_dialog(){
        final CustomAlertDialog dialog = new CustomAlertDialog();
        String msg = "Do you want to cancel the transaction?";
        dialog.setMessage(msg);

        dialog.setPositiveButtonText("Yes");
        dialog.setNegativeButtonText("No");

        dialog.setCustomDialogCloseListener(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                //String data = SendPaymentActivity.model;
                String model = StorageUtility.getDataFromPreferences(getContext(), "send_pay", "");
                Gson gson = new Gson();
                FriendModel friendModel = gson.fromJson(model, FriendModel.class);
                Intent intent = new Intent(getContext(), ChatPageActivity.class);
                intent.putExtra("receiverUid", friendModel.getNumberInDevice());
                intent.putExtra("receiverName", friendModel.getFirstName());
                intent.putExtra("documentId", friendModel.get_id());
                intent.putExtra("Username", friendModel.getFirstName());
                intent.putExtra("Image", friendModel.getAvatarImageUrl());
                intent.putExtra("msisdn", friendModel.getMsisdn());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });
        dialog.show(getFragmentManager(), "exit transaction");
    }

    private void initValues() {
//        spinnerCity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setCities();
//            }
//        });
        spinnerCashLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCashPayoutSpinner();
            }
        });
        spinnerBankLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBankPayoutSpinner();
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle();

    }

    private void setTitle() {
        ((SendPaymentActivity) getActivity()).UpdateTitle(" Choose Payout Location");
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle();
    }


    private void setCities() {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select Cities");
        final ArrayAdapter<GetCities.AllCity> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, citiesList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedCity = adapter.getItem(which);
                spinnerCity.setText(selectedCity.cityName);
                //     RequestLocations();
                dismiss();
            }
        });
        builderSingle.show();


    }

    private void setCashPayoutSpinner() {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select CashOut Location");
        final ArrayAdapter<GetCashPayoutLocations.AvailableLocation> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, cashPayoutList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedCashLocation = adapter.getItem(which);
                spinnerCashLocation.setText(selectedCashLocation.payout_Location_Name);

                dismiss();
            }
        });
        builderSingle.show();


    }

    private void setBankPayoutSpinner() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getContext());
        builderSingle.setTitle("Select BankPayout Location");
        final ArrayAdapter<GetBankPayoutLocations.AvailableBanks> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, bankPayoutList);

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedBankLocation = adapter.getItem(which);
                spinnerBankLocation.setText(selectedBankLocation.bankName);
                dismiss();
            }
        });
        builderSingle.show();

    }


    private void RequestLocations() {
        progressDialog.show();
        spinnerBankLocation.setVisibility(View.VISIBLE);
        ApiRequestModel apiRequestModel2 = new ApiRequestModel(SessionManager.getInstance(getContext()).getPhoneNumberOfCurrentUser(), SessionManager.getInstance(getContext()).getCashPlusToken(), selectedCountry.countryCode, true);
        ApiCalls.getInstance(getContext()).GetBanksPayoutLocations(getContext(), apiRequestModel2, this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.icNext:
                if (!spinnerCity.getText().toString().equals("")) {

                    if (selectedBankLocation != null) {
                        selectedCity.cityName = spinnerCity.getText().toString();
                        SendPaymentFragmentStep4 sendPaymentFragmentStep4 = new SendPaymentFragmentStep4();
                        Bundle b1 = new Bundle();
                        b1.putSerializable("selectedCountry", selectedCountry);
                        b1.putSerializable("selectedBankLocation", selectedBankLocation);
                        b1.putSerializable("selectedCity", selectedCity);
                        sendPaymentFragmentStep4.setArguments(b1);
                        ((SendPaymentActivity) getActivity()).OpenFragment(sendPaymentFragmentStep4);
                    } else {

                        Toast.makeText(getContext(), "Select Payout Location", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.field_must_not_be_empty), Toast.LENGTH_SHORT).show();

                }
                break;
            case R.id.icBack:
                getActivity().onBackPressed();
                break;


        }


    }

    @Override
    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        progressDialog.dismiss();
        switch (responseForRequest) {
            case Constants.Cities:
                if (status) {

                    GetCities cities = (GetCities) body;
                    if (cities.getErrorCode().equals("0"))
                        citiesList = cities.getCities().allCities;
                    else
                        Toast.makeText(getContext(), cities.getErrorDescription(), Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;
            case Constants.CashPayoutLocations:
                if (status) {

                    GetCashPayoutLocations cashPayoutLocations = (GetCashPayoutLocations) body;
                    cashPayoutList = cashPayoutLocations.getPayoutLocations().availableLocations;

                } else
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;
            case Constants.BankPayoutLocations:
                if (status) {

                    GetBankPayoutLocations bankPayoutLocations = (GetBankPayoutLocations) body;
                    bankPayoutList = bankPayoutLocations.getBanks().allBanks;
                } else
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                break;
        }
    }

}