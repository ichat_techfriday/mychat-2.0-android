package com.chat.android.app.activity

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.app.Activity
import android.app.ActivityManager
import android.app.NotificationManager
import android.app.ProgressDialog
import android.content.*
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.*
import android.preference.PreferenceManager
import android.provider.ContactsContract
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.*
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.BitmapTypeRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import com.chat.android.R
import com.chat.android.app.adapter.At_InfoAdapter
import com.chat.android.app.adapter.MessageAdapter
import com.chat.android.app.calls.CallMessage
import com.chat.android.app.calls.CallsActivity
import com.chat.android.app.dialog.*
import com.chat.android.app.model.BottomSheetModel
import com.chat.android.app.model.GroupMemberFetched
import com.chat.android.app.utils.*
import com.chat.android.app.web.IChatWithoutHeaderAPIController
import com.chat.android.app.widget.AvnNextLTProRegEditText
import com.chat.android.core.*
import com.chat.android.core.database.ContactDB_Sqlite
import com.chat.android.core.database.MessageDbController
import com.chat.android.core.message.*
import com.chat.android.core.model.*
import com.chat.android.core.scimbohelperclass.ScimboImageUtils
import com.chat.android.core.scimbohelperclass.ScimboRegularExp
import com.chat.android.core.scimbohelperclass.ScimboUtilities
import com.chat.android.core.service.Constants
import com.chat.android.core.service.ContactsSync
import com.chat.android.core.socket.MessageService
import com.chat.android.core.socket.NotificationUtil
import com.chat.android.core.socket.SocketManager
import com.chat.android.core.uploadtoserver.FetchDownloadManager
import com.chat.android.core.uploadtoserver.FileDownloadListener
import com.chat.android.core.uploadtoserver.FileUploadDownloadManager
import com.chat.android.utils.*
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.ui.PlaybackControlView
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import de.hdodenhof.circleimageview.CircleImageView
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.models.sort.SortingTypes
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText
import io.codetail.animation.SupportAnimator
import io.codetail.animation.ViewAnimationUtils
import kotlinx.coroutines.*
import me.leolin.shortcutbadger.ShortcutBadger
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import okhttp3.ResponseBody
import org.appspot.apprtc.CallActivity
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Jsoup
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class ChatPageActivity : CoreActivity(), View.OnClickListener,
    AdapterView.OnItemLongClickListener, View.OnLongClickListener,
    MuteAlertDialog.MuteAlertCloseListener, AdapterView.OnItemClickListener, ItemClickListener,
    ScreenShotDetector.ScreenShotListener, FileDownloadListener {
    var pathList: ArrayList<Imagepath_caption>? = null
    var mAgoraCallingKey: String? = null
    var imagepath_caption: Imagepath_caption? = null
    private var mSelectedPath: String? = null
    private var mSelectedType: String? = null
    var mContactSaved = false
    private var isGroupMemebersNotLoaded = false
    var mListviewScrollBottom = false
    private var mLnrcallStatus: LinearLayout? = null
    var QuotedTranslatedText: String? = null
    var context: Context? = this
    private val EXIT_GROUP_REQUEST_CODE = 11
    private val AUDIO_RECORD_PERMISSION_REQUEST_CODE = 14
    private val MUTE_ACTIVITY = 18
    private val ADD_CONTACT = 21
    private val REQUEST_CODE_FORWARD_MSG = 15
    var reply = false
    var isSelectedWithUnStarMsg: Boolean? = null
    var hasGroupInfo = false
    var lastvisibleitempostion = 0
    var unreadmsgcount = 0
    var Message_id = ""
    var Group_Message_id = ""
    var getcontactname: Getcontactname? = null
    var convId: String? = null
    var docId = ""
    var mFirstVisibleMsgId: String? = null
    var chatType: String? = null
    var mypath: String? = null
    var audioRecordPath: String? = null
    var receiverUid: String? = null
    var backfrom = false
    var canShowLastSeen = false
    var isLastSeenCalled = false
    var mCurrentUserData: GroupMembersPojo? = null
    var totla_progress = 3
    var session: Session? = null
    var audioURI: Uri? = null
    var contacts: ArrayList<ContactToSend>? = null
    var sharedprf_video_uploadprogress: ShortcutBadgeManager? = null
    var fromLastTypedAt: Long = 0
    var toLastTypedAt: Long = 0
    var lastViewStatusSentAt: Long = 0
    var toTypingHandler: Handler? = Handler(Looper.getMainLooper())
    val handler = Handler(Looper.getMainLooper())
    var ContactString: String? = null
    var name: String? = null
    var number: String? = null
    var date: String? = null
    var r1messagetoreplay: RelativeLayout? = null
    var RelativeLayout_group_delete: RelativeLayout? = null
    var text_lay_out: RelativeLayout? = null
    var rlSend: RelativeLayout? = null
    var cameraphoto: ImageView? = null
    var videoimage: ImageView? = null
    var audioimage: ImageView? = null
    var personimage: ImageView? = null
    var cameraimage: ImageView? = null
    var sentimage: ImageView? = null
    var replytype: String? = null
    var value: String? = null
    var ReplySender: String? = ""
    var messageold = ""
    var mDataId: String? = null
    var mRawContactId: String? = null
    var isAlrdychatlocked = false
    var emai1send: ImageView? = null
    var gmailsend: ImageView? = null
    var myTemp: Bitmap? = null
    var emailChatlock: String? = null
    var recemailChatlock: String? = null
    var recPhoneChatlock: String? = null
    var contactDB_sqlite: ContactDB_Sqlite? = null
    var myChronometer: Chronometer? = null
    var image_to: ImageView? = null
    var avoid_twotimescall = 0
    var slidetocencel: TextView? = null
    var ivCancelRecording: ImageView? = null
    var rootView: View? = null

    //EmojIconActions emojIcon;
    var imgDecodableString: String? = null
    var Search1: AvnNextLTProRegEditText? = null
    var add_contact: TextView? = null
    var block_contact: TextView? = null
    var report_spam: TextView? = null
    var pastVisibleItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var unreadcount: TextView? = null
    var setscrolllastpoition = 0
    var sb: StringBuilder? = null
    var receiver: Receiver? = null
    var isMassChat = false
    private var chat_list: RecyclerView? = null
    private var Delete_Type = ""
    private var messageDatabase: MessageDbController? = null
    private var from: String? = null
    private var mChatData: ArrayList<MessageItemChat?>? = null

    // private NewLayoutChangeChatAdapter madapter;
    private var sessionManager: SessionManager? = null
    private var receiverMsisdn: String? = null
    private var mReceiverName: String? = ""
    private var msgid: String? = null
    private var receiverName: String? = null
    private var receiverpincode: String? = null
    private var mGroupId: String? = null
    private var mCurrentUserId: String? = null
    private var userInfoSession: UserInfoSession? = null
    private var contactname: String? = null
    private var mConvId: String? = ""
    private var uploadDownloadManager: FileUploadDownloadManager? = null
    private var attachment_icon: ImageView? = null
    private var nudgeIcon: ImageView? = null
    private var translateIcon: ImageView? = null
    private val ivTranslate: ImageView? = null
    private var isHidden = true
    private var attachmentLayout: LinearLayout? = null
    private var camera_choose: LinearLayout? = null
    private var image_choose: LinearLayout? = null
    private var video_choose: LinearLayout? = null
    private var audio_choose: LinearLayout? = null
    private var document_choose: LinearLayout? = null
    private var location_choose: LinearLayout? = null
    private var send_payment: LinearLayout? = null
    private var contact_choose: LinearLayout? = null
    private var include: Toolbar? = null
    private var mBackImage: ImageView? = null
    private var mReceiverId: String? = null
    private var enableGroupChat = false
    private var gmailintent = false
    private var isMenuBtnClick = false
    private var layout_new: RelativeLayout? = null
    private var longPressMillis: Long = 0
    private var atAdapter: At_InfoAdapter? = null
    private var capture_image: ImageView? = null
    private var selKeybord: ImageView? = null
    private var ivVoiceCall: ImageView? = null
    private var ivVideoCall: ImageView? = null
    private var sendButton: ImageButton? = null
    private var groupUsername: TextView? = null
    private var tvWebLink: TextView? = null
    private var tvWebLinkDesc: TextView? = null
    private var tvWebTitle: TextView? = null
    private var message_old: TextView? = null
    private var Documentimage: ImageView? = null
    private var dateView: TextView? = null
    private var Ifname: TextView? = null
    private var messagesetmedio: TextView? = null
    private var tvBlock: TextView? = null
    private var tvAddToContact: TextView? = null
    private var mymsgid: String? = ""
    private var locationName: String? = null
    private var quotedMessage = ""
    private var imgpath: String? = null
    private var imageAsBytes: String? = null
    private var close: ImageView? = null
    private var sendMessage: EmojiconEditText? = null
    private var deletestarred = false
    var isAudioRecording = false
        private set
    private var mClearChat = false
    private var mCheckedDelete = false
    private var emailgmail: RelativeLayout? = null
    private var record: ImageButton? = null
    private var groupInfoSession: GroupInfoSession? = null
    private var savedMembersList: MutableList<GroupMembersPojo>? = null
    private var unsavedMembersList: MutableList<GroupMembersPojo>? = null
    private var rvGroupMembers: RecyclerView? = null
    var is_telpon_chat = false
    private var myReceiver: MyReceiver? = null
    private var MsgProgressBar: ProgressBar? = null
    var chat_videoview: SimpleExoPlayerView? = null
    var player: SimpleExoPlayer? = null
    var watch: TextWatcher? = object : TextWatcher {
        override fun afterTextChanged(arg0: Editable) {

            //checkLinkDetails(sendMessage.getText().toString().trim());
        }

        override fun beforeTextChanged(
            arg0: CharSequence, arg1: Int, arg2: Int,
            arg3: Int
        ) {
            // TODO Auto-generated method stub
        }

        override fun onTextChanged(cs: CharSequence, a: Int, b: Int, c: Int) {
            // TODO Auto-generated method stub
            MyLog.d(TAG, "onTextChanged: ")
            handleTypingEvent(cs.length)
            if (cs.length > 0) {
                hideMenu()
                if (isGroupChat) {
                    if (cs.length == 1) {
                        val value = cs.toString()
                        if (value.equals("@", ignoreCase = true)) {
                            if (allMembersList!!.size > 0) {
                                rvGroupMembers!!.visibility = View.VISIBLE
                            }
                        }
                    } else {
                        val value = cs.toString()
                        val splittedText = value.split(" ").toTypedArray()
                        var value1 = value
                        if (splittedText.size > 0) value1 = splittedText[splittedText.size - 1]
                        MyLog.d(
                            TAG,
                            "onTextChanged: $value1"
                        )
                        if (value1.contains(" @")) {
                            var haveMembersText = false
                            for (groupMembersPojo: GroupMembersPojo in allMembersList!!) {
                                val name = groupMembersPojo.name
                                val number = groupMembersPojo.msisdn
                                if (name != null && name.contains(cs)) {
                                    haveMembersText = true
                                    break
                                } else if (number != null && number.contains(cs)) {
                                    haveMembersText = true
                                    break
                                }
                            }
                            if (haveMembersText) rvGroupMembers!!.visibility =
                                View.VISIBLE else rvGroupMembers!!.visibility =
                                View.GONE
                        }
                    }
                }
                capture_image!!.visibility = View.GONE
                attachment_icon!!.visibility = View.GONE
                nudgeIcon!!.visibility = View.GONE
                translateIcon!!.visibility = View.GONE
                if (capture_image!!.visibility == View.GONE) {
                }
                sendButton!!.visibility = View.VISIBLE
                record!!.visibility = View.GONE
            } else {
                if (isGroupChat) {
//                    AppUtils.slideDown(rvGroupMembers);
                    rvGroupMembers!!.visibility = View.GONE
                    //                    rvGroupMembers.setVisibility(View.GONE);
                }
                capture_image!!.visibility = View.GONE
                attachment_icon!!.visibility = View.VISIBLE
                //    nudgeIcon.setVisibility(View.VISIBLE);
                translateIcon!!.visibility = View.VISIBLE
                sendButton!!.visibility = View.GONE
                record!!.visibility = View.VISIBLE
            }
        }
    }
    private var webLink: String? = null
    private var webLinkTitle: String? = null
    private var webLinkDesc: String? = null
    private var webLinkImgUrl: String? = null
    private var ivWebLink: ImageView? = null
    private var rlWebLink: RelativeLayout? = null
    private var hasLinkInfo = false
    private var audioRecorder: MediaRecorder? = null
    private var llAddBlockContact: LinearLayout? = null
    private var background: ImageView? = null
    private var chatMenu: Menu? = null
    private val loadingMore = false
    private var mLayoutManager: LinearLayoutManager? = null
    private var iBtnScroll: View? = null
    private val isMessageLoadedOnScroll = true
    private var frameL: FrameLayout? = null
    private var starred_msgid: String? = ""

    //--------------------------Header Intialize------------------------
    private var newMsgIds: ArrayList<String>? = ArrayList()
    private var relativeLayout: View? = null
    private var group_left_layout: RelativeLayout? = null

    // private RelativeLayout back_layout;
    private var user_profile_image: CircleImageView? = null
    private var user_name: TextView? = null
    private var status_textview: TextView? = null
    var toTypingRunnable: Runnable? = Runnable {
        if (status_textview == null) return@Runnable
        val currentTime = Calendar.getInstance().timeInMillis
        if (currentTime > toLastTypedAt) {
            if (!isGroupChat) {
                status_textview!!.text = ""
                status_textview!!.text = resources.getString(R.string.online)
                status_textview!!.visibility = View.GONE
            } else {
                status_textview!!.text = ""
                if (sb != null) {
                    status_textview!!.text = sb
                }
            }
        }
    }

    /*  private RippleView Ripple_Video;
      private RippleView Ripple_call;
  */
    //------------------------Chat Action Intialize-------------------------
    private var overflow: ImageView? = null
    private var name_status_layout: RelativeLayout? = null
    private var user_info_container: RelativeLayout? = null
    private var menu_layout: RelativeLayout? = null
    private var call_layout: LinearLayout? = null
    private var rlChatActions: RelativeLayout? = null
    private var iBtnBack2: ImageView? = null
    private var longpressback: ImageView? = null
    private var replymess: ImageView? = null
    private var copychat: ImageView? = null
    private var showOriginal: ImageView? = null
    private var starred: ImageView? = null
    private var info: ImageView? = null
    private var delete: ImageView? = null
    private var isFirstTimeLoad = true
    private var forward: ImageView? = null
    private var share: ImageView? = null
    private var isFrom: String? = null
    //------------------------------New Record Button---------------------------
    //  private RecordButton record_button;
    // private RecordView record_view;
    private var messageIds: MutableList<String>? = ArrayList()
    private var edit_text_layout: RelativeLayout? = null
    private var mAdapter: MessageAdapter? = null
    private val audioRecordTouchListener: View.OnTouchListener = object : View.OnTouchListener {
        override fun onTouch(pView: View, pEvent: MotionEvent): Boolean {
            pView.onTouchEvent(pEvent)
            if (pEvent.action == MotionEvent.ACTION_UP) {
                try {
                    stopRecording(true)
                } catch (e: RuntimeException) {
                    e.printStackTrace()
                }
                isAudioRecording = false
            }
            return false
        }
    }

    private fun stopRecording(sendAudio: Boolean) {
        //if (audioRecordStarted) {
        val sendAudioPath = audioRecordPath
        audioRecordPath = ""
        //record.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_primary_icon));
        record!!.visibility = View.VISIBLE
        sendButton!!.visibility = View.GONE
        record!!.setImageResource(R.drawable.ic_audio_chat)
        //selEmoji.setImageResource(R.drawable.smile);
        sendMessage!!.visibility = View.VISIBLE
        myChronometer!!.visibility = View.GONE
        image_to!!.visibility = View.GONE
        slidetocencel!!.visibility = View.GONE
        ivCancelRecording!!.visibility = View.GONE
        capture_image!!.visibility = View.GONE
        attachment_icon!!.visibility = View.VISIBLE
        //    nudgeIcon.setVisibility(View.VISIBLE);
        translateIcon!!.visibility = View.VISIBLE
        //                    audioRecorder.stop();
        audioRecorder?.release()
        myChronometer?.stop()
        if (!sendAudio) return
        val file: File
        val newUri: Uri?
        val newPath: String?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            file = File(FileChooser.getPath(this@ChatPageActivity, audioURI))
            newUri = audioURI
            newPath = FileChooser.getPath(this@ChatPageActivity, audioURI)
            audioURI = null
        } else {
            file = File(sendAudioPath)
            newUri = Uri.parse(sendAudioPath)
            newPath = sendAudioPath
        }
        if (file.exists()) {
            val mmr = MediaMetadataRetriever()
            mmr.setDataSource(this@ChatPageActivity, newUri)
            val durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
            if (AppUtils.parseLong(durationStr) < 1000) {
                Toast.makeText(
                    this@ChatPageActivity,
                    resources.getString(R.string.record_atleast_one_second),
                    Toast.LENGTH_SHORT
                ).show()
            } else sendAudioMessage(newPath, durationStr, MessageFactory.AUDIO_FROM_RECORD, false)
        } else Log.d(TAG, "file dont exist")
        isAudioRecording = false
    }

    private val msgComparator: Comparator<MessageItemChat?> =
        object : Comparator<MessageItemChat?> {

            override fun compare(p0: MessageItemChat?, p1: MessageItemChat?): Int {
                val lhsMsgTS = AppUtils.parseLong(p0?.ts)
                val rhsMsgTS = AppUtils.parseLong(p1?.ts)
                return lhsMsgTS.compareTo(rhsMsgTS)
                /*            if (lhsMsgTS > rhsMsgTS) {
                return 1;
            } else {
                return -1;
            }*/
            }
        }
    private val isAckNotSend = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.nlc_new_chat_layout_activity)
        getAgoraCallingKey()
        Chat_Activity = this@ChatPageActivity
        isGroupChat = false
        isFirstItemSelected = false
        isMassChat = resources.getBoolean(R.bool.is_mass_chat)
        is_telpon_chat = resources.getBoolean(R.bool.is_telpon_chat)
        //getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.whatsapp_background));
        initializeactivity()
        lifecycleScope.launch { loadFromDB() }
        startInstantTranslationPopup()
    }

    private fun startInstantTranslationPopup() {
        if (!SharedPreference.getInstance()
                .getBool(this, AppConstants.INSTANT_TRANSLATION_POPUP_ANY_CHAT)
        ) {
            SharedPreference.getInstance()
                .saveBool(this, AppConstants.INSTANT_TRANSLATION_POPUP_ANY_CHAT, true)
            val handler = Handler()
            handler.postDelayed({
                val dialog = InstantTranslationPopup()
                dialog.show(supportFragmentManager, "itp")
            }, 500)
        }
    }

    private fun initializeactivity() {
        pathList = ArrayList()
        //AppUtils.startService(this, ScreenShotDetector.class);
        // startService(new Intent(this, ScreenShotDetector.class));
        FetchDownloadManager.getInstance().normalChatListener(this)
        sessionManager = SessionManager.getInstance(this)
        sharedprf_video_uploadprogress = ShortcutBadgeManager(this)
        contactDB_sqlite = CoreController.getContactSqliteDBintstance(this)
        mChatData = ArrayList()
        uploadDownloadManager = FileUploadDownloadManager(this)
        userInfoSession = UserInfoSession(this)
        getcontactname = Getcontactname(this)
        messageDatabase = CoreController.getDBInstance(this)
        session = Session(this)
        groupInfoSession = GroupInfoSession(this)
        savedMembersList = ArrayList()
        unsavedMembersList = ArrayList()
        allMembersList = ArrayList()
        include = findViewById(R.id.chatheaderinclude)
        setSupportActionBar(include)
        mBackImage = findViewById(R.id.back_image)
        chat_list = findViewById(R.id.chat_list)
        layout_new = findViewById(R.id.layout_new)
        mLnrcallStatus = findViewById(R.id.lnrcallStatus)
        user_name = include?.findViewById(R.id.usernamechatsceen)
        r1messagetoreplay = findViewById(R.id.r1messagetoreplay)
        videoimage = findViewById(R.id.videoimage)
        cameraphoto = findViewById(R.id.cameraphoto)
        audioimage = findViewById(R.id.audioimage)
        personimage = findViewById(R.id.personimage)
        Documentimage = findViewById(R.id.Documentimage)
        sentimage = findViewById(R.id.sentimage)
        messagesetmedio = findViewById(R.id.messagesetmedio)
        message_old = findViewById(R.id.message)
        message_old?.setTextColor(resources.getColor(R.color.title))
        Ifname = findViewById(R.id.Ifname)
        close = findViewById(R.id.close)
        sendMessage = findViewById(R.id.chat_edit_text1)
        overflow = findViewById(R.id.overflow)
        emailgmail = findViewById(R.id.email_gmail)
        emai1send = findViewById(R.id.emai1send)
        gmailsend = findViewById(R.id.gmailsend)
        tvBlock = findViewById(R.id.tvBlock)
        tvAddToContact = findViewById(R.id.tvAddToContact)
        block_contact = findViewById(R.id.block_contact)
        rvGroupMembers = findViewById(R.id.rvGroupMembers)
        ivWebLink = findViewById(R.id.ivWebLink)
        tvWebTitle = findViewById(R.id.tvWebTitle)
        tvWebLink = findViewById(R.id.tvWebLink)
        rlWebLink = findViewById(R.id.rlWebLink)
        sendButton = findViewById(R.id.enter_chat1)
        record = findViewById(R.id.record)
        myChronometer = findViewById(R.id.chronometer)
        image_to = findViewById(R.id.image_to)
        slidetocencel = findViewById(R.id.slidetocencel)
        ivCancelRecording = findViewById(R.id.iv_cancel_recorder)
        //selEmoji = findViewById(R.id.emojiButton);
        rootView = findViewById(R.id.mainRelativeLayout)
        //        emojIcon = new EmojIconActions(this, rootView, sendMessage, selEmoji);
//        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.input_emoji);
        sendMessage?.visibility = View.VISIBLE
        llAddBlockContact = findViewById(R.id.llAddBlockContact)
        background = findViewById(R.id.background)
        chat_videoview = findViewById<View>(R.id.chat_videoview) as SimpleExoPlayerView
        chat_videoview!!.setControllerVisibilityListener(PlaybackControlView.VisibilityListener { i ->
            if (i == 0) {
                chat_videoview!!.hideController()
            }
        })
        Search1 = findViewById(R.id.etSearch)
        add_contact = findViewById(R.id.add_contact)
        block_contact = findViewById(R.id.block_contact)
        report_spam = findViewById(R.id.report_spam)
        iBtnScroll = findViewById(R.id.iBtnScroll)
        unreadcount = findViewById(R.id.unreadcount)
        frameL = findViewById(R.id.frame)
        RelativeLayout_group_delete = findViewById(R.id.RelativeLayout_group_delete)
        relativeLayout = findViewById(R.id.bottomlayout)
        rlSend = findViewById(R.id.rlSend)
        group_left_layout = findViewById(R.id.group_left_layout)

        //------------attachment Intialize----------------------------
        nudgeIcon = findViewById(R.id.iv_chat_nudge)
        translateIcon = findViewById(R.id.iv_chat_translate)
        attachment_icon = findViewById(R.id.attachment_icon)
        attachmentLayout = findViewById(R.id.menu_attachments)
        camera_choose = findViewById(R.id.camera_choose)
        image_choose = findViewById(R.id.image_choose)
        video_choose = findViewById(R.id.video_choose)
        audio_choose = findViewById(R.id.audio_choose)
        document_choose = findViewById(R.id.document_choose)
        location_choose = findViewById(R.id.location_choose)
        send_payment = findViewById(R.id.send_payment)
        contact_choose = findViewById(R.id.contact_choose)
        //---------------------EditText Inside Send Layout Intialize--------------------
        capture_image = findViewById(R.id.capture_image)
        from = sessionManager?.currentUserID
        mCurrentUserId = sessionManager?.currentUserID

        //---------------------Header Initialize--------------------------------
//
        //   back_layout = findViewById(R.id.back_layout);
        user_profile_image = findViewById(R.id.user_profile_image)
        user_name = findViewById(R.id.user_name)
        status_textview = findViewById(R.id.status_textview)
        /*  Ripple_Video = (RippleView) findViewById(R.id.Ripple_Video);
        Ripple_call = (RippleView) findViewById(R.id.Ripple_call);
     */ivVideoCall = findViewById(R.id.ivVideoCall)
        ivVoiceCall = findViewById(R.id.ivVoiceCall)
        menu_layout = findViewById(R.id.menu_layout)
        call_layout = findViewById(R.id.call_layout)
        name_status_layout = findViewById(R.id.name_status_layout)
        user_info_container = findViewById(R.id.user_info_container)

        //-------------------------Chat Action Intialize--------------------------
        rlChatActions = findViewById(R.id.rlChatActions)
        iBtnBack2 = findViewById(R.id.iBtnBack2)
        replymess = findViewById(R.id.replymess)
        copychat = findViewById(R.id.copychat)
        showOriginal = findViewById(R.id.show_original)
        starred = findViewById(R.id.starred)
        info = findViewById(R.id.info)
        delete = findViewById(R.id.delete)
        forward = findViewById(R.id.forward)
        longpressback = findViewById(R.id.iBtnBack3)
        share = findViewById(R.id.share)
        MsgProgressBar = findViewById(R.id.msg_progressbar)
        rlChatActions?.setVisibility(View.GONE)
        include?.setVisibility(View.VISIBLE)
        sendMessage?.addTextChangedListener(watch)
        receiver = Receiver()
        val filter = IntentFilter()
        filter.addAction("com.groupname.change")
        filter.addAction("com.groupprofile.update")
        filter.addAction("com.group.delete.members")
        filter.addAction("com.group.makeadmin")
        registerReceiver(receiver, filter)
        getintent()
        OnclickMethod()
        initAtAdapter()
        setupScrollListener()
        KeyboardVisibilityEvent.setEventListener(
            this
        ) { isOpen ->
            if (isOpen) {
                mChatData?.let {
                    if (iBtnScroll?.visibility == View.GONE)
                        chat_list?.layoutManager?.scrollToPosition(it.size - 1)
                }
            }
        }
        //receiverUid
        getUserPin(to)
    }

    private val groupDetails: Unit
        private get() {
            val event = SendMessageEvent()
            event.eventName = SocketManager.EVENT_GROUP_DETAILS
            try {
                val `object` = JSONObject()
                `object`.put("from", mCurrentUserId)
                `object`.put("convId", mGroupId)
                event.messageObject = `object`
                EventBus.getDefault().post(event)
            } catch (ex: JSONException) {
                ex.printStackTrace()
            }
        }


    private fun setupScrollListener() {

        chat_list?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    pastVisibleItems =
                        (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                    visibleItemCount = chat_list!!.childCount
                    totalItemCount = mAdapter!!.itemCount
                    val lastItem = (pastVisibleItems + visibleItemCount)
                    if ((lastItem == totalItemCount) and mListviewScrollBottom == true) {
                        //this to prevent the list to make more than one request in time
                        mListviewScrollBottom = false
                        //you can put the index for your next page here
                        //  loadMore(int page);
                    } else
                        mListviewScrollBottom = true


                    if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                        iBtnScroll!!.visibility = View.GONE
                        unreadcount!!.visibility = View.GONE
                        unreadmsgcount = 0
                        changeBadgeCount(mConvId!!)
                    } else if (totalItemCount > 0) {
                        iBtnScroll!!.visibility = View.VISIBLE
                    }
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                //This check was added to handle the delete chat case where all messages are deleted
                //and then this method is triggered
                if (mChatData!!.size == 0)
                    return;

                if (dy < 0) {
                    pastVisibleItems = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                    totalItemCount = mAdapter!!.itemCount
                    val threshHold = 15
                    if (pastVisibleItems <= threshHold) {
                        val firstItem = mChatData?.get(0)
                        val ts = firstItem?.ts
                        // up scroll
                        val items = messageDatabase!!.selectAllMessagesWithLimit(
                            docId, chatType,
                            ts, MessageDbController.MESSAGE_PAGE_LOADED_LIMIT
                        )
                        Collections.sort(items, msgComparator)
                        if (items.size > 0) {
                            for (i in items.indices) {
                                if (!isAlreadyExist(items[i]))
                                    mChatData!!.add(i, items[i])
                            }
                            if (items.size > 0)
                                mAdapter?.notifyItemRangeInserted(0, mChatData!!.indexOf(firstItem))
                        }
                    }
                } else {
                    lastvisibleitempostion = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                    totalItemCount = mAdapter!!.itemCount
                    val threshHold = 15
                    if (lastvisibleitempostion >= totalItemCount - threshHold) {
                        val lastposition: Int = mChatData!!.size-1

                        // down scroll
                        val tss = mChatData!![lastposition]!!.ts
                        val items = messageDatabase!!.selectAllMessagesWithLimit_again(
                            docId, chatType,
                            tss, MessageDbController.MESSAGE_PAGE_LOADED_LIMIT
                        )
                        Collections.sort(items, msgComparator)
                        val previousSize = mChatData!!.size
                        if (items.size > 0) {
                            Collections.sort(items, msgComparator)
                            for (i in items.indices) {
                                if (!isAlreadyExist(items[i]))
                                    mChatData!!.add(previousSize + i, items[i])
                            }

                            val sizeDifference = mChatData!!.size - previousSize
                            mAdapter?.notifyItemRangeInserted(previousSize + 1, sizeDifference)
                        }
                    }
                }
            }
        })
    }


    private fun initAtAdapter() {
        val llm = LinearLayoutManager(this)
        llm.orientation = RecyclerView.VERTICAL
        rvGroupMembers!!.layoutManager = llm
        atAdapter = At_InfoAdapter(this@ChatPageActivity, allMembersList)
        rvGroupMembers!!.adapter = atAdapter
        if (isGroupChat) {
            groupDetails
        }
    }

    //--------------------------------------------New Contact Added-----------------------------------------------
    private fun OnclickMethod() {
        ivCancelRecording!!.setOnClickListener(this)
        nudgeIcon!!.setOnClickListener(this)
        translateIcon!!.setOnClickListener(this)
        attachment_icon!!.setOnClickListener(this)
        camera_choose!!.setOnClickListener(this)
        image_choose!!.setOnClickListener(this)
        video_choose!!.setOnClickListener(this)
        audio_choose!!.setOnClickListener(this)
        document_choose!!.setOnClickListener(this)
        location_choose!!.setOnClickListener(this)
        send_payment!!.setOnClickListener(this)
        contact_choose!!.setOnClickListener(this)
        capture_image!!.setOnClickListener(this)
        mBackImage!!.setOnClickListener(this)
        ivVideoCall!!.setOnClickListener(this)
        ivVoiceCall!!.setOnClickListener(this)
        mLnrcallStatus!!.setOnClickListener(this)
        close!!.setOnClickListener(this)
        iBtnBack2!!.setOnClickListener(this)
        replymess!!.setOnClickListener(this)
        copychat!!.setOnClickListener(this)
        showOriginal!!.setOnClickListener(this)
        starred!!.setOnClickListener(this)
        delete!!.setOnClickListener(this)
        forward!!.setOnClickListener(this)
        share!!.setOnClickListener(this)
        overflow!!.setOnClickListener(this)
        longpressback!!.setOnClickListener(this)
        sendButton!!.setOnClickListener(this)
        name_status_layout!!.setOnClickListener(this)
        user_info_container!!.setOnClickListener(this)
        menu_layout!!.setOnClickListener(this)
        //        selEmoji.setOnClickListener(this);
        tvBlock!!.setOnClickListener(this)
        tvAddToContact!!.setOnClickListener(this)
        iBtnScroll!!.setOnClickListener(this)
        add_contact!!.setOnClickListener(this)
        block_contact!!.setOnClickListener(this)
        report_spam!!.setOnClickListener(this)
        record!!.setOnClickListener(this)
        //record.setOnTouchListener(audioRecordTouchListener);
    }

    override fun onBackPressed() {
        MessageAdapter.lastPlayedAt = -1
        if (isAudioRecording) {
            isAudioRecording = false
            stopRecording(true)
        }
        Chat_Activity = null
        val mngr: ActivityManager? = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        var taskList: List<ActivityManager.RunningTaskInfo>? = null
        if (mngr != null) {
            taskList = mngr.getRunningTasks(10)
        } else {
            return
        }
        val backstackActivitiesCount = taskList.get(0).numActivities
        MyLog.d(
            TAG,
            "onBackPressed: $backstackActivitiesCount"
        )
        if (isFrom != null && isFrom!!.equals("meetup")) {
            val intent = Intent(
                this@ChatPageActivity,
                NewHomeScreenActivty::class.java
            )
            intent.putExtra(NewHomeScreenActivty.OPEN_TAB_INDEX, 1);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent)
        } else if (backstackActivitiesCount == 1 && taskList.get(0).topActivity!!.className == this.javaClass.name) {
            MyLog.e(TAG, "This is last activity in the stackkkk")
            val intent = Intent(
                this@ChatPageActivity,
                NewHomeScreenActivty::class.java
            )
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
        } else if (backstackActivitiesCount <= 1) {
            val intent = Intent(
                this@ChatPageActivity,
                NewHomeScreenActivty::class.java
            )
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            startActivity(intent)
        } else if (backstackActivitiesCount == 3 || backstackActivitiesCount > 3) {
            val intent = Intent(
                this@ChatPageActivity,
                NewHomeScreenActivty::class.java
            )
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent)
        }
        noNeedRefresh = true
        finish()
    }

    //----------------------------------------Report Spam---------------------------------------------
    override fun onClick(view: View) {
        when (view.id) {
            R.id.iv_cancel_recorder -> {
                isAudioRecording = false
                stopRecording(false)
            }
            R.id.record -> {
                MyChatUtils.hideKeyboard(this)
                mAdapter!!.pauseAudioPlay(-1)
                startRecording()
            }
            R.id.lnrcallStatus -> Redirecttocall()
            R.id.back_image -> onBackPressed()
            R.id.iv_chat_translate -> showTranslationBottomSheet()
            R.id.iv_chat_nudge -> if (verifyTimestamp()) sendMessage("Nudge", MessageFactory.nudge)
            R.id.attachment_icon -> if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) showMenuBelowLollipop() else showMenu()
            R.id.image_choose -> {
                //                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//                    if (!Environment.isExternalStorageManager()) {
//                        askForPermissions();
//                    } else
//                        goToImageCaptionScreen();
//                } else
                goToImageCaptionScreen()
                hideMenu()
            }
            R.id.video_choose -> {
                hideMenu()
                val video = Intent(
                    this@ChatPageActivity,
                    ImagecaptionActivity::class.java
                )
                video.putExtra("phoneno", mReceiverName)
                video.putExtra("from", "Video")
                video.putExtra("group", "value")
                startActivityForResult(video, RESULT_LOAD_VIDEO)
            }
            R.id.audio_choose -> {
                hideMenu()
                val audioIntent = Intent(
                    this@ChatPageActivity,
                    AudioFilesListActivity::class.java
                )
                startActivityForResult(audioIntent, REQUEST_SELECT_AUDIO)
            }
            R.id.document_choose -> {
                hideMenu()
                FilePickerBuilder.instance.setMaxCount(1)
                    .setSelectedFiles(ArrayList()) //.setActivityTheme(R.style.AppTheme)
                    .sortDocumentsBy(SortingTypes.name)
                    .pickFile(this@ChatPageActivity)
            }
            R.id.location_choose -> {
                hideMenu()
                requestForPermissions(
                    arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) { granted ->
                    if (granted) {
                        val intent = Intent(
                            this@ChatPageActivity,
                            LocationActivity::class.java
                        )
                        intent.putExtra(AppConstants.IntentKeys.FRIEND_NAME.value, "")
                        intent.putExtra(AppConstants.IntentKeys.FRIEND_IMAGE.value, "")
                        intent.putExtra(AppConstants.IntentKeys.ONLINE_STATUS.value, "")
                        startActivityForResult(
                            intent,
                            RESULT_SHARE_LOCATION
                        )
                    }
                }
            }
            R.id.send_payment -> {
                hideMenu()
                val sendpayment = Intent(
                    this@ChatPageActivity,
                    SendPaymentLoginActivity::class.java
                )
                sendpayment.putExtra("Username", user_name!!.text.toString())
                sendpayment.putExtra("msisdn", receiverMsisdn)
                startActivity(sendpayment)
                noNeedRefresh = true
            }
            R.id.contact_choose -> {
                hideMenu()
                val intentContact = Intent(
                    this@ChatPageActivity,
                    SendContact::class.java
                )
                startActivityForResult(intentContact, REQUEST_CODE_CONTACTS)
            }
            R.id.camera_choose -> {
                hideMenu()
                if (contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1") {
                    DisplayAlert("Unblock $mReceiverName to send message?")
                } else {
                    val camera = Intent(
                        this@ChatPageActivity,
                        ImagecaptionActivity::class.java
                    )
                    camera.putExtra("phoneno", mReceiverName)
                    camera.putExtra("from", "Camera")
                    startActivityForResult(camera, CAMERA_REQUEST)
                }
            }
            R.id.menu_layout -> openMenu()
            R.id.replymess -> ReplyMessageClick()
            R.id.starred -> StartMessageClick()
            R.id.copychat -> CopyClick()
            R.id.show_original -> showOriginalClick()
            R.id.delete -> DeleteMessageClick()
            R.id.forward -> ForwardClick()
            R.id.share -> {
                showUnSelectedActions()
                val mFile = Uri.fromFile(File(mSelectedPath))
                ShareClick(mFile)
            }
            R.id.close -> {
                sendMessage!!.text.clear()
                r1messagetoreplay!!.visibility = View.GONE
                reply = false
                showUnSelectedActions()
            }
            R.id.iBtnBack3 -> showUnSelectedActions()
            R.id.ivVoiceCall -> if (isAudioRecording) {
                Toast.makeText(context, "Cant make a call with audio recording", Toast.LENGTH_SHORT).show()
            } else {
                if (contactDB_sqlite!!.getBlockedStatus(
                    mReceiverId,
                    false
                ) == "1") {
                    DisplayAlert(
                        resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                            R.string.to_place_a
                        ) + " "
                                + getString(R.string.app_name) + " " + resources.getString(R.string.call)
                    )
                } else {
                    Activecall(false)
                }
            }
            R.id.ivVideoCall -> if (isAudioRecording) {
                Toast.makeText(context, "Cant make a call with audio recording", Toast.LENGTH_SHORT).show()
            } else {
                if ((contactDB_sqlite!!.getBlockedStatus(
                        mReceiverId,
                        false
                    ) == "1")
                ) {
                    DisplayAlert(
                        (resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                            R.string.to_place_a
                        ) + " "
                                + getString(R.string.app_name) + " " + resources.getString(R.string.call))
                    )
                } else {
                    Activecall(true)
                }
            }
            R.id.enter_chat1 -> {
                if (isAudioRecording) {
                    isAudioRecording = false
                    stopRecording(true)
                    return
                }
                //                if (ConnectivityInfo.isInternetConnected(getApplicationContext())) {
                val message: String = sendMessage!!.text.toString().trim()
                if (message == "") {
                    Toast.makeText(this, "Only spaces not allowed", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    MsgProgressBar!!.visibility = View.VISIBLE
                    sendMessage(
                        sendMessage!!.text.toString().trim { it <= ' ' },
                        MessageFactory.text
                    )
                }
            }
            R.id.user_info_container -> goInfoPage()
            R.id.emojiButton -> {}
            R.id.add_contact -> addnewcontact(receiverMsisdn)
            R.id.block_contact -> performMenuBlock()
            R.id.report_spam -> performreportspam()
            R.id.tvBlock -> performMenuBlock()
            R.id.tvAddToContact -> {
                AppUtils.startService(this, ContactsSync::class.java)
                val intent1 = Intent(ContactsContract.Intents.Insert.ACTION)
                intent1.putExtra(INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED, true)
                intent1.type = ContactsContract.RawContacts.CONTENT_TYPE
                intent1.putExtra(ContactsContract.Intents.Insert.PHONE, user_name!!.text.toString())
                startActivityForResult(intent1, ADD_CONTACT)
            }
            R.id.iBtnScroll -> {
                if (mChatData!!.size > 0)
                    chat_list!!.layoutManager!!.scrollToPosition(mChatData!!.size - 1)
                iBtnScroll!!.visibility = View.GONE
                unreadcount!!.visibility = View.GONE
                mListviewScrollBottom = false
            }
        }
    }

//    override fun onConfigurationChanged(newConfig: Configuration) {
//        super.onConfigurationChanged(newConfig)
//        if (newConfig.keyboardHidden == Configuration.KEYBOARDHIDDEN_NO) {
//            mChatData?.let{
//                chat_list?.layoutManager?.scrollToPosition(it.size - 1)
//            }
//        }
//    }

    private fun goToImageCaptionScreen() {
        val i = Intent(
            this@ChatPageActivity,
            ImagecaptionActivity::class.java
        )
        i.putExtra("phoneno", mReceiverName)
        i.putExtra("from", "Gallary")
        startActivityForResult(i, RESULT_LOAD_IMAGE)
    }

    private fun showTranslationBottomSheet() {
        val currentSysLang = Locale.getDefault().displayLanguage
        val key = MyChatUtils.getLanguageKey(this, languageId)
        val selectedLanguage = StorageUtility.getDataFromPreferences(this, key, currentSysLang)
        val outgoingLang = String.format(
            Locale.getDefault(),
            resources.getString(R.string.outgoing_language),
            selectedLanguage
        )
        val data = ArrayList<BottomSheetModel>()
        data.add(BottomSheetModel(outgoingLang, 0))
        if (currentSysLang != selectedLanguage) {
            data.add(BottomSheetModel(resources.getString(R.string.turn_off_translation), 0))
            data.add(BottomSheetModel(resources.getString(R.string.cancel), 0))
        }
        DialogUtils.showBottomSheetDialog(this, "", data,
            com.chat.android.app.callbacks.CallbackListener { result ->
                if ((result == AppConstants.CANCEL)) return@CallbackListener
                if ((result == resources.getString(R.string.turn_off_translation))) {
                    val intentKeyLanguage = "English"
                    val langCode = "en"
                    val keyCode = MyChatUtils.getLanguageKey(
                        this@ChatPageActivity, AppConstants.SPKeys.OUT_GOING_LANGUAGE_CODE.value,
                        languageId
                    )
                    StorageUtility.saveDataInPreferences(
                        this@ChatPageActivity,
                        key,
                        intentKeyLanguage
                    )
                    StorageUtility.saveDataInPreferences(
                        this@ChatPageActivity,
                        keyCode,
                        langCode
                    )
                    setTranslateIcon()
                } else if (result.contains(outgoingLang)) {
                    val intent = Intent(
                        this@ChatPageActivity,
                        LanguageSelectionActivity::class.java
                    )
                    intent.putExtra(
                        AppConstants.IntentKeys.EXTRA_KEY_LANGUAGE_KEY.value,
                        MyChatUtils.getLanguageKey(
                            this@ChatPageActivity,
                            languageId
                        )
                    )
                    startActivityForResult(
                        intent,
                        AppConstants.RequestCodes.REQUEST_CHANGE_LANGUAGE.code
                    )
                }
            })
    }

    private val languageId: String?
        private get() {
            return if (isGroupChat) mGroupId else to
        }

    fun verifyTimestamp(): Boolean {
        if (SharedPreference.getInstance()
                .getValue(applicationContext, Constants.NUDGE_TIMESTAMP) != null
        ) {
            if (TimeStampUtils.getTimestampDifferenceMinutes(
                    SharedPreference.getInstance().getValue(
                        applicationContext, Constants.NUDGE_TIMESTAMP
                    ), TimeStampUtils.getTimeStamp()
                ) >= 1
            ) {
                SharedPreference.getInstance().save(
                    applicationContext,
                    Constants.NUDGE_TIMESTAMP,
                    TimeStampUtils.getTimeStamp()
                )
                return true
            } else return false
        } else SharedPreference.getInstance()
            .save(applicationContext, Constants.NUDGE_TIMESTAMP, TimeStampUtils.getTimeStamp())
        return true
    }

    //---------------------------------------------Going User Information Page-----------------------------
    //------------------------------------Record Button Long Click-----------------------------
    override fun onLongClick(view: View): Boolean {
        if (view.id == R.id.record) {
            startRecording()
        }
        return true
    }

    private fun startRecording() {
        attachmentLayout!!.visibility = View.GONE
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            avoid_twotimescall++
            if (avoid_twotimescall == 1) {
                DisplayAlert(
                    resources.getString(R.string.unblock) + mReceiverName + " " + resources.getString(
                        R.string.to_send_message
                    )
                )
            } else if (avoid_twotimescall == 2) {
                avoid_twotimescall = 0
            }
        } else {
            if (checkAudioRecordPermission()) {

                //  record.setImageResource(R.drawable.record_hold);
                //record.setBackground(ContextCompat.getDrawable(context, R.drawable.record_hold));
                //record.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_record_icon));
                record!!.setImageResource(R.drawable.ic_audio_chat)
                record!!.visibility = View.GONE
                sendButton!!.visibility = View.VISIBLE
                nudgeIcon!!.visibility = View.GONE
                translateIcon!!.visibility = View.GONE
                sendMessage!!.visibility = View.GONE
                //selEmoji.setImageResource(R.drawable.record_usericon);
                myChronometer!!.visibility = View.VISIBLE
                image_to!!.visibility = View.GONE
                slidetocencel!!.visibility = View.GONE
                ivCancelRecording!!.visibility = View.VISIBLE
                capture_image!!.visibility = View.GONE
                attachment_icon!!.visibility = View.GONE
                // myChronometer.setTypeface(face);
                //selEmoji.setEnabled(false);
                startAudioRecord()
            } else {
                requestAudioRecordPermission()
            }
        }
    }

    //----------------------------------------------Record Start Audio-----------------------------------------
    fun addnewcontact(number: String?) {
        val dialog = CustomAlertDialog()
        dialog.setMessage(resources.getString(R.string.new_exitcontact))
        dialog.setPositiveButtonText(getString(R.string.new_))
        dialog.setNegativeButtonText(getString(R.string.existing))
        dialog.setCustomDialogCloseListener(object : CustomAlertDialog.OnCustomDialogCloseListener {
            override fun onPositiveButtonClick() {
                val intent = Intent(ContactsContract.Intents.Insert.ACTION)
                // Sets the MIME type to match the Contacts Provider
                intent.type = ContactsContract.RawContacts.CONTENT_TYPE
                intent.putExtra(ContactsContract.Intents.Insert.NAME, name)
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, number)
                startActivityForResult(intent, ADD_CONTACT)
                //finish();
            }

            override fun onNegativeButtonClick() {
                val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
                intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(intent, 1)
                finish()
            }
        })
        dialog.show(supportFragmentManager, "Save contact")
    }

    //-----------------------------------------------Record Touch Stop Listener---------------------------------------
    private fun performreportspam() {
        AlertDialog.Builder((context)!!)
            .setMessage(resources.getString(R.string.sure_you_want_report_this_msg))
            .setPositiveButton(
                android.R.string.yes
            ) { dialog, which ->
                val messageEvent = SendMessageEvent()
                messageEvent.eventName = SocketManager.EVENT_REPORT_SPAM_USER
                try {
                    val myobj = JSONObject()
                    myobj.put("from", from)
                    myobj.put("to", to)
                    messageEvent.messageObject = myobj
                    EventBus.getDefault().post(messageEvent)
                } catch (e: JSONException) {
                    MyLog.e(
                        TAG,
                        "",
                        e
                    )
                }
            }
            .setNegativeButton(
                android.R.string.no
            ) { dialog, which -> dialog.dismiss() }
            .show()
    }

    private fun goInfoPage() {
        showUnSelectedActions()
        if (selectedChatItems != null) selectedChatItems!!.clear()
        if (isGroupChat) {
            val infoIntent = Intent(
                this@ChatPageActivity,
                GroupInfo::class.java
            )
            infoIntent.putExtra("GroupId", mGroupId)
            infoIntent.putExtra("GroupName", user_name!!.text.toString())
            startActivityForResult(infoIntent, EXIT_GROUP_REQUEST_CODE)
        } else {
            val infoIntent = Intent(
                this@ChatPageActivity,
                UserInfo::class.java
            )
            infoIntent.putExtra("UserId", to)
            infoIntent.putExtra("UserName", user_name!!.text.toString())
            infoIntent.putExtra("UserAvatar", receiverAvatar)
            infoIntent.putExtra("UserNumber", receiverMsisdn)
            infoIntent.putExtra("FromSecretChat", false)
            startActivityForResult(infoIntent, MUTE_ACTIVITY)
        }
    }

    private fun startAudioRecord() {
        try {
            if (!isAudioRecording) {
                isAudioRecording = true
                val audioDir = File(MessageFactory.AUDIO_STORAGE_PATH)
                if (!audioDir.exists()) {
                    audioDir.mkdirs()
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    saveFileToExternalStorage(System.currentTimeMillis().toString(), "rc")
                } else {
                    try {
                        audioRecordPath =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).absolutePath +
                                    MessageFactory.getMessageFileName(
                                        MessageFactory.audio,
                                        Calendar.getInstance().timeInMillis.toString() + "rc",
                                        ".mp3"
                                    )
                    } catch (e: Exception) {
                        Log.d(
                            TAG,
                            "Exception: $e"
                        )
                    }
                    audioRecorder = MediaRecorder()
                    audioRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
                    audioRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                    audioRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
                    audioRecorder!!.setOutputFile(audioRecordPath)
                    audioRecorder!!.prepare()
                    audioRecorder!!.start()
                    myChronometer!!.start()
                    myChronometer!!.base = SystemClock.elapsedRealtime()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun saveFileToExternalStorage(displayName: String, content: String) {
        audioRecorder = MediaRecorder()
        val contentValues = ContentValues()
        contentValues.put(
            MediaStore.Files.FileColumns.DISPLAY_NAME,
            "$displayName.mp3"
        )
        contentValues.put(MediaStore.Files.FileColumns.MIME_TYPE, "audio/mp3")
        contentValues.put(MediaStore.Files.FileColumns.TITLE, displayName)
        contentValues.put(
            MediaStore.Files.FileColumns.DATE_ADDED,
            System.currentTimeMillis() / 1000
        )
        contentValues.put(MediaStore.Files.FileColumns.RELATIVE_PATH, Environment.DIRECTORY_MUSIC)
        contentValues.put(MediaStore.Files.FileColumns.DATE_TAKEN, System.currentTimeMillis())
        try {
            val audiouri =
                contentResolver.insert(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, contentValues)
            val file = contentResolver.openFileDescriptor((audiouri)!!, "w")
            if (file != null) {
                audioRecordPath = audiouri.path
                audioURI = audiouri
                audioRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
                audioRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                audioRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
                audioRecorder!!.setOutputFile(file.fileDescriptor)
                audioRecorder!!.prepare()
                audioRecorder!!.start()
                myChronometer!!.start()
                myChronometer!!.base = SystemClock.elapsedRealtime()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    //-------------------------------------------------Send Message--------------------------------------
    private fun showAudioRecordSentAlert(
        audioRecordPath: String,
        durationStr: String
    ) {
        val dialog = CustomAlertDialog()
        dialog.setMessage(resources.getString(R.string.send_recorded_audio))
        dialog.setPositiveButtonText(resources.getString(R.string.send))
        dialog.setNegativeButtonText(resources.getString(R.string.cancel))
        dialog.isCancelable = false
        dialog.setCustomDialogCloseListener(object : CustomAlertDialog.OnCustomDialogCloseListener {
            override fun onPositiveButtonClick() {
                sendAudioMessage(
                    audioRecordPath,
                    durationStr,
                    MessageFactory.AUDIO_FROM_RECORD,
                    false
                )
            }

            override fun onNegativeButtonClick() {}
        })
        dialog.show(supportFragmentManager, "Record Alert")
    }

    private fun getTimeString(millis: Long): String {
        val buf = StringBuffer()
        val hours = (millis / (1000 * 60 * 60)).toInt()
        val minutes = ((millis % (1000 * 60 * 60)) / (1000 * 60)).toInt()
        val seconds = (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000).toInt()
        buf.append(String.format("%02d", minutes))
            .append(":")
            .append(String.format("%02d", seconds))
        return buf.toString()
    }

    //----------------------------------------Send Particular Message Reply------------------------------------------------------
    private fun sendMessage(message: String, type: Int) {
        sendButton!!.visibility = View.GONE
        if ((contactDB_sqlite!!.getBlockedStatus(to, false) == "1")) {
            DisplayAlert(
                resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                    R.string.to_send_message
                )
            )
            MsgProgressBar!!.visibility = View.GONE
            sendButton!!.visibility = View.VISIBLE
        } else {
            if ((!hasLinkInfo && !reply)) {
                reply = false
                val deviceLanguage = Locale.getDefault().displayLanguage
                val key = MyChatUtils.getLanguageKey(this, languageId)
                val selectedLanguage =
                    StorageUtility.getDataFromPreferences(this, key, deviceLanguage)
                if (type == MessageFactory.nudge) {
                    val itemChat = getChatMessageItem(message, type)
                    var index = -1
                    if (!isAlreadyExist(itemChat)) {
                        mChatData!!.add(itemChat)
                        index = mChatData!!.size - 1
                    }
                    sendTextMessage(itemChat, index, message, message, type)
                    MsgProgressBar!!.visibility = View.GONE
                } else translateText(message, type)
            } else if (reply) {
                r1messagetoreplay!!.visibility = View.GONE
                val data = sendMessage!!.text.toString().trim { it <= ' ' }
                TranslateText(data)
                if (at_memberlist != null) {
                    at_memberlist!!.clear()
                }
                MsgProgressBar!!.visibility = View.GONE
                sendButton!!.visibility = View.VISIBLE
            } else {
                sendWebLinkMessage()
                if (at_memberlist != null) {
                    at_memberlist!!.clear()
                }
                MsgProgressBar!!.visibility = View.GONE
                sendButton!!.visibility = View.VISIBLE
            }
        }
    }

    //---------------------------------------------------------Send WebLink Messages------------------------------------------------------
    //-------------------------------------------------SEND TEXT MESSAGE --------------------------------------------------------------
    private fun translateText(textToTranslate: String, type: Int) {
        val keyCode = MyChatUtils.getLanguageKey(
            this, AppConstants.SPKeys.OUT_GOING_LANGUAGE_CODE.value,
            languageId
        )
        val source = MyChatUtils.getKeyboardLanguageNew(this)
        val target = StorageUtility.getDataFromPreferences(
            this@ChatPageActivity,
            keyCode,
            Locale.getDefault().language
        )
        val url =
            WebConfig.GOOGLE_API_URL + "?key=" + WebConfig.LanguageKey.GOOGLE_LANGUAGE_API_KEY + "&target=" + target + "&source=" + source + "&q=" +  /*URLEncoder.encode(*/textToTranslate /*, "UTF-8")*/
        if (source != target) {
            IChatWithoutHeaderAPIController.translateText(
                this@ChatPageActivity,
                url, object : Callback<ResponseBody?> {

                    override fun onResponse(
                        call: Call<ResponseBody?>,
                        response: Response<ResponseBody?>
                    ) {
                        if (response.isSuccessful) {
                            try {
                                val responseText = response.body()!!.string()
                                val responseJson = JSONObject(responseText)
                                val dataJSON = responseJson.getJSONObject("data")
                                val translationsJsonArray = dataJSON.getJSONArray("translations")
                                val translationJSONObject = translationsJsonArray.getJSONObject(0)
                                val translatedText =
                                    translationJSONObject.getString("translatedText")
                                val itemChat = getChatMessageItem(translatedText, type)
                                var index = -1
                                if (!isAlreadyExist(itemChat)) {
                                    mChatData!!.add(itemChat)
                                    index = mChatData!!.size - 1
                                }
                                sendTextMessage(
                                    itemChat,
                                    index,
                                    textToTranslate,
                                    translatedText,
                                    type
                                )
                                MsgProgressBar!!.visibility = View.GONE
                                return
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        } else {
                            val itemChat = getChatMessageItem(textToTranslate, type)
                            var index = -1
                            if (!isAlreadyExist(itemChat)) {
                                mChatData!!.add(itemChat)
                                index = mChatData!!.size - 1
                            }
                            sendTextMessage(itemChat, index, textToTranslate, textToTranslate, type)
                            MsgProgressBar!!.visibility = View.GONE
                        }
                        MsgProgressBar!!.visibility = View.GONE
                    }

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        DialogUtils.dismiss()
                    }
                })
        } else {
            val itemChat = getChatMessageItem(textToTranslate, type)
            var index = -1
            if (!isAlreadyExist(itemChat)) {
                mChatData!!.add(itemChat)
                index = mChatData!!.size - 1
            }
            sendTextMessage(itemChat, index, textToTranslate, textToTranslate, type)
            MsgProgressBar!!.visibility = View.GONE
        }
    }

    private fun sendTextMessage(itemChat: MessageItemChat, data: String, type: Int) {
        sendTextMessage(itemChat, 0, data, "", type)
    }

    private fun getChatMessageItem(data: String, type: Int): MessageItemChat {
        var data_at = data
        val htmlText = data
        var isTagapplied = 0
        val names = ArrayList<String>()
        val userID = ArrayList<String>()
        if (at_memberlist != null && at_memberlist!!.size >= 0) for (groupMembersPojo: GroupMembersPojo in at_memberlist!!) {
            val userName = "@" + groupMembersPojo.contactName
            names.add(userName)
            userID.add(groupMembersPojo.userId)
            if (data_at.contains(userName)) {
                data_at = data_at.replace(
                    userName,
                    TextMessage.TAG_KEY + groupMembersPojo.userId + TextMessage.TAG_KEY
                )
                isTagapplied = 1
            }
        }

        //if (!data.equalsIgnoreCase("") || type == MessageFactory.nudge) {
        if (session!!.getarchivecount() != 0) {
            if (session!!.getarchive(from + "-" + to)) session!!.removearchive(from + "-" + to)
        }
        if (session!!.getarchivecountgroup() != 0) {
            if (session!!.getarchivegroup(from + "-" + to + "-g")) session!!.removearchivegroup(from + "-" + to + "-g")
        }
        if (type == MessageFactory.nudge) {
            AudioPlayerUtils.playSound(context, R.raw.msn_nudge_sound)
        }
        val message = MessageFactory.getMessage(type, this) as TextMessage
        ///data_at
        Log.e(
            TAG,
            "mReceiverId$mReceiverId"
        )
        Log.e(TAG, "user_name.getText().toString()" + user_name!!.text.toString())
        val msgObj: JSONObject
        if (isGroupChat) {
            msgObj = message.getGroupMessageObject(
                to,
                data_at,
                user_name!!.text.toString(),
                type
            ) as JSONObject
            try {
                msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE)
                msgObj.put("userName", user_name!!.text.toString())
            } catch (ex: JSONException) {
                ex.printStackTrace()
            }
        } else {
            msgObj = message.getMessageObject(to, data, false, type) as JSONObject
        }
        val item = message.createMessageItem(
            true,
            htmlText,
            MessageFactory.DELIVERY_STATUS_NOT_SENT,
            mReceiverId,
            user_name!!.text.toString(),
            type
        )
        item.textMessageObj = message
        return item
        //        }
//        return null;
    }

    private fun sendTextMessage(
        item: MessageItemChat,
        index: Int,
        data: String,
        translatedText: String,
        type: Int
    ) {
        var item: MessageItemChat? = item
        var data_at = data
        val htmlText = data
        var isTagapplied = 0
        val names = ArrayList<String>()
        val userID = ArrayList<String>()
        if (at_memberlist != null && at_memberlist!!.size >= 0) for (groupMembersPojo: GroupMembersPojo in at_memberlist!!) {
            val userName = "@" + groupMembersPojo.contactName
            names.add(userName)
            userID.add(groupMembersPojo.userId)
            if (data_at.contains(userName)) {
                data_at = data_at.replace(
                    userName,
                    TextMessage.TAG_KEY + groupMembersPojo.userId + TextMessage.TAG_KEY
                )
                isTagapplied = 1
            }
        }

//        if (data.equalsIgnoreCase("") || type != MessageFactory.nudge) {
//            return;
//        }
        if (session!!.getarchivecount() != 0) {
            if (session!!.getarchive(from + "-" + to)) session!!.removearchive(from + "-" + to)
        }
        if (session!!.getarchivecountgroup() != 0) {
            if (session!!.getarchivegroup(from + "-" + to + "-g")) session!!.removearchivegroup(from + "-" + to + "-g")
        }
        if (type == MessageFactory.nudge) {
            AudioPlayerUtils.playSound(context, R.raw.msn_nudge_sound)
        }
        val messageEvent = SendMessageEvent()
        var message: TextMessage? = null
        if (item != null) {
            message = item.textMessageObj
            item.textMessageObj = null
        } else message = MessageFactory.getMessage(type, this) as TextMessage
        val msgObj: JSONObject
        if (isGroupChat) {
            messageEvent.eventName = SocketManager.EVENT_GROUP
            msgObj = message!!.getGroupMessageObject(
                to,
                data_at,
                user_name!!.text.toString(),
                type
            ) as JSONObject
            try {
                msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE)
                msgObj.put("userName", user_name!!.text.toString())
            } catch (ex: JSONException) {
                ex.printStackTrace()
            }
        } else {
            msgObj = message!!.getMessageObject(to, data, false, type) as JSONObject
            messageEvent.eventName = SocketManager.EVENT_MESSAGE
        }

        ///data_at
        Log.e(
            TAG,
            "mReceiverId$mReceiverId"
        )
        Log.e(TAG, "user_name.getText().toString()" + user_name!!.text.toString())
        if (item == null) item = getChatMessageItem(data, type)
        var messageObjectChat: MessageObjectChat? = null
        if (!TextUtils.isEmpty(translatedText)) {
            try {
                messageObjectChat = MessageObjectChat()
                messageObjectChat.setTranslatedMessage(translatedText)
                val jsonObject = JSONObject()
                jsonObject.put("translatedMessage", translatedText)
                msgObj.put("messageObject", jsonObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        //Check if it blocked and check it is to value and receiver no
        if ((contactDB_sqlite!!.getBlockedMineStatus(
                to,
                false
            ) == ContactDB_Sqlite.BLOCKED_STATUS)
        ) {
            item.setisBlocked(true)
        }
        MyLog.d(
            TAG,
            "message obj : $msgObj"
        )
        messageEvent.messageObject = msgObj
        if (isTagapplied == 1) {
            item.setTagapplied(true)
            item.setSetArrayTagnames(names)
            item.setEditTextmsg(data)
            item.setUserId_tag(userID)
        }
        if (messageObjectChat != null) item.setMessageObject(messageObjectChat)
        if (isGroupChat) {
            if (enableGroupChat) {
                item.setGroupName(user_name!!.text.toString())
                messageDatabase!!.updateChatMessage(item, chatType)
                if (!isAlreadyExist(item) && index > -1) {
                    mChatData!!.add(item)
                } else {
                    mChatData!![index] = item
                }
                EventBus.getDefault().post(messageEvent)
                notifyDatasetChange()
                sendMessage!!.text.clear()
                sendMessage!!.setText("")
            } else {
                Toast.makeText(
                    this@ChatPageActivity,
                    resources.getString(R.string.you_are_not_member_of_this_group),
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else {
            item.setSenderMsisdn(receiverMsisdn)
            item.setSenderName(user_name!!.text.toString())
            if (!isAlreadyExist(item) && index > -1) {
                mChatData!!.add(item)
            } else {
                mChatData!![index] = item
            }
            EventBus.getDefault().post(messageEvent)
            messageDatabase!!.updateChatMessage(item, chatType)
            setEncryptionMsg()
            mAdapter?.notifyItemInserted(mChatData!!.size -  1)
            if (mChatData!!.size > 0) chat_list!!.layoutManager!!.scrollToPosition(mChatData!!.size - 1)
            iBtnScroll!!.visibility = View.GONE
            unreadcount!!.visibility = View.GONE
            mListviewScrollBottom = false
            sendMessage!!.text.clear()
            sendMessage!!.setText("")
            //Log.e(TAG, "item" + item.toString());
        }
        if (at_memberlist != null) {
            at_memberlist!!.clear()
        }
    }

    private fun TranslateText(Data: String) {
        val keyCode = MyChatUtils.getLanguageKey(
            this, AppConstants.SPKeys.OUT_GOING_LANGUAGE_CODE.value,
            languageId
        )
        val source = MyChatUtils.getKeyboardLanguageNew(this)
        val target = StorageUtility.getDataFromPreferences(
            this@ChatPageActivity,
            keyCode,
            Locale.getDefault().language
        )
        val url =
            WebConfig.GOOGLE_API_URL + "?key=" + WebConfig.LanguageKey.GOOGLE_LANGUAGE_API_KEY + "&target=" + target + "&source=" + source + "&q=" +  /*URLEncoder.encode(*/Data /*, "UTF-8")*/
        IChatWithoutHeaderAPIController.translateText(
            this@ChatPageActivity,
            url,
            object : Callback<ResponseBody?> {

                override fun onResponse(
                    call: Call<ResponseBody?>,
                    response: Response<ResponseBody?>
                ) {
                    if (response.isSuccessful) {
                        try {
                            val responseText = response.body()!!.string()
                            val responseJson = JSONObject(responseText)
                            val dataJSON = responseJson.getJSONObject("data")
                            val translationsJsonArray = dataJSON.getJSONArray("translations")
                            val translationJSONObject = translationsJsonArray.getJSONObject(0)
                            val translatedText = translationJSONObject.getString("translatedText")
                            sendparticularmsgreply(translatedText)
                            MsgProgressBar!!.visibility = View.GONE
                            return
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        val data = sendMessage!!.text.toString().trim { it <= ' ' }
                        sendparticularmsgreply(data)
                        MsgProgressBar!!.visibility = View.GONE
                    }
                    MsgProgressBar!!.visibility = View.GONE
                }

                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                    DialogUtils.dismiss()
                }
            })
    }

    //-----------------------------------------------------Call Functionality Navigate------------------------------------------
    private fun sendparticularmsgreply(data: String) {
        var data_at = data
        var isTagapplied = 0
        val names = ArrayList<String>()
        val userID = ArrayList<String>()
        if (isGroupChat) {
            if (enableGroupChat) {
                if (at_memberlist != null && at_memberlist!!.size >= 0) for (groupMembersPojo: GroupMembersPojo in at_memberlist!!) {
                    val userName = "@" + groupMembersPojo.contactName
                    names.add(userName)
                    userID.add(groupMembersPojo.userId)
                    if (data_at.contains(userName)) {
                        data_at = data.replace(
                            userName,
                            TextMessage.TAG_KEY + groupMembersPojo.userId + TextMessage.TAG_KEY
                        )
                        isTagapplied = 1
                    }
                }
            }
        }
        if (!data.equals("", ignoreCase = true)) {
            if (session!!.getarchivecount() != 0) {
                if (session!!.getarchive(from + "-" + to)) session!!.removearchive(from + "-" + to)
            }
            if (session!!.getarchivecountgroup() != 0) {
                if (session!!.getarchivegroup(from + "-" + to + "-g")) session!!.removearchivegroup(
                    from + "-" + to + "-g"
                )
            }
            val messageEvent = SendMessageEvent()
            val message = MessageFactory.getMessage(MessageFactory.text, this) as TextMessage
            val msgObj: JSONObject
            if (isGroupChat) {
                messageEvent.eventName = SocketManager.EVENT_GROUP
                msgObj = message.getGroupMessageObject(
                    to,
                    data_at,
                    user_name!!.text.toString()
                ) as JSONObject
                try {
                    msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_REPlY_MESSAGE)
                    msgObj.put("userName", user_name!!.text.toString())
                    msgObj.put("recordId", mymsgid)
                    msgObj.put("quotedMessage", quotedMessage)
                } catch (ex: JSONException) {
                    ex.printStackTrace()
                }
            } else {
                messageEvent.eventName = SocketManager.EVENT_REPLY_MESSAGE
                msgObj = message.getMessageObject(to, data, false) as JSONObject
                try {
                    msgObj.put("recordId", mymsgid)
                    msgObj.put("quotedMessage", quotedMessage)
                    msgObj.put("translated_message", "")
                } catch (ex: JSONException) {
                    ex.printStackTrace()
                }
            }
            messageEvent.messageObject = msgObj
            val item = message.createMessageItem(
                true, data,
                MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId, user_name!!.text.toString()
            )
            item.replyType = replytype
            if (isTagapplied == 1) {
                item.isTagapplied = true
                item.setArrayTagnames = names
                item.editTextmsg = data
                item.userId_tag = userID
            }
            if (replytype!!.toInt() == MessageFactory.text) {
                item.replyMessage = messageold
            } else if (replytype!!.toInt() == MessageFactory.picture) {
                item.setreplyimagepath(imgpath)
                item.setreplyimagebase64(imageAsBytes)
            } else if (replytype!!.toInt() == MessageFactory.audio) {
                item.replyMessage = "Audio"
            } else if (replytype!!.toInt() == MessageFactory.video) {
                item.replyMessage = "Video"
                item.setreplyimagebase64(imageAsBytes)
            } else if (replytype!!.toInt() == MessageFactory.document) {
                item.replyMessage = messageold
            } else if (replytype!!.toInt() == MessageFactory.web_link) {
                item.replyMessage = messageold
            } else if (replytype!!.toInt() == MessageFactory.contact) {
                item.replyMessage = contactname
            } else if (replytype!!.toInt() == MessageFactory.location) {
                if (locationName != null && locationName != "") {
                    item.replyMessage = locationName
                } else {
                    item.replyMessage = "Location"
                }
                item.setreplyimagebase64(imageAsBytes)
            }
            item.senderMsisdn = receiverMsisdn
            item.senderName = mReceiverName
            item.setReplySender(ReplySender)
            reply = false
            if (isGroupChat) {
                if (enableGroupChat) {
                    item.replyType = replytype
                    item.setreplyimagebase64(imageAsBytes)
                    if (replytype!!.toInt() == MessageFactory.text) {
                        item.replyMessage = messageold
                    } else if (replytype!!.toInt() == MessageFactory.picture) {
                        item.setreplyimagepath(imgpath)
                    } else if (replytype!!.toInt() == MessageFactory.audio) {
                        item.replyMessage = "Audio"
                    } else if (replytype!!.toInt() == MessageFactory.document) {
                        item.replyMessage = messageold
                    } else if (replytype!!.toInt() == MessageFactory.web_link) {
                        item.replyMessage = messageold
                    } else if (replytype!!.toInt() == MessageFactory.contact) {
                        item.replyMessage = contactname
                    }
                    item.groupName = user_name!!.text.toString()
                    messageDatabase!!.updateChatMessage(item, chatType)
                    if (!isAlreadyExist(item)) mChatData!!.add(item)
                    EventBus.getDefault().post(messageEvent)
                    notifyDatasetChange()
                    sendMessage!!.text.clear()
                } else {
                    Toast.makeText(
                        this@ChatPageActivity,
                        resources.getString(R.string.you_are_not_member_of_this_group),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                item.senderMsisdn = receiverMsisdn
                item.senderName = user_name!!.text.toString()
                messageDatabase!!.updateChatMessage(item, chatType)
                if (!isAlreadyExist(item)) mChatData!!.add(item)
                EventBus.getDefault().post(messageEvent)
                notifyDatasetChange()
                sendMessage!!.text.clear()
            }
        }
    }

    //-------------------------------------------------Call Audio Permission-------------------------------------------
    private fun sendWebLinkMessage() {
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            DisplayAlert("Unblock $mReceiverName to send message?")
        } else {
            val data = sendMessage!!.text.toString().trim { it <= ' ' }
            if (!data.equals("", ignoreCase = true)) {
                if (session!!.getarchivecount() != 0) {
                    if (session!!.getarchive(from + "-" + to)) session!!.removearchive(from + "-" + to)
                }
                if (session!!.getarchivecountgroup() != 0) {
                    if (session!!.getarchivegroup(from + "-" + to + "-g")) session!!.removearchivegroup(
                        from + "-" + to + "-g"
                    )
                }
                val messageEvent = SendMessageEvent()
                val message =
                    MessageFactory.getMessage(MessageFactory.web_link, this) as WebLinkMessage
                var webLinkThumb: String? = null
                if (ivWebLink!!.drawable != null) {
                    var linkBmp: Bitmap? = null
                    try {
                        linkBmp = (ivWebLink!!.drawable.current as BitmapDrawable).bitmap
                    } catch (e: Exception) {
                        linkBmp = (ivWebLink!!.drawable.current as GlideBitmapDrawable).bitmap
                    }
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    linkBmp!!.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
                    val byteArray = byteArrayOutputStream.toByteArray()
                    webLinkThumb = Base64.encodeToString(byteArray, Base64.DEFAULT)
                    try {
                        byteArrayOutputStream.close()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                var msgObj: JSONObject?
                val receiverName = user_name!!.text.toString()
                if (isGroupChat) {
                    messageEvent.eventName = SocketManager.EVENT_GROUP
                    msgObj = message.getGroupMessageObject(to, data, receiverName) as JSONObject
                } else {
                    msgObj = message.getMessageObject(to, data, false) as JSONObject
                    messageEvent.eventName = SocketManager.EVENT_MESSAGE
                }
                val item = message.createMessageItem(
                    true,
                    data,
                    MessageFactory.DELIVERY_STATUS_NOT_SENT,
                    mReceiverId,
                    receiverName,
                    webLink,
                    webLinkTitle,
                    webLinkDesc,
                    webLinkImgUrl,
                    webLinkThumb
                )
                msgObj = message.getWebLinkObject(
                    msgObj,
                    webLink,
                    webLinkTitle,
                    webLinkDesc,
                    webLinkImgUrl,
                    webLinkThumb
                ) as JSONObject
                messageEvent.messageObject = msgObj
                if (isGroupChat) {
                    if (enableGroupChat) {
                        item.groupName = user_name!!.text.toString()
                        messageDatabase!!.updateChatMessage(item, chatType)
                        if (!isAlreadyExist(item)) mChatData!!.add(item)
                        EventBus.getDefault().post(messageEvent)
                        notifyDatasetChange()
                    } else {
                        Toast.makeText(
                            this@ChatPageActivity,
                            resources.getString(R.string.you_are_not_member_of_this_group),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    item.senderMsisdn = receiverMsisdn
                    item.senderName = user_name!!.text.toString()
                    messageDatabase!!.updateChatMessage(item, chatType)
                    if (!isAlreadyExist(item)) mChatData!!.add(item)
                    EventBus.getDefault().post(messageEvent)
                    notifyDatasetChange()
                }
                sendMessage!!.text.clear()
                hasLinkInfo = false
                rlWebLink!!.visibility = View.GONE
                webLink = ""
                webLinkTitle = ""
                webLinkImgUrl = ""
                webLinkDesc = ""
            }
        }
    }

    //--------------------------------------------------Send Text Message Watcher-----------------------------------------------
    private fun Activecall(isVideoCall: Boolean) {
        if (ConnectivityInfo.isInternetConnected(this)) if (checkAudioRecordPermission()) {
            val message = CallMessage(this@ChatPageActivity)
            val isOutgoingCall = true
            val roomid = message.getroomid()
            val timestamp = message.getroomid()
            val callid = "$mCurrentUserId-$mReceiverId-$timestamp"
            if (!CallsActivity.isStarted) {
                if (isOutgoingCall) {
                    CallsActivity.opponentUserId = to
                }
                PreferenceManager.setDefaultValues(
                    context,
                    org.appspot.apprtc.R.xml.preferences,
                    false
                )
                val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
                val keyprefRoomServerUrl =
                    context!!.getString(org.appspot.apprtc.R.string.pref_room_server_url_key)
                val roomUrl = sharedPref.getString(
                    keyprefRoomServerUrl,
                    context!!.getString(org.appspot.apprtc.R.string.pref_room_server_url_default)
                )
                var videoWidth = 0
                var videoHeight = 0
                val resolution =
                    context!!.getString(org.appspot.apprtc.R.string.pref_resolution_default)
                val dimensions = resolution.split("[ x]+").toTypedArray()
                if (dimensions.size == 2) {
                    try {
                        videoWidth = dimensions[0].toInt()
                        videoHeight = dimensions[1].toInt()
                    } catch (e: NumberFormatException) {
                        videoWidth = 0
                        videoHeight = 0
                        //             MyLog.e("ScimboCallError", "Wrong video resolution setting: " + resolution);
                    }
                }
                mAdapter!!.stopAudioOnClearChat()
                val uri = Uri.parse(roomUrl)
                val intent = Intent(context, CallsActivity::class.java)
                intent.data = uri
                intent.putExtra(CallsActivity.EXTRA_IS_OUTGOING_CALL, isOutgoingCall)
                intent.putExtra(CallsActivity.EXTRA_DOC_ID, callid)
                intent.putExtra(CallsActivity.EXTRA_FROM_USER_ID, from)
                intent.putExtra(CallsActivity.EXTRA_TO_USER_ID, to)
                intent.putExtra(CallsActivity.EXTRA_USER_MSISDN, receiverMsisdn)
                intent.putExtra(CallsActivity.EXTRA_OPPONENT_PROFILE_PIC, "")
                intent.putExtra(
                    CallsActivity.EXTRA_NAVIGATE_FROM,
                    context!!.javaClass.simpleName
                ) // For navigating from call activity
                intent.putExtra(CallsActivity.EXTRA_CALL_CONNECT_STATUS, "0")
                intent.putExtra(CallsActivity.EXTRA_CALL_TIME_STAMP, timestamp)
                intent.putExtra(CallsActivity.EXTRA_ROOMID, roomid)
                intent.putExtra(CallsActivity.EXTRA_LOOPBACK, false)
                intent.putExtra(CallsActivity.EXTRA_VIDEO_CALL, isVideoCall)
                intent.putExtra(CallsActivity.EXTRA_SCREENCAPTURE, false)
                intent.putExtra(CallsActivity.EXTRA_CAMERA2, true)
                intent.putExtra(CallsActivity.EXTRA_VIDEO_WIDTH, videoWidth)
                intent.putExtra(CallsActivity.EXTRA_VIDEO_HEIGHT, videoHeight)
                intent.putExtra(CallsActivity.EXTRA_VIDEO_FPS, 0)
                intent.putExtra(CallsActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false)
                intent.putExtra(CallsActivity.EXTRA_VIDEO_BITRATE, 0)
                intent.putExtra(
                    CallsActivity.EXTRA_VIDEOCODEC,
                    context!!.getString(org.appspot.apprtc.R.string.pref_videocodec_default)
                )
                intent.putExtra(CallsActivity.EXTRA_HWCODEC_ENABLED, false)
                intent.putExtra(CallsActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, true)
                intent.putExtra(CallsActivity.EXTRA_FLEXFEC_ENABLED, false)
                intent.putExtra(CallsActivity.EXTRA_NOAUDIOPROCESSING_ENABLED, false)
                intent.putExtra(CallsActivity.EXTRA_AECDUMP_ENABLED, false)
                intent.putExtra(CallsActivity.EXTRA_OPENSLES_ENABLED, false)
                intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AEC, false)
                intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_AGC, false)
                intent.putExtra(CallsActivity.EXTRA_DISABLE_BUILT_IN_NS, false)
                intent.putExtra(CallsActivity.EXTRA_ENABLE_LEVEL_CONTROL, false)
                intent.putExtra(CallsActivity.EXTRA_AUDIO_BITRATE, 0)
                intent.putExtra(
                    CallsActivity.EXTRA_AUDIOCODEC,
                    context!!.getString(org.appspot.apprtc.R.string.pref_audiocodec_default)
                )
                intent.putExtra(CallsActivity.EXTRA_DISPLAY_HUD, false)
                intent.putExtra(CallsActivity.EXTRA_TRACING, false)
                intent.putExtra(CallsActivity.EXTRA_CMDLINE, false)
                intent.putExtra(CallsActivity.EXTRA_RUNTIME, 0)
                intent.putExtra(CallActivity.EXTRA_DATA_CHANNEL_ENABLED, true)
                intent.putExtra(CallActivity.EXTRA_ORDERED, true)
                intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS_MS, -1)
                intent.putExtra(CallActivity.EXTRA_MAX_RETRANSMITS, -1)
                intent.putExtra(
                    CallActivity.EXTRA_PROTOCOL,
                    context!!.getString(org.appspot.apprtc.R.string.pref_data_protocol_default)
                )
                intent.putExtra(CallActivity.EXTRA_NEGOTIATED, false)
                intent.putExtra(CallActivity.EXTRA_ID, -1)
                intent.putExtra(CallsActivity.EXTRA_AGORA_CALLING_KEY, mAgoraCallingKey)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                noNeedRefresh = true
                context!!.startActivity(intent)
            }
        } else {
            requestAudioRecordPermission()
        } else {
            Toast.makeText(
                applicationContext,
                getString(R.string.no_internet_connection),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun Redirecttocall() {
        if (ConnectivityInfo.isInternetConnected(this)) {
            //      Log.e(TAG,"isStarted"+CallsActivity.isStarted);
            val mCall = SharedPreference.getInstance().getBool(context, "isStarted")
            Log.e("mCall", "mCall$mCall")
            if (mCall) {
                val callIntent = Intent(
                    context,
                    CallsActivity::class.java
                )
                callIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                noNeedRefresh = true
                context!!.startActivity(callIntent)
            } else {
                //    Activecall(false);
                val callIntent = Intent(context, CallsActivity::class.java)
                callIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                noNeedRefresh = true
                context!!.startActivity(callIntent)
            }
            /*  if (CallsActivity.isStarted) {
                Intent callIntent = new Intent(context, CallsActivity.class);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                context.startActivity(callIntent);
            } else {
                Activecall(false);
            }*/
        }
    }

    private fun requestAudioRecordPermission() {
        ActivityCompat.requestPermissions(
            this@ChatPageActivity,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO),
            AUDIO_RECORD_PERMISSION_REQUEST_CODE
        )
    }

    private fun handleTypingEvent(length: Int) {
        if (length > 0) {
            val currentTime = Calendar.getInstance().timeInMillis
            val timeDiff = currentTime - fromLastTypedAt
            if (timeDiff > MessageFactory.TYING_MESSAGE_MIN_TIME_DIFFERENCE) {
                fromLastTypedAt = currentTime
                if (isGroupChat) {
                    if (enableGroupChat) {
                        sendTypeEvent()
                    }
                } else {
                    sendTypeEvent()
                }
            }
        }
    }

    private fun checkLinkDetails(text: String) {
        var text = text
        text = " $text"
        val arrSplitText = text.split(" ").toTypedArray()
        for (splitText: String in arrSplitText) {
            if (Patterns.WEB_URL.matcher(splitText).matches()) {
                Thread {
                    try {
                        var url: String = splitText
                        if (!splitText.startsWith("http")) {
                            url = "http://$splitText"
                        }
                        val doc = Jsoup.connect(url).get()
                        val metaElems = doc.select("meta")
                        runOnUiThread {
                            webLinkTitle = doc.title()
                            webLink = doc.baseUri()
                            webLinkImgUrl = "$webLink/favicon.ico"
                            tvWebTitle!!.text = webLinkTitle
                            tvWebLink!!.text = webLink
                            Glide.with(this@ChatPageActivity).load(webLinkImgUrl)
                                .into(ivWebLink)
                            if (rlWebLink!!.visibility == View.GONE) {
                                rlWebLink!!.visibility = View.VISIBLE
                            }
                        }
                        hasLinkInfo = true
                    } catch (ex: IOException) {
                        ex.printStackTrace()
                    } catch (nullEx: NullPointerException) {
                        hasLinkInfo = false
                        nullEx.printStackTrace()
                    } catch (e: Exception) {
                        hasLinkInfo = false
                        e.printStackTrace()
                    }
                }.start()
                break
            } else {
                hasLinkInfo = false
            }
        }
    }

    //-----------------------------------------------Menu Popup-------------------------------------------------------------
    private fun sendTypeEvent() {
        val blockedStatus = (contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")
        if (!blockedStatus) {
            val msgObj = JSONObject()
            val messageEvent = SendMessageEvent()
            messageEvent.eventName = SocketManager.EVENT_TYPING
            try {
                msgObj.put("from", from)
                msgObj.put("to", to)
                if (!isGroupChat) {
                    msgObj.put("convId", mConvId)
                    msgObj.put("type", "single")
                } else {
                    msgObj.put("convId", mGroupId)
                    msgObj.put("type", "group")
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            messageEvent.messageObject = msgObj
            EventBus.getDefault().post(messageEvent)
        }
    }

    override fun onOptionsMenuClosed(menu: Menu) {
        isMenuBtnClick = false
        super.onOptionsMenuClosed(menu)
    }

    private fun openMenu() {
        val popupView = findViewById<View>(R.id.popup)
        if (isMenuBtnClick) {
            val labelsList: MutableList<MultiTextDialogPojo> = ArrayList()
            val strBlock: String
            val strChatlock: String
            if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
                strBlock = getString(R.string.unblock)
            } else {
                strBlock = getString(R.string.block_)
            }
            var pojo = MultiTextDialogPojo()
            if (!isGroupChat) {
                pojo.labelText = strBlock
                labelsList.add(pojo)
            }
            pojo = MultiTextDialogPojo()
            pojo.labelText = getString(R.string.delete_chat)
            labelsList.add(pojo)
            pojo = MultiTextDialogPojo()
            pojo.labelText = getString(R.string.email_chat)
            labelsList.add(pojo)
            pojo = MultiTextDialogPojo()
            pojo.labelText = getString(R.string.add_shortcut)
            labelsList.add(pojo)
            if ((sessionManager!!.lockChatEnabled == "1")) {
                if (!checkIsAlreadyLocked(docId)) {
                    strChatlock = getString(R.string.lock_chat)
                } else {
                    strChatlock = getString(R.string.unlock_chat)
                }
                pojo = MultiTextDialogPojo()
                pojo.labelText = strChatlock
                labelsList.add(pojo)
            }
            val dialog = CustomMultiTextItemsDialog()
            dialog.setLabelsList(labelsList)
            if (!isMassChat) {
                dialog.setDialogItemClickListener { position ->
                    when (position) {
                        0 -> performMenuBlock()
                        1 -> performMenuClearChat()
                        2 -> performMenuEmailChat()
                        3 -> addShortcutConfirmationDialog()
                        4 -> performChatlock()
                    }
                }
            } else {
                dialog.setDialogItemClickListener { position ->
                    when (position) {
                        0 -> performMenuBlock()
                        1 -> performMenuClearChat()
                        2 -> performMenuDeleteChat()
                        3 -> performMenuEmailChat()
                        4 -> addShortcutConfirmationDialog()
                        5 -> performChatlock()
                    }
                }
            }
            dialog.show(supportFragmentManager, "custom multi")
        } else {
            val popup = PopupMenu(this, popupView)
            val inflater = popup.menuInflater
            inflater.inflate(R.menu.activity_chat_view, popup.menu)
            popup.menu.findItem(R.id.menuSearch).isVisible = false
            popup.menu.findItem(R.id.menuMute).isVisible = false
            popup.menu.findItem(R.id.menuWallpaper).isVisible = false
            popup.menu.findItem(R.id.menuMore).isVisible = false
            if (!isGroupChat) {
                popup.menu.findItem(R.id.menuSecretChat).isVisible = false
                popup.menu.findItem(R.id.menuBlock).isVisible = true
            } else {
                popup.menu.findItem(R.id.menuSecretChat).isVisible = false
            }
            popup.menu.findItem(R.id.menuClearChat).isVisible = true
            popup.menu.findItem(R.id.menuReportUser).isVisible = true
            if (isMassChat) {
                popup.menu.findItem(R.id.menudeletechat).isVisible = true
            }
            popup.menu.findItem(R.id.menuEmailChat).isVisible = false
            popup.menu.findItem(R.id.menuAddShortcut).isVisible = false
            popup.menu.findItem(R.id.menuChatLock).isVisible = false
            if ((sessionManager!!.lockChatEnabled == "1")) {
                if (!checkIsAlreadyLocked(docId)) {
                    popup.menu.findItem(R.id.menuChatLock).title =
                        resources.getString(R.string.lock_chat)
                } else {
                    popup.menu.findItem(R.id.menuChatLock).title =
                        resources.getString(R.string.unlock_chat)
                }
            } else {
                popup.menu.findItem(R.id.menuChatLock).isVisible = false
            }
            if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
                popup.menu.findItem(R.id.menuBlock).title = getString(R.string.unblock_contact)
            } else {
                popup.menu.findItem(R.id.menuBlock).title = getString(R.string.block)
            }
            if (!checkIsAlreadyLocked(docId)) {
                popup.menu.findItem(R.id.menuChatLock).title =
                    resources.getString(R.string.lock_chat)
            } else {
                popup.menu.findItem(R.id.menuChatLock).title =
                    resources.getString(R.string.unlock_chat)
            }
            popup.menu.findItem(R.id.menuBlock).setOnMenuItemClickListener {
                performMenuBlock()
                false
            }
            popup.menu.findItem(R.id.menuEmailChat).setOnMenuItemClickListener {
                performMenuEmailChat()
                false
            }
            popup.menu.findItem(R.id.menuClearChat).setOnMenuItemClickListener {
                performMenuClearChat()
                false
            }

            popup.menu.findItem(R.id.menuReportUser).setOnMenuItemClickListener {
                performMenuReportUser()
                false
            }
            if (isMassChat) {
                popup.menu.findItem(R.id.menudeletechat).setOnMenuItemClickListener {
                    performMenuDeleteChat()
                    false
                }
            }
            popup.menu.findItem(R.id.menuAddShortcut).setOnMenuItemClickListener {
                addShortcutConfirmationDialog()
                false
            }
            popup.menu.findItem(R.id.menuChatLock).setOnMenuItemClickListener {
                performChatlock()
                false
            }
            popup.show()
        }
    }

    private fun performMenuBlock() {
        val msg: String
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            msg = String.format(getString(R.string.you_want_unblock), mReceiverName)
        } else {
            msg = String.format(getString(R.string.you_want_block), mReceiverName)
        }
        val dialog = CustomAlertDialog()
        dialog.setMessage(msg)
        dialog.setPositiveButtonText(getString(R.string.ok))
        dialog.setNegativeButtonText(getString(R.string.cancel))
        dialog.setCustomDialogCloseListener(object : CustomAlertDialog.OnCustomDialogCloseListener {
            override fun onPositiveButtonClick() {
                dialog.dismiss()
                putBlockUser()
            }

            override fun onNegativeButtonClick() {
                dialog.dismiss()
            }
        })
        dialog.show(supportFragmentManager, "Block alert")
    }

    private fun performMenuClearChat() {
        val msg = getString(R.string.sure_you_want_to_clear_chat_msgs)
        val dialog = CustomAlertClearDialog()
        dialog.setNegativeButtonText(getString(R.string.cancel))
        dialog.setPositiveButtonText(getString(R.string.clear))
        dialog.setCheckBoxtext(getString(R.string.keep_starred_msg))
        val isMassChat = resources.getBoolean(R.bool.is_mass_chat)
        if (isMassChat) {
            dialog.setCheckBoxtext2(getString(R.string.also_delete_chat_for) + mReceiverName)
        }
        dialog.setMessage(msg)
        val docId: String
        deletestarred = false
        if (isGroupChat) {
            docId = mCurrentUserId + "-" + to + "-g"
        } else {
            docId = mCurrentUserId + "-" + to
        }
        dialog.setCheckBoxCheckedChangeListener { isChecked ->
            MyLog.e("Star", "Star$isChecked")
            deletestarred = isChecked
        }
        dialog.setCheckBox2CheckedChangeListener { isChecked -> // deletestarred = isChecked;
            // MyLog.e("delete for","Star"+isChecked);
            mClearChat = isChecked
        }
        dialog.setCustomDialogCloseListener(object :
            CustomAlertClearDialog.OnCustomDialogCloseListener {
            override fun onPositiveButtonClick() {
                var star_status = 0
                if (deletestarred) {
                    star_status = 1
                    // db.clearUnStarredMessage(docId);
                }
                if (!isGroupChat) {
                    if (userInfoSession!!.hasChatConvId(docId)) {
                        if (mChatData!!.size > 0) {
                            try {
                                val array = mChatData!![mChatData!!.size - 1]!!
                                    .messageId.split("-").toTypedArray()
                                val convId = userInfoSession!!.getChatConvId(docId)
                                val `object` = JSONObject()
                                `object`.put("convId", convId)
                                `object`.put("from", mCurrentUserId)
                                //   boolean isMassChat = getResources().getBoolean(R.bool.is_mass_chat);
                                //  if (isMassChat) {
                                if (mClearChat) {
                                    `object`.put("delete_opponent", "1")
                                } else {
                                    `object`.put("delete_opponent", "0")
                                }
                                `object`.put("lastId", array[2])
                                // }
                                `object`.put("from", mCurrentUserId)
                                `object`.put(
                                    "MessageId",
                                    mChatData!![mChatData!!.size - 1]!!.messageId
                                )
                                `object`.put("star_status", star_status)
                                `object`.put("type", MessageFactory.CHAT_TYPE_SINGLE)
                                val event = SendMessageEvent()
                                event.eventName = SocketManager.EVENT_CLEAR_CHAT
                                event.messageObject = `object`
                                deletestarred = false
                                mClearChat = false
                                Log.e(
                                    TAG,
                                    "EVENT_CLEAR_CHAT$`object`"
                                )
                                EventBus.getDefault().post(event)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }
                        // db.clearAllSingleChatMessage(docId);
                    }
                } else {
                    try {
                        val `object` = JSONObject()
                        `object`.put("convId", to)
                        `object`.put("from", mCurrentUserId)
                        `object`.put("star_status", star_status)
                        `object`.put("type", MessageFactory.CHAT_TYPE_GROUP)
                        val event = SendMessageEvent()
                        event.eventName = SocketManager.EVENT_CLEAR_CHAT
                        event.messageObject = `object`
                        deletestarred = false
                        EventBus.getDefault().post(event)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onNegativeButtonClick() {
                dialog.dismiss()
            }
        })
        /*    dialog.setCustomDialogCloseListener2(new CustomAlertDialog.OnCustomDialogCloseListener() {
            @Override
            public void onPositiveButtonClick() {
                int star_status = 0;
                if (deletestarred) {
                    star_status = 1;
                    // db.clearUnStarredMessage(docId);
                }
                if (!isGroupChat) {
                    if (userInfoSession.hasChatConvId(docId)) {
                        try {
                            String convId = userInfoSession.getChatConvId(docId);
                            JSONObject object = new JSONObject();
                            object.put("convId", convId);
                            object.put("from", mCurrentUserId);


                            object.put("from", mCurrentUserId);

                            object.put("star_status", star_status);
                            object.put("type", MessageFactory.CHAT_TYPE_SINGLE);
                            SendMessageEvent event = new SendMessageEvent();
                            event.setEventName(SocketManager.EVENT_CLEAR_CHAT);
                            event.setMessageObject(object);
                            EventBus.getDefault().post(event);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // db.clearAllSingleChatMessage(docId);
                    }
                } else {

                    try {
                        JSONObject object = new JSONObject();
                        object.put("convId", to);
                        object.put("from", mCurrentUserId);

                        object.put("from", mCurrentUserId);




                        object.put("star_status", star_status);
                        object.put("type", MessageFactory.CHAT_TYPE_GROUP);
                        SendMessageEvent event = new SendMessageEvent();
                        event.setEventName(SocketManager.EVENT_CLEAR_CHAT);
                        event.setMessageObject(object);
                        EventBus.getDefault().post(event);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }

            @Override
            public void onNegativeButtonClick() {
                dialog.dismiss();
            }
        });

    */dialog.show(supportFragmentManager, "Delete member alert")
    }
    private fun performMenuReportUser() {
//        val intent = Intent(Intent.ACTION_SENDTO)
//        intent.data = Uri.parse("mailto:") // only email apps should handle this
//        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("info@hydeparkcorner.com"));
//        intent.putExtra(Intent.EXTRA_SUBJECT, "Report: "+ mReceiverName +" - " + receiverpincode)
//        intent.putExtra(Intent.EXTRA_TEXT, "Hi,\n\n" +
//                "Please review my report against the user "+ mReceiverName +" - "+ receiverpincode +". I feel this user is not aligned with myChat terms and conditions.\n\n" +
//                "Thank you.");
//            startActivity(intent)
//

        val email = "info@hydeparkcorner.com"
        val subject = "Report: "+ mReceiverName +" - " + receiverpincode
        val body = "Hi,\n\n" +
                "Please review my report against the user "+ mReceiverName +" - "+ receiverpincode +". I feel this user is not aligned with myChat terms and conditions.\n\n" +
                "Thank you."

        val selectorIntent = Intent(Intent.ACTION_SENDTO)
        val urlString = "mailto:" + Uri.encode(email) + "?subject=" + Uri.encode(subject) + "&body=" + Uri.encode(body)
        selectorIntent.data = Uri.parse(urlString)

        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        emailIntent.putExtra(Intent.EXTRA_TEXT, body)
        emailIntent.selector = selectorIntent

        startActivity(Intent.createChooser(emailIntent, "Send email"))

    }

       fun getUserPin(userId: String?) {
        val eventObj = Getcontactname.getUserDetailsObject(this, userId)
        val event = SendMessageEvent()
        event.eventName = SocketManager.EVENT_GET_USER_DETAILS
        event.messageObject = eventObj
        EventBus.getDefault().post(event)
    }

    private fun loadUserDetails(event: ReceviceMessageEvent) {
        val data = event.objectsArray
        try {
            val `object` = JSONObject(data[0].toString())
            Log.d("response: ", "sc_get_user_Details: $`object`")
            if (`object`.has("pinCode")) {
                receiverpincode = `object`.getString("pinCode")
            } else {
                receiverpincode = resources.getString(R.string.default_user_status)
            }
        } catch (e: JSONException) {
            MyLog.e(TAG, "", e)
        }
    }
    private fun performMenuDeleteChat() {
        val msg =
            resources.getString(R.string.delete_chat_with) + mReceiverName + resources.getString(R.string.question_mark)
        val dialogg = CustomAlertDialog()
        //  dialogg.setNegativeButtonText("Delete Chat");
        // dialogg.setPositiveButtonText("Cancel");
        dialogg.setNegativeButtonText(getString(R.string.cancel))
        dialogg.setPositiveButtonText(getString(R.string.delete_chat))
        val mMessage = String.format(getString(R.string.also_delete_for), mReceiverName)
        dialogg.setCheckBoxtext(mMessage)
        dialogg.setMessage(msg)
        val docId: String
        // deletestarred = false;
        if (isGroupChat) {
            docId = mCurrentUserId + "-" + to + "-g"
        } else {
            docId = mCurrentUserId + "-" + to
        }
        dialogg.setCheckBoxCheckedChangeListener { isChecked -> //       deletestarred = isChecked;
            mCheckedDelete = isChecked
        }
        dialogg.setCustomDialogCloseListener(object :
            CustomAlertDialog.OnCustomDialogCloseListener {
            override fun onPositiveButtonClick() {

                //Check it is group or single
                try {
                    val `object` = JSONObject()
                    if (mCheckedDelete) {
                        `object`.put("delete_opponent", "1")
                    } else {
                        `object`.put("delete_opponent", "0")
                    }
                    val docId = from + "-" + to
                    if (mChatData!!.size > 0) {
                        val array = mChatData!![mChatData!!.size - 1]!!
                            .messageId.split("-").toTypedArray()
                        Log.e(
                            "lastId", "lastId" + mChatData!![mChatData!!.size - 1]!!
                                .messageId
                        )
                        Log.e("lastId", "lastId" + array[2])
                        var mConvId = mChatData!![mChatData!!.size - 1]!!.convId
                        if (AppUtils.isEmpty(mConvId)) {
                            mConvId = userInfoSession!!.getChatConvId(docId)
                        }
                        `object`.put("lastId", array[2])
                        Log.e("mConvId", "mConvId$mConvId")
                        `object`.put("convId", mConvId)
                        `object`.put("MessageId", mChatData!![mChatData!!.size - 1]!!.messageId)
                    } else {
                        `object`.put("lastId", "0")
                        var mConvId: String? = ""
                        if (AppUtils.isEmpty(mConvId)) {
                            mConvId = userInfoSession!!.getChatConvId(docId)
                        }
                        `object`.put("convId", mConvId)
                        `object`.put("MessageId", "$docId-0")
                    }
                    `object`.put("from", mCurrentUserId)
                    `object`.put("type", MessageFactory.CHAT_TYPE_SINGLE)
                    // object.put("MessageId", mChatData.get(mChatData.size() - 1).getMessageId());
                    val event = SendMessageEvent()
                    event.eventName = SocketManager.EVENT_DELETE_CHAT
                    MyLog.e(
                        "EVENT_DELETE_CHAT",
                        "EVENT_DELETE_CHAT$`object`"
                    )
                    event.messageObject = `object`
                    EventBus.getDefault().post(event)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                //   }
            }

            override fun onNegativeButtonClick() {
//                Log.e("onNegativeButtonClick", "onNegativeButtonClick" + mChatData.get(mChatData.size() - 1).getMessageId());
                dialogg.dismiss()
            }
        })
        dialogg.show(supportFragmentManager, "Delete member alert")
    }

    private fun performMenuEmailChat() {
        val msg = resources.getString(R.string.attaching_media_will_generate_a_large_email_message)
        val dialog = CustomAlertDialog()
        dialog.setNegativeButtonText(resources.getString(R.string.without_media))
        dialog.setPositiveButtonText(resources.getString(R.string.attach_media))
        dialog.setMessage(msg)
        dialog.setCustomDialogCloseListener(object : CustomAlertDialog.OnCustomDialogCloseListener {
            override fun onPositiveButtonClick() {
                dialog.dismiss()
                val dialogpr = ProgressDialog.show(
                    this@ChatPageActivity, "",
                    resources.getString(R.string.loading_please_wait_dots), true
                )
                val timer2 = Timer()
                timer2.schedule(object : TimerTask() {
                    override fun run() {
                        dialogpr.dismiss()
                        timer2.cancel() //this will cancel the timer of the system
                    }
                }, 2500) // the timer will count 2.5 seconds....
                emailgmail!!.visibility = View.VISIBLE
                val animation = AnimationUtils.loadAnimation(
                    applicationContext,
                    R.anim.bottom_up
                )
                emailgmail!!.animation = animation
                dialogpr.dismiss()
                emai1send!!.setOnClickListener {
                    gmailintent = false
                    val docId: String
                    var chatType: String = MessageFactory.CHAT_TYPE_SINGLE
                    if (isGroupChat) {
                        docId =
                            mCurrentUserId + "-" + to + "-g"
                        chatType = MessageFactory.CHAT_TYPE_GROUP
                    } else {
                        docId =
                            mCurrentUserId + "-" + to
                    }
                    val emailChat = EmailChatHistoryUtils(this@ChatPageActivity)
                    emailChat.send(docId, user_name!!.text.toString(), true, false, chatType)
                    emailgmail!!.visibility = View.GONE
                }
                gmailsend!!.setOnClickListener { //  sendgmail();
                    // textfilesend();
                    gmailintent = true
                    val docId: String
                    var chatType: String = MessageFactory.CHAT_TYPE_SINGLE
                    if (isGroupChat) {
                        docId =
                            mCurrentUserId + "-" + to + "-g"
                        chatType = MessageFactory.CHAT_TYPE_GROUP
                    } else {
                        docId =
                            mCurrentUserId + "-" + to
                    }
                    val emailChat = EmailChatHistoryUtils(this@ChatPageActivity)
                    emailChat.send(docId, user_name!!.text.toString(), true, true, chatType)
                    emailgmail!!.visibility = View.GONE
                }
            }

            override fun onNegativeButtonClick() {
                dialog.dismiss()
                emailgmail!!.visibility = View.VISIBLE
                val dialogpr = ProgressDialog.show(
                    this@ChatPageActivity, "",
                    resources.getString(R.string.loading_please_wait_dots), true
                )
                val animation = AnimationUtils.loadAnimation(
                    applicationContext,
                    R.anim.bottom_up
                )
                emailgmail!!.animation = animation
                dialogpr.dismiss()
                emai1send!!.setOnClickListener {
                    val docId: String
                    var chatType: String = MessageFactory.CHAT_TYPE_SINGLE
                    if (isGroupChat) {
                        docId =
                            mCurrentUserId + "-" + to + "-g"
                        chatType = MessageFactory.CHAT_TYPE_GROUP
                    } else {
                        docId =
                            mCurrentUserId + "-" + to
                    }
                    val emailChat = EmailChatHistoryUtils(this@ChatPageActivity)
                    emailChat.send(docId, user_name!!.text.toString(), false, false, chatType)

                    //sendgmailmedia();
                    emailgmail!!.visibility = View.GONE
                }
                gmailsend!!.setOnClickListener {
                    val docId: String
                    var chatType: String = MessageFactory.CHAT_TYPE_SINGLE
                    if (isGroupChat) {
                        docId =
                            mCurrentUserId + "-" + to + "-g"
                        chatType = MessageFactory.CHAT_TYPE_GROUP
                    } else {
                        docId =
                            mCurrentUserId + "-" + to
                    }
                    val emailChat = EmailChatHistoryUtils(this@ChatPageActivity)
                    emailChat.send(docId, user_name!!.text.toString(), false, true, chatType)

                    //   sendgmailmedia();
                    emailgmail!!.visibility = View.GONE
                }
            }
        })
        dialog.show(supportFragmentManager, "Delete member alert")
    }

    private fun addShortcutConfirmationDialog() {
        val dialog = CustomAlertDialog()
        dialog.setMessage(resources.getString(R.string.do_you_want_to_create_shortcut))
        dialog.setPositiveButtonText(resources.getString(R.string.zain_dialog_btn_txt))
        dialog.setNegativeButtonText(resources.getString(R.string.cancel))
        dialog.setCustomDialogCloseListener(object : CustomAlertDialog.OnCustomDialogCloseListener {
            override fun onPositiveButtonClick() {
                val receiverName = user_name!!.text.toString()
                val bitmap: Bitmap? = null
                if (receiverAvatar != null && receiverAvatar != "") {
                    addShortcut(receiverAvatar, isGroupChat)
                } else {
                    val drawable = user_profile_image!!.drawable
                    if (drawable != null) {
                        try {
                            myTemp = (drawable as BitmapDrawable).bitmap
                            // bitmap = Bitmap.createScaledBitmap(myTemp, 128, 128, true);
                        } catch (ex: ClassCastException) {
                            try {
                                myTemp =
                                    (user_profile_image!!.drawable.current as GlideBitmapDrawable).bitmap
                                //     bitmap = Bitmap.createScaledBitmap(myTemp, 128, 128, true);
                            } catch (e: Exception) {
                                MyLog.e(TAG, "onPositiveButtonClick: ", e)
                            }
                        } catch (ex2: Exception) {
                            MyLog.e(TAG, "onPositiveButtonClick: ", ex2)
                        }
                    }
                    ShortcutBadgeManager.addChatShortcut(
                        this@ChatPageActivity, isGroupChat, mReceiverId, receiverName,
                        receiverAvatar, receiverMsisdn, myTemp
                    )
                }
            }

            override fun onNegativeButtonClick() {}
        })
        dialog.show(supportFragmentManager, "CustomAlert")
    }

    private fun addShortcut(path: String?, isGroupChat: Boolean) {
        var path = path
        try {
            var receiverId = to
            if (isGroupChat) receiverId = mGroupId
            MyLog.d(
                TAG,
                "loadImage imagetest1: $path"
            )
            if (isGroupChat && path!!.isEmpty() || AppUtils.isEmptyImage(path)) {
                MyLog.d(TAG, "loadImage imagetest2 empty image: ")
                ShortcutBadgeManager.addChatShortcut(
                    this, isGroupChat, receiverId, receiverName,
                    receiverAvatar, receiverMsisdn, null
                )
            } else {
                if (isGroupChat) path = AppUtils.getValidGroupPath(path) else {
                    val userImagePath = contactDB_sqlite!!.getSingleData(
                        receiverId,
                        ContactDB_Sqlite.AVATARIMAGEURL
                    )
                    path = AppUtils.getValidProfilePath(userImagePath)
                }
                val glideRequestmgr: BitmapTypeRequest<*> =
                    Glide.with(this).load(AppUtils.getGlideURL(path, this)).asBitmap()
                val finalReceiverId = receiverId
                glideRequestmgr.diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .dontAnimate()
                    .into(object : SimpleTarget<Bitmap>() {

                        override fun onLoadFailed(e: Exception, errorDrawable: Drawable) {
                            super.onLoadFailed(e, errorDrawable)
                            ShortcutBadgeManager.addChatShortcut(
                                context, isGroupChat, finalReceiverId, receiverName,
                                receiverAvatar, receiverMsisdn, null
                            )
                        }

                        override fun onResourceReady(
                            resource: Bitmap?,
                            glideAnimation: GlideAnimation<in Bitmap?>?
                        ) {
                            ShortcutBadgeManager.addChatShortcut(
                                context, isGroupChat, finalReceiverId, receiverName,
                                receiverAvatar, receiverMsisdn, resource
                            )
                        }
                    })
            }
        } catch (e: Exception) {
            MyLog.e(TAG, "LoadImage: ", e)
        }
    }

    private fun checkAndOpenChatUnlockDialog() {
        val isLockChat = intent.getBooleanExtra("isLockChat", false)
        if (isLockChat) {
            val docId: String
            if (isGroupChat) {
                chatType = MessageFactory.CHAT_TYPE_GROUP
                docId = mCurrentUserId + "-" + to + "-g"
            } else {
                chatType = MessageFactory.CHAT_TYPE_SINGLE
                docId = mCurrentUserId + "-" + to
            }
            val convId = userInfoSession!!.getChatConvId(docId)
            val receiverId = userInfoSession!!.getReceiverIdByConvId(convId)
            val lockPojo = messageDatabase!!.getChatLockData(receiverId, chatType)
            if (lockPojo != null) {
                val stat = lockPojo.status
                val pwd = lockPojo.password
                unlockChat(docId, pwd, 0)
            }
        }
    }

    private fun unlockChat(docId: String, pwd: String, position: Int) {
        val convId = userInfoSession!!.getChatConvId(docId)
        val dialog = ChatLockPwdDialog()
        dialog.setTextLabel1(resources.getString(R.string.enter_your_pwd_label))
        dialog.setEditTextdata(resources.getString(R.string.hint_newPwd))
        dialog.setforgotpwdlabel(resources.getString(R.string.forgotChatpwd))
        dialog.setHeader(resources.getString(R.string.unlock_chat))
        dialog.setButtonText(resources.getString(R.string.unlock))
        dialog.isCancelable = false

        //  Objects.requireNonNull(dialog.getDialog().getWindow()).setDimAmount(1f);
        val bundle = Bundle()
        // bundle.putSerializable("MessageItem", mChatDat);
        bundle.putString("convID", convId)
        bundle.putString("status", "1")
        bundle.putString("pwd", pwd)
        bundle.putString("page", "chatlist")
        bundle.putString("type", "single")
        bundle.putString("from", mCurrentUserId)
        bundle.putBoolean("isShortCutPasswordCheck", true)
        dialog.arguments = bundle
        dialog.show(supportFragmentManager, "chatunLock")
    }

    private fun performChatlock() {
        if ((sessionManager!!.lockChatEnabled == "1")) {
            emailChatlock = sessionManager!!.userEmailId
            recemailChatlock = sessionManager!!.recoveryEMailId
            recPhoneChatlock = sessionManager!!.recoveryPhoneNo
            val emailVerifyStatus = sessionManager!!.chatLockEmailIdVerifyStatus
            if ((emailChatlock != "" && recemailChatlock != "" && recPhoneChatlock != ""
                        && emailVerifyStatus.equals("yes", ignoreCase = true))
            ) {
                val docId: String
                if (isGroupChat) {
                    docId = mCurrentUserId + "-" + to + "-g"
                } else {
                    docId = mCurrentUserId + "-" + to
                }
                val convId = userInfoSession!!.getChatConvId(docId)
                val receiverId = userInfoSession!!.getReceiverIdByConvId(convId)
                val lockPojo = messageDatabase!!.getChatLockData(receiverId, chatType)
                if (lockPojo != null) {
                    val stat = lockPojo.status
                    val pwd = lockPojo.password
                    if ((stat == "0")) {
                        if (ConnectivityInfo.isInternetConnected(this@ChatPageActivity)) {
                            openChatLockDialog(docId)
                        } else {
                            showInternetAlert()
                        }
                    } else {
                        openUnlockChatDialog(docId, stat, pwd, true)
                    }
                } else {
                    if (ConnectivityInfo.isInternetConnected(this@ChatPageActivity)) {
                        openChatLockDialog(docId)
                    } else {
                        showInternetAlert()
                    }
                }
            } else {
                val intent = Intent(
                    this@ChatPageActivity,
                    EmailSettings::class.java
                )
                startActivity(intent)
            }
        }
    }

    private fun openUnlockChatDialog(
        docId: String, status: String, pwd: String,
        isCancelable: Boolean
    ) {
        val convId = userInfoSession!!.getChatConvId(docId)
        val dialog = ChatLockPwdDialog()
        dialog.setTextLabel1(resources.getString(R.string.enter_your_pwd_label))
        dialog.setEditTextdata(resources.getString(R.string.hint_newPwd))
        dialog.setforgotpwdlabel(resources.getString(R.string.forgotChatpwd))
        dialog.setHeader(resources.getString(R.string.unlock_chat))
        dialog.setButtonText(resources.getString(R.string.unlock))
        dialog.isCancelable = isCancelable
        //dialog.getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        val bundle = Bundle()
        if (docId.contains("-g")) {
            bundle.putString("convID", to)
        } else {
            bundle.putString("convID", convId)
        }
        bundle.putString("status", "1")
        bundle.putString("pwd", pwd)
        bundle.putString("page", "chatview")
        if (docId.contains("-g")) {
            bundle.putString("type", "group")
        } else {
            bundle.putString("type", "single")
        }
        bundle.putString("from", mCurrentUserId)
        dialog.arguments = bundle
        dialog.show(supportFragmentManager, "chatunLock")
    }

    private fun showInternetAlert() {
        val dialog = CustomAlertDialog()
        dialog.setMessage(resources.getString(R.string.check_your_internet_connection))
        dialog.setNegativeButtonText(resources.getString(R.string.cancel))
        dialog.setPositiveButtonText(resources.getString(R.string.zain_dialog_btn_txt))
        dialog.isCancelable = false
        dialog.setCustomDialogCloseListener(object : CustomAlertDialog.OnCustomDialogCloseListener {
            override fun onPositiveButtonClick() {
                dialog.dismiss()
            }

            override fun onNegativeButtonClick() {
                dialog.dismiss()
            }
        })
        dialog.show(supportFragmentManager, "Delete member alert")
    }

    private fun openChatLockDialog(docId: String) {
        convId = userInfoSession!!.getChatConvId(docId)
        val dialog = ChatLockPwdDialog()
        dialog.setTextLabel1(resources.getString(R.string.newPasswordLabel))
        dialog.setTextLabel2(resources.getString(R.string.confirmPasswordLabel))
        dialog.setEditTextdata(resources.getString(R.string.hint_newPwd))
        dialog.setEditTextdata2(resources.getString(R.string.hint_confirmPwd))
        dialog.setHeader(resources.getString(R.string.setpassWordlabel))
        dialog.setButtonText(resources.getString(R.string.lock))
        val bundle = Bundle()
        bundle.putString("from", mCurrentUserId)
        if (docId.contains("-g")) {
            bundle.putString("type", "group")
        } else {
            bundle.putString("type", "single")
        }
        bundle.putString("page", "chatview")
        if (docId.contains("-g")) {
            bundle.putString("convID", to)
        } else {
            bundle.putString("convID", convId)
        }
        bundle.putString("status", "0")
        dialog.arguments = bundle
        dialog.show(supportFragmentManager, "chatLock")
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
        //  MyLog.d(TAG, "@@@@ onItemClick: ");
        //Hide if any view is visible
        hideMenu()
        itemClicked(position)
    }

    /*
    private void itemClicked(int position) {
        if (isFirstItemSelected) {

            MyLog.d(TAG, "@@@@ onItemClick: ");

            long currentMillis = System.currentTimeMillis();
            long difference = (currentMillis - longPressMillis);
            MyLog.d(TAG, "onItemClick: " + difference);

            if (difference < 300)
                return;

            if (isEncryptionInfo(position)) {
                return;
            }


            performSelection(position);
            if (selectedChatItems.size() == 0) {
                showUnSelectedActions();
            }

        }
    }
*/
    private fun itemClicked(position: Int) {
        if (isFirstItemSelected) {
            MyLog.d(TAG, "@@@@ onItemClick: ")
            val currentMillis = System.currentTimeMillis()
            val difference = (currentMillis - longPressMillis)
            MyLog.d(
                TAG,
                "onItemClick: $difference"
            )
            if (difference < 300) return
            if (isEncryptionInfo(position)) {
                return
            }
            performSelection(position)
            if (selectedChatItems!!.size == 0) {
                showUnSelectedActions()
            }
        }
    }

    private fun itemLongClicked(position: Int): Boolean {
        val selection = mChatData!![position]!!.isSelected
        val isDeletedMsgItem = performSelection(position)
        if (isDeletedMsgItem) return false
        if (isEncryptionInfo(position)) {
            return false
        }
        if (selectedChatItems!!.size <= 0) {
            showUnSelectedActions()
        } else {
            isFirstItemSelected = true
            iBtnBack2!!.visibility = View.GONE
            Search1!!.visibility = View.GONE

            //CheckMessageItem

            /*   String msgType = mChatData.get(position).getMessageType();
            int type = Integer.parseInt(msgType);
            if (mChatData.get(position).getDownloadStatus() == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {


                if (type == MessageFactory.audio ||
                        type == MessageFactory.document ||
                        type == MessageFactory.video ||
                        type == MessageFactory.picture) {
                    mSelectedPath = mChatData.get(position).getChatFileLocalPath();
                    share.setVisibility(View.VISIBLE);
                } else {
                    share.setVisibility(View.GONE);
                }
            } else {
                share.setVisibility(View.GONE);

            }*/
        }
        MyLog.d(TAG, "@@@@ onItemLongClick: long press")
        longPressMillis = System.currentTimeMillis()
        info!!.setOnClickListener {
            val messageItemChat = mChatData!![position]
            if (!isGroupChat) {
                val intent = Intent(
                    context,
                    SingleMessageInfoActivity::class.java
                )
                intent.putExtra("selectedData", messageItemChat as Parcelable?)
                noNeedRefresh = true
                startActivity(intent)
            } else {
                val groupinfointent = Intent(
                    context,
                    GroupMessageInfoActivity::class.java
                )
                groupinfointent.putExtra("selectedData", messageItemChat as Parcelable?)
                messageItemChat!!.thumb_bitmap_image = null
                val bundle = Bundle()
                bundle.putParcelable("PARAM_BITMAP", messageItemChat.thumb_bitmap_image)
                groupinfointent.putExtra("bundle", bundle)
                noNeedRefresh = true
                startActivity(groupinfointent)
            }
            mChatData!!.get(0)!!.isSelected = false
            Log.e(
                TAG,
                "onItemLongClick: notifyDataSetChanged"
            )
            mAdapter!!.notifyDataSetChanged()
            showUnSelectedActions()
        }
        return true
    }

    private fun checkIsAlreadyLocked(docId: String): Boolean {
        val convId = userInfoSession!!.getChatConvId(docId)
        val receiverId = userInfoSession!!.getReceiverIdByConvId(convId)
        val lockPojo = messageDatabase!!.getChatLockData(receiverId, chatType)
        if ((sessionManager!!.lockChatEnabled == "1") && lockPojo != null) {
            val stat = lockPojo.status
            val pwd = lockPojo.password
            isAlrdychatlocked = (stat == "1")
        } else {
            isAlrdychatlocked = false
        }
        return isAlrdychatlocked
    }

    private fun CopyClick() {
        Search1!!.setText("")
        var finaltext = ""
        if (selectedChatItems!!.size == 1) {
            finaltext = selectedChatItems!![0]!!
                .textMessage
            if ((finaltext == getString(R.string.you_deleted_text))) return
        } else {
            for (chatItem: MessageItemChat? in selectedChatItems!!) {
                val mesgid = chatItem!!.messageId
                val array = mesgid.split("-").toTypedArray()
                if (!isGroupChat) {
                    date = array[2]
                } else {
                    if (array.size > 3) {
                        date = array[3]
                    }
                }
                val l = date!!.toLong()
                val d = Date(l)
                val sdf: DateFormat = SimpleDateFormat("dd/MM/yyyy")
                val myDate = sdf.format(d)
                val dateObj = Date(l)
                var timeStamp = SimpleDateFormat("h:mm a", Locale.ENGLISH).format(dateObj)
                timeStamp = timeStamp.replace(".", "")
                val msisdn = chatItem.senderName
                val sendername = getcontactname!!.getGroupMemberName(chatItem.groupMsgFrom, msisdn)
                val textmessage =
                    ("\n" + "[" + myDate + ", " + timeStamp + "] " + sendername + ": " + chatItem.textMessage)
                finaltext = finaltext + textmessage
            }
        }
        val cm = context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("text", finaltext)
        cm.setPrimaryClip(clip)
        Toast.makeText(
            this@ChatPageActivity,
            resources.getString(R.string.message_copied),
            Toast.LENGTH_SHORT
        ).show()
        showUnSelectedActions()
    }

    private fun showOriginalClick() {
        Search1!!.setText("")
        for (chatItem: MessageItemChat? in selectedChatItems!!) {
            val messageObj = chatItem!!.messageObject
            chatItem.messageObject = null
        }
        showUnSelectedActions()
    }

    private fun ForwardClick() {
        val bundle = Bundle()
        //bundle.putSerializable("MsgItemList", selectedChatItems);
        CommonData.setForwardedItems(selectedChatItems)
        bundle.putBoolean("FromScimbo", true)
        val intent = Intent(context, ForwardContact::class.java)
        intent.putExtras(bundle)
        startActivityForResult(intent, REQUEST_CODE_FORWARD_MSG)
    }

    private fun ShareClick(tempUri: Uri) {
        val blacklist = arrayOf("com.any.package", "net.other.package")
        val intent = Intent(Intent.ACTION_SEND)
        if (mSelectedType != null) {
            if ((mSelectedType == "" + MessageFactory.picture)) {
                intent.type = "image/*"
            } else if ((mSelectedType == "" + MessageFactory.video)) {
                intent.type = "video/*"
            } else if ((mSelectedType == "" + MessageFactory.audio)) {
                intent.type = "audio/*"
            } else if ((mSelectedType == "" + MessageFactory.document)) {
                intent.type = "application/*"
            }
        } else intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_STREAM, tempUri)
        intent.putExtra(Intent.EXTRA_TEXT, "")
        var mIntent: Intent? = null
        mIntent = generateCustomChooserIntent(intent, blacklist)
        if (mIntent != null) {
            startActivity(mIntent)
        }
        noNeedRefresh = true
        //     startActivity(generateCustomChooserIntent(intent, blacklist));
    }

    private fun generateCustomChooserIntent(
        prototype: Intent,
        forbiddenChoices: Array<String>
    ): Intent {
        val targetedShareIntents: MutableList<Intent> = ArrayList()
        val intentMetaInfo: MutableList<HashMap<String, String>> = ArrayList()
        val chooserIntent: Intent
        val dummy = Intent(prototype.action)
        dummy.type = prototype.type
        val resInfo = packageManager.queryIntentActivities(dummy, 0)
        if (!resInfo.isEmpty()) {
            for (resolveInfo: ResolveInfo in resInfo) {
                if (resolveInfo.activityInfo == null || Arrays.asList(*forbiddenChoices)
                        .contains(resolveInfo.activityInfo.packageName)
                ) continue
                val info = HashMap<String, String>()
                info["packageName"] = resolveInfo.activityInfo.packageName
                info["className"] = resolveInfo.activityInfo.name
                info["simpleName"] = resolveInfo.activityInfo.loadLabel(packageManager).toString()
                intentMetaInfo.add(info)
            }
            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, object : Comparator<HashMap<String, String>> {

                    override fun compare(
                        p0: HashMap<String, String>,
                        p1: HashMap<String, String>
                    ): Int {
                        return p0["simpleName"]!!.compareTo((p1["simpleName"])!!)
                    }
                })

                // create the custom intent list
                for (metaInfo: HashMap<String, String> in intentMetaInfo) {
                    val targetedShareIntent = prototype.clone() as Intent
                    targetedShareIntent.setPackage(metaInfo["packageName"])
                    targetedShareIntent.setClassName(
                        (metaInfo["packageName"])!!,
                        (metaInfo["className"])!!
                    )
                    targetedShareIntents.add(targetedShareIntent)
                }
                chooserIntent = Intent.createChooser(
                    targetedShareIntents.removeAt(targetedShareIntents.size - 1),
                    "share"
                )
                chooserIntent.putExtra(
                    Intent.EXTRA_INITIAL_INTENTS,
                    arrayOf(targetedShareIntents)
                )
                return chooserIntent
            }
        }
        return Intent.createChooser(prototype, "Share it")
    }

    private fun DeleteMessageClick() {
        Search1!!.setText("")
        if (Delete_Type.equals("delete received message", ignoreCase = true)) {
            val dialog = CustomAlertDialog()
            dialog.setMessage(getString(R.string.sure_you_want_delete_msg))
            dialog.setNegativeButtonText(getString(R.string.no))
            dialog.setPositiveButtonText(getString(R.string.yes))
            dialog.isCancelable = false
            dialog.setCustomDialogCloseListener(object :
                CustomAlertDialog.OnCustomDialogCloseListener {
                override fun onPositiveButtonClick() {
                    deleteMsgs()
                }

                override fun onNegativeButtonClick() {
                    dialog.dismiss()
                }
            })
            dialog.show(supportFragmentManager, "Delete member alert")
        } else if (Delete_Type.equals("delete sent message", ignoreCase = true)) {
            val dialog = CustomDeleteDialog()
            dialog.setMessage(resources.getString(R.string.sure_you_want_delete_msg))
            dialog.setForMeButtonText(resources.getString(R.string.delete_forme))
            dialog.setEveryOneButtonText(resources.getString(R.string.delete_everyone))
            dialog.setCancelButtonText(resources.getString(R.string.cancel))
            dialog.isCancelable = false
            dialog.setDeleteDialogCloseListener(object :
                CustomDeleteDialog.OnDeleteDialogCloseListener {
                override fun onForMeButtonClick() {
                    deleteMsgs()
                }

                override fun onEveryOneButtonClick() {
                    for (i in selectedChatItems!!.indices) {
                        val msgItem = selectedChatItems!![i]
                        if (msgItem!!.recordId != null && !msgItem.recordId.equals(
                                "",
                                ignoreCase = true
                            )
                        ) {
                            if (msgItem.isMediaPlaying) {
                                val chatIndex = mChatData!!.indexOf(msgItem)
                                mAdapter!!.stopAudioOnMessageDelete(chatIndex)
                            }
                            var lastMsgStatus: String
                            if (mChatData!!.size == 1) {
                                lastMsgStatus = "1"
                            } else {
                                lastMsgStatus = "0"
                            }
                            var docId = from + "-" + to
                            if (isGroupChat) {
                                docId = "$docId-g"
                                mConvId = mGroupId
                            } else {
                                if (mConvId == null) {
                                    mConvId = msgItem.convId
                                }
                            }
                            //     SendMessageEvent messageEvent = new SendMessageEvent();
                            //      messageEvent.setEventName(SocketManager.EVENT_REMOVE_MESSAGE);
                            val type = msgItem.messageType
                            if ((type.equals("1", ignoreCase = true) || type.equals(
                                    "2",
                                    ignoreCase = true
                                )
                                        || type.equals("3", ignoreCase = true) || type.equals(
                                    "6",
                                    ignoreCase = true
                                ))
                            ) {
                                try {
                                    messageDatabase!!.DeleteFileStatusUpdate(
                                        msgItem.messageId,
                                        "delete"
                                    )
                                } catch (e: Exception) {
                                }
                            }
                            try {
                                val deleteMsgObj = JSONObject()
                                deleteMsgObj.put("from", from)
                                deleteMsgObj.put("type", chatType)
                                deleteMsgObj.put("convId", mConvId)
                                deleteMsgObj.put("status", "1")
                                deleteMsgObj.put("recordId", msgItem.recordId)
                                deleteMsgObj.put("last_msg", lastMsgStatus)
                                //  messageEvent.setMessageObject(deleteMsgObj);
                                // EventBus.getDefault().post(messageEvent);
                                val index = mChatData!!.indexOf(selectedChatItems!![i])

                                //--------------Delete Chat--------------
                                val Deletemessage = SendMessageEvent()
                                Deletemessage.eventName = SocketManager.EVENT_DELETE_MESSAGE
                                try {
                                    val deleteMsg = JSONObject()
                                    deleteMsg.put("from", from)
                                    deleteMsg.put("recordId", msgItem.recordId)
                                    deleteMsg.put("convId", mConvId)
                                    if (isGroupChat) {
                                        deleteMsg.put("type", "group")
                                    } else {
                                        deleteMsg.put("type", "single")
                                    }
                                    Deletemessage.messageObject = deleteMsg
                                    EventBus.getDefault().post(Deletemessage)
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        } else {
                            DeleteSenderSide(msgItem, i)
                        }
                    }
                    showUnSelectedActions()
                }

                override fun onCancelButtonClick() {
                    dialog.dismiss()
                }
            })
            dialog.show(supportFragmentManager, "Delete member alert")
        }

        //view.setSelected(false);
    }

    private fun deleteMsgs() {
        for (i in selectedChatItems!!.indices) {
            val msgItem = selectedChatItems!![i]
            if (msgItem!!.isMediaPlaying) {
                val chatIndex = mChatData!!.indexOf(msgItem)
                mAdapter!!.stopAudioOnMessageDelete(chatIndex)
            }
            var lastMsgStatus: String
            if (mChatData!!.size == 1) {
                lastMsgStatus = "1"
            } else {
                lastMsgStatus = "0"
            }
            var docId = from + "-" + to
            if (isGroupChat) {
                docId = "$docId-g"
                mConvId = mGroupId
            } else {
                if (mConvId == null) {
                    mConvId = msgItem.convId
                }
            }
            val messageEvent = SendMessageEvent()
            messageEvent.eventName = SocketManager.EVENT_REMOVE_MESSAGE
            try {
                val deleteMsgObj = JSONObject()
                deleteMsgObj.put("from", from)
                deleteMsgObj.put("type", chatType)
                deleteMsgObj.put("convId", mConvId)
                deleteMsgObj.put("status", "1")
                deleteMsgObj.put("recordId", msgItem.recordId)
                deleteMsgObj.put("last_msg", lastMsgStatus)
                messageEvent.messageObject = deleteMsgObj
                EventBus.getDefault().post(messageEvent)
                messageDatabase!!.deleteChatMessage(docId, msgItem.messageId, chatType)
                messageDatabase!!.updateTempDeletedMessage(msgItem.recordId, deleteMsgObj)
                val index = mChatData!!.indexOf(selectedChatItems!![i])
                if (index > -1) {
                    mChatData!!.removeAt(index)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        Log.e(TAG, "deleteMsgs: notifyDataSetChanged")
        mAdapter!!.notifyDataSetChanged()
        showUnSelectedActions()
    }

    //-----------------Every one delete record id null scenario----------------
    private fun DeleteSenderSide(msgItem: MessageItemChat?, i: Int) {
        if (msgItem!!.isMediaPlaying) {
            val chatIndex = mChatData!!.indexOf(msgItem)
            mAdapter!!.stopAudioOnMessageDelete(chatIndex)
        }
        val lastMsgStatus: String
        if (mChatData!!.size == 1) {
            lastMsgStatus = "1"
        } else {
            lastMsgStatus = "0"
        }
        var docId = from + "-" + to
        if (isGroupChat) {
            docId = "$docId-g"
            mConvId = mGroupId
        } else {
            if (mConvId == null) {
                mConvId = msgItem.convId
            }
        }
        val type = msgItem.messageType
        if ((type.equals("1", ignoreCase = true) || type.equals("2", ignoreCase = true)
                    || type.equals("3", ignoreCase = true) || type.equals("6", ignoreCase = true))
        ) {
            try {
                messageDatabase!!.DeleteFileStatusUpdate(msgItem.messageId, "delete")
            } catch (e: Exception) {
            }
        }
        val messageEvent = SendMessageEvent()
        messageEvent.eventName = SocketManager.EVENT_REMOVE_MESSAGE
        try {
            val deleteMsgObj = JSONObject()
            deleteMsgObj.put("from", from)
            deleteMsgObj.put("type", chatType)
            deleteMsgObj.put("convId", mConvId)
            deleteMsgObj.put("status", "1")
            deleteMsgObj.put("recordId", msgItem.recordId)
            deleteMsgObj.put("last_msg", lastMsgStatus)
            messageEvent.messageObject = deleteMsgObj
            EventBus.getDefault().post(messageEvent)
            val index = mChatData!!.indexOf(selectedChatItems!![i])
            mChatData!!.get(index)!!.messageType = MessageFactory.DELETE_SELF.toString() + ""
            mChatData!![index]!!.setIsSelf(true)
            if (isGroupChat) {
                val str_ids = msgItem.messageId
                val ids = str_ids.split("-").toTypedArray()
                val groupAndMsgId = ids.get(1) + "-g-" + ids[3]
                Log.e(TAG, "DeleteSenderSide: notifyDataSetChanged")
                mAdapter!!.notifyDataSetChanged()
                messageDatabase!!.deleteSingleMessage(groupAndMsgId, str_ids, chatType, "self")
                messageDatabase!!.deleteChatListPage(groupAndMsgId, str_ids, chatType, "self")
            } else {
                Log.e(TAG, "DeleteSenderSide: notifyDataSetChanged")
                mAdapter!!.notifyDataSetChanged()
                messageDatabase!!.deleteSingleMessage(docId, msgItem.messageId, chatType, "self")
                messageDatabase!!.deleteChatListPage(docId, msgItem.messageId, chatType, "self")
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun StartMessageClick() {
        Search1!!.setText("")
        for (i in selectedChatItems!!.indices) {
            val msgItem = selectedChatItems!![i]

            // Add only un-starNextLine if don't have selected starNextLine messages
            //Check it is downloaded or not
            //  Log.e("msgItem.getDownloadStatus()", "msgItem.getDownloadStatus()" + msgItem.getDownloadStatus());
            // Log.e("msgItem.getMessageType()", "msgItem.getMessageType()" + msgItem.getMessageType());

            //    Log.e("msgItem.getDownloadStatus()", "msgItem.getDownloadStatus()" + msgItem.toString());
            if (is_telpon_chat) {
                if ((msgItem!!.downloadStatus != 0) || (msgItem.messageType.toInt() == 0) || (msgItem.messageType.toInt() == 14)) {
                    var starStatus: String
                    if ((isSelectedWithUnStarMsg)!!) {
                        starStatus = MessageFactory.MESSAGE_STARRED
                    } else {
                        if ((msgItem.starredStatus == MessageFactory.MESSAGE_STARRED)) {
                            starStatus = MessageFactory.MESSAGE_UN_STARRED
                        } else {
                            starStatus = MessageFactory.MESSAGE_STARRED
                        }
                    }
                    val messageEvent = SendMessageEvent()
                    messageEvent.eventName = SocketManager.EVENT_STAR_MESSAGE
                    try {
                        val starMsgObj = JSONObject()
                        starMsgObj.put("from", from)
                        starMsgObj.put("type", chatType)
                        starMsgObj.put("status", starStatus)
                        starMsgObj.put("recordId", msgItem.recordId)
                        starMsgObj.put("doc_id", msgItem.messageId)
                        messageEvent.messageObject = starMsgObj
                        if (isGroupChat) {
                            starMsgObj.put("convId", mGroupId)
                            EventBus.getDefault().post(messageEvent)
                            messageDatabase!!.updateStarredMessage(
                                msgItem.messageId,
                                starStatus,
                                MessageFactory.CHAT_TYPE_GROUP
                            )
                            messageDatabase!!.updateTempStarredMessage(msgItem.recordId, starMsgObj)
                        } else {
                            if (mConvId != null && mConvId != "") {
                                starMsgObj.put("convId", mConvId)
                                EventBus.getDefault().post(messageEvent)
                                messageDatabase!!.updateStarredMessage(
                                    msgItem.messageId,
                                    starStatus,
                                    MessageFactory.CHAT_TYPE_SINGLE
                                )
                                messageDatabase!!.updateTempStarredMessage(
                                    msgItem.recordId,
                                    starMsgObj
                                )
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                    var docId = from + "-" + to
                    if (isGroupChat) {
                        docId = "$docId-g"
                    }
                    val index = mChatData!!.indexOf(selectedChatItems!![i])
                    if (index > -1) {
                        mChatData!!.get(index)!!.starredStatus = starStatus
                        mChatData!!.get(index)!!.isSelected = false
                    }
                }
            } else {
                if ((msgItem!!.downloadStatus != 0) || (msgItem.messageType.toInt() == 0) || (msgItem.messageType.toInt() == 14)) {
                    var starStatus: String
                    if ((isSelectedWithUnStarMsg)!!) {
                        starStatus = MessageFactory.MESSAGE_STARRED
                    } else {
                        if ((msgItem.starredStatus == MessageFactory.MESSAGE_STARRED)) {
                            starStatus = MessageFactory.MESSAGE_UN_STARRED
                        } else {
                            starStatus = MessageFactory.MESSAGE_STARRED
                        }
                    }
                    val messageEvent = SendMessageEvent()
                    messageEvent.eventName = SocketManager.EVENT_STAR_MESSAGE
                    try {
                        val starMsgObj = JSONObject()
                        starMsgObj.put("from", from)
                        starMsgObj.put("type", chatType)
                        starMsgObj.put("status", starStatus)
                        starMsgObj.put("recordId", msgItem.recordId)
                        starMsgObj.put("doc_id", msgItem.messageId)
                        messageEvent.messageObject = starMsgObj
                        if (isGroupChat) {
                            starMsgObj.put("convId", mGroupId)
                            EventBus.getDefault().post(messageEvent)
                            messageDatabase!!.updateStarredMessage(
                                msgItem.messageId,
                                starStatus,
                                MessageFactory.CHAT_TYPE_GROUP
                            )
                            messageDatabase!!.updateTempStarredMessage(msgItem.recordId, starMsgObj)
                        } else {
                            if (mConvId != null && mConvId != "") {
                                starMsgObj.put("convId", mConvId)
                                EventBus.getDefault().post(messageEvent)
                                messageDatabase!!.updateStarredMessage(
                                    msgItem.messageId,
                                    starStatus,
                                    MessageFactory.CHAT_TYPE_SINGLE
                                )
                                messageDatabase!!.updateTempStarredMessage(
                                    msgItem.recordId,
                                    starMsgObj
                                )
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                    var docId = from + "-" + to
                    if (isGroupChat) {
                        docId = "$docId-g"
                    }
                    val index = mChatData!!.indexOf(selectedChatItems!![i])
                    if (index > -1) {
                        mChatData!!.get(index)!!.starredStatus = starStatus
                        mChatData!!.get(index)!!.isSelected = false
                    }
                }
            }
            showUnSelectedActions()
        }
    }

    private fun ReplyMessageClick() {
        Search1!!.setText("")
        if (selectedChatItems!!.size == 1) {
            r1messagetoreplay!!.visibility = View.VISIBLE
            reply = true
            val msgItem = selectedChatItems!![0]
            val type = msgItem!!.messageType.toInt()
            mymsgid = msgItem.recordId
            if (msgItem.textMessage != null) {
                quotedMessage = msgItem.textMessage
            } else if (type == MessageFactory.audio){
                quotedMessage = "Audio"
            }
            replytype = msgItem.messageType
            //session.putposition(postionreplay);
            personimage!!.visibility = View.GONE
            Documentimage!!.visibility = View.GONE
            cameraphoto!!.visibility = View.GONE
            audioimage!!.visibility = View.GONE
            sentimage!!.visibility = View.GONE
            videoimage!!.visibility = View.GONE
            messagesetmedio!!.visibility = View.GONE
            val ivLocation = findViewById<ImageView>(R.id.ivLocation)
            ivLocation.visibility = View.GONE
            if (msgItem.isSelf && !isGroupChat) {
                ReplySender = resources.getString(R.string.you)
                Ifname!!.text = resources.getString(R.string.you)
                if (MessageFactory.picture == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    sentimage!!.visibility = View.VISIBLE
                    cameraphoto!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = resources.getString(R.string.photo)
                    sentimage!!.visibility = View.VISIBLE
                    try {
                        imgpath = msgItem.imagePath
                        imageAsBytes = msgItem.thumbnailData
                        sentimage!!.setImageBitmap(
                            ScimboImageUtils.decodeBitmapFromBase64(
                                imageAsBytes, 50, 50
                            )
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else if (MessageFactory.text == type) {
                    message_old!!.visibility = View.VISIBLE
                    messageold = msgItem.textMessage
                    message_old!!.text = messageold
                    message_old!!.setTextColor(resources.getColor(R.color.title))
                } else if (MessageFactory.web_link == type) {
                    message_old!!.visibility = View.VISIBLE
                    messageold = msgItem.webLinkTitle
                    message_old!!.text = messageold
                } else if (MessageFactory.document == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    Documentimage!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messageold = msgItem.textMessage
                    messagesetmedio!!.text = messageold
                } else if (MessageFactory.video == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    videoimage!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = resources.getString(R.string.video)
                    sentimage!!.visibility = View.VISIBLE
                    try {
                        imageAsBytes = msgItem.thumbnailData.replace("data:image/jpeg;base64,", "")
                        val photo = ConvertToImage(imageAsBytes!!)
                        sentimage!!.setImageBitmap(photo)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else if (MessageFactory.audio == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    audioimage!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = resources.getString(R.string.audio)
                } else if (MessageFactory.contact == type) {
                    contactname = "" + msgItem.contactName
                    messagesetmedio!!.visibility = View.VISIBLE
                    personimage!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = contactname
                } else if (MessageFactory.location == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    ivLocation.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = msgItem.webLinkTitle
                    sentimage!!.visibility = View.VISIBLE
                    locationName = msgItem.webLinkTitle
                    try {
                        imageAsBytes =
                            msgItem.webLinkImgThumb.replace("data:image/jpeg;base64,", "")
                        val photo = ConvertToImage(imageAsBytes!!)
                        sentimage!!.setImageBitmap(photo)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                //  msgItem.setReplySender("you");
            } else if (!msgItem.isSelf && !isGroupChat) {
                if (MessageFactory.picture == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    cameraphoto!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = resources.getString(R.string.photo)
                    sentimage!!.visibility = View.VISIBLE
                    try {
                        imgpath = msgItem.chatFileLocalPath
                        imageAsBytes = msgItem.thumbnailData
                        sentimage!!.setImageBitmap(
                            ScimboImageUtils.decodeBitmapFromBase64(
                                imageAsBytes, 50, 50
                            )
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else if (MessageFactory.text == type) {
                    message_old!!.visibility = View.VISIBLE
                    messageold = msgItem.textMessage
                    message_old!!.setTextColor(resources.getColor(R.color.title))
                    message_old!!.text = messageold
                } else if (MessageFactory.web_link == type) {
                    message_old!!.visibility = View.VISIBLE
                    messageold = msgItem.textMessage
                    message_old!!.text = messageold
                } else if (MessageFactory.document == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    Documentimage!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messageold = msgItem.textMessage
                    messagesetmedio!!.text = messageold
                } else if (MessageFactory.video == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    videoimage!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = resources.getString(R.string.Video)
                    sentimage!!.visibility = View.VISIBLE
                    try {
                        imageAsBytes = msgItem.thumbnailData.replace("data:image/jpeg;base64,", "")
                        val photo = ConvertToImage(imageAsBytes!!)
                        sentimage!!.setImageBitmap(photo)
                        //                Picasso.with(mContext).load(imgPath).into(vh2.imageView);
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else if (MessageFactory.audio == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    audioimage!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = resources.getString(R.string.Audio)
                } else if (MessageFactory.contact == type) {
                    contactname = resources.getString(R.string.contact) + msgItem.contactName
                    messagesetmedio!!.visibility = View.VISIBLE
                    personimage!!.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = contactname
                } else if (MessageFactory.location == type) {
                    messagesetmedio!!.visibility = View.VISIBLE
                    ivLocation.visibility = View.VISIBLE
                    message_old!!.visibility = View.GONE
                    messagesetmedio!!.text = msgItem.webLinkTitle
                    sentimage!!.visibility = View.VISIBLE
                    locationName = msgItem.webLinkTitle
                    try {
                        imageAsBytes =
                            msgItem.webLinkImgThumb.replace("data:image/jpeg;base64,", "")
                        val photo = ConvertToImage(imageAsBytes!!)
                        sentimage!!.setImageBitmap(photo)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                //                Picasso.with(mContext).load(imgPath).into(vh2.imageView);
                ReplySender = mReceiverName
                Ifname!!.text = mReceiverName
            } else if (isGroupChat) {
                if (msgItem.isSelf) {
                    ReplySender = resources.getString(R.string.you)
                    Ifname!!.text = ReplySender
                    if (MessageFactory.picture == type) {
                        messagesetmedio!!.visibility = View.VISIBLE
                        cameraphoto!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.text = resources.getString(R.string.photo)
                        sentimage!!.visibility = View.VISIBLE
                        try {
                            imgpath = msgItem.imagePath
                            /*
                                                sentimage.setImageBitmap(ScimboImageUtils.decodeBitmapFromFile(imgpath, 50, 50));*/imageAsBytes =
                                msgItem.thumbnailData
                            sentimage!!.setImageBitmap(
                                ScimboImageUtils.decodeBitmapFromBase64(
                                    imageAsBytes, 50, 50
                                )
                            )
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else if (MessageFactory.text == type) {
                        message_old!!.visibility = View.VISIBLE
                        messageold = msgItem.textMessage
                        message_old!!.text = messageold
                    } else if (MessageFactory.web_link == type) {
                        message_old!!.visibility = View.VISIBLE
                        messageold = msgItem.textMessage
                        message_old!!.text = messageold
                    } else if (MessageFactory.document == type) {
                        messagesetmedio!!.visibility = View.VISIBLE
                        Documentimage!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messageold = msgItem.textMessage
                        messagesetmedio!!.text = messageold
                    } else if (MessageFactory.video == type) {
                        messagesetmedio!!.visibility = View.VISIBLE
                        videoimage!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.text = resources.getString(R.string.video)
                        sentimage!!.visibility = View.VISIBLE
                        try {
                            imageAsBytes =
                                msgItem.thumbnailData.replace("data:image/jpeg;base64,", "")
                            val photo = ConvertToImage(imageAsBytes!!)
                            sentimage!!.setImageBitmap(photo)
                            //                Picasso.with(mContext).load(imgPath).into(vh2.imageView);
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else if (MessageFactory.audio == type) {
                        messagesetmedio!!.visibility = View.VISIBLE
                        audioimage!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.text = resources.getString(R.string.Audio)
                    } else if (MessageFactory.contact == type) {
                        contactname = resources.getString(R.string.contact) + msgItem.contactName
                        messagesetmedio!!.visibility = View.VISIBLE
                        personimage!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.text = contactname
                    } else if (MessageFactory.location == type) {
                        messagesetmedio!!.visibility = View.VISIBLE
                        ivLocation.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.text = msgItem.webLinkTitle
                        sentimage!!.visibility = View.VISIBLE
                        locationName = msgItem.webLinkTitle
                        try {
                            imageAsBytes =
                                msgItem.webLinkImgThumb.replace("data:image/jpeg;base64,", "")
                            val photo = ConvertToImage(imageAsBytes!!)
                            sentimage!!.setImageBitmap(photo)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                } else {
                    ReplySender =
                        getcontactname!!.getSendername(msgItem.groupMsgFrom, msgItem.senderName)
                    Ifname!!.text = ReplySender
                    if (MessageFactory.picture == type) {
                        cameraphoto!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.visibility = View.VISIBLE
                        messagesetmedio!!.text = resources.getString(R.string.photo)
                        sentimage!!.visibility = View.VISIBLE
                        try {
                            imgpath = msgItem.chatFileLocalPath
                            /*
                                                sentimage.setImageBitmap(ScimboImageUtils.decodeBitmapFromFile(imgpath, 50, 50));*/
//                Picasso.with(mContext).load(imgPath).into(vh2.imageView);
                            imageAsBytes = msgItem.thumbnailData
                            sentimage!!.setImageBitmap(
                                ScimboImageUtils.decodeBitmapFromBase64(
                                    imageAsBytes, 50, 50
                                )
                            )
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else if (MessageFactory.text == type) {
                        message_old!!.visibility = View.VISIBLE
                        messageold = msgItem.textMessage
                        message_old!!.text = messageold
                    } else if (MessageFactory.web_link == type) {
                        message_old!!.visibility = View.VISIBLE
                        messageold = msgItem.textMessage
                        message_old!!.text = messageold
                    } else if (MessageFactory.document == type) {
                        messagesetmedio!!.visibility = View.VISIBLE
                        Documentimage!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messageold = msgItem.textMessage
                    } else if (MessageFactory.video == type) {
                        messagesetmedio!!.visibility = View.VISIBLE
                        videoimage!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.text = resources.getString(R.string.video)
                        sentimage!!.visibility = View.VISIBLE
                        try {
                            imageAsBytes =
                                msgItem.thumbnailData.replace("data:image/jpeg;base64,", "")
                            val photo = ConvertToImage(imageAsBytes!!)
                            sentimage!!.setImageBitmap(photo)
                            //                Picasso.with(mContext).load(imgPath).into(vh2.imageView);
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else if (MessageFactory.audio == type) {
                        audioimage!!.visibility = View.VISIBLE
                        messagesetmedio!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.text = resources.getString(R.string.photo)
                    } else if (MessageFactory.contact == type) {
                        contactname = resources.getString(R.string.contact) + msgItem.contactName
                        messagesetmedio!!.visibility = View.VISIBLE
                        personimage!!.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.text = contactname
                    } else if (MessageFactory.location == type) {
                        messagesetmedio!!.visibility = View.VISIBLE
                        ivLocation.visibility = View.VISIBLE
                        message_old!!.visibility = View.GONE
                        messagesetmedio!!.text = msgItem.webLinkTitle
                        sentimage!!.visibility = View.VISIBLE
                        locationName = msgItem.webLinkTitle
                        try {
                            imageAsBytes =
                                msgItem.webLinkImgThumb.replace("data:image/jpeg;base64,", "")
                            val photo = ConvertToImage(imageAsBytes!!)
                            sentimage!!.setImageBitmap(photo)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            //msgItem.setReplySender(username);
            showUnSelectedActions()
        }
    }

    fun ConvertToImage(image: String): Bitmap {
        val imageAsBytes = Base64.decode(image.toByteArray(), Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.size)
    }

    //------------------------------Chat List Long Item Click Listener--------------------------------------------
    /*   @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {

        boolean selection = mChatData.get(position).isSelected();

        boolean isDeletedMsgItem = performSelection(position);
        if (isEncryptionInfo(position)) {
            return false;
        }
        if (selectedChatItems.size() <= 0) {
            showUnSelectedActions();

        } else {
            isFirstItemSelected = true;
            iBtnBack2.setVisibility(View.GONE);
            Search1.setVisibility(View.GONE);
        }

        MyLog.d(TAG, "@@@@ onItemLongClick: long press");
        longPressMillis = System.currentTimeMillis();
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MessageItemChat messageItemChat = mChatData.get(position);

                if (!isGroupChat) {
                    Intent intent = new Intent(context, SingleMessageInfoActivity.class);
                    intent.putExtra("selectedData", messageItemChat);
                    startActivity(intent);

                } else {

                    Intent groupinfointent = new Intent(context, GroupMessageInfoActivity.class);
                    groupinfointent.putExtra("selectedData", messageItemChat);
                    startActivity(groupinfointent);
                }

                mChatData.get(0).setSelected(false);
                madapter.notifyDataSetChanged();
                showUnSelectedActions();
            }
        });


        return true;
    }
*/
    override fun onItemLongClick(
        adapterView: AdapterView<*>?, view: View, position: Int,
        l: Long
    ): Boolean {
        itemLongClicked(position)
        return true
    }

    /*

    private void showUnSelectedActions() {
        include.setVisibility(View.VISIBLE);
        rlChatActions.setVisibility(View.GONE);
        //   share.setVisibility(View.GONE);
        isFirstItemSelected = false;
        selectedChatItems.clear();

        for (int i = 0; i < mChatData.size(); i++) {
            mChatData.get(i).setSelected(false);
        }
        if (null != madapter)
            madapter.notifyDataSetChanged();
    }
*/
    private fun showUnSelectedActions() {
        include!!.visibility = View.VISIBLE
        rlChatActions!!.visibility = View.GONE
        //   share.setVisibility(View.GONE);
        isFirstItemSelected = false
        if (selectedChatItems != null) selectedChatItems!!.clear()
        for (i in mChatData!!.indices) {
            mChatData!!.get(i)!!.isSelected = false
        }
        if (null != mAdapter) {
            Log.e(TAG, "showUnSelectedActions: notifyDataSetChanged")
            mAdapter!!.notifyDataSetChanged()
        }
    }

    private fun isEncryptionInfo(position: Int): Boolean {
        try {
            val msgItem = mChatData!![position]
            val msgType = msgItem!!.messageType
            if ((msgType == "" + MessageFactory.ENCRYPTION_INFO)) {
                return true
            }
        } catch (e: Exception) {
            MyLog.e(TAG, "isEncryptionInfo: ", e)
        }
        return false
    }

    //------------------------------Perform Click Action---------------------------------------------------------------
    private fun performSelection(position: Int): Boolean {
        if (selectedChatItems == null) selectedChatItems = ArrayList()
        val msgItem = mChatData!![position]
        // MessageItemChat msgItem = (MessageItemChat) madapter.getItem(position);
        val msgType = msgItem!!.messageType
        val SelfType = msgItem.isSelf
        if (msgType.equals(
                MessageFactory.DELETE_SELF.toString() + "",
                ignoreCase = true
            ) || msgType.equals(MessageFactory.DELETE_OTHER.toString() + "", ignoreCase = true)
        ) {
            Delete_Type = "delete received message"
        } else if ((!msgType.equals(
                MessageFactory.DELETE_SELF.toString() + "",
                ignoreCase = true
            ) || !msgType.equals(
                MessageFactory.DELETE_OTHER.toString() + "",
                ignoreCase = true
            )) && !SelfType
        ) {
            Delete_Type = "delete received message"
        } else {
            Delete_Type = "delete sent message"
        }
        if (!msgItem.isInfoMsg && msgItem.messageType != MessageFactory.missed_call.toString() + "") {
            mypath = msgItem.chatFileLocalPath
            //int index = mChatData.indexOf(msgItem);
            if ((msgItem.messageType == MessageFactory.DELETE_SELF.toString() + "") || (msgItem.messageType == MessageFactory.DELETE_OTHER.toString() + "")) {
                return true
            }
            if (selectedChatItems!!.size > 0) {
                if (selectedChatItems!!.contains(msgItem)) {
                    selectedChatItems!!.remove(msgItem)
                    mChatData!!.get(position)!!.isSelected = false
                } else {
                    selectedChatItems!!.add(msgItem)
                    mChatData!!.get(position)!!.isSelected = true
                }
            } else {
                mChatData!!.get(position)!!.isSelected = true
                selectedChatItems!!.add(msgItem)
            }
            for (chat: MessageItemChat? in selectedChatItems!!) {
                if (((chat!!.messageType == MessageFactory.text.toString() + "") || ((chat.messageType == MessageFactory.contact.toString() + ""))
                            || ((chat.messageType == MessageFactory.web_link.toString() + ""))
                            || ((chat.messageType == MessageFactory.location.toString() + "")))
                ) {
                    forward!!.visibility = View.VISIBLE
                } else {
                    if (chat.isSelf) {
                        if (chat.uploadStatus == MessageFactory.UPLOAD_STATUS_COMPLETED) {
                            forward!!.visibility = View.VISIBLE
                        } else {
                            forward!!.visibility = View.GONE
                            break
                        }
                    } else {
                        if (chat.downloadStatus == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
                            forward!!.visibility = View.VISIBLE
                        } else {
                            forward!!.visibility = View.GONE
                            break
                        }
                    }
                }
            }
            // Add copy action only if all selected messages as text
            var allTextMsg = true
            var isDeletedMsg = false
            for (selectedItem: MessageItemChat? in selectedChatItems!!) {
                if ((selectedItem!!.messageType == MessageFactory.DELETE_SELF.toString() + "") || (selectedItem.messageType == MessageFactory.DELETE_OTHER.toString() + "")) {
                    isDeletedMsg = true
                    break
                }
            }
            for (selectedItem: MessageItemChat? in selectedChatItems!!) {
                if ((selectedItem!!.messageType != MessageFactory.text.toString()
                            && selectedItem.messageType != MessageFactory.web_link.toString())
                ) {
                    allTextMsg = false
                    break
                }
            }
            //            if (allTextMsg) {
//                copychat.setVisibility(View.VISIBLE);
//                showOriginal.setVisibility(View.VISIBLE);
//            } else {
//                showOriginal.setVisibility(View.GONE);
//                copychat.setVisibility(View.GONE);
//
//            }
            if (selectedChatItems!!.size > 0) {
                isFirstItemSelected = true
                if (selectedChatItems!!.size == 1) {
                    replymess!!.visibility = View.VISIBLE
                    if (selectedChatItems!![0]!!
                            .isSelf && !isDeletedMsg
                    ) {
                        info!!.visibility = View.VISIBLE
                    } else {
                        info!!.visibility = View.GONE
                    }
                    val msgTypee = selectedChatItems!![0]!!
                        .messageType
                    val type = msgTypee.toInt()
                    var canShowShareImage = false
                    if (selectedChatItems!![0]!!
                            .isSelf
                    ) {
                        if ((type == MessageFactory.audio) || (
                                    type == MessageFactory.document) || (
                                    type == MessageFactory.video) || (
                                    type == MessageFactory.picture)
                        ) {
                            canShowShareImage = true
                        }
                    } else {
                        if (selectedChatItems!![0]!!
                                .downloadStatus == MessageFactory.DOWNLOAD_STATUS_COMPLETED
                        ) {
                            if ((type == MessageFactory.audio) || (
                                        type == MessageFactory.document) || (
                                        type == MessageFactory.video) || (
                                        type == MessageFactory.picture)
                            ) {
                                canShowShareImage = true
                            }
                        }
                    }
                    if (canShowShareImage) {
                        mSelectedPath = selectedChatItems!![0]!!
                            .chatFileLocalPath
                        mSelectedType = selectedChatItems!![0]!!
                            .messageType
                        share!!.visibility = View.GONE
                    } else {
                        share!!.visibility = View.GONE
                    }
                } else {
                    replymess!!.visibility = View.GONE
                    share!!.visibility = View.GONE
                    reply = false
                    info!!.visibility = View.GONE
                    //Check whether the
                }
                isSelectedWithUnStarMsg = false
                for (selectedItem: MessageItemChat? in selectedChatItems!!) {
                    if ((selectedItem!!.starredStatus == MessageFactory.MESSAGE_UN_STARRED)) {
                        isSelectedWithUnStarMsg = true
                        break
                    }
                }
                if (isSelectedWithUnStarMsg!!) {
                    starred!!.setImageResource(R.drawable.ic_starred)
                } else {
                    starred!!.setImageResource(R.drawable.ic_unstarred)
                }
                if (isDeletedMsg) {
                    showDeleteActions()
                } else {
                    showBaseActions(allTextMsg)
                }
            }
            mAdapter!!.setfirstItemSelected(isFirstItemSelected)
            Log.e(TAG, "performSelection: notifyDataSetChanged")
            mAdapter!!.notifyDataSetChanged()
        }
        return false
    }

    private fun showDeleteActions() {
        include!!.visibility = View.GONE
        rlChatActions!!.visibility = View.VISIBLE
        starred!!.visibility = View.GONE
        forward!!.visibility = View.GONE
        replymess!!.visibility = View.GONE
        copychat!!.visibility = View.GONE

        //  share.setVisibility(View.GONE);
        info!!.visibility = View.GONE
        delete!!.visibility = View.VISIBLE
        longpressback!!.visibility = View.VISIBLE
    }

    private fun showBaseActions(allTextMsgs : Boolean) {
        include!!.visibility = View.GONE
        rlChatActions!!.visibility = View.VISIBLE
        starred!!.visibility = View.VISIBLE
        delete!!.visibility = View.VISIBLE
        longpressback!!.visibility = View.VISIBLE
        if (allTextMsgs)
            copychat!!.visibility = View.VISIBLE
        else
            copychat!!.visibility = View.GONE
    }

    fun showMenuBelowLollipop() {
        val cx = (attachmentLayout!!.left + attachmentLayout!!.right)
        val cy = attachmentLayout!!.top
        val radius = Math.max(attachmentLayout!!.width, attachmentLayout!!.height)
        try {
            val animator = ViewAnimationUtils.createCircularReveal(
                attachmentLayout,
                cx,
                cy,
                0f,
                radius.toFloat()
            )
            animator.setInterpolator(AccelerateDecelerateInterpolator())
            animator.setDuration(300)
            if (isHidden) {
                //MyLog.e(getClass().getSimpleName(), "showMenuBelowLollipop");
                attachmentLayout!!.visibility = View.VISIBLE
                animator.start()
                isHidden = false
            } else {
                val animatorReverse = animator.reverse()
                animatorReverse.start()
                animatorReverse.addListener(object : SupportAnimator.AnimatorListener {
                    override fun onAnimationStart() {}
                    override fun onAnimationEnd() {
                        //MyLog.e("MainActivity", "onAnimationEnd");
                        isHidden = true
                        attachmentLayout!!.visibility = View.INVISIBLE
                    }

                    override fun onAnimationCancel() {}
                    override fun onAnimationRepeat() {}
                })
            }
        } catch (e: Exception) {
            //MyLog.e(getClass().getSimpleName(), "try catch");
            isHidden = true
            attachmentLayout!!.visibility = View.INVISIBLE
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun showMenu() {
        val cx = (attachmentLayout!!.left + attachmentLayout!!.right)
        val cy = attachmentLayout!!.top
        val radius = Math.max(attachmentLayout!!.width, attachmentLayout!!.height)
        if (isHidden) {
            val anim = android.view.ViewAnimationUtils.createCircularReveal(
                attachmentLayout,
                cx,
                cy,
                0f,
                radius.toFloat()
            )
            attachmentLayout!!.visibility = View.VISIBLE
            anim.start()
            isHidden = false
        } else {
            val anim = android.view.ViewAnimationUtils.createCircularReveal(
                attachmentLayout,
                cx,
                cy,
                radius.toFloat(),
                0f
            )
            anim.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    attachmentLayout!!.visibility = View.INVISIBLE
                    isHidden = true
                }
            })
            anim.start()
        }
    }

    private fun hideMenu() {
        attachmentLayout!!.visibility = View.GONE
        isHidden = true
    }

    private fun getintent() {
        if (intent.data != null) {
            val cursor = managedQuery(intent.data, null, null, null, null)
            if (cursor.moveToNext()) {
                receiverUid = cursor.getString(cursor.getColumnIndex("DATA1"))
                to = cursor.getString(cursor.getColumnIndex("DATA1"))
                receiverMsisdn = cursor.getString(cursor.getColumnIndex("DATA2"))
                backfrom = true
                mReceiverName = getcontactname!!.getSendername(receiverUid, receiverMsisdn)
                mReceiverId = to
                friendId = to
            }
        } else {
            val bundle = intent.extras
            if (bundle != null) {
                backfrom = bundle.getBoolean("backfrom")
                receiverUid = bundle.getString("receiverUid")
                msgid = bundle.getString("msgid", "")
                receiverName = bundle.getString("receiverName")
                friendId = bundle.getString("friendId")
                to = bundle.getString("documentId")
                starred_msgid = bundle.getString("starred", "")
                mReceiverName = bundle.getString("Username")
                receiverAvatar = bundle.getString("Image")
                receiverMsisdn = bundle.getString("msisdn")
                isFrom = bundle.getString("isFrom", "");
                val hasMessage = bundle.containsKey("message")

                if (hasMessage) {
                    val uri = bundle.getString("message")
                    if (uri!!.contains("&&&ABC&&&123")) {
                        val array = uri.split("&&&ABC&&&").toTypedArray()
                        sendMessage!!.setText(array[0])
                    } else {
                        imagepath_caption = Imagepath_caption()
                        imagepath_caption!!.setPath(uri)
                        imagepath_caption!!.setCaption("")
                        pathList!!.add(imagepath_caption!!)
                        val gson = Gson()
                        val data = gson.toJson(pathList)
                        val i = Intent(
                            this@ChatPageActivity,
                            ImagecaptionActivity::class.java
                        )
                        i.putExtra("phoneno", mReceiverName)
                        i.putExtra("data", data)
                        i.putExtra("from", "direct")
                        startActivityForResult(i, SHARE_INTENT)
                    }
                    //                    try {
//                        chatType = MessageFactory.CHAT_TYPE_SINGLE;
//                        //ArrayList<Imagepath_caption> pathlist = (ArrayList<Imagepath_caption>) bundle.getSerializable("pathlist");
//                        for (int i = 0; i < pathList.size(); i++) {
//                            String path = pathList.get(i).getPath();
//                            String caption = pathList.get(i).getCaption();
//
//                            if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
//                                DisplayAlert(getResources().getString(R.string.unblock) + " " + mReceiverName + " " + getResources().getString(R.string.to_send_message));
//                            } else {
//                                sendImageChatMessage(path, caption, false);
//                            }
//                        }
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                    sendData();
                }
                model = bundle.getString("model")
                mReceiverId = to
            }
        }
        Chat_to = mReceiverId
        try {
            getcontactname!!.configCircleProfilepic(
                user_profile_image,
                to,
                true,
                true,
                R.drawable.ic_placeholder_black
            )
        } catch (e: Exception) {
        }

    }

    private fun sendData() {
        val bundle = Bundle()
        bundle.putSerializable("pathlist", pathList)
        val okIntent = Intent()
        okIntent.putExtras(bundle)
        setResult(Activity.RESULT_OK, okIntent)
        finish()
    }

    private fun updateName() {
        if (mReceiverName == null || mReceiverName.equals("", ignoreCase = true)) {
            mReceiverName = getcontactname!!.getSendername(receiverUid, receiverMsisdn)
        }
        if (mReceiverName != null && !mReceiverName.equals("", ignoreCase = true)) {
            user_name!!.text = mReceiverName
            SharedPreference.getInstance().save(context, "userName", mReceiverName)
        } else {
            SharedPreference.getInstance().save(context, "userName", mReceiverName)
            user_name!!.text = receiverMsisdn
        }
        if (ScimboContactsService.savedName != null && !ScimboContactsService.savedName.isEmpty()) {
            user_name!!.text = ScimboContactsService.savedName
            SharedPreference.getInstance()
                .save(context, "userName", ScimboContactsService.savedName)
        }
    }

    private fun initAdapter() {
        mAdapter = MessageAdapter(false, this, mChatData, this.supportFragmentManager, this)
        mAdapter?.setIsGroupChat(isGroupChat)
        chat_list?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        chat_list?.itemAnimator = null
        chat_list?.adapter = mAdapter
        Log.e(TAG, "initAdapter" + "chat_list.setSelection")
        chat_list?.layoutManager?.scrollToPosition(mChatData!!.size - 1)
    }

    private suspend fun loadFromDB() {
        docId = from + "-" + to + "-g"
        var items = ArrayList<MessageItemChat?>()
        var badge_count_id = ""
        if (messageDatabase != null) {
            if (messageDatabase!!.isGroupId(docId)) {
                call_layout!!.visibility = View.GONE
                isGroupChat = true
                mGroupId = to
                mConvId = to
                Activity_GroupId = mGroupId
                chatType = MessageFactory.CHAT_TYPE_GROUP
                badge_count_id = from + "-" + to + "-g"
                items = ArrayList()
                items.addAll(
                    messageDatabase!!.selectAllMessagesWithLimit(
                        docId, chatType,
                        "", MessageDbController.MESSAGE_SELECTION_LIMIT_FIRST_TIME
                    )
                )
                Collections.sort(items, msgComparator)
                mChatData!!.clear()
                mChatData!!.addAll(items)
                isGroupChat = true
                hasGroupInfo = groupInfoSession!!.hasGroupInfo(docId)
                setGroupMemmbersName()
            } else {
                call_layout!!.visibility = View.VISIBLE
                isGroupChat = false
                chatType = MessageFactory.CHAT_TYPE_SINGLE
                docId = from + "-" + to
                badge_count_id = from + "-" + to
                val encryptionMsgInfo = messageDatabase!!.getFirstMsgTimeStamp(chatType, docId)
                items = ArrayList()
                items.addAll(
                    messageDatabase!!.selectAllMessagesWithLimit(
                        docId,
                        chatType,
                        "",
                        MessageDbController.MESSAGE_SELECTION_LIMIT_FIRST_TIME
                    )
                )
                if (items.size > 0) {
                    try {
                        Collections.sort(items, msgComparator)
                    } catch (e: Exception) {
                        Log.e(TAG, "loadDbAsync: ", e)
                    }
                    val firstItemTs = AppUtils.parseLong(items[0]!!.ts)
                    encryptionMsgInfo.ts = "" + (firstItemTs - 10L)
                }
                // items.add(encryptionMsgInfo);
                Collections.sort(items, msgComparator)
                if (items.size > 0) {
                    mFirstVisibleMsgId = items[0]!!.messageId
                }
                if (userInfoSession!!.hasChatConvId(docId)) {
                    mConvId = userInfoSession!!.getChatConvId(docId)
                }
                mChatData!!.clear()
                mChatData!!.addAll(items)
                MyLog.d(TAG, "loadFromDB: finished")
            }
        }
        changeBadgeCount(badge_count_id)
        sendAllAcksToServer(items)
        if (isFirstTimeLoad) {
            isFirstTimeLoad = false
            runOnUiThread { profilepicupdation() }
        }
        val finalItems = items
        handler.post {
            if (isGroupChat) {
                CheckGroupExitUser(finalItems)
            } else {
                group_left_layout!!.visibility = View.GONE
                relativeLayout!!.visibility = View.VISIBLE
                rlSend!!.visibility = View.VISIBLE
            }
        }
        withContext(Dispatchers.Main) {
            initAdapter()
            setEncryptionMsg()
        }

        //getReceiverOnlineTimeStatus();
        //notifyDatasetChange();
        initDataBase()
    }

    private fun setLastItemSelection() {
        chat_list!!.post {
            if (mChatData!!.size > 0) {
                chat_list!!.layoutManager!!.scrollToPosition(mChatData!!.size - 1)
                iBtnScroll!!.visibility = View.GONE
                unreadcount!!.visibility = View.GONE
                mListviewScrollBottom = false
            }
            //                    chat_list.setSelection(mChatData.size() - 1);
        }
    }

    //--------------------------Group Exit CurrentUser Check-------------------------
    private fun CheckGroupExitUser(items: ArrayList<MessageItemChat?>) {
        var createdBy = ""
        var createdTo = ""
        for (i in items.indices) {
            val message = items[i]
            if (message!!.createdByUserId != null) {
                createdBy = message.createdByUserId
            }
            if (message.createdToUserId != null) {
                createdTo = message.createdToUserId
            }
            if (message.groupEventType != null) {
                when (message.groupEventType) {
                    "" + MessageFactory.exit_group -> if (createdBy.equals(
                            mCurrentUserId,
                            ignoreCase = true
                        )
                    ) {
                        relativeLayout!!.visibility = View.INVISIBLE
                        rlSend!!.visibility = View.INVISIBLE
                        group_left_layout!!.visibility = View.VISIBLE
                    } else {
                        // createdByName = getContactNameIfExists(createdBy);
                    }
                    "" + MessageFactory.delete_member_by_admin -> if (createdTo.equals(
                            mCurrentUserId,
                            ignoreCase = true
                        )
                    ) {
                        relativeLayout!!.visibility = View.GONE
                        rlSend!!.visibility = View.GONE
                        group_left_layout!!.visibility = View.VISIBLE
                    }
                    "" + MessageFactory.add_group_member -> if (createdTo.equals(
                            mCurrentUserId,
                            ignoreCase = true
                        )
                    ) {
                        relativeLayout!!.visibility = View.VISIBLE
                        rlSend!!.visibility = View.VISIBLE
                        group_left_layout!!.visibility = View.GONE
                    }
                }
            }
        }
    }

    private suspend fun initDataBase() {
        if (starred_msgid != null && starred_msgid != "" && noNeedRefresh) {
            Starredmsg_test()
        }
        layout_new!!.visibility = View.GONE
        if (!isGroupChat) {
            if (!getcontactname!!.isContactExists(mReceiverId) && (!sessionManager!!.isFirstMessage(
                    to + "firstmsg"
                ))
            ) {
                //layout_new.setVisibility(View.VISIBLE);
                if (mContactSaved) {
                    llAddBlockContact!!.visibility = View.GONE
                } else {
                    llAddBlockContact!!.visibility = View.VISIBLE
                }
                //          llAddBlockContact.setVisibility(View.VISIBLE);
            } else if (!getcontactname!!.isContactExists(mReceiverId)) {
                llAddBlockContact!!.visibility = View.VISIBLE
                setTopLayoutBlockText()
            }
            val blockStatus = contactDB_sqlite!!.getBlockedStatus(to, false)
            if ((blockStatus == "1")) {
                block_contact!!.text = getString(R.string.unblock)
            } else {
                block_contact!!.text = getString(R.string.block_)
            }
        }
        getgroupmemberdetails()
    }

    //----------------------------------Select the Star Position Chat Message----------------------------------
    private suspend fun Starredmsg_test() {
        if (starred_msgid != null && starred_msgid != "") {
            val message = messageDatabase!!.getParticularMessage(starred_msgid)
            val items = messageDatabase!!.selectAllMessagesWithLimitUponScroll(
                docId,
                message.ts, MessageDbController.MESSAGE_SELECTION_LIMIT_FIRST_TIME
            )
            items.add(message)
            Collections.sort(items, msgComparator)
            val starredMsg = message.textMessage
            MyLog.d(TAG, "Starredmsg_test: $starredMsg")
            mChatData!!.clear()
            mChatData!!.addAll(items)

            val index = mChatData!!.indexOf(message)
            withContext(Dispatchers.Main) {
                chat_list?.recycledViewPool?.clear();
                mAdapter?.notifyDataSetChanged()
                if (index != -1) {
                    chat_list?.scrollToPosition(index)
                }
            }
            Log.e(TAG, "Starredmsg_test: notifyDataSetChanged")

            mChatData!!.get(index)!!.isSelected = true
            val handler = Handler(Looper.getMainLooper())
            //final int finalStarredpostion = starredpostion;
            handler.postDelayed({
                val newIndex = mChatData!!.indexOf(message)
                if (newIndex != -1) {
                    mChatData!!.get(newIndex)!!.isSelected = false
                    Log.e(TAG, "Starredmsg_test: notifyDataSetChanged")
                    mAdapter!!.notifyItemChanged(index)
                }
            }, 2500)
        };
        setEncryptionMsg()
    }

    //------------------------------------------------------------Group Member Details Get----------------------------------------
    private fun getgroupmemberdetails() {
        try {
            val docId = mCurrentUserId + "-" + to + "-g"
            hasGroupInfo = groupInfoSession!!.hasGroupInfo(docId)
        } catch (e: Exception) {
            Log.e(TAG, "getgroupmemberdetails: ", e)
        }
        if (hasGroupInfo) {
            val infoPojo = groupInfoSession!!.getGroupInfo(docId)
            if (infoPojo != null && infoPojo.groupMembers != null) {
                val membersId = infoPojo.groupMembers.split(",").toTypedArray()
                for (i in membersId.indices) {
                    getUserDetails(membersId[i])
                }
            } else {
                Toast.makeText(
                    this,
                    resources.getString(R.string.you_are_not_member_of_this_group),
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        }
    }

    override fun onStart() {
        //overridePendingTransition(0, 0);
        super.onStart()
        EventBus.getDefault().register(this)
        val intentFilter = IntentFilter(SocketManager.EVENT_MESSAGE)
        registerReceiver(newMessageListener, intentFilter)
    }

    override fun onResume() {
        super.onResume()
        // ScreenShotDetector.setListener(this);

        // This delay was added to handle a race condition where the value for isChatPage is set to
        // false by the previous instance of chat screen (as it gets destroyed) when user opens a
        // chat from the notification while sitting in a different chat screen
        Handler().postDelayed(Runnable() {
            isChatPage = true
            Log.d("Taha", "onResume: isChatPage " + isChatPage + "from instance" + this)
            isKilled = false
        }, 1000)

        //sendAllAcksToServer(mChatData);
        setTranslateIcon()
        if (!noNeedRefresh) {
            val isLockChat = intent.getBooleanExtra("isLockChat", false)
            if (!isLockChat) {
                lifecycleScope.launch {
                    withContext(Dispatchers.IO) {
                        loadFromDB()
                    }
                }
            }
        }
        noNeedRefresh = false
        /*if (!isFirstTimeLoad) {
            isFirstTimeLoad = false;
        loadFromDB();
        }*/if (isGroupChat) {
            val infoPojo = groupInfoSession!!.getGroupInfo(from + "-" + to + "-g")
            if (infoPojo != null) {
                enableGroupChat = infoPojo.isLiveGroup
            }
            info!!.visibility = View.VISIBLE
        } else info!!.visibility = View.GONE
        session!!.puttoid(docId)
        NotificationUtil.clearNotificationData()
        Handler().postDelayed({
            val notifManager =
                application.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notifManager.cancelAll()
            if (!isFirstTimeLoad) profilepicupdation()
            try {
                ShortcutBadger.removeCountOrThrow(this@ChatPageActivity) //for 1.1.4+
            } catch (e: Exception) {
                //  e.printStackTrace();
            }
        }, 300)
        wallpaperdisplay()

        //----------Delete Chat---------------
        /*if (isGroupChat) {
            sendGroupOffline();
        } else {
            sendSingleOffline();
        }
        sendNormalOffline();*/updateName()
        checkcallstatus()
        MyLog.d(TAG, "onResume end: chatmove")
        //        if (null != sendMessage && sendMessage.getVisibility() == View.GONE)
//            sendMessage.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    sendMessage.setVisibility(View.VISIBLE);
//                }
//            }, 500);
    }

    fun checkcallstatus() {
        val mCallOngoing =
            SharedPreference.getInstance().getBool(CoreController.mcontext, "callongoing")
        if (mCallOngoing) {
            //Register you receiver
            registerCallReceiver()
            if (mLnrcallStatus!!.visibility == View.GONE) {
                mLnrcallStatus!!.visibility = View.VISIBLE
            }
        } else {
            if (mLnrcallStatus!!.visibility == View.VISIBLE) {
                mLnrcallStatus!!.visibility = View.GONE
            }
        }
    }

    private fun registerCallReceiver() {
        //Register BroadcastReceiver
        //to receive event from our service
        if (myReceiver == null) {
            myReceiver = MyReceiver()
            val intentFilter = IntentFilter()
            intentFilter.addAction(MessageService.SENDMESAGGE)
            registerReceiver(myReceiver, intentFilter)
        }
    }

    override fun onPause() {
        super.onPause()
        mAdapter!!.pauseAudioPlay(-1)
        //overridePendingTransition(0, 0);
//        session.puttoid("");
        //       isChatPage = false;
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mChatData!!.size > 0) {
            for (i in mChatData!!.indices) {
                if (i > -1 && mChatData!![i]!!.isMediaPlaying) {
                    mChatData!![MessageAdapter.lastPlayedAt]!!.setIsMediaPlaying(false)
                    mAdapter!!.stopAudio()
                    mAdapter!!.notifyDataSetChanged()
                }
            }
        }
        isChatPage = false
        Log.d("Taha", "onDestroy: isChatPage " + isChatPage + "from instance" + this)
        EventBus.getDefault().unregister(this)
        if (receiver != null) {
            unregisterReceiver(receiver)
        }
        clearAllViews()
    }

    override fun onStop() {
        super.onStop()
        if (isAudioRecording) {
            isAudioRecording = false
            stopRecording(false)
        }
        isChatPage = false
        Log.d("Taha", "onStop: isChatPage " + isChatPage + "from instance" + this)
        if (myReceiver != null) {
            try {
                unregisterReceiver(myReceiver)
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            }
        }
        unregisterReceiver(newMessageListener)
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: GroupMemberFetched?) {
        Log.d(TAG, "onMessageEvent:commaIssue ")
        if (isGroupMemebersNotLoaded) {
            isGroupMemebersNotLoaded = false
            Handler().postDelayed({ setGroupMemmbersName() }, 3000)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    @Throws(JSONException::class)
    fun onMessageEvent(event: ReceviceMessageEvent) {

        //        if (!isGroupChat) {
//            Object[] response1 = event.getObjectsArray();
//            JSONObject jsonObject1 = (JSONObject) response1[0];
//            Log.d(TAG, "Receiving Event Name: " + event.getEventName() + " Receiving List: " + jsonObject1.toString());
//        }
//        else
//        {
//            Object[] objs = event.getObjectsArray();
//            JSONObject jsonObject1 = (JSONObject) objs[1];
//            Log.d(TAG, "Receiving Event Name: " + event.getEventName() + " Receiving List: " + jsonObject1.toString());
//        }
        if (!isGroupChat) {
            if (event.eventName.equals(
                    SocketManager.EVENT_START_FILE_DOWNLOAD,
                    ignoreCase = true
                )
            ) {
                try {
                    val response = event.objectsArray
                    val jsonObject = response[1] as JSONObject
                    Log.d(
                        TAG,
                        "onMessageEvent Chat: " + "event name : " + event.eventName + "... response : " + jsonObject.toString()
                    )
                    writeBufferToFile(jsonObject)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if (event.eventName.equals(
                    SocketManager.EVENT_FILE_RECEIVED,
                    ignoreCase = true
                )
            ) {
                try {
                    val obj = event.objectsArray
                    val jsnObj = JSONObject(obj[0].toString())
                    GlobalScope.launch {
                        withContext(Dispatchers.IO) {
                            loadFileUploaded(jsnObj)
                        }
                    }
//                    loadFileUploaded(`object`).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if (event.eventName.equals(SocketManager.EVENT_MESSAGE, ignoreCase = true)) {
                try {
                    val array = event.objectsArray
                    val messageObj: JSONObject = JSONObject(array[0].toString())
                    loadMessage(messageObj)
                } catch (e : Exception ) {
                    Log.e(TAG, "onMessageEvent: json parsing exception = $e" )
                }
            } else if (event.eventName.equals(SocketManager.EVENT_MESSAGE_RES, ignoreCase = true)) {
                loadMessageRes(event)
            } else if (event.eventName.equals(SocketManager.EVENT_GET_USER_DETAILS, ignoreCase = true)) {
                loadUserDetails(event)
            } else if (event.eventName.equals(
                    SocketManager.EVENT_PRIVACY_SETTINGS,
                    ignoreCase = true
                )
            ) {
                loadPrivacySetting(event)
            } else if (event.eventName.equals(
                    SocketManager.EVENT_UN_FRIEND_REQUEST,
                    ignoreCase = true
                )
            ) {
                val obj = event.objectsArray
                unFriendEventHandling(obj[0].toString())
            } else if (event.eventName.equals(SocketManager.EVENT_CLEAR_CHAT, ignoreCase = true)) {
                val obj = event.objectsArray
                load_clear_chat(obj[0].toString())
            } else if (event.eventName.equals(SocketManager.EVENT_DELETE_CHAT, ignoreCase = true)) {
                val obj = event.objectsArray
                val isMassChat = resources.getBoolean(R.bool.is_mass_chat)
                if (isMassChat) {
                    lifecycleScope.launch { loadFromDB() }
                    //load_delete_chat(obj[0].toString());
                }
            } else if (event.eventName.equals(
                    SocketManager.EVENT_CHANGE_USER_NAME,
                    ignoreCase = true
                )
            ) {
                handleUserPinChanged(event)
            } else if (event.eventName.equals(SocketManager.EVENT_TYPING, ignoreCase = true)) {
                loadTypingStatus(event)
            } else if (event.eventName.equals(
                    SocketManager.EVENT_CAHNGE_ONLINE_STATUS,
                    ignoreCase = true
                )
            ) {
                loadOnlineStatus(event)
            } else if (event.eventName.equals(
                    SocketManager.EVENT_GET_CURRENT_TIME_STATUS,
                    ignoreCase = true
                )
            ) {
                loadCurrentTimeMessage(event)
            } else if (event.eventName.equals(
                    SocketManager.EVENT_STAR_MESSAGE,
                    ignoreCase = true
                )
            ) {
                loadStarredMessage(event)
            } else if (event.eventName.equals(
                    SocketManager.EVENT_REMOVE_MESSAGE,
                    ignoreCase = true
                )
            ) {
                loadDeleteMessage(event)
            } else if (event.eventName.equals(SocketManager.EVENT_MUTE, ignoreCase = true)) {
                val response = event.objectsArray
                loadMuteMessage(response[0].toString())
            } else if (event.eventName.equals(SocketManager.EVENT_GET_MESSAGE_DETAILS, ignoreCase = true)) {
                loadReplyMessageDetails(event.objectsArray[0].toString())
            } else if (event.eventName.equals(SocketManager.EVENT_DELETE_MESSAGE, ignoreCase = true)) {
                deleteSingleMessage(event)
            } else if (event.eventName.equals(SocketManager.EVENT_SINGLE_OFFLINE_MSG, ignoreCase = true)) {
                getSingleOffline(event)
            } else if (event.eventName.equals(SocketManager.EVENT_IMAGE_UPLOAD, ignoreCase = true)) {
                updateProfileImage(event)
            } else if (event.eventName.equals(SocketManager.EVENT_MESSAGE_STATUS_UPDATE, ignoreCase = true)) {
                loadMessageStatusupdate(event)
            } else if (event.eventName.equals(SocketManager.EVENT_GET_MESSAGE, ignoreCase = true)) {
                loadOfflineMessage(event)
            } else if (event.eventName.equals(SocketManager.EVENT_BLOCK_USER, ignoreCase = true)) {
                blockunblockcontact(event)
            } else if (event.eventName.equals(SocketManager.EVENT_CALL)){
                if (mChatData!!.size > 0) {
                    for (i in mChatData!!.indices) {
                        if (i > -1 && mChatData!![i]!!.isMediaPlaying) {
                            mChatData!![MessageAdapter.lastPlayedAt]!!.setIsMediaPlaying(false)
//                            mAdapter!!.stopAudioOnClearChat()
                            mAdapter!!.stopAudio()
                            mAdapter!!.notifyDataSetChanged()
//                            mChatData!![i]!!.setIsMediaPlaying(false)
                        }

                    }
                }
            } else if (event.eventName.equals(
                    SocketManager.EVENT_REPORT_SPAM_USER,
                    ignoreCase = true
                )
            ) {
                try {
                    val obj = event.objectsArray
                    val `object` = JSONObject(obj[0].toString())
                    Toast.makeText(
                        this,
                        resources.getString(R.string.contact_is_reported_as_spam),
                        Toast.LENGTH_LONG
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } else {
            if (event.eventName.equals(SocketManager.EVENT_GROUP, ignoreCase = true)) {
                try {
                    val obj = event.objectsArray
                    val `object` = JSONObject(obj[0].toString())
                    val error = `object`.getString("err")
                    if (error.equals("0", ignoreCase = true) || error.equals(
                            "",
                            ignoreCase = true
                        )
                    ) {
                        val groupAction = `object`.getString("groupType")
                        if (groupAction.equals(
                                SocketManager.ACTION_EVENT_GROUP_MESSAGE,
                                ignoreCase = true
                            )
                        ) {
                            val from = `object`.getString("from")
                            //  if (!from.equalsIgnoreCase(mCurrentUserId)) {
                            //getUserDetails(from);
                            handleGroupMessage(event)
                            //   }
                            //   handleGroupMessage(event);
                        } else if (groupAction.equals(
                                SocketManager.ACTION_ACK_GROUP_MESSAGE,
                                ignoreCase = true
                            )
                        ) {
//                    Toast.makeText(ChatViewActivity.this, "".concat(String.valueOf(object)), Toast.LENGTH_LONG).show();
                            updateGroupMsgStatus(`object`)
                        } else if (groupAction.equals(
                                SocketManager.ACTION_CHANGE_GROUP_NAME,
                                ignoreCase = true
                            )
                        ) {
                            loadChangeGroupNameMessage(`object`)
                        } else if (groupAction.equals(
                                SocketManager.ACTION_EXIT_GROUP,
                                ignoreCase = true
                            )
                        ) {
                            loadExitMessage(`object`)
                        } else if ((groupAction == SocketManager.ACTION_CHANGE_GROUP_DP)) {
                            performGroupChangeDp(`object`)
                        } else if (groupAction.equals(
                                SocketManager.ACTION_DELETE_GROUP_MEMBER,
                                ignoreCase = true
                            )
                        ) {
                            loadDeleteMemberMessage(`object`)
                        } else if (groupAction.equals(
                                SocketManager.ACTION_ADD_GROUP_MEMBER,
                                ignoreCase = true
                            )
                        ) {
                            loadAddMemberMessage(`object`)
                        } else if (groupAction.equals(
                                SocketManager.ACTION_MAKE_GROUP_ADMIN,
                                ignoreCase = true
                            )
                        ) {
                            loadMakeAdminMessage(`object`)
                        } else if (groupAction.equals(
                                SocketManager.ACTION_EVENT_GROUP_MSG_DELETE,
                                ignoreCase = true
                            )
                        ) {
                            deleteGroupMessage(event)
                        } else if (groupAction.equals(
                                SocketManager.ACTION_EVENT_GROUP_OFFLINE,
                                ignoreCase = true
                            )
                        ) {
                            getGroupOffline(event)
                        } else if (groupAction.equals(
                                SocketManager.ACTION_JOIN_NEW_GROUP,
                                ignoreCase = true
                            )
                        ) {
                            lifecycleScope.launch {
                                delay(5000)
                                loadFromDB()
                            }
//                            Handler(Looper.getMainLooper()).postDelayed({ loadFromDB() }, 5000)
                        }
                    } else {
                        val message = `object`.getString("msg")
                        val messageid = `object`.getString("doc_id")
                        for (i in mChatData!!.indices) {
                            val msg_id = mChatData!![i]!!.messageId
                            if (msg_id.equals(messageid, ignoreCase = true)) {
                                mChatData!!.get(i)!!.uploadStatus = 1000
                                mAdapter!!.notifyDataSetChanged()
                                break
                            }
                        }
                        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
                    }
                } catch (ex: JSONException) {
                    ex.printStackTrace()
                }
            } else if (event.eventName.equals(
                    SocketManager.EVENT_GROUP_DETAILS,
                    ignoreCase = true
                )
            ) {
                loadGroupDetails(event)
            } else if (event.eventName.equals(
                    SocketManager.EVENT_FILE_RECEIVED,
                    ignoreCase = true
                )
            ) {
                try {
                    val objs = event.objectsArray
                    val objects = JSONObject(objs[0].toString())
                    Log.d(
                        TAG,
                        "onMessageEvent Chat: " + "event name : " + event.eventName + "... response : " + objects.toString()
                    )
                    GlobalScope.launch {
                        withContext(Dispatchers.IO) {
                            loadFileUploaded(objects)
                        }
                    }
//                    loadFileUploaded(objects).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if (event.eventName.equals(
                    SocketManager.EVENT_START_FILE_DOWNLOAD,
                    ignoreCase = true
                )
            ) {
                try {
                    val response = event.objectsArray
                    val jsonObject = response[1] as JSONObject
                    writeBufferToFile(jsonObject)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if (event.eventName.equals(SocketManager.EVENT_CLEAR_CHAT, ignoreCase = true)) {
                val obj = event.objectsArray
                load_clear_chat(obj[0].toString())
            }
        }
        if (event.eventName.equals(SocketManager.EVENT_USER_AUTHENTICATED, ignoreCase = true)) {
            if (!isGroupChat) {
                if (!isLastSeenCalled) {
                    receiverOnlineTimeStatus
                }
            }
            if (mChatData!!.size > 0) {
                sendAllAcksToServer(mChatData)
            }
        }
    }

    private fun setGroupMemmbersName() {
        if (groupInfoSession == null) return
        if (hasGroupInfo) {
            val infoPojo = groupInfoSession!!.getGroupInfo(docId)
            sb = StringBuilder()
            var memername: String?
            if (infoPojo != null && infoPojo.groupMembers != null) {
                if (infoPojo.groupMembers.contains("[")) {
                    infoPojo.groupMembers.replace("[", "")
                    infoPojo.groupMembers.replace("]", "")
                }
                val contacts = infoPojo.groupMembers.split(",").toTypedArray()
                for (i in contacts.indices) {
                    if (!contacts[i].equals(from, ignoreCase = true)) {
                        val msisdn =
                            contactDB_sqlite!!.getSingleData(contacts[i], ContactDB_Sqlite.MSISDN)
                        if (msisdn != null) {
                            memername = getcontactname!!.getSendername(contacts[i], msisdn)
                            if (memername == null || memername.isEmpty()) {
                                memername = msisdn
                            }
                            if (AppUtils.isEmpty(memername)) {
                                isGroupMemebersNotLoaded = true
                                break
                            }
                            sb!!.append(memername)
                            if (contacts.size - 1 != i) {
                                sb!!.append(", ")
                            }
                        }
                    } else {
                        memername = "You"
                        sb!!.append(memername)
                        if (contacts.size - 1 != i) {
                            sb!!.append(", ")
                        }
                    }
                }
                MyLog.d(
                    TAG,
                    "loadFromDB>>>>: $sb"
                )
                status_textview?.post { status_textview?.text = sb }
                enableGroupChat = infoPojo.isLiveGroup
            }
        }
    }

    private fun handleUserPinChanged(event: ReceviceMessageEvent) {
        try {
            val obj = event.objectsArray
            val `object` = JSONObject(obj[0].toString())
            val err = `object`.getString("err")
            if ((err == "0")) {
                val from = `object`.getString("from")
                val pinCode = `object`.getString("pinCode")
                val mCurrentUserId = SessionManager.getInstance(context).currentUserID
                if (from != mCurrentUserId) {
                    val friendModel =
                        CoreController.getContactSqliteDBintstance(context).getFriendDetails(from)
                    if (friendModel != null) {
                        if (friendModel.pinCode != pinCode) {
                            finish()
                        }
                    }
                }
            }
        } catch (ex: JSONException) {
            showToast(context, getString(R.string.call_fail_error))
            ex.printStackTrace()
        }
    }

    //--------------------------------------Message Receiver Events------------------------------------
    private fun DeleteGroupMessage(objects: JSONObject) {
        try {
            val errorState = objects.getInt("err")
            if (errorState == 0) {
                val fromId = objects.getString("from")
                val docId: String
                val msgId: String
                val type: String
                var recId: String
                var lastMsg_Status: String
                var convId: String
                //                String deleteStatus = objects.getString("status");
                val chat_id = objects["doc_id"] as String
                val ids = chat_id.split("-").toTypedArray()
                type = objects.getString("type")
                //                recId = objects.getString("recordId");
//                convId = objects.getString("convId");
                docId = ids.get(1) + "-" + ids[0]
                msgId = docId + "-" + ids[2]
                if (fromId.equals(from, ignoreCase = true)) {
                    for (items: MessageItemChat? in mChatData!!) {
                        if (items != null && items.messageId.equals(chat_id, ignoreCase = true)) {
                            val index = mChatData!!.indexOf(items)
                            if (index > -1 && mChatData!![index]!!.isMediaPlaying) {
                                mAdapter!!.stopAudioOnMessageDelete(index)
                            }
                            mChatData!!.get(index)!!.messageType =
                                MessageFactory.DELETE_SELF.toString() + ""
                            mChatData!![index]!!.setIsSelf(true)
                            mAdapter!!.notifyDataSetChanged()
                            messageDatabase!!.deleteSingleMessage(
                                docId,
                                items.messageId,
                                chatType,
                                "self"
                            )
                            messageDatabase!!.deleteChatListPage(
                                docId,
                                items.messageId,
                                chatType,
                                "self"
                            )
                            break
                        }
                    }
                }
                if (!fromId.equals(from, ignoreCase = true) && fromId.equals(
                        Chat_to,
                        ignoreCase = true
                    )
                ) {
                    for (items: MessageItemChat? in mChatData!!) {
                        if (items != null && items.messageId.equals(msgId, ignoreCase = true)) {
                            val index = mChatData!!.indexOf(items)
                            if (index > -1 && mChatData!![index]!!.isMediaPlaying) {
                                mAdapter!!.stopAudioOnMessageDelete(index)
                            }
                            mChatData!!.get(index)!!.messageType =
                                MessageFactory.DELETE_OTHER.toString() + ""
                            mChatData!![index]!!.setIsSelf(false)
                            mAdapter!!.notifyDataSetChanged()
                            messageDatabase!!.deleteSingleMessage(docId, msgId, type, "other")
                            messageDatabase!!.deleteChatListPage(docId, msgId, type, "other")
                            break
                        }
                    }
                }
            }
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }
    }

    private fun writeBufferToFile(`object`: JSONObject) {
        try {
            val fileName = `object`.getString("ImageName")
            val localPath = `object`.getString("LocalPath")
            //          String localFileName = object.getString("LocalFileName");
            val end = `object`.getString("end")
            val bytesRead = `object`.getInt("bytesRead")
            val fileSize = `object`.getInt("filesize")
            val msgId = `object`.getString("MsgId")
            val Start = `object`.getString("start")
            val isFilePaused = uploadDownloadManager!!.isDownloadFilePaused(msgId)
            MyLog.e(
                "DownloadFileStatus",
                "DownloadingGetStreaming$`object`"
            )
            MyLog.e(
                "DownloadFileStatus",
                "DownloadingPauseStatus $isFilePaused"
            )
            if (!isFilePaused) {

                //  int progress = (bytesRead / fileSize) * 100;
                val start = Start.toInt()
                val progress = (start * 100) / fileSize
                for (i in mChatData!!.indices) {
                    if (msgId.equals(mChatData!![i]!!.messageId, ignoreCase = true)) {
                        //   progress = (start * 100) / fileSize;
                        if (progress == 0) {
                            mChatData!!.get(i)!!.uploadDownloadProgress = 2
                            // mChatData.get(i).setVideoPath(localPath);
                        } else {
                            mChatData!!.get(i)!!.uploadDownloadProgress = progress
                        }
                        if (end.equals("1", ignoreCase = true)) {
                            mChatData!!.get(i)!!.downloadStatus =
                                MessageFactory.DOWNLOAD_STATUS_COMPLETED
                            // db.updateMessageDownloadStatus(docId, msgId, MessageFactory.DOWNLOAD_STATUS_COMPLETED);
                        }
                        break
                    }
                }
                mAdapter!!.notifyDataSetChanged()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun loadMessageRes(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            val objects = JSONObject(array[0].toString())
            MyLog.e("loadMessageRes", "loadMessageRes$objects")
            val errorState = objects.getInt("err")
            if (errorState == 0) {
                val chat_id = objects["doc_id"] as String
                val ids = chat_id.split("-").toTypedArray()
                val doc_id = ids.get(0) + "-" + ids[1]
                val delivery = objects["deliver"] as Int
                val to = ids[1]
                val msgData = objects.getJSONObject("data")
                val recordId = msgData.getString("recordId")
                val convId = msgData.getString("convId")
                val type = msgData.getString("type").toInt()
                if (type == MessageFactory.nudge) {
                    // AudioPlayerUtils.playSound(context, R.raw.msn_nudge_sound);
                }
                //msgid is null not coming
                if (msgData.has("msgId")) {
                    val id = msgData.getString("msgId")
                    if (newMsgIds!!.contains(id)) {
                        return
                    }
                    newMsgIds!!.add(id)
                } else {
                    return
                } /*
                if (newMsgIds.contains(id)) {
                    return;
                }
    */
                //   newMsgIds.add(id);
                val secretType = msgData.getString("secret_type")
                if (to.equals(mReceiverId, ignoreCase = true) && secretType.equals(
                        "no",
                        ignoreCase = true
                    )
                ) {
                    layout_new!!.visibility = View.GONE
                    mConvId = convId
                    var item: MessageItemChat? = null
                    for (msgItemChat: MessageItemChat? in mChatData!!) {
                        if (msgItemChat!!.messageId.equals(chat_id, ignoreCase = true)) {
                            item = msgItemChat
                            break
                        }
                    }
                    if (item != null) {
                        val msgIndex = mChatData!!.indexOf(item)
                        mChatData!!.get(msgIndex)!!.uploadStatus =
                            MessageFactory.UPLOAD_STATUS_COMPLETED
                        mChatData!!.get(msgIndex)!!.deliveryStatus = "" + delivery
                        mChatData!!.get(msgIndex)!!.recordId = recordId
                        mChatData!!.get(msgIndex)!!.convId = convId
                        mAdapter!!.notifyDataSetChanged()
                    } else {
                        val toUserId = msgData.getString("to")
                        if (toUserId.equals(mReceiverId, ignoreCase = true)) {
                            val incomingMessage = IncomingMessage(this@ChatPageActivity)
                            val newMsgItem = incomingMessage.loadSingleMessageFromWeb(objects)
                            if (newMsgItem != null) {
                                if (!isAlreadyExist(newMsgItem)) mChatData!!.add(newMsgItem)
                                notifyDatasetChange()
                            }
                        }
                    }
                    if (session!!.prefsNameintoneouttone) {
                        try {
/*                            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                            r.play();*/
                            MediaPlayer.create(this, R.raw.send_message).start()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                } /*else if (errorState == 1) {
                SessionManager.getInstance(ChatViewActivity.this).logoutUser();
            }*/
            } else if (errorState == 1) {
                val chat_id = objects["doc_id"] as String
                var item: MessageItemChat? = null
                for (msgItemChat: MessageItemChat? in mChatData!!) {
                    if (msgItemChat!!.messageId.equals(chat_id, ignoreCase = true)) {
                        item = msgItemChat
                        break
                    }
                }
                if (item != null) {
                    val msgIndex = mChatData!!.indexOf(item)
                    mChatData!!.get(msgIndex)!!.uploadStatus =
                        MessageFactory.UPLOAD_STATUS_UPLOADING
                    mChatData!!.get(msgIndex)!!.uploadDownloadProgress = 0
                    Log.e(TAG, "loadMessageRes: notifyDataSetChanged")
                    mAdapter!!.notifyDataSetChanged()
                }
            }
        } catch (e: Exception) {
            MyLog.e(TAG, "loadMessageRes: ", e)
        }
    }

    private fun loadOfflineMessage(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            MyLog.d(TAG, "loadOfflineMessage: OfflineMsgTest " + array[0])
            val responseObj: JSONObject = JSONObject(array[0].toString())
            if (responseObj.has("result")) {
                MyLog.d(TAG, "loadOfflineMessage: OfflineMsgTest has result array")
                val resultArray = responseObj.getJSONArray("result")
                for (i in 0 until resultArray.length()) {
                    MyLog.d(TAG, "loadOfflineMessage: OfflineMsgTest for loop")
                    val offlineMsg = resultArray.getJSONObject(i)
                    MyLog.d(TAG, "loadOfflineMessage: OfflineMsgTest $offlineMsg")
                    loadMessage(offlineMsg)
                }
            }
        } catch (e: java.lang.Exception) {
            MyLog.e(TAG, "loadOfflineMessage", e)
            //for Non-Encryption apps
            try {
                val msgObj: JSONObject = JSONObject(array[0].toString())
                loadMessage(msgObj)
            } catch (ex: java.lang.Exception) {
                MyLog.e(TAG, "loadOfflineMessage: OfflineMsgTest ", ex)
            }
        }
    }

    private fun loadMessage(objects : JSONObject) {
        try {
            var type: String = ""
            var normal_Offline = 0
            if (objects.has("is_deleted_everyone")) {
                normal_Offline = objects.getInt("is_deleted_everyone")
            }
            if (objects.has("type")) {
                type = objects.getString("type").toString()
                if (objects.optString("type").toInt() == 23) {
                    return
                }
            }
            var secretType ="no";
            if (objects.has("secret_type")) {
                secretType = objects.getString("secret_type")
            }
            if (secretType.equals("no", ignoreCase = true)) {
                val incomingMsg = IncomingMessage(this@ChatPageActivity)
                val item = incomingMsg.loadSingleMessage(objects)
//                if (isAlreadyExist(item))
//                    return
                val from = objects.getString("from")
                val to = objects.getString("to")
                session!!.Removemark(from)
                val id = objects.getString("msgId")
                val convId = objects.getString("convId")
                val doc_id: String
                if (objects.has("docId")) {
                    doc_id = objects.getString("docId")
                } else {
                    doc_id = objects.getString("doc_id")
                }
                var uniqueID = ""
                if (from.equals(mCurrentUserId, ignoreCase = true)) {
                    uniqueID = "$from-$to"
                    item.deliveryStatus = MessageFactory.DELIVERY_STATUS_SENT
                } else if (to.equals(mCurrentUserId, ignoreCase = true)) {
                    uniqueID = "$to-$from"
                    if (sessionManager!!.canSendReadReceipt()) {
                        uniqueID = "$to-$from"
                        if (from.equals(mReceiverId, ignoreCase = true)) {
                            sendAckToServer(from, doc_id, "" + id, convId)
                        }
                    }
                    item.deliveryStatus = MessageFactory.DELIVERY_STATUS_READ
                    getUserDetails(from)
                }
                item.messageId = "$uniqueID-$id"
                if (secretType.equals("yes", ignoreCase = true)) {
                    uniqueID = uniqueID + "-" + MessageFactory.CHAT_TYPE_SECRET
                    val timer = objects.getString("incognito_timer")
                    item.secretTimer = timer
                    item.secretTimeCreatedBy = from
                }
                if (session!!.prefsNameintoneouttone) {
                    try {
/*                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        r.play();*/
                        MediaPlayer.create(this, R.raw.send_message).start()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                if (from.equals(mReceiverId, ignoreCase = true)) {
                    mConvId = convId
                    if (isChatPage) {
                        changeBadgeCount(uniqueID)
                    }
                    sendChatViewedStatus()
                    if (lastvisibleitempostion < mChatData!!.size - 1) {
                        unreadmsgcount++
                        unreadmessage()
                        if (!id.equals(Message_id, ignoreCase = true)) {
                            if (!isAlreadyExist(item)) mChatData!!.add(item)
                            Message_id = id
                            setEncryptionMsg()
                            mAdapter?.notifyItemInserted(mChatData!!.size - 1)
                            //Check if the last item is not visible move to it

//                            int last = chat_list.getLastVisiblePosition();
                            //  Log.e(TAG, "getLastVisiblePosition: last"+last);
                            if (!mListviewScrollBottom) {
//                                mListviewScrollBottom = true
                                setLastItemSelection()
                            }
                        }
                    } else {
                        if (!id.equals(Message_id, ignoreCase = true)) {
                            if (!isAlreadyExist(item)) {
                                mChatData!!.add(item)
                            }
                            Message_id = id
                            setEncryptionMsg()
                            mAdapter?.notifyItemInserted(mChatData!!.size - 1)
                            setLastItemSelection()
                        }
                    }


                    //-------------Delete Chat-----------------
                    if (normal_Offline == 1) {
                        val new_docId: String
                        val new_msgId: String
                        val new_type: String
                        var new_recId: String
                        var new_convId: String
                        try {
                            val fromId = objects.getString("from")
                            if (!fromId.equals(mCurrentUserId, ignoreCase = true)) {
                                val chat_id = objects["docId"] as String
                                val ids = chat_id.split("-").toTypedArray()
                                new_type = objects.getString("chat_type")
                                //                                new_recId = objects.getString("recordId");
//                                new_convId = objects.getString("convId");
                                if (new_type.equals("single", ignoreCase = true)) {
                                    new_docId = ids.get(1) + "-" + ids[0]
                                    new_msgId = new_docId + "-" + ids[2]
                                    for (i in mChatData!!.indices) {
                                        if (mChatData!![i]!!
                                                .messageId.equals(new_msgId, ignoreCase = true)
                                        ) {

//                                            mChatData.get(i).setMessageType("Delete Others");
                                            mChatData!!.get(i)!!.messageType =
                                                MessageFactory.DELETE_OTHER.toString() + ""
                                            mChatData!![i]!!.setIsSelf(false)
                                            mAdapter!!.notifyDataSetChanged()
                                            messageDatabase!!.deleteSingleMessage(
                                                new_docId,
                                                new_msgId,
                                                new_type,
                                                "other"
                                            )
                                            messageDatabase!!.deleteChatListPage(
                                                new_docId,
                                                new_msgId,
                                                new_type,
                                                "other"
                                            )
                                            break
                                        }
                                    }
                                } else {
                                    new_docId = fromId + "-" + ids[1] + "-g"
                                    new_msgId = docId + "-" + ids[3]
                                    val groupAndMsgId = ids.get(1) + "-g-" + ids[3]
                                    if (mChatData!!.size > 0) {
                                        for (i in mChatData!!.indices) {
                                            if (mChatData!![i]!!.messageId.contains(groupAndMsgId)) {
                                                if (i > -1 && mChatData!![i]!!.isMediaPlaying) {
                                                    mAdapter!!.stopAudioOnMessageDelete(i)
                                                }

//                                                mChatData.get(i).setMessageType("Delete Others");
                                                mChatData!!.get(i)!!.messageType =
                                                    MessageFactory.DELETE_OTHER.toString() + ""
                                                mChatData!![i]!!.setIsSelf(false)
                                                mAdapter!!.notifyDataSetChanged()
                                                messageDatabase!!.deleteSingleMessage(
                                                    groupAndMsgId,
                                                    new_msgId,
                                                    type,
                                                    "other"
                                                )
                                                messageDatabase!!.deleteChatListPage(
                                                    groupAndMsgId,
                                                    new_msgId,
                                                    type,
                                                    "other"
                                                )
                                                break
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (ex: JSONException) {
                            ex.printStackTrace()
                        }
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun unreadmessage() {
        /* int lastvisibleitempostion = mLayoutManager.findLastVisibleItemPosition();

        if (lastvisibleitempostion == mChatData.size() - 1) {
            iBtnScroll.setVisibility(View.GONE);
            unreadcount.setVisibility(View.GONE);
            unreadmsgcount = 0;
        } else {
            unreadcount.setVisibility(View.VISIBLE);
            iBtnScroll.setVisibility(View.VISIBLE);
            unreadcount.setText(String.valueOf(unreadmsgcount));
        }*/
    }

    private fun sendAckToServer(to: String?, doc_id: String, id: String, convId: String) {
        MyLog.d(TAG, "$$$$$$ sendAckToServer(): ")
        val messageEvent = SendMessageEvent()
        messageEvent.eventName = SocketManager.EVENT_MESSAGE_ACK
        val ack = MessageFactory.getMessage(MessageFactory.message_ack, this) as MessageAck
        messageEvent.messageObject =
            ack.getMessageObject(
                to,
                doc_id,
                MessageFactory.DELIVERY_STATUS_READ,
                id,
                false,
                convId
            ) as JSONObject?
        EventBus.getDefault().post(messageEvent)
    }

    private var requestedMemebers = 0
    fun getUserDetails(userId: String?) {
        if (!contactDB_sqlite!!.isUserAvailableInDB(userId)) {
            try {
                if (requestedMemebers < 5) {
                    requestedMemebers++
                    val eventObj = JSONObject()
                    eventObj.put("userId", userId)
                    val event = SendMessageEvent()
                    event.eventName = SocketManager.EVENT_GET_USER_DETAILS
                    event.messageObject = eventObj
                    EventBus.getDefault().post(event)
                }
            } catch (ex: JSONException) {
                ex.printStackTrace()
            }
        }
    }

    private fun sendChatViewedStatus() {
        val currentTime = Calendar.getInstance().timeInMillis
        val timeDiff = currentTime - lastViewStatusSentAt
        if (timeDiff > 4000) {
            lastViewStatusSentAt = currentTime
            if (mConvId != null && mConvId != "") {
                sendViewedStatusToWeb(mConvId!!)
            }
        }
    }

    private fun sendViewedStatusToWeb(convId: String) {
        try {
            val `object` = JSONObject()
            `object`.put("from", mCurrentUserId)
            `object`.put("convId", convId)
            if (isGroupChat) {
                `object`.put("type", "group")
            } else {
                `object`.put("type", "single")
            }
            `object`.put("mode", "phone")
            val event = SendMessageEvent()
            event.eventName = SocketManager.EVENT_VIEW_CHAT
            event.messageObject = `object`
            EventBus.getDefault().post(event)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun setEncryptionMsg() {
        handler.post {
            if (mChatData != null && mChatData!!.size == 0) {
                findViewById<View>(R.id.encryptionlabel).visibility =
                    View.VISIBLE
            } else {
                findViewById<View>(R.id.encryptionlabel).visibility =
                    View.GONE
            }
        }
    }

    //----------------------------------Image Download----------------------------
    fun imagedownloadmethod(position: Int) {
        val item = mChatData!![position]
        if ((item!!.uploadDownloadProgress == 0) && !item.isSelf && (item.downloadStatus == MessageFactory.DOWNLOAD_STATUS_NOT_START)) {
            item.downloadStatus = MessageFactory.DOWNLOAD_STATUS_DOWNLOADING
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        } else if ((item.downloadStatus == MessageFactory.DOWNLOAD_STATUS_NOT_START
                    && item.isSelf)
        ) {
            /*Download option for sent documents from web chat*/
            item.downloadStatus = MessageFactory.DOWNLOAD_STATUS_DOWNLOADING
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        }
        if (FileUploadDownloadManager.FETCH_DOWNLOAD_ENABLED) {
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        }
    }

    fun VideoDownload(position: Int) {
        val item = mChatData!![position]
        if ((item!!.uploadDownloadProgress == 0) && !item.isSelf && (
                    item.downloadStatus == MessageFactory.DOWNLOAD_STATUS_NOT_START)
        ) {
            item.downloadStatus = MessageFactory.DOWNLOAD_STATUS_DOWNLOADING
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)

            /* String fullPath = item.getChatFileServerPath();

            String message_id=item.getMessageId();

            DownloadServiceClass.DownloadFileFromURL download=new DownloadServiceClass.DownloadFileFromURL(this);
            download.execute(fullPath,message_id);*/

            // download.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,fullPath,message_id);
        } else if (!item.isSelf && item.downloadStatus == MessageFactory.DOWNLOAD_STATUS_COMPLETED) {
            try {
                var videoPath = item.chatFileLocalPath
                val file = File(item.chatFileLocalPath)
                if (!file.exists()) {
                    try {
                        val filePathSplited =
                            item.chatFileLocalPath.split(File.separator).toTypedArray()
                        val fileName = filePathSplited[filePathSplited.size - 1]
                        val publicDirPath = Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOWNLOADS
                        ).absolutePath
                        videoPath = publicDirPath + File.separator + fileName
                    } catch (e: Exception) {
                        MyLog.e(TAG, "configureViewHolderImageReceived: ", e)
                    }
                }
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(videoPath))
                val path = "file://$videoPath"
                intent.setDataAndType(Uri.parse(path), "video/*")
                noNeedRefresh = true
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(
                    this@ChatPageActivity,
                    getString(R.string.no_app_to_play_video),
                    Toast.LENGTH_LONG
                ).show()
            }
        } else if ((item.downloadStatus == MessageFactory.DOWNLOAD_STATUS_NOT_START
                    && item.isSelf)
        ) {
            /*Download option for sent documents from web chat*/
            item.downloadStatus = MessageFactory.DOWNLOAD_STATUS_DOWNLOADING
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        }
        if (FileUploadDownloadManager.FETCH_DOWNLOAD_ENABLED) {
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        }
    }

    //------------------------------Video Download-------------------------------------------
    fun AudioDownload(position: Int) {
        val item = mChatData!![position]
        if ((item!!.uploadDownloadProgress == 0) && !item.isSelf && (item.downloadStatus == MessageFactory.DOWNLOAD_STATUS_NOT_START)) {
            item.downloadStatus = MessageFactory.DOWNLOAD_STATUS_DOWNLOADING
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        } else if ((item.downloadStatus == MessageFactory.DOWNLOAD_STATUS_NOT_START
                    && item.isSelf)
        ) {
            /*Download option for sent documents from web chat*/
            item.downloadStatus = MessageFactory.DOWNLOAD_STATUS_DOWNLOADING
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        }
        if (FileUploadDownloadManager.FETCH_DOWNLOAD_ENABLED) {
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        }
    }

    //-------------------------------------Audio Download---------------------------------------
    fun DocumentDownload(position: Int) {
        val item = mChatData!![position]
        val downloadStatus = item!!.downloadStatus
        MyLog.d(
            TAG,
            "DocumentDownload downloadStatus: $downloadStatus"
        )
        if (item.downloadStatus == MessageFactory.DOWNLOAD_STATUS_NOT_START && !item.isSelf) {
            item.downloadStatus = MessageFactory.DOWNLOAD_STATUS_DOWNLOADING
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        } else if ((item.downloadStatus == MessageFactory.DOWNLOAD_STATUS_NOT_START
                    && item.isSelf)
        ) {
            /*Download option for sent documents from web chat*/
            item.downloadStatus = MessageFactory.DOWNLOAD_STATUS_DOWNLOADING
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        } else if (FileUploadDownloadManager.FETCH_DOWNLOAD_ENABLED) {
            uploadDownloadManager!!.startFileDownload(EventBus.getDefault(), item, false)
        } else {
            val extension = MimeTypeMap.getFileExtensionFromUrl(item.chatFileLocalPath)
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            val packageManager = packageManager
            val testIntent = Intent(Intent.ACTION_VIEW)
            testIntent.type = mimeType
            try {
                val list: List<*> = packageManager.queryIntentActivities(
                    testIntent,
                    PackageManager.MATCH_DEFAULT_ONLY
                )
                if (list.size > 0) {
                    val file = File(item.chatFileLocalPath)
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.setDataAndType(Uri.fromFile(file), mimeType)
                    noNeedRefresh = true
                    startActivity(intent)
                } else {
                    Toast.makeText(
                        this@ChatPageActivity,
                        resources.getString(R.string.no_app_installed_to_view_this_document),
                        Toast.LENGTH_LONG
                    ).show()
                }
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(
                    this@ChatPageActivity,
                    resources.getString(R.string.no_app_installed_to_view_this_document),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    //-------------------------------------Document Download----------------------------------------
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((requestCode == RESULT_LOAD_IMAGE) && (resultCode == Activity.RESULT_OK) && (null != data)) {
            try {
                noNeedRefresh = true
                val bundle = data.extras
                try {
                    val pathlist =
                        bundle!!.getSerializable("pathlist") as ArrayList<Imagepath_caption>?
                    for (i in pathlist!!.indices) {
                        val path = pathlist[i].getPath()
                        val caption = pathlist[i].getCaption()
                        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
                            DisplayAlert(
                                resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                                    R.string.to_send_message
                                )
                            )
                        } else {
                            sendImageChatMessage(path, caption, false)
                        }
                    }
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if ((requestCode == SHARE_INTENT) && (resultCode == Activity.RESULT_OK) && (null != data)) {
            try {
                noNeedRefresh = true
                val bundle = data.extras
                try {
                    val pathlist =
                        bundle!!.getSerializable("pathlist") as ArrayList<Imagepath_caption>?
                    for (i in pathlist!!.indices) {
                        val path = pathlist[i].getPath()
                        val caption = pathlist[i].getCaption()
                        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
                            DisplayAlert(
                                resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                                    R.string.to_send_message
                                )
                            )
                        } else {
                            sendImageChatMessage(path, caption, false)
                        }
                    }
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (requestCode == RESULT_LOAD_VIDEO && resultCode == Activity.RESULT_OK) {
            noNeedRefresh = true
            var pathlist: ArrayList<Imagepath_caption>? = ArrayList()
            val bundle = data!!.extras
            try {
                pathlist = bundle!!.getSerializable("pathlist") as ArrayList<Imagepath_caption>?
                val path = pathlist!![0].getPath()
                val caption = pathlist[0].getCaption()
                sendVideoChatMessage(path, caption, false)
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        } else if (requestCode == REQUEST_SELECT_AUDIO && resultCode == Activity.RESULT_OK) {
            noNeedRefresh = true
            val fileName = data!!.getStringExtra("FileName")
            val filePath = data.getStringExtra("FilePath")
            val duration = data.getStringExtra("Duration")
            sendAudioMessage(filePath, duration, MessageFactory.AUDIO_FROM_ATTACHMENT, false)
        } else if (requestCode == FilePickerConst.REQUEST_CODE_DOC) {
            noNeedRefresh = true
            if (resultCode == Activity.RESULT_OK && data != null) {
                val docPaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS)
                if (docPaths!!.size > 0) {
                    sendDocumentMessage(docPaths[0], false)
                }
            }
        } else if ((requestCode == RESULT_SHARE_LOCATION) && (resultCode == Activity.RESULT_OK) && (null != data)) {
            noNeedRefresh = true
            val path = data.getStringExtra(AppConstants.IntentKeys.FILE_PATH.value)
            val thumbnail = data.getStringExtra(AppConstants.IntentKeys.THUMBNAIL_PATH.value)
            val address1 = data.getStringExtra(AppConstants.IntentKeys.LOCATION_ADDRESS.value)
            val latitude = data.getDoubleExtra(AppConstants.IntentKeys.LOCATION_LATITUDE.value, 0.0)
            val longitude =
                data.getDoubleExtra(AppConstants.IntentKeys.LOCATION_LONGITUDE.value, 0.0)
            //Place place = PlacePicker.getPlace(ChatPageActivity.this, data);
            //if (place != null) {
            val latlng: LatLng = LatLng(latitude, longitude)
            //if (place.getAddress() != null)
            run {
                val address: String? = address1
                var name: String? = address1
                if (name!!.length > 0 && name.get(0) == '(') {
                    val parts: Array<String> = name.split(",").toTypedArray()
                    name = parts.get(0).trim { it <= ' ' } + "," + parts.get(1).trim { it <= ' ' }
                }
                if (latlng != null) {
                    sendLocationMessage(latlng, name, address)
                }
            }
        } else if ((requestCode == REQUEST_CODE_CONTACTS) && (resultCode == Activity.RESULT_OK) && (data != null)) {
            noNeedRefresh = true
            val bundle = data.extras
            try {
                contacts = bundle!!.getSerializable("ContactToSend") as ArrayList<ContactToSend>?
                val Uname = bundle.getString("name")
                val Title = bundle.getString("title")
                val phone = JSONArray()
                val email = JSONArray()
                val address = JSONArray()
                val IM = JSONArray()
                val Organisation = JSONArray()
                val name = JSONArray()
                var contactScimboId: String? = ""
                val savedScimboContacts = contactDB_sqlite!!.savedScimboContacts
                if (contacts!!.size > 0) {
                    for (i in contacts!!.indices) {
                        if (contacts!![i].type.equals("Phone", ignoreCase = true)) {
                            try {
                                val jsonObject = JSONObject()
                                jsonObject.put("type", contacts!![i].subType)
                                jsonObject.put("value", contacts!![i].number)
                                phone.put(jsonObject)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        } else if (contacts!![i].type.equals("Email", ignoreCase = true)) {
                            try {
                                val jsonObject = JSONObject()
                                jsonObject.put("type", contacts!![i].subType)
                                jsonObject.put("value", contacts!![i].number)
                                email.put(jsonObject)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        } else if (contacts!![i].type.equals("Address", ignoreCase = true)) {
                            try {
                                val jsonObject = JSONObject()
                                jsonObject.put("type", contacts!![i].subType)
                                jsonObject.put("value", contacts!![i].number)
                                address.put(jsonObject)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        } else if (contacts!![i].type.equals(
                                "Instant Messenger",
                                ignoreCase = true
                            )
                        ) {
                            try {
                                val jsonObject = JSONObject()
                                jsonObject.put("type", contacts!![i].subType)
                                jsonObject.put("value", contacts!![i].number)
                                IM.put(jsonObject)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        } else if (contacts!![i].type.equals("Organisation", ignoreCase = true)) {
                            try {
                                val jsonObject = JSONObject()
                                jsonObject.put("type", contacts!![i].subType)
                                jsonObject.put("value", contacts!![i].number)
                                Organisation.put(jsonObject)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        } else if (contacts!![i].type.equals("Name", ignoreCase = true)) {
                            try {
                                val jsonObject = JSONObject()
                                jsonObject.put("type", contacts!![i].subType)
                                jsonObject.put("value", contacts!![i].number)
                                name.put(jsonObject)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }
                    }
                    val itemChat = MessageItemChat()
                    val finalObj = JSONObject()
                    try {
                        finalObj.put("phone_number", phone)
                        finalObj.put("email", email)
                        finalObj.put("address", address)
                        finalObj.put("im", IM)
                        finalObj.put("organisation", Organisation)
                        finalObj.put("name", name)
                        ContactString = finalObj.toString()
                        itemChat.detailedContacts = ContactString
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    if (contacts!!.size >= 1) {
                        number = contacts!![0].number
                        number = number?.replace(" ", "")?.replace("(", "")
                            ?.replace(")", "")?.replace("-", "")
                    } else {
                        number = ""
                    }
                    for (scimboModel: FriendModel in savedScimboContacts) {
                        if ((number.equals(scimboModel.numberInDevice, ignoreCase = true)
                                    || number.equals(scimboModel.msisdn, ignoreCase = true))
                        ) {
                            contactScimboId = scimboModel._id
                            break
                        }
                    }
                }

                // Check whether contact is current user details
                val phNo =
                    number!!.replace(" ", "").replace("(", "").replace(")", "").replace("-", "")
                if (phNo.equals(sessionManager!!.phoneNumberOfCurrentUser, ignoreCase = true) ||
                    phNo.equals(sessionManager!!.userMobileNoWithoutCountryCode, ignoreCase = true)
                ) {
                    contactScimboId = mCurrentUserId
                }
                sendContactMessage("", contactScimboId, Uname, number, ContactString)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            try {
                var pathlist: ArrayList<Imagepath_caption>? = ArrayList()
                val bundle = data!!.extras
                try {
                    pathlist = bundle!!.getSerializable("pathlist") as ArrayList<Imagepath_caption>?
                    val path = pathlist!![0].getPath()
                    val caption = pathlist[0].getCaption()
                    sendImageChatMessage(path, caption, false)
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if ((requestCode == RESULT_WALLPAPER) && (resultCode == Activity.RESULT_OK) && (null != data)) {
            try {
                val selectedImageUri = data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursor =
                    contentResolver.query((selectedImageUri)!!, filePathColumn, null, null, null)
                cursor!!.moveToFirst()
                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                imgDecodableString = cursor.getString(columnIndex)
                if (imgDecodableString == null) {
                    imgDecodableString = getRealFilePath(data)
                }
                cursor.close()
                session!!.putgalleryPrefs(imgDecodableString)
                val bitmap = BitmapFactory.decodeFile(imgDecodableString)
                background!!.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (requestCode == EXIT_GROUP_REQUEST_CODE) { // Finish activity when exit group by user
            if (resultCode == Activity.RESULT_OK && data != null) {
                val isExitFromGroup = data.getBooleanExtra("exitFromGroup", false)
                val ismuteGroupchange = data.getBooleanExtra("ismutechange", false)
                val isgroupemty = data.getBooleanExtra("isgroupempty", false)
                if (isExitFromGroup) {
                    finish()
                }
                if (ismuteGroupchange) {
                    finish()
                }
                if (isgroupemty) {
                    rlSend!!.visibility = View.GONE
                    RelativeLayout_group_delete!!.visibility = View.VISIBLE
                }
            }
        } else if (requestCode == MUTE_ACTIVITY) { // Finish activity when exit group by user
            if (resultCode == Activity.RESULT_OK && data != null) {
                val ismutechange = data.getBooleanExtra("muteactivity", false)
                if (ismutechange) {
                    finish()
                }
            }
        } else if (requestCode == REQUEST_CODE_FORWARD_MSG) {
            if (resultCode == Activity.RESULT_OK) {
                val isMultiForward = data!!.getBooleanExtra("MultiForward", false)
                if (isMultiForward) {
                    reloadAdapter()
                    showUnSelectedActions()
                }
            } else {
                if (selectedChatItems != null && selectedChatItems!!.size > 0) {
                    for (msgItem: MessageItemChat? in selectedChatItems!!) {
                        val index = mChatData!!.indexOf(msgItem)
                        if (index > -1) {
                            mChatData!!.get(index)!!.isSelected = true
                        }
                    }
                    Log.e(TAG, "REQUEST_CODE_FORWARD_MSG: notifyDataSetChanged")
                    mAdapter!!.notifyDataSetChanged()
                } else {
                    showUnSelectedActions()
                }
            }
        } else if (requestCode == ADD_CONTACT && resultCode == Activity.RESULT_OK) {
            mContactSaved = true
            runOnUiThread {
                llAddBlockContact!!.visibility = View.GONE
                ScimboContactsService.savedNumber = user_name!!.text.toString()
                ScimboContactsService.bindContactService(this@ChatPageActivity, false)
                ScimboContactsService.setBroadCastSavedName { name ->
                    user_name!!.post {
                        user_name!!.text = name
                        SharedPreference.getInstance().save(context, "userName", name)
                        llAddBlockContact!!.visibility = View.GONE
                    }
                }
            }
            //      llAddBlockContact.setVisibility(View.GONE);
        } else if (requestCode == AppConstants.RequestCodes.REQUEST_CHANGE_LANGUAGE.code) {
            if (data == null || resultCode != Activity.RESULT_OK) return
            val intentKeyLanguage =
                data.getStringExtra(AppConstants.IntentKeys.EXTRA_KEY_LANGUAGE.value)
            val langCode =
                data.getStringExtra(AppConstants.IntentKeys.EXTRA_KEY_LANGUAGE_CODE.value)
            val key = MyChatUtils.getLanguageKey(this, languageId)
            val keyCode = MyChatUtils.getLanguageKey(
                this, AppConstants.SPKeys.OUT_GOING_LANGUAGE_CODE.value,
                languageId
            )
            StorageUtility.saveDataInPreferences(this@ChatPageActivity, key, intentKeyLanguage)
            StorageUtility.saveDataInPreferences(this@ChatPageActivity, keyCode, langCode)
            setTranslateIcon()
            return
        }
    }

    private fun setTranslateIcon() {
        val intentKeyLanguage = "English"
        val selectedLanguage = StorageUtility.getDataFromPreferences(
            this,
            MyChatUtils.getLanguageKey(this, languageId),
            intentKeyLanguage
        )
        if (selectedLanguage != intentKeyLanguage) translateIcon!!.setImageResource(R.drawable.translate) else translateIcon!!.setImageResource(
            R.drawable.translate_disable
        )
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private fun getRealFilePath(data: Intent): String {
        val selectedImage = data.data
        val wholeID = DocumentsContract.getDocumentId(selectedImage)
        // Split at colon, use second item in the array
        val id = wholeID.split(":").toTypedArray()[1]
        val column = arrayOf(MediaStore.Images.Media.DATA)
        // where id is equal to
        val sel = MediaStore.Images.Media._ID + "=?"
        val cursor = contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            column, sel, arrayOf(id), null
        )
        var filePath = ""
        var columnIndex = 0
        if (cursor != null) {
            columnIndex = cursor.getColumnIndex(column[0])
        }
        if (cursor != null && cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex)
        }
        cursor!!.close()
        return filePath
    }

    @Synchronized
    private fun reloadAdapter() {
        val dbItems = messageDatabase!!.selectAllMessagesWithLimit(
            docId, chatType,
            "", MessageDbController.MESSAGE_SELECTION_LIMIT
        )
        Collections.sort(dbItems, msgComparator)
        mChatData!!.clear()
        mChatData!!.addAll(dbItems)
        sendAllAcksToServer(dbItems)
        notifyDatasetChange()
    }

    //-------------------------------------Send Image Chat-----------------------------------------
    fun sendImageChatMessage(imgPath: String?, caption: String, isRetry: Boolean) {
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            DisplayAlert(
                resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                    R.string.to_send_message
                )
            )
        } else {
            var isTagapplied = 0
            val names = ArrayList<String>()
            val userID = ArrayList<String>()
            if (imgPath != null) {
                var data_at = caption
                if (ImagecaptionActivity.at_memberlist != null && ImagecaptionActivity.at_memberlist.size >= 0) for (groupMembersPojo: GroupMembersPojo in ImagecaptionActivity.at_memberlist) {
                    val userName = "@" + groupMembersPojo.contactName
                    names.add(userName)
                    userID.add(groupMembersPojo.userId)
                    if (data_at.contains(userName)) {
                        data_at = data_at.replace(
                            userName,
                            TextMessage.TAG_KEY + groupMembersPojo.userId + TextMessage.TAG_KEY
                        )
                        isTagapplied = 1
                    }
                }
                val message =
                    MessageFactory.getMessage(MessageFactory.picture, this) as PictureMessage
                var item: MessageItemChat? = null
                var canSent = false
                val options = BitmapFactory.Options()
                options.inJustDecodeBounds = true
                BitmapFactory.decodeFile(imgPath, options)
                val imageHeight = options.outHeight
                val imageWidth = options.outWidth
                if (isGroupChat) {
                    if (enableGroupChat) {
                        canSent = true
                        message.getGroupMessageObject(to, imgPath, user_name!!.text.toString())
                        item = message.createMessageItem(
                            true, caption, imgPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            mReceiverId, user_name!!.text.toString(), imageWidth, imageHeight
                        )
                        item.groupName = user_name!!.text.toString()
                    } else {
                        Toast.makeText(
                            this@ChatPageActivity,
                            resources.getString(R.string.you_are_not_member_of_this_group),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    canSent = true
                    message.getMessageObject(to, imgPath, false)
                    item = message.createMessageItem(
                        true, caption, imgPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                        mReceiverId, user_name!!.text.toString(), imageWidth, imageHeight
                    )
                    item.senderMsisdn = receiverMsisdn
                }
                if (isTagapplied == 1) {
                    item!!.isTagapplied = true
                    item.setArrayTagnames = names
                    item.editTextmsg = caption
                    item.userId_tag = userID
                }
                if (item != null) {
                    item.caption = caption
                }
                if (canSent) {
                    if (!isRetry) {
                        messageDatabase!!.updateChatMessage(item, chatType)
                        if (!isAlreadyExist(item)) mChatData!!.add(item)
                        notifyDatasetChange()
                    }
                    val fileExtension = FileUploadDownloadManager.getFileExtnFromPath(imgPath)
                    val imgName = item!!.messageId + fileExtension
                    val docId: String
                    if (isGroupChat) {
                        docId = mCurrentUserId + "-" + to + "-g"
                    } else {
                        docId = mCurrentUserId + "-" + to
                    }
                    val uploadObj = message.createImageUploadObject(
                        item.messageId, docId, imgName, imgPath,
                        user_name!!.text.toString(), data_at, chatType, false
                    ) as JSONObject
                    uploadDownloadManager!!.uploadFile(EventBus.getDefault(), uploadObj)
                    if (ImagecaptionActivity.at_memberlist != null) {
                        ImagecaptionActivity.at_memberlist.clear()
                    }
                }
            }
        }
    }

    fun sendVideoChatMessage(videoPath: String?, caption: String, isRetry: Boolean) {
        if (ImagecaptionActivity.CompressFilePath != null && !ImagecaptionActivity.CompressFilePath.equals(
                "",
                ignoreCase = true
            )
        ) {

            // videoPath=ImagecaptionActivity.CompressFilePath;
            MyLog.e(
                "CheckEventConnection",
                "CompressVideoPathSetChatActivity$videoPath"
            )
        }
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            DisplayAlert(
                resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                    R.string.to_send_message
                )
            )
        } else {
            var isTagapplied = 0
            val names = ArrayList<String>()
            val userID = ArrayList<String>()
            if (videoPath != null) {
                var data_at = caption
                if (ImagecaptionActivity.at_memberlist != null && ImagecaptionActivity.at_memberlist.size >= 0) for (groupMembersPojo: GroupMembersPojo in ImagecaptionActivity.at_memberlist) {
                    val userName = "@" + groupMembersPojo.contactName
                    names.add(userName)
                    userID.add(groupMembersPojo.userId)
                    if (data_at.contains(userName)) {
                        data_at = data_at.replace(
                            userName,
                            TextMessage.TAG_KEY + groupMembersPojo.userId + TextMessage.TAG_KEY
                        )
                        isTagapplied = 1
                    }
                }
                val message = MessageFactory.getMessage(MessageFactory.video, this) as VideoMessage
                var item: MessageItemChat? = null
                var canSent = false
                if (isGroupChat) {
                    if (enableGroupChat) {
                        canSent = true
                        message.getGroupMessageObject(to, user_name!!.text.toString(), caption)
                        item = message.createMessageItem(
                            true, videoPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            mReceiverId, user_name!!.text.toString(), caption
                        )
                        item.groupName = user_name!!.text.toString()
                    } else {
                        Toast.makeText(
                            this@ChatPageActivity,
                            resources.getString(R.string.you_are_not_member_of_this_group),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    canSent = true
                    message.getMessageObject(to, videoPath, false)
                    item = message.createMessageItem(
                        true, videoPath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                        mReceiverId, user_name!!.text.toString(), caption
                    )
                    item.senderMsisdn = receiverMsisdn
                }
                if (isTagapplied == 1) {
                    item!!.isTagapplied = true
                    item.setArrayTagnames = names
                    item.editTextmsg = caption
                    item.userId_tag = userID
                }
                if (canSent) {
                    try {
                        val thumbBmp = ThumbnailUtils.createVideoThumbnail(
                            videoPath,
                            MediaStore.Video.Thumbnails.MICRO_KIND
                        )
                        val out = ByteArrayOutputStream()
                        thumbBmp!!.compress(Bitmap.CompressFormat.PNG, 10, out)
                        val thumbArray = out.toByteArray()
                        try {
                            out.close()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        val thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT)
                        if (thumbData != null) {
                            item!!.thumbnailData = thumbData
                            item.thumb_bitmap_image = thumbBmp
                        }
                        val bitmapstring = thumbBmp.toString()
                        item!!.videobitmap = bitmapstring
                        item.caption = caption
                    } catch (e: Exception) {
                    }
                    if (!isRetry) {
                        messageDatabase!!.updateChatMessage(item, chatType)
                        if (!isAlreadyExist(item)) mChatData!!.add(item)
                        notifyDatasetChange()
                    }
                    val fileExtension = FileUploadDownloadManager.getFileExtnFromPath(videoPath)
                    val videoName = item!!.messageId + fileExtension
                    val docId: String
                    if (isGroupChat) {
                        docId = mCurrentUserId + "-" + to + "-g"
                    } else {
                        docId = mCurrentUserId + "-" + to
                    }
                    val uploadObj = message.createVideoUploadObject(
                        item.messageId, docId,
                        videoName, videoPath, user_name!!.text.toString(), data_at, chatType, false
                    ) as JSONObject
                    uploadDownloadManager!!.uploadFile(EventBus.getDefault(), uploadObj)
                    if (ImagecaptionActivity.at_memberlist != null) {
                        ImagecaptionActivity.at_memberlist.clear()
                    }
                }
            }
        }
    }

    //----------------------------------------Send Video Chat--------------------------------------------------------
    fun sendAudioMessage(filePath: String?, duration: String?, audioFrom: Int, retry: Boolean) {
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            DisplayAlert(
                resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                    R.string.to_send_message
                )
            )
        } else {
            val message = MessageFactory.getMessage(MessageFactory.audio, this) as AudioMessage
            var item: MessageItemChat? = null
            var canSent = false
            if (isGroupChat) {
                if (enableGroupChat) {
                    canSent = true
                    message.getGroupMessageObject(to, filePath, user_name!!.text.toString())
                    item = message.createMessageItem(
                        true, filePath, duration, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                        mReceiverId, user_name!!.text.toString(), audioFrom
                    )
                    item.groupName = user_name!!.text.toString()
                } else {
                    Toast.makeText(
                        this@ChatPageActivity,
                        resources.getString(R.string.you_are_not_member_of_this_group),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                canSent = true
                message.getMessageObject(to, filePath, false)
                item = message.createMessageItem(
                    true, filePath, duration, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                    mReceiverId, user_name!!.text.toString(), audioFrom
                )
                item.senderMsisdn = receiverMsisdn
                item.setaudiotype(audioFrom)
            }
            if (canSent) {
                if (!retry) {
                    messageDatabase!!.updateChatMessage(item, chatType)
                    if (!isAlreadyExist(item)) mChatData!!.add(item)
                    notifyDatasetChange()
                }
                val fileExtension = FileUploadDownloadManager.getFileExtnFromPath(filePath)
                val audioName = item!!.messageId + fileExtension
                val docId: String
                if (isGroupChat) {
                    docId = mCurrentUserId + "-" + to + "-g"
                } else {
                    docId = mCurrentUserId + "-" + to
                }
                val uploadObj = message.createAudioUploadObject(
                    item.messageId,
                    docId,
                    audioName,
                    filePath,
                    duration,
                    user_name!!.text.toString(),
                    audioFrom,
                    chatType,
                    false
                ) as JSONObject
                uploadDownloadManager!!.uploadFile(EventBus.getDefault(), uploadObj)
            }
        }
    }

    //-----------------------------------------------------Send Audio Chat-------------------------------------------------------
    fun sendDocumentMessage(filePath: String?, Retry: Boolean) {
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            DisplayAlert(
                resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                    R.string.to_send_message
                )
            )
        } else {
            val message =
                MessageFactory.getMessage(MessageFactory.document, this) as DocumentMessage
            var item: MessageItemChat? = null
            var canSent = false
            if (isGroupChat) {
                if (enableGroupChat) {
                    canSent = true
                    message.getGroupMessageObject(to, filePath, user_name!!.text.toString())
                    item = message.createMessageItem(
                        true, filePath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                        mReceiverId, user_name!!.text.toString()
                    )
                    item.groupName = user_name!!.text.toString()
                } else {
                    Toast.makeText(
                        this@ChatPageActivity,
                        resources.getString(R.string.you_are_not_member_of_this_group),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                canSent = true
                message.getMessageObject(to, filePath, false)
                item = message.createMessageItem(
                    true, filePath, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                    mReceiverId, user_name!!.text.toString()
                )
                item.senderMsisdn = receiverMsisdn
            }
            if (canSent) {
                if (!Retry) {
                    messageDatabase!!.updateChatMessage(item, chatType)
                    if (!isAlreadyExist(item)) mChatData!!.add(item)
                    notifyDatasetChange()
                }
                val fileExtension = FileUploadDownloadManager.getFileExtnFromPath(filePath)
                val docName = item!!.messageId + fileExtension
                val docId: String
                if (isGroupChat) {
                    docId = mCurrentUserId + "-" + to + "-g"
                } else {
                    docId = mCurrentUserId + "-" + to
                }
                val uploadObj = message.createDocUploadObject(
                    item.messageId, docId,
                    docName, filePath, user_name!!.text.toString(), chatType, false
                ) as JSONObject
                uploadDownloadManager!!.uploadFile(EventBus.getDefault(), uploadObj)
            }
        }
    }

    //-------------------------------------------------Send Document Chat---------------------------------------------------------
    fun sendLocationMessage(
        latlng: LatLng, addressName: String?,
        address: String?
    ) {
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            DisplayAlert(
                resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                    R.string.to_send_message
                )
            )
        } else {
            //            if (getResources().getBoolean(R.bool.is_base)) {
//
//            }
            val ai: ApplicationInfo = applicationContext.packageManager
                .getApplicationInfo(applicationContext.packageName, PackageManager.GET_META_DATA)
            val value = ai.metaData["apiKey"]
            val apiKey = value.toString()
            val thumbUrl =
                ("https://maps.googleapis.com/maps/api/staticmap?center=" + latlng.latitude + "," + latlng.longitude
                        + "&zoom=10&size=320x320&maptype=roadmap&markers=color:blue%7Clabel:N%7C" + latlng.latitude
                        + "," + latlng.longitude + "&key=" + apiKey)


            //   https:
//maps.googleapis.com/maps/api/staticmap?center=Chennai&zoom=13&size=600x300&key=AIzaSyCR7bhMU4qZncunudEsvt6M3riu0P5_rEs
            initProgress(getString(R.string.loading_address), true)
            showProgressDialog()
            val imageLoader = CoreController.getInstance().imageLoader
            imageLoader[thumbUrl, object : ImageLoader.ImageListener {
                override fun onResponse(imageContainer: ImageLoader.ImageContainer, b: Boolean) {
                    val bitmap = imageContainer.bitmap
                    //use bitmap
                    if (bitmap != null) {
                        hideProgressDialog()
                        val newBmp = Bitmap.createScaledBitmap(bitmap, 320, 320, true)
                        val out = ByteArrayOutputStream()
                        newBmp.compress(Bitmap.CompressFormat.JPEG, 100, out)
                        val thumbArray = out.toByteArray()
                        try {
                            out.close()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        var thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT)
                        if (thumbData != null) {
                            thumbData = thumbData.replace("\n", "")
                            if (!thumbData.startsWith("data:image/jpeg;base64,")) {
                                thumbData = "data:image/jpeg;base64,$thumbData"
                            }
                            val point = latlng.latitude.toString() + "," + latlng.longitude
                            val url = ("https://maps.google.com/maps?q=" + point
                                    + " (" + addressName + ")&amp;z=15&amp;hl=en")

                            //point= addressName;
                            val messageEvent = SendMessageEvent()
                            val message = MessageFactory.getMessage(
                                MessageFactory.location,
                                this@ChatPageActivity
                            ) as LocationMessage
                            var msgObj: JSONObject?
                            val receiverName = user_name!!.text.toString()
                            if (isGroupChat) {
                                messageEvent.eventName = SocketManager.EVENT_GROUP
                                msgObj = message.getGroupMessageObject(
                                    to,
                                    point,
                                    receiverName
                                ) as JSONObject
                            } else {
                                msgObj = message.getMessageObject(to, point, false) as JSONObject
                                messageEvent.eventName = SocketManager.EVENT_MESSAGE
                            }
                            val item = message.createMessageItem(
                                true,
                                point,
                                MessageFactory.DELIVERY_STATUS_NOT_SENT,
                                mReceiverId,
                                receiverName,
                                addressName,
                                address,
                                url,
                                thumbUrl,
                                thumbData
                            )
                            msgObj = message.getLocationObject(
                                msgObj,
                                addressName,
                                address,
                                url,
                                thumbUrl,
                                thumbData
                            ) as JSONObject
                            messageEvent.messageObject = msgObj
                            if (isGroupChat) {
                                if (enableGroupChat) {
                                    item.groupName = user_name!!.text.toString()
                                    messageDatabase!!.updateChatMessage(item, chatType)
                                    if (!isAlreadyExist(item)) mChatData!!.add(item)
                                    EventBus.getDefault().post(messageEvent)
                                    notifyDatasetChange()
                                } else {
                                    Toast.makeText(
                                        this@ChatPageActivity,
                                        resources.getString(R.string.you_are_not_member_of_this_group),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            } else {
                                item.senderMsisdn = receiverMsisdn
                                item.senderName = user_name!!.text.toString()
                                messageDatabase!!.updateChatMessage(item, chatType)
                                if (!isAlreadyExist(item)) mChatData!!.add(item)
                                EventBus.getDefault().post(messageEvent)
                                notifyDatasetChange()
                            }
                        }
                    }
                }

                override fun onErrorResponse(volleyError: VolleyError) {
                    hideProgressDialog()
                    var json: String? = null
                    val response = volleyError.networkResponse
                    if (response != null && response.data != null) {
                        when (response.statusCode) {
                            403 -> {
                                //     MyLog.e(TAG,"statuscode"+response.statusCode);
                                json = String(response.data)
                                Toast.makeText(
                                    this@ChatPageActivity,
                                    getString(R.string.enablepayment),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    } else {
                        //  Log.i("Volley Error", error.toString());
                        /*Toast.makeText(getApplicationContext(),
                                        "Error: " + error.toString(),
                                        Toast.LENGTH_LONG).show();*/
                        Toast.makeText(
                            this@ChatPageActivity,
                            getString(R.string.networkerror),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }]
        }
    }

    /*   public void sendLocationMessage(final LatLng latlng, final String addressName, final String address, final String thumbUrl) {


        if (contactDB_sqlite.getBlockedStatus(mReceiverId, false).equals("1")) {
            DisplayAlert("Unblock " + mReceiverName + " to send message?");
        } else {
            showProgressDialog();

            File image = new File(thumbUrl);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(),bmOptions);

            //use bitmap
            if (bitmap != null) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                byte[] thumbArray = out.toByteArray();
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String thumbData = Base64.encodeToString(thumbArray, Base64.DEFAULT);
                if (thumbData != null) {
                    thumbData = thumbData.replace("\n", "");
                    if (!thumbData.startsWith("data:image/jpeg;base64,")) {
                        thumbData = "data:image/jpeg;base64," + thumbData;
                    }

                    String point = latlng.latitude + "," + latlng.longitude;

                    String url = "https://maps.google.com/maps?q=" + point
                            + " (" + addressName + ")&amp;z=15&amp;hl=en";

                    //point= addressName;

                    SendMessageEvent messageEvent = new SendMessageEvent();
                    LocationMessage message = (LocationMessage) MessageFactory.getMessage(MessageFactory.location, ChatPageActivity.this);

                    JSONObject msgObj;
                    String receiverName = user_name.getText().toString();
                    if (isGroupChat) {
                        messageEvent.setEventName(SocketManager.EVENT_GROUP);
                        msgObj = (JSONObject) message.getGroupMessageObject(to, point, receiverName);
                    } else {
                        msgObj = (JSONObject) message.getMessageObject(to, point, false);
                        messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                    }

                    MessageItemChat item = message.createMessageItem(true, point, MessageFactory.DELIVERY_STATUS_NOT_SENT,
                            mReceiverId, receiverName, addressName, address, url, thumbUrl, thumbData);
                    msgObj = (JSONObject) message.getLocationObject(msgObj, addressName, address, url, thumbUrl, thumbData);
                    messageEvent.setMessageObject(msgObj);

                    if (isGroupChat) {
                        if (enableGroupChat) {
                            item.setGroupName(user_name.getText().toString());
                            db.updateChatMessage(item, chatType);
                            if (!isAlreadyExist(item))
                                mChatData.add(item);
                            EventBus.getDefault().post(messageEvent);

                            notifyDatasetChange();
                        } else {
                            Toast.makeText(ChatPageActivity.this, "You are not a member in this group", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        item.setSenderMsisdn(receiverMsisdn);
                        item.setSenderName(user_name.getText().toString());
                        db.updateChatMessage(item, chatType);
                        if (!isAlreadyExist(item))
                            mChatData.add(item);
                        EventBus.getDefault().post(messageEvent);
                        notifyDatasetChange();
                    }
                }
            }
//                }
//
//                @Override
//                public void onErrorResponse(VolleyError volleyError) {
//                    hideProgressDialog();
//                    String json = null;
//
//                    NetworkResponse response = volleyError.networkResponse;
//                    if (response != null && response.data != null) {
//                        switch (response.statusCode) {
//                            case 403:
//                                //     MyLog.e(TAG,"statuscode"+response.statusCode);
//
//                                json = new String(response.data);
//                                Toast.makeText(ChatPageActivity.this, getString(R.string.enablepayment), Toast.LENGTH_SHORT).show();
//
//                              / *  json = trimMessage(json, "errors");
//                                if (json != null) displayMessage(json,context);*/
    //                                break;
    //                        }
    //
    //
    //                    } else {
    //                        //  Log.i("Volley Error", error.toString());
    //                        /*Toast.makeText(getApplicationContext(),
    //                                "Error: " + error.toString(),
    //                                Toast.LENGTH_LONG).show();*/
    //                        Toast.makeText(ChatPageActivity.this, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();
    //
    //
    //                    }
    //                }
    //            });
    /*  }


    }*/
    //Somewhere that has access to a context
    fun displayMessage(toastString: String?, mContext: Context?) {
        Toast.makeText(mContext, toastString, Toast.LENGTH_LONG).show()
    }

    fun trimMessage(json: String?, key: String?): String? {
        var trimmedString: String? = null
        try {
            val obj = JSONObject(json)
            trimmedString = obj.getString(key)
        } catch (e: JSONException) {
            e.printStackTrace()
            return null
        }
        return trimmedString
    }

    //--------------------------------------------------Send Location Chat-----------------------------------------------------------------------
    fun sendContactMessage(
        data: String?,
        contactScimboId: String?,
        contactName: String?,
        contactNumber: String?,
        contactDetail: String?
    ) {
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            DisplayAlert(
                resources.getString(R.string.unblock) + " " + mReceiverName + " " + resources.getString(
                    R.string.to_send_message
                )
            )
        } else {
            val messageEvent = SendMessageEvent()
            val message = MessageFactory.getMessage(MessageFactory.contact, this) as ContactMessage
            val msgObj: JSONObject
            if (isGroupChat) {
                messageEvent.eventName = SocketManager.EVENT_GROUP
                msgObj = message.getGroupMessageObject(
                    to,
                    data,
                    user_name!!.text.toString(),
                    contactScimboId,
                    contactName,
                    contactNumber,
                    contactDetail
                ) as JSONObject
            } else {
                messageEvent.eventName = SocketManager.EVENT_MESSAGE
                msgObj = message.getMessageObject(
                    to,
                    data,
                    contactScimboId,
                    contactName,
                    contactNumber,
                    contactDetail,
                    false
                ) as JSONObject
            }
            messageEvent.messageObject = msgObj
            val item = message.createMessageItem(
                true,
                data,
                MessageFactory.DELIVERY_STATUS_NOT_SENT,
                mReceiverId,
                user_name!!.text.toString(),
                contactName,
                contactNumber,
                contactScimboId,
                contactDetail
            )
            item.groupName = user_name!!.text.toString()
            item.senderMsisdn = receiverMsisdn
            item.contactScimboId = contactScimboId
            /*  String image = ("uploads/users/").concat(item.get_id()).concat(".jpg");
        String path =  Constants.SOCKET_IP.concat(image);*/item.avatarImageUrl = receiverAvatar
            item.detailedContacts = contactDetail
            if (isGroupChat && enableGroupChat) {
                messageDatabase!!.updateChatMessage(item, chatType)
                if (!isAlreadyExist(item)) mChatData!!.add(item)
                notifyDatasetChange()
                EventBus.getDefault().post(messageEvent)
            } else if (!isGroupChat) {
                messageDatabase!!.updateChatMessage(item, chatType)
                if (!isAlreadyExist(item)) mChatData!!.add(item)
                notifyDatasetChange()
                EventBus.getDefault().post(messageEvent)
            }
        }
    }

    //-------------------------------------------------Send Contact Message--------------------------------------------------------
    private fun isAlreadyExist(messageItemChat: MessageItemChat?): Boolean {
        val msgId = messageItemChat!!.messageId
        if (messageIds!!.contains(msgId)) {
            return true
        }
        messageIds!!.add(msgId)
        return false
    }

    private fun notifyDatasetChange() {
        setEncryptionMsg()
        chat_list!!.post {
            mAdapter!!.setIsGroupChat(isGroupChat)
            Log.e(
                TAG,
                "notifyDatasetChange: notifyDataSetChanged"
            )
            mAdapter!!.notifyDataSetChanged()
            if (mChatData!!.size > 0) chat_list!!.layoutManager!!.scrollToPosition(mChatData!!.size - 1)
            iBtnScroll!!.visibility = View.GONE
            unreadcount!!.visibility = View.GONE
            mListviewScrollBottom = false
        }
    }

    private fun DisplayAlert(txt: String) {
        val dialog = CustomAlertDialog()
        dialog.setMessage(txt)
        dialog.setNegativeButtonText(resources.getString(R.string.cancel))
        dialog.setPositiveButtonText(resources.getString(R.string.unblock))
        dialog.isCancelable = false
        dialog.setCustomDialogCloseListener(object : CustomAlertDialog.OnCustomDialogCloseListener {
            override fun onPositiveButtonClick() {
                putBlockUser()
                dialog.dismiss()
            }

            override fun onNegativeButtonClick() {
                dialog.dismiss()
            }
        })
        dialog.show(supportFragmentManager, "Unblock a person")
    }

    private fun putBlockUser() {
        BlockUserUtils.changeUserBlockedStatus(
            this@ChatPageActivity, EventBus.getDefault(),
            mCurrentUserId, mReceiverId, false
        )
        /*        BlockUserUtils.changeUserBlockedStatus(ChatPageActivity.this, EventBus.getDefault(),
                mCurrentUserId, mReceiverId, true);*/
    }

    private suspend fun loadFileUploaded(`object`: JSONObject) {
        try {
            val uploadedSize = `object`.getInt("UploadedSize")
            val totalSize = `object`.getInt("size")
            val msgId = `object`.getString("id")
            val progress = (uploadedSize * 100) / totalSize
            val Upload_status = messageDatabase!!.fileuploadStatusget(msgId)
            MyLog.e(
                "CheckEventConnection",
                "UPLOAD STATUS CHAT CLASS$Upload_status"
            )
            if (mChatData == null) return
            if (Upload_status.equals("uploading", ignoreCase = true)) {
                for (msgItem: MessageItemChat? in mChatData!!) {
                    if (msgItem!!.messageId.equals(msgId, ignoreCase = true)) {
                        val index = mChatData!!.indexOf(msgItem)
                        if (progress >= mChatData!![index]!!.uploadDownloadProgress) {
                            sharedprf_video_uploadprogress!!.setfileuploadingprogress(
                                progress,
                                msgId
                            )
                            if (progress == 0) {
                                runOnUiThread {
                                    mChatData!!.get(index)!!.uploadDownloadProgress = 2
                                    Log.e(
                                        TAG,
                                        "loadFileUploaded: notifyDataSetChanged"
                                    )
                                    if (mAdapter != null) mAdapter!!.notifyDataSetChanged()
                                }
                            } else {
                                runOnUiThread {
                                    if (mChatData != null) mChatData!!.get(index)!!.uploadDownloadProgress =
                                        progress
                                    Log.e(
                                        TAG,
                                        "loadFileUploaded: notifyDataSetChanged"
                                    )
                                    if (mAdapter != null) mAdapter!!.notifyDataSetChanged()
                                }
                            }
                            runOnUiThread {
                                progressglobal =
                                    progress
                            }
                        }
                        break
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun loadPrivacySetting(event: ReceviceMessageEvent) {
        val `object` = event.objectsArray
        try {
            val jsonObject = JSONObject(`object`[0].toString())
            val from = jsonObject.getString("from")
            val status = jsonObject["status"] as String
            val lastseen = jsonObject["last_seen"].toString()
            val profile = jsonObject["profile_photo"].toString()
            val contactUserList = jsonObject.getJSONArray("contactUserList")
            if (from.equals(mReceiverId, ignoreCase = true)) {
                var iscontact = false
                if (contactUserList != null) {
                    iscontact = contactUserList.toString().contains((mCurrentUserId)!!)
                }
                if (!isGroupChat) {
                    if (lastseen.equals("nobody", ignoreCase = true)) {
                        canShowLastSeen = false
                        if (!status_textview!!.text.toString()
                                .equals(resources.getString(R.string.online), ignoreCase = true)
                        ) {
                            status_textview!!.visibility = View.GONE
                        }
                    } else if (lastseen.equals("everyone", ignoreCase = true)) {
                        status_textview!!.visibility = View.GONE
                        canShowLastSeen = true
                    } else if (lastseen.equals("mycontacts", ignoreCase = true) && iscontact) {
                        canShowLastSeen = true
                        status_textview!!.visibility = View.GONE
                    } else {
                        canShowLastSeen = false
                        if (!status_textview!!.text.toString()
                                .equals(resources.getString(R.string.online), ignoreCase = true)
                        ) {
                            status_textview!!.visibility = View.GONE
                        }
                    }
                    if ((contactDB_sqlite!!.getBlockedMineStatus(
                            to,
                            false
                        ) == ContactDB_Sqlite.BLOCKED_STATUS)
                    ) {
                        canShowLastSeen = false
                        status_textview!!.visibility = View.GONE
                    }
                }
                profilepicupdation()
            }
        } catch (e: Exception) {
        }
    }

    private fun profilepicupdation() {
        Log.d(TAG, "profilepicupdation:11")
        //  if (receiverAvatar != null) {
        if (!isGroupChat) {
            //     getcontactname.configCircleProfilepic(user_profile_image, to, true, true, R.drawable.ic_placeholder_black);
            AppUtils.loadImage(
                context,
                AppUtils.getValidProfilePath(getcontactname!!.getAvatarUrl(to)),
                user_profile_image,
                0,
                R.drawable.ic_placeholder_black
            )
        } else {
            groupInfoSession = GroupInfoSession(this)
            mCurrentUserId = SessionManager.getInstance(this).currentUserID
            Log.d(TAG, "profilepicupdation: to " + to)
            Log.d(
                TAG,
                "profilepicupdation: mCurrentUserId $mCurrentUserId"
            )
            val infoPojo = groupInfoSession!!.getGroupInfo(mCurrentUserId + "-" + to + "-g")
                ?: return
            receiverAvatar = infoPojo.avatarPath
            receiverAvatar = AppUtils.getValidProfilePath(receiverAvatar)
            Log.d(TAG, "profilepicupdation: " + receiverAvatar)
            if (!AppUtils.isEmptyImage(receiverAvatar)) {
                AppUtils.loadImage(
                    this,
                    receiverAvatar,
                    user_profile_image,
                    100,
                    R.drawable.ic_placeholder_black
                )
            } else {
                user_profile_image!!.setImageResource(R.drawable.ic_placeholder_black)
            }
        }
        //  }
    }

    //----------------------------------------------UnFriend Event Method------------------------------------------------------
    private fun unFriendEventHandling(data: String) {
        var removedFriendID = "-1"
        try {
            val `object` = JSONObject(data)
            val err = `object`.getString("err")
            if ((err == "0")) {
                val gsonBuilder = GsonBuilder()
                val gson = gsonBuilder.create()
                val friendModel = gson.fromJson(
                    `object`.getString("friends"),
                    FriendModel::class.java
                )
                if (friendModel != null) removedFriendID = friendModel._id
                if (removedFriendID.equals(to, ignoreCase = true)) finish()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //----------------------------------------------Load Settings Details------------------------------------------------------
    private fun load_clear_chat(data: String) {
        try {
            val `object` = JSONObject(data)
            MyLog.e(
                TAG,
                "loadClearChat$`object`"
            )
            try {
                val from = `object`.getString("from")
                val convId = `object`.getString("convId")
                val type = `object`.getString("type")
                if (from.equals(mCurrentUserId, ignoreCase = true) && convId.equals(
                        mConvId,
                        ignoreCase = true
                    )
                ) {
                    val star_status: Int
                    var starred = false
                    if (`object`.has("star_status")) {
                        star_status = `object`.getInt("star_status")
                        starred = star_status != 0
                    }
                    if (type.equals(MessageFactory.CHAT_TYPE_GROUP, ignoreCase = true)) {
                        if (starred) {
                            val value = ArrayList<MessageItemChat?>()
                            for (i in mChatData!!.indices) {
                                if (mChatData!![i]!!.starredStatus.equals(
                                        MessageFactory.MESSAGE_UN_STARRED,
                                        ignoreCase = true
                                    )
                                ) {
                                    value.add(mChatData!![i])
                                }
                            }
                            for (i in value.indices) {
                                if (value[i]!!.isMediaPlaying) {
                                    val chatIndex = mChatData!!.indexOf(value[i])
                                    if (chatIndex > -1 && mChatData!![chatIndex]!!.isMediaPlaying) {
                                        mAdapter!!.stopAudioOnMessageDelete(chatIndex)
                                    }
                                }
                                mChatData!!.remove(value[i])
                            }
                        } else {
                            mAdapter!!.stopAudioOnClearChat()
                            mChatData!!.clear()
                        }
                    } else {
                        if (starred) {
                            val value = ArrayList<MessageItemChat?>()
                            for (i in mChatData!!.indices) {
                                if (mChatData!![i]!!.starredStatus.equals(
                                        MessageFactory.MESSAGE_UN_STARRED,
                                        ignoreCase = true
                                    )
                                ) {
                                    value.add(mChatData!![i])
                                }
                            }
                            for (i in value.indices) {
                                if (value[i]!!.isMediaPlaying) {
                                    val chatIndex = mChatData!!.indexOf(value[i])
                                    if (chatIndex > -1 && mChatData!![chatIndex]!!.isMediaPlaying) {
                                        mAdapter!!.stopAudioOnMessageDelete(chatIndex)
                                    }
                                }
                                mChatData!!.remove(value[i])
                            }
                        } else {
                            mChatData!!.clear()
                        }
                    }
                    Log.e(TAG, "load_clear_chat: notifyDataSetChanged")
                    mAdapter!!.notifyDataSetChanged()
                    Toast.makeText(this@ChatPageActivity, R.string.chat_is_deleted_successfully, Toast.LENGTH_SHORT).show()
                    if (isAudioRecording) {
                        isAudioRecording = false
                        stopRecording(false)
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun load_delete_chat(data: String) {
        try {
            val `object` = JSONObject(data)
            MyLog.e(
                TAG,
                "load_delete_chat$`object`"
            )
            try {
                val from = `object`.getString("from")
                val convId = `object`.getString("convId")
                val type = `object`.getString("type")
                val lastId = `object`.getString("lastId")
                var mLong = 0L
                if (`object`.has("delete_opponent")) {
                    //Add 2000 millisec
                    mLong = lastId.toLong() + 2000
                    val delete_opponent = `object`.getString("delete_opponent")
                    if (Integer.valueOf(delete_opponent) == 0) {
                        for (i in mChatData!!.indices) {
                            val groupid = mChatData!![i]!!.messageId.split("-").toTypedArray()
                            if (groupid[2].toLong() <= mLong) {
                                mChatData!!.remove(mChatData!![i])
                                MyLog.e(
                                    "reemoved",
                                    "removed" + groupid[2] + "groupid[2]" + "mLong" + mLong + mChatData!!.size
                                )
                            } else {
                                // mChatData.remove(mChatData.get(i));
                                MyLog.e(
                                    "reemoved",
                                    "not removed" + groupid[2] + "groupid[2]" + "mLong" + mLong
                                )
                                //    break;
                            }
                        }
                        mAdapter!!.notifyDataSetChanged()
                    } else if (Integer.valueOf(delete_opponent) == 1) {
                        // lastId=lastId+2000;
                        mLong = lastId.toLong() + 2000
                        for (i in mChatData!!.indices) {
                            val groupid = mChatData!![i]!!.messageId.split("-").toTypedArray()
                            if (groupid[2].toLong() <= mLong) {
                                mChatData!!.remove(mChatData!![i])
                                MyLog.e("reemoved", "mChatData" + mChatData!!.size)
                            } else {
                                break
                            }
                        }
                        mAdapter!!.notifyDataSetChanged()
                    }
                } else {
                    if (!from.equals(mCurrentUserId, ignoreCase = true)) {
                        mChatData!!.clear()
                        mAdapter!!.notifyDataSetChanged()
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //-----------------------------------Profile Picture Updation-------------------------------------------
    private fun loadTypingStatus(event: ReceviceMessageEvent) {
        val `object` = event.objectsArray
        try {
            val jsonObject = JSONObject(`object`[0].toString())
            val from_typing = jsonObject["from"] as String
            val to_typing = jsonObject["to"] as String
            val convId = jsonObject["convId"] as String
            val type = jsonObject["type"] as String
            var typingPerson = ""
            if (!isGroupChat) {
                val blockedStatus = (contactDB_sqlite!!.getBlockedStatus(from_typing, false) == "1")
                if ((from_typing.equals(to, ignoreCase = true) && type.equals(
                        "single",
                        ignoreCase = true
                    ) && (convId == mConvId) && !blockedStatus)
                ) {
                    status_textview!!.text = resources.getString(R.string.typing_dots)
                    status_textview!!.visibility = View.VISIBLE
                    handleReceiverTypingEvent()
                }
            } else {
                if (!from_typing.equals(from, ignoreCase = true) && to_typing.equals(
                        to,
                        ignoreCase = true
                    )
                ) {
                    if (!from_typing.equals(from, ignoreCase = true)) {
                        val msisdn =
                            contactDB_sqlite!!.getSingleData(from_typing, ContactDB_Sqlite.MSISDN)
                        if (msisdn != null) {
                            typingPerson = msisdn
                            typingPerson = getcontactname!!.getSendername(from_typing, typingPerson)
                        }
                        for (i in mChatData!!.indices) {
                            val item = mChatData!![i]
                            val groupid = item!!.messageId.split("-").toTypedArray()
                            if (convId.equals(groupid[1], ignoreCase = true)) {
                                status_textview!!.text =
                                    typingPerson + " " + resources.getString(R.string.is_typing_dots)
                                status_textview!!.visibility = View.VISIBLE
                                handleReceiverTypingEvent()
                                break
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //-----------------------------------------------Clear Chat-------------------------------------------------------------------
    private fun handleReceiverTypingEvent() {
        toLastTypedAt =
            Calendar.getInstance().timeInMillis + MessageFactory.TYING_MESSAGE_MIN_TIME_DIFFERENCE
        toTypingHandler!!.postDelayed((toTypingRunnable)!!, MessageFactory.TYING_MESSAGE_TIMEOUT)
    }

    //-------------------------------------------------Typing Status-------------------------------------------------------------
    private fun loadOnlineStatus(event: ReceviceMessageEvent) {
        val `object` = event.objectsArray
        try {
            val jsonObject = JSONObject(`object`[0].toString())
            val id = jsonObject["_id"] as String
            val status = jsonObject["Status"].toString()
            if (id.equals(to, ignoreCase = true)) {
                if (status.equals("1", ignoreCase = true)) {
                    status_textview!!.text = resources.getString(R.string.online)
                    status_textview!!.visibility = View.GONE
                } else {
                    val contactDB_sqlite = CoreController.getContactSqliteDBintstance(this)
                    if (canShowLastSeen && !sessionManager!!.lastSeenVisibleTo.equals(
                            "nobody",
                            ignoreCase = true
                        )
                    ) {
                        val lastSeen = jsonObject.getString("DateTime")
                        setOnlineStatusText(lastSeen)
                    } else if (!canShowLastSeen /*&& contactDB_sqlite.getLastSeenVisibility(id).equalsIgnoreCase("nobody")*/) {
                        Log.e(
                            TAG,
                            "nobody" + contactDB_sqlite.getLastSeenVisibility(id)
                                .equals("nobody", ignoreCase = true)
                        )
                        status_textview!!.text = ""
                        status_textview!!.visibility = View.GONE
                    } else {
                        //      status_textview.setText("");
                        val lastSeen = jsonObject.getString("DateTime")
                        setOnlineStatusText(lastSeen)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setOnlineStatusText(lastSeen: String) {
        var lastSeen: String? = lastSeen
        try {
            var lastSeenTime = lastSeen!!.toLong()
            val serverDiff = sessionManager!!.serverTimeDifference
            lastSeenTime = lastSeenTime + serverDiff
            val lastSeenAt = Date(lastSeenTime)
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss z")
            val date2 = Date()
            var currentDate = sdf.format(date2)
            currentDate = currentDate.substring(0, 10)
            val strLastSeenAt = sdf.format(lastSeenAt)
            val onlineStatus: String
            //  status_textview.setInitialDelay(2000);
            // Remove a character every 150ms
            // status_textview.setCharacterDelay(250);
            if (lastSeen != null) {
                if ((currentDate == strLastSeenAt.substring(0, 10))) {
                    lastSeen =
                        ScimboUtilities.convert24to12hourformat(strLastSeenAt.substring(11, 19))
                    lastSeen = lastSeen.replace(".", "")
                    //     onlineStatus = "last seen at " + lastSeen;
                    onlineStatus = lastSeen
                } else {
                    var last =
                        ScimboUtilities.convert24to12hourformat(strLastSeenAt.substring(11, 19))
                    val separated = strLastSeenAt.substring(0, 10).split("-").toTypedArray()
                    last = last.replace(".", "")
                    val date = separated.get(2) + "-" + separated[1] + "-" + separated[0]
                    //   onlineStatus = "last seen " + date + " " + last;
                    onlineStatus = "$date $last"
                }
                //  status_textview.animateText(LAST_SEEN_TEXT, LAST_SEEN_TEXT + onlineStatus);

                //status_textview.setText(LAST_SEEN_TEXT + onlineStatus);
                status_textview!!.text = ""
                status_textview!!.visibility = View.GONE
            }
        } catch (e: Exception) {
            status_textview!!.text = ""
        }
    }

    private fun loadCurrentTimeMessage(event: ReceviceMessageEvent) {
        val `object` = event.objectsArray
        try {
            val jsonObject = JSONObject(`object`[0].toString())
            val id = jsonObject["_id"] as String
            val status = jsonObject["Status"].toString()
            if (id.equals(to, ignoreCase = true)) {
                isLastSeenCalled = true
                canShowLastSeen = true
                val privacyObj = jsonObject.getJSONObject("Privacy")
                if (privacyObj.has("last_seen")) {
                    val showLastSeen = privacyObj.getString("last_seen")
                    if (showLastSeen.equals(
                            ContactDB_Sqlite.PRIVACY_TO_NOBODY,
                            ignoreCase = true
                        )
                    ) {
                        canShowLastSeen = false
                        contactDB_sqlite!!.updateLastSeenVisibility(
                            to,
                            ContactDB_Sqlite.PRIVACY_STATUS_NOBODY
                        )
                    } else if (showLastSeen.equals(
                            ContactDB_Sqlite.PRIVACY_TO_MY_CONTACTS,
                            ignoreCase = true
                        )
                    ) {
                        val isContactUser = jsonObject.getString("is_contact_user")
                        contactDB_sqlite!!.updateLastSeenVisibility(
                            to,
                            ContactDB_Sqlite.PRIVACY_STATUS_MY_CONTACTS
                        )
                        contactDB_sqlite!!.updateMyContactStatus(to, isContactUser)
                        if ((isContactUser == "0")) {
                            canShowLastSeen = false
                        }
                    } else {
                        contactDB_sqlite!!.updateLastSeenVisibility(
                            to,
                            ContactDB_Sqlite.PRIVACY_STATUS_EVERYONE
                        )
                    }
                }
                //Check it is blocked or not
                if ((contactDB_sqlite!!.getBlockedMineStatus(
                        to,
                        false
                    ) == ContactDB_Sqlite.BLOCKED_STATUS)
                ) {
                    canShowLastSeen = false
                    status_textview!!.visibility = View.GONE
                }
                if ("1".equals(status, ignoreCase = true)) {
                    status_textview!!.text = resources.getString(R.string.online)
                    status_textview!!.visibility = View.GONE
                } else {
                    if (canShowLastSeen && !sessionManager!!.lastSeenVisibleTo.equals(
                            "nobody",
                            ignoreCase = true
                        )
                    ) {
                        val lastSeen = jsonObject.getString("DateTime")
                        if (lastSeen != "0" && !lastSeen.equals("null", ignoreCase = true)) {
                            setOnlineStatusText(lastSeen)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //------------------------------------------------------Load Online Status-------------------------------------------------
    private fun loadStarredMessage(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            val objects = JSONObject(array[0].toString())
            val errorState = objects.getInt("err")
            if (errorState == 0) {
                val fromId = objects.getString("from")
                if (fromId.equals(from, ignoreCase = true)) {
                    val chat_id = objects.getString("doc_id")
                    val ids = chat_id.split("-").toTypedArray()
                    var docId: String
                    if (fromId.equals(ids[0], ignoreCase = true)) {
                        docId = ids.get(0) + "-" + ids[1]
                    } else {
                        docId = ids.get(1) + "-" + ids[0]
                    }
                    if (chat_id.contains("-g-")) {
                        docId = "$docId-g"
                    }
                    val starred = objects.getString("status")
                    val msgId = docId + "-" + ids[2]
                    for (items: MessageItemChat? in mChatData!!) {
                        if (items != null && items.messageId.equals(msgId, ignoreCase = true)) {
                            val index = mChatData!!.indexOf(items)
                            mChatData!!.get(index)!!.starredStatus = starred
                            mChatData!!.get(index)!!.isSelected = false
                            mAdapter!!.notifyDataSetChanged()
                            break
                        }
                    }
                }
            }
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }
    }

    private fun loadDeleteMessage(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            val objects = JSONObject(array[0].toString())
            val errorState = objects.getInt("err")
            if (errorState == 0) {
                val fromId = objects.getString("from")
                if (fromId.equals(from, ignoreCase = true)) {
                    val deleteStatus = objects.getString("status")
                    val chat_id = objects["doc_id"] as String
                    val ids = chat_id.split("-").toTypedArray()
                    val docId: String
                    val msgId: String
                    if (chat_id.contains("-g-")) {
                        docId = fromId + "-" + ids[1] + "-g"
                        msgId = docId + "-" + ids[3]
                    } else {
                        if (fromId.equals(ids[0], ignoreCase = true)) {
                            docId = ids.get(0) + "-" + ids[1]
                        } else {
                            docId = ids.get(1) + "-" + ids[0]
                        }
                        msgId = docId + "-" + ids[2]
                    }

                    //  String groupAndMsgId = ids[1] + "-g-" + ids[3];
                    if (deleteStatus.equals("1", ignoreCase = true)) {
                        for (items: MessageItemChat? in mChatData!!) {
                            if (items != null && items.messageId.equals(msgId, ignoreCase = true)) {
                                val index = mChatData!!.indexOf(items)
                                if (index > -1 && mChatData!![index]!!.isMediaPlaying) {
                                    mAdapter!!.stopAudioOnMessageDelete(index)
                                }
                                mAdapter!!.notifyDataSetChanged()
                                break
                            }
                        }
                    }
                }
            }
        } catch (ex: Exception) {
            MyLog.e(TAG, "loadDeleteMessage: ", ex)
        }
    }

    //---------------------------------------------------Load Current Time Message------------------------------------------------
    private fun loadMuteMessage(data: String) {
        try {
            val `object` = JSONObject(data)
            val from = `object`.getString("from")
            if (from.equals(mCurrentUserId, ignoreCase = true)) {
                val convId = `object`.getString("convId")
                if (convId.equals(mConvId, ignoreCase = true)) {
//                    onCreateOptionsMenu(chatMenu);
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //--------------------------------------------------------Load Stared Message---------------------------------------------------
    private fun loadReplyMessageDetails(data: String) {
        try {
            val `object` = JSONObject(data)
            val err = `object`.getString("err")
            val dataObj = `object`.getJSONObject("data")
            val from = dataObj.getString("from")
            val convId = dataObj.getString("convId")
            if ((err == "0") && convId.equals(mConvId, ignoreCase = true)) {
                val to = dataObj.getString("to")
                val requestMsgId = dataObj.getString("requestMsgId")
                val chatType = dataObj.getString("type")
                var docId = "$from-$to"
                if (chatType.equals(MessageFactory.CHAT_TYPE_GROUP, ignoreCase = true)) {
                    docId = "$docId-g"
                } else {
                    var secretType = "no"
                    if (dataObj.has("secret_type")) {
                        secretType = dataObj.getString("secret_type")
                    }
                    if (secretType.equals("yes", ignoreCase = true)) {
                        docId = "$docId-secret"
                    }
                }
                try {
                    for (i in mChatData!!.indices) {
                        val msgItem = mChatData!![i]
                        if (msgItem!!.messageId.equals(requestMsgId, ignoreCase = true)) {
                            val modifiedItem = messageDatabase!!.getParticularMessage(requestMsgId)
                            modifiedItem.isSelected = msgItem.isSelected
                            mChatData!![i] = modifiedItem
                            mAdapter!!.notifyDataSetChanged()
                            break
                        }
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {
                    e.printStackTrace()
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //--------------------------------------------------------Load Deleted Messages---------------------------------------------
    private fun deleteSingleMessage(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            val objects = JSONObject(array[0].toString())
            val errorState = objects.getInt("err")
            if (errorState == 0) {
                val fromId = objects.getString("from")
                val docId: String
                val msgId: String
                val type: String
                var recId: String
                var lastMsg_Status: String
                var convId: String
                //                String deleteStatus = objects.getString("status");
                val chat_id = objects["doc_id"] as String
                val ids = chat_id.split("-").toTypedArray()
                type = objects.getString("type")
                //                recId = objects.getString("recordId");
//                convId = objects.getString("convId");
                docId = ids.get(1) + "-" + ids[0]
                msgId = docId + "-" + ids[2]
                if (fromId.equals(from, ignoreCase = true)) {
                    for (items: MessageItemChat? in mChatData!!) {
                        if (items != null && items.messageId.equals(chat_id, ignoreCase = true)) {
                            val index = mChatData!!.indexOf(items)
                            if (index > -1 && mChatData!![index]!!.isMediaPlaying) {
                                mAdapter!!.stopAudioOnMessageDelete(index)
                            }
                            mChatData!!.get(index)!!.messageType =
                                MessageFactory.DELETE_SELF.toString() + ""
                            mChatData!![index]!!.setIsSelf(true)
                            mAdapter!!.notifyDataSetChanged()
                            messageDatabase!!.deleteSingleMessage(
                                docId,
                                items.messageId,
                                chatType,
                                "self"
                            )
                            messageDatabase!!.deleteChatListPage(
                                docId,
                                items.messageId,
                                chatType,
                                "self"
                            )
                            break
                        }
                    }
                }
                if (!fromId.equals(from, ignoreCase = true) && fromId.equals(
                        Chat_to,
                        ignoreCase = true
                    )
                ) {
                    for (items: MessageItemChat? in mChatData!!) {
                        if (items != null && items.messageId.equals(msgId, ignoreCase = true)) {
                            val index = mChatData!!.indexOf(items)
                            if (index > -1 && mChatData!![index]!!.isMediaPlaying) {
                                mAdapter!!.stopAudioOnMessageDelete(index)
                            }
                            mChatData!!.get(index)!!.messageType =
                                MessageFactory.DELETE_OTHER.toString() + ""
                            mChatData!![index]!!.setIsSelf(false)
                            mAdapter!!.notifyDataSetChanged()
                            messageDatabase!!.deleteSingleMessage(docId, msgId, type, "other")
                            messageDatabase!!.deleteChatListPage(docId, msgId, type, "other")
                            break
                        }
                    }
                }
            }
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }
    }

    //---------------------------------------------------------------Load Mute Messages------------------------------------------
    private fun getSingleOffline(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            val objects = JSONObject(array[0].toString())
            val is_everyone = objects.getInt("is_deleted_everyone")
            if (is_everyone == 1) {
                val fromId = objects.getString("from")
                if (!fromId.equals(mCurrentUserId, ignoreCase = true)) {
                    val chat_id = objects.getString("docId")
                    val ids = chat_id.split("-").toTypedArray()
                    val docId: String
                    val msgId: String
                    val chat_type: String
                    var recId: String
                    var lastMsg_Status: String
                    var convId: String
                    var deleteStatus: String
                    chat_type = objects.getString("chat_type")
                    //                    recId = objects.getString("recordId");
//                    convId = objects.getString("convId");
//                    deleteStatus = objects.getString("is_deleted_everyone");
                    docId = ids.get(1) + "-" + ids[0]
                    msgId = docId + "-" + ids[2]
                    if (mChatData!!.size > 0) {
                        for (i in mChatData!!.indices) {
                            if (mChatData!![i]!!.messageId.equals(msgId, ignoreCase = true)) {
                                if (i > -1 && mChatData!![i]!!.isMediaPlaying) {
                                    mAdapter!!.stopAudioOnMessageDelete(i)
                                }
                                mChatData!!.get(i)!!.messageType =
                                    MessageFactory.DELETE_OTHER.toString() + ""
                                mChatData!![i]!!.setIsSelf(false)
                                mAdapter!!.notifyDataSetChanged()
                                messageDatabase!!.deleteSingleMessage(
                                    docId,
                                    msgId,
                                    chat_type,
                                    "other"
                                )
                                messageDatabase!!.deleteChatListPage(
                                    docId,
                                    msgId,
                                    chat_type,
                                    "other"
                                )
                                break
                            }
                        }
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //--------------------------------------------------------------Load Reply Message Details--------------------------
    private fun updateProfileImage(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            val objects = JSONObject(array[0].toString())
            val from = objects.getString("from")
            val type = objects.getString("type")
            if (type.equals("single", ignoreCase = true) && to.equals(from, ignoreCase = true)) {
                val path = objects.getString("file") + "?id=" + Calendar.getInstance().timeInMillis
                profilepicupdation()
            }
        } catch (e: Exception) {
            MyLog.e(TAG, "updateProfileImage: ", e)
        }
    }

    //-------------------------------------------Delete Single Message---------------------------------------------------------
    private fun loadMessageStatusupdate(event: ReceviceMessageEvent) {
        val `object` = event.objectsArray
        try {
            val jsonObject = JSONObject(`object`[0].toString())
            val from = jsonObject.getString("from")
            val to = jsonObject.getString("to")
            val msgIds = jsonObject.getString("msgIds")
            val doc_id = jsonObject.getString("doc_id")
            val status = jsonObject.getString("status")
            val secretType = jsonObject.getString("secret_type")
            if (from.equals(mReceiverId, ignoreCase = true) && secretType.equals(
                    "no",
                    ignoreCase = true
                )
            ) {
                if (sessionManager!!.canSendReadReceipt() || !(status.equals(
                        MessageFactory.DELIVERY_STATUS_READ,
                        ignoreCase = true
                    ))
                ) {
                    for (i in mChatData!!.indices) {
                        val items = mChatData!![i]
                        if (items!!.isBlockedMsg) continue
                        if (items != null && items.messageId.equals(doc_id, ignoreCase = true)) {
                            items.deliveryStatus = "" + status
                            mChatData!!.get(i)!!.deliveryStatus = status
                            break
                        } else if (status.equals(
                                MessageFactory.DELIVERY_STATUS_DELIVERED,
                                ignoreCase = true
                            ) && mChatData!![i]!!
                                .isSelf
                        ) {
                            if ((mChatData!![i]!!.deliveryStatus == MessageFactory.DELIVERY_STATUS_SENT)) {
                                mChatData!!.get(i)!!.deliveryStatus = status
                            }
                        } else if ((mChatData!![i]!!.isSelf && status.equals(
                                MessageFactory.DELIVERY_STATUS_READ,
                                ignoreCase = true
                            ) &&
                                    (((mChatData!![i]!!.deliveryStatus == MessageFactory.DELIVERY_STATUS_SENT) || ((mChatData!![i]!!
                                        .deliveryStatus == MessageFactory.DELIVERY_STATUS_DELIVERED)))))
                        ) {
                            mChatData!!.get(i)!!.deliveryStatus = status
                        }
                    }
                }
                mAdapter!!.notifyDataSetChanged()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //--------------------------------------------------------Get Single Offline-----------------------------------------------
    private fun blockunblockcontact(event: ReceviceMessageEvent) {
        var toid = ""
        var fromid = ""
        try {
            val obj = event.objectsArray
            val `object` = JSONObject(obj[0].toString())
            val stat = `object`.getString("status")
            toid = `object`.getString("to")
            fromid = `object`.getString("from")
            if (mCurrentUserId.equals(fromid, ignoreCase = true) && toid.equals(
                    mReceiverId,
                    ignoreCase = true
                )
            ) {
                setTopLayoutBlockText()
                if (stat.equals("1", ignoreCase = true)) {
                    block_contact!!.text = getString(R.string.unblock)
                    Toast.makeText(this, getString(R.string.number_is_blocked), Toast.LENGTH_SHORT)
                        .show()
                } else {
                    block_contact!!.text = getString(R.string.block_)
                    Toast.makeText(
                        this,
                        getString(R.string.number_is_unblocked),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else if (mCurrentUserId.equals(toid, ignoreCase = true) && fromid.equals(
                    mReceiverId,
                    ignoreCase = true
                )
            ) {
                getcontactname!!.configCircleProfilepic(
                    user_profile_image,
                    to,
                    true,
                    false,
                    R.drawable.ic_placeholder_black
                )
                if (stat.equals("1", ignoreCase = true)) {
                    status_textview!!.visibility = View.GONE
                } else {
                    status_textview!!.visibility = View.GONE
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //----------------------------------------Update Profile Image---------------------------------------
    private fun setTopLayoutBlockText() {
        if ((contactDB_sqlite!!.getBlockedStatus(mReceiverId, false) == "1")) {
            tvBlock!!.text = getString(R.string.unblock)
        } else {
            tvBlock!!.text = getString(R.string.block_)
        }
    }

    //---------------------------------------------------Load Message Status Updated--------------------------------------------
    private val receiverOnlineTimeStatus: Unit
        private get() {
            try {
                val `object` = JSONObject()
                `object`.put("from", mCurrentUserId)
                `object`.put("to", to)
                val event = SendMessageEvent()
                event.eventName = SocketManager.EVENT_GET_CURRENT_TIME_STATUS
                event.messageObject = `object`
                EventBus.getDefault().post(event)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

    //----------------------------------------------------------Block Unblock Contact---------------------------------------------------
    private fun handleGroupMessage(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            val objects = JSONObject(array[0].toString())
            val `object` = objects["type"]
            if (objects.has("id")) {
                val msgId = objects.getString("id")
                if (msgId != null && !msgId.isEmpty()) {
                    if (messageIds!!.contains(msgId)) {
                        return
                    }
                    messageIds!!.add(msgId)
                }
            }
            var type: Int? = 0
            if (`object` is String) {
                type = Integer.valueOf(objects["type"] as String)
            } else if (`object` is Int) {
                type = Integer.valueOf((objects["type"] as Int))
            }
            when (type) {
                MessageFactory.delete_member_by_admin -> loadDeleteMemberMessage(objects)
                else -> loadGroupMessage(objects)
            }


            //-------------Delete Chat----------------
            if (isGroupChat) {
                val group_type: Int
                val is_deleted: Int
                val new_msgId: String
                if (objects.has("groupType")) {
                    group_type = objects.getInt("groupType")
                    if (group_type == 9) {
                        if (objects.has("is_deleted_everyone")) {
                            is_deleted = objects.getInt("is_deleted_everyone")
                            if (is_deleted == 1) {
                                val chat_id = objects["toDocOId"] as String
                                val ids = chat_id.split("-").toTypedArray()
                                new_msgId = mCurrentUserId + "-" + ids[1] + "-g-" + ids[3]
                                val groupAndMsgId = ids.get(1) + "-g-" + ids[3]
                                try {
                                    val fromId = objects.getString("from")
                                    if (fromId.equals(from, ignoreCase = true)) {
                                        if (mChatData!!.size > 0) {
                                            for (i in mChatData!!.indices) {
                                                if (mChatData!![i]!!.messageId.contains(chat_id)) {
                                                    if (i > -1 && mChatData!![i]!!.isMediaPlaying) {
                                                        mAdapter!!.stopAudioOnMessageDelete(i)
                                                    }
                                                    mChatData!!.get(i)!!.messageType =
                                                        MessageFactory.DELETE_OTHER.toString() + ""
                                                    mChatData!![i]!!.setIsSelf(false)
                                                    mAdapter!!.notifyDataSetChanged()
                                                    messageDatabase!!.deleteSingleMessage(
                                                        groupAndMsgId,
                                                        chat_id,
                                                        chatType,
                                                        "self"
                                                    )
                                                    messageDatabase!!.deleteChatListPage(
                                                        groupAndMsgId,
                                                        chat_id,
                                                        chatType,
                                                        "self"
                                                    )
                                                    break
                                                }
                                            }
                                        }
                                    }
                                    if (!fromId.equals(mCurrentUserId, ignoreCase = true)) {
                                        if (mChatData!!.size > 0) {
                                            for (i in mChatData!!.indices) {
                                                if (mChatData!![i]!!.messageId.contains(
                                                        groupAndMsgId
                                                    )
                                                ) {
                                                    if (i > -1 && mChatData!![i]!!.isMediaPlaying) {
                                                        mAdapter!!.stopAudioOnMessageDelete(i)
                                                    }
                                                    mChatData!!.get(i)!!.messageType =
                                                        MessageFactory.DELETE_OTHER.toString() + ""
                                                    mChatData!![i]!!.setIsSelf(false)
                                                    mAdapter!!.notifyDataSetChanged()
                                                    messageDatabase!!.deleteSingleMessage(
                                                        groupAndMsgId,
                                                        new_msgId,
                                                        "group",
                                                        "other"
                                                    )
                                                    messageDatabase!!.deleteChatListPage(
                                                        groupAndMsgId,
                                                        new_msgId,
                                                        "group",
                                                        "other"
                                                    )
                                                    break
                                                }
                                            }
                                        }
                                    }
                                } catch (ex: JSONException) {
                                    ex.printStackTrace()
                                }
                            }
                        }
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun loadGroupMessage(objects: JSONObject) {
        var msgId = ""
        try {
            val from = objects.getString("from")
            val groupId = objects.getString("groupId")
            var msgIndex = -1
            if (objects.optString("type").toInt() == 23) {
                return
            }
            if (from.equals(mCurrentUserId, ignoreCase = true)) {
                if (mGroupId.equals(groupId, ignoreCase = true)) {
                    msgId = objects.getString("toDocId")
                    for (i in mChatData!!.indices) {
                        if (mChatData!![i]!!.messageId.equals(msgId, ignoreCase = true)) {
                            msgIndex = i
                            break
                        }
                    }
                    if (msgIndex > -1) {
                        groupMessageRes(objects, msgIndex)
                    } else {
                        val incomingMessage = IncomingMessage(this@ChatPageActivity)
                        val msgItem = incomingMessage.loadGroupMessageFromWeb(objects)
                        if (msgItem != null) {
                            mChatData!!.add(msgItem)
                            notifyDatasetChange()
                        }
                    }
                }
            } else {
                if (groupId.equals(mGroupId, ignoreCase = true)) {
                    val incomingMessage = IncomingMessage(this@ChatPageActivity)
                    val msgItem = incomingMessage.loadGroupMessage(objects)
                    val senderName =
                        getcontactname!!.getSendername(msgItem!!.receiverID, msgItem.senderName)
                    msgItem.senderName = senderName
                    msgId = objects.getString("toDocId")
                    if (msgItem != null) {
                        if (!from.equals(mCurrentUserId, ignoreCase = true)) {
                            if (lastvisibleitempostion == mChatData!!.size - 1) {
                                if (!msgId.equals(Group_Message_id, ignoreCase = true)) {
                                    Group_Message_id = msgId
                                    mChatData!!.add(msgItem)
                                    notifyDatasetChange()
                                }
                            } else {
                                if (!msgId.equals(Group_Message_id, ignoreCase = true)) {
                                    unreadmsgcount++
                                    unreadmessage()
                                    mChatData!!.add(msgItem)
                                    notifyDatasetChange()
                                }
                            }
                        } else {
                            notifyDatasetChange()
                        }
                    }
                    val id = objects.getString("id")
                    val uniqueID = mCurrentUserId + "-" + to + "-g"
                    if (!from.equals(mCurrentUserId, ignoreCase = true)) {
                        sendGroupAckToServer(
                            mCurrentUserId,
                            to,
                            id,
                            MessageFactory.GROUP_MSG_READ_ACK
                        )
                    }
                    if (isChatPage) {
                        changeBadgeCount(uniqueID)
                    }
                    sendChatViewedStatus()
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //-----------------------------------------Get Receiver Online Time Status------------------------------
    private fun sendGroupAckToServer(from: String?, groupId: String?, id: String, status: String) {
        val messageEvent = SendMessageEvent()
        messageEvent.eventName = SocketManager.EVENT_GROUP
        try {
            val groupAckObj = JSONObject()
            groupAckObj.put("groupType", SocketManager.ACTION_ACK_GROUP_MESSAGE)
            groupAckObj.put("from", from)
            groupAckObj.put("groupId", groupId)
            groupAckObj.put("status", status)
            groupAckObj.put("msgId", id)
            messageEvent.messageObject = groupAckObj
            EventBus.getDefault().post(messageEvent)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    //-----------------------------------------------------------------GROUP MESSAGE DETAILS------------------------------------------
    private fun groupMessageRes(jsonObject: JSONObject, msgIndex: Int) {
        try {
            val deliver = jsonObject.getString("deliver")
            val recordId = jsonObject.getString("recordId")
            mChatData!!.get(msgIndex)!!.uploadStatus = MessageFactory.UPLOAD_STATUS_COMPLETED
            mChatData!!.get(msgIndex)!!.deliveryStatus = deliver
            mChatData!!.get(msgIndex)!!.recordId = recordId
            mAdapter!!.notifyDataSetChanged()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //----------------------------------Load Group Message---------------------------------------------
    fun loadDeleteMemberMessage(`object`: JSONObject) {
        try {
            val err = `object`.getString("err")
            if (err.equals("0", ignoreCase = true)) {
                val from = `object`.getString("from")
                val groupId = `object`.getString("groupId")
                if (groupId.equals(mGroupId, ignoreCase = true)) {
                    val msgId = `object`.getString("id")
                    val timeStamp: String
                    if (`object`.has("timeStamp")) {
                        timeStamp = `object`.getString("timeStamp")
                    } else {
                        timeStamp = `object`.getString("timestamp")
                    }
                    val removeId: String
                    if (`object`.has("removeId")) {
                        removeId = `object`.getString("removeId")
                    } else {
                        removeId = `object`.getString("createdTo")
                    }
                    val groupInfoPojo = groupInfoSession!!.getGroupInfo(docId)
                    val groupName = groupInfoPojo.groupName
                    val message = MessageFactory.getMessage(
                        MessageFactory.group_event_info,
                        context
                    ) as GroupEventInfoMessage
                    val item = message.createMessageItem(
                        MessageFactory.delete_member_by_admin,
                        false,
                        null,
                        MessageFactory.DELIVERY_STATUS_READ,
                        groupId,
                        groupName,
                        from,
                        removeId
                    )
                    item.messageId = "$docId-$msgId"
                    item.ts = timeStamp
                    val fromUserContact = contactDB_sqlite!!.isUserAvailableInDB(from)
                    val removedContact = contactDB_sqlite!!.isUserAvailableInDB(removeId)
                    //
                    if (removeId.equals(mCurrentUserId, ignoreCase = true)) {
                        enableGroupChat = false
                        if (fromUserContact) {
                            mChatData!!.add(item)
                            mAdapter!!.notifyDataSetChanged()
                            relativeLayout!!.visibility = View.GONE
                            group_left_layout!!.visibility = View.VISIBLE
                        }
                    } else if (from.equals(mCurrentUserId, ignoreCase = true)) {
                        if (removedContact) {
                            mChatData!!.add(item)
                            mAdapter!!.notifyDataSetChanged()
                        }
                    } else {
                        if (fromUserContact && removedContact) {
                            mChatData!!.add(item)
                            mAdapter!!.notifyDataSetChanged()
                        }
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun updateGroupMsgStatus(objects: JSONObject) {
        try {
            //   Log.e("updateGroupMsgStatus ", "objects " + objects);
            val err = objects.getString("err")
            if (err.equals("0", ignoreCase = true)) {
                val from = objects.getString("from")
                val groupId = objects.getString("groupId")
                val msgId = objects.getString("msgId")
                val deliverStatus = objects.getString("status")
                //                String recordId = objects.getString("recordId");
                val docId = sessionManager!!.currentUserID + "-" + groupId + "-g"
                val resMsgId = "$docId-$msgId"
                for (i in mChatData!!.indices) {
                    val dbItem = mChatData!![i]
                    if (dbItem != null && dbItem.messageId.equals(resMsgId, ignoreCase = true)) {
                        //   Log.e("updateGroupMsgStatus equalsIgnoreCase", "resMsgId " + resMsgId + "dbItem.getMessageId()." + dbItem.getMessageId());
                        dbItem.deliveryStatus = "" + deliverStatus //test it here
                        //   sendAllAcksToServer(mChatData);
                        //  sendGroupACK(groupId, msgId, uniqueCurrentID);
                        //   sendGroupAckToServer(mCurrentUserId, to, msgId, MessageFactory.GROUP_MSG_READ_ACK);

                        //     loadFromDB();
                        mChatData!!.get(i)!!.deliveryStatus = deliverStatus
                        if (deliverStatus.equals(
                                MessageFactory.DELIVERY_STATUS_DELIVERED,
                                ignoreCase = true
                            )
                        ) {
                            //       loadFromDB();
                        }
                        //Check if the status is 2
                        break
                    } else if (deliverStatus.equals(
                            MessageFactory.DELIVERY_STATUS_DELIVERED,
                            ignoreCase = true
                        ) && mChatData!![i]!!
                            .isSelf
                    ) {

                        // Log.e("updateGroupMsgStatus equals", "DELIVERY_STATUS_DELIVERED " + deliverStatus + " mChatData.get(i).isSelf()" + mChatData.get(i).isSelf());
                        if ((mChatData!![i]!!.deliveryStatus == MessageFactory.DELIVERY_STATUS_SENT)) {
                            mChatData!!.get(i)!!.deliveryStatus = deliverStatus
                        }
                    } else if ((deliverStatus.equals(
                            MessageFactory.DELIVERY_STATUS_READ,
                            ignoreCase = true
                        ) && mChatData!![i]!!
                            .isSelf &&
                                mChatData!!.get(i)!!.deliveryStatus != MessageFactory.DELIVERY_STATUS_READ)
                    ) {
                        //   Log.e("updateGroupMsgStatus equals", "DELIVERY_STATUS_READ " + deliverStatus + " mChatData.get(i).isSelf()" + mChatData.get(i).isSelf());
                        mChatData!!.get(i)!!.deliveryStatus = deliverStatus
                    }
                }
                // sendAllAcksToServer(mChatData);

                //   initAdapter();
                //   madapter.updateInfo(mChatData);
                //notifyDataSetChanged();
                mAdapter!!.notifyDataSetChanged()


                // madapter.notifyDataSetChanged();

                //  loadFromDB();
            }
        } catch (e: JSONException) {
            mAdapter!!.updateInfo(mChatData) //notifyDataSetChanged();
            e.printStackTrace()
        }
    }

    fun loadChangeGroupNameMessage(`object`: JSONObject) {
        try {
            val from = `object`.getString("from")
            val id = `object`.getString("id")
            val msg = `object`.getString("message")
            val groupId = `object`.getString("groupId")
            val timeStamp = `object`.getString("timeStamp")
            val groupNewName = `object`.getString("groupName")
            val groupPrevName = `object`.getString("prev_name")
            val docId = sessionManager!!.currentUserID + "-" + groupId + "-g"
            val message = MessageFactory.getMessage(
                MessageFactory.group_event_info,
                context
            ) as GroupEventInfoMessage
            val item = message.createMessageItem(
                MessageFactory.change_group_name, false, null,
                MessageFactory.DELIVERY_STATUS_READ, groupId, groupNewName, from, null
            )
            item.prevGroupName = groupPrevName
            item.messageId = "$docId-$id"
            item.ts = timeStamp
            if (from.equals(mCurrentUserId, ignoreCase = true)) {
                mChatData!!.add(item)
                mAdapter!!.notifyDataSetChanged()
            } else {
                if (contactDB_sqlite!!.isUserAvailableInDB(from)) {
                    mChatData!!.add(item)
                    mAdapter!!.notifyDataSetChanged()
                }
            }
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }
    }

    //---------------------------------------------Load Delete Members------------------------------------
    fun loadExitMessage(`object`: JSONObject) {
        try {
            val from = `object`.getString("from")
            val groupId = `object`.getString("groupId")
            val timeStamp = `object`.getString("timeStamp")
            val id = `object`.getString("id")
            if (groupId.equals(mGroupId, ignoreCase = true)) {
                val docId = sessionManager!!.currentUserID + "-" + groupId + "-g"
                val infoPojo = groupInfoSession!!.getGroupInfo(docId)
                val groupName = infoPojo.groupName
                val message = MessageFactory.getMessage(
                    MessageFactory.group_event_info,
                    context
                ) as GroupEventInfoMessage
                val item = message.createMessageItem(
                    MessageFactory.exit_group, false, null,
                    MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null
                )
                item.messageId = "$docId-$id"
                item.ts = timeStamp
                if (from.equals(mCurrentUserId, ignoreCase = true)) {
                    mChatData!!.add(item)
                    mAdapter!!.notifyDataSetChanged()
                } else {
                    if (contactDB_sqlite!!.isUserAvailableInDB(from)) {
                        mChatData!!.add(item)
                        mAdapter!!.notifyDataSetChanged()
                    }
                }
            }
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }
    }

    //--------------------------------------------Update Group Message Status-------------------------------------------------
    private fun performGroupChangeDp(`object`: JSONObject) {
        try {
            val err = `object`.getString("err")
            if (err.equals("0", ignoreCase = true)) {
                val from = `object`.getString("from")
                val groupId = `object`.getString("groupId")
                val avatar = `object`.getString("avatar")
                val groupName = `object`.getString("groupName")
                val timeStamp: String
                if (`object`.has("timeStamp")) {
                    timeStamp = `object`.getString("timeStamp")
                } else {
                    timeStamp = `object`.getString("timestamp")
                }
                if (groupId.equals(to, ignoreCase = true)) {
                    receiverAvatar = avatar
                    /*                    Glide.with(ChatViewActivity.this).load(Constants.SOCKET_IP.concat(receiverAvatar))

                            .error(R.mipmap.group_chat_attachment_profile_icon).into(ivProfilePic);*/if (receiverAvatar != null && !receiverAvatar!!.isEmpty()) {
                        AppUtils.loadImage(
                            this@ChatPageActivity, AppUtils.getValidGroupPath(
                                receiverAvatar
                            ),
                            user_profile_image, 100, R.drawable.ic_placeholder_black
                        )
                    } else {
                        user_profile_image!!.setImageResource(R.drawable.ic_placeholder_black)
                    }
                    val message = MessageFactory.getMessage(
                        MessageFactory.group_event_info,
                        context
                    ) as GroupEventInfoMessage
                    val item = message.createMessageItem(
                        MessageFactory.change_group_icon, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null
                    )
                    item.avatarImageUrl = avatar
                    item.ts = timeStamp
                    val id: String
                    if (`object`.has("id")) {
                        id = `object`.getString("id")
                    } else {
                        id = Calendar.getInstance().timeInMillis.toString() + ""
                    }
                    item.messageId = "$docId-$id"
                    if (!from.equals(mCurrentUserId, ignoreCase = true)) {
                        if (contactDB_sqlite!!.isUserAvailableInDB(from)) {
                            mChatData!!.add(item)
                            mAdapter!!.notifyDataSetChanged()
                        }
                    } else {
                        mChatData!!.add(item)
                        mAdapter!!.notifyDataSetChanged()
                    }
                }
            }
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }
    }

    //--------------------------------------------Load Change Group Name---------------------------------------------------
    private fun loadAddMemberMessage(`object`: JSONObject) {
        try {
            val groupId = `object`.getString("groupId")
            if (groupId.equals(mGroupId, ignoreCase = true)) {
                val msgId = `object`.getString("id")
                val timeStamp = `object`.getString("timeStamp")
                val from = `object`.getString("from")
                var groupName: String? = ""
                if (`object`.has("groupName")) {
                    groupName = `object`.getString("groupName")
                }
                val newUserObj = `object`.getJSONObject("newUser")
                val newUserId = newUserObj.getString("_id")
                val newUserMsisdn = newUserObj.getString("msisdn")
                val newUserName = newUserObj.getString("Name")
                val docId = "$mCurrentUserId-$groupId-g"
                val infoPojo = groupInfoSession!!.getGroupInfo(docId)
                if (infoPojo != null) {
                    groupName = infoPojo.groupName
                }
                val pincodesCacheUtility = PincodesCacheUtility.getInstance(this)
                pincodesCacheUtility.savePinCodeItem(context, newUserId, newUserMsisdn, newUserName)
                val message = MessageFactory.getMessage(
                    MessageFactory.group_event_info,
                    context
                ) as GroupEventInfoMessage
                val item = message.createMessageItem(
                    MessageFactory.add_group_member, false, null,
                    MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, newUserId
                )
                item.messageId = "$docId-$msgId"
                item.ts = timeStamp
                val fromUserContact = contactDB_sqlite!!.isUserAvailableInDB(from)
                val newUserContact = contactDB_sqlite!!.isUserAvailableInDB(newUserId)
                if (from.equals(mCurrentUserId, ignoreCase = true)) {
                    if (newUserContact) {
                        mChatData!!.add(item)
                        mAdapter!!.notifyDataSetChanged()
                    }
                } else {
                    if (newUserId.equals(mCurrentUserId, ignoreCase = true)) {
                        if (fromUserContact) {
                            mChatData!!.add(item)
                            mAdapter!!.notifyDataSetChanged()
                        }
                    } else {
                        if (fromUserContact && newUserContact) {
                            mChatData!!.add(item)
                            mAdapter!!.notifyDataSetChanged()
                        }
                    }
                }
                //Check visiblity if send message is hidden
                if (RelativeLayout_group_delete!!.visibility == View.GONE) {
                    rlSend!!.visibility = View.VISIBLE
                    RelativeLayout_group_delete!!.visibility = View.GONE
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //--------------------------------------------Load Exit Message--------------------------------------------------
    private fun loadMakeAdminMessage(`object`: JSONObject) {
        try {
            val groupId = `object`.getString("groupId")
            if (groupId.equals(mGroupId, ignoreCase = true)) {
                val msgId = `object`.getString("id")
                val timeStamp = `object`.getString("timeStamp")
                //                    String toDocId = object.getString("toDocId");
                val from = `object`.getString("from")
                val docId = "$mCurrentUserId-$groupId-g"
                val infoPojo = groupInfoSession!!.getGroupInfo(docId)
                val groupName: String
                if (`object`.has("groupName")) {
                    groupName = `object`.getString("groupName")
                } else {
                    groupName = infoPojo.groupName
                }
                val newAdminUserId = `object`.getString("adminuser")
                val message = MessageFactory.getMessage(
                    MessageFactory.group_event_info,
                    context
                ) as GroupEventInfoMessage
                val item = message.createMessageItem(
                    MessageFactory.make_admin_member, false, null,
                    MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, newAdminUserId
                )
                item.messageId = "$docId-$msgId"
                item.ts = timeStamp
                if (newAdminUserId.equals(mCurrentUserId, ignoreCase = true)) {
                    mChatData!!.add(item)
                    mAdapter!!.notifyDataSetChanged()
                } else if (contactDB_sqlite!!.isUserAvailableInDB(newAdminUserId)) {
                    mChatData!!.add(item)
                    mAdapter!!.notifyDataSetChanged()
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //-------------------------------------------------Group Change DB----------------------------------------------------
    private fun deleteGroupMessage(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            val objects = JSONObject(array[0].toString())
            val fromId = objects.getString("from")
            //            String deleteStatus = objects.getString("status");
            val chat_id = objects["doc_id"] as String
            val ids = chat_id.split("-").toTypedArray()
            val docId: String
            val msgId: String
            val type: String
            var recId: String
            var convId: String
            type = objects.getString("type")
            //            recId = objects.getString("recordId");
//            convId = objects.getString("convId");
            docId = fromId + "-" + ids[1] + "-g"
            msgId = docId + "-" + ids[3]
            val groupAndMsgId = ids.get(1) + "-g-" + ids[3]
            if (fromId.equals(from, ignoreCase = true)) {
                if (mChatData!!.size > 0) {
                    for (i in mChatData!!.indices) {
                        if (mChatData!![i]!!.messageId.contains(chat_id)) {
                            if (i > -1 && mChatData!![i]!!.isMediaPlaying) {
                                mAdapter!!.stopAudioOnMessageDelete(i)
                            }
                            mChatData!!.get(i)!!.messageType =
                                MessageFactory.DELETE_SELF.toString() + ""
                            mChatData!![i]!!.setIsSelf(true)
                            val from_ids = chat_id.split("-").toTypedArray()
                            val from_groupAndMsgId = from_ids.get(1) + "-g-" + from_ids[3]
                            mAdapter!!.notifyDataSetChanged()
                            messageDatabase!!.deleteSingleMessage(
                                from_groupAndMsgId,
                                chat_id,
                                chatType,
                                "self"
                            )
                            messageDatabase!!.deleteChatListPage(
                                from_groupAndMsgId,
                                chat_id,
                                chatType,
                                "self"
                            )
                            break
                        }
                    }
                }
            }
            if (!fromId.equals(from, ignoreCase = true)) {
                if (mChatData!!.size > 0) {
                    for (i in mChatData!!.indices) {
                        if (mChatData!![i]!!.messageId.contains(groupAndMsgId)) {
                            if (i > -1 && mChatData!![i]!!.isMediaPlaying) {
                                mAdapter!!.stopAudioOnMessageDelete(i)
                            }
                            mChatData!!.get(i)!!.messageType =
                                MessageFactory.DELETE_OTHER.toString() + ""
                            mChatData!![i]!!.setIsSelf(false)
                            mAdapter!!.notifyDataSetChanged()
                            messageDatabase!!.deleteSingleMessage(
                                groupAndMsgId,
                                msgId,
                                type,
                                "other"
                            )
                            messageDatabase!!.deleteChatListPage(
                                groupAndMsgId,
                                msgId,
                                type,
                                "other"
                            )
                            break
                        }
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //---------------------------------------------Add Members Details in Group Message---------------------------------
    private fun getGroupOffline(event: ReceviceMessageEvent) {
        val array = event.objectsArray
        try {
            val objects = JSONObject(array[0].toString())
            val status = objects.getString("err")
            val fromID: String
            val toDocId: String
            val type: String
            if (status.equals("0", ignoreCase = true)) {
                val groupType = objects.getInt("groupType")
                if (groupType == 20) {
                    val is_deleted_everyone = objects.getInt("is_deleted_everyone")
                    if (is_deleted_everyone == 1) {
                        fromID = objects.getString("from")
                        toDocId = objects.getString("toDocId")
                        type = objects.getString("groupName")
                        val ids = toDocId.split("-").toTypedArray()
                        val groupAndMsgId = ids.get(1) + "-g-" + ids[3]
                        if (!fromID.equals(from, ignoreCase = true)) {
                            if (mChatData!!.size > 0) {
                                for (i in mChatData!!.indices) {
                                    if (mChatData!![i]!!.messageId.contains(groupAndMsgId)) {
                                        if (i > -1 && mChatData!![i]!!.isMediaPlaying) {
                                            mAdapter!!.stopAudioOnMessageDelete(i)
                                        }
                                        mChatData!!.get(i)!!.messageType =
                                            MessageFactory.DELETE_OTHER.toString() + ""
                                        mChatData!![i]!!.setIsSelf(false)
                                        mAdapter!!.notifyDataSetChanged()
                                        messageDatabase!!.deleteSingleMessage(
                                            groupAndMsgId,
                                            toDocId,
                                            type,
                                            "other"
                                        )
                                        messageDatabase!!.deleteChatListPage(
                                            groupAndMsgId,
                                            toDocId,
                                            type,
                                            "other"
                                        )
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //---------------------------------------------------Admin Message ---------------------------------------------------
    private fun loadGroupDetails(event: ReceviceMessageEvent) {
        try {
            val array = event.objectsArray
            val objects = JSONObject(array[0].toString())
            val groupId = objects.getString("_id")
            if (groupId.equals(mGroupId, ignoreCase = true)) {
                allMembersList!!.clear()
                savedMembersList!!.clear()
                unsavedMembersList!!.clear()
                val arrMembers = objects.getJSONArray("GroupUsers")
                val membersCount = arrMembers.length()
                for (i in 0 until arrMembers.length()) {
                    val userObj = arrMembers.getJSONObject(i)
                    val userId = userObj.getString("id")
                    val active = userObj.getString("active")
                    val isDeleted = userObj.getString("isDeleted")
                    val msisdn = userObj.getString("msisdn")
                    val phNumber = userObj.getString("PhNumber")
                    val name = userObj.getString("ContactName")
                    var status = userObj.getString("Status")
                    val userDp = userObj.getString("avatar")
                    val adminUser = userObj.getString("isAdmin")
                    val contactmsisdn = userObj.getString("ContactName")
                    val isExitsContact = userObj.getString("isExitsContact")
                    try {
                        if (ScimboRegularExp.isEncodedBase64String(status)) {
                            val arrStatus = Base64.decode(status, Base64.DEFAULT)
                            status = String(arrStatus, StandardCharsets.UTF_8)
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        status = ""
                    }
                    var contactName = getcontactname!!.getSendername(userId, msisdn)
                    if (mCurrentUserId.equals(userId, ignoreCase = true)) {
                        contactName = resources.getString(R.string.you)
                        status = SessionManager.getInstance(this).getcurrentUserstatus()
                    }
                    val data = GroupMembersPojo()
                    data.userId = userId
                    data.active = active
                    data.isDeleted = isDeleted
                    data.msisdn = msisdn
                    data.phNumber = phNumber
                    data.name = name
                    data.status = status
                    data.userDp = userDp
                    data.isAdminUser = adminUser
                    data.contactName = contactName
                    if (userId.equals(mCurrentUserId, ignoreCase = true)) {
                        mCurrentUserData = data
                    } else {
                        if (msisdn.equals(contactName, ignoreCase = true)) {
                            unsavedMembersList!!.add(data)
                        } else {
                            savedMembersList!!.add(data)
                        }
                    }
                }
                Collections.sort(savedMembersList, Getcontactname.groupMemberAsc)
                Collections.sort(unsavedMembersList, Getcontactname.groupMemberAsc)
                allMembersList!!.addAll((savedMembersList)!!)
                if (membersCount > 0) allMembersList!!.addAll(
                    (unsavedMembersList)!!
                )

//                if (mCurrentUserData != null) {
//                    allMembersList.add(mCurrentUserData);
//                }
                atAdapter!!.notifyDataSetChanged()
            }
        } catch (e: JSONException) {
        }
    }

    private inner class SendAckAsync(var items: ArrayList<MessageItemChat?>?) :
        AsyncTask<Void?, Void?, Void?>() {
        override fun doInBackground(vararg p0: Void?): Void? {
            sendAck(items)
            return null
        }
    }

    //---------------------------------------------------Delete Group Message-------------------------------------------
    private fun sendAllAcksToServer(items: ArrayList<MessageItemChat?>?) {
        Log.d(TAG, "sendAllAcksToServer: ")
        SendAckAsync(items).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        //sendAck(items);
    }

    private fun sendAck(items: ArrayList<MessageItemChat?>?) {
        //check blocked or not
        // For sent & receive acks
        Log.e(TAG, "sendAcktest: ")
        var lastDeliveredItem: MessageItemChat? = null
        var lastSentItem: MessageItemChat? = null
        val arrSentMsgRecordIds = JSONArray()
        var chatType: String = MessageFactory.CHAT_TYPE_SINGLE
        if (isGroupChat) {
            chatType = MessageFactory.CHAT_TYPE_GROUP
        }
        for (msgItem: MessageItemChat? in items!!) {
            val msgStatus = msgItem!!.deliveryStatus
            lastDeliveredItem = null
            if (msgItem.convId != null && msgItem.convId != "") {
                mConvId = msgItem.convId
            }
            if (msgStatus != null) {
                if (!msgItem.isSelf && !msgStatus.equals(
                        MessageFactory.DELIVERY_STATUS_READ,
                        ignoreCase = true
                    )
                ) {
                    if (isGroupChat && msgItem.groupMsgFrom != null) {
                        lastDeliveredItem = msgItem
                    } else if (!isGroupChat) {
                        lastDeliveredItem = msgItem
                    }
                } else if (msgItem.isSelf && (((msgStatus == MessageFactory.DELIVERY_STATUS_SENT) || (msgStatus == MessageFactory.DELIVERY_STATUS_DELIVERED)))) {
                    if (msgItem.recordId != null && arrSentMsgRecordIds.length() < 100) {
                        arrSentMsgRecordIds.put(msgItem.recordId)
                    }
                }
                if (msgItem.isSelf && msgStatus != MessageFactory.DELIVERY_STATUS_NOT_SENT) {
                    lastSentItem = msgItem
                }

                // For getting reply message details from server
                if ((!msgItem.isSelf && (msgItem.replyId != null) && msgItem.replyId != ""
                            && (msgItem.replyType == null || (msgItem.replyType == "")))
                ) {
                    getReplyMessageDetails(
                        mReceiverId,
                        msgItem.replyId,
                        chatType,
                        "no",
                        msgItem.messageId
                    )
                }
            }
            if (lastDeliveredItem != null) {
                sessionManager?.let{
                    if(it.canSendReadReceipt()) {
                        if (isGroupChat) {
                            val messageId = lastDeliveredItem.messageId
                            val splitList = lastDeliveredItem.messageId.split("-").toTypedArray()
                            val msgId = if (splitList.size > 3) splitList[3] else ""
                            sendGroupAckToServer(
                                mCurrentUserId,
                                mGroupId,
                                msgId,
                                MessageFactory.GROUP_MSG_READ_ACK
                            )
                        } else {
                            val msgId = lastDeliveredItem.messageId.split("-").toTypedArray()[2]
                            val ackDocId = to + "-" + mCurrentUserId + "-" + msgId
                            if (SocketManager.getInstance().isConnected) {
                                sendAckToServer(to, ackDocId, msgId, lastDeliveredItem.convId)
                            }
                            resendAck(to, ackDocId, msgId, lastDeliveredItem.convId)
                        }
                        lastDeliveredItem.convId?.let { convoId ->
                            sendViewedStatusToWeb(convoId)
                        }
                    }
                }
            } else if (lastSentItem != null && lastSentItem.convId != null) {
                sendViewedStatusToWeb(lastSentItem.convId)
            }
        }
        if (arrSentMsgRecordIds.length() > 0) {
            getMessageInfo(arrSentMsgRecordIds)
        }
        if (!isGroupChat) {
            val docId = "$mCurrentUserId-$mReceiverId"
            userInfoSession?.let{
                if(it.hasChatConvId(docId))
                    mConvId = userInfoSession?.getChatConvId(docId)
            }
        }
    }

    //for socket connection delay
    private fun resendAck(
        to: String?, ackDocId: String, msgId: String,
        convid: String
    ) {
        Handler(Looper.getMainLooper()).postDelayed({
            sendAckToServer(
                to,
                ackDocId,
                msgId,
                convid
            )
        }, 4000)
    }

    //---------------------------------------------------------------------Load Group Offline ------------------------------------
    private fun getMessageInfo(arrSentMsgRecordIds: JSONArray) {
        try {
            val `object` = JSONObject()
            `object`.put("recordId", arrSentMsgRecordIds)
            `object`.put("from", mCurrentUserId)
            if (isGroupChat) {
                `object`.put("type", MessageFactory.CHAT_TYPE_GROUP)
            } else {
                `object`.put("type", MessageFactory.CHAT_TYPE_SINGLE)
            }
            val infoEvent = SendMessageEvent()
            infoEvent.eventName = SocketManager.EVENT_GET_MESSAGE_INFO
            infoEvent.messageObject = `object`
            // EventBus.getDefault().post(infoEvent);
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //----------------------------------------------------------Load Group Details------------------------------------------------------
    fun getReplyMessageDetails(
        toUserId: String?, recordId: String?, chatType: String?,
        secretType: String?, msgId: String?
    ) {
        try {
            val `object` = JSONObject()
            `object`.put("from", mCurrentUserId)
            `object`.put("to", toUserId)
            `object`.put("recordId", recordId)
            `object`.put("requestMsgId", msgId)
            `object`.put("type", chatType)
            `object`.put("secret_type", secretType)
            val event = SendMessageEvent()
            event.eventName = SocketManager.EVENT_GET_MESSAGE_DETAILS
            event.messageObject = `object`
            EventBus.getDefault().post(event)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //-----------------------------------------------Send Acknowledgement To server All Messages--------------------------------------
    //-----------------------------------------WallPaper Set-------------------------------------
    fun wallpaperdisplay() {
        if ((session!!.getgalleryPrefsName() == "def")) {
            background!!.setImageResource(R.drawable.custom_wallpaper_1)
        } else if ((session!!.getgalleryPrefsName() == "no")) {
            val bmp = Bitmap.createBitmap(128, 128, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bmp)
            canvas.drawColor(Color.parseColor("#f0f0f0"))
            background!!.setImageBitmap(bmp)
        } else if (session!!.getgalleryPrefsName().contains("video#@")) {
            val galleryPref = session!!.getgalleryPrefsName()
            val arr = galleryPref.split("@").toTypedArray()
            val video_uri = arr[1]
            background!!.visibility = View.GONE
            chat_videoview!!.visibility = View.VISIBLE
            initializePlayer(video_uri)
            //String path="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
//            chat_videoview.setVideoURI(Uri.parse(video_uri));
//            chat_videoview.start();
        } else if (session!!.getgalleryPrefsName() != "") {
            session!!.putgalleryPrefs(session!!.getgalleryPrefsName())
            val gallery_string = session!!.getgalleryPrefsName()
            val bitmap = BitmapFactory.decodeFile(gallery_string)
            background!!.setImageBitmap(bitmap)
        } else if (session!!.color != "") {
            try {
                val color = session!!.color
                val bmp = Bitmap.createBitmap(128, 128, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bmp)
                canvas.drawColor(Color.parseColor(color))
                background!!.setImageBitmap(bmp)
                if ((color == "#FFFFFF")) {
                    sendMessage!!.setTextColor(resources.getColor(R.color.black))
                } else sendMessage!!.setTextColor(resources.getColor(R.color.white))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            val bmp = Bitmap.createBitmap(128, 128, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bmp)
            canvas.drawColor(Color.parseColor("#f0f0f0"))
            background!!.setImageBitmap(bmp)
        }
    }

    private fun initializePlayer(video_uri: String) {
        // Create a default TrackSelector
        val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory: TrackSelection.Factory =
            AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector: TrackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        //Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)

        //Initialize simpleExoPlayerView
        chat_videoview!!.player = player

        // Produces DataSource instances through which media data is loaded.
        val dataSourceFactory: DataSource.Factory =
            DefaultDataSourceFactory(this, Util.getUserAgent(this, "CloudinaryExoplayer"))

        // Produces Extractor instances for parsing the media data.
        val extractorsFactory: ExtractorsFactory = DefaultExtractorsFactory()

        // This is the MediaSource representing the media to be played.
        val videoUri = Uri.parse(video_uri)
        val videoSource: MediaSource = ExtractorMediaSource(
            videoUri,
            dataSourceFactory, extractorsFactory, null, null
        )

        // Prepare the player with the source.
        player?.prepare(videoSource)
        player?.setVolume(0f)
        player?.setPlayWhenReady(true)
    }

    private fun sendGroupOffline() {
        val groupMsgEvent = SendMessageEvent()
        groupMsgEvent.eventName = SocketManager.EVENT_GROUP
        try {
            val `object` = JSONObject()
            `object`.put("from", mCurrentUserId)
            `object`.put("groupType", SocketManager.ACTION_EVENT_GROUP_OFFLINE)
            groupMsgEvent.messageObject = `object`
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        EventBus.getDefault().post(groupMsgEvent)
    }

    private fun sendSingleOffline() {
        val groupMsgEvent = SendMessageEvent()
        groupMsgEvent.eventName = SocketManager.EVENT_SINGLE_OFFLINE_MSG
        try {
            val `object` = JSONObject()
            `object`.put("msg_to", mCurrentUserId)
            groupMsgEvent.messageObject = `object`
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        EventBus.getDefault().post(groupMsgEvent)
    }

    private fun sendNormalOffline() {
        val groupMsgEvent = SendMessageEvent()
        groupMsgEvent.eventName = SocketManager.EVENT_GET_MESSAGE
        try {
            val `object` = JSONObject()
            `object`.put("msg_to", mCurrentUserId)
            groupMsgEvent.messageObject = `object`
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        EventBus.getDefault().post(groupMsgEvent)
    }

    //----------------------------------------Group Offline-----------------------------------------------------------
    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        try {
            var muteData: MuteStatusPojo? = null
            if (isGroupChat) {
                chatMenu!!.findItem(R.id.menuSecretChat).isVisible = false
                muteData = contactDB_sqlite!!.getMuteStatus(mCurrentUserId, null, mGroupId, false)
            } else {
                chatMenu!!.findItem(R.id.menuSecretChat).isVisible = false
                muteData =
                    contactDB_sqlite!!.getMuteStatus(mCurrentUserId, mReceiverId, mConvId, false)
            }
            if (muteData != null && (muteData.muteStatus == "1")) {
                chatMenu!!.findItem(R.id.menuMute).title = getString(R.string.unmute)
            } else {
                chatMenu!!.findItem(R.id.menuMute).title = getString(R.string.mute)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return super.onPrepareOptionsMenu(menu)
    }

    //------------------------------Single Offline--------------------------------
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        chatMenu = menu
        menuInflater.inflate(R.menu.activity_chat_view, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuSearch -> performMenuSearch()
            R.id.menuSecretChat -> startSecretChat()
            R.id.menuMute -> performMenuMute()
            R.id.menuWallpaper -> goInfoPage()
            R.id.menuBlock -> performMenuBlock()
            R.id.menuClearChat -> performMenuClearChat()
            R.id.menuReportUser -> performMenuReportUser()
            R.id.menuEmailChat -> performMenuEmailChat()
            R.id.menuAddShortcut -> addShortcutConfirmationDialog()
            R.id.menuMore -> //                showMoreMenu();
                openMenu()
            R.id.menuChatLock ->                 /*Intent intent = new Intent(ChatViewActivity.this, EmailSettings.class);
                startActivity(intent);*/performChatlock()
        }
        return true
    }

    //----------------------------------------------------------------Menu Button Click---------------------------------------------
    private fun performMenuSearch() {
        showSearchActions()
        showProgressDialog()
        val allMessages = messageDatabase!!.selectAllChatMessages(docId, chatType)
        Collections.sort(allMessages, msgComparator)
        mChatData!!.clear()
        mChatData!!.addAll(allMessages)
        hideProgressDialog()
        Search1!!.background.clearColorFilter()
        Search1!!.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(cs: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
                if (cs.length > 0) {
                    mAdapter!!.filter.filter(cs)
                } else {
                    mAdapter!!.updateInfo(mChatData)
                }
            }

            override fun beforeTextChanged(
                arg0: CharSequence, arg1: Int, arg2: Int,
                arg3: Int
            ) {
                // TODO Auto-generated method stub
            }

            override fun afterTextChanged(arg0: Editable) {
                // TODO Auto-generated method stub
            }
        })
        iBtnBack2!!.setOnClickListener {
            Search1!!.setText("")
            mAdapter!!.updateInfo(mChatData)
            showUnSelectedActions()
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (this@ChatPageActivity.currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(
                    this@ChatPageActivity.currentFocus!!.windowToken, 0
                )
            }
        }
    }

    private fun showSearchActions() {
        // Make all selected items to unselect
        include!!.visibility = View.GONE
        rlChatActions!!.visibility = View.VISIBLE
        //    share.setVisibility(View.GONE);
        replymess!!.visibility = View.GONE
        copychat!!.visibility = View.GONE
        showOriginal!!.visibility = View.GONE
        starred!!.visibility = View.GONE
        info!!.visibility = View.GONE
        delete!!.visibility = View.GONE
        forward!!.visibility = View.GONE
        longpressback!!.visibility = View.GONE
        iBtnBack2!!.visibility = View.VISIBLE
        Search1!!.visibility = View.VISIBLE
    }

    private fun startSecretChat() {
        val intent = Intent(
            this@ChatPageActivity,
            SecretChatViewActivity::class.java
        )
        intent.putExtra("receiverUid", receiverUid)
        intent.putExtra("receiverName", receiverName)
        intent.putExtra("documentId", to)
        intent.putExtra("Username", mReceiverName)
        intent.putExtra("Image", receiverAvatar)
        intent.putExtra("msisdn", receiverMsisdn)
        intent.putExtra("type", 0)
        startActivity(intent)
        //overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    //-------------------------------------Menu Search-------------------------------
    private fun performMenuMute() {
        if (ConnectivityInfo.isInternetConnected(this@ChatPageActivity)) {
            var muteData: MuteStatusPojo? = null
            if (isGroupChat) {
                muteData = contactDB_sqlite!!.getMuteStatus(mCurrentUserId, null, mGroupId, false)
            } else {
                muteData =
                    contactDB_sqlite!!.getMuteStatus(mCurrentUserId, mReceiverId, mConvId, false)
            }
            if (muteData != null && (muteData.muteStatus == "1")) {
                if (!isGroupChat) {
                    MuteUnmute.performUnMute(
                        this@ChatPageActivity, EventBus.getDefault(), mReceiverId,
                        MessageFactory.CHAT_TYPE_SINGLE, "no"
                    )
                } else {
                    MuteUnmute.performUnMute(
                        this@ChatPageActivity,
                        EventBus.getDefault(),
                        mReceiverId,
                        MessageFactory.CHAT_TYPE_GROUP,
                        "no"
                    )
                }
            } else {
                val muteUserPojo = MuteUserPojo()
                muteUserPojo.receiverId = mReceiverId
                muteUserPojo.secretType = "no"
                if (isGroupChat) {
                    muteUserPojo.chatType = MessageFactory.CHAT_TYPE_GROUP
                } else {
                    muteUserPojo.chatType = MessageFactory.CHAT_TYPE_SINGLE
                }
                val muteUserList = ArrayList<MuteUserPojo>()
                muteUserList.add(muteUserPojo)
                val putBundle = Bundle()
                putBundle.putSerializable("MuteUserList", muteUserList)
                val dialog = MuteAlertDialog()
                dialog.arguments = putBundle
                dialog.isCancelable = false
                dialog.setMuteAlertCloseListener(this@ChatPageActivity)
                dialog.show(supportFragmentManager, "Mute")
            }
        } else {
            Toast.makeText(
                this@ChatPageActivity,
                resources.getString(R.string.check_your_internet_connection),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onMuteDialogClosed(isMuted: Boolean) {}
    private fun performMenuWallpaper() {
        val labelsList: MutableList<MultiTextDialogPojo> = ArrayList()
        var label = MultiTextDialogPojo()
        //        label.setImageResource(R.drawable.gallery_ic);
//        label.setLabelText(getString(R.string.gallery));
//        labelsList.add(label);
        label = MultiTextDialogPojo()
        label.imageResource = R.drawable.solidcolor_ic
        label.labelText = getString(R.string.solid_color)
        labelsList.add(label)
        label = MultiTextDialogPojo()
        label.imageResource = R.drawable.default_ic
        label.labelText = getString(R.string.Default_value)
        labelsList.add(label)

//        label = new MultiTextDialogPojo();
//        label.setImageResource(R.drawable.nowallpaper_ic);
//        label.setLabelText(getString(R.string.no_wallpaper));
//        labelsList.add(label);
        val dialog = CustomMultiTextItemsDialog()
        dialog.setTitleText(getString(R.string.wallpaper))
        dialog.setLabelsList(labelsList)
        dialog.setDialogItemClickListener { position ->
            when (position) {
                0 -> {
                    val intent = Intent(
                        context,
                        SolidColorsActivity::class.java
                    )
                    startActivity(intent)
                }
                1 -> {
                    val setbgdef = "def"
                    background!!.setImageResource(R.drawable.custom_wallpaper_1)
                    session!!.putgalleryPrefs(setbgdef)
                }
                2 -> {
                    val setbg = "no"
                    val bmp = Bitmap.createBitmap(
                        128,
                        128,
                        Bitmap.Config.ARGB_8888
                    )
                    val canvas = Canvas(bmp)
                    canvas.drawColor(Color.parseColor("#000000"))
                    background!!.setImageBitmap(bmp)
                    session!!.putgalleryPrefs("")
                    session!!.putColor("#000000")
                }
            }
        }
        dialog.show(supportFragmentManager, "Profile Pic")
    }

    private fun sendScreenShotMsg() {
        val data = " took a screenshot!"
        val messageEvent = SendMessageEvent()
        val message = MessageFactory.getMessage(MessageFactory.text, this) as TextMessage
        val msgObj = message.getMessageObject(to, data, false) as JSONObject
        try {
            msgObj.put("type", MessageFactory.SCREEN_SHOT_TAKEN)
        } catch (e: Exception) {
            MyLog.e(TAG, "sendScreenShotMsg: ", e)
        }
        messageEvent.eventName = SocketManager.EVENT_MESSAGE
        val item = message.createMessageItem(
            true,
            "You$data", MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId, mReceiverName
        )
        messageEvent.messageObject = msgObj
        item.senderMsisdn = receiverMsisdn
        item.senderName = mReceiverName
        item.messageType = MessageFactory.SCREEN_SHOT_TAKEN.toString() + ""
        messageDatabase!!.updateChatMessage(item, chatType)
        if (!isAlreadyExist(item)) mChatData!!.add(item)
        EventBus.getDefault().post(messageEvent)
        chat_list!!.postDelayed({ notifyDatasetChange() }, 100)
    }

    //------------------------------------------------------Mute click Menu-----------------------------------------------------
    private fun changeBadgeCount(docId: String) {
        session!!.Removemark(to)
        if (mConvId != null && mConvId != "") {
            //ShortcutBadgeManager shortcutBadgeMgnr = new ShortcutBadgeManager(ChatPageActivity.this);
            sharedprf_video_uploadprogress!!.removeMessageCount(mConvId, msgid)
            val totalCount = sharedprf_video_uploadprogress!!.totalCount
            // Badge working if supported devices
            if (totalCount > 0) {
                try {
                    ShortcutBadger.applyCount(context, totalCount)
                } catch (e: Exception) {
                    MyLog.e(TAG, "", e)
                }
            }
        }
    }

    override fun onKeyDown(keycode: Int, e: KeyEvent): Boolean {
        when (keycode) {
            KeyEvent.KEYCODE_MENU -> {
                isMenuBtnClick = true
                return super.onKeyDown(keycode, e)
            }
            KeyEvent.KEYCODE_BACK -> {
                if (selectedChatItems != null && selectedChatItems!!.size > 0) {
                    var i = 0
                    while (i < mChatData!!.size) {
                        mChatData!!.get(i)!!.isSelected = false
                        i++
                    }
                    mAdapter!!.notifyDataSetChanged()
                    include!!.visibility = View.VISIBLE
                    rlChatActions!!.visibility = View.GONE
                    selectedChatItems!!.clear()
                } else if (rlChatActions != null && rlChatActions!!.visibility == View.VISIBLE) {
                    Search1!!.setText("")
                    mAdapter!!.updateInfo(mChatData)
                    showUnSelectedActions()
                    val inputMethodManager =
                        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    if (this@ChatPageActivity.currentFocus != null) {
                        inputMethodManager.hideSoftInputFromWindow(
                            this@ChatPageActivity.currentFocus!!.windowToken, 0
                        )
                    }
                } else if (attachmentLayout != null && attachmentLayout!!.visibility == View.VISIBLE) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) showMenuBelowLollipop() else showMenu()
                } else {
                    if (MessageAdapter.mPlayer != null) {
                        var i = 0
                        while (i < mChatData!!.size) {
                            mChatData!![i]!!.setIsMediaPlaying(false)
                            MessageAdapter.mTimer.cancel()
                            MessageAdapter.mPlayer.release()
                            i++
                        }
                        mAdapter!!.notifyDataSetChanged()
                    }
                    onBackPressed()
                }
                return true
            }
        }
        return false
    }

    fun VideoDownloadComplete(Docid: String?, Msgid: String, localpath: String?) {
        if (mChatData == null) return
        for (i in mChatData!!.indices) {
            if (Msgid.equals(mChatData!![i]!!.messageId, ignoreCase = true)) {
                mChatData!!.get(i)!!.downloadStatus = MessageFactory.DOWNLOAD_STATUS_COMPLETED
                messageDatabase!!.updateMessageDownloadStatus(
                    Docid,
                    Msgid,
                    MessageFactory.DOWNLOAD_STATUS_COMPLETED,
                    localpath
                )
                mAdapter!!.notifyDataSetChanged()
                break
            }
        }
    }

    override fun itemClick(position: Int) {
        itemClicked(position)
    }

    override fun itemLongClick(position: Int) {
        itemLongClicked(position)
    }

    override fun screenShotTaken() {
        val isMassChat = resources.getBoolean(R.bool.is_mass_chat)
        if (isMassChat) {
            if (!chatType.equals(MessageFactory.CHAT_TYPE_GROUP, ignoreCase = true)) {
                if (chatType.equals("single", ignoreCase = true)) {
                    if (!resources.getString(R.string.app_name).contains("Neo")) sendScreenShotMsg()
                }
            }
        }
    }

    override fun downloadCompleted(msgId: String, path: String) {
        VideoDownloadComplete("", msgId, path)
    }

    override fun progress(progress: Int, msgId: String) {
        if (mChatData == null) return
        for (i in mChatData!!.indices) {
            if (msgId.equals(mChatData!![i]!!.messageId, ignoreCase = true)) {
                mChatData!!.get(i)!!.uploadDownloadProgress = progress
                mAdapter!!.notifyDataSetChanged()
                break
            }
        }
    }

    override fun DownloadError(progress: Int, msgId: String) {
        for (i in mChatData!!.indices) {
            if (msgId.equals(mChatData!![i]!!.messageId, ignoreCase = true)) {
                //  mChatData.get(i).setDownloadError(true);
                mChatData!!.get(i)!!.uploadDownloadProgress = 0
                mAdapter!!.notifyDataSetChanged()
                break
            }
        }
    }

    inner class Receiver() : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action.equals("com.groupname.change", ignoreCase = true)) {
                val data = intent.getStringExtra("object")
                var `object`: JSONObject? = null
                try {
                    `object` = JSONObject(data)
                    val groupId = `object`.getString("groupId")
                    val id = `object`.getString("id")
                    val groupPrevName = `object`.getString("prev_name")
                    var groupNewName = `object`.getString("groupName")
                    val from = `object`.getString("from")
                    val timeStamp: String
                    if (`object`.has("timeStamp")) {
                        timeStamp = `object`.getString("timeStamp")
                    } else {
                        timeStamp = `object`.getString("timestamp")
                    }
                    if (`object`.has("changed_name")) {
                        groupNewName = `object`.getString("changed_name")
                    }
                    val docId = "$mCurrentUserId-$groupId-g"
                    val message = MessageFactory.getMessage(
                        MessageFactory.group_event_info,
                        context
                    ) as GroupEventInfoMessage
                    val item = message.createMessageItem(
                        MessageFactory.change_group_name, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupNewName, from, null
                    )
                    item.prevGroupName = groupPrevName
                    item.messageId = "$docId-$id"
                    item.ts = timeStamp
                    mChatData!!.add(item)
                    notifyDatasetChange()
                    user_name!!.text = groupNewName
                    SharedPreference.getInstance().save(context, "userName", groupNewName)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else if (intent.action.equals("com.groupprofile.update", ignoreCase = true)) {
                val data = intent.getStringExtra("object")
                val image_path = intent.getStringExtra("image")
                var `object`: JSONObject? = null
                try {
                    `object` = JSONObject(data)
                    val from = `object`.getString("from")
                    val groupId = `object`.getString("groupId")
                    val groupDp: String
                    if (`object`.has("avatar")) {
                        groupDp = `object`.getString("avatar")
                    } else {
                        groupDp = `object`.getString("thumbnail")
                    }
                    val groupName = `object`.getString("groupName")
                    val timeStamp: String
                    if (`object`.has("timeStamp")) {
                        timeStamp = `object`.getString("timeStamp")
                    } else {
                        timeStamp = `object`.getString("timestamp")
                    }
                    val docId = "$mCurrentUserId-$groupId-g"
                    val message = MessageFactory.getMessage(
                        MessageFactory.group_event_info,
                        context
                    ) as GroupEventInfoMessage
                    val item = message.createMessageItem(
                        MessageFactory.change_group_icon, false, null,
                        MessageFactory.DELIVERY_STATUS_READ, groupId, groupName, from, null
                    )
                    item.avatarImageUrl = groupDp
                    item.ts = timeStamp
                    val id: String
                    if (`object`.has("id")) {
                        id = `object`.getString("id")
                    } else {
                        id = Calendar.getInstance().timeInMillis.toString() + ""
                    }
                    item.messageId = "$docId-$id"
                    mChatData!!.add(item)
                    notifyDatasetChange()
                    if (user_profile_image != null) {
                        receiverAvatar = AppUtils.getValidProfilePath(image_path)
                        AppUtils.loadImage(
                            this@ChatPageActivity,
                            AppUtils.getValidProfilePath(image_path),
                            user_profile_image,
                            100,
                            R.drawable.ic_placeholder_black
                        )
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else if (intent.action.equals("com.group.delete.members", ignoreCase = true)) {
                val data = intent.getStringExtra("object")
                try {
                    val `object` = JSONObject(data)
                    loadDeleteMemberMessage(`object`)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else if (intent.action.equals("com.group.makeadmin", ignoreCase = true)) {
                val data = intent.getStringExtra("object")
                try {
                    val `object` = JSONObject(data)
                    loadMakeAdminMessage(`object`)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    //------------------------------------------------------Default Back Press Function---------------------------------------------
    // DownloadImage AsyncTask
//    private inner class DownloadImage(var imageURL: String) :
//        AsyncTask<String?, Void?, Bitmap?>() {
//        protected override fun doInBackground(vararg URL: String): Bitmap {
//            var bitmap: Bitmap? = null
//            try {
//                // Download Image from URL
//                val input = URL(imageURL).openStream()
//                // Decode Bitmap
//                bitmap = BitmapFactory.decodeStream(input)
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//            return (bitmap)!!
//        }
//
//        protected override fun onPostExecute(result: Bitmap) {
//            ShortcutBadgeManager.addChatShortcut(
//                this@ChatPageActivitykt, isGroupChat, to, user_name!!.text.toString(),
//                receiverAvatar, receiverMsisdn, result
//            )
//        }
//    }

    //----------------------------------------Video Download Complete Update------------------
//    private class loadFileUploaded(var jsonObject_bg: JSONObject) :
//        AsyncTask<String?, Void?, String?>() {
//
//        override fun doInBackground(vararg p0: String?): String? {
//            loadFileUploaded(jsonObject_bg)
//            return ""
//        }
//
//        override fun onPostExecute(result: String) {
//            //UI thread
//        }
//    }

    private fun isBlockedUser(to: String): Boolean {
        try {
            val contactDB_sqlite = ContactDB_Sqlite(this)
            val isBlockedInSecret = (contactDB_sqlite.getBlockedStatus(to, true) == "1")
            val isBlockedInNormal = (contactDB_sqlite.getBlockedStatus(to, false) == "1")
            if (isBlockedInNormal || isBlockedInSecret) return true
        } catch (e: Exception) {
            return false
        }
        return false
    }

    private inner class MyReceiver() : BroadcastReceiver() {
        override fun onReceive(arg0: Context, arg1: Intent) {
            if (arg1.hasExtra("message")) {
                checkcallstatus()
                //  System.out.println(arg1.getStringExtra("message"));
            }
        }
    }

    private fun clearAllViews() {
        mSelectedPath = null
        mSelectedType = null
        if (selectedChatItems != null) selectedChatItems!!.clear()
        selectedChatItems = null
        if (allMembersList != null) allMembersList!!.clear()
        at_memberlist = null
        getcontactname = null
        mCurrentUserData = null
        session = null
        if (contacts != null) contacts!!.clear()
        contacts = null
        sharedprf_video_uploadprogress = null
        toTypingHandler = null
        r1messagetoreplay = null
        RelativeLayout_group_delete = null
        text_lay_out = null
        rlSend = null
        cameraphoto = null
        videoimage = null
        audioimage = null
        personimage = null
        cameraimage = null
        sentimage = null
        emai1send = null
        gmailsend = null
        myTemp = null
        contactDB_sqlite = null
        myChronometer = null
        image_to = null
        slidetocencel = null
        ivCancelRecording = null
        rootView = null
        Search1 = null
        add_contact = null
        block_contact = null
        report_spam = null
        unreadcount = null
        sb = null
        receiver = null
        chat_list = null
        messageDatabase = null
        if (mChatData != null) mChatData!!.clear()
        mChatData = null
        sessionManager = null
        userInfoSession = null
        uploadDownloadManager = null
        attachment_icon = null
        nudgeIcon = null
        translateIcon = null
        attachmentLayout = null
        camera_choose = null
        image_choose = null
        video_choose = null
        audio_choose = null
        document_choose = null
        location_choose = null
        contact_choose = null
        include = null
        mBackImage = null
        mReceiverId = null
        layout_new = null
        atAdapter = null
        capture_image = null
        selKeybord = null
        ivVoiceCall = null
        ivVideoCall = null
        sendButton = null
        groupUsername = null
        tvWebLink = null
        tvWebLinkDesc = null
        tvWebTitle = null
        message_old = null
        Documentimage = null
        dateView = null
        Ifname = null
        messagesetmedio = null
        tvBlock = null
        tvAddToContact = null
        mymsgid = null
        locationName = null
        imgpath = null
        imageAsBytes = null
        close = null
        sendMessage = null
        emailgmail = null
        record = null
        groupInfoSession = null
        if (savedMembersList != null) savedMembersList!!.clear()
        savedMembersList = null
        if (unsavedMembersList != null) unsavedMembersList!!.clear()
        unsavedMembersList = null
        rvGroupMembers = null
        myReceiver = null
        watch = null
        webLink = null
        webLinkTitle = null
        webLinkDesc = null
        webLinkImgUrl = null
        ivWebLink = null
        rlWebLink = null
        audioRecorder = null
        llAddBlockContact = null
        background = null
        chatMenu = null
        mLayoutManager = null
        iBtnScroll = null
        frameL = null
        starred_msgid = ""
        if (newMsgIds != null) newMsgIds!!.clear()
        newMsgIds = null
        relativeLayout = null
        group_left_layout = null
        user_profile_image = null
        user_name = null
        status_textview = null
        toTypingRunnable = null
        overflow = null
        name_status_layout = null
        user_info_container = null
        menu_layout = null
        call_layout = null
        rlChatActions = null
        iBtnBack2 = null
        longpressback = null
        replymess = null
        copychat = null
        showOriginal = null
        starred = null
        info = null
        delete = null
        forward = null
        share = null
        if (messageIds != null) messageIds!!.clear()
        messageIds = null
        edit_text_layout = null
        mAdapter = null
        context = null
    }

    companion object {
        var LAST_SEEN_TEXT = "last seen "
        val INTENT_KEY_FINISH_ACTIVITY_ON_SAVE_COMPLETED = "finishActivityOnSaveCompleted"
        val HTML_FRONT_TAG = "<font color=\"#01a9e5\">"
        val HTML_END_TAG = "</font>  "
        private val RESULT_LOAD_IMAGE = 1
        private val RESULT_LOAD_VIDEO = 2
        private val RESULT_CAPTURE_VIDEO = 3
        private val REQUEST_CODE_CONTACTS = 4
        private val SHARE_INTENT = 200
        private val RESULT_SHARE_LOCATION = 5
        private val CHECKING_LOCATION = 6
        private val REQUEST_SELECT_AUDIO = 7
        private val CAMERA_REQUEST = 1888
        private val RESULT_WALLPAPER = 8
        private val REQUEST_CODE_DOCUMENT = 9
        private val PICK_CONTACT_REQUEST = 10
        private val TAG = ChatPageActivity::class.java.simpleName + ":"
        var Chat_Activity: Activity? = null
        var to: String? = null
        var model: String? = null
        var friendId: String? = null
        var Chat_to: String? = null
        var isGroupChat = false
        var isKilled = false
        var isChatPage = false
        var noNeedRefresh = true
        var progressglobal = 0
        var selectedChatItems: ArrayList<MessageItemChat?>? = ArrayList()
        var allMembersList: MutableList<GroupMembersPojo>? = null
        var at_memberlist: MutableList<GroupMembersPojo>? = ArrayList()
        var Activity_GroupId: String? = ""
        var isFirstItemSelected = false
        var receiverAvatar: String? = null
        fun checkPlayServices(context: Context): Boolean {
            val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
            val api = GoogleApiAvailability.getInstance()
            val resultCode = api.isGooglePlayServicesAvailable(context)
            when (resultCode) {
                ConnectionResult.API_UNAVAILABLE -> {}
                ConnectionResult.NETWORK_ERROR -> {}
                ConnectionResult.RESTRICTED_PROFILE -> {}
                ConnectionResult.SERVICE_MISSING -> {}
                ConnectionResult.SIGN_IN_REQUIRED -> {}
                ConnectionResult.SUCCESS -> {}
            }
            if (resultCode != ConnectionResult.SUCCESS) {
                if (api.isUserResolvableError(resultCode)) {
                    api.getErrorDialog(
                        (context as Activity),
                        resultCode,
                        PLAY_SERVICES_RESOLUTION_REQUEST
                    ).show()
                } else {
                    Toast.makeText(
                        context,
                        context.getString(R.string.devicenotsupport),
                        Toast.LENGTH_SHORT
                    ).show()
                    (context as Activity).finish()
                }
                return false
            }
            return true
        }

        fun loadFromDb() {

        }
    }

    private val newMessageListener: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            lifecycleScope.launch{
                withContext(Dispatchers.IO) {
                    loadFromDB()
                }
            }
        }
    }

    private fun getAgoraCallingKey() {
        showProgressDialog()
        // prepare the Request
        val queue: RequestQueue = Volley.newRequestQueue(this)
        val url = Constants.GET_AGORA_CALLING_KEY
        val getRequest = JsonObjectRequest(Request.Method.GET, url, null,
            com.android.volley.Response.Listener<JSONObject> { `object` -> // display response
                hideProgressDialog()
                Log.e("Response", `object`.toString())
                try {
                    val app_key = `object`["agora_app_key"] as String
                    mAgoraCallingKey = app_key
                    SharedPreference.getInstance().save(context, "agora_key", app_key)
                } catch (e: java.lang.Exception) {
                    Log.e("Exception", "Exception$e")
                }
            },
            com.android.volley.Response.ErrorListener {
                hideProgressDialog()
                //     Log.d("Error.Response", error);
            }
        )

        // add it to the RequestQueue
        queue.add(getRequest)
    }
}