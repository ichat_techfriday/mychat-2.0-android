package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NationalityModel {

    String CountryCode;
    String CountryName;
    String Nationalty;

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getNationalty() {
        return Nationalty;
    }

    public void setNationalty(String nationalty) {
        Nationalty = nationalty;
    }

    public NationalityModel(String CountryCode, String CountryName, String Nationalty) {
        this.CountryCode = CountryCode;
        this.CountryName = CountryName;
        this.Nationalty = Nationalty;
    }

    @Override
    public String toString() {
        return CountryName;
    }
}
