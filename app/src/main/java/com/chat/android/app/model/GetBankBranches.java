package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetBankBranches implements Serializable {
    @SerializedName("BankBranches")
    public BankBranches BankBranches;
    @SerializedName("ErrorCode")
    public String errorCode;
    @SerializedName("ErrorDescription")
    public String errorDescription;


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public GetBankBranches.BankBranches getBankBranches() {
        return BankBranches;
    }

    public void setBankBranches(GetBankBranches.BankBranches bankBranches) {
        BankBranches = bankBranches;
    }

    public class AllBranches implements Serializable {
        @SerializedName("BranchCode")
        public String BranchCode;
        @SerializedName("BranchName")
        public String BranchName;
        @SerializedName("BranchAddress1")
        public String BranchAddress1;
        @SerializedName("BranchAddress2")
        public String BranchAddress2 ;
        @SerializedName("Branch_ID")
        public String Branch_ID;
        @SerializedName("BranchNotes")
        public String BranchNotes;
        @SerializedName("Phone")
        public String Phone;


        @Override
        public String toString() {
            return BranchName;
        }
    }

    public class BankBranches implements Serializable {
        @SerializedName("AllBankBranches")
        public List<AllBranches> allBankBranches;
    }



}
