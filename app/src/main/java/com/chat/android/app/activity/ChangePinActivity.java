package com.chat.android.app.activity;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chat.android.R;
import com.chat.android.app.utils.ConnectivityInfo;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.ErrorMessageModel;
import com.chat.android.core.model.PinCodeModel;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.service.Constants;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.DialogUtils;
import com.chat.android.utils.PincodesCacheUtility;
import com.chat.android.utils.StorageUtility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ChangePinActivity extends CoreActivity {
    private Context context;
    private EditText etPincode;
    private EditText etConfirmpincode;
    private TextView tvUserName, tvPinCodeDesc;
    private Button btnChangePin;
    private String userPinCode;
    private View parentView;
    private ImageView ivBack;
    private String username = "";
    private static final String TAG = "ChangePin";
    private String mCurrentUserId;
    private int allowedLength = 6;
    private Handler eventHandler;
    private Runnable eventRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);

        context = this;
        initViews();
    }

    private void initViews() {
        int length = 6;
        etPincode = findViewById(R.id.et_create_pin_code);
        etConfirmpincode = findViewById(R.id.et_confirm_pin_code);
        tvUserName = findViewById(R.id.tv_user_name);
        btnChangePin = findViewById(R.id.btnSave);
        tvPinCodeDesc = findViewById(R.id.tv_pin_code_desc);
        ivBack = findViewById(R.id.iv_back);
        parentView = findViewById(R.id.parent_view);

        mCurrentUserId = SessionManager.getInstance(this).getCurrentUserID();
        userPinCode = SessionManager.getInstance(this).getPinCodeOfCurrentUser();
        username = SessionManager.getInstance(this).getnameOfCurrentUser();
        tvUserName.setText(username);
        initProgress(getString(R.string.loading_in), true);
        getPinCodesFromServer();
        if (!TextUtils.isEmpty(userPinCode)) {
            length = userPinCode.length();
            etPincode.setText(userPinCode);
            etPincode.setSelection(length);
            etConfirmpincode.setText(userPinCode);
            etConfirmpincode.setSelection(length);
            tvPinCodeDesc.setText(context.getString(R.string.pin_description).replace("[?]", String.valueOf(length)));
        }
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnChangePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleChangePinBtnClick();
            }
        });
        etPincode.addTextChangedListener(textWatcher);
        etConfirmpincode.addTextChangedListener(textWatcher2);
        enableChangeButton(false);
        getSupportActionBar().hide();
    }

    private void handleChangePinBtnClick() {
        SessionManager sessionManager = SessionManager.getInstance(this);
        String currentPin = sessionManager.getPinCodeOfCurrentUser();
        if(!etConfirmpincode.getText().toString().equals(etPincode.getText().toString()))
        {
            showSuccessOrErrorMessage(context.getString(R.string.pin_code_does_not_match), true);
            etConfirmpincode.startAnimation(shakeError());
            return;

        }

        if (currentPin.equals(etPincode.getText().toString())) {
            finish();
            return;
        }
        DialogUtils.showAlertWithButtons(context, context.getString(R.string.change_pin), context.getString(R.string.change_pin_warning_msg),
                context.getString(R.string.yes), context.getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        changePin();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        dialogInterface.dismiss();
                    }
                });
    }

    private void enableChangeButton(boolean enable) {
        if (enable) {
            btnChangePin.setEnabled(true);
            btnChangePin.setBackgroundResource(R.drawable.btn_selecter_dark);
        } else {
            btnChangePin.setEnabled(false);
            btnChangePin.setBackgroundResource(R.drawable.all_button_disabled_bg);
        }
    }

    private TextWatcher  textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String temp = charSequence.toString().replaceAll("-", "");
            temp = temp.replaceAll("\u00A0", "").trim();
            int size = temp.length();
            setPinCodeBasedOnSize(temp, size);
            if (temp.equals(userPinCode)) {
//                tvError.setVisibility(View.INVISIBLE);
                enableChangeButton(true);
            } else {
                validateEnteredPin(size);
            }
            if (size <= 5 && allowedLength == 8) {
                allowedLength = 6;
                etPincode.setInputType(InputType.TYPE_CLASS_NUMBER);
                etPincode.setText(etPincode.getText().toString().substring(0, 6));
            } else if (size < 6)
                etPincode.setInputType(InputType.TYPE_CLASS_NUMBER);
            else if (allowedLength == 8 && size > 6)
                etPincode.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

            tvPinCodeDesc.setText(context.getString(R.string.pin_description).replace("[?]", String.valueOf(allowedLength)));
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    private TextWatcher textWatcher2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String temp = charSequence.toString().replaceAll("-", "");
            temp = temp.replaceAll("\u00A0", "").trim();
            int size = temp.length();
            setPinCodeBasedOnSizeConfirm(temp, size);
            if (temp.equals(userPinCode)) {
//                tvError.setVisibility(View.INVISIBLE);
                enableChangeButton(true);
            } else {
                validateEnteredPinConfirm(size);
            }
            if (size <= 5 && allowedLength == 8) {
                allowedLength = 6;
                etConfirmpincode.setInputType(InputType.TYPE_CLASS_NUMBER);
                etConfirmpincode.setText(etConfirmpincode.getText().toString().substring(0, 6));
            } else if (size < 6)
                etConfirmpincode.setInputType(InputType.TYPE_CLASS_NUMBER);
            else if (allowedLength == 8 && size > 6)
                etConfirmpincode.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private TranslateAnimation shakeError() {
        TranslateAnimation shake = new TranslateAnimation(0, 10, 0, 0);
        shake.setDuration(500);
        shake.setInterpolator(new CycleInterpolator(7));
        return shake;
    }

    private static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showSuccessOrErrorMessage(String message, boolean isError) {
        hideKeyboardFrom(context, parentView);
        if (isError) {
            ErrorMessageModel errorMessageModel = new ErrorMessageModel(context.getString(R.string.pin_code), message);
            DialogUtils.showNativeDialog(context, errorMessageModel, context.getString(R.string.retry), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    etPincode.setText("------");
                    etConfirmpincode.setText("------");
                }
            });
        } else {
            if (com.chat.android.utils.TextUtils.isGoldPin(etPincode.getText().toString()) && !etPincode.getText().toString().equals(userPinCode))
                return;
            ErrorMessageModel errorMessageModel = new ErrorMessageModel(context.getString(R.string.pin_code), context.getString(R.string.golden_pin_with_congrats));
            DialogUtils.showNativeDialog(context, errorMessageModel);
        }
    }

    private void validateEnteredPin(int size) {
        if (size >= allowedLength) {
            boolean isGolden = /*isInAppAllowed &&*/ com.chat.android.utils.TextUtils.isGoldPin(etPincode.getText().toString());
            PincodesCacheUtility pinCodesCacheUtility = PincodesCacheUtility.getInstance(context);
            if (pinCodesCacheUtility.doesPinCodeExists(etPincode.getText().toString()) && !isGolden) {
                //User picked a normal pin code which is already being taken
                etPincode.startAnimation(shakeError());
                showSuccessOrErrorMessage(context.getString(R.string.pin_code_already_taken_updated), true);
                return;
            } else if (pinCodesCacheUtility.doesPinCodeExists(etPincode.getText().toString()) && isGolden && allowedLength == 6) {
                /* user picked a golden pin of 6 characters which is already been taken, now purposing
                user to add 2 alphabets along with it */
                hideKeyboardFrom(context, parentView);
                allowedLength = 8;
                String newPin = etPincode.getText().toString() + "--";
                etPincode.setText(newPin);
                ErrorMessageModel errorMessageModel = new ErrorMessageModel(context.getString(R.string.pin_code), context.getString(R.string.golden_pin_with_congrats));
                DialogUtils.showNativeDialog(context, errorMessageModel);
                etPincode.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                return;
            } else if (pinCodesCacheUtility.doesPinCodeExists(etPincode.getText().toString()) && isGolden && allowedLength == 8) {
                // user picked a golden pin with alphabets, which is already been taken
                showSuccessOrErrorMessage(context.getString(R.string.pin_code_already_taken_updated), true);
                etPincode.startAnimation(shakeError());
                return;
            }

            enableChangeButton(true);
        } else {
            enableChangeButton(false);
        }
    }
    private void validateEnteredPinConfirm(int size) {
        if (size >= allowedLength) {
            boolean isGolden = /*isInAppAllowed &&*/ com.chat.android.utils.TextUtils.isGoldPin(etConfirmpincode.getText().toString());
            PincodesCacheUtility pinCodesCacheUtility = PincodesCacheUtility.getInstance(context);
            if (pinCodesCacheUtility.doesPinCodeExists(etConfirmpincode.getText().toString()) && !isGolden) {
                //User picked a normal pin code which is already being taken
                etConfirmpincode.startAnimation(shakeError());
                showSuccessOrErrorMessage(context.getString(R.string.pin_code_already_taken_updated), true);
                return;
            } else if (pinCodesCacheUtility.doesPinCodeExists(etConfirmpincode.getText().toString()) && isGolden && allowedLength == 6) {
                /* user picked a golden pin of 6 characters which is already been taken, now purposing
                user to add 2 alphabets along with it */
                hideKeyboardFrom(context, parentView);
                allowedLength = 8;
                String newPin = etConfirmpincode.getText().toString() + "--";
                etConfirmpincode.setText(newPin);
//                ErrorMessageModel errorMessageModel = new ErrorMessageModel(context.getString(R.string.pin_code), context.getString(R.string.golden_pin_with_congrats));
//                DialogUtils.showNativeDialog(context, errorMessageModel);
                etConfirmpincode.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                return;
            } else if (pinCodesCacheUtility.doesPinCodeExists(etConfirmpincode.getText().toString()) && isGolden && allowedLength == 8) {
                // user picked a golden pin with alphabets, which is already been taken
                showSuccessOrErrorMessage(context.getString(R.string.pin_code_already_taken_updated), true);
                etConfirmpincode.startAnimation(shakeError());
                return;
            }

            enableChangeButton(true);
        } else {
            enableChangeButton(false);
        }
    }

    private void setPinCodeBasedOnSize(String temp, int size) {
        etPincode.removeTextChangedListener(textWatcher);
        StringBuilder dashes = new StringBuilder();
        if (size <= allowedLength) {
            for (int k = 0; k < allowedLength - size; k++)
                dashes.append("-");
        } else {
            temp = temp.substring(0, allowedLength);
            size = temp.length();
        }

        SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString str1 = new SpannableString(temp);
        str1.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black_22)), 0, str1.length(), 0);
        builder.append(str1);

        SpannableString str2 = new SpannableString(dashes.toString());
        str2.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.grey_54)), 0, str2.length(), 0);
        builder.append(str2);
        etPincode.setText(builder, TextView.BufferType.SPANNABLE);
        etPincode.setSelection(size);
        etPincode.addTextChangedListener(textWatcher);
    }
 private void setPinCodeBasedOnSizeConfirm(String temp, int size) {
        etConfirmpincode.removeTextChangedListener(textWatcher2);
        StringBuilder dashes = new StringBuilder();
        if (size <= allowedLength) {
            for (int k = 0; k < allowedLength - size; k++)
                dashes.append("-");
        } else {
            temp = temp.substring(0, allowedLength);
            size = temp.length();
        }

        SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString str1 = new SpannableString(temp);
        str1.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black_22)), 0, str1.length(), 0);
        builder.append(str1);

        SpannableString str2 = new SpannableString(dashes.toString());
        str2.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.grey_54)), 0, str2.length(), 0);
        builder.append(str2);
     etConfirmpincode.setText(builder, TextView.BufferType.SPANNABLE);
     etConfirmpincode.setSelection(size);
     etConfirmpincode.addTextChangedListener(textWatcher2);
    }

    private void getPinCodesFromServer() {
        // prepare the Request
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Constants.PIN_CODE;
        String timestamp = StorageUtility.getDataFromPreferences(context, AppConstants.SPKeys.PIN_CODE_TIMESTAMP.getValue(), "");
        if (!TextUtils.isEmpty(timestamp))
            url = url + "?timestamp=\"" + timestamp + "\"";
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject object) {
                        // display response
                        Log.e("Response", object.toString());
                        try {
                            if (object.has("status")) {
                                if (object.optString("status").equals("1")) {
                                    ArrayList<PinCodeModel> pinCodeModels = null;
                                    if (object.has("pinCodes")) {
                                        Type type = new TypeToken<ArrayList<PinCodeModel>>() {
                                        }.getType();
                                        pinCodeModels = new Gson().fromJson(object.optString("pinCodes"), type);
                                        PincodesCacheUtility pinCodesCacheUtility = PincodesCacheUtility.getInstance(context);
                                        pinCodesCacheUtility.savePinCodes(context, pinCodeModels);
                                    }
                                    if (object.has("timestamp")) {
                                        String timeStamp = object.optString("timestamp");
                                        StorageUtility.saveDataInPreferences(context, AppConstants.SPKeys.PIN_CODE_TIMESTAMP.getValue(), timeStamp);
                                    }
                                }
                            }

                        } catch (Exception e) {
                            Log.e("Exception", "Exception" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //     Log.d("Error.Response", error);
                    }
                }
        );

// add it to the RequestQueue
        queue.add(getRequest);
    }

    private void changePin() {
        hideKeyboard();

        if (ConnectivityInfo.isInternetConnected(this)) {
            showProgressDialog();
            setEventTimeout();

            try {

                JSONObject pinCodeObject = new JSONObject();
                pinCodeObject.put("from", SessionManager.getInstance(context).getCurrentUserID());
                pinCodeObject.put("name", username);
                pinCodeObject.put("pinCode", etPincode.getText().toString().replaceAll("-", ""));

                SendMessageEvent userPinEvent = new SendMessageEvent();
                userPinEvent.setMessageObject(pinCodeObject);
                userPinEvent.setEventName(SocketManager.EVENT_CHANGE_USER_NAME);
                EventBus.getDefault().post(userPinEvent);

            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else {
            showToast(context, getString(R.string.check_internet_connection));
        }
    }

    private void setEventTimeout() {

        if (eventHandler == null) {
            eventHandler = new Handler();
            eventRunnable = new Runnable() {
                @Override
                public void run() {
                    Dialog loadingDialog = getLoadingDialog();
                    if (loadingDialog != null && loadingDialog.isShowing() && !isFinishing()) {
                        showToast(context, getString(R.string.call_fail_error));
                        hideProgressDialog();
                    }
                }
            };
        }
        eventHandler.postDelayed(eventRunnable, SocketManager.RESPONSE_TIMEOUT);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {

        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_USER_NAME)) {
            hideProgressDialog();
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String err = object.getString("err");

                if (err.equals("0")) {

                    String from = object.getString("from");
                    if (from.equalsIgnoreCase(mCurrentUserId)) {

                        String pinCode = object.getString("pinCode");
                        PincodesCacheUtility.getInstance(context).removePinCode(userPinCode);
                        SessionManager.getInstance(context).setUserPinCode(pinCode);
                        CoreController.getContactSqliteDBintstance(context).clearDatabase();
                        CoreController.getDBInstance(context).clearDatabase();
                        finish();
                    }
                }
            } catch (JSONException ex) {
                showToast(context, getString(R.string.call_fail_error));
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(ChangePinActivity.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(ChangePinActivity.this);
    }

}
