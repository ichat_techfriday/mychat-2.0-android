package com.chat.android.app.activity;

import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.adapter.DeviceLockSettingAdapter;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.LoginAuthUtils;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.SessionManager;
import com.kyleduo.switchbutton.SwitchButton;

public class ChatSettingsActivity extends CoreActivity {

    private SwitchButton switchButton;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_settings);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setSwitchSelection();
    }

    private void setSwitchSelection() {
        if (AppUtils.isDeviceLockEnabled(this)) {
            switchButton.setChecked(true);
        } else {
            switchButton.setChecked(false);
        }
    }

    private void initView() {
        switchButton = findViewById(R.id.switch_chat_lock);

        recyclerView = findViewById(R.id.rv_chat_settings);
        recyclerView.setLayoutManager(AppUtils.getDefaultLayoutManger(this));
        setAdapter();
        setSwitchSelection();
        //setListVisiblity();
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(LoginAuthUtils.isDeviceHasLock(ChatSettingsActivity.this)) {
                    SessionManager.getInstance(ChatSettingsActivity.this).setDeviceLock(isChecked);
                }
                else{
                    switchButton.setChecked(false);
                    Toast.makeText(ChatSettingsActivity.this,getResources().getString(R.string.device_not_have_default_lock_settings),Toast.LENGTH_SHORT).show();
                }
                //setListVisiblity();
            }
        });

        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setListVisiblity(){
        if (AppUtils.isDeviceLockEnabled(this)) {
            recyclerView.setVisibility(View.VISIBLE);
        }
        else{
            recyclerView.setVisibility(View.INVISIBLE);
        }
    }
    private void setAdapter() {
        DeviceLockSettingAdapter deviceLockSettingAdapter = new DeviceLockSettingAdapter(this);
        recyclerView.setAdapter(deviceLockSettingAdapter);
    }


}
