package com.chat.android.app.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chat.android.R;

import com.chat.android.app.dialog.CustomMultiTextItemsDialog;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.ConnectivityInfo;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.widget.AvnNextLTProDemiTextView;
import com.chat.android.app.widget.AvnNextLTProRegTextView;
import com.chat.android.app.widget.CircleImageView;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.PictureMessage;
import com.chat.android.core.model.MultiTextDialogPojo;
import com.chat.android.core.model.ReceviceMessageEvent;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.model.User;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.core.uploadtoserver.FileUploadDownloadManager;
import com.makeramen.roundedimageview.RoundedImageView;
import com.soundcloud.android.crop.Crop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.zelory.compressor.Compressor;
import io.socket.client.Socket;

/**
 * Created by CAS63 on 12/1/2016.
 */
public class UserProfile extends CoreActivity {

    private RoundedImageView ivUserProfile;
    private ImageView ivChangeUserProfile;
    private AvnNextLTProDemiTextView notification_actionbar_1;
    private AvnNextLTProRegTextView  usermessage;
    private EditText etBio, etPhoneNumber, etUserName;
    private ImageView backimg_uprofile;
    private Button btnSave;
    private final int GALLERY_REQUEST_CODE = 1;
    private final int CAMERA_REQUEST_CODE = 2;
    private final int CAMERA_PERMISSION_REQUEST_CODE = 3;
    private final int CHANGE_UNAME = 5;
    private Uri cameraImageUri;
    final Context context = this;
    private String mCurrentUserId;
    private Handler eventHandler;
    private Runnable eventRunnable;
    private boolean userNameUpdated = false;
    private boolean userStatusUpdated = false;
    private boolean errorDisplayed = false;
    Session session;
    boolean mUpload = false;
    private static final String TAG = "UserProfile";
    // private ProgressBar upload_progress;

    /* private final int REQUEST_PICK = 3;
     private final int REQUEST_CROP = 4;
 */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        String uniqueCurrentID = SessionManager.getInstance(UserProfile.this).getCurrentUserID();
        if (savedInstanceState != null) {
            cameraImageUri = Uri.parse(savedInstanceState.getString("ImageUri"));
        } else {
            cameraImageUri = Uri.parse("");
        }

        ivUserProfile = findViewById(R.id.ic_user_profile);
        ivChangeUserProfile = findViewById(R.id.ic_change_user_profile);
        etBio = findViewById(R.id.et_account_bio);
        etUserName = findViewById(R.id.et_user_name);
        usermessage = findViewById(R.id.usermessage);
        backimg_uprofile = findViewById(R.id.backarrow_userprofile);
        etPhoneNumber = findViewById(R.id.et_account_phone_number);
        btnSave = findViewById(R.id.btn_account_save);
        notification_actionbar_1 = findViewById(R.id.notification_actionbar_1);
        //    upload_progress = (ProgressBar) findViewById(R.id.upload_progress);
        getSupportActionBar().hide();

        backimg_uprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ActivityLauncher.launchSettingScreen(UserProfile.this);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserData();
            }
        });
        final SessionManager sessionMgr = SessionManager.getInstance(UserProfile.this);
        mCurrentUserId = sessionMgr.getCurrentUserID();

        //String mypic = AppUtils.getProfileFilePath(this);
        //Log.d("Whatsup Image Url", sessionMgr.getUserProfilePic() + " " + mypic);
        // Bitmap mybitmap=getBitmapFromurl();
        /*if (!mypic.isEmpty()) {
            Picasso.with(UserProfile.this).load(mypic).error(
                    R.drawable.ic_placeholder_black).into(profile);
            //new getBitmapFromurl().execute(mypic);
        }*/

        if (!SessionManager.getInstance(UserProfile.this).getcurrentUserstatus().isEmpty()) {
            etBio.setText(SessionManager.getInstance(UserProfile.this).getcurrentUserstatus());
        }
        etUserName.setText(SessionManager.getInstance(UserProfile.this).getnameOfCurrentUser());
        etPhoneNumber.setText(SessionManager.getInstance(UserProfile.this).getPhoneNumberOfCurrentUser());

        final String pic = AppUtils.getProfileFilePath(this);
        if (pic != null && !pic.isEmpty()) {
         /*   Picasso.with(this).load(pic).error(R.drawable.ic_placeholder_black)
                    .transform(new CircleTransform()).into(profile);*/


      /*   GlideUrl   glideUrl = new GlideUrl(pic,

                    new LazyHeaders.Builder()

                            .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                            .addHeader("requesttype", "site")
                            .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                            .addHeader("referer", pic)
                            .build());
            Glide.with(this).load(glideUrl).error(R.drawable.ic_placeholder_black)
                   .into(profile);*/
            GlideUrl glideUrl = null;
            //       Picasso.with(NewHomeScreenActivty.this).load(path).error(R.drawable.nav_menu_background).noPlaceholder().into(ivProfilePic);
            if (AppUtils.isEncryptionEnabled(context)) {
                glideUrl = new GlideUrl(pic,

                        new LazyHeaders.Builder()

                                .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                                .addHeader("requesttype", "site")
                                .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                                .addHeader("referer", pic)
                                .build());

            } else {
                glideUrl = new GlideUrl(pic,

                        new LazyHeaders.Builder()

                                .addHeader("authorization", SessionManager.getInstance(context).getSecurityToken())
                                .addHeader("requesttype", "site")
                                .addHeader("userid", SessionManager.getInstance(context).getCurrentUserID())
                                .addHeader("referer", pic)
                                .build());

            }

            Glide
                    .with(context)
                    .load(glideUrl)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .error(R.drawable.ic_placeholder_black)
                    .into(new SimpleTarget<Bitmap>() {

                        @Override
                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                            // TODO Auto-generated method stub
                            ivUserProfile.setImageBitmap(arg0);
                        }
                    });

        } else {
            ivUserProfile.setImageResource(R.drawable.ic_placeholder_black);
        }

        ivUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*String userAvatar = SessionManager.getInstance(UserProfile.this).getUserProfilePic();
                String[] userpic = userAvatar.split("id=");
                if (!userpic[0].equalsIgnoreCase("?")) {
                    if (!userAvatar.equalsIgnoreCase("")) {
                        Intent intent = new Intent(UserProfile.this, ImageZoom.class);
                        intent.putExtra("ProfilePath", userAvatar);
                        startActivity(intent);
                    }
                }*/
                Intent intent = new Intent(UserProfile.this, ImageZoom.class);
                intent.putExtra("ProfilePath", AppUtils.getProfileFilePath(UserProfile.this));
                startActivity(intent);
            }
        });

        initProgress(getString(R.string.loading_in), true);

        ivChangeUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUpload = false;
                //String userAvatar = SessionManager.getInstance(UserProfile.this).getUserProfilePic();
                //String[] userpic = userAvatar.split("id=");
                String profilePicPath = AppUtils.getProfileFilePath(UserProfile.this);
                List<MultiTextDialogPojo> labelsList = new ArrayList<>();
                MultiTextDialogPojo label = new MultiTextDialogPojo();
                label.setImageResource(R.drawable.chat_add_dialog_camera);
                label.setLabelText(getString(R.string.take_image_from_camera));
                labelsList.add(label);

                label = new MultiTextDialogPojo();
                label.setImageResource(R.drawable.chat_add_dialog_gallery);
                label.setLabelText(getString(R.string.add_image_from_gallery));
                labelsList.add(label);

                if (profilePicPath != null && profilePicPath.length() > 0) {
                    label = new MultiTextDialogPojo();
                    label.setImageResource(R.drawable.ic_remove_profile);
                    label.setLabelText(getString(R.string.remove_profile_picture));
                    labelsList.add(label);
                }


                CustomMultiTextItemsDialog dialog = new CustomMultiTextItemsDialog();
                dialog.setTitleText(getString(R.string.profile_picture));
                dialog.setNegativeButtonText(getString(R.string.cancel));
                dialog.setLabelsList(labelsList);

                dialog.setDialogItemClickListener(new CustomMultiTextItemsDialog.DialogItemClickListener() {
                    @Override
                    public void onDialogItemClick(int position) {
                        switch (position) {

                            case 0:
                                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                    StrictMode.setVmPolicy(builder.build());
                                }

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                                            CAMERA_PERMISSION_REQUEST_CODE);
                                } else {
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    File cameraImageOutputFile = new File(
                                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                            createCameraImageFileName());

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        cameraImageUri = FileProvider.getUriForFile(context, getPackageName(), cameraImageOutputFile);
                                    } else {
                                        cameraImageUri = Uri.fromFile(cameraImageOutputFile);
                                    }

                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
                                    intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                                    startActivityForResult(intent, CAMERA_REQUEST_CODE);
                                }

                                break;

                            case 1:
                                try {
                                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                    photoPickerIntent.setType("image/*");
                                    startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE);
                                } catch (Exception e) {
                                    MyLog.e(TAG, "onDialogItemClick: ", e);
                                }
                                break;


                            case 2:
                                if (ConnectivityInfo.isInternetConnected(getApplication())) {

                                    //    upload_progress.setVisibility(View.VISIBLE);

                                    FileUploadDownloadManager uploadDownloadManager = new FileUploadDownloadManager(context);
                                    uploadDownloadManager.setRemovePhoto(true);
                                    uploadDownloadManager.updatePropic(EventBus.getDefault());
                                    showProgres();
                                } else {
                                    showToast(context, getString(R.string.check_internet_connection));
                                }
                                break;

                        }
                    }
                });
                dialog.show(getSupportFragmentManager(), "Profile Pic");
            }
        });

        initProfilePicData();
    }

    private void initProfilePicData() {
        if (getIntent().getAction() != null && getIntent().getData() != null
                && getIntent().getAction().equalsIgnoreCase(Intent.ACTION_ATTACH_DATA)) {
            String path = getIntent().getData().getPath();
            if (path == null) {
                path = getRealFilePath(getIntent());
            }

            if (path != null && !path.equals("")) {
                beginCrop(getIntent().getData());
            }
        }
    }

    private String getRealFilePath(Intent data) {
        Uri selectedImage = data.getData();
        String wholeID = DocumentsContract.getDocumentId(selectedImage);
        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);
        String filePath = "";
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }


    private String createCameraImageFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return timeStamp + ".jpg";
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("ImageUri", cameraImageUri.toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    Uri selectedImageUri = data.getData();
                    if (ConnectivityInfo.isInternetConnected(getApplication())) {
                        beginCrop(selectedImageUri);
                    } else {
                        showToast(context, getString(R.string.check_internet_connection));
                    }
                }
            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    showToast(context, getString(R.string.failed_to_capture_image));
                }
            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (ConnectivityInfo.isInternetConnected(getApplication())) {
                    beginCrop(cameraImageUri);
                } else {
                    showToast(context, getString(R.string.check_internet_connection));
                }


            } else {
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    showToast(context, getString(R.string.failed_to_capture_image));
                }
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            Uri uri = Crop.getOutput(data);

            String filePath = uri.getPath();
            File image = new File(filePath);
            Bitmap compressBmp = Compressor.getDefault(UserProfile.this).compressToBitmap(image);

            //Bitmap alignedBitmap = ScimboImageUtils.getAlignedBitmap(compressBmp, filePath);
            ivUserProfile.setImageBitmap(compressBmp);
            if (!mUpload) {
                uploadImage(compressBmp);

                mUpload = true;
            }
        } else if (resultCode == RESULT_OK && requestCode == CHANGE_UNAME) {
            Boolean message = data.getBooleanExtra("name", false);
            if (message) {
                etUserName.setText(SessionManager.getInstance(UserProfile.this).getnameOfCurrentUser());

            }

        }
    }


    private void uploadImage(Bitmap circleBmp) {

        if (circleBmp != null) {
            try {

                showProgres();
                //String profileImgPath = imgDir + "/" + Calendar.getInstance().getTimeInMillis() + "_pro.jpg";
                File file = getBitmapFile(circleBmp, "pro_" + Calendar.getInstance().getTimeInMillis() + ".jpg");
                //File compressFile = Compressor.getDefault(UserProfile.this).compressToFile(file);
                //    Log.d("USER_PROFILE", "uploadImage: " + file.getAbsolutePath());
                //     Log.d("USER_PROFILE", "file exist: " + file.exists());

                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + file.getAbsolutePath())));
                String serverFileName = mCurrentUserId.concat(".jpg");
                PictureMessage message = new PictureMessage(UserProfile.this);
                JSONObject object = (JSONObject) message.createUserProfileImageObject(serverFileName, file.getAbsolutePath());

                MessageDbController db = CoreController.getDBInstance(context);
                try {

                    db.userpicinsertitem(serverFileName, "uploading", object);
                //    db.FirsttimeFileUploadinsert(serverFileName, "firsttimestoredb", object);

                    Log.e(TAG, "userpicinsertitem" + object);

                    // setUploadProgress(msgId, 1, object);
                } catch (Exception e) {
                    MyLog.e(TAG, "", e);
                }
                FileUploadDownloadManager fileUploadDownloadMgnr = new FileUploadDownloadManager(UserProfile.this);
                Log.d(TAG, "onClick: startFileUpload15");
                fileUploadDownloadMgnr.startFileUpload(EventBus.getDefault(), object);
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }
        }
    }


    private File getBitmapFile(Bitmap imageToSave, String fileName) {


        String path;
        path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString();
/*        File direct = new File(Environment.getExternalStorageDirectory() + "/Scimbo");

        if (!direct.exists()) {

           boolean created= direct.mkdirs();
           direct.setWritable(true);

            Log.d("USER_PROFILE", "dirCreated: "+created);


        }
        else {
            Log.d("USER_PROFILE", "dir exists ");
        }*/
        File file = new File(path + "/" + fileName);

        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            //     Log.e("USER_PROFILE", "getBitmapFile: " + e.getMessage());
        }

        return file;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ReceviceMessageEvent event) {

        if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_IMAGE_UPLOAD)) {
            hidepDialog();
            Object[] array = event.getObjectsArray();
            String path = "";
            try {
                JSONObject objects = new JSONObject(array[0].toString());
                Log.e(TAG, "objects" + objects);
                String err = objects.getString("err");
                String message = objects.getString("message");
//                String removePhoto = objects.getString("removePhoto");
                String from = objects.getString("from");
                String type = objects.getString("type");

                String removePhoto = null;
                if (objects.has("removePhoto")) {
                    removePhoto = objects.getString("removePhoto");

                    if (removePhoto.equalsIgnoreCase("yes")) {
                        showToast(context, getString(R.string.profile_image_removed_successfully));
                    }
                }

                if (from.equalsIgnoreCase(SessionManager.getInstance(UserProfile.this).getCurrentUserID()) && type.equalsIgnoreCase("single")) {
                    if (!objects.getString("file").equals("") || removePhoto != null) {
                        path = objects.getString("file") + "?id=" + Calendar.getInstance().getTimeInMillis();
                        final String finalPath = AppUtils.getValidProfilePath(path);
                        if (removePhoto.equalsIgnoreCase("yes")) {
                            //  message = "Profile image removed successfully";
                            mUpload = false;
                            SessionManager.getInstance(UserProfile.this).setUserProfilePic("");
                            ivUserProfile.setImageResource(R.drawable.ic_placeholder_black);
                        } else {
                            mUpload = false;
                            SessionManager.getInstance(UserProfile.this).setUserProfilePic(finalPath);
                            Glide
                                    .with(context)
                                    .load(/*profilePicPath*/AppUtils.getGlideURL(finalPath, context))
                                    .asBitmap()
                                    .error(R.drawable.ic_placeholder_black)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .into(new SimpleTarget<Bitmap>() {

                                        @Override
                                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                            // TODO Auto-generated method stub
                                            ivUserProfile.setImageBitmap(arg0);
                                            showToast(context, getString(R.string.profileupdated));
                                        }
                                    });
                        }
                        //   SessionManager.getInstance(UserProfile.this).setUserProfilePic(finalPath);
                    }
                }
            } catch (Exception e) {
                MyLog.e(TAG, "", e);
            }


        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_PROFILE_STATUS)) {
            hideProgressDialog();
            Object[] array = event.getObjectsArray();
            try {
                JSONObject object = new JSONObject(array[0].toString());

                String err = object.getString("err");
                if (err.equalsIgnoreCase("0")) {

                    String from = object.getString("from");
                    if (from.equalsIgnoreCase(mCurrentUserId)) {
                        String message = object.getString("message");
                        String status = object.getString("status");

                        byte[] decodeStatus = Base64.decode(status, Base64.DEFAULT);
                        status = new String(decodeStatus, StandardCharsets.UTF_8);
                        etBio.setText(status);
                        userStatusUpdated = true;
                        if(userNameUpdated) {
                            showToast(context, getString(R.string.user_profile_updated));
                            finish();
                        }
                    }
                } else {
                    userStatusUpdated = false;
                    if(!errorDisplayed) {
                        showToast(context, getString(R.string.call_fail_error));
                        errorDisplayed = true;
                    }
                }
            } catch (JSONException e) {
                showToast(context, getString(R.string.call_fail_error));
                userStatusUpdated = false;
                MyLog.e(TAG, "", e);
            }
        } else if (event.getEventName().equalsIgnoreCase(SocketManager.EVENT_CHANGE_USER_NAME)) {
            hideProgressDialog();
            try {
                Object[] obj = event.getObjectsArray();
                JSONObject object = new JSONObject(obj[0].toString());
                String err = object.getString("err");

                if (err.equals("0")) {

                    String from = object.getString("from");
                    if (from.equalsIgnoreCase(mCurrentUserId)) {

                        String name = object.getString("name");
                        SessionManager.getInstance(UserProfile.this).setnameOfCurrentUser(name);
                        etUserName.setText(name);
                        userNameUpdated = true;
                        if(userStatusUpdated) {
                            showToast(context, getString(R.string.user_profile_updated));
                            finish();
                        }
                    }
                } else {
                    userNameUpdated = false;
                    if(!errorDisplayed) {
                        showToast(context, getString(R.string.call_fail_error));
                        errorDisplayed = true;
                    }
                }
            } catch (JSONException ex) {
                showToast(context, getString(R.string.call_fail_error));
                userNameUpdated = false;
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(UserProfile.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(UserProfile.this);
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap circuleBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(circuleBitmap);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return circuleBitmap;
    }

    private void beginCrop(Uri source) {
        MyLog.d(TAG, "beginCrop: ");
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
       /* if (resultCode == RESULT_OK) {
            profile.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        etBio.setText(SessionManager.getInstance(UserProfile.this).getcurrentUserstatus());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        try {
            bytes.close();
        } catch (IOException e) {
            MyLog.e(TAG, "", e);
        }
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void saveUserData() {
        errorDisplayed = false;
        userNameUpdated = false;
        userStatusUpdated = false;
        if (etUserName.getText().toString().trim().length() == 0) {
            showToast(context, getString(R.string.empty_profile_name_alert));
            return;
        }
        if (ConnectivityInfo.isInternetConnected(this)) {
            showProgressDialog();
            setEventTimeout();

            try {

                JSONObject userNameObject = new JSONObject();
                userNameObject.put("from", SessionManager.getInstance(UserProfile.this).getCurrentUserID());
                userNameObject.put("name", etUserName.getText().toString());
                userNameObject.put("pinCode",SessionManager.getInstance(UserProfile.this).getPinCodeOfCurrentUser());

                SendMessageEvent userNameEvent = new SendMessageEvent();
                userNameEvent.setMessageObject(userNameObject);
                userNameEvent.setEventName(SocketManager.EVENT_CHANGE_USER_NAME);
                EventBus.getDefault().post(userNameEvent);

                JSONObject statusObject = new JSONObject();
                statusObject.put("from", SessionManager.getInstance(UserProfile.this).getCurrentUserID());
                statusObject.put("status", etBio.getText().toString());

                SendMessageEvent statusEvent = new SendMessageEvent();
                statusEvent.setMessageObject(statusObject);
                statusEvent.setEventName(SocketManager.EVENT_CHANGE_PROFILE_STATUS);
                EventBus.getDefault().post(statusEvent);

            } catch (JSONException e) {
                MyLog.e(TAG, "", e);
            }
        } else {
            showToast(context, getString(R.string.check_internet_connection));
        }
    }

    private void setEventTimeout() {

        if(eventHandler == null) {
            eventHandler = new Handler();
            eventRunnable = new Runnable() {
                @Override
                public void run() {
                    Dialog loadingDialog = getLoadingDialog();
                    if (loadingDialog != null && loadingDialog.isShowing() && !isFinishing()) {
                        showToast(context, getString(R.string.call_fail_error));
                        hideProgressDialog();
                    }
                }
            };
        }
        eventHandler.postDelayed(eventRunnable, SocketManager.RESPONSE_TIMEOUT);
    }
}
