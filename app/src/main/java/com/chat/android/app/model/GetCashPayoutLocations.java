package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetCashPayoutLocations implements Serializable {
    @SerializedName("Payout_Locations")
    public PayoutLocations PayoutLocations;


    public GetCashPayoutLocations.PayoutLocations getPayoutLocations() {
        return PayoutLocations;
    }

    public void setPayoutLocations(GetCashPayoutLocations.PayoutLocations payoutLocations) {
        PayoutLocations = payoutLocations;
    }

    public class AvailableLocation implements Serializable{
        @SerializedName("Payout_Location_Code")
        public String payout_Location_Code;
        @SerializedName("Payout_Location_Name")
        public String payout_Location_Name;
        @SerializedName("Payout_Location_Notes")
        public String payout_Location_Notes;
        @SerializedName("Payout_Location_Address1")
        public Object payout_Location_Address1;
        @SerializedName("Payout_Location_Address2")
        public Object payout_Location_Address2;
        @SerializedName("Payout_Location_Phone")
        public String payout_Location_Phone;
        @SerializedName("Payout_Location_ID")
        public String payout_Location_ID;
        @SerializedName("Service_Provider_Code")
        public String service_Provider_Code;
        @SerializedName("Service_Provider_Name")
        public String service_Provider_Name;
        @Override
        public String toString() {
            return payout_Location_Name;
        }
    }

    public class PayoutLocations implements Serializable{
        @SerializedName("AvailableLocations")
        public List<AvailableLocation> availableLocations;
    }

    public class Root implements Serializable{
        @SerializedName("Payout_Locations")
        public PayoutLocations payout_Locations;
        @SerializedName("ErrorCode")
        public String errorCode;
        @SerializedName("ErrorDescription")
        public String errorDescription;
    }



}
