package com.chat.android.app.activity;

import static com.chat.android.backend.Constants.TestingIMEI;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.model.RegisterCashPlusResponse;
import com.chat.android.app.model.VerificationPinResponseModel;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.backend.ApiCalls;
import com.chat.android.backend.Constants;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.SessionManager;
import com.chat.android.interfaces.INetworkResponseListener;
import com.chat.android.utils.TextUtils;

public class CashplusPinActivity extends AppCompatActivity implements INetworkResponseListener {
    private Button mConfirmButton;
    private ImageView iv_back;
    private EditText pinCode;
    private View parentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cashplus_pin_verification);
        getSupportActionBar().hide();
        mConfirmButton = findViewById(R.id.btnSave);
        pinCode = findViewById(R.id.et_create_pin_code);
        iv_back = findViewById(R.id.iv_back);
        parentView = findViewById(R.id.parent_view);
        enableChangeButton(false);
        pinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(charSequence)) {
                    pinCode.setHint("");
                } else {
                    pinCode.setHint("----");
                }
                checkAndEnableGoButton(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VerifyPinApi();
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        RegisterUser();

    }

    private void RegisterUser() {
        ApiCalls.getInstance(getApplicationContext()).RegisterMobileUser(getApplicationContext(),  SessionManager.getInstance(this).getPhoneNumberOfCurrentUser(), TestingIMEI, this);
    }

    private void VerifyPinApi() {
        ApiCalls.getInstance(getApplicationContext()).VerifyMobileUser(getApplicationContext(), SessionManager.getInstance(this).getPhoneNumberOfCurrentUser(), TestingIMEI, Integer.parseInt(pinCode.getText().toString()), Constants.myChat, this);
    }


    private void enableChangeButton(boolean enable) {
        if (enable) {
            mConfirmButton.setEnabled(true);
            mConfirmButton.setBackgroundResource(R.drawable.btn_selecter_dark);
        } else {
            mConfirmButton.setEnabled(false);
            mConfirmButton.setBackgroundResource(R.drawable.all_button_disabled_bg);
        }
    }

    private void checkAndEnableGoButton(String number) {
        if (number.length() == 4)
            enableChangeButton(true);
        else
            enableChangeButton(false);
    }

    private void GeneratePassword(String pwd) {
        String password = AppUtils.md5(pwd);
        ActivityLauncher.launchCashplusPasswordActivity(CashplusPinActivity.this, password);
    }

    public void onNetworkResponse(boolean status, String message, String responseForRequest, Object body) {
        switch (responseForRequest) {
            case Constants.RegisterMobileUser:
                if (status) {
                    RegisterCashPlusResponse registerCashPlusResponse = (RegisterCashPlusResponse) body;
                } else
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                break;
            case Constants.Verification:
                if (status) {

                    VerificationPinResponseModel verificationPinResponseModel = (VerificationPinResponseModel) body;
                    if (verificationPinResponseModel.getErrorCode().equals("00")) {
                        GeneratePassword(verificationPinResponseModel.getpWD());
                    } else {
                        Toast.makeText(getApplicationContext(), "Verification failed, please try again", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                } else
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                break;
        }


    }

}

