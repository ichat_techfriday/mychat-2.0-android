package com.chat.android.app.activity;

import android.Manifest;
import android.accounts.AccountAuthenticatorActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.SharedPreference;
import com.chat.android.core.ActivityLauncher;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.service.ContactsSync;
import com.chat.android.core.socket.MessageService;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.AudioPlayerUtils;
import com.chat.android.utils.MyChatUtils;
import com.chat.android.utils.StorageUtility;
import com.chat.android.utils.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResult;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.security.MessageDigest;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

import static android.Manifest.permission.RECORD_AUDIO;
import static com.chat.android.app.activity.VerifyPhoneScreen.SKIP_OTP_VERIFICATION;

/**
 * Created by Administrator on 10/7/2016.
 */
public class IntialLoaderActivityScimbo extends AccountAuthenticatorActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    RelativeLayout rlParent;
    ImageView splash;
    Handler mHandler = new Handler();
    private final int CONTACTS_REQUEST_CODE = 10;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    private boolean mShowToast;
    final int PERMISSION_REQUEST_CODE = 111;
    private Handler handleCheckStatus = null;
    private final int AUDIO_RECORD_PERMISSION_REQUEST_CODE = 1;

    private Context mContext;
    int SPLASH_TIME = 2500;
    String GCM_Id = "";
    private static final String TAG = IntialLoaderActivityScimbo.class.getSimpleName();
    private String msisdn = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader_scimboo);
        splash = findViewById(R.id.splash);

        rlParent = findViewById(R.id.rlParent);
        mContext = this;
        AudioPlayerUtils.playSound(this, R.raw.msn_message);
        MyChatUtils.setLocale(this, StorageUtility.getDataFromPreferences(this,AppConstants.KEY_USER_LANGUAGE,AppConstants.USER_DEFAULT_LANGUAGE));
        MyLog.d(TAG, "performance onCreate: ");

        handleCheckStatus = new Handler();

        mGoogleApiClient = new GoogleApiClient.Builder(IntialLoaderActivityScimbo.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                MyLog.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //if (SessionManager.getInstance(mContext).getlogin()) {
            //loadActivity(SessionManager.getInstance(mContext));
//        } else {
//
//            if (Build.VERSION.SDK_INT >= 23) {
//                // Marshmallow+
//                if (!checkWriteExternalStoragePermission() || !checkphonecall() || !canRecordAudio()) {
//                    requestPermission();
//                } else {
//                    setLocation();
//                }
//            } else {
//                setLocation();
//            }
//        }
        navigationHandling();
        MyLog.d(TAG, "performance onCreate: 2");
    }

    private void navigationHandling() {
        Intent actionIntent = getIntent();
        String action = actionIntent.getAction();
        if(Intent.ACTION_VIEW.equals(action)) {
            SPLASH_TIME = 3000;
        }
        loadActivity(SessionManager.getInstance(mContext));
    }

    private void loadActivity(final SessionManager mSessionManager) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MyLog.d("loadActivity : " + msisdn);
                boolean isLanguageSet = StorageUtility.getDataFromPreferences(IntialLoaderActivityScimbo.this, AppConstants.SPKeys.LANGUAGE.getValue(), false);
                if(!isLanguageSet) {
                    ActivityLauncher.launchLanguage(IntialLoaderActivityScimbo.this);
                    return;
                }
                if (!mSessionManager.getlogin()) {
                    ActivityLauncher.launchVerifyPhoneScreen(IntialLoaderActivityScimbo.this);
                } else if (!mSessionManager.getIsprofileUpdate()) {
                    ActivityLauncher.launchProfileInfoScreen(IntialLoaderActivityScimbo.this, null);
                } else if (!mSessionManager.getBackupRestored()) {

                    if (!MessageService.isStarted()) {
                        //  AppUtils.startService(mContext, MessageService.class);
                        startService();
                    }
                    ActivityLauncher.launchHomeScreen(IntialLoaderActivityScimbo.this);

                } else {

                    if (!MessageService.isStarted()) {
                        startService();
                    }
                    ActivityLauncher.launchHomeScreen(IntialLoaderActivityScimbo.this);
                }
                finish();
            }
        }, SPLASH_TIME);

    }

    public void startService() {

        AppUtils.startService(IntialLoaderActivityScimbo.this, MessageService.class);

    }

    private void loadContacts() {
        CoreController.registerContactObserver();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppUtils.isNetworkAvailable(mContext)) {


                    if (AppUtils.isServiceRunning(mContext, ContactsSync.class)) {
                        mHandler.removeCallbacksAndMessages(null);
                        if (SessionManager.getInstance(mContext).getlogin()) {
                            loadActivity(SessionManager.getInstance(mContext));
                        } else {
                            SessionManager sessionManager = SessionManager.getInstance(IntialLoaderActivityScimbo.this);
                            loadActivity(sessionManager);
                        }
                    } else {

                        int contactsCount = 0;

                        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                        contactsCount = contactDB_sqlite.getContactsCount();
                        if (contactsCount == 0) {
                            if (!ContactsSync.isStarted)
                                //    AppUtils.startService(mContext, ContactsSync.class);
                                startService();
                        }

                        mHandler.removeCallbacksAndMessages(null);
                        if (SessionManager.getInstance(mContext).getlogin()) {
                            loadActivity(SessionManager.getInstance(mContext));
                        } else {
                            // mHandler.postDelayed(runnable, 3000);
                            SessionManager sessionManager = SessionManager.getInstance(IntialLoaderActivityScimbo.this);
                            loadActivity(sessionManager);

                        }
                        //}
                    }

                } else {
                    //     Toast.makeText(mContext, getString(R.string.networkerror), Toast.LENGTH_SHORT).show();


                    if (getString(R.string.app_name).equals("Scimbo")) {
                        if (!mShowToast) {
                            showAToast(getResources().getString(R.string.networkerror));

                            mShowToast = true;

                        }
                    } else {
                        showAToast(getResources().getString(R.string.networkerror));
                    }
                    mHandler.postDelayed(this, 3000);
                }

            }
        }, 700);

    }

    public void showAToast(String message) {
        CoreActivity.showToast(this, message);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String permissions[], int[] grantResults) {
        switch (requestCode) {


            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CoreController.getLocation();
                    setLocation();
                } else {
                    finish();
                }
                break;
            case CONTACTS_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //   loadContacts();
                    setLocation();
                } else {
                    finish();
                }
                break;
            case AUDIO_RECORD_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadContacts();
                } else {
                    finish();
                }
                break;
        }

    }

    public boolean canRecordAudio() {
        int recordPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return recordPermission == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkphonecall() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        if (CoreController.isRaad) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE
                    , Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);

        } else
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    private void setLocation() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACTS_REQUEST_CODE);
            } else if (checkSelfPermission(Manifest.permission.RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, AUDIO_RECORD_PERMISSION_REQUEST_CODE);

            } else {
                loadContacts();
            }
        } else {
            loadContacts();
        }
    }

    private void AfterGpsActivesetLocation() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACTS_REQUEST_CODE);
            } else {
                loadContacts();
            }
        } else {
            loadContacts();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.e(TAG, "onActivityResult");
        if (requestCode == 9) {
            if (resultCode == RESULT_OK) {
                loadContacts();
            }
        }

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handleCheckStatus != null) {
            handleCheckStatus.removeCallbacksAndMessages(null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override public void onStart() {
        super.onStart();
        MyLog.d(TAG, "in on start ");
        Branch.sessionBuilder(this).withCallback(branchReferralInitListener).withData(getIntent() != null ? getIntent().getData() : null).init();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        MyLog.d(TAG, "in on onNewIntent ");
        // if activity is in foreground (or in backstack but partially visible) launching the same
        // activity will skip onStart, handle this case with reInitSession
        Branch.sessionBuilder(this).withCallback(branchReferralInitListener).reInit();
    }

    private Branch.BranchReferralInitListener branchReferralInitListener = new Branch.BranchReferralInitListener() {
        @Override
        public void onInitFinished(JSONObject linkProperties, BranchError error) {
            // do stuff with deep link data (nav to page, display content, etc)
            if (error == null) {
                try {
                    if (linkProperties != null && !linkProperties.toString().equals("{}")) {
                        JSONObject json = new JSONObject(linkProperties.toString());
                        if (json != null && json.has("msisdn"))
                            msisdn = json.getString("msisdn");
                        MyLog.d(TAG, "branch io : " + json + " ,,, " + msisdn);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
