package com.chat.android.app.web;

import com.chat.android.core.model.SaveSubDataResponse;
import com.chat.android.core.model.UserSubscription;
import com.chat.android.core.model.ValidateSubResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;


public interface IChatWithoutHeaderAPI {

    @GET
    Call<ResponseBody> translateText(@Url String url);


    @GET("cancelCallFromNotification")
    Call<ResponseBody> disconnectCall(@Query("roomid") String saveCustomerModel);

    @GET("appstore/purchase/get")
    Call<ValidateSubResponse> validateSubscription(@Query("user_id") String userId,
                                                   @Query("sub_id") String subId,
                                                   @Query("purchaseToken") String purchaseToken,
                                                   @Query("device_id") String deviceId,
                                                   @Query("purchase_time") long purchaseTime);

    @GET("appstore/purchase")
    Call<SaveSubDataResponse> saveSubPurchaseData(@Query("user_id") String userId,
                                                  @Query("sub_id") String subId,
                                                  @Query("purchaseToken") String purchaseToken,
                                                  @Query("device_id") String deviceId,
                                                  @Query("purchase_time") long purchaseTime);
}
