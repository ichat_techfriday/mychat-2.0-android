package com.chat.android.app.callbacks;

public interface CallbackListener {
    void callback(String result);
}
