package com.chat.android.app.model;

import com.google.gson.annotations.SerializedName;

public class CashplusLoginModel {
    @SerializedName("UserName")
    String UserName;
    @SerializedName("Password")
    String Password;
    @SerializedName("RefreshToken")
    boolean RefreshToken;


    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public boolean isRefreshToken() {
        return RefreshToken;
    }

    public void setRefreshToken(boolean refreshToken) {
        RefreshToken = refreshToken;
    }
}
