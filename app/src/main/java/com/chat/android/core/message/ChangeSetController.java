package com.chat.android.core.message;

import android.content.Context;

import com.chat.android.app.utils.MyLog;
import com.chat.android.core.SessionManager;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/**
 */
public class ChangeSetController {
    static Context context;
    private static final String TAG = "ChangeSetController";
    public ChangeSetController(Context context) {
        ChangeSetController.context = context;
    }

    public static void setChangeStatus(String status) {

        SendMessageEvent messageEvent = new SendMessageEvent();
        messageEvent.setEventName(SocketManager.EVENT_CHANGE_ST);
        JSONObject obj = new JSONObject();
        try {
            obj.put("from", SessionManager.getInstance(context).getCurrentUserID());
            obj.put("status", status);
        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }
        MyLog.e(TAG,"setChangeStatus"+obj);
        messageEvent.setMessageObject(obj);
        EventBus.getDefault().post(messageEvent);
    }

}
