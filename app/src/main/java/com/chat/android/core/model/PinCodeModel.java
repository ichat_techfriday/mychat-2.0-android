package com.chat.android.core.model;

import com.google.gson.annotations.SerializedName;

public class PinCodeModel {
    @SerializedName("msisdn")
    private String phoneNumber;
    @SerializedName("pinCode")
    private String pinCode;
    @SerializedName("Name")
    private String name;

    public PinCodeModel() {
    }

    public PinCodeModel(String phoneNumber, String pinCode) {
        this.phoneNumber = phoneNumber;
        this.pinCode = pinCode;
    }

    public PinCodeModel(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getPinCode() {
        return pinCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getName() {
        return name;
    }
}
