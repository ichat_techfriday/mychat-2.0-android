package com.chat.android.core;

import android.app.Activity;
import android.content.Intent;

import com.chat.android.app.activity.AboutHelp;
import com.chat.android.app.activity.About_contactus;
import com.chat.android.app.activity.Account_main_list;
import com.chat.android.app.activity.BuySubscription;
import com.chat.android.app.activity.CashpluPasswordActivity;
import com.chat.android.app.activity.CashplusPinActivity;
import com.chat.android.app.activity.CashplusUpdateProfile;
import com.chat.android.app.activity.ChangeNumber_sub;
import com.chat.android.app.activity.ChangePinActivity;
import com.chat.android.app.activity.ChatBackUpRestoreActivity;
import com.chat.android.app.activity.ChatHistory;
import com.chat.android.app.activity.ChatPageActivity;
import com.chat.android.app.activity.ChatSettingsActivity;
import com.chat.android.app.activity.ChatWallpaperActivity;
import com.chat.android.app.activity.Chatbackup;
import com.chat.android.app.activity.ChooseCountryScreen;
import com.chat.android.app.activity.ContactSettings;
import com.chat.android.app.activity.LanguageActivity;
import com.chat.android.app.activity.NewHomeScreenActivty;
import com.chat.android.app.activity.NewgroupListActivity;
import com.chat.android.app.activity.NotificationSettings;
import com.chat.android.app.activity.PaymentContactList;
import com.chat.android.app.activity.PhoneInputScreen;
import com.chat.android.app.activity.RenewSubscriptionActivity;
import com.chat.android.app.activity.ScimboNewPageAbout;
import com.chat.android.app.activity.ScimboProfileInfoScreen;
import com.chat.android.app.activity.ScimboProfileScreen;
import com.chat.android.app.activity.ScimboSettings;
import com.chat.android.app.activity.ScimboSmsVScreen;
import com.chat.android.app.activity.SendPaymentActivity;
import com.chat.android.app.activity.SettingContact;
import com.chat.android.app.activity.SharePinActivity;
import com.chat.android.app.activity.Status;
import com.chat.android.app.activity.Systemstatus;
import com.chat.android.app.activity.UserProfile;
import com.chat.android.app.activity.VerifyPhoneScreen;
import com.chat.android.app.activity.WelcomeScreen;
import com.chat.android.app.utils.MyLog;


/**
 * Created by  CASPERON TECH on 10/5/2016.
 */
public class ActivityLauncher {
    private static final String TAG = "ActivityLauncher";

    public static void launchVerifyPhoneScreen(Activity context) {
        Intent intent = new Intent(context, VerifyPhoneScreen.class);
        // intent.putExtra("Text", true);
        context.startActivity(intent);
        context.finish();
    }

    public static void launchAccount(Activity context) {
        Intent intent = new Intent(context, Account_main_list.class);
        context.startActivity(intent);
//        context.finish();
        //  context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public static void launchChangenumber2(Activity context) {
        Intent intent = new Intent(context, ChangeNumber_sub.class);
        context.startActivity(intent);
//        context.finish();
        //  context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public static void launchChatSettings(Activity context) {
        Intent intent = new Intent(context, ChatSettingsActivity.class);
        context.startActivity(intent);
//        context.finish();
        //  context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public static void launchChatHistory(Activity context) {
        Intent intent = new Intent(context, ChatHistory.class);
        context.startActivity(intent);
//        context.finish();
    }

    public static void launchSystemstatus(Activity context) {
        Intent intent = new Intent(context, Systemstatus.class);
        context.startActivity(intent);
//        context.finish();
        //context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public static void launchAboutnew(Activity context) {
        Intent intent = new Intent(context, ScimboNewPageAbout.class);
        context.startActivity(intent);
//        context.finish();
    }

    public static void launchSettingContactScreen(Activity context) {
        Intent intent = new Intent(context, SettingContact.class);
        context.startActivity(intent);
    }

    public static void launchPaymentContactScreen(Activity context) {
        Intent intent = new Intent(context, PaymentContactList.class);
        context.startActivity(intent);
    }

    public static void launchGroupInvitationPage(Activity context) {
        Intent intent = new Intent(context, NewgroupListActivity.class);
        context.startActivity(intent);
        // context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public static void launchAbouthelp(Activity context) {
        Intent intent = new Intent(context, AboutHelp.class);
        context.startActivity(intent);
        // context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public static void launchProfileInfoScreen(Activity context, String msisdn) {
        Intent intent = new Intent(context, ScimboProfileInfoScreen.class);
        if (msisdn != null)
            intent.putExtra("msisdn", msisdn);
 //       context.startActivityForResult(intent, 9);
        context.startActivity(intent);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.finish();
    }

    public static void launchPhonenumberInputScreen(Activity context) {
        Intent intent = new Intent(context, PhoneInputScreen.class);
        context.startActivityForResult(intent, 9);
    }

    public static void launchBackUpRestoreScreen(Activity context) {
        Intent intent = new Intent(context, ChatBackUpRestoreActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        context.finish();
    }


    public static void launchProfileScreen(Activity context) {
        Intent intent = new Intent(context, ScimboProfileScreen.class);
        context.startActivity(intent);
        // context.finish();
        // context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
        //  context.finish();
    }
    /*public static void launchWebViewScreen(Activity context) {
        Intent intent = new Intent(context, Webview.class);
        context.startActivity(intent);
    }*/

    public static void launchStatusScreen(Activity context) {
        Intent intent = new Intent(context, Status.class);
        context.startActivity(intent);
//        context.finish();
    }


    public static void launchSettingScreen(Activity context) {
        Intent intent = new Intent(context, ScimboSettings.class);
        context.startActivity(intent);
        // context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }


    public static void launchHomeScreen(Activity context) {
        MyLog.d(TAG, "performance launchHomeScreen: ");
        Intent intent = new Intent(context, NewHomeScreenActivty.class);
        context.startActivity(intent);
        //context.overridePendingTransition(0, 0);
        context.finish();
    }

    public static void launchHomeScreenPaymentTab(Activity context) {
        Intent intent = new Intent(context, NewHomeScreenActivty.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("OpenPaymentTab", true);
        context.startActivity(intent);
        context.finish();
    }

    public static void launchChooseCountryScreen(Activity context, int requestCode) {
        Intent intent = new Intent(context, ChooseCountryScreen.class);
        context.startActivityForResult(intent, requestCode);
    }

    public static void launchWelcomeScreen(Activity context) {
        Intent intent = new Intent(context, WelcomeScreen.class);
        context.startActivity(intent);
        context.finish();
    }


    public static void launchChatViewScreen(Activity context) {
        Intent intent = new Intent(context, ChatPageActivity.class);
        context.startActivity(intent);
        context.finish();
    }

    public static void launchNotification(Activity context) {
        Intent intent = new Intent(context, NotificationSettings.class);
        context.startActivity(intent);
//        context.finish();
    }

    public static void launchChangePin(Activity context) {
        Intent intent = new Intent(context, ChangePinActivity.class);
        context.startActivity(intent);
//        context.finish();
    }

    public static void launchSharePin(Activity context) {
        Intent intent = new Intent(context, SharePinActivity.class);
        context.startActivity(intent);
//        context.finish();
    }

    public static void launchContactSettings(Activity context) {
        Intent intent = new Intent(context, ContactSettings.class);
        context.startActivity(intent);
//        context.finish();
    }

    public static void launchChatBackup(Activity context) {
        Intent intent = new Intent(context, Chatbackup.class);
        context.startActivity(intent);
//        context.finish();
    }

    public static void launchUserProfile(Activity context) {
        Intent intent = new Intent(context, UserProfile.class);
        context.startActivity(intent);
        //  context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public static void launchChatWallpaper(Activity context) {
        Intent intent = new Intent(context, ChatWallpaperActivity.class);
        context.startActivity(intent);
    }

    public static void launchPinActivity(Activity context) {
        Intent intent = new Intent(context, CashplusPinActivity.class);
        context.startActivity(intent);
        //  context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public static void launchCashplusPasswordActivity(Activity context, String Password) {
        Intent intent = new Intent(context, CashpluPasswordActivity.class);
        if (Password != null)
            intent.putExtra("Password", Password);
        context.startActivity(intent);
        //  context.overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public static void launchCashplusUpdateprofile(Activity context) {
        Intent intent = new Intent(context, CashplusUpdateProfile.class);
        context.startActivity(intent);
    }

    public static void launchSendPayment(Activity context) {
        Intent intent = new Intent(context, SendPaymentActivity.class);
        context.startActivity(intent);
    }


    public static void launchSMSVerificationScreen(Activity context, String msisdn, String code, String phone, String otp, String Gcm_id, boolean fromEmail) {
        Intent intent = new Intent(context, ScimboSmsVScreen.class);
        if (msisdn != null)
            intent.putExtra("msisdn", msisdn);
        if (code != null)
            intent.putExtra("code", "" + code);
        if (phone != null)
            intent.putExtra("Phone", phone);

        if (otp != null)
            intent.putExtra("otp", otp);

        if (context instanceof VerifyPhoneScreen) {
            intent.putExtra("FromVerifyPage", true);
        }

        intent.putExtra("Fromemail", fromEmail);
        intent.putExtra("gcmid", Gcm_id);
        context.startActivity(intent);
        //    context.finish();

    }


    public static void launchAbout_contactus(AboutHelp context) {
        Intent intent = new Intent(context, About_contactus.class);
        context.startActivity(intent);
//        context.finish();
    }

    public static void launchLanguage(Activity context) {
        Intent intent = new Intent(context, LanguageActivity.class);
        context.startActivity(intent);
        context.finish();
    }

    public static void launchRenewSubscription(Activity context) {
        Intent intent = new Intent(context, RenewSubscriptionActivity.class);
        context.startActivity(intent);
        context.finish();
    }

    public static void launchBuySubscription(Activity context, boolean renewSub) {
        Intent intent = new Intent(context, BuySubscription.class);
        intent.putExtra("renewSub", renewSub);
        context.startActivity(intent);
        context.finish();
    }
}
