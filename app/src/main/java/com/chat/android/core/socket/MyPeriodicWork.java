package com.chat.android.core.socket;

import android.content.Context;
import android.util.Log;

import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class MyPeriodicWork extends Worker {
private Context mContext;
    private static final String TAG = "MyPeriodicWork";
    public MyPeriodicWork(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        mContext = context;
    }
    @Override
    public Result doWork() {

        if(!AppUtils.isMyServiceRunning(mContext, MessageService.class)){

            AppUtils.startService(mContext, MessageService.class);

            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, MessageService.class));
            } else {
                context.startService(new Intent(context, MessageService.class));
            }*/

        }else {
          /*  if(!SocketManager.isConnected){
                MessageService.manager.connect();
            }*/

        }
        return Result.success();
    }
}
