package com.chat.android.core.scimbohelperclass;

import java.util.regex.Pattern;

/**
 * Created by CAS60 on 6/15/2017.
 */
public class ScimboRegularExp {

    public static Boolean isEncodedBase64String(String data) {
        String base64Exp = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
        Pattern base64Pattern = Pattern.compile(base64Exp);
        return base64Pattern.matcher(data).matches();
    }

}
