package com.chat.android.core.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.chat.android.core.model.PinCodeModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class PinCodesDB_Sqlite extends SQLiteOpenHelper {
    private final static String DB_Name = "pinCodes";
    private final static int DB_Version = 1;

    private final String COLUMN_PIN_CODE = "COLUMN_PIN_CODES";
    private final String COLUMN_MSISDN = "COLUMN_MSISDN";
    private final String COLUMN_NAME = "COLUMN_NAME";

    private final Context context;

    private Gson gson;
    private GsonBuilder gsonBuilder;

    private SQLiteDatabase mDatabaseInstance;

    //tableName
    private final static String TABLE_PIN_CODES = "PIN_CODES";

    //Query for table creation
    String CREATE_TABLE_PIN_CODES = "CREATE TABLE IF NOT EXISTS " + TABLE_PIN_CODES
            + "(" + COLUMN_MSISDN + " TEXT PRIMARY KEY ,"
            + COLUMN_PIN_CODE + " TEXT," + COLUMN_NAME + " TEXT)";

    //Query for Select All
    private final String getPinCodesQuery = "SELECT * FROM "+TABLE_PIN_CODES;

    public PinCodesDB_Sqlite(Context context) {
        super(context, DB_Name, null, DB_Version);
        this.context = context;
        gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    private synchronized SQLiteDatabase getDatabaseInstance() {
        if (mDatabaseInstance == null) {
            mDatabaseInstance = getWritableDatabase();
        }

        if (!mDatabaseInstance.isOpen()) {
            mDatabaseInstance = getWritableDatabase();
        }

        return mDatabaseInstance;
    }

    public synchronized void close() {
        if (mDatabaseInstance != null && mDatabaseInstance.isOpen()) {
            mDatabaseInstance.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PIN_CODES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PIN_CODES);
    }

    public void deleteDatabase() {
        close();
        context.deleteDatabase(DB_Name);
    }

    public void clearDatabase() {
        getDatabaseInstance().delete(TABLE_PIN_CODES, null, null);
    }

    public ArrayList<PinCodeModel> getPinCodes() {
        ArrayList<PinCodeModel> array_list = new ArrayList<>();
        Cursor res = getDatabaseInstance().rawQuery( getPinCodesQuery, null );
        res.moveToFirst();
        while(!res.isAfterLast()) {
            PinCodeModel pinCodeModel = new PinCodeModel(res.getString(res.getColumnIndex(COLUMN_MSISDN)), res.getString(res.getColumnIndex(COLUMN_PIN_CODE)) );
            array_list.add(pinCodeModel);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public void savePinCodes(ArrayList<PinCodeModel> pinCodeModels) {
        if(pinCodeModels==null)
            return;
        for (PinCodeModel pinCodeModel : pinCodeModels) {
            String query = "INSERT INTO " + TABLE_PIN_CODES + "(COLUMN_PIN_CODES, COLUMN_MSISDN, COLUMN_NAME) " +
                    "VALUES( '"+pinCodeModel.getPinCode()+"',"+"'" + pinCodeModel.getPhoneNumber() +"',"+"'" + pinCodeModel.getName() +"') " +
                    "ON CONFLICT (COLUMN_MSISDN) DO UPDATE SET COLUMN_PIN_CODES=COLUMN_PIN_CODES.excluded";
            getDatabaseInstance().execSQL(query);
        }
    }

    public void updatePinCode(PinCodeModel pinCodeModel) {
        if(pinCodeModel==null)
            return;
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_PIN_CODE, pinCodeModel.getPinCode());
        cv.put(COLUMN_MSISDN, pinCodeModel.getPhoneNumber());
        cv.put(COLUMN_NAME, pinCodeModel.getName());
        getDatabaseInstance().update(TABLE_PIN_CODES, cv, COLUMN_MSISDN + "=" + pinCodeModel.getPhoneNumber(), null);
        getPinCodes();
    }


}
