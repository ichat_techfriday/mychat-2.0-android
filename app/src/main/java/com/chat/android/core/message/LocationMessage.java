package com.chat.android.core.message;

import android.content.Context;

import com.chat.android.app.utils.GroupInfoSession;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.model.GroupInfoPojo;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.socket.SocketManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 11/2/2016.
 */
public class LocationMessage extends BaseMessage implements Message {

    private static final String TAG = "LocationMessage";
    private Context context;

    public LocationMessage(Context context) {
        super(context);
        setType(MessageFactory.location);
        this.context = context;
    }


    @Override
    public Object getMessageObject(String to, String payload,Boolean isSecretChat) {
        this.to = to;
        setId(from + "-" + to);
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", type);
            object.put("payload", "");
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);

            if (isSecretChat) {
//                setId(getId() + "-secret");
                object.put("chat_type", MessageFactory.CHAT_TYPE_SECRET);
            }
        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }
        return object;
    }

    public Object getLocationObject(JSONObject msgObj, String addressName, String address,
                                   String locationUrl, String thumbUrl, String locationImgThumb) {

        JSONObject locationObj = new JSONObject();
        try {
            locationObj.put("title", addressName);
            locationObj.put("url", locationUrl);
            locationObj.put("description", address);
            locationObj.put("image", thumbUrl);
            locationObj.put("thumbnail_data", locationImgThumb);

            msgObj.put("metaDetails", locationObj);

        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }
        return msgObj;
    }

    @Override
    public Object getGroupMessageObject(String to, String payload, String groupName) {
        this.to = to;
        setId(from + "-" + to + "-g");
        JSONObject object = new JSONObject();
        try {
            object.put("from", from);
            object.put("to", to);
            object.put("type", MessageFactory.location);
            object.put("payload", payload);
            object.put("id", tsForServerEpoch);
            object.put("toDocId", getId() + "-" + tsForServerEpoch);
            object.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
            object.put("userName", groupName);
        } catch (JSONException e) {
            MyLog.e(TAG,"",e);
        }
        return object;
    }

    public MessageItemChat createMessageItem(boolean isSelf, String message, String status, String receiverUid,
                                             String senderName, String addressName, String address, String mapUrl,
                                             String thumbUrl, String thumbData) {
        item = new MessageItemChat();
        item.setMessageId(getId() + "-" + tsForServerEpoch);
        item.setIsSelf(isSelf);
        item.setTextMessage(message);
        item.setDeliveryStatus(status);
        item.setReceiverID(to);
        item.setReceiverUid(receiverUid);
        item.setMessageType("" + type);
        item.setSenderName(senderName);
        item.setTS(getShortTimeFormat());

        item.setWebLink(mapUrl);
        item.setWebLinkTitle(addressName);
        item.setWebLinkDesc(address);
        item.setWebLinkImgUrl(thumbUrl);
        item.setWebLinkImgThumb(thumbData);

        if (getId().contains("-g")) {
            GroupInfoSession groupInfoSession = new GroupInfoSession(context);
            GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(getId());

            if (infoPojo != null) {
                String[] groupMembers = infoPojo.getGroupMembers().split(",");
                try {
                    JSONArray arrMembers = new JSONArray();
                    for (String member : groupMembers) {
                        JSONObject userObj = new JSONObject();
                        userObj.put("UserId", member);
                        userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_SENT);
                        if (from.equals(member)) {
                            userObj.put("DeliverStatus", MessageFactory.DELIVERY_STATUS_READ);
                        }
                        userObj.put("DeliverTime", "");
                        userObj.put("ReadTime", "");
                        arrMembers.put(userObj);
                    }
                    JSONObject deliverObj = new JSONObject();
                    deliverObj.put("GroupMessageStatus", arrMembers);
                    item.setGroupMsgDeliverStatus(deliverObj.toString());
                } catch (JSONException e) {
                    MyLog.e(TAG,"",e);
                }
            }
        }

        return item;
    }

}
