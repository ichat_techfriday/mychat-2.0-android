package com.chat.android.core.model;
/**
 * Setter-Getter class for chat messages
 */

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.chat.android.app.utils.AppUtils;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.message.TextMessage;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


/**
 */
public class MessageItemChat extends FriendModel implements Serializable, Parcelable {
    private static final String TAG = "MessageItemChat";

    private String message, MessageDateOverlay, MessageType, id, fromName, videoPath, imagepath, audioPath, ts, date, DeliveryStatus;
    private boolean isSelf;
    private boolean isDate;
    private boolean isBlockedMsg;
    private boolean isBlocked;
    private String downloadingPath;
    private int downloadId;
    private String mMessageId;


    public String getMsgId() {
        return mMessageId;
    }

    public void setMsgId(String mMessageId) {
        this.mMessageId = mMessageId;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setisBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public ArrayList<String> getUserId_tag() {
        return userId_tag;
    }

    public void setUserId_tag(ArrayList<String> userId_tag) {
        this.userId_tag = userId_tag;
    }

    private ArrayList<String> userId_tag;

    public String getEditTextmsg() {
        return EditTextmsg;
    }

    public void setEditTextmsg(String editTextmsg) {
        EditTextmsg = editTextmsg;
    }

    private String EditTextmsg;

    public boolean isTagapplied() {
        return isTagapplied;
    }

    public void setTagapplied(boolean tagapplied) {
        isTagapplied = tagapplied;
    }

    public ArrayList<String> getSetArrayTagnames() {
        return setArrayTagnames;
    }

    public void setSetArrayTagnames(ArrayList<String> setArrayTagnames) {
        this.setArrayTagnames = setArrayTagnames;
    }

    private boolean isTagapplied;
    private ArrayList<String> setArrayTagnames;

    private long MessageDateGMT;
    private Uri imageUrl;
    private int downloadStatus, playerCurrentPosition = 0, playerMaxDuration = 0;
    private String thumbnailPath, webLink, webLinkTitle, webLinkDesc, webLinkImgUrl, webLinkImgThumb,groupId;

    private int uploadStatus = MessageFactory.UPLOAD_STATUS_UPLOADING;
    private Boolean iscontactthere = false;
    private String chatFileLocalPath;
    private String chatFileServerPath;
    private String contactName, contactNumber="", contactScimboId,DetailedContacts;

    private String fileSize, duration, chatFileWidth, chatFileHeight;
    private String DeliveryTime,ReadTime, groupMsgDeliverStatus, msgSentAt, secretMsgReadAt;
    private String receiverUid;
    private boolean isNewMessage, isMediaPlaying;
    private String newMessageCount;
    private int count;
    private int uploadDownloadProgress;
    private Object object;
    private String senderMsisdn, callType;
    boolean selected = false;
    private int fileBufferAt;
    private boolean isStatusReply;
    private String replyimagepath = "";
    //    private boolean isStarred;
    private String groupMsgFrom;
    private String groupName, prevGroupName, groupEventType;
    private String recordId, convId, thumbnailData;
    private String receiverName, secretTimerMode, secretTimer, secretTimeCreatedBy;
    private String receiverID, createdByUserId, createdToUserId;
    private String timerecevied;
    private String starredStatus = MessageFactory.MESSAGE_UN_STARRED;
    //    private boolean isLiveGroup = true; // User live in a group by default
    private boolean isInfoMsg = false; // Information message always false, while sent group event only it changed true
    private String replyMsisdn = "";
    private String replyFrom = "";
    private String replyType = "";
    private String replyMessage = "";
    private String replyId = "";
    private String replyServerLoad = "";
    private String replysender = "";
    private String imgreplyrecevie = "";
    private long toTypingAt = 0;
    private String typingPerson = "";
    private String caption;
    private int audiotype;
    private String statusDocId;
    private boolean isSecretChat;
    private String videobitmap;
    private MessageObjectChat messageObject;
    private boolean isclearchat;
    private TextMessage textMessage;

    private Bitmap thumb_bitmap_image;

    public Bitmap getThumb_bitmap_image() {
        return thumb_bitmap_image;
    }

    public void setThumb_bitmap_image(Bitmap thumb_bitmap_image) {
        this.thumb_bitmap_image = thumb_bitmap_image;
    }

    public boolean isclearchat() {
        return isclearchat;
    }


    public void setisclearchat(boolean isclearchat) {
        isclearchat = isclearchat;
    }


    public String getVideobitmap() {
        return videobitmap;
    }

    public void setVideobitmap(String videobitmap) {
        this.videobitmap = videobitmap;
    }

    public boolean isGroup() {
        return isthisGroup;
    }


    public void setGroup(boolean group) {
        isthisGroup = group;
    }

    private boolean isthisGroup ;
    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getMsgSentAt() {
        return msgSentAt;
    }

    public void setMsgSentAt(String msgSentAt) {
        this.msgSentAt = msgSentAt;
    }

    public String getSecretMsgReadAt() {
        return secretMsgReadAt;
    }

    public void setSecretMsgReadAt(String secretMsgReadAt) {
        this.secretMsgReadAt = secretMsgReadAt;
    }

    public String getSecretTimeCreatedBy() {
        return secretTimeCreatedBy;
    }

    public void setSecretTimeCreatedBy(String secretTimeCreatedBy) {
        this.secretTimeCreatedBy = secretTimeCreatedBy;
    }

    public String getSecretTimerMode() {
        return secretTimerMode;
    }

    public void setSecretTimerMode(String secretTimerMode) {
        this.secretTimerMode = secretTimerMode;
    }

    public String getSecretTimer() {
        return secretTimer;
    }

    public void setSecretTimer(String secretTimer) {
        this.secretTimer = secretTimer;
    }

    public int getPlayerCurrentPosition() {
        return playerCurrentPosition;
    }

    public void setPlayerCurrentPosition(int playerCurrentPosition) {
        this.playerCurrentPosition = playerCurrentPosition;
    }

    public int getPlayerMaxDuration() {
        return playerMaxDuration;
    }

    public void setPlayerMaxDuration(int playerMaxDuration) {
        this.playerMaxDuration = playerMaxDuration;
    }

    public boolean isMediaPlaying() {
        return isMediaPlaying;
    }

    public void setIsMediaPlaying(boolean mediaPlaying) {
        isMediaPlaying = mediaPlaying;
    }

    public String getGroupEventType() {
        return groupEventType;
    }

    public void setGroupEventType(String groupEventType) {
        this.groupEventType = groupEventType;
    }

    public String getPrevGroupName() {
        return prevGroupName;
    }

    public void setPrevGroupName(String prevGroupName) {
        this.prevGroupName = prevGroupName;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(String createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public String getCreatedToUserId() {
        return createdToUserId;
    }

    public void setCreatedToUserId(String createdToUserId) {
        this.createdToUserId = createdToUserId;
    }

    public String getGroupMsgDeliverStatus() {
        return groupMsgDeliverStatus;
    }

    public void setGroupMsgDeliverStatus(String groupMsgDeliverStatus) {
        this.groupMsgDeliverStatus = groupMsgDeliverStatus;
    }

    public int getaudiotype() {
        return audiotype;
    }

    public void setaudiotype(int audiotype) {
        this.audiotype = audiotype;
    }

    public void setTypePerson(String typingPerson) {
        this.typingPerson = typingPerson;
    }

    public String getTypePerson() {
        return typingPerson;
    }

    public long getTypingAt() {
        return toTypingAt;
    }

    public void setTypingAt(long toTypingAt) {
        this.toTypingAt = toTypingAt;
    }

    public String getChatFileWidth() {
        return chatFileWidth;
    }

    public void setChatFileWidth(String chatFileWidth) {
        this.chatFileWidth = chatFileWidth;
    }

    public String getChatFileHeight() {
        return chatFileHeight;
    }

    public void setChatFileHeight(String chatFileHeight) {
        this.chatFileHeight = chatFileHeight;
    }

    public int getUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactScimboId() {
        return contactScimboId;
    }

    public void setContactScimboId(String contactScimboId) {
        this.contactScimboId = contactScimboId;
    }

    public void setreplyimagebase64(String imgreplyrecevie) {
        this.imgreplyrecevie = imgreplyrecevie;
    }

    public String getreplyimagebase64() {
        return imgreplyrecevie;
    }

    public String getreplyimgpath() {
        return replyimagepath;
    }

    public void setreplyimagepath(String replyimagepath) {
        this.replyimagepath = replyimagepath;
    }

    public String getReplySenser() {
        return replysender;
    }

    public void setReplySender(String replysender) {

        this.replysender = replysender;
    }

    public String getReplyFrom() {
        return replyFrom;
    }

    public void setReplyFrom(String replyFrom) {
        this.replyFrom = replyFrom;
    }

    public String getReplyMsisdn() {
        return replyMsisdn;
    }

    public void setReplyMsisdn(String replyMsisdn) {
        this.replyMsisdn = replyMsisdn;
    }

    public String getReplyType() {
        return replyType;
    }

    public void setReplyType(String replyType) {
        this.replyType = replyType;
    }

    public String getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(String replyMessage) {
        this.replyMessage = replyMessage;
    }

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public String getReplyServerLoad() {
        return replyServerLoad;
    }

    public void setReplyServerLoad(String replyServerLoad) {
        this.replyServerLoad = replyServerLoad;
    }

    public int getUploadDownloadProgress() {
        return uploadDownloadProgress;
    }

    public void setUploadDownloadProgress(int uploadDownloadProgress) {
        this.uploadDownloadProgress = uploadDownloadProgress;
    }

    public String getChatFileLocalPath() {
        return chatFileLocalPath;
    }

    public void setChatFileLocalPath(String chatFileLocalPath) {
        this.chatFileLocalPath = chatFileLocalPath;
    }

    public String getChatFileServerPath() {
        return chatFileServerPath;
    }

    public void setChatFileServerPath(String chatFileServerPath) {
        this.chatFileServerPath = chatFileServerPath;
    }

    public String getSenderMsisdn() {
        return senderMsisdn;
    }

    public void setSenderMsisdn(String senderMsisdn) {
        this.senderMsisdn = senderMsisdn;
    }

    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    public String getWebLinkTitle() {
        return webLinkTitle;
    }

    public void setWebLinkTitle(String webLinkTitle) {
        this.webLinkTitle = webLinkTitle;
    }

    public String getWebLinkDesc() {
        return webLinkDesc;
    }

    public void setWebLinkDesc(String webLinkDesc) {
        this.webLinkDesc = webLinkDesc;
    }

    public String getWebLinkImgUrl() {
        return webLinkImgUrl;
    }

    public void setWebLinkImgUrl(String webLinkImgUrl) {
        this.webLinkImgUrl = webLinkImgUrl;
    }

    public String getWebLinkImgThumb() {
        return webLinkImgThumb;
    }

    public void setWebLinkImgThumb(String webLinkImgThumb) {
        this.webLinkImgThumb = webLinkImgThumb;
    }

    public String getDuration() {
        if (duration != null) {
            if (duration.contains(":"))
                return duration;
            else
                return AppUtils.getTimeString(AppUtils.parseLong(duration));
        }
        else
            return null;
    }



    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getFileBufferAt() {
        return fileBufferAt;
    }

    public void setFileBufferAt(int fileBufferAt) {
        this.fileBufferAt = fileBufferAt;
    }

    public String getThumbnailData() {
        return thumbnailData;
    }

    public void setThumbnailData(String thumbnailData) {
        this.thumbnailData = thumbnailData;
    }

    public String getStarredStatus() {
        return starredStatus;
    }

    public void setStarredStatus(String starredStatus) {
        this.starredStatus = starredStatus;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getConvId() {
        return convId;
    }

    public void setConvId(String convId) {
        this.convId = convId;
    }

    public boolean isInfoMsg() {
        return isInfoMsg;
    }

    public void setIsInfoMsg(boolean infoMsg) {
        isInfoMsg = infoMsg;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

/*    public boolean isLiveGroup() {
        return isLiveGroup;
    }

    public void setIsLiveGroup(boolean liveGroup) {
        isLiveGroup = liveGroup;
    }*/

    public String getGroupMsgFrom() {
        return groupMsgFrom;
    }

    public void setGroupMsgFrom(String groupMsgFrom) {
        this.groupMsgFrom = groupMsgFrom;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        Log.d(TAG, "setSelected: "+selected+" msg: "+message);
        this.selected = selected;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public boolean hasNewMessage() {
        return isNewMessage;
    }

    public void sethasNewMessage(boolean isNewMessage) {
        this.isNewMessage = isNewMessage;
    }


    public void setMessageId(String id) {
        this.id = id;
    }

    public String getMessageId() {
        return id;
    }


    public String getMessageDateOverlay() {
        return MessageDateOverlay;
    }

    public void setMessageDateOverlay(String MessageDateOverlay) {
        this.MessageDateOverlay = MessageDateOverlay;
    }


    public long getMessageDateGMTEpoch() {
        return MessageDateGMT;
    }

    public void setMessageDateGMTEpoch(long MessageDateGMT) {
        this.MessageDateGMT = MessageDateGMT;
    }

    public String getDeliveryTime() {
        return DeliveryTime;
    }

    public void setDeliveryTime(String DeliveryTime) {
        this.DeliveryTime = DeliveryTime;
    }

    public String getReadTime() {
        return ReadTime;
    }

    public void setReadTime(String ReadTime) {
        this.ReadTime = ReadTime;
    }

    public String getTS() {
        return ts;
    }

    public void setTS(String ts) {
        this.ts = ts;
    }

    /*public String getTime() {
        return timerecevied;
    }

    public void setTime(String timerecevied) {
        this.timerecevied = timerecevied;
    }*/


    public String getSenderName() {
        return fromName;
    }

    public void setSenderName(String fromName) {
        this.fromName = fromName;
    }


    public boolean isSelf() {
        return isSelf;
    }

    public void setIsSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }


    /**
     * 0-text,1-image,2-video,3-location,4-contact,5-audio
     */
    public void setMessageType(String MessageType) {
        this.MessageType = MessageType;
    }

    public String getMessageType() {
        return MessageType;
    }


    public String getTextMessage() {
        return message;
    }

    public void setTextMessage(String message) {
        this.message = message;
    }


    public void setImagePath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getImagePath() {
        return imagepath;
    }


    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public String getAudioPath() {
        return audioPath;
    }


    public boolean isDate() {
        return isDate;
    }

    public void setIsDate(boolean isDate) {
        this.isDate = isDate;
    }


    public void setDate(String date) {
        this.date = date;
    }


    public String getDate() {
        return date;
    }


    /**
     * status-0 not sent
     * status-1 sent
     * status-2 delivered
     * status-3 read
     */


    public String getDeliveryStatus() {
        return DeliveryStatus;
    }

    public void setDeliveryStatus(String DeliveryStatus) {
        Log.d("tickMiss", "setDeliveryStatus: "+DeliveryStatus);
        this.DeliveryStatus = DeliveryStatus;
    }


    public int getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(int downloadStatus) {
        this.downloadStatus = downloadStatus;
    }


    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }


    public String getReceiverUid() {
        return receiverUid;
    }

    public void setReceiverUid(String receiverUid) {
        this.receiverUid = receiverUid;
    }


    public Uri getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Uri imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getNewMessageCount() {
        return newMessageCount;
    }

    public void setNewMessageCount(String newMessageCount) {
        this.newMessageCount = newMessageCount;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }

    public Boolean getcontactsavethere() {
        return iscontactthere;
    }

    public void setcontactsavethere(Boolean iscontactthere) {
        this.iscontactthere = iscontactthere;
    }

    public String getDetailedContacts() {
        return DetailedContacts;
    }
    public void setDetailedContacts(String DetailedContacts)
    {
        this.DetailedContacts = DetailedContacts;
    }

    public boolean isStatusReply() {
        return isStatusReply;
    }

    public void setStatusReply(boolean statusReply) {
        isStatusReply = statusReply;
    }

    public String getStatusDocId() {
        return statusDocId;
    }

    public void setStatusDocId(String statusDocId) {
        this.statusDocId = statusDocId;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean isSecretChat() {
        return isSecretChat;
    }

    public MessageItemChat() {
    }

    public void setSecretChat(boolean secretChat) {
        isSecretChat = secretChat;
    }

    public boolean isBlockedMsg() {
        return isBlockedMsg;
    }

    public void setBlockedMsg(boolean blockedMsg) {
        isBlockedMsg = blockedMsg;
    }

    public String getDownloadingPath() {
        return downloadingPath;
    }

    public void setDownloadingPath(String downloadingPath) {
        this.downloadingPath = downloadingPath;
    }

    public int getDownloadId() {
        return downloadId;
    }

    public void setDownloadId(int downloadId) {
        this.downloadId = downloadId;
    }

    public MessageObjectChat getMessageObject() {
        return messageObject;
    }

    public void setMessageObject(MessageObjectChat messageObject) {
        this.messageObject = messageObject;
    }

    @NonNull
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MessageItemChat)) return false;
        if (!super.equals(o)) return false;
        MessageItemChat that = (MessageItemChat) o;
        return isSelf() == that.isSelf() && isDate() == that.isDate() &&
                isBlockedMsg() == that.isBlockedMsg() && isBlocked() == that.isBlocked() &&
                getDownloadId() == that.getDownloadId() && isTagapplied() == that.isTagapplied() &&
                MessageDateGMT == that.MessageDateGMT &&
                getDownloadStatus() == that.getDownloadStatus() &&
                getPlayerCurrentPosition() == that.getPlayerCurrentPosition() &&
                getPlayerMaxDuration() == that.getPlayerMaxDuration() &&
                getUploadStatus() == that.getUploadStatus() && isNewMessage == that.isNewMessage &&
                isMediaPlaying() == that.isMediaPlaying() && getCount() == that.getCount() &&
                getUploadDownloadProgress() == that.getUploadDownloadProgress() &&
                isSelected() == that.isSelected() && getFileBufferAt() == that.getFileBufferAt() &&
                isStatusReply() == that.isStatusReply() && isInfoMsg() == that.isInfoMsg() &&
                toTypingAt == that.toTypingAt && audiotype == that.audiotype &&
                isSecretChat() == that.isSecretChat() && isclearchat == that.isclearchat &&
                isthisGroup == that.isthisGroup && Objects.equals(message, that.message) &&
                Objects.equals(getMessageDateOverlay(), that.getMessageDateOverlay()) &&
                Objects.equals(getMessageType(), that.getMessageType()) &&
                Objects.equals(id, that.id) && Objects.equals(fromName, that.fromName) &&
                Objects.equals(getVideoPath(), that.getVideoPath()) &&
                Objects.equals(imagepath, that.imagepath) &&
                Objects.equals(getAudioPath(), that.getAudioPath()) && Objects.equals(ts, that.ts) &&
                Objects.equals(isDate(), that.isDate()) &&
                Objects.equals(getDeliveryStatus(), that.getDeliveryStatus()) &&
                Objects.equals(getDownloadingPath(), that.getDownloadingPath()) &&
                Objects.equals(mMessageId, that.mMessageId) &&
                Objects.equals(getUserId_tag(), that.getUserId_tag()) &&
                Objects.equals(getEditTextmsg(), that.getEditTextmsg()) &&
                Objects.equals(getSetArrayTagnames(), that.getSetArrayTagnames()) &&
                Objects.equals(getImageUrl(), that.getImageUrl()) &&
                Objects.equals(getThumbnailPath(), that.getThumbnailPath()) &&
                Objects.equals(getWebLink(), that.getWebLink()) &&
                Objects.equals(getWebLinkTitle(), that.getWebLinkTitle()) &&
                Objects.equals(getWebLinkDesc(), that.getWebLinkDesc()) &&
                Objects.equals(getWebLinkImgUrl(), that.getWebLinkImgUrl()) &&
                Objects.equals(getWebLinkImgThumb(), that.getWebLinkImgThumb()) &&
                Objects.equals(groupId, that.groupId) &&
                Objects.equals(iscontactthere, that.iscontactthere) &&
                Objects.equals(getChatFileLocalPath(), that.getChatFileLocalPath()) &&
                Objects.equals(getChatFileServerPath(), that.getChatFileServerPath()) &&
                Objects.equals(getContactName(), that.getContactName()) &&
                Objects.equals(getContactNumber(), that.getContactNumber()) &&
                Objects.equals(getContactScimboId(), that.getContactScimboId()) &&
                Objects.equals(getDetailedContacts(), that.getDetailedContacts()) &&
                Objects.equals(getFileSize(), that.getFileSize()) &&
                Objects.equals(getDuration(), that.getDuration()) &&
                Objects.equals(getChatFileWidth(), that.getChatFileWidth()) &&
                Objects.equals(getChatFileHeight(), that.getChatFileHeight()) &&
                Objects.equals(getDeliveryTime(), that.getDeliveryTime()) &&
                Objects.equals(getReadTime(), that.getReadTime()) &&
                Objects.equals(getGroupMsgDeliverStatus(), that.getGroupMsgDeliverStatus()) &&
                Objects.equals(getMsgSentAt(), that.getMsgSentAt()) &&
                Objects.equals(getSecretMsgReadAt(), that.getSecretMsgReadAt()) &&
                Objects.equals(getReceiverUid(), that.getReceiverUid()) &&
                Objects.equals(getNewMessageCount(), that.getNewMessageCount()) &&
                Objects.equals(getObject(), that.getObject()) &&
                Objects.equals(getSenderMsisdn(), that.getSenderMsisdn()) &&
                Objects.equals(getCallType(), that.getCallType()) &&
                Objects.equals(replyimagepath, that.replyimagepath) &&
                Objects.equals(getGroupMsgFrom(), that.getGroupMsgFrom()) &&
                Objects.equals(getGroupName(), that.getGroupName()) &&
                Objects.equals(getPrevGroupName(), that.getPrevGroupName()) &&
                Objects.equals(getGroupEventType(), that.getGroupEventType()) &&
                Objects.equals(getRecordId(), that.getRecordId()) &&
                Objects.equals(getConvId(), that.getConvId()) &&
                Objects.equals(getThumbnailData(), that.getThumbnailData()) &&
                Objects.equals(getReceiverName(), that.getReceiverName()) &&
                Objects.equals(getSecretTimerMode(), that.getSecretTimerMode()) &&
                Objects.equals(getSecretTimer(), that.getSecretTimer()) &&
                Objects.equals(getSecretTimeCreatedBy(), that.getSecretTimeCreatedBy()) &&
                Objects.equals(getReceiverID(), that.getReceiverID()) &&
                Objects.equals(getCreatedByUserId(), that.getCreatedByUserId()) &&
                Objects.equals(getCreatedToUserId(), that.getCreatedToUserId()) &&
                Objects.equals(timerecevied, that.timerecevied) &&
                Objects.equals(getStarredStatus(), that.getStarredStatus()) &&
                Objects.equals(getReplyMsisdn(), that.getReplyMsisdn()) &&
                Objects.equals(getReplyFrom(), that.getReplyFrom()) &&
                Objects.equals(getReplyType(), that.getReplyType()) &&
                Objects.equals(getReplyMessage(), that.getReplyMessage()) &&
                Objects.equals(getReplyId(), that.getReplyId()) &&
                Objects.equals(getReplyServerLoad(), that.getReplyServerLoad()) &&
                Objects.equals(replysender, that.replysender) &&
                Objects.equals(imgreplyrecevie, that.imgreplyrecevie) &&
                Objects.equals(typingPerson, that.typingPerson) &&
                Objects.equals(getCaption(), that.getCaption()) &&
                Objects.equals(getStatusDocId(), that.getStatusDocId()) &&
                Objects.equals(getVideobitmap(), that.getVideobitmap()) &&
                Objects.equals(getMessageObject(), that.getMessageObject()) &&
                Objects.equals(getTextMessage(), that.getTextMessage()) &&
                Objects.equals(getThumb_bitmap_image(), that.getThumb_bitmap_image());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), message, getMessageDateOverlay(), getMessageType(), id, fromName, getVideoPath(), imagepath, getAudioPath(), ts, isDate(), getDeliveryStatus(), isSelf(), isDate(), isBlockedMsg(), isBlocked(), getDownloadingPath(), getDownloadId(), mMessageId, getUserId_tag(), getEditTextmsg(), isTagapplied(), getSetArrayTagnames(), MessageDateGMT, getImageUrl(), getDownloadStatus(), getPlayerCurrentPosition(), getPlayerMaxDuration(), getThumbnailPath(), getWebLink(), getWebLinkTitle(), getWebLinkDesc(), getWebLinkImgUrl(), getWebLinkImgThumb(), groupId, getUploadStatus(), iscontactthere, getChatFileLocalPath(), getChatFileServerPath(), getContactName(), getContactNumber(), getContactScimboId(), getDetailedContacts(), getFileSize(), getDuration(), getChatFileWidth(), getChatFileHeight(), getDeliveryTime(), getReadTime(), getGroupMsgDeliverStatus(), getMsgSentAt(), getSecretMsgReadAt(), getReceiverUid(), isNewMessage, isMediaPlaying(), getNewMessageCount(), getCount(), getUploadDownloadProgress(), getObject(), getSenderMsisdn(), getCallType(), isSelected(), getFileBufferAt(), isStatusReply(), replyimagepath, getGroupMsgFrom(), getGroupName(), getPrevGroupName(), getGroupEventType(), getRecordId(), getConvId(), getThumbnailData(), getReceiverName(), getSecretTimerMode(), getSecretTimer(), getSecretTimeCreatedBy(), getReceiverID(), getCreatedByUserId(), getCreatedToUserId(), timerecevied, getStarredStatus(), isInfoMsg(), getReplyMsisdn(), getReplyFrom(), getReplyType(), getReplyMessage(), getReplyId(), getReplyServerLoad(), replysender, imgreplyrecevie, toTypingAt, typingPerson, getCaption(), audiotype, getStatusDocId(), isSecretChat(), getVideobitmap(), getMessageObject(), isclearchat, getTextMessage(), getThumb_bitmap_image(), isthisGroup);
    }

    public void setTextMessageObj(TextMessage message) {
        this.textMessage = message;
    }

    public TextMessage getTextMessageObj() {
        return this.textMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      dest.writeString(message);
      dest.writeString(MessageDateOverlay);
      dest.writeString(MessageType);
      dest.writeString(id);
      dest.writeString(fromName);
      dest.writeString(videoPath);
      dest.writeString(imagepath);
      dest.writeString(audioPath);
      dest.writeString(ts);
      dest.writeString(date);
      dest.writeString(DeliveryStatus);
      dest.writeString(downloadingPath);
      dest.writeString(mMessageId);
      dest.writeString(thumbnailPath);
      dest.writeString(videobitmap);
      dest.writeString(statusDocId);
      dest.writeString(caption);
      dest.writeString(typingPerson);
      dest.writeString(imgreplyrecevie);
      dest.writeString(replysender);
      dest.writeString(replyServerLoad);
      dest.writeString(replyId);
      dest.writeString(replyMessage);
      dest.writeString(replyType);
      dest.writeString(replyFrom);
      dest.writeString(replyMsisdn);
      dest.writeString(starredStatus);
      dest.writeString(timerecevied);
      dest.writeString(receiverID);
      dest.writeString(createdByUserId);
      dest.writeString(createdToUserId);
      dest.writeString(receiverName);
      dest.writeString(secretTimerMode);
      dest.writeString(secretTimer);
      dest.writeString(secretTimeCreatedBy);
      dest.writeString(recordId);
      dest.writeString(convId);
      dest.writeString(thumbnailData);
      dest.writeString(groupName);
      dest.writeString(prevGroupName);
      dest.writeString(groupEventType);
      dest.writeString(groupMsgFrom);
      dest.writeString(replyimagepath);
      dest.writeString(senderMsisdn);
      dest.writeString(callType);
      dest.writeString(newMessageCount);
      dest.writeString(receiverUid);
      dest.writeString(DeliveryTime);
      dest.writeString(ReadTime);
      dest.writeString(groupMsgDeliverStatus);
      dest.writeString(msgSentAt);
      dest.writeString(secretMsgReadAt);
      dest.writeString(fileSize);
      dest.writeString(duration);
      dest.writeString(chatFileWidth);
      dest.writeString(chatFileHeight);
      dest.writeString(contactName);
      dest.writeString(contactNumber);
      dest.writeString(contactScimboId);
      dest.writeString(DetailedContacts);
      dest.writeString(chatFileServerPath);
      dest.writeString(chatFileLocalPath);
      dest.writeString(webLink);
      dest.writeString(webLinkTitle);
      dest.writeString(webLinkDesc);
      dest.writeString(webLinkImgUrl);
      dest.writeString(webLinkImgThumb);
      dest.writeString(groupId);
      dest.writeInt(downloadId);
      dest.writeInt(downloadStatus);
      dest.writeInt(playerCurrentPosition);
      dest.writeInt(playerMaxDuration);
      dest.writeInt(uploadStatus);
      dest.writeInt(count);
      dest.writeInt(uploadDownloadProgress);
      dest.writeInt(fileBufferAt);
      dest.writeInt(audiotype);
      dest.writeLong(MessageDateGMT);
      dest.writeLong(toTypingAt);
      dest.writeByte((byte) (isSelf ? 0 : 1));
      dest.writeByte((byte) (isDate ? 0 : 1));
      dest.writeByte((byte) (isBlockedMsg ? 0 : 1));
      dest.writeByte((byte) (isBlocked ? 0 : 1));
      dest.writeByte((byte) (isTagapplied ? 0 : 1));
      dest.writeByte((byte) (iscontactthere ? 0 : 1));
      dest.writeByte((byte) (isNewMessage ? 0 : 1));
      dest.writeByte((byte) (isMediaPlaying ? 0 : 1));
      dest.writeByte((byte) (selected ? 0 : 1));
      dest.writeByte((byte) (isStatusReply ? 0 : 1));
      dest.writeByte((byte) (isInfoMsg ? 0 : 1));
      dest.writeByte((byte) (isSecretChat ? 0 : 1));
      dest.writeByte((byte) (isclearchat ? 0 : 1));
      dest.writeList(setArrayTagnames);
      dest.writeValue(textMessage);
      dest.writeValue(object);
      dest.writeValue(imageUrl);
      dest.writeValue(messageObject);
    }

    public MessageItemChat(Parcel in) {
        message = in.readString();
        MessageDateOverlay = in.readString();
        MessageType = in.readString();
        id = in.readString();
        fromName = in.readString();
        videoPath = in.readString();
        imagepath = in.readString();
        audioPath = in.readString();
        ts = in.readString();
        date = in.readString();
        DeliveryStatus = in.readString();
        downloadingPath = in.readString();
        mMessageId = in.readString();
        thumbnailPath = in.readString();
        videobitmap = in.readString();
        statusDocId = in.readString();
        caption = in.readString();
        typingPerson = in.readString();
        imgreplyrecevie = in.readString();
        replysender = in.readString();
        replyServerLoad = in.readString();
        replyId = in.readString();
        replyMessage = in.readString();
        replyType = in.readString();
        replyFrom = in.readString();
        replyMsisdn = in.readString();
        starredStatus = in.readString();
        timerecevied = in.readString();
        receiverID = in.readString();
        createdByUserId = in.readString();
        createdToUserId = in.readString();
        receiverName = in.readString();
        secretTimerMode = in.readString();
        secretTimer = in.readString();
        secretTimeCreatedBy = in.readString();
        recordId = in.readString();
        convId = in.readString();
        thumbnailData = in.readString();
        groupName = in.readString();
        prevGroupName = in.readString();
        groupEventType = in.readString();
        groupMsgFrom = in.readString();
        replyimagepath = in.readString();
        senderMsisdn = in.readString();
        callType = in.readString();
        newMessageCount = in.readString();
        receiverUid = in.readString();
        DeliveryTime = in.readString();
        ReadTime = in.readString();
        groupMsgDeliverStatus = in.readString();
        msgSentAt = in.readString();
        secretMsgReadAt = in.readString();
        fileSize = in.readString();
        duration = in.readString();
        chatFileWidth = in.readString();
        chatFileHeight = in.readString();
        contactName = in.readString();
        contactNumber = in.readString();
        contactScimboId = in.readString();
        DetailedContacts = in.readString();
        chatFileServerPath = in.readString();
        chatFileLocalPath = in.readString();
        webLink = in.readString();
        webLinkTitle = in.readString();
        webLinkDesc = in.readString();
        webLinkImgUrl = in.readString();
        webLinkImgThumb = in.readString();
        groupId = in.readString();
        downloadId = in.readInt();
        downloadStatus = in.readInt();
        playerCurrentPosition = in.readInt();
        playerMaxDuration = in.readInt();
        uploadStatus = in.readInt();
        count = in.readInt();
        uploadDownloadProgress = in.readInt();
        fileBufferAt = in.readInt();
        audiotype = in.readInt();
        MessageDateGMT = in.readLong();
        toTypingAt = in.readLong();
        isSelf = in.readByte() !=0;
        isDate = in.readByte() !=0;
        isBlockedMsg = in.readByte() !=0;
        isBlocked = in.readByte() !=0;
        isTagapplied = in.readByte() !=0;
        iscontactthere = in.readByte() !=0;
        isNewMessage = in.readByte() !=0;
        isMediaPlaying = in.readByte() !=0;
        selected = in.readByte() !=0;
        isStatusReply = in.readByte() !=0;
        isInfoMsg = in.readByte() !=0;
        isSecretChat = in.readByte() !=0;
        isclearchat = in.readByte() !=0;
        setArrayTagnames = in.readArrayList(getClass().getClassLoader());
        textMessage = (TextMessage) in.readValue(getClass().getClassLoader());
        object = in.readValue(getClass().getClassLoader());
        imageUrl = (Uri) in.readValue(getClass().getClassLoader());
        messageObject = (MessageObjectChat) in.readValue(getClass().getClassLoader());
       }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public MessageItemChat createFromParcel(Parcel in) {
            return new MessageItemChat(in);
        }

        public MessageItemChat[] newArray(int size) {
            return new MessageItemChat[size];
        }
    };
}
