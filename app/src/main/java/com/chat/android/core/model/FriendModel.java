package com.chat.android.core.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 */
public class FriendModel implements Serializable {
    private String requestId;
    @SerializedName("_id")
    private String _id;
    @SerializedName("RequestStatus")
    private String requestStatus;
    @SerializedName("PhNumber")
    private String Msisdn;
    private String onlineStatus;
    @SerializedName("Name")
    private String FirstName;
    @SerializedName("ProfilePic")
    private String AvatarImageUrl;
    @SerializedName("CountryCode")
    private String countryCode;
    @SerializedName("Status")
    private String Status;
    private String pinCode;

    private String noInDevice = "";
    private String type;
    private boolean isSelected = false;

    public String getGroupDocID() {
        return GroupDocID;
    }

    public void setGroupDocID(String groupDocID) {
        GroupDocID = groupDocID;
    }

    private String GroupDocID;

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    private boolean isGroup = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getNumberInDevice() {
        return noInDevice;
    }

    public void setNumberInDevice(String noInDevice) {
        this.noInDevice = noInDevice;
    }

    public String getAvatarImageUrl() {
        return AvatarImageUrl;
    }

    public void setAvatarImageUrl(String AvatarImageUrl) {
        this.AvatarImageUrl = AvatarImageUrl;
    }

    public String getMsisdn() {
        return Msisdn;
    }

    public void setMsisdn(String Msisdn) {
        this.Msisdn = Msisdn;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String country_code) {
        this.countryCode = country_code;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(String onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getNoInDevice() {
        return noInDevice;
    }

    public void setNoInDevice(String noInDevice) {
        this.noInDevice = noInDevice;
    }

    @Override
    public int hashCode() {
        return Objects.hash(_id, FirstName, AvatarImageUrl);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final FriendModel other = (FriendModel) obj;
        if (!Objects.equals(this._id, other._id)) return false;
        if (!Objects.equals(this.FirstName, other.FirstName)) return false;
        if (!Objects.equals(this.AvatarImageUrl, other.AvatarImageUrl)) return false;
        return true;
    }

    public static List<FriendModel> parseJSON(JSONArray friendsArray) {
        List<FriendModel> friendModels = new ArrayList<>();
        try {
            for (int i = 0; i < friendsArray.length(); i++) {
                JSONObject jsonObject = friendsArray.getJSONObject(i);
                FriendModel friendModel = new FriendModel();
                if (jsonObject.has("requestId"))
                    friendModel.requestId = jsonObject.getString("requestId");
                if (jsonObject.has("_id"))
                    friendModel._id = jsonObject.getString("_id");
                if (jsonObject.has("RequestStatus"))
                    friendModel.requestStatus = jsonObject.getString("RequestStatus");
                if (jsonObject.has("PhNumber"))
                    friendModel.Msisdn = jsonObject.getString("PhNumber");
                if (jsonObject.has("onlineStatus"))
                    friendModel.onlineStatus = jsonObject.getString("onlineStatus");
                if (jsonObject.has("Name"))
                    friendModel.FirstName = jsonObject.getString("Name");
                if (jsonObject.has("ProfilePic"))
                    friendModel.AvatarImageUrl = jsonObject.getString("ProfilePic");
                if (jsonObject.has("CountryCode"))
                    friendModel.countryCode = jsonObject.getString("CountryCode");
                if (jsonObject.has("Status"))
                    friendModel.Status = jsonObject.getString("Status");
                if (jsonObject.has("pinCode"))
                    friendModel.pinCode = jsonObject.getString("pinCode");
                friendModels.add(friendModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return friendModels;
    }

    public static FriendModel parseJSON(JSONObject jsonObject) {
        FriendModel friendModel = new FriendModel();
        try {
            if (jsonObject.has("requestId"))
                friendModel.requestId = jsonObject.getString("requestId");
            if (jsonObject.has("_id"))
                friendModel._id = jsonObject.getString("_id");
            if (jsonObject.has("RequestStatus"))
                friendModel.requestStatus = jsonObject.getString("RequestStatus");
            if (jsonObject.has("PhNumber"))
                friendModel.Msisdn = jsonObject.getString("PhNumber");
            if (jsonObject.has("onlineStatus"))
                friendModel.onlineStatus = jsonObject.getString("onlineStatus");
            if (jsonObject.has("Name"))
                friendModel.FirstName = jsonObject.getString("Name");
            if (jsonObject.has("ProfilePic"))
                friendModel.AvatarImageUrl = jsonObject.getString("ProfilePic");
            if (jsonObject.has("CountryCode"))
                friendModel.countryCode = jsonObject.getString("CountryCode");
            if (jsonObject.has("Status"))
                friendModel.Status = jsonObject.getString("Status");
            if (jsonObject.has("pinCode"))
                friendModel.pinCode = jsonObject.getString("pinCode");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return friendModel;
    }
}