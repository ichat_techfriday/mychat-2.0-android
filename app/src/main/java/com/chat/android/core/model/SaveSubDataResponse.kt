package com.chat.android.core.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Taha Adnan on 6/13/2022.
 */
data class SaveSubDataResponse(
    @SerializedName("status")
    var status : Boolean,
    @SerializedName("data")
    var data : UserSubscription
) :Serializable
