package com.chat.android.core.service;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.chat.android.app.calls.CallMessage;
import com.chat.android.app.calls.CallsActivity;
import com.chat.android.app.calls.IncomingCallActivity;
import com.chat.android.app.utils.ConnectivityInfo;
import com.chat.android.app.utils.MyLog;
import com.chat.android.backend.ApiCalls;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.SocketManager;
import com.chat.android.fcm.NotificationUtils;
import com.chat.android.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;


public class CallNotificationActionReceiver extends BroadcastReceiver {


    private static final String TAG = "CallNotificationActionReceiver";
    Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.mContext=context;
        String name = "", callType = "", docId = "", fromUserId = "", toUserId = "", fromUserMSISDN = "", callRoomId = "", recordId = "", callTimestamp = "";
        boolean isVideoCall = false;

        if (intent != null && intent.getExtras() != null) {
            Bundle data = intent.getExtras();
            String action ="";
            action=intent.getStringExtra(IncomingCallActivity.EXTRA_ACTION_TYPE);
            data = intent.getExtras();
            name = data.getString(AppConstants.INITIATOR);
            callType = data.getString(AppConstants.CALL_TYPE);
            docId = data.getString(IncomingCallActivity.EXTRA_DOC_ID);
            fromUserId = data.getString(IncomingCallActivity.EXTRA_FROM_USER_ID);
            toUserId = data.getString(IncomingCallActivity.EXTRA_TO_USER_ID);
            fromUserMSISDN = data.getString(IncomingCallActivity.EXTRA_FROM_USER_MSISDN);
            callRoomId = data.getString(IncomingCallActivity.EXTRA_CALL_ROOM_ID);
            recordId = data.getString(IncomingCallActivity.EXTRA_CALL_RECORD_ID);
            isVideoCall = data.getBoolean(IncomingCallActivity.EXTRA_CALL_TYPE);
            callTimestamp = data.getString(IncomingCallActivity.EXTRA_CALL_TIME_STAMP);

            if (action != null && !action.equalsIgnoreCase("")) {
                performClickAction(context, action, docId, fromUserId, toUserId, fromUserMSISDN, callRoomId, recordId, isVideoCall, callTimestamp);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                int notificationId = NotificationUtils.getNotificationID(recordId);
                NotificationManagerCompat.from(mContext).cancel(notificationId);
            } else {
                // Close the notification after the click action is performed.
                Intent iclose = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                context.sendBroadcast(iclose);
            }
            context.stopService(new Intent(context, CallNotificationService.class));

        }


    }
    private void performClickAction(Context context, String action, String docId, String fromUserId, String toUserId, String fromUserMSISDN, String callRoomId, String recordId, boolean isVideoCall, String callTimestamp) {
        if(action.equalsIgnoreCase(IncomingCallActivity.RECEIVE_CALL) || action.equalsIgnoreCase(IncomingCallActivity.DIALOG_CALL)) {
            Intent intent = new Intent();
            intent.setClass(CoreController.mcontext, IncomingCallActivity.class);

            intent.putExtra(IncomingCallActivity.EXTRA_DOC_ID, docId);
            intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_ID, fromUserId);
            intent.putExtra(IncomingCallActivity.EXTRA_TO_USER_ID, toUserId);
            intent.putExtra(IncomingCallActivity.EXTRA_FROM_USER_MSISDN, fromUserMSISDN);
            intent.putExtra(IncomingCallActivity.EXTRA_CALL_ROOM_ID, callRoomId);
            intent.putExtra(IncomingCallActivity.EXTRA_CALL_RECORD_ID, recordId);
            intent.putExtra(IncomingCallActivity.EXTRA_CALL_TYPE, isVideoCall);
            intent.putExtra(IncomingCallActivity.EXTRA_CALL_TIME_STAMP, callTimestamp);
            intent.setAction(action);
            intent.putExtra(IncomingCallActivity.EXTRA_ACTION_TYPE, action);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);

        } else if(action.equalsIgnoreCase(IncomingCallActivity.CANCEL_CALL)){
            rejectCall(docId,toUserId,fromUserId,recordId, callRoomId, isVideoCall);
        } else {
            context.stopService(new Intent(context, CallNotificationService.class));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                int notificationId = NotificationUtils.getNotificationID(recordId);
                NotificationManagerCompat.from(mContext).cancel(notificationId);
            } else {
                Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                context.sendBroadcast(it);
            }
        }
    }
    private void rejectCall(String mCallId,String toUserId,String fromUserId,String mRecordId, String callRoomId, boolean isVideoCall) {
        String mCurrentUserId = SessionManager.getInstance(CoreController.mcontext).getCurrentUserID();

        String[] splitIds = mCallId.split("-");
        String id = splitIds[2];
        String callDocId = toUserId + "-" + fromUserId + "-" + id;
        int callStatus = MessageFactory.CALL_STATUS_REJECTED;
        if (!ConnectivityInfo.isInternetConnected(CoreController.mcontext)) {
            callStatus = MessageFactory.CALL_STATUS_MISSED;
        }
        String type = "" + MessageFactory.audio_call;
        if (isVideoCall) {
            type = "" + MessageFactory.video_call;
        }
        JSONObject object = CallMessage.getCallStatusObject(mCurrentUserId, fromUserId, id, callDocId, mRecordId, callStatus, "" + type);
        MessageDbController db = CoreController.getDBInstance(CoreController.mcontext);
        db.updateCallStatus(mCallId, callStatus, "00:00");

        ApiCalls.getInstance(mContext).cancelCallFromNotification(mContext, callRoomId, fromUserId);

        SendMessageEvent event = new SendMessageEvent();
        event.setEventName(SocketManager.EVENT_CALL_STATUS);
        event.setMessageObject(object);
        EventBus.getDefault().post(event);
    }

    private Boolean checkAppPermissions() {
        return hasReadPermissions() && hasWritePermissions() && hasCameraPermissions() && hasAudioPermissions();
    }

    private boolean hasAudioPermissions() {
        return (ContextCompat.checkSelfPermission(CoreController.mcontext, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(CoreController.mcontext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(CoreController.mcontext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }
    private boolean hasCameraPermissions() {
        return (ContextCompat.checkSelfPermission(CoreController.mcontext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }
}