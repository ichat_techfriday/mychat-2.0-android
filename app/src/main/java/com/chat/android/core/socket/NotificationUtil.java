package com.chat.android.core.socket;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;

import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.chat.android.NotificationReceiver;
import com.chat.android.R;
import com.chat.android.app.activity.ChatPageActivity;
import com.chat.android.app.activity.NewHomeScreenActivty;
import com.chat.android.app.activity.NewgroupListActivity;
import com.chat.android.app.activity.SecretChatViewActivity;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.SharedPreference;
import com.chat.android.app.utils.UserInfoSession;
import com.chat.android.core.CoreController;
import com.chat.android.core.Session;
import com.chat.android.core.SessionManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.MuteStatusPojo;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.AudioPlayerUtils;
import com.chat.android.utils.StorageUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationUtil {
    private static final NotificationUtil ourInstance = new NotificationUtil();
    private static final String TAG = "NotificationUtil";
    public static ArrayList<String> messagepayload = new ArrayList<>();
    public static ArrayList<HashMap<String, Boolean>> chat = new ArrayList<>();
    private static int missedCallCount = 0;
    private static String lastMissedCallId = "";
    public static NotificationCompat.Builder builder;
    private Context mContext;
    private static Getcontactname getcontactname;
    private static Session session;
    private static UserInfoSession userInfoSession;
    private static List<String> msgIds = new ArrayList<>();
    //Notificaton with reply
    public static final String KEY_INTENT_MORE = "keyintentmore";
    public static final int REQUEST_CODE_MORE = 100;

    public static HashMap<String, String> previousRecordId = new HashMap();

    public static NotificationUtil getInstance() {
        return ourInstance;
    }

    private NotificationUtil() {
    }

    public static void clearNotificationData() {
        lastMissedCallId = "";
        chat.clear();
        messagepayload.clear();
        //MessageService.messageIds.clear();
        missedCallCount = 0;
    }

    public void init(Context context) {
        if (context == null) {
            mContext = CoreController.mcontext;
        }
        mContext = context;
        getcontactname = new Getcontactname(mContext);
        session = new Session(mContext);
        userInfoSession = new UserInfoSession(mContext);

    }

    public static void CustomshowNotificationWithReply(Context mContext, JSONObject data, String mFrom, String mType, boolean isSingleChat, boolean isSecretMsg, boolean isLockChat) {
        try {
            Log.d("Taha", "CustomshowNotificationWithReply: " + data);
            Log.e(TAG, "CustomshowNotificationWithReply: " + data);
            String uniqueCurrentID = SessionManager.getInstance(mContext).getCurrentUserID();
            //   String from = null;
            if (mFrom != null) {
                //   from = data.getString("from");.
                if (!mFrom.equalsIgnoreCase(uniqueCurrentID)) {
                    NotificationCompat.Action action = null;
                    String username = "";
                    String varchatmess = "";
                    String varmsg = "";
                    String thumbnail = "";
                    String groupName = "";
                    String doc_id = "";
                    String payLoad = "";
                    String resendTo = "";
                    String msgid = "";
                    String mContactNo = "";
                    String contactno = "";
                    PendingIntent pIntent = null;
                    String receiverDocumentID = "";
                    long aMilliesecond = 0;//AppUtils.parseLong(response.getString("timestamp"));

                    //   String typeStr = data.getString("type");
                    session = new Session(mContext);
                    userInfoSession = new UserInfoSession(mContext);

                    int type = Integer.parseInt(mType);
                    if (data.has("ContactName")) {
                        mContactNo = data.getString("ContactName");
                    }
                    if (data.has("timestamp")) {
                        aMilliesecond = AppUtils.parseLong(data.getString("timestamp"));
                    }
                    MyLog.d("data+data" + data);
                    String recordId = "";
                    if (data.has("recordId")) {
                        recordId = data.getString("recordId");
                    }

                    if (previousRecordId.get(recordId) == null) //already parsed
                        return;
                    previousRecordId.put(recordId, recordId);

                    RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(),
                            R.layout.customnotification);
                    Date cDate = new Date();
                    if (isSingleChat) {
                        //   if (!isSecretMsg) {
                        resendTo = data.getString("from");
                      /*  String typeStr = data.getString("type");
                        int type = Integer.parseInt(typeStr);
                    */
                        if (data.has("ContactMsisdn")) {
                            contactno = data.getString("ContactMsisdn");

                        } else if (data.has("msisdn")) {
                            contactno = data.getString("msisdn");

                        }

                        Log.e("mFrom", "mFrom" + mFrom);

                        Log.e("contactno", "contactno" + contactno);
                        getcontactname = new Getcontactname(mContext);

                        username = getcontactname.getSendername(mFrom, contactno, mContext);

                        //doc_id = (String) data.get("docId");
                        if (!data.has("doc_id") && !data.has("docId")) {
                            return;
                        }
                        if (data.has("doc_id"))
                            doc_id = data.optString("doc_id");

                        else if (data.has("docId"))
                            doc_id = data.optString("docId");


                        String[] array = doc_id.split("-");

                        HashMap<String, Boolean> chatAndType = new HashMap<>();
                        chatAndType.put(resendTo, isSecretMsg);
                        if (!chat.contains(chatAndType)) {
                            chat.add(chatAndType);
                        }

                        receiverDocumentID = "";
                        if (array[0].equalsIgnoreCase(uniqueCurrentID)) {
                            receiverDocumentID = array[1];
                        } else if (array[1].equalsIgnoreCase(uniqueCurrentID)) {
                            receiverDocumentID = array[0];
                        }
                        msgid = data.getString("msgId");
                        if (msgid != null && msgIds.contains(msgid)) {
                            return;
                        }
                        msgIds.add(msgid);
                        if (data.has("payload"))
                            payLoad = data.getString("payload");


                        //************ for avoid duplicate notifications *************//

                        if (isSecretMsg) {
                            if (MessageFactory.text == type || MessageFactory.nudge == type) {

                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                //   MyLog.d("line message========================================>" + linemessage);
                            } else if (MessageFactory.video == type) {
                                payLoad = mContext.getString(R.string.video);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                //      messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.picture == type) {
                                payLoad = mContext.getString(R.string.photo);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+";";
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.audio == type) {
                                payLoad = mContext.getString(R.string.Audio);
                                //linemessage = linemessage + username + ":" + payLoad + "\n"+",";
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.contact == type) {
                                payLoad = mContext.getString(R.string.contact);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.web_link == type) {
                                payLoad = mContext.getString(R.string.weblink);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.location == type) {
                                payLoad = mContext.getString(R.string.location);
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.document == type) {
                                payLoad = mContext.getString(R.string.document);
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.missed_call == type) {
                                payLoad = mContext.getString(R.string.missed_call);;
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.CALL_STATUS_RECEIVED == type) {
                                payLoad = mContext.getString(R.string.call_rejected);;
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            }
                        } else {
                            if (MessageFactory.text == type || MessageFactory.nudge == type) {

                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                //   MyLog.d("line message========================================>" + linemessage);
                            } else if (MessageFactory.video == type) {
                                payLoad = mContext.getString(R.string.video);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.picture == type) {
                                payLoad = mContext.getString(R.string.photo);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+";";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.audio == type) {
                                payLoad = mContext.getString(R.string.Audio);
                                //linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));

                            } else if (MessageFactory.contact == type) {
                                payLoad = mContext.getString(R.string.contact);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.web_link == type) {
                                payLoad = mContext.getString(R.string.weblink);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.location == type) {
                                payLoad = mContext.getString(R.string.location);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.document == type) {
                                payLoad = mContext.getString(R.string.document);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.missed_call == type) {
                                payLoad = mContext.getString(R.string.missed_call);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.CALL_STATUS_RECEIVED == type) {
                                payLoad = mContext.getString(R.string.call_rejected);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            }
                        }

                        // session.putmessagePrefs(messagepayload.toString());
                        String countDocId = uniqueCurrentID.concat("-").concat(resendTo);
                        //changeBadgeCount(countDocId,mContext);
                        //     }
                    } else {

                        try {
                            String phoneno;
                            String userid = data.getString("from");
                      /*  String typeStr = data.getString("type");
                        int type = Integer.parseInt(typeStr);*/
                            payLoad = data.getString("payload");
                            if (data.has("is_tag_applied")) {
                                String istagapply = data.getString("is_tag_applied");
                                if (istagapply.equalsIgnoreCase("1")) {
                                    payLoad = AppUtils.getTaggedMsgs(uniqueCurrentID, payLoad, mContext);
                                }
                            }

                            int groupType = data.optInt("groupType");

                            switch (groupType) {
                                case 4:
                                    Log.d(TAG, "CustomshowNotification: no need notify for remove user");
                                    return;

                            }

                            resendTo = (String) data.get("groupId");
                        /*try {
                            startDate = dff.parse(session.gettime(resendTo + "tsNextLine"));
                        } catch (ParseException e) {
                            MyLog.e(TAG,"",e);
                        }*/
                            String contactno2 = data.getString("msisdn");
                            phoneno = getcontactname.getSendername(userid, contactno2);
                            groupName = data.getString("groupName");
                            if (data.has("From_avatar")) {
                                thumbnail = data.getString("From_avatar");
                            }
                      /*  try {

                            startDate = dff.parse(session.gettime(resendTo + "tsNextLine"));
                        } catch (ParseException e) {
                            MyLog.e(TAG,"",e);
                        }*/
                            username = groupName;
                            if (MessageFactory.text == type || MessageFactory.nudge == type) {
                                // linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg),phoneno, username, payLoad));
                                //  MyLog.d("line message========================================>" + linemessage);
                            } else if (MessageFactory.video == type) {
                                payLoad = mContext.getString(R.string.video);
                                //linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg),phoneno, username, payLoad));
                            } else if (MessageFactory.picture == type) {
                                payLoad = mContext.getString(R.string.photo);
                                //   linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg),phoneno, username, payLoad));
                            } else if (MessageFactory.audio == type) {
                                payLoad = mContext.getString(R.string.Audio);
                                // linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg),phoneno, username, payLoad));
                            } else if (MessageFactory.contact == type) {
                                payLoad = mContext.getString(R.string.contact);
                                //linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg),phoneno, username, payLoad));
                            } else if (MessageFactory.web_link == type) {
                                payLoad = mContext.getString(R.string.weblink);
                                //linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg),phoneno, username, payLoad));
                            } else if (MessageFactory.location == type) {
                                payLoad = mContext.getString(R.string.location);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg),phoneno, username, payLoad));
                            } else if (MessageFactory.group_document_message == type) {
                                payLoad = mContext.getString(R.string.document);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            }

                            //  session.putmessagePrefs(linemessage);
                            doc_id = data.getString("toDocId");

                            HashMap<String, Boolean> chatAndType = new HashMap<>();
                            chatAndType.put(resendTo, isSecretMsg);
                            if (!chat.contains(chatAndType)) {
                                chat.add(chatAndType);
                            }

                            String[] array = doc_id.split("-");
                            receiverDocumentID = array[1];


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    if (chat.size() == 1) {
                        MessageItemChat mchat = new MessageItemChat();
                        mchat.setMessageId(uniqueCurrentID.concat("-").concat(receiverDocumentID).concat("-").concat(msgid));
                        mchat.setTextMessage(payLoad);
                        mchat.setSenderName(username);
                        mchat.setNumberInDevice(contactno);
                        Log.e("NotificationUtil","setNumberInDevice"+contactno);

                        Intent intent;

                        if (isSingleChat) {
//Check Missed call redirect
                            //      if (!isSecretMsg) {
                            if (isSecretMsg) {
                                intent = new Intent(mContext, SecretChatViewActivity.class);
                                navigateToChatFromService(intent, receiverDocumentID, username);
                            } else {
                                intent = new Intent(mContext, ChatPageActivity.class);
                                navigateToChatFromService(intent, receiverDocumentID, username);
                            }
                        } else {
                            intent = new Intent(mContext, ChatPageActivity.class);
                            navigateToChatFromService(intent, receiverDocumentID, username);
                        }
                        if (isLockChat) {
                            intent.putExtra("isLockChat", true);
                        }

                       /* boolean is_gossip = mContext.getResources().getBoolean(R.bool.is_gossip);
                        if (is_gossip) {

                            pIntent  = PendingIntent.getBroadcast(mContext, REQUEST_CODE_MORE, new Intent(mContext, NotificationReceiver.class).putExtra(KEY_INTENT_MORE, REQUEST_CODE_MORE), PendingIntent.FLAG_UPDATE_CURRENT);


                        }else {
                            pIntent = PendingIntent.getActivity(mContext, 0, intent,
                                    PendingIntent.FLAG_UPDATE_CURRENT);
                        }*/

                       /* pIntent = PendingIntent.getActivity(mContext, 0, intent,
                                PendingIntent.FLAG_UPDATE_CURRENT);*/

                       /* pIntent = PendingIntent.getActivity(mContext, 0, intent,
                                PendingIntent.FLAG_UPDATE_CURRENT);*/

                        Intent intent1 = new Intent(mContext, NotificationReceiver.class);
                        intent1.putExtra(KEY_INTENT_MORE, REQUEST_CODE_MORE);
                        intent1.putExtra("data", data.toString());
                        pIntent = PendingIntent.getBroadcast(mContext, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_MUTABLE);
                    } else {
                        Intent intent = new Intent(mContext, NewHomeScreenActivty.class);
                        pIntent = PendingIntent.getActivity(mContext, 0, intent,
                                PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_MUTABLE);
                    }


                    //We need this object for getting direct input from notification
                    RemoteInput remoteInput = new RemoteInput.Builder(MessageService.NOTIFICATION_REPLY)
                            .setLabel(mContext.getString(R.string.please_enter_your_name))
                            .build();

                    //For the remote input we need this action object
                    action = new NotificationCompat.Action.Builder(android.R.drawable.ic_delete,
                            mContext.getString(R.string.reply), pIntent)
                            .addRemoteInput(remoteInput)
                            .build();


                    // Set Notification Title


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        builder = new NotificationCompat.Builder(mContext, "1")
                                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                                .setSmallIcon(R.mipmap.ic_launcher)
                                // Set Ticker Message
                                .setTicker(mContext.getString(R.string.notification_ticker) + username)
                                .setAutoCancel(true)

                                // Set PendingIntent into Notification
                                .setContentIntent(pIntent)
                                // Set RemoteViews into Notification
                                .setContent(remoteViews)
                                .setContentIntent(pIntent)
                                .addAction(action)
                                //.addAction(android.R.drawable.ic_menu_compass, "Mark as Read", pIntent)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setDefaults(Notification.DEFAULT_ALL)
                                .setColor(0xF01a9e5);
                    } else {
                        builder = new NotificationCompat.Builder(mContext)
                                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                                .setSmallIcon(R.mipmap.ic_launcher)
                                // Set Ticker Message
                                .setTicker(mContext.getString(R.string.notification_ticker) + username)
                                .setAutoCancel(true)
                                // Set PendingIntent into Notification
                                .setContentIntent(pIntent)
                                .addAction(action)
                                // .addAction(android.R.drawable.ic_menu_compass, "Mark as Read", pIntent)
                                // Set RemoteViews into Notification
                                .setContent(remoteViews)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setDefaults(Notification.DEFAULT_ALL)
                                .setColor(0xF01a9e5);
                    }

                    // Set Icon

//                        .setColor(ContextCompat.getColor(MessageService.mContext, R.color.transparent));

                    if (chat.size() == 1) {
                        ///           builder.addAction(R.drawable.reload, "reply", pIntent);
                    }


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        int smallIconViewId = mContext.getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

                        if (smallIconViewId != 0) {
                            RemoteViews view = builder.getContentView();
                            if (view != null)
                                view.setViewVisibility(smallIconViewId, View.INVISIBLE);

                            if (builder.getHeadsUpContentView() != null)
                                builder.getHeadsUpContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);

                            if (builder.getBigContentView() != null)
                                builder.getBigContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);
                        }
                    }


                    NotificationCompat.BigTextStyle style =
                            new NotificationCompat.BigTextStyle(builder);
                    int j = 0;
                    for (int i = messagepayload.size() - 1; i >= 0; i--) {
                        varchatmess = varchatmess + messagepayload.get(i);
                        if (j >= 10) {
                            break;
                        }
                        MyLog.d(varmsg);
                    }
                    style.bigText(varchatmess);
                    if (chat.size() == 1) {
                        if (messagepayload.size() == 1) {
                            //  style.setSummaryText(messagepayload.size() + " new message");
                            builder.setContentTitle((mContext.getResources().getString(R.string.notification_title)));

                            builder.setContentText(String.format(mContext.getString(R.string.num_new_message), messagepayload.size()));

                        } else {
                            //   style.setSummaryText(messagepayload.size() + " new messages");
                            builder.setContentTitle((mContext.getResources().getString(R.string.notification_title)));
                            builder.setContentText(String.format(mContext.getString(R.string.num_new_message), messagepayload.size()));
                        }
                    } else {
                        //    style.setSummaryText(messagepayload.size() + " messages from " + chat.size() + " chats");
                        if (isSecretMsg) {
                            builder.setContentText(String.format(mContext.getString(R.string.num_new_secret_msg), chat.size()));

                        } else {
                            builder.setContentText(String.format(mContext.getString(R.string.message_from_chat), messagepayload.size(), chat.size()));
                        }

                    }
                    style.setBigContentTitle(mContext.getResources().getString(R.string.notification_title));


                    remoteViews.setImageViewResource(R.id.imagenotileft, R.mipmap.ic_launcher);
                    remoteViews.setTextViewText(R.id.title, (mContext.getResources().getString(R.string.notification_title)));

                    String strMessageCount = mContext.getString(R.string.messages);
                    if (messagepayload.size() == 1) {
                        strMessageCount = mContext.getString(R.string.message);
                    }

                    String strChatCount = mContext.getString(R.string.chats_);
                    if (chat.size() == 1) {
                        strChatCount = mContext.getString(R.string.chat);
                    }
                    payLoad = String.format(mContext.getString(R.string.payload_From_msg_count), messagepayload.size(), strMessageCount, chat.size(), strChatCount);
                    remoteViews.setTextViewText(R.id.text, payLoad);
                    String time = getDate(aMilliesecond, "h:mm a");
                    MyLog.e("time", "time" + time);

                    remoteViews.setTextViewText(R.id.text1, time);

                    if (isSingleChat) {
                        if (session.getlightPrefsName().equals("None") || session.getlightPrefsName().equals("White"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.transparent1);
                        else if (session.getlightPrefsName().equals("Red"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.red);
                        else if (session.getlightPrefsName().equals("Yellow"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.yellow);
                        else if (session.getlightPrefsName().equals("Green"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.green);
                        else if (session.getlightPrefsName().equals("Cyan"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.cyan);
                        else if (session.getlightPrefsName().equals("Blue"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.blue);
                        else if (session.getlightPrefsName().equals("Purple"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.purple);
                    } else {
                        if (session.getlightPrefsNamegroup().equals("None") || session.getlightPrefsNamegroup().equals("White"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.transparent1);
                        else if (session.getlightPrefsNamegroup().equals("Red"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.red);
                        else if (session.getlightPrefsNamegroup().equals("Yellow"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.yellow);
                        else if (session.getlightPrefsNamegroup().equals("Green"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.green);
                        else if (session.getlightPrefsNamegroup().equals("Cyan"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.cyan);
                        else if (session.getlightPrefsNamegroup().equals("Blue"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.blue);
                        else if (session.getlightPrefsNamegroup().equals("Purple"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.purple);
                    }
                    // Locate and set the Text into customnotificationtext.xml TextViews
                    remoteViews.setTextViewText(R.id.title, (mContext.getResources().getString(R.string.notification_title)));
                    remoteViews.setTextViewText(R.id.text, payLoad);
                    String timee = getDate(aMilliesecond, "h:mm a");
                    MyLog.e("time", "time" + timee);

                    remoteViews.setTextViewText(R.id.text1, timee);


                    // Create Notification Manager
                    //  if (!isSecretMsg) {
                    if (isSecretMsg) {
                        if (isSingleChat) {
                            String locDbDocId = uniqueCurrentID + "-" + resendTo;
                            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);

                            String convId = userInfoSession.getChatConvId(locDbDocId);
                            MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, resendTo, convId, false);
                            boolean canNotify = false;
                            boolean notifySoundEnabled = true;
                            if (muteData != null && muteData.getMuteStatus().equals("1")) {

                                if (cDate.getTime() > muteData.getExpireTs()) {
                                    canNotify = true;
                                    contactDB_sqlite.updateMuteStatus(uniqueCurrentID, resendTo, resendTo, 0, "", "0", isSecretMsg);
                                } else {
                                    notifySoundEnabled = false;//for muted notifications
                                }
                                if (!canNotify && muteData.getNotifyStatus().equals("1")) {
                                    canNotify = true;
                                }
                            } else {
                                canNotify = true;
                                if (type == 21) {
                                    showMissedCallNotification(mFrom, mContactNo);
                                }
                            }

                            setSound(mContext, notifySoundEnabled, canNotify, type,mFrom, builder);

                        } else {
                            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                            boolean canNotify = false, notifySoundEnabled = true;
                            MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, null, resendTo, false);
                            if (muteData != null && muteData.getMuteStatus().equals("1")) {

                                if (cDate.getTime() > muteData.getExpireTs()) {
                                    canNotify = true;
                                    contactDB_sqlite.updateMuteStatus(uniqueCurrentID, resendTo, resendTo, 0, "", "0", isSecretMsg);
                                } else {
                                    notifySoundEnabled = false;//for muted notifications
                                }
                                if (!canNotify && muteData.getNotifyStatus().equals("1")) {
                                    canNotify = true;
                                }
                            } else {
                                canNotify = true;
                            }
                            setSound(mContext, notifySoundEnabled, canNotify, type,mFrom, builder);
                        }
                    } else {
                        if (isSingleChat) {
                            String locDbDocId = uniqueCurrentID + "-" + resendTo;
                            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);

                            String convId = userInfoSession.getChatConvId(locDbDocId);
                            MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, resendTo, convId, false);
                            boolean canNotify = false;
                            boolean notifySoundEnabled = true;
                            if (muteData != null && muteData.getMuteStatus().equals("1")) {

                                if (cDate.getTime() > muteData.getExpireTs()) {
                                    canNotify = true;
                                    contactDB_sqlite.updateMuteStatus(uniqueCurrentID, resendTo, resendTo, 0, "", "0", isSecretMsg);
                                } else {
                                    notifySoundEnabled = false;//for muted notifications
                                }
                                if (!canNotify && muteData.getNotifyStatus().equals("1")) {
                                    canNotify = true;
                                }
                            } else {
                                canNotify = true;
                                if (type == 21) {
                                    showMissedCallNotification(mFrom, mContactNo);
                                }
                            }

                            setSound(mContext, notifySoundEnabled, canNotify, type, mFrom, builder);

                        } else {
                            setSound(mContext, true, true, type, mFrom, builder);
                        }
                    }
                    //   }
                }
            }


        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
    }

    public void CustomshowNotification(Context mContext, JSONObject data, String mFrom, String mType, boolean isSingleChat, boolean isSecretMsg, boolean isLockChat) {

        try {
            Log.d("Taha", "CustomshowNotification: " + data);
            MyLog.e(TAG, "CustomshowNotification: " + data);
            String uniqueCurrentID = SessionManager.getInstance(mContext).getCurrentUserID();
            //   String from = null;
            if (mFrom != null) {
                //   from = data.getString("from");.
                if (!mFrom.equalsIgnoreCase(uniqueCurrentID)) {
                    String username = "";
                    String varchatmess = "";
                    String varmsg = "";
                    String thumbnail = "";
                    String groupName = "";
                    String doc_id = "";
                    String payLoad = "";
                    String resendTo = "";
                    String msgid = "";
                    String mContactNo = "";
                    String contactno = "";
                    PendingIntent pIntent;
                    String receiverDocumentID = "";
                    long aMilliesecond = 0;//AppUtils.parseLong(response.getString("timestamp"));

                    //   String typeStr = data.getString("type");
                    session = new Session(mContext);
                    userInfoSession = new UserInfoSession(mContext);

                    int type = Integer.parseInt(mType);
                    if (data.has("ContactName")) {
                        mContactNo = data.getString("ContactName");
                    }
                    if (data.has("contact_name"))
                        mContactNo = data.getString("contact_name");
                    if (data.has("timestamp")) {
                        aMilliesecond = AppUtils.parseLong(data.getString("timestamp"));
                    }
                    MyLog.d(TAG, "data+data" + data);
                    String recordId = "";
                    if (data.has("recordId")) {
                        recordId = data.getString("recordId");
                    }
                    MyLog.d(TAG, "previousRecordId.equals(recordId " +  previousRecordId +  " " + recordId);
                    if (previousRecordId.get(recordId) != null) //already parsed
                        return;
                    previousRecordId.put(recordId, recordId);

                    RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(),
                            R.layout.customnotification);
                    Date cDate = new Date();
                    if (isSingleChat) {
                        //   if (!isSecretMsg) {
                        resendTo = data.getString("from");
                      /*  String typeStr = data.getString("type");
                        int type = Integer.parseInt(typeStr);
                    */
                        if (data.has("ContactMsisdn")) {
                            contactno = data.getString("ContactMsisdn");

                        } else if (data.has("msisdn")) {
                            contactno = data.getString("msisdn");
                        }

                        MyLog.e(TAG, "mFrom" + mFrom);

                        MyLog.e(TAG, "contactno" + contactno);
                        getcontactname = new Getcontactname(mContext);

                        username = getcontactname.getSendername(mFrom, contactno, mContext);

                        //doc_id = (String) data.get("docId");
                        if (!data.has("doc_id") && !data.has("docId")) {
                            MyLog.d(TAG, "..... returning........");
                            return;
                        }
                        if (data.has("doc_id"))
                            doc_id = data.optString("doc_id");

                        else if (data.has("docId"))
                            doc_id = data.optString("docId");


                        String[] array = doc_id.split("-");

                        HashMap<String, Boolean> chatAndType = new HashMap<>();
                        chatAndType.put(resendTo, isSecretMsg);
                        if (!chat.contains(chatAndType)) {
                            chat.add(chatAndType);
                        }

                        receiverDocumentID = "";
                        if (array[0].equalsIgnoreCase(uniqueCurrentID)) {
                            receiverDocumentID = array[1];
                        } else if (array[1].equalsIgnoreCase(uniqueCurrentID)) {
                            receiverDocumentID = array[0];
                        }
                        msgid = data.getString("msgId");
                        if (msgid != null && msgIds.contains(msgid)) {
                            MyLog.d(TAG, ".....msgid returning........");
                            return;
                        }
                        msgIds.add(msgid);
                        if (data.has("payload"))
                            payLoad = data.getString("payload");


                        //************ for avoid duplicate notifications *************//

                        if (isSecretMsg) {
                            if (MessageFactory.text == type || MessageFactory.nudge == type) {

                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                //   MyLog.d("line message========================================>" + linemessage);
                            } else if (MessageFactory.video == type) {
                                payLoad = mContext.getString(R.string.video);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                //      messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.picture == type) {
                                payLoad = mContext.getString(R.string.photo);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+";";
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.audio == type) {
                                payLoad = mContext.getString(R.string.Audio);
                                //linemessage = linemessage + username + ":" + payLoad + "\n"+",";
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.contact == type) {
                                payLoad = mContext.getString(R.string.contact);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.web_link == type) {
                                payLoad = mContext.getString(R.string.weblink);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.location == type) {
                                payLoad = mContext.getString(R.string.location);
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.document == type) {
                                payLoad = mContext.getString(R.string.document);
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.missed_call == type) {
                                payLoad = mContext.getString(R.string.missed_call);
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            } else if (MessageFactory.CALL_STATUS_RECEIVED == type) {
                                payLoad = mContext.getString(R.string.call_rejected);
//                                messagepayload.add(username + " : " + payLoad + "\n");
                                messagepayload.add(mContext.getString(R.string.payload_new_secret_msg));

                            }
                        } else {
                            if (MessageFactory.text == type || MessageFactory.nudge == type) {

                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                //   MyLog.d("line message========================================>" + linemessage);
                            } else if (MessageFactory.video == type) {
                                payLoad = mContext.getString(R.string.video);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.picture == type) {
                                payLoad = mContext.getString(R.string.photo);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+";";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.audio == type) {
                                payLoad = mContext.getString(R.string.Audio);
                                //linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));

                            } else if (MessageFactory.contact == type) {
                                payLoad = mContext.getString(R.string.contact);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.web_link == type) {
                                payLoad = mContext.getString(R.string.weblink);
                                // linemessage = linemessage + username + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.location == type) {
                                payLoad = mContext.getString(R.string.location);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.document == type) {
                                payLoad = mContext.getString(R.string.document);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.missed_call == type) {
                                payLoad = mContext.getString(R.string.missed_call);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            } else if (MessageFactory.CALL_STATUS_RECEIVED == type) {
                                payLoad = mContext.getString(R.string.call_rejected);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            }
                        }

                        // session.putmessagePrefs(messagepayload.toString());
                        String countDocId = uniqueCurrentID.concat("-").concat(resendTo);
                        //changeBadgeCount(countDocId,mContext);
                        //     }
                    } else {

                        try {
                            String phoneno;
                            String userid = data.getString("from");
                      /*  String typeStr = data.getString("type");
                        int type = Integer.parseInt(typeStr);*/
                            payLoad = "";
                            if (data.has("payload"))
                                payLoad = data.getString("payload");
                            if (data.has("is_tag_applied")) {
                                String istagapply = data.getString("is_tag_applied");
                                if (istagapply.equalsIgnoreCase("1")) {
                                    payLoad = AppUtils.getTaggedMsgs(uniqueCurrentID, payLoad, mContext);
                                }
                            }
                            int groupType = data.optInt("groupType");

                            switch (groupType) {
                                case 4:
                                    MyLog.d(TAG, "CustomshowNotification: no need notify for remove user");
                                    return;

                            }

                            if(data.has("groupId")){
                                resendTo = (String) data.get("groupId");
                            }
                        /*try {
                            startDate = dff.parse(session.gettime(resendTo + "tsNextLine"));
                        } catch (ParseException e) {
                            MyLog.e(TAG,"",e);
                        }*/
                            String contactno2 = data.getString("msisdn");
                            phoneno = getcontactname.getSendername(userid, contactno2);
                            groupName = data.getString("groupName");
                            if (data.has("From_avatar")) {
                                thumbnail = data.getString("From_avatar");
                            }
                      /*  try {

                            startDate = dff.parse(session.gettime(resendTo + "tsNextLine"));
                        } catch (ParseException e) {
                            MyLog.e(TAG,"",e);
                        }*/
                            username = groupName;
                            if (MessageFactory.text == type) {
                                // linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg), phoneno, username, payLoad));
                                //  MyLog.d("line message========================================>" + linemessage);
                            } else if (MessageFactory.video == type) {
                                payLoad = mContext.getString(R.string.video);
                                //linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg), phoneno, username, payLoad));
                            } else if (MessageFactory.picture == type) {
                                payLoad = mContext.getString(R.string.photo);
                                //   linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg), phoneno, username, payLoad));
                            } else if (MessageFactory.audio == type) {
                                payLoad = mContext.getString(R.string.Audio);
                                // linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg), phoneno, username, payLoad));
                            } else if (MessageFactory.contact == type) {
                                payLoad = mContext.getString(R.string.contact);
                                //linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg), phoneno, username, payLoad));
                            } else if (MessageFactory.web_link == type) {
                                payLoad = mContext.getString(R.string.weblink);
                                //linemessage = linemessage + username + "@" + phoneno + ":" + payLoad + "\n"+",";
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg), phoneno, username, payLoad));
                            } else if (MessageFactory.location == type) {
                                payLoad = mContext.getString(R.string.location);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_group_msg), phoneno, username, payLoad));
                            } else if (MessageFactory.group_document_message == type) {
                                payLoad = mContext.getString(R.string.document);
                                messagepayload.add(String.format(mContext.getString(R.string.payload_single_msg), username, payLoad));
                            }

                            //  session.putmessagePrefs(linemessage);
                            doc_id = data.getString("toDocId");

                            HashMap<String, Boolean> chatAndType = new HashMap<>();
                            chatAndType.put(resendTo, isSecretMsg);
                            if (!chat.contains(chatAndType)) {
                                chat.add(chatAndType);
                            }

                            String[] array = doc_id.split("-");
                            receiverDocumentID = array[1];


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    if (chat.size() == 1) {
                        MessageItemChat mchat = new MessageItemChat();
                        mchat.setMessageId(uniqueCurrentID.concat("-").concat(receiverDocumentID).concat("-").concat(msgid));
                        mchat.setTextMessage(payLoad);
                        mchat.setSenderName(username);
                        mchat.setNumberInDevice(contactno);
                        MyLog.e(TAG,"setNumberInDevice"+contactno);

                        Intent intent;

                        if (isSingleChat) {//Check Missed call redirect
                            MyLog.e(TAG, "receiverDocumentID" + receiverDocumentID);

                            MyLog.e(TAG, "username" + username);
                            //      if (!isSecretMsg) {
                            if (isSecretMsg) {
                                intent = new Intent(mContext, SecretChatViewActivity.class);
                                navigateToChatFromService(intent, receiverDocumentID, username);
                            } else {
                                intent = new Intent(mContext, ChatPageActivity.class);
                                if (isLockChat) {
                                    intent.putExtra("isLockChat", true);
                                }
                                navigateToChatFromService(intent, receiverDocumentID, username);
                            }
                        } else {
                            intent = new Intent(mContext, ChatPageActivity.class);
                            navigateToChatFromService(intent, receiverDocumentID, username);
                        }
                        if (isLockChat) {
                            intent.putExtra("isLockChat", true);
                        }
                        pIntent = PendingIntent.getActivity(mContext, 0, intent,
                                PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_MUTABLE);
                    } else {
                        Intent intent = new Intent(mContext, NewHomeScreenActivty.class);
                        pIntent = PendingIntent.getActivity(mContext, 0, intent,
                                PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_MUTABLE);
                    }

                    // Set Notification Title
                    NotificationCompat.Builder builder;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        builder = new NotificationCompat.Builder(mContext, "1")
                                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                                .setSmallIcon(R.mipmap.ic_launcher)
                                // Set Ticker Message
                                .setTicker(String.format(mContext.getString(R.string.notification_ticker_username), username))
                                .setAutoCancel(true)

                                // Set PendingIntent into Notification
                                .setContentIntent(pIntent)
                                // Set RemoteViews into Notification
                                .setContentTitle(mContext.getString(R.string.app_name))
                                .setContent(remoteViews)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setDefaults(Notification.DEFAULT_ALL)
                                .setColor(0xF01a9e5);
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new NotificationCompat.Builder(mContext, "1")
                                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                                .setSmallIcon(R.mipmap.ic_launcher)
                                // Set Ticker Message
                                .setTicker(String.format(mContext.getString(R.string.notification_ticker_username), username))
                                .setAutoCancel(true)
                                .setContentTitle(mContext.getString(R.string.app_name))
                                // Set PendingIntent into Notification
                                .setContentIntent(pIntent)
                                // Set RemoteViews into Notification
                                .setContent(remoteViews)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setDefaults(Notification.DEFAULT_ALL)
                                .setColor(0xF01a9e5);
                    } else {
                        builder = new NotificationCompat.Builder(mContext)
                                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                                .setSmallIcon(R.mipmap.ic_launcher)
                                // Set Ticker Message
                                .setTicker(String.format(mContext.getString(R.string.notification_ticker_username), username))
                                .setAutoCancel(true)
                                // Set PendingIntent into Notification
                                .setContentIntent(pIntent)
                                .setContentTitle(mContext.getString(R.string.app_name))
                                // Set RemoteViews into Notification
                                .setContent(remoteViews)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setDefaults(Notification.DEFAULT_ALL)
                                .setColor(0xF01a9e5);
                    }

                    // Set Icon

//                        .setColor(ContextCompat.getColor(MessageService.mContext, R.color.transparent));

                    if (chat.size() == 1) {
                        builder.addAction(R.drawable.reload, mContext.getString(R.string.reply).toLowerCase(), pIntent);
                    }


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        int smallIconViewId = mContext.getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

                        if (smallIconViewId != 0) {
                            RemoteViews view = builder.getContentView();
                            if (view != null)
                                view.setViewVisibility(smallIconViewId, View.INVISIBLE);

                            if (builder.getHeadsUpContentView() != null)
                                builder.getHeadsUpContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);

                            if (builder.getBigContentView() != null)
                                builder.getBigContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);
                        }
                    }


                    NotificationCompat.BigTextStyle style =
                            new NotificationCompat.BigTextStyle(builder);
                    int j = 0;
                    for (int i = messagepayload.size() - 1; i >= 0; i--) {
                        varchatmess = varchatmess + messagepayload.get(i);
                        if (j >= 10) {
                            break;
                        }
                        MyLog.d(TAG, varmsg);
                    }
                    style.bigText(varchatmess);
                    if (chat.size() == 1) {
                        if (messagepayload.size() == 1) {
                            //  style.setSummaryText(messagepayload.size() + " new message");
                            builder.setContentTitle((mContext.getResources().getString(R.string.notification_title)));

                            builder.setContentText(String.format(mContext.getString(R.string.num_new_message), messagepayload.size()));

                        } else {
                            //   style.setSummaryText(messagepayload.size() + " new messages");
                            builder.setContentTitle((mContext.getResources().getString(R.string.notification_title)));
                            builder.setContentText(String.format(mContext.getString(R.string.num_new_messages), messagepayload.size()));
                        }
                    } else {
                        //    style.setSummaryText(messagepayload.size() + " messages from " + chat.size() + " chats");
                        if (isSecretMsg) {
                            builder.setContentText(String.format(mContext.getString(R.string.you_have_secret_msg), chat.size()));

                        } else {
                            builder.setContentText(String.format(mContext.getString(R.string.message_from_chat), messagepayload.size(), chat.size()));
                        }

                    }
                    style.setBigContentTitle(mContext.getResources().getString(R.string.notification_title));


                    remoteViews.setImageViewResource(R.id.imagenotileft, R.mipmap.ic_launcher);
                    remoteViews.setTextViewText(R.id.title, (mContext.getResources().getString(R.string.notification_title)));

                    String strMessageCount = mContext.getString(R.string.messages);
                    if (messagepayload.size() == 1) {
                        strMessageCount = mContext.getString(R.string.message);
                    }

                    String strChatCount = mContext.getString(R.string.chats_);
                    if (chat.size() == 1) {
                        strChatCount = mContext.getString(R.string.chat);
                    }

                    payLoad = String.format(Locale.getDefault(), mContext.getString(R.string.payload_From_msg_count), messagepayload.size(), strMessageCount, chat.size(), strChatCount);
                    remoteViews.setTextViewText(R.id.text, payLoad);
                    String time = getDate(aMilliesecond, "h:mm a");
                    MyLog.e(TAG, "time" + time);

                    remoteViews.setTextViewText(R.id.text1, time);

                    if (isSingleChat) {
                        if (session.getlightPrefsName().equals("None") || session.getlightPrefsName().equals("White"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.transparent1);
                        else if (session.getlightPrefsName().equals("Red"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.red);
                        else if (session.getlightPrefsName().equals("Yellow"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.yellow);
                        else if (session.getlightPrefsName().equals("Green"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.green);
                        else if (session.getlightPrefsName().equals("Cyan"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.cyan);
                        else if (session.getlightPrefsName().equals("Blue"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.blue);
                        else if (session.getlightPrefsName().equals("Purple"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.purple);
                    } else {
                        if (session.getlightPrefsNamegroup().equals("None") || session.getlightPrefsNamegroup().equals("White"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.transparent1);
                        else if (session.getlightPrefsNamegroup().equals("Red"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.red);
                        else if (session.getlightPrefsNamegroup().equals("Yellow"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.yellow);
                        else if (session.getlightPrefsNamegroup().equals("Green"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.green);
                        else if (session.getlightPrefsNamegroup().equals("Cyan"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.cyan);
                        else if (session.getlightPrefsNamegroup().equals("Blue"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.blue);
                        else if (session.getlightPrefsNamegroup().equals("Purple"))
                            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.purple);
                    }
                    // Locate and set the Text into customnotificationtext.xml TextViews
                    remoteViews.setTextViewText(R.id.title, (mContext.getResources().getString(R.string.notification_title)));
                    remoteViews.setTextViewText(R.id.text, payLoad);
                    String timee = getDate(aMilliesecond, "h:mm a");
                    MyLog.e(TAG, "time" + timee);

                    remoteViews.setTextViewText(R.id.text1, timee);


                    // Create Notification Manager
                    //  if (!isSecretMsg) {
                    if (isSecretMsg) {
                        if (isSingleChat) {
                            String locDbDocId = uniqueCurrentID + "-" + resendTo;
                            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);

                            String convId = userInfoSession.getChatConvId(locDbDocId);
                            MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, resendTo, convId, false);
                            boolean canNotify = false;
                            boolean notifySoundEnabled = true;
                            if (muteData != null && muteData.getMuteStatus().equals("1")) {

                                if (cDate.getTime() > muteData.getExpireTs()) {
                                    canNotify = true;
                                    contactDB_sqlite.updateMuteStatus(uniqueCurrentID, resendTo, resendTo, 0, "", "0", isSecretMsg);
                                } else {
                                    notifySoundEnabled = false;//for muted notifications
                                }
                                if (!canNotify && muteData.getNotifyStatus().equals("1")) {
                                    canNotify = true;
                                }
                            } else {
                                canNotify = true;
                                if (type == 21) {
                                    showMissedCallNotification(mFrom, mContactNo);
                                }
                            }

                            setSound(mContext, notifySoundEnabled, canNotify, type, mFrom, builder);

                        } else {
                            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
                            boolean canNotify = false, notifySoundEnabled = true;
                            MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, null, resendTo, false);
                            if (muteData != null && muteData.getMuteStatus().equals("1")) {

                                if (cDate.getTime() > muteData.getExpireTs()) {
                                    canNotify = true;
                                    contactDB_sqlite.updateMuteStatus(uniqueCurrentID, resendTo, resendTo, 0, "", "0", isSecretMsg);
                                } else {
                                    notifySoundEnabled = false;//for muted notifications
                                }
                                if (!canNotify && muteData.getNotifyStatus().equals("1")) {
                                    canNotify = true;
                                }
                            } else {
                                canNotify = true;
                            }
                            setSound(mContext, notifySoundEnabled, canNotify, type, mFrom, builder);
                        }
                    } else {
                        if (isSingleChat) {
                            String locDbDocId = uniqueCurrentID + "-" + resendTo;
                            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);

                            String convId = userInfoSession.getChatConvId(locDbDocId);
                            MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, resendTo, convId, false);
                            boolean canNotify = false;
                            boolean notifySoundEnabled = true;
                            if (muteData != null && muteData.getMuteStatus().equals("1")) {

                                if (cDate.getTime() > muteData.getExpireTs()) {
                                    canNotify = true;
                                    contactDB_sqlite.updateMuteStatus(uniqueCurrentID, resendTo, resendTo, 0, "", "0", isSecretMsg);
                                } else {
                                    notifySoundEnabled = false;//for muted notifications
                                }
                                if (!canNotify && muteData.getNotifyStatus().equals("1")) {
                                    canNotify = true;
                                }
                            } else {
                                canNotify = true;
                                if (type == 21) {
                                    showMissedCallNotification(mFrom, mContactNo);
                                }
                            }

                            setSound(mContext, notifySoundEnabled, canNotify, type, mFrom, builder);

                        } else {
                            setSound(mContext, true, true, type, mFrom, builder);
                        }
                    }
                    //   }
                }
            }


        } catch (Exception e) {
            MyLog.e(TAG, "", e);
            e.printStackTrace();
        }
    }

    public static void setSound(Context context, boolean notifySoundEnabled, boolean canNotify,
                                int type, String mFrom, NotificationCompat.Builder builder) {
        if (!StorageUtility.getDataFromPreferences(context, AppConstants.SPKeys.SOUNDS.getValue(), true))
            notifySoundEnabled = false;
        if (StorageUtility.getDataFromPreferences(context, AppConstants.SPKeys.ONLINE_STATUS.getValue(), "").equals(AppConstants.UserStatus.OFFLINE.getValue()))
            notifySoundEnabled = false;
        if (StorageUtility.getDataFromPreferences(context, AppConstants.SPKeys.ONLINE_STATUS.getValue(), "").equals(AppConstants.UserStatus.BUSY.getValue()))
            notifySoundEnabled = false;

        boolean mDBrefresh = true;
        if (context != null) {
            mDBrefresh = SharedPreference.getInstance().getBool(context, "dbrefresh");

        }
        int sound = R.raw.msn_message;
        Log.d(TAG, "setSound: mDBrefresh " + mDBrefresh);
        if (mDBrefresh && canNotify) {
            if (type == 15) {
                sound = R.raw.msn_nudge_sound;
            }

            if (!notifySoundEnabled)
                sound =  R.raw.silence;

            if (notifySoundEnabled) {
                AudioPlayerUtils.playSound(context, sound);
            }

            builder.setSound(null);
            builder.setDefaults(0);
            Log.d(TAG, "CustomshowNotification: chatsise" + chat.size());
            NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel("1", context.getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);
//                if (!(session.getTone().contains("None")) && notifySoundEnabled) {
//                    channel.setSound(notification, attributes);
//                } else {
//                    channel.setSound(notification, attributes);
//                    //channel.setSound(null, null);
//                }
                //if(notifySoundEnabled)
                // setVibrateForOreo(context, channel);

                //Configure the notification channel, NO SOUND
                channel.setDescription("no sound");
                channel.setSound(null,null);
                channel.enableLights(false);
                channel.setLightColor(Color.BLUE);
                channel.enableVibration(false);

                notificationmanager.createNotificationChannel(channel);
            }
//            String vibSettings = session.getvibratePrefsName();
//            if (vibSettings == null || vibSettings.isEmpty()) {
//                session.putvibratePrefs("Off");
//            }
//            if (type == 21) {
//
//            } else {
            //  MyLog.e("messagepayload.size()","messagepayload.size()"+messagepayload.size());
            if (messagepayload.size() != 0) {
                notificationNotify(notificationmanager, notifySoundEnabled, 1, mFrom, builder, context);
            }
            //}
        }
    }

    private static void setVibrateForOreo(Context context, NotificationChannel channel) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String vibPref = session.getvibratePrefsName();
            if (vibPref != null && vibPref.contains("Default")) {
                return;
            }
            if (vibPref != null && !vibPref.contains("Off")) {
                channel.enableVibration(true);
                if (session.getvibratePrefsName().contains("Long")) {
                    Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(3000);
                } else if (session.getvibratePrefsName().contains("Short")) {
                    Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(500);

                } else {
                    Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(1500);
                }
            }
        }

    }

    private static void navigateToChatFromService(Intent intent, String receiverDocumentID, String username) {

        intent.putExtra("receiverUid", "");
        intent.putExtra("receiverName", "");
        intent.putExtra("documentId", receiverDocumentID);
        intent.putExtra("friendId", receiverDocumentID);
        intent.putExtra("Image", "");
        intent.putExtra("type", 0);
        intent.putExtra("backfrom", true);
        intent.putExtra("Username", username);
    }


    private static void notificationNotify(NotificationManager notificationmanager,
                                           boolean notifySoundEnabled, int id, String mFrom,
                                           NotificationCompat.Builder builder, Context context) {

        if (!notifySoundEnabled) {
            //builder.setDefaults(Notification.DEFAULT_LIGHTS);
        }
        String friendId = ChatPageActivity.Companion.getFriendId();
        if (!ChatPageActivity.Companion.isChatPage() ||
                (ChatPageActivity.Companion.isChatPage() && friendId != null && !friendId.equals(mFrom))) {
            if (!session.getvibratePrefsName().contains("Off")) {
                Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

                if (session.getvibratePrefsName().contains("Long")) {
                    if (notifySoundEnabled) {
                        vibrateWithDelay(2000, context);
                    } else
                        vibrator.vibrate(3000);
                } else if (session.getvibratePrefsName().contains("Short"))
                    builder.setVibrate(new long[]{1000});
                vibrator.vibrate(500);

            } else {
                builder.setVibrate(new long[0]);
            }
            if (!notifySoundEnabled) {
                builder.setVibrate(new long[0]);
            }
            notificationmanager.notify(id, builder.build());
        }


    }

    private static void vibrateWithDelay(final long millis, final Context context) {
        try {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(millis);
                }
            }, 200);

        } catch (Exception e) {
            Log.e(TAG, "vibrateWithDelay: ", e);
        }
    }

    private static void showMissedCallNotification(String toUserId, String toUserMsisdn) {
        MyLog.e(TAG, "showMissedCallNotification");
        missedCallCount++;
        String content;
        if (missedCallCount > 1) {
            content = String.format(CoreController.mcontext.getString(R.string.missed_call_count), missedCallCount);
        } else {
            content = String.format(CoreController.mcontext.getString(R.string.missed_calls_count), missedCallCount);;
            lastMissedCallId = toUserId;
        }

        if (lastMissedCallId.equalsIgnoreCase(toUserId)) {
            String name = getcontactname.getSendername(toUserId, toUserMsisdn);
            content = String.format(CoreController.mcontext.getString(R.string.from_context), content, name);
        }

        Intent intent = new Intent(CoreController.mcontext, NewHomeScreenActivty.class);
        intent.putExtra(NewHomeScreenActivty.FROM_MISSED_CALL_NOTIFICATION, true);
        PendingIntent pIntent = PendingIntent.getActivity(CoreController.mcontext, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_MUTABLE);
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder =
                    new NotificationCompat.Builder(CoreController.mcontext, "1")
                            //       .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification))
                            // .setSmallIcon(R.drawable.ic_notification)
                            .setSmallIcon(R.mipmap.ic_launcher)

                            // Set Ticker Message
                            .setTicker("")
                            .setContentTitle(CoreController.mcontext.getString(R.string.app_name))
                            .setContentText(content)
                            .setAutoCancel(true)

                            // Set PendingIntent into Notification
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setDefaults(Notification.DEFAULT_ALL)
                            .setContentIntent(pIntent)
                            // Set RemoteViews into Notification
                            // .setVibrate(new long[]{500, 500, 500, 500, 500, 500, 500, 500, 500})
                            .setColor(0xF01a9e5);
        } else {
            builder = new NotificationCompat.Builder(CoreController.mcontext)
                    // Set Icon
                    //   .setSmallIcon(R.drawable.ic_notification)
                    .setSmallIcon(R.mipmap.ic_launcher)

                    // Set Ticker Message
                    .setTicker("")
                    .setContentTitle(CoreController.mcontext.getString(R.string.app_name))
                    .setContentText(content)
                    .setAutoCancel(true)
                    // Set PendingIntent into Notification
                    .setContentIntent(pIntent)
                    // Set RemoteViews into Notification

                    .setColor(ContextCompat.getColor(CoreController.mcontext, R.color.transparent));
        }

        NotificationCompat.BigTextStyle style =
                new NotificationCompat.BigTextStyle(builder);


        style.bigText(content)
                .setBigContentTitle(CoreController.mcontext.getString(R.string.app_name));

        builder.setStyle(style);


        NotificationManager notificationmanager = (NotificationManager) CoreController.mcontext.getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("1",
                    CoreController.mcontext.getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH);

            notificationmanager.createNotificationChannel(channel);
        }
        notificationmanager.notify(1, builder.build());
    }

    public void newGroupNotification(Context context, JSONObject response) {
        String groupType = "0";
        try {
            groupType = response.getString("type");
            if (!groupType.equals("" + MessageFactory.join_new_group)) {
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.new_group_notification);
        Session session = new Session(context);
        String groupName = "", payload = "", from = "", resendTo = "", time = "";

        String uniqueCurrentID = SessionManager.getInstance(context).getCurrentUserID();
        Date cDate = new Date();

        Intent intent = new Intent(context, NewgroupListActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_MUTABLE);
        NotificationCompat.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(context, "1")
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    // Set Ticker Message
                    .setTicker(context.getString(R.string.notification_ticker))
                    .setAutoCancel(true)
                    // Set RemoteViews into Notification
                    .setContent(remoteViews)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setColor(0xF01a9e5);
        } else {
            builder = new NotificationCompat.Builder(context)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    // Set Ticker Message
                    .setTicker(context.getString(R.string.notification_ticker))
                    .setAutoCancel(true)
                    // Set RemoteViews into Notification
                    .setContent(remoteViews)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setColor(0xF01a9e5);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int smallIconViewId = context.getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

            if (smallIconViewId != 0) {
                RemoteViews view = builder.getContentView();
                if (view != null)
                    view.setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getHeadsUpContentView() != null)
                    builder.getHeadsUpContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getBigContentView() != null)
                    builder.getBigContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);
            }
        }


        if (session.getlightPrefsNamegroup().equals("None") || session.getlightPrefsNamegroup().equals("White"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.transparent1);
        else if (session.getlightPrefsNamegroup().equals("Red"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.red);
        else if (session.getlightPrefsNamegroup().equals("Yellow"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.yellow);
        else if (session.getlightPrefsNamegroup().equals("Green"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.green);
        else if (session.getlightPrefsNamegroup().equals("Cyan"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.cyan);
        else if (session.getlightPrefsNamegroup().equals("Blue"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.blue);
        else if (session.getlightPrefsNamegroup().equals("Purple"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.purple);


        try {
            groupName = response.getString("groupName");
            from = response.getString("from");
            resendTo = response.getString("groupId");
            long aMilliesecond = AppUtils.parseLong(response.getString("timestamp"));
            time = getDate(aMilliesecond, "h:mm a");
            MyLog.e("time", "time" + time);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //if its from group created person (you) no need to show notification
        if (uniqueCurrentID != null && uniqueCurrentID.equals(from)) {
            return;
        }

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        // ArrayList<FriendModel> contactList = contactDB_sqlite.getAllScimboContacts();

        String firstName = contactDB_sqlite.getSingleData(from, ContactDB_Sqlite.FIRSTNAME);

        if (firstName != null) {
            payload = payload + firstName;
        } else {
            payload = payload + context.getString(R.string.unknown);
        }
        payload = String.format(context.getString(R.string.invite_you_to_group), payload, groupName);

        remoteViews.setTextViewText(R.id.title, groupName);
        remoteViews.setTextViewText(R.id.text, payload);
        remoteViews.setTextViewText(R.id.text1, time);
        MyLog.e("time", "time" + time);
        remoteViews.setImageViewResource(R.id.imagenotileft, R.mipmap.ic_launcher);


        boolean canNotify = false, notifySoundEnabled = true;
        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, null, resendTo, false);
        if (muteData != null && muteData.getMuteStatus().equals("1")) {

            if (cDate.getTime() > muteData.getExpireTs()) {
                canNotify = true;
                contactDB_sqlite.updateMuteStatus(uniqueCurrentID, resendTo, resendTo, 0, "", "0", false);
            } else {
                notifySoundEnabled = false;
            }
            if (!canNotify && muteData.getNotifyStatus().equals("1")) {
                canNotify = true;
            }
        } else {
            canNotify = true;
        }

        //type 1 -new group

        setSound(context, notifySoundEnabled, canNotify, 1, from, builder);
    }

    private static String getDate(long milliSeconds, String aDateFormat) {
        DateFormat formatter = new SimpleDateFormat(aDateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public void newFriendCustomNotification(Context context, JSONObject jsonObject) {
        String requestStatus = "", message = "", name="", from = "";
        boolean isAccepted = false;
        try {
            requestStatus = jsonObject.getString("RequestStatus");
            name = jsonObject.getString("Name");
            from = jsonObject.getString("_id");
            if (!requestStatus.equals(AppConstants.FriendStatus.REQUEST_RECEIVED.getValue())
                    && !requestStatus.equals(AppConstants.FriendStatus.FRIENDS.getValue())) {
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(requestStatus.equals(AppConstants.FriendStatus.REQUEST_RECEIVED.getValue()))
            message = String.format(context.getString(R.string.friend_request_sent_message), name);
        else if(requestStatus.equals(AppConstants.FriendStatus.FRIENDS.getValue())) {
            message = String.format(context.getString(R.string.friend_request_accepted_message), name);
            try {
                isAccepted = jsonObject.getBoolean("isAccepted");
                if(isAccepted)
                    return;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.customnotification);
        Session session = new Session(context);

        String uniqueCurrentID = SessionManager.getInstance(context).getCurrentUserID();

        Intent intent = new Intent(context, NewHomeScreenActivty.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent.putExtra(NewHomeScreenActivty.FRIEND_ID, from);
        intent.putExtra(NewHomeScreenActivty.FROM_FRIEND_NOTIFICATION, true);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_MUTABLE|PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(mContext, "1")
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    // Set Ticker Message
                    .setTicker(context.getString(R.string.notification_ticker))
                    .setAutoCancel(true)
                    // Set PendingIntent into Notification
                    .setContentIntent(pendingIntent)
                    // Set RemoteViews into Notification
                    .setContentTitle(mContext.getString(R.string.app_name))
                    .setContent(remoteViews)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setColor(0xF01a9e5);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new NotificationCompat.Builder(mContext, "1")
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    // Set Ticker Message
                    .setTicker(context.getString(R.string.notification_ticker))
                    .setAutoCancel(true)
                    .setContentTitle(mContext.getString(R.string.app_name))
                    // Set PendingIntent into Notification
                    .setContentIntent(pendingIntent)
                    // Set RemoteViews into Notification
                    .setContent(remoteViews)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setColor(0xF01a9e5);
        } else {
            builder = new NotificationCompat.Builder(context)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    // Set Ticker Message
                    .setTicker(context.getString(R.string.notification_ticker))
                    .setAutoCancel(true)
                    // Set RemoteViews into Notification
                    .setContent(remoteViews)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setColor(0xF01a9e5);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int smallIconViewId = context.getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

            if (smallIconViewId != 0) {
                RemoteViews view = builder.getContentView();
                if (view != null)
                    view.setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getHeadsUpContentView() != null)
                    builder.getHeadsUpContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getBigContentView() != null)
                    builder.getBigContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);
            }
        }


        if (session.getlightPrefsNamegroup().equals("None") || session.getlightPrefsNamegroup().equals("White"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.transparent1);
        else if (session.getlightPrefsNamegroup().equals("Red"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.red);
        else if (session.getlightPrefsNamegroup().equals("Yellow"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.yellow);
        else if (session.getlightPrefsNamegroup().equals("Green"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.green);
        else if (session.getlightPrefsNamegroup().equals("Cyan"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.cyan);
        else if (session.getlightPrefsNamegroup().equals("Blue"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.blue);
        else if (session.getlightPrefsNamegroup().equals("Purple"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.purple);

        //if its from group created person (you) no need to show notification
        if (uniqueCurrentID != null && uniqueCurrentID.equals(from)) {
            return;
        }

        remoteViews.setTextViewText(R.id.title, mContext.getString(R.string.app_name));
        remoteViews.setTextViewText(R.id.text, message);
        remoteViews.setImageViewResource(R.id.imagenotileft, R.mipmap.ic_launcher);

        boolean canNotify = true, notifySoundEnabled = true;

        setSound(context, notifySoundEnabled, canNotify, 1, from, builder);

        NotificationManager notificationmanager = (NotificationManager) CoreController.mcontext.getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("1",
                    CoreController.mcontext.getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH);

            notificationmanager.createNotificationChannel(channel);
        }
        notificationmanager.notify(1, builder.build());
    }

    public void newFriendNotification(Context context, JSONObject response) {
        String type = "0", payload = "", userId = "";
        try {
            type = response.getString("type");
            payload = response.getString("payload");
            userId = response.getString("from");
            if (!type.equals("" + MessageFactory.send_friend_request)
                    && !type.equals("" + MessageFactory.accept_friend_request)) {
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        messagepayload.add(payload + "\n");
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.customnotification);
        Session session = new Session(context);
        String groupName = "", from = "", resendTo = "", time = "";

        String uniqueCurrentID = SessionManager.getInstance(context).getCurrentUserID();
        Date cDate = new Date();

        Intent intent = new Intent(context, NewHomeScreenActivty.class);
        intent.putExtra(NewHomeScreenActivty.FROM_FRIEND_NOTIFICATION, true);
        intent.putExtra(NewHomeScreenActivty.FRIEND_ID, userId);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_MUTABLE);
        NotificationCompat.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new NotificationCompat.Builder(context, "1")
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    // Set Ticker Message
                    .setTicker(context.getString(R.string.notification_ticker))
                    .setAutoCancel(true)
                    // Set RemoteViews into Notification
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContent(remoteViews)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setColor(0xF01a9e5);
        } else {
            builder = new NotificationCompat.Builder(context)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    // Set Ticker Message
                    .setTicker(context.getString(R.string.notification_ticker))
                    .setAutoCancel(true)
                    // Set RemoteViews into Notification
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContent(remoteViews)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setColor(0xF01a9e5);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int smallIconViewId = context.getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

            if (smallIconViewId != 0) {
                RemoteViews view = builder.getContentView();
                if (view != null)
                    view.setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getHeadsUpContentView() != null)
                    builder.getHeadsUpContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);

                if (builder.getBigContentView() != null)
                    builder.getBigContentView().setViewVisibility(smallIconViewId, View.INVISIBLE);
            }
        }


        if (session.getlightPrefsNamegroup().equals("None") || session.getlightPrefsNamegroup().equals("White"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.transparent1);
        else if (session.getlightPrefsNamegroup().equals("Red"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.red);
        else if (session.getlightPrefsNamegroup().equals("Yellow"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.yellow);
        else if (session.getlightPrefsNamegroup().equals("Green"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.green);
        else if (session.getlightPrefsNamegroup().equals("Cyan"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.cyan);
        else if (session.getlightPrefsNamegroup().equals("Blue"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.blue);
        else if (session.getlightPrefsNamegroup().equals("Purple"))
            remoteViews.setInt(R.id.relative_customnotify, "setBackgroundResource", R.color.purple);


        try {
//            groupName = response.getString("groupName");
//            from = response.getString("from");
//            resendTo = response.getString("groupId");
            long aMilliesecond = AppUtils.parseLong(response.getString("timestamp"));
            time = getDate(aMilliesecond, "h:mm a");
            MyLog.e("time", "time" + time);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //if its from group created person (you) no need to show notification
        if (uniqueCurrentID != null && uniqueCurrentID.equals(from)) {
            return;
        }

        ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(context);
        // ArrayList<FriendModel> contactList = contactDB_sqlite.getAllScimboContacts();

        String firstName = contactDB_sqlite.getSingleData(from, ContactDB_Sqlite.FIRSTNAME);

        remoteViews.setTextViewText(R.id.title, mContext.getString(R.string.app_name));
        remoteViews.setTextViewText(R.id.text, payload);
        remoteViews.setTextViewText(R.id.text1, time);
        MyLog.e("time", "time" + time);
        remoteViews.setImageViewResource(R.id.imagenotileft, R.mipmap.ic_launcher);


        boolean canNotify = false, notifySoundEnabled = true;
        MuteStatusPojo muteData = contactDB_sqlite.getMuteStatus(uniqueCurrentID, null, resendTo, false);
        if (muteData != null && muteData.getMuteStatus().equals("1")) {

            if (cDate.getTime() > muteData.getExpireTs()) {
                canNotify = true;
                contactDB_sqlite.updateMuteStatus(uniqueCurrentID, resendTo, resendTo, 0, "", "0", false);
            } else {
                notifySoundEnabled = false;
            }
            if (!canNotify && muteData.getNotifyStatus().equals("1")) {
                canNotify = true;
            }
        } else {
            canNotify = true;
        }

        NotificationManager notificationmanager = (NotificationManager) CoreController.mcontext.getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("1",
                    CoreController.mcontext.getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH);

            notificationmanager.createNotificationChannel(channel);
        }
        //   notificationmanager.notify(1, builder.build());


        setSound(context, notifySoundEnabled, canNotify, 1, from, builder);
    }
}