package com.chat.android.core.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MessageObjectChat implements Parcelable {
    private String translatedMessage;

    protected MessageObjectChat(Parcel in) {
        translatedMessage = in.readString();
    }
    public MessageObjectChat() {
    }

    public static final Creator<MessageObjectChat> CREATOR = new Creator<MessageObjectChat>() {
        @Override
        public MessageObjectChat createFromParcel(Parcel in) {
            return new MessageObjectChat(in);
        }

        @Override
        public MessageObjectChat[] newArray(int size) {
            return new MessageObjectChat[size];
        }
    };

    public String getTranslatedMessage() {
        return translatedMessage;
    }

    public void setTranslatedMessage(String translatedMessage) {
        this.translatedMessage = translatedMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(translatedMessage);
    }
}
