package com.chat.android.core.model;

import java.io.Serializable;

/**
 * Created by CAS60 on 12/26/2016.
 */
public class CommonInGroupPojo implements Serializable {

    private String avatarPath;
    private String groupName;
    private String groupContactNames;
    private String groupId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupContactNames() {
        return groupContactNames;
    }

    public void setGroupContactNames(String groupContactNames) {
        this.groupContactNames = groupContactNames;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
