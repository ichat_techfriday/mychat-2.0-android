package com.chat.android.core.service;

import static androidx.core.app.NotificationCompat.PRIORITY_LOW;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.chat.android.R;
import com.chat.android.app.calls.CallsActivity;
import com.chat.android.app.calls.DummyCallNotificationActivity;

/**
 * Created by Taha Adnan on 3/21/2022.
 * This service keeps a notification in the status bar that lets user know when a call is ongoing
 */
public class CallService extends Service {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String CHANNEL_ID = "my_channel_01";
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Call notification channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Call Notifications");
            Intent notificationIntent = new Intent(this, DummyCallNotificationActivity.class);

            PendingIntent pendingIntent =
                    PendingIntent.getActivity(this,
                            0,
                            notificationIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_MUTABLE);


            NotificationManager notificationManager = ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE));
            notificationManager.createNotificationChannel(channel);
            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setOngoing(true)
                    .setContentIntent(pendingIntent)
                    .setPriority(PRIORITY_LOW)
                    .setCategory(NotificationCompat.CATEGORY_CALL)
                    .setContentTitle(getString(R.string.ongoing_call))
                    .setContentText(getString(R.string.call_is_in_progress))
                    .build();

            startForeground(100, notification);
        } else {
            Intent notificationIntent = new Intent(this, CallsActivity.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
            PendingIntent pendingIntent =
                    PendingIntent.getActivity(this, 0, notificationIntent, 0|PendingIntent.FLAG_MUTABLE);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setOngoing(true)
                    .setContentIntent(pendingIntent)
                    .setPriority(PRIORITY_LOW)
                    .setCategory(NotificationCompat.CATEGORY_CALL)
                    .setContentTitle(getString(R.string.ongoing_call))
                    .setContentText(getString(R.string.call_is_in_progress))
                    .build();

            startForeground(100, notification);
        }
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
