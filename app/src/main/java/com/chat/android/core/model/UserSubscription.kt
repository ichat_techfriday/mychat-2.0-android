package com.chat.android.core.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Taha Adnan on 6/13/2022.
 */
data class UserSubscription(
    @SerializedName("user_id")
    var userId : String,
    @SerializedName("device_id")
    var deviceId : String,
    @SerializedName("purchaseToken")
    var purchaseToken : String,
    @SerializedName("purchase_time")
    var purchaseTime: Long,
    @SerializedName("sub_id")
    var subId : String) : Serializable {

    override fun toString(): String {
        return super.toString()
    }
}
