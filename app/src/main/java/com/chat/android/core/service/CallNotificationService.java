package com.chat.android.core.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.chat.android.R;
import com.chat.android.app.activity.NewHomeScreenActivty;
import com.chat.android.app.calls.IncomingCallActivity;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.fcm.NotificationUtils;
import com.chat.android.utils.AppConstants;
import com.chat.android.utils.StorageUtility;
import com.chat.android.utils.TextUtils;

import java.util.Objects;

public class CallNotificationService extends Service {
    private static final String TAG = "CallNotificationService";
    private String CHANNEL_ID = CoreController.mcontext.getString(R.string.app_name) + "CallChannel";
    private String CHANNEL_ID2 = CoreController.mcontext.getString(R.string.app_name) + "CallChannel2";
    private String CHANNEL_NAME = CoreController.mcontext.getString(R.string.app_name) + "Call Channel";
    private String CHANNEL_NAME2 = CoreController.mcontext.getString(R.string.app_name) + "Call Channel2";
    private boolean notifySoundEnabled = true;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle data = null;
        String name = "", callType = "", docId = "", fromUserId = "", toUserId = "", fromUserMSISDN = "", callRoomId = "", recordId = "", callTimestamp = "";
        boolean isVideoCall = false;
        if (intent != null && intent.getExtras() != null) {

            data = intent.getExtras();
            name = data.getString(AppConstants.INITIATOR);
            callType = data.getString(AppConstants.CALL_TYPE);
            docId = data.getString(IncomingCallActivity.EXTRA_DOC_ID);
            fromUserId = data.getString(IncomingCallActivity.EXTRA_FROM_USER_ID);
            toUserId = data.getString(IncomingCallActivity.EXTRA_TO_USER_ID);
            fromUserMSISDN = data.getString(IncomingCallActivity.EXTRA_FROM_USER_MSISDN);
            callRoomId = data.getString(IncomingCallActivity.EXTRA_CALL_ROOM_ID);
            recordId = data.getString(IncomingCallActivity.EXTRA_CALL_RECORD_ID);
            isVideoCall = data.getBoolean(IncomingCallActivity.EXTRA_CALL_TYPE);
            callTimestamp = data.getString(IncomingCallActivity.EXTRA_CALL_TIME_STAMP);

        }
        MyLog.d("Initiate notification from CallNotification service ");

        int NOTIFICATION_ID = NotificationUtils.getNotificationID(recordId);
        NotificationCompat.Builder notificationBuilder = NotificationUtils.generateNotificationBuilder(this, docId, callType, name, fromUserId, toUserId, fromUserMSISDN, callRoomId, recordId, isVideoCall, callTimestamp);
        Notification incomingCallNotification = null;
        if (notificationBuilder != null) {
            incomingCallNotification = notificationBuilder.build();
            startForeground(NOTIFICATION_ID, incomingCallNotification);
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();// release your media player here
    }

}