package com.chat.android.eventnotejob;

import android.content.Context;
import androidx.annotation.NonNull;

import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.socket.MessageService;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

/**
 * @author rwondratschek
 */
public class DemoSyncJob extends Job {

    public static final String TAG = "job_demo_tag";

    @Override
    @NonNull
    protected Result onRunJob(@NonNull final Params params) {
        MyLog.e("DemoSyncJob","onRunJob");
        final Context context=this.getContext();
        if(!AppUtils.isMyServiceRunning(context, MessageService.class)){
            MyLog.e("isMyServiceRunning","MessageService started");

            AppUtils.startService(context,MessageService.class);


        }else {
            MyLog.e("isMyServiceRunning","MessageService running already");
        }


        return Result.SUCCESS;

        //     return success ? Result.SUCCESS : Result.FAILURE;
    }
    public static void scheduleJob() {

        new JobRequest.Builder(DemoSyncJob.TAG)
                .startNow()
                .build()
                .schedule();

       /* new JobRequest.Builder(DemoSyncJob.TAG)
                //.setExecutionWindow(2_000L, 2_000L)
                .setUpdateCurrent(true)
                .setPeriodic(900000)
                .build()
                .schedule();

        new JobRequest.Builder(DemoSyncJob.TAG)
                .setExecutionWindow(10000,10000)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setUpdateCurrent(true)
                .build()
                .schedule();*/

    }
}
