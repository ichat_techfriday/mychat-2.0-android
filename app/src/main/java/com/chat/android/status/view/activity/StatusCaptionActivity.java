package com.chat.android.status.view.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.chat.android.R;
import com.chat.android.app.adapter.RItemAdapter;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.app.utils.VideoRequestHandler;
import com.chat.android.app.widget.AvnNextLTProDemiTextView;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.crop.CropImage;
import com.chat.android.crop.CropImageView;
import com.chat.android.status.controller.StatusData;
import com.chat.android.status.controller.interfaces.OnSwipeTouchListener;
import com.chat.android.status.model.StatusModel;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Random;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import id.zelory.compressor.Compressor;
import life.knowledge4.videotrimmer.K4LVideoTrimmerNew;
import life.knowledge4.videotrimmer.interfaces.OnK4LVideoListener;
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener;
import life.knowledge4.videotrimmer.utils.BackgroundExecutor;
import life.knowledge4.videotrimmer.utils.TrimVideoUtils;

import static com.chat.android.status.view.activity.StatusAddActivity.STATUS_ADD;

public class StatusCaptionActivity extends CoreActivity implements View.OnClickListener,
        OnTrimVideoListener, OnK4LVideoListener {
    private static final int MAX_VIDEO_DURATION = 30; // IN SECONDS
    private static final String TAG = StatusCaptionActivity.class.getSimpleName();
    private ImageView delete;
    private ImageView crop;
    private ImageView rotate;
    private ImageView selectimage;
    private EmojiconEditText edCaption;
    private RecyclerView image_recyclerview;
    private ArrayList<StatusModel> pathList = new ArrayList<>();
    private HorizontalAdapter horizontalAdapter;
    private EmojIconActions emojIcon;
    private int currentIndex = 0;
    TextWatcher watch = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub
            edCaption.post(new Runnable() {
                @Override
                public void run() {
                    if (currentIndex > -1 && pathList.size() > 0) {
                        pathList.get(currentIndex).setCaption(edCaption.getText().toString().trim());
                    }
                }
            });

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence cs, int a, int b, int c) {
            // TODO Auto-generated method

        }
    };
    private Bitmap myBitmap;
    private int rotateangle = 0;
    private Picasso picassoInstance;
    private String from, mCurrentUserId;
    private Handler handler;
    private K4LVideoTrimmerNew videoTrimmer;
    private boolean videoTrimmingProblemFound = false;
    private LinearLayout trimmerLayoutHolder;
    private OnSwipeTouchListener onSwipeTouchListener;
    private HashMap<Integer, StatusModel> selectedGalleryItems;

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        try {
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_status_caption);
        handler = new Handler();
        VideoRequestHandler videoRequestHandler = new VideoRequestHandler();
        picassoInstance = new Picasso.Builder(getApplicationContext())
                .addRequestHandler(videoRequestHandler)
                .build();

        SessionManager sessionManager = SessionManager.getInstance(this);
        from = sessionManager.getCurrentUserID();
        mCurrentUserId = sessionManager.getCurrentUserID();
        ImageView ibBack = findViewById(R.id.ibBack);
        selectimage = findViewById(R.id.selectimage);
        setTouchListener(this);
        selectimage.setOnTouchListener(onSwipeTouchListener);
        trimmerLayoutHolder = findViewById(R.id.trimmer_view_holder);
        ImageView sendbutton = findViewById(R.id.iv_send_btn);
        delete = findViewById(R.id.delete);
        ImageView selEmoji = findViewById(R.id.emojiButton);
        crop = findViewById(R.id.crop);
        rotate = findViewById(R.id.rotate);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        View rootView = findViewById(R.id.mainRelativeLayout);
        AvnNextLTProDemiTextView sendername = findViewById(R.id.sendername);

        edCaption = findViewById(R.id.ed_caption);
        image_recyclerview = findViewById(R.id.image_recyclerview);
        LinearLayoutManager mediaManager = new LinearLayoutManager(StatusCaptionActivity.this, LinearLayoutManager.HORIZONTAL, false);
        image_recyclerview.setLayoutManager(mediaManager);
        // ivPlayIcon = (ImageView) findViewById(R.id.video_ic);


        Intent intent_from = getIntent();
        String mReceiverName = "";
        if (intent_from != null) {
            // String from = intent_from.getStringExtra("from");
            mReceiverName = intent_from.getStringExtra("phoneno");
            //  boolean fromGalleryPicker = intent_from.getBooleanExtra(Keys.FROM_GALLERY_PICKER, false);
        }
        sendername.setText(mReceiverName);


        ibBack.setOnClickListener(this);
        emojIcon = new EmojIconActions(this, rootView, edCaption, selEmoji);
        selEmoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emojIcon.ShowEmojIcon();
            }
        });
        //  emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {

                MyLog.e("", "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                MyLog.e("", "Keyboard Closed!");

            }
        });
        emojIcon.setUseSystemEmoji(false);
        edCaption.addTextChangedListener(watch);
        sendbutton.setOnClickListener(this);
        crop.setOnClickListener(this);
        rotate.setOnClickListener(this);
        delete.setOnClickListener(this);
        image_recyclerview.addOnItemTouchListener(new RItemAdapter(StatusCaptionActivity.this, image_recyclerview, new RItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                setPreviewImage(position);


            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        if (StatusData.getInstance().selectedGalleryItems != null) {
            selectedGalleryItems = StatusData.getInstance().selectedGalleryItems;
            pathList.clear();
            if (selectedGalleryItems.size() > 0) {
                for (Integer key : selectedGalleryItems.keySet()) {
                    MyLog.d(TAG, "onCreate: " + key);
                    long timeSelected = selectedGalleryItems.get(key).getTimeSelected();
                    pathList.add(selectedGalleryItems.get(key));
                }
            }
        }
        if (pathList.size() > 0) {
            //sort by selected time

            Collections.sort(pathList, new Comparator<StatusModel>() {
                @Override
                public int compare(StatusModel o1, StatusModel o2) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        return Long.compare(o1.getTimeSelected(), o2.getTimeSelected());
                    }
                    return -1;
                }
            });

            //reverse the list --> for recent selection come to first
            //Collections.reverse(pathList);
        }
        if (pathList.size() > 0) {
            setPreviewImage(0);
        }
        setAdapter();
    }

    private void initAndShowVideoTrimmer(String path) {

        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View trimmerLayout = inflater.inflate(R.layout.status_video_trimmer_layout, null);

        trimmerLayoutHolder.removeAllViews();

        videoTrimmer = trimmerLayout.findViewById(R.id.trimmer_view);
        videoTrimmer.setVideoURI(Uri.parse(path));
        trimmerLayoutHolder.addView(videoTrimmer);


        videoTrimmer.setOnTouchListener(onSwipeTouchListener);
        videoTrimmer.setVisibility(View.VISIBLE);
        videoTrimmer.setOnTrimVideoListener(this);
        videoTrimmer.setOnK4LVideoListener(this);
        videoTrimmer.setVideoInformationVisibility(true);
    }

    @Override
    public void onBackPressed() {
        StatusData.getInstance().selectedGalleryItems = selectedGalleryItems;
        super.onBackPressed();

    }

    private void swipe(int changes) {
        try {
            int tempCurrentIndex = currentIndex + changes;
            if (tempCurrentIndex >= 0 && pathList != null && (tempCurrentIndex < pathList.size())) {
                StatusModel statusModel = pathList.get(tempCurrentIndex);
                if (statusModel != null) {
                    setPreviewImage(tempCurrentIndex);
                    image_recyclerview.getLayoutManager().scrollToPosition(tempCurrentIndex);
                }
            }
        } catch (Exception e) {
            MyLog.e(TAG, "swipe: ", e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (handler != null)
            handler.removeCallbacksAndMessages(null);
    }

    private void setTouchListener(Context context) {
        onSwipeTouchListener = new OnSwipeTouchListener(context) {
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                MyLog.d(TAG, "onSwipeLeft: ");
                swipe(1);

            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                swipe(-1);


            }
        };

    }

    private void setPreviewImage(int position) {
        StatusModel statusModel = pathList.get(position);
        if (statusModel != null) {
            //video
            if (statusModel.isVideo()) {
                hideMenuItems();
                selectimage.setVisibility(View.GONE);

                // ivPlayIcon.setVisibility(View.VISIBLE);
                initAndShowVideoTrimmer(statusModel.getLocalPath());
                picassoInstance.load(VideoRequestHandler.SCHEME_VIDEO + ":" + statusModel.getLocalPath()).into(selectimage);
            }
            //Image
            else {
                selectimage.setVisibility(View.VISIBLE);
                //   ivPlayIcon.setVisibility(View.GONE);
                if (videoTrimmer != null)
                    videoTrimmer.setVisibility(View.GONE);
                showMenuItems();
                AppUtils.loadLocalImage(StatusCaptionActivity.this, statusModel.getLocalPath(), selectimage);
            }


            currentIndex = position;
            edCaption.setText(pathList.get(position).getCaption());
        }
    }

    private void showMenuItems() {
        delete.setVisibility(View.VISIBLE);
        rotate.setVisibility(View.GONE);
        crop.setVisibility(View.VISIBLE);

    }

    private void hideMenuItems() {
        //delete.setVisibility(View.GONE);
        rotate.setVisibility(View.GONE);
        crop.setVisibility(View.INVISIBLE);
        crop.getLayoutParams().width=1;
        crop.setOnClickListener(null);

    }

    private void setAdapter() {
        horizontalAdapter = new HorizontalAdapter();
        image_recyclerview.setAdapter(horizontalAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                String filePath = resultUri.getPath();


                File image = new File(filePath);
                Bitmap compressBmp = Compressor.getDefault(StatusCaptionActivity.this).compressToBitmap(image);
                selectimage.setImageBitmap(compressBmp);


                myBitmap = compressBmp;
                saveImage(compressBmp);


                selectimage.setScaleType(ImageView.ScaleType.FIT_XY);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            Uri uri = Crop.getOutput(data);
            String filePath = uri.getPath();


            File image = new File(filePath);
            Bitmap compressBmp = Compressor.getDefault(StatusCaptionActivity.this).compressToBitmap(image);
            selectimage.setImageBitmap(compressBmp);
            myBitmap = compressBmp;
            saveImage(compressBmp);
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_CANCELED) {

        } else if (resultCode == RESULT_CANCELED) {
            pathList.clear();
            finish();
        }


    }

    private void saveImage(Bitmap finalBitmap) {

        File imgDir = new File(MessageFactory.IMAGE_STORAGE_PATH);
        if (!imgDir.exists()) {
            imgDir.mkdirs();
        }
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(imgDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            MyLog.e(TAG, "saveImage: ", e);
        }
        pathList.get(currentIndex).setLocalPath(file.getAbsolutePath());

        if (horizontalAdapter != null) {
            horizontalAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        MyLog.d(TAG, "onClick: ");

        switch (v.getId()) {
            case R.id.ibBack:
                Intent cancelIntent = new Intent();
                setResult(RESULT_CANCELED, cancelIntent);
                finish();
                pathList.clear();
                break;


            case R.id.iv_send_btn:
                MyLog.d(TAG, "onClick: send button");
                if (pathList != null && pathList.size() > 0) {
                    int uploadIndex = 0;
                    for (final StatusModel statusModel : pathList) {
                        uploadIndex++;
                        if (statusModel.isVideo()) {
                            if (statusModel.isTrimmed()) {
                                initProgress("Processing status " + uploadIndex + "/" + pathList.size(), false);
                                showProgressDialog();
                                BackgroundExecutor.execute(
                                        new BackgroundExecutor.Task("", 0L, "") {
                                            @Override
                                            public void execute() {
                                                try {
                                                    final int endPosition = statusModel.getVideoEndPosition();
                                                    MyLog.d(TAG, "uploadStatus: " + endPosition);
                                                    File folder = Environment.getExternalStorageDirectory();
                                                    final String destinationPath = folder.getPath() + File.separator;
                                                    TrimVideoUtils.startTrim(new File(statusModel.getLocalPath()), destinationPath, statusModel.getVideoStartPosition(),
                                                            endPosition, new OnTrimVideoListener() {

                                                                @Override
                                                                public void onTrimStarted() {

                                                                }

                                                                @Override
                                                                public void getResult(Uri uri) {
                                                                    if (uri != null) {
                                                                        MyLog.d(TAG, "getResult: " + uri.getPath());
                                                                        if (null != uri.getPath()) {
                                                                            statusModel.setLocalPath(uri.getPath());
                                                                            CoreController.getStatusDB(getApplicationContext()).insertNewStatus(Collections.singletonList(statusModel), mCurrentUserId);
                                                                        } else {
                                                                            videoTrimmingProblemFound = true;
                                                                        }

                                                                    }
                                                                }

                                                                @Override
                                                                public void cancelAction() {

                                                                }

                                                                @Override
                                                                public void onError(String message) {

                                                                }

                                                                @Override
                                                                public void getCurrentPosition(int startPosition, int endPosition) {

                                                                }
                                                            });
                                                } catch (Exception e) {
                                                    MyLog.e(TAG, "uploadStatus: ", e);
                                                }

                                            }
                                        }
                                );
                            } else {
                                CoreController.getStatusDB(getApplicationContext()).insertNewStatus(Collections.singletonList(statusModel), mCurrentUserId);
                            }
                        } else {
                            CoreController.getStatusDB(getApplicationContext()).insertNewStatus(Collections.singletonList(statusModel), mCurrentUserId);
                        }
                    }
                    hideProgressDialog();

                }

                Intent okIntent = new Intent();
                setResult(STATUS_ADD, okIntent);
                finish();

                break;

            case R.id.rotate:
                try {
                    rotateangle = rotateangle + 90;
                    Bitmap rotatebitmap = RotateBitmap(myBitmap, rotateangle);
                    if (rotatebitmap != null) {
                        selectimage.setImageBitmap(rotatebitmap);
                        saveImage(rotatebitmap);
                    }
                } catch (Exception e) {
                    MyLog.e(TAG, "rotate ", e);
                }
                break;

            case R.id.crop:
                //     beginCrop(Uri.fromFile(new File(pathList.get(currentIndex).getLocalPath())));
                //  beginCrop(Uri.fromFile(new File(pathList.get(currentIndex).getLocalPath())));


                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this, pathList.get(currentIndex).getLocalPath());

                break;
            case R.id.delete:
                if (pathList == null)
                    return;
                if (pathList.size() > 1) {
                    try {
                        if (currentIndex < pathList.size()) {
                            pathList.get(currentIndex).setVideoStartPosition(0);
                            pathList.get(currentIndex).setVideoEndPosition(0);
                            StatusModel statusModel = pathList.get(currentIndex);
                            pathList.remove(currentIndex);
                            try {
                                selectedGalleryItems.values().remove(statusModel);
                            } catch (Exception e) {
                                MyLog.e(TAG, "onClick: ", e);
                            }
                        }
                        if (pathList.size() > 0) {
                            setAdapter();
                            if (pathList.size() > 0) {
                                setPreviewImage(0);
                                currentIndex = 0;
                                if (pathList.size() == 1) {
                                    image_recyclerview.setVisibility(View.GONE);
                                    currentIndex = 0;
                                } else {
                                    image_recyclerview.setVisibility(View.VISIBLE);
                                }
                            } else {
                                finish();
                            }
                        } else {
                            finish();
                        }
                    } catch (Exception e) {
                        MyLog.e(TAG, "onClick: ", e);
                    }

                } else {
                    StatusData.getInstance().selectedGalleryItems=new HashMap<>();
                    finish();
                }
                break;
        }
    }

    @Override
    public void onTrimStarted() {
        MyLog.d(TAG, "onTrimStarted: ");
    }

    @Override
    public void getResult(Uri uri) {
        if (uri != null && uri.getPath() != null)
            MyLog.d(TAG, "getResult: " + uri.getPath());
    }

    @Override
    public void cancelAction() {
        MyLog.d(TAG, "cancelAction: ");
    }

    @Override
    public void onError(String message) {
        MyLog.d(TAG, "onError: " + message);
    }

    @Override
    public void getCurrentPosition(int startPosition, int endPosition) {
        MyLog.d(TAG, "getCurrentPosition: " + startPosition);
        MyLog.d(TAG, "getCurrentPosition: " + endPosition);
        pathList.get(currentIndex).setVideoStartPosition(startPosition);
        pathList.get(currentIndex).setVideoEndPosition(endPosition);
        pathList.get(currentIndex).setTrimmed(true);
    }

    @Override
    public void onVideoPrepared(int totalDuration) {
        MyLog.d(TAG, "onVideoPrepared: ");
        int maxDurationInMillis = MAX_VIDEO_DURATION * 1000;
        if (totalDuration > maxDurationInMillis) {
            pathList.get(currentIndex).setTrimmed(true);
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                int startPosition = pathList.get(currentIndex).getVideoStartPosition();
                int endPosition = pathList.get(currentIndex).getVideoEndPosition();
                MyLog.d(TAG, "K4LVideoTrimmerNew run: " + startPosition);
                MyLog.d(TAG, "K4LVideoTrimmerNew run: " + endPosition);
                if (startPosition > 0 || endPosition > 0) {
                    videoTrimmer.setStartPosition(startPosition);
                    videoTrimmer.setEndPosition(endPosition);
                    videoTrimmer.setRangeSeekBar();
                }
                videoTrimmer.setTimeFrames();
            }
        });
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listrow_status_gallery_caption, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            if (pathList != null && pathList.get(position) != null) {
                StatusModel statusModel = pathList.get(position);

                if (statusModel.isVideo()) {
                    holder.ivVideoIcon.setVisibility(View.VISIBLE);
                    holder.listimage.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
                    //new LoadVideoThumbnailTask(path,holder.ivGalleryImage);
                    picassoInstance.load(VideoRequestHandler.SCHEME_VIDEO + ":" + statusModel.getLocalPath()).into(holder.listimage);
                } else {

                    AppUtils.loadLocalImage(StatusCaptionActivity.this, statusModel.getLocalPath(), holder.listimage);
                    holder.ivVideoIcon.setVisibility(View.GONE);

                }
            }
        }

        @Override
        public int getItemCount() {
            return pathList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView listimage, ivVideoIcon;

            public MyViewHolder(View view) {
                super(view);

                listimage = view.findViewById(R.id.listimage);
                ivVideoIcon = view.findViewById(R.id.video_icon);
            }
        }
    }

}


