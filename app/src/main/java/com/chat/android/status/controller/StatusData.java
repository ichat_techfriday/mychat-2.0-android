package com.chat.android.status.controller;

import com.chat.android.status.model.StatusModel;

import java.util.HashMap;

/**
 * Created by user134 on 4/2/2018.
 */

public class StatusData {
    private static final StatusData ourInstance = new StatusData();

    public static StatusData getInstance() {
        return ourInstance;
    }

    private StatusData() {
    }
    public HashMap<Integer,StatusModel> selectedGalleryItems;
}
