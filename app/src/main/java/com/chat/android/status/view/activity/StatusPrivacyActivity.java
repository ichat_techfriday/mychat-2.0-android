package com.chat.android.status.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.chat.android.R;
import com.chat.android.app.utils.Keys;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreActivity;
import com.chat.android.status.controller.SharedPrefUtil;
import com.chat.android.status.controller.StatusSocketEvents;

/**
 * Created by user134 on 4/20/2018.
 */

public class StatusPrivacyActivity extends CoreActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = StatusPrivacyActivity.class.getSimpleName();
    private RadioButton rbAllContacts, rbAllContactsExcept, rbOnlyShareWith;

    public static final String TYPE_MY_CONTACTS ="my_contacts";
    public static final String TYPE_ONLY_SHARE_WITH ="only_share";
    public static final String TYPE_SHARE_ALL_EXCEPT ="my_contacts_except";

    private boolean isFirstTime=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_privacy);
        findViewById(R.id.iv_back).setOnClickListener(this);

       /* Toolbar toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
*/
        rbAllContacts = findViewById(R.id.rb_all_contacts);

        rbAllContactsExcept = findViewById(R.id.rb_all_contacts_except);
        rbOnlyShareWith = findViewById(R.id.rb_only_share_with);

        rbAllContacts.setOnCheckedChangeListener(this);
        rbAllContactsExcept.setOnCheckedChangeListener(this);
        rbOnlyShareWith.setOnCheckedChangeListener(this);


        rbAllContacts.setOnClickListener(this);
        rbAllContactsExcept.setOnClickListener(this);
        rbOnlyShareWith.setOnClickListener(this);

        String prevSelection= SharedPrefUtil.getPrivacyType();
        switch (prevSelection){
            case TYPE_MY_CONTACTS:
                rbAllContacts.setChecked(true);
                break;

            case TYPE_ONLY_SHARE_WITH:
                rbOnlyShareWith.setChecked(true);
                break;

            case TYPE_SHARE_ALL_EXCEPT:
                rbAllContactsExcept.setChecked(true);
                break;

        }

    }

    private boolean resetCheckBox() {
        rbAllContacts.setChecked(false);
        rbAllContactsExcept.setChecked(false);
        rbOnlyShareWith.setChecked(false);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.rb_all_contacts:
                if(rbAllContacts.isChecked())
                    allContactsClick();
                break;

            case R.id.rb_all_contacts_except:
                if(rbAllContactsExcept.isChecked())
                    allContactsExcept();
                break;

            case R.id.rb_only_share_with:
                if(rbOnlyShareWith.isChecked())
                onlyShareWith();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyLog.d(TAG, "onResume: ");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //first time we checked programatically.. so skip this click event.
        // we need to capture event only from user
        if(isFirstTime){
            isFirstTime=false;
            return;
        }
        boolean isReset= resetCheckBox();
        if(!isChecked){
            return;
        }


        switch (buttonView.getId()) {
            case R.id.rb_all_contacts:
               allContactsClick();
                break;

            case R.id.rb_all_contacts_except:

                allContactsExcept();
                break;

            case R.id.rb_only_share_with:
                onlyShareWith();
                break;
        }
    }

    private void onlyShareWith() {
        rbOnlyShareWith.setChecked(true);
        startStatusPrivacyContactsPage(TYPE_ONLY_SHARE_WITH);
    }

    private void allContactsExcept() {
        rbAllContactsExcept.setChecked(true);
        startStatusPrivacyContactsPage(TYPE_SHARE_ALL_EXCEPT);
    }

    private void allContactsClick() {
        rbAllContacts.setChecked(true);
        StatusSocketEvents.setStatusPrivacy(this,"my_contacts",null,"");
        finish();
    }


    private void startStatusPrivacyContactsPage(String type){
        MyLog.d(TAG, "startStatusPrivacyContactsPage: "+type);
        Intent intent=new Intent(this,StatusPrivacyContactsActivity.class);
        intent.putExtra(Keys.TYPE,type);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }
}
