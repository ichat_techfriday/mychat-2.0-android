package com.chat.android.status.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chat.android.R;
import com.chat.android.app.activity.ForwardContact;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.CommonData;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreController;
import com.chat.android.core.message.IncomingMessage;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.message.TextMessage;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.status.controller.StatusUtil;
import com.chat.android.status.controller.interfaces.OnListClickListener;
import com.chat.android.status.model.StatusDB;
import com.chat.android.status.model.StatusModel;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user134 on 3/29/2018.
 */

public class MyStatusListAdapter extends RecyclerView.Adapter<MyStatusListAdapter.MyViewHolder> {
    private Activity mContext;
    private List<StatusModel> mData = new ArrayList<>();
    private static final String TAG = MyStatusListAdapter.class.getSimpleName();
    private long lastClickTime = 0L;
    private boolean isLongPressed = false;
    private OnListClickListener onListClickListener;

    public MyStatusListAdapter(Activity context, List<StatusModel> data, OnListClickListener onListClickListener) {
        this.mContext = context;
        this.mData = data;
        this.onListClickListener = onListClickListener;
    }

    public List<StatusModel> getData() {
        return mData;
    }

    public void resetLongPress() {
        isLongPressed = false;
    }

    public boolean isLongPressed() {
        return isLongPressed;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.status_list_row_my_status, viewGroup, false);
        return new MyViewHolder(view);
    }

    public void resetSelection() {
        if (mData != null)
            for (int i = 0; i < mData.size(); i++) {
                mData.get(i).setSelected(false);
            }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int postion) {

        if (mData != null && mData.get(postion) != null) {
            final StatusModel statusModel = mData.get(postion);
            if (statusModel != null) {
                holder.rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isLongPressed) {
                            openViewedPersonsList(statusModel, false);
                        } else {
                            mData.get(postion).setSelected(!mData.get(postion).isSelected());
                            notifyItemChanged(postion);
                            if (onListClickListener != null) {
                                onListClickListener.onItemClick(postion, "", isLongPressed);
                            }
                        }
                    }
                });
                holder.rootView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (!isLongPressed) {
                            isLongPressed = true;
                            resetSelection();
                            mData.get(postion).setSelected(true);
                            notifyItemChanged(postion);
                            if (onListClickListener != null) {
                                onListClickListener.onItemClick(postion, "", isLongPressed);
                            }
                        }
                        return true;
                    }
                });


                holder.tvStatusCount.setText("" + statusModel.getStatusViewHistory().size());

                holder.ivEye.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openViewedPersonsList(statusModel, true);
                    }
                });
                holder.tvStatusCount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openViewedPersonsList(statusModel, true);
                    }
                });

                holder.ivFoward.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (statusModel.isTextstatus()) {
                            Log.e("statusModel","statusModel"+statusModel.toString());
                            //Check group or not

                            IncomingMessage incomingMsg = new IncomingMessage(mContext);
                            MessageItemChat item = incomingMsg.loadSingleStatusMessage(statusModel);
                            ArrayList<MessageItemChat> selectedChatItems = new ArrayList<>();
                            selectedChatItems.add(item);
                            Bundle fwdBundle = new Bundle();
                            fwdBundle.putBoolean("FromScimbo", true);
                            CommonData.setForwardedItems(selectedChatItems);
                            Intent intent = new Intent(mContext, ForwardContact.class);
                            intent.putExtras(fwdBundle);
                            mContext.startActivity(intent);

                        } else {
                            try {
                                long currentMillis = System.currentTimeMillis();
                                long minTimeForNextClick = 2000L;
                                if (lastClickTime == 0 || (currentMillis - lastClickTime) > minTimeForNextClick) {
                                    Uri fileUri = Uri.fromFile(new File(statusModel.getLocalPath()));
                                    String caption = statusModel.getCaption();
                                    if (caption == null)
                                        caption = "";
                                    StatusUtil.shareImageOrVideo(caption, fileUri, statusModel.isImage(), mContext);
                                    lastClickTime = System.currentTimeMillis();
                                }
                            } catch (Exception e) {
                                MyLog.e(TAG, "onClick: ", e);
                            }
                        }
                    }
                });
                if (statusModel.isTextstatus()) {
                    int color = Color.parseColor(statusModel.getTextstatus_color_code());
                    holder.ivStatusImage.setImageDrawable(new ColorDrawable(color));
                    if (statusModel != null) {
                        if (statusModel.getTextstatus_caption() != null) {
                            holder.text_status_caption.setText(statusModel.getTextstatus_caption());
                        }
                    }
                } else {
                    //Check status
                    StatusUtil.setImage(mContext, holder.ivStatusImage, statusModel);

                }
                //   StatusUtil.setImage(mContext,holder.ivStatusImage,statusModel);
                //AppUtils.LoadImage(mContext,statusModel.getUrl(),holder.ivStatusImage,R.color.transparent);


                int uploadStatus = CoreController.getStatusDB(mContext).uploadStatus(statusModel.getDocId());


                switch (uploadStatus) {
                    case StatusDB.NOT_UPLOADED:
                        holder.tvTime.setText(mContext.getString(R.string.waiting));
                        break;
                    case StatusDB.UPLOADING:
                        holder.tvTime.setText(mContext.getString(R.string.sending));
                        break;
                    case StatusDB.UPLOADED:
                        // holder.tvTime.setText(AppUtils.getStatusTime(mContext, statusModel.getTimeUploaded()));
                        holder.tvTime.setText(AppUtils.getStatusTime(mContext, statusModel.getTimeUploaded(), true));

                        break;

                }


                if (isLongPressed && statusModel.isSelected()) {
                    holder.ivSelectionIcon.setVisibility(View.VISIBLE);
                    holder.rootView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.gray_list_selection));
                } else {
                    holder.ivSelectionIcon.setVisibility(View.INVISIBLE);
                    holder.rootView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
                }
                MyLog.d(TAG, "onBindViewHolder: docId" + statusModel.getDocId());

            }
        }
    }

    private void sendTextMessage() {
        String data = "";//sendMessage.getText().toString().trim();
        String data_at = data;
        String htmlText = data;
        int isTagapplied = 0;
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> userID = new ArrayList<>();

  /*          for (GroupMembersPojo groupMembersPojo : at_memberlist) {
                String userName = "@" + groupMembersPojo.getContactName();
                names.add(userName);
                userID.add(groupMembersPojo.getUserId());
                if (data_at.contains(userName)) {
                    data_at = data_at.replace(userName, TextMessage.TAG_KEY + groupMembersPojo.getUserId() + TextMessage.TAG_KEY);
                    isTagapplied = 1;
                }
  */

        TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, mContext);
        //Check group or not

        JSONObject msgObj;
      //  msgObj = (JSONObject) message.getGroupMessageObject(to, data_at, user_name.getText().toString());


    /*} else{
        msgObj = (JSONObject) message.getMessageObject(to, data, false);
    }*/
    ///data_at

  //  MessageItemChat item = message.createMessageItem(true, htmlText, MessageFactory.DELIVERY_STATUS_NOT_SENT, mReceiverId, user_name.getText().toString());

    //Check if it blocked and check it is to value and receiver no


    //   if (contactDB_sqlite.getBlockedMineStatus(to, false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
    //       item.setisBlocked(true);
    //  }




    // if (isGroupChat) {
    //    if (enableGroupChat) {
    //          item.setGroupName(user_name.getText().toString());

    // } else {
    //    item.setSenderMsisdn(receiverMsisdn);
    //   item.setSenderName(user_name.getText().toString());

    //     db.updateChatMessage(item, chatType);
    //}

    //}
       /* if (at_memberlist != null) {
            at_memberlist.clear();
        }*/
}


    private void openViewedPersonsList(StatusModel statusModel, boolean showPopup) {


        if (statusModel != null) {
            StatusUtil.startStatusViewActivity(mContext, statusModel, true, true, showPopup);
        }

    }

    @Override
    public int getItemCount() {
        if (mData == null)
            return 0;
        return mData.size();
    }

public class MyViewHolder extends RecyclerView.ViewHolder {
    View rootView;
    ImageView ivStatusImage;
    ImageView ivFoward, ivEye, ivSelectionIcon;
    TextView tvTime, tvStatusCount;
    TextView text_status_caption;

    public MyViewHolder(View itemView) {
        super(itemView);
        rootView = itemView.findViewById(R.id.root_view);
        ivStatusImage = itemView.findViewById(R.id.iv_status_image);
        ivFoward = itemView.findViewById(R.id.iv_my_status_forward);
        ivEye = itemView.findViewById(R.id.iv_my_status_eye);
        tvTime = itemView.findViewById(R.id.tv_my_status_time);
        tvStatusCount = itemView.findViewById(R.id.iv_my_status_count);
        ivSelectionIcon = itemView.findViewById(R.id.iv_selection_icon);
        text_status_caption = (TextView) itemView.findViewById(R.id.text_status_caption);

    }
}
}
