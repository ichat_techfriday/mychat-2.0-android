package com.chat.android.status.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chat.android.R;
import com.chat.android.app.utils.AppUtils;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreActivity;
import com.chat.android.core.CoreController;
import com.chat.android.core.SessionManager;
import com.chat.android.status.controller.StatusData;
import com.chat.android.status.controller.interfaces.OnGalleryItemClickListener;
import com.chat.android.status.controller.interfaces.OnSwipeTouchListener;
import com.chat.android.status.controller.interfaces.SwipeCallBack;
import com.chat.android.status.model.StatusModel;
import com.chat.android.status.view.adapter.GalleryImagesAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import static com.chat.android.app.utils.AppUtils.setCameraDisplayOrientation;


public class StatusAddActivity extends CoreActivity implements SurfaceHolder.Callback, View.OnClickListener, OnGalleryItemClickListener, SwipeCallBack {
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int OPEN_MEDIA_PICKER = 1;
    public static final int MEDIA_PICK_COMPLETED=5;
    public static final int STATUS_ADD=6;
    private  int rotationDegree = 0;
    private Camera camera;
    private boolean needToOn;
    private SurfaceHolder surfaceHolder;
    private ImageView ivSplash, ivCaptureButton,ivCameraSwitch,ivArrow;
    private TextView tvCameraInfoMsg;
    private TextView tvTimerText;
    private boolean previewing = false;
    private static final String TAG = StatusAddActivity.class.getSimpleName();
    private RecyclerView mRvGalleryImages;
    private int currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private boolean isFlashOn = false;
    private SurfaceView surfaceView;
    private boolean isCameraFlashOn = false;
    private MediaRecorder mediaRecorder;
    private boolean recording;
    //  private Handler handler;
    private String videoPath = null;
    private TextView tvStatusItemsCount;
    private ImageView ivSendBtnClick;
    private HashMap<Integer, StatusModel> selectedGalleryItems;
    GalleryImagesAdapter adapter=null;

    boolean hasCameraFlash;
    boolean isEnabled;
    private static final int CAMERA_REQUEST = 50;
    private static final int AUDIO_REQUEST = 51;
    private boolean flashLightStatus = false;
    private boolean isActivityPaused=false;
    CountDownTimer VideoCountDownTimer;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_add);


        getWindow().setFormat(PixelFormat.UNKNOWN);
        surfaceView = findViewById(R.id.camerapreview);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        LayoutInflater controlInflater = LayoutInflater.from(getBaseContext());
        View viewControl = controlInflater.inflate(R.layout.status_add_custom_layout, null);
        viewControl.setOnTouchListener(new OnSwipeTouchListener(this){

            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
                MyLog.d(TAG, "onSwipeTop: ");
                openGallery();
            }
        });

        mRvGalleryImages = viewControl.findViewById(R.id.rv_camera_gallary_images);
        ivSplash = viewControl.findViewById(R.id.iv_splash);
        ivCaptureButton = viewControl.findViewById(R.id.iv_take_picture);
        ivCameraSwitch = viewControl.findViewById(R.id.iv_camera_switch);
        tvTimerText= viewControl.findViewById(R.id.tv_timer_text);
        tvCameraInfoMsg= viewControl.findViewById(R.id.tv_msg);
        mRvGalleryImages.setLayoutManager(AppUtils.getHorizontalLayoutManger(this));
        //mRvGalleryImages.setItemViewCacheSize(20);
        //mRvGalleryImages.setDrawingCacheEnabled(true);
        //mRvGalleryImages.getItemAnimator().setChangeDuration(0);
        ///mRvGalleryImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        //mRvGalleryImages.getRecycledViewPool().setMaxRecycledViews(1, 50);
        ivCaptureButton.setOnTouchListener(onTouchListener);

        ivCameraSwitch.setOnClickListener(this);
        ivSplash.setOnClickListener(this);
        ivArrow = viewControl.findViewById(R.id.show_gallary_view);
        ivArrow.setOnClickListener(this);
        ivSendBtnClick= viewControl.findViewById(R.id.iv_send_btn);
        ivSendBtnClick.setOnClickListener(this);
        tvStatusItemsCount= viewControl.findViewById(R.id.tv_status_items_count);
        LayoutParams layoutParamsControl = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
        this.addContentView(viewControl, layoutParamsControl);


        hasCameraFlash = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        isEnabled = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

    }

    private void setGallaryAdapter(long delay) {
     /*   if (handler == null)
            handler = new Handler();*/
        selectedGalleryItems=StatusData.getInstance().selectedGalleryItems;
        if(selectedGalleryItems!=null) {
            int selectedItemsCount =selectedGalleryItems.size();
            showSendBtn(selectedItemsCount);
        }


        if (adapter==null){
            initAdapter();
        }else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(selectedGalleryItems!=null && selectedGalleryItems.size()>0) {
                        adapter.updateSelectedItems(selectedGalleryItems);
                        adapter.notifyDataSetChanged();
                    }
                    else {
                        initAdapter();
                    }

                }
            });
        }

    }

    private void initAdapter(){
        adapter = new GalleryImagesAdapter(StatusAddActivity.this,
                StatusAddActivity.this ,
                StatusAddActivity.this,false,selectedGalleryItems);
        mRvGalleryImages.setAdapter(adapter);
    }

    private void setTorchMode(boolean on) {
        try {
            if (camera != null) {
                //camera.stopPreview();
                //camera.release();
                setFlashOnOff(on);
                camera.startPreview();
            }
        } catch (Exception e) {
            MyLog.e(TAG, "error ", e);
        }
    }

    public void setFlashOnOff(boolean on) {
        Camera.Parameters params;
        isCameraFlashOn = on;
        if (camera != null) {
            params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            if (!on) {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            }
            camera.setParameters(params);
        }
    }

    private void setAppropriateCameraPictureSize(int surfaceWidth) {
        Camera.Parameters cameraParameters = camera.getParameters();
        boolean sizeSet = false;
        MyLog.d(TAG, "set picture size surface width: " + surfaceWidth);
        for (Camera.Size size : cameraParameters.getSupportedPictureSizes()) {
            int desiredMinimumWidth;
            int desiredMaximumWidth;
            if (surfaceWidth > 1000) {
                desiredMinimumWidth = surfaceWidth - 200;
                desiredMaximumWidth = surfaceWidth + 200;
            } else {
                desiredMinimumWidth = 500;
                desiredMaximumWidth = 1000;
            }

            if (size.width > desiredMinimumWidth && size.width < desiredMaximumWidth && size.height < size.width) {
                cameraParameters.setPictureSize(size.width, size.height);
                MyLog.d("setting size width", size.width + "");
                MyLog.d("setting size height", size.height + "");
                sizeSet = true;
                break;
            }
        }

        if (!sizeSet) {
            Camera.Size size = cameraParameters.getSupportedPictureSizes().get(cameraParameters.getSupportedPictureSizes().size() - 1);
            cameraParameters.setPictureSize(size.width, size.height);

            sizeSet = true;
        }

        //rotate image for back camera
        if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
            cameraParameters.setRotation(rotationDegree);
        else
            cameraParameters.setRotation(270);

        camera.setParameters(cameraParameters);
        MyLog.d(TAG, cameraParameters.getPictureSize().width + "- WIDTH");
        MyLog.d(TAG, cameraParameters.getPictureSize().height + "-HEIGHT");
    }

    private void switchCamera() {

        try{

            if (camera == null)
                return;

            camera.stopPreview();
//NB: if you don't release the current camera before switching, you app will crash
            camera.release();

//swap the id of the camera to be used
            if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
                currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
            } else {
                currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
            }
            try {

                camera = Camera.open(currentCameraId);
                setCameraSizeAndOrientation();
                camera.setPreviewDisplay(surfaceHolder);

            } catch (Exception e) {
                MyLog.e(TAG, "switchCamera: ", e);
            }

            //Check flash is on
          /*  MyLog.e(TAG, "isCameraFlashOn: "+ isCameraFlashOn);

            if (isCameraFlashOn) {  // camera flash on off
                setFlashOnOff(true);
            }*/
           // setFlashOnOff(on);
          //  camera.startPreview();
            camera.startPreview();

        }catch(Exception e){
            MyLog.e(TAG, "CameraError: ", e);

        }

    }



    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (previewing) {
            camera.stopPreview();
            previewing = false;
        }

        if (camera != null) {
            try {
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                previewing = true;
            } catch (Exception e) {
                MyLog.e(TAG, "surfaceChanged: ", e);
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        try{

            camera = Camera.open();
            //setImageSize();
            setCameraSizeAndOrientation();

        }catch(Exception e){

            //System.out.println("Error"+e.getMessage().toString());


        }

    }

    private void setCameraSizeAndOrientation() {
        rotationDegree= setCameraDisplayOrientation(StatusAddActivity.this, currentCameraId, camera);
        setAppropriateCameraPictureSize(surfaceView.getWidth());
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (camera != null) {
            try {
                camera.stopPreview();
                camera.release();
                camera = null;
                previewing = false;
            } catch (Exception e) {
                MyLog.d(TAG, "surfaceDestroyed: " + e.getMessage());
            }
        }
    }

    private void takePhoto() {
        try {

            MyLog.e(TAG, "isCameraFlashOn: "+ isCameraFlashOn);

        //    setTorchMode(needToOn);
            MyLog.e(TAG, "needToOn: "+ needToOn);
            try {
                if (camera != null) {
                    //camera.stopPreview();
                    //camera.release();
                    setFlashOnOff(needToOn);
                    camera.startPreview();
                }
            } catch (Exception e) {
                MyLog.e(TAG, "error ", e);
            }
            camera.takePicture(shutterCallback, null, mPicture);
        } catch (Exception e) {
            MyLog.e(TAG, "takePhoto: ", e);
        }
    }

    Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
            MyLog.d(TAG, "onShutter'd");
        }
    };


    private void resetCam() {
        if(camera!=null)
            camera.startPreview();
    }

    private boolean prepareMediaRecorder() {
        mediaRecorder = new MediaRecorder();
        try {
            if (isCameraFlashOn) {  // camera flash on off
                setFlashOnOff(true);
            }
            camera.unlock();
        } catch (Exception e) {
            //Log.e(TAG, "error ",e );
        }
        mediaRecorder.setCamera(camera);
/*        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);*/
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        mediaRecorder.setProfile(CamcorderProfile.get(currentCameraId, CamcorderProfile.QUALITY_HIGH));
        /*CamcorderProfile cpHigh = CamcorderProfile
                .get(CamcorderProfile.QUALITY_HIGH);*/
        //mediaRecorder.setProfile(cpHigh);
        MyLog.d(TAG, "prepareMediaRecorder: rotation: " + rotationDegree);
        if (rotationDegree != 0)
            mediaRecorder.setOrientationHint(rotationDegree);
        try {
            videoPath = (String) getOutputMediaFile(MEDIA_TYPE_VIDEO);
            MyLog.d(TAG, "prepareMediaRecorder: " + videoPath);
        } catch (Exception e) {
            MyLog.e(TAG, "prepareMediaRecorder: ", e);
        }
        if (videoPath != null) {
            mediaRecorder.setOutputFile(videoPath);

        }
        //mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setPreviewDisplay(surfaceView.getHolder().getSurface());
        //mediaRecorder.setOrientationHint(setOrientationHint);
        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            MyLog.e(TAG, "error ", e);
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            MyLog.e(TAG, "error ", e);
            releaseMediaRecorder();
            return false;
        }
        mediaRecorder.start();
        return true;
    }

    private void releaseMediaRecorder() {
        MyLog.d(TAG, "releaseMediaRecorder: ");
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release();  // release the recorder object
            mediaRecorder = null;
            camera.lock();       // lock camera for later use
        }
        ivCaptureButton.setImageDrawable(new ColorDrawable(ContextCompat.getColor(StatusAddActivity.this, R.color.transparent)));
    }

    private void releaseCamera() {
        if (camera != null) {
            camera.release();    // release the camera for other applications
            camera = null;
        }
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFile = null;
            try {
                pictureFile = (File) getOutputMediaFile(MEDIA_TYPE_IMAGE);
            } catch (Exception e) {
                MyLog.e(TAG, "onPictureTaken: ", e);
            }
            if (pictureFile == null) {
                MyLog.d(TAG, "Error creating media file, check storage permissions: ");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                startCaptionActivity(pictureFile.getPath(),false);
            } catch (FileNotFoundException e) {
                MyLog.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                MyLog.d(TAG, "Error accessing file: " + e.getMessage());
            } catch (Exception e) {
                MyLog.d(TAG, "onPictureTaken: " + e.getMessage());
            }

            resetCam();


/*            AppUtils.refreshGalleryImage(pictureFile.getAbsolutePath(), StatusAddActivity.this);
            setGallaryAdapter(1000);*/
        }
    };


    @Override
    public void onBackPressed() {
        if (recording) {
            stopVideoRecording();
        } else {
            releaseCamera();
            super.onBackPressed();
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityPaused=true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityPaused=false;
        setGallaryAdapter(1000);
        //     adapter.notifyDataSetChanged();
    }

    // stop video recording
    public void stopVideoRecording() {
        try {
            mediaRecorder.stop();   // stop the recording
            releaseMediaRecorder(); // release the MediaRecorder object\
            if (isFlashAvailable(this))
                setFlashOnOff(false);
            releaseCamera();
            recording = false;
            finish();
        } catch (Exception e) {
            MyLog.e(TAG, "error ", e);
        }
    }

    public static boolean isFlashAvailable(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    private long previousTouchMillis = 0L;
    private boolean recordButtonClicked = false;
    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    recordButtonClicked = true;
                    previousTouchMillis = System.currentTimeMillis();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ActivityCompat.checkSelfPermission(StatusAddActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(StatusAddActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, AUDIO_REQUEST);
                        } else {
                            checkAndRecordVideo();
                        }
                    } else {
                        checkAndRecordVideo();
                    }
                    return true;
                case MotionEvent.ACTION_UP:
                    long currentMillis = System.currentTimeMillis();
                    long difference = currentMillis - previousTouchMillis;
                    //single click --> photo
                    if (difference < 1000) {
                        recordButtonClicked = false;
                        takePhoto();
                    }
                    //long press --> video
                    else {
                        //after video record completed
                        recordButtonClicked = false;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ActivityCompat.checkSelfPermission(StatusAddActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                                releaseMediaRecorder();
                                startCaptionActivity(videoPath,true);
                            }
                        } else {
                            releaseMediaRecorder();
                            startCaptionActivity(videoPath,true);
                        }

                    }
                    return true;
            }

            MyLog.d(TAG, "onTouch: others ");

            return true;
        }
    };

    private void startCaptionActivity(String path, boolean isVideo) {
        StatusModel statusModel = new StatusModel();
        String currentUserId=SessionManager.getInstance(this).getCurrentUserID();
        statusModel.setUserId(currentUserId);
        statusModel.setLocalPath(path);
        if(isVideo) {
            if(time>=2) {
                statusModel.setVideo(true);
                HashMap<Integer, StatusModel> selectedItems = new HashMap<>();
                selectedItems.put(0, statusModel);
                StatusData.getInstance().selectedGalleryItems = selectedItems;

                Intent intent = new Intent(StatusAddActivity.this, StatusCaptionActivity.class);
                startActivityForResult(intent, STATUS_ADD);

                VideoCountDownTimer.cancel();
                tvTimerText.setVisibility(View.GONE);
                ivSplash.setVisibility(View.GONE);
                ivCameraSwitch.setVisibility(View.VISIBLE);
                mRvGalleryImages.setVisibility(View.VISIBLE);
                ivArrow.setVisibility(View.VISIBLE);
                tvCameraInfoMsg.setVisibility(View.VISIBLE);
            }
            else{

                VideoCountDownTimer.cancel();
                tvTimerText.setVisibility(View.GONE);
                ivSplash.setVisibility(View.GONE);
                ivCameraSwitch.setVisibility(View.VISIBLE);
                mRvGalleryImages.setVisibility(View.VISIBLE);
                ivArrow.setVisibility(View.VISIBLE);
                tvCameraInfoMsg.setVisibility(View.VISIBLE);
                Toast.makeText(this,"Please record minimum 2 seconds video!",Toast.LENGTH_SHORT).show();
            }
        } else {
            statusModel.setImage(true);
            HashMap<Integer, StatusModel> selectedItems = new HashMap<>();
            selectedItems.put(0, statusModel);
            StatusData.getInstance().selectedGalleryItems = selectedItems;
            Intent intent = new Intent(StatusAddActivity.this, StatusCaptionActivity.class);
            startActivityForResult(intent, STATUS_ADD);
            finish();
        }
    }



    private void checkAndRecordVideo() {
        Handler handler = null;
        if (handler == null)
            handler = new Handler();
        ivCaptureButton.setImageDrawable(new ColorDrawable(ContextCompat.getColor(StatusAddActivity.this, R.color.red)));
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (recordButtonClicked) {
                    boolean prepared= prepareMediaRecorder();
                    if(prepared){
                        videoRecordingStarted();

                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Sorry! Try again later!",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, 1100);
    }
    int time;
    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }
    private void videoRecordingStarted() {
        ivSplash.setVisibility(View.GONE);
        ivCameraSwitch.setVisibility(View.GONE);
        mRvGalleryImages.setVisibility(View.GONE);
        ivArrow.setVisibility(View.GONE);
        tvCameraInfoMsg.setVisibility(View.GONE);
        tvTimerText.setVisibility(View.VISIBLE);

        VideoCountDownTimer= new CountDownTimer(300000, 1000) {

            public void onTick(long millisUntilFinished) {
                tvTimerText.setText("00:"+ checkDigit(time));
                if(!isActivityPaused)
                    time++;
            }

            public void onFinish() {
                tvTimerText.setVisibility(View.GONE);
            }

        }.start();







        Animation animation= AnimationUtils.loadAnimation(this,R.anim.zoom_anim);
        animation.setFillAfter(true);
        ivCaptureButton.setAnimation(animation);

    }

    /**
     * Create a File for saving an image or video
     */
    private static Object getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        String appname=CoreController.getAppName();
        appname=appname.trim();
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), appname+"_Status");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                MyLog.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            //return video path
            return mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4";
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onStop() {
        super.onStop();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.iv_camera_switch:
                switchCamera();
                break;


            case R.id.show_gallary_view:
                openGallery();
                break;

            case R.id.iv_send_btn:
                sendBtnClick();
                break;

            case R.id.iv_splash:

                if (isFlashOn) {
                    needToOn = false;
                    isFlashOn = false;
                    ivSplash.setImageResource(R.drawable.camera_flash_off);
                } else {
                    needToOn = true;
                    isFlashOn = true;
                    ivSplash.setImageResource(R.drawable.camera_flash_on);
                }
              //  setTorchMode(needToOn);
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check which request we're responding to
       /* if (requestCode == OPEN_MEDIA_PICKER) {
            // Make sure the request was successful
            if (resultCode == FINISH_ACTIVITY  ) {
               finish();
            }
        }*/
        //status adding completed from Status caption page
        if(resultCode==STATUS_ADD || resultCode== OPEN_MEDIA_PICKER){
            MyLog.d(TAG, "onActivityResult: from status add complete");
            onBackPressed();
        }
        else if(resultCode==MEDIA_PICK_COMPLETED){
            onBackPressed();
        }

    }

    private void openGallery() {
        startImageVideoPickerPage();
    }

    private void startImageVideoPickerPage(){
        StatusData.getInstance().selectedGalleryItems = selectedGalleryItems;
        Intent intent= new Intent(this, StatusGalleryPickerActivity.class);
        intent.putExtra("title","Select media");
        intent.putExtra("mode",1);
        intent.putExtra("maxSelection",3);
        startActivityForResult(intent,OPEN_MEDIA_PICKER);
    }

    @Override
    public void onItemClick(int totalCount , HashMap<Integer, StatusModel> selectedItems) {
        selectedGalleryItems=selectedItems;
        showSendBtn(totalCount);
    }

    private void showSendBtn(int totalCount) {
        if(totalCount>0){
            tvStatusItemsCount.setText(String.valueOf(totalCount));
            ivSendBtnClick.setVisibility(View.VISIBLE);
            tvStatusItemsCount.setVisibility(View.VISIBLE);
        }
        else {
            ivSendBtnClick.setVisibility(View.GONE);
            tvStatusItemsCount.setVisibility(View.GONE);
        }
    }

    private void sendBtnClick(){
        if(selectedGalleryItems!=null && selectedGalleryItems.size()>0) {
            StatusData.getInstance().selectedGalleryItems = selectedGalleryItems;
            Intent intent = new Intent(StatusAddActivity.this, StatusCaptionActivity.class);
            startActivityForResult(intent,STATUS_ADD);
        }
    }

    @Override
    public void onSwipeTop() {
        openGallery();
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case AUDIO_REQUEST :
                if (grantResults.length > 0  &&  grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                break;
        }
    }


}
