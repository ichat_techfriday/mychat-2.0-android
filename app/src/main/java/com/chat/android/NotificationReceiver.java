package com.chat.android;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.chat.android.app.activity.ChatPageActivity;
import com.chat.android.app.activity.NewHomeScreenActivty;
import com.chat.android.app.utils.Getcontactname;
import com.chat.android.app.utils.GroupInfoSession;
import com.chat.android.app.utils.MyLog;
import com.chat.android.core.CoreController;
import com.chat.android.core.ShortcutBadgeManager;
import com.chat.android.core.database.ContactDB_Sqlite;
import com.chat.android.core.database.MessageDbController;
import com.chat.android.core.message.MessageFactory;
import com.chat.android.core.message.TextMessage;
import com.chat.android.core.model.GroupInfoPojo;
import com.chat.android.core.model.MessageItemChat;
import com.chat.android.core.model.SendMessageEvent;
import com.chat.android.core.socket.MessageService;
import com.chat.android.core.socket.NotificationUtil;
import com.chat.android.core.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.core.app.RemoteInput;
import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by Belal on 2/27/2018.
 */
public class NotificationReceiver extends BroadcastReceiver {
    private static final String TAG = "NotificationReceiver";
    public Getcontactname getcontactname;
    public MessageDbController db;
    private boolean enableGroupChat;
    private GroupInfoSession groupInfoSession;
    public boolean hasGroupInfo;

    @Override
    public void onReceive(final Context context, Intent intent) {
        //getting the remote input bundle from intent
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);

        //if there is some input
        if (remoteInput != null) {
            //getting the input value
            final CharSequence name = remoteInput.getCharSequence(MessageService.NOTIFICATION_REPLY);
            Toast.makeText(context, "You NOTIFICATION_REPLY " + name, Toast.LENGTH_LONG).show();

            final String mData = intent.getStringExtra("data");
            try {
                final JSONObject jsonObject = new JSONObject(mData);
                Log.e(TAG, "mData" + mData);
                Log.e(TAG, "jsonObject" + jsonObject);
                groupInfoSession = new GroupInfoSession(context);

     /*           if (intent.getIntExtra(MessageService.KEY_INTENT_MORE, -1) == MessageService.REQUEST_CODE_MORE) {

                         new Handler().post(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(context, "You Replyed", Toast.LENGTH_LONG).show();

                                // Code here will run in UI thread
                                NotificationUtil.clearNotificationData();
                                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.cancelAll();
                                cancelNotification(context,1);
                                //Send Text message
                                sendTextMessage(String.valueOf(name), mData, jsonObject, context);

                            }
                        });


                } else {
                    //Navigate to chat page

                }*/
                getcontactname = new Getcontactname(context);

                sendTextMessage(String.valueOf(name), mData, jsonObject, context);
                clearnotify(context);


         //       updating the notification with the input value
                NotificationManager notificationManager = (NotificationManager) context.
                        getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(1, NotificationUtil.builder.build());
                notificationManager.cancel(1);
                String to = jsonObject.optString("to");
                String from = jsonObject.optString("from");
                String receiverMsisdn = jsonObject.optString("ContactMsisdn");
                String mReceiverName = getcontactname.getSendername(receiverMsisdn);

                //if more button is clicked
                if (intent.getIntExtra(MessageService.KEY_INTENT_MORE, -1) == MessageService.REQUEST_CODE_MORE) {
               //     Toast.makeText(context, "You Clicked More", Toast.LENGTH_LONG).show();

                    intent = new Intent(context, ChatPageActivity.class);

                    navigateToChatFromService(intent, to, mReceiverName);
                }else {
                    navigateToChatFromService(intent, to, mReceiverName);

                }
                 //NotificationUtil.CustomshowNotificationWithReply(context, jsonObject, jsonObject.getString("from"), "0", true,false,false);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //  NotificationUtil.setSound(context, true, true, 0, NotificationUtil.builder);

          /*  NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "1");
                 //   .setSmallIcon(android.R.drawable.ic_menu_info_details)
              //      .setContentTitle("Hey Thanks, " + name);
            NotificationManager notificationManager = (NotificationManager) context.
                    getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, mBuilder.build());
*/
            //updating the notification with the input value
        }else{
            clearnotify(context);
            Intent intentt = new Intent(context, NewHomeScreenActivty.class);
            context.startActivity(intentt);
        }


    }

    private static void navigateToChatFromService(Intent intent, String receiverDocumentID, String username) {

        intent.putExtra("receiverUid", "");
        intent.putExtra("receiverName", "");
        intent.putExtra("documentId", receiverDocumentID);
        intent.putExtra("Image", "");
        intent.putExtra("type", 0);
        intent.putExtra("backfrom", true);
        intent.putExtra("Username", username);
    }

    public  void clearnotify(Context mcontext) {
        //   if (!isRunning(mcontext)) {
        NotificationUtil.clearNotificationData();
        NotificationManager notificationManager = (NotificationManager) mcontext.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancelAll();
        ShortcutBadgeManager shortcutBadgeManager = new ShortcutBadgeManager(mcontext);
        shortcutBadgeManager.clearBadgeCount();
        try {
            ShortcutBadger.applyCount(mcontext, 0);
        } catch (Exception e) {
            MyLog.e(TAG, "", e);
        }
        //   }
    }


    //-------------------------------------------------SEND TEXT MESSAGE --------------------------------------------------------------
    private void sendTextMessage(String mReply, String data, JSONObject jsonObject, Context mContext) {
        //Check it iz sinfle or group

        boolean isGroupChat = false;
        if (!data.equalsIgnoreCase("")) {
          /*  if (session.getarchivecount() != 0) {
                if (session.getarchive(from + "-" + to))
                    session.removearchive(from + "-" + to);
            }
            if (session.getarchivecountgroup() != 0) {
                if (session.getarchivegroup(from + "-" + to + "-g"))
                    session.removearchivegroup(from + "-" + to + "-g");
            }*/

            SendMessageEvent messageEvent = new SendMessageEvent();
            TextMessage message = (TextMessage) MessageFactory.getMessage(MessageFactory.text, mContext);
            JSONObject msgObj;

            String docId;
            String chatType = MessageFactory.CHAT_TYPE_SINGLE;

            docId = jsonObject.optString("doc_id");
            hasGroupInfo = groupInfoSession.hasGroupInfo(docId);

            db = CoreController.getDBInstance(mContext);
            if (db.isGroupId(docId)) {

                isGroupChat = true;

            }
            Log.e(TAG, "isGroupChat" + isGroupChat);
            if (isGroupChat) {
                //docId = mCurrentUserId.concat("-").concat(to).concat("-g");
                chatType = MessageFactory.CHAT_TYPE_GROUP;
            } else {
                //  docId = mCurrentUserId.concat("-").concat(to);
                chatType = MessageFactory.CHAT_TYPE_SINGLE;
            }
            String to = jsonObject.optString("to");
            String from = jsonObject.optString("from");
            ContactDB_Sqlite contactDB_sqlite = CoreController.getContactSqliteDBintstance(mContext);
            String receiverMsisdn = jsonObject.optString("ContactMsisdn");

            String mReceiverName = getcontactname.getSendername(receiverMsisdn);

            // String mReceiverName = contactDB_sqlite.getNameByUserId(to);
            Log.e("contactDB_sqlite", "contactDB_sqlite" + mReceiverName);


            if (isGroupChat) {
                messageEvent.setEventName(SocketManager.EVENT_GROUP);
                //   msgObj = (JSONObject) message.getGroupMessageObject(to, data_at, user_name.getText().toString());
                msgObj = (JSONObject) message.getGroupMessageObject(from, mReply, mReceiverName);

                try {
                    msgObj.put("groupType", SocketManager.ACTION_EVENT_GROUP_MESSAGE);
                    msgObj.put("userName", mReceiverName);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            } else {

                msgObj = (JSONObject) message.getMessageObject(from, mReply, false);
                messageEvent.setEventName(SocketManager.EVENT_MESSAGE);
                // }
                ///data_at
                MessageItemChat item = message.createMessageItem(true, mReply, MessageFactory.DELIVERY_STATUS_NOT_SENT, from, mReceiverName);

                //Check if it blocked and check it is to value and receiver no


                if (contactDB_sqlite.getBlockedMineStatus(from, false).equals(ContactDB_Sqlite.BLOCKED_STATUS)) {
                    item.setisBlocked(true);
                }
                messageEvent.setMessageObject(msgObj);


         /*   if (isTagapplied == 1) {
                item.setTagapplied(true);
                item.setSetArrayTagnames(names);
                item.setEditTextmsg(data);
                item.setUserId_tag(userID);
            }*/

                /**/

               /* if (hasGroupInfo) {
                    GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);
                    if (infoPojo != null && infoPojo.getGroupMembers() != null) {
                        String[] membersId = infoPojo.getGroupMembers().split(",");


                    } else {
                        Toast.makeText(mContext, "You are not member of this group!", Toast.LENGTH_SHORT).show();

                    }
                }*/
                GroupInfoPojo infoPojo = groupInfoSession.getGroupInfo(docId);// groupInfoSession.getGroupInfo(from + "-" + to + "-g");
                if (infoPojo != null) {
                    enableGroupChat = infoPojo.isLiveGroup();
                }

                if (isGroupChat) {
                    if (enableGroupChat) {
                        item.setGroupName(mReceiverName);
                        db.updateChatMessage(item, chatType);
                        EventBus.getDefault().post(messageEvent);
                    } else {
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.you_are_not_member_of_this_group), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    item.setSenderMsisdn(receiverMsisdn);
                    item.setSenderName(mReceiverName);
                    Log.e(TAG, "MessageItemChat" + item.toString());
                    //      if (!isAlreadyExist(item))
                    //         mChatData.add(item);
                    EventBus.getDefault().post(messageEvent);
                    MessageDbController db = CoreController.getDBInstance(mContext);

                    db.updateChatMessage(item, chatType);
                    //  notifyDatasetChange();

                    //   }

                }

            }

        }
    }

    public void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }
}
