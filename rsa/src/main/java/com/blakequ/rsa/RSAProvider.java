package com.blakequ.rsa;

import android.util.Base64;
import android.util.Log;

import org.spongycastle.asn1.ASN1Integer;
import org.spongycastle.asn1.ASN1Sequence;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

/**
 * Copyright (C) BlakeQu All Rights Reserved <blakequ@gmail.com>
 * <p>
 * Licensed under the blakequ.com License, Version 1.0 (the "License");
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p>
 * author  : quhao <blakequ@gmail.com> <br>
 * date     : 2017/2/22 16:03 <br>
 * last modify author : <br>
 * version : 1.0 <br>
 * description:
 */

public class RSAProvider {

    /**
     * KEY_ALGORITHM
     */
    public static final String KEY_ALGORITHM = "RSA";
    /**
     * 加密Key的长度等于1024
     */
    public static int KEYSIZE = 512;
    /**
     * 解密时必须按照此分组解密
     */
    public static int decodeLen = KEYSIZE / 8;
    /**
     * 加密时小于117即可
     */
    public static int encodeLen = 110;//(DEFAULT_KEY_SIZE / 8) - 11;
    /**
     * 公钥
     */
//    private static final String PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\n" +
//            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCDbSmvjSB5Bq2ofovX4re/OUHR\n" +
//            "vnWu0aXtV74gARyuHZAm2DzaDx15PBra3ijmAsH2VYjeoNd5jWH6E/FJo5V4DIH1\n" +
//            "FS/roJozeXiiVGPfm8qzCxYUGZQsgBge5WT3GfeViqunf/autEkwKerzhD6KflWf\n" +
//            "DCwzA+7fAr0Bt2XthwIDAQAB\n" +
//            "-----END RSA PUBLIC KEY-----";
//    /**
//     * 私钥
//     */
//    private static final String PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----\n" +
//            "MIICWwIBAAKBgQCDbSmvjSB5Bq2ofovX4re/OUHRvnWu0aXtV74gARyuHZAm2Dza\n" +
//            "Dx15PBra3ijmAsH2VYjeoNd5jWH6E/FJo5V4DIH1FS/roJozeXiiVGPfm8qzCxYU\n" +
//            "GZQsgBge5WT3GfeViqunf/autEkwKerzhD6KflWfDCwzA+7fAr0Bt2XthwIDAQAB\n" +
//            "AoGAEsEZGbwBzkNttIWaXwwzPE5nXg7XrEKuctf6gYanGRGO6Hwi8tsFUa+KBYF+\n" +
//            "RJRQKV67UUmSdf3+5TjDTcbSJUJOB8j91HMm3ft3Rhhu+M3NqS32XGWv1iTJaFbj\n" +
//            "yF4jNcXE1QZz7QHGEEk+Zc3QGO35/HSWTEieRzr8N0WeGYECQQDY1g/cA/jLCjb4\n" +
//            "MoKKMO1iZd7eFKqTwRza935fmS7fmMEnNy8hHSo+qbhGBWWqM1pq0YgWY2mEiP57\n" +
//            "/xeT1umpAkEAmyn6DeB3hAHUlbcUoRvrFfWcA9kNp6bwa/uyQLN+mDguJdjpD4yO\n" +
//            "mJPOMQR1NaOjwYifNheZkYEJOVgFrnh7rwJAMtfmuhk4Uiuf/gyh7VNRpjvqyToh\n" +
//            "USn74SAdQr9BbW59A/v9kg8ro6vaKJkFYKZP8jIVbeJ7kHcq7NpApgrDmQJAMWd9\n" +
//            "Mdvt0f9PM9hWXjHoeVZ5tZPv0BjMZmV+zTEZttf0gaJ+GrBTWut89IiJ1WlkI6Qf\n" +
//            "pXaANB8U08heDr5YCwJATVTg3pSqHd9kJOgLcIgZJ/YzNsghJJ6brOkTKVQ2i3Kw\n" +
//            "QFqlkyZjTd+iHL8H1m0jcBvOE7R9k8pSUQk2hg5jWw==\n" +
//            "-----END RSA PRIVATE KEY-----";

    private static final String PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\\nMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKSfCMocXqu5FZ3vQdwnobCLB+ueF0sY\\nDElsKLHfMN5pL1vs/CdRfXHn7UYtiy5IwVO3uVzrlTSKr7pVUa14KGECAwEAAQ==\\n-----END PUBLIC KEY-----\\n";
    /**
     * 私钥
     */
    private static final String PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----\\nMIIBOgIBAAJBAKSfCMocXqu5FZ3vQdwnobCLB+ueF0sYDElsKLHfMN5pL1vs/CdR\\nfXHn7UYtiy5IwVO3uVzrlTSKr7pVUa14KGECAwEAAQJAR9hIg7yh7JwYPRdATQ7W\\n2C+1QzV5wQI7yodzsz1PHKFkIz8Inin+U0mDxn8kw6VlaI/zqg0iIgwj8Pddve0u\\niQIhAM40TWRWhmHnCV2ZNrmYpYqeQHDN9xm1q90qSJDdbq9TAiEAzGAJUQ+MXFmW\\n+/Caa8+nG02TPhYd/ilEH52tthQWdvsCIQDKH5p88BNpg/46MSTjaC1cYyLnb/z2\\nxo1gjUH0Phlw4QIgafWYnvhKs06XowN5bE/6uYMcxeG9pxpKAYJEn9QGWaUCIAg5\\n8Jt0VA6/Ppvl92AEtDcYzAXXWZOSjUDzVHaNJCsC\\n-----END RSA PRIVATE KEY-----\\n";

    /**
     * MODULES
     */
    private static final String MODULES = "RSAModules";
    /**
     * 签名算法
     */
    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";
    /**
     * 加密填充方式,android系统的RSA实现是"RSA/None/NoPadding"，而标准JDK实现是"RSA/None/PKCS1Padding" ，这造成了在android机上加密后无法在服务器上解密的原因
     */
    //public static final String ECB_PKCS1_PADDING = "RSA/ECB/PKCS1Padding";
    public static final String ECB_PKCS1_PADDING = "RSA/ECB/PKCS1Padding";

    /**
     * 生成KeyPair
     *
     * @return
     * @throws Exception
     */
    public static Map<String, Object> generateKeyPair() throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        keyPairGen.initialize(KEYSIZE);
        KeyPair keyPair = keyPairGen.generateKeyPair();

        // 公钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();

        // 私钥
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        BigInteger modules = privateKey.getModulus();

        Map<String, Object> keys = new HashMap<String, Object>(3);
        keys.put(PUBLIC_KEY, publicKey);
        keys.put(PRIVATE_KEY, privateKey);
        keys.put(MODULES, modules);
        return keys;
    }

    public static byte[] getModulesBytes(Map<String, Object> keys) {
        BigInteger big = (BigInteger) keys.get(MODULES);
        return big.toByteArray();
    }

    /**
     * 取得私钥
     *
     * @return
     * @throws Exception
     */
    public static String getPrivateKeyBytes(Map<String, Object> keys) throws Exception {
        Key key = (Key) keys.get(PRIVATE_KEY);
        return Base64Utils.encode(key.getEncoded());
    }

    /**
     * 取得公钥
     *
     * @return
     * @throws Exception
     */
    public static String getPublicKeyBytes(Map<String, Object> keys) throws Exception {
        Key key = (Key) keys.get(PUBLIC_KEY);
        return Base64Utils.encode(key.getEncoded());
    }

    /**
     * 用私钥对信息生成数字签名
     *
     * @param data       已加密数据
     * @param privateKey 私钥(BASE64编码)
     * @return
     * @throws Exception
     */
    public static String sign(byte[] data, String privateKey) throws Exception {
        PrivateKey privateK = loadPrivateKey(privateKey);
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initSign(privateK);
        signature.update(data);
        return Base64Utils.encode(signature.sign());
    }

    /**
     * 校验数字签名
     *
     * @param data      已加密数据
     * @param publicKey 公钥(BASE64编码)
     * @param sign      数字签名
     * @return
     * @throws Exception
     */
    public static boolean verify(byte[] data, String publicKey, String sign)
            throws Exception {
        PublicKey publicK = loadPublicKey(publicKey);
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initVerify(publicK);
        signature.update(data);
        return signature.verify(Base64Utils.decode(sign));
    }

    /**
     * 通过私钥加密
     *
     * @param encryptedData
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] encryptPrivateKey(byte[] encryptedData, String key) throws Exception {
        if (encryptedData == null) {
            throw new IllegalArgumentException("Input encryption data is null");
        }
        byte[] encode = new byte[]{};
        for (int i = 0; i < encryptedData.length; i += encodeLen) {
            byte[] subarray = ArrayUtils.subarray(encryptedData, i, i + encodeLen);
            byte[] doFinal = encryptByPrivateKey(subarray, key);
            encode = ArrayUtils.addAll(encode, doFinal);
        }
        return encode;
    }

    /**
     * 通过公钥解密
     *
     * @param encode
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] decryptPublicKey(byte[] encode, String key) throws Exception {
        if (encode == null) {
            throw new IllegalArgumentException("Input encryption data is null");
        }
        byte[] buffers = new byte[]{};
        for (int i = 0; i < encode.length; i += decodeLen) {
            byte[] subarray = ArrayUtils.subarray(encode, i, i + decodeLen);
            byte[] doFinal = decryptByPublicKey(subarray, key);
            buffers = ArrayUtils.addAll(buffers, doFinal);
        }
        return buffers;
    }

    /**
     * 通过公钥加密
     *
     * @param encryptedData
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] encryptPublicKey(byte[] encryptedData, String key) throws Exception {
        if (encryptedData == null) {
            throw new IllegalArgumentException("Input encryption data is null");
        }
        byte[] encode = new byte[]{};
        for (int i = 0; i < encryptedData.length; i += encodeLen) {
            byte[] subarray = ArrayUtils.subarray(encryptedData, i, i + encodeLen);
            byte[] doFinal = encryptByPublicKey(subarray, key);
            encode = ArrayUtils.addAll(encode, doFinal);
        }
        return encode;
    }

    /**
     * 通过私钥解密
     *
     * @param encode
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] decryptPrivateKey(byte[] encode, String key) throws Exception {
        if (encode == null) {
            throw new IllegalArgumentException("Input data is null");
        }
        byte[] buffers = new byte[]{};
        for (int i = 0; i < encode.length; i += decodeLen) {
            byte[] subarray = ArrayUtils.subarray(encode, i, i + decodeLen);
            byte[] doFinal = ownDecryptByPrivateKey(subarray, key);
            buffers = ArrayUtils.addAll(buffers, doFinal);
        }
        return buffers;
    }

    /**
     * 从字符串中加载公钥
     *
     * @param publicKeyStr 公钥数据字符串
     * @throws Exception 加载公钥时产生的异常
     */
    public static PublicKey loadPublicKey(String publicKeyStr) throws Exception {
        try {
            byte[] buffer = Base64Utils.decode(publicKeyStr);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            return keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("No such algorithm");
        } catch (InvalidKeySpecException e) {
            throw new Exception("Illegal private key");
        } catch (NullPointerException e) {
            throw new Exception("key data is empty");
        }
    }

    /**
     * 从字符串中加载私钥<br>
     * 加载时使用的是PKCS8EncodedKeySpec（PKCS#8编码的Key指令）。
     *
     * @param privateKeyStr
     * @return
     * @throws Exception
     */
    public static PrivateKey loadPrivateKey(String privateKeyStr) throws Exception {
        try {
            byte[] buffer = Base64Utils.decode(privateKeyStr);
            //表示按照 ASN.1 类型 PrivateKeyInfo 进行编码的专用密钥的 ASN.1 编码。
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            return keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("No such algorithm");
        } catch (InvalidKeySpecException e) {
            throw new Exception("Illegal private key");
        } catch (NullPointerException e) {
            throw new Exception("key data is empty");
        }
    }


    /**
     * 从文件中输入流中加载公钥
     *
     * @param in 公钥输入流
     * @throws Exception 加载公钥时产生的异常
     */
    public static PublicKey loadPublicKey(InputStream in) throws Exception {
        try {
            return loadPublicKey(FileUtils.readString(in));
        } catch (IOException e) {
            throw new Exception("公钥数据流读取错误");
        } catch (NullPointerException e) {
            throw new Exception("公钥输入流为空");
        }
    }

    /**
     * 读取私钥
     *
     * @param in
     * @return
     * @throws Exception
     */
    public static PrivateKey loadPrivateKey(InputStream in) throws Exception {
        try {
            return loadPrivateKey(FileUtils.readString(in));
        } catch (IOException e) {
            throw new Exception("私钥数据读取错误");
        } catch (NullPointerException e) {
            throw new Exception("私钥输入流为空");
        }
    }

    /**
     * 用私钥解密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    private static byte[] decryptByPrivateKey(byte[] data, String key) throws Exception {
        if (data == null) {
            throw new IllegalArgumentException("Input data is null");
        }
        //  Log.e("Converted Key", key);

        //  Log.e("Key1", "MIIBOgIBAAJBAKSfCMocXqu5FZ3vQdwnobCLB+ueF0sYDElsKLHfMN5pL1vs/CdR\\nfXHn7UYtiy5IwVO3uVzrlTSKr7pVUa14KGECAwEAAQJAR9hIg7yh7JwYPRdATQ7W\\n2C+1QzV5wQI7yodzsz1PHKFkIz8Inin+U0mDxn8kw6VlaI/zqg0iIgwj8Pddve0u\\niQIhAM40TWRWhmHnCV2ZNrmYpYqeQHDN9xm1q90qSJDdbq9TAiEAzGAJUQ+MXFmW\\n+/Caa8+nG02TPhYd/ilEH52tthQWdvsCIQDKH5p88BNpg/46MSTjaC1cYyLnb/z2\\nxo1gjUH0Phlw4QIgafWYnvhKs06XowN5bE/6uYMcxeG9pxpKAYJEn9QGWaUCIAg5\\n8Jt0VA6/Ppvl92AEtDcYzAXXWZOSjUDzVHaNJCsC");
        //取得私钥
        //     Key privateKey = loadPrivateKey("MIIBOgIBAAJBAKSfCMocXqu5FZ3vQdwnobCLB+ueF0sYDElsKLHfMN5pL1vs/CdR\\nfXHn7UYtiy5IwVO3uVzrlTSKr7pVUa14KGECAwEAAQJAR9hIg7yh7JwYPRdATQ7W\\n2C+1QzV5wQI7yodzsz1PHKFkIz8Inin+U0mDxn8kw6VlaI/zqg0iIgwj8Pddve0u\\niQIhAM40TWRWhmHnCV2ZNrmYpYqeQHDN9xm1q90qSJDdbq9TAiEAzGAJUQ+MXFmW\\n+/Caa8+nG02TPhYd/ilEH52tthQWdvsCIQDKH5p88BNpg/46MSTjaC1cYyLnb/z2\\nxo1gjUH0Phlw4QIgafWYnvhKs06XowN5bE/6uYMcxeG9pxpKAYJEn9QGWaUCIAg5\\n8Jt0VA6/Ppvl92AEtDcYzAXXWZOSjUDzVHaNJCsC");

//        Key privateKey = loadPrivateKey(key);
//        // 对数据解密
//        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
//        cipher.init(Cipher.DECRYPT_MODE, privateKey);

        String privKeyPEM = "MIIBOgIBAAJBAKSfCMocXqu5FZ3vQdwnobCLB+ueF0sYDElsKLHfMN5pL1vs/CdRfXHn7UYtiy5IwVO3uVzrlTSKr7pVUa14KGECAwEAAQJAR9hIg7yh7JwYPRdATQ7W2C+1QzV5wQI7yodzsz1PHKFkIz8Inin+U0mDxn8kw6VlaI/zqg0iIgwj8Pddve0uiQIhAM40TWRWhmHnCV2ZNrmYpYqeQHDN9xm1q90qSJDdbq9TAiEAzGAJUQ+MXFmW+/Caa8+nG02TPhYd/ilEH52tthQWdvsCIQDKH5p88BNpg/46MSTjaC1cYyLnb/z2xo1gjUH0Phlw4QIgafWYnvhKs06XowN5bE/6uYMcxeG9pxpKAYJEn9QGWaUCIAg58Jt0VA6/Ppvl92AEtDcYzAXXWZOSjUDzVHaNJCsC";
        byte[] encodedPrivateKey = Base64.decode("", Base64.DEFAULT);

        ASN1Sequence primitive = (ASN1Sequence) ASN1Sequence
                .fromByteArray(encodedPrivateKey);
        Enumeration<?> e = primitive.getObjects();
        BigInteger v = ((ASN1Integer) e.nextElement()).getValue();

        int version = v.intValue();
        if (version != 0 && version != 1) {
            throw new IllegalArgumentException("wrong version for RSA private key");
        }
        /**
         * In fact only modulus and private exponent are in use.
         */
        BigInteger modulus = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger publicExponent = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger privateExponent = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger prime1 = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger prime2 = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger exponent1 = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger exponent2 = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger coefficient = ((ASN1Integer) e.nextElement()).getValue();

        RSAPrivateKeySpec spec = new RSAPrivateKeySpec(modulus, privateExponent);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey pk = kf.generatePrivate(spec);

        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        cipher.init(Cipher.DECRYPT_MODE, pk);
        return cipher.doFinal(data);
    }

    private static byte[] ownDecryptByPrivateKey(byte[] data, String privKeyPEM) throws Exception {
        if (data == null) {
            throw new IllegalArgumentException("Input data is null");
        }
        //String privKeyPEM = "MIIBOgIBAAJBAKSfCMocXqu5FZ3vQdwnobCLB+ueF0sYDElsKLHfMN5pL1vs/CdRfXHn7UYtiy5IwVO3uVzrlTSKr7pVUa14KGECAwEAAQJAR9hIg7yh7JwYPRdATQ7W2C+1QzV5wQI7yodzsz1PHKFkIz8Inin+U0mDxn8kw6VlaI/zqg0iIgwj8Pddve0uiQIhAM40TWRWhmHnCV2ZNrmYpYqeQHDN9xm1q90qSJDdbq9TAiEAzGAJUQ+MXFmW+/Caa8+nG02TPhYd/ilEH52tthQWdvsCIQDKH5p88BNpg/46MSTjaC1cYyLnb/z2xo1gjUH0Phlw4QIgafWYnvhKs06XowN5bE/6uYMcxeG9pxpKAYJEn9QGWaUCIAg58Jt0VA6/Ppvl92AEtDcYzAXXWZOSjUDzVHaNJCsC";
        byte[] encodedPrivateKey = Base64.decode(privKeyPEM, Base64.DEFAULT);
        ASN1Sequence primitive = (ASN1Sequence) ASN1Sequence
                .fromByteArray(encodedPrivateKey);
        Enumeration<?> e = primitive.getObjects();
        BigInteger v = ((ASN1Integer) e.nextElement()).getValue();

        int version = v.intValue();
        if (version != 0 && version != 1) {
            throw new IllegalArgumentException("wrong version for RSA private key");
        }
        /**
         * In fact only modulus and private exponent are in use.
         */
        BigInteger modulus = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger publicExponent = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger privateExponent = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger prime1 = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger prime2 = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger exponent1 = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger exponent2 = ((ASN1Integer) e.nextElement()).getValue();
        BigInteger coefficient = ((ASN1Integer) e.nextElement()).getValue();

        RSAPrivateKeySpec spec = new RSAPrivateKeySpec(modulus, privateExponent);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey pk = kf.generatePrivate(spec);

        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        cipher.init(Cipher.DECRYPT_MODE, pk);
        return cipher.doFinal(data);
    }

    /**
     * 用公钥解密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    private static byte[] decryptByPublicKey(byte[] data, String key) throws Exception {
        if (data == null) {
            throw new IllegalArgumentException("Input data is null");
        }
        //取得公钥
        Key publicKey = loadPublicKey(key);
        // 对数据解密
        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        cipher.init(Cipher.DECRYPT_MODE, publicKey);

        return cipher.doFinal(data);
    }

    /**
     * 用公钥加密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    private static byte[] encryptByPublicKey(byte[] data, String key) throws Exception {
        if (data == null) {
            throw new IllegalArgumentException("Input data is null");
        }
        Key publicKey = loadPublicKey(key);
        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(data);
    }

    /**
     * 用私钥加密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    private static byte[] encryptByPrivateKey(byte[] data, String key) throws Exception {
        if (data == null) {
            throw new IllegalArgumentException("Input data is null");
        }
        // 取得私钥
        Key privateKey = loadPrivateKey(key);
        // 对数据加密
        Cipher cipher = Cipher.getInstance(ECB_PKCS1_PADDING);
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);

        return cipher.doFinal(data);
    }

}
