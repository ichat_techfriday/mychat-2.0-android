package org.appspot.apprtc;

public class WebrtcConstants {
    //public static String OWN_TURN_SERVER="https://raadsecure.com";//change in strings.xml too key ="pref_room_server_url_own_server"
    public static String OWN_TURN_SERVER="";//change in strings.xml too key ="pref_room_server_url_own_server"
    public static String APPRTC_SERVER="https://appr.tc";//change in strings.xml too key ="pref_room_server_url_default"
    public static boolean isTurnServerEnabled=true;//change in strings.xml too key ="is_turn_server_enabled"

    public static String getBaseUrl(){

        if(isTurnServerEnabled)
            return OWN_TURN_SERVER;

        return APPRTC_SERVER;
    }
}
