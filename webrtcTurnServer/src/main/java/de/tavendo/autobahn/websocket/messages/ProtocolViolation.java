package de.tavendo.autobahn.websocket.messages;

import de.tavendo.autobahn.WebSocketException;

/// WebSockets reader detected WS protocol violation.
public class ProtocolViolation extends Message {

    public WebSocketException mException;

    public ProtocolViolation(WebSocketException e) {
        mException = e;
    }
}
